package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AllGameList;
import com.yowzapp.playstore.model.GameListModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vaishakha on 16/1/17.
 */
public class ProfileGames extends BaseActivity {
    RecyclerView recyclerView;
    PreferenceManager mPref;
    GameListModel gameListModel;
    Gson gson;
    List<AllGameList> myGameLists;
    ProfileGamesAdapter adapter;
    Toolbar toolbar;
    NestedScrollView nested;
    ProgressBar progressBar;
    int pages = 1, id;
    EndlessParentScrollListener endlessParentScrollListener;
    private LinearLayoutManager mLinearLayoutManager;
    private List<AllGameList> tempList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_location_layout);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        if (getIntent().hasExtra("ID"))
            id = getIntent().getIntExtra("ID", 0);

        Log.e("ID", id + "");

        mPref = PreferenceManager.instance(this);
        initializeAllComponent();

        TextView toolBarTitle = (TextView) findViewById(R.id.followers_toolbar_title);
        toolBarTitle.setText("Games");
        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mLinearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);

        endlessParentScrollListener = new EndlessParentScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    if (gameListModel.isHasMorePages()) {
                        Log.e("page_DONEG", page + "");
                        progressBar.setVisibility(View.VISIBLE);
                        pages = page;
                        loadGames(pages, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);

        loadGames(pages, false);

    }

    private void initializeAllComponent() {
        progressBar = (ProgressBar) findViewById(R.id.paginate_profile_bar);
        nested = (NestedScrollView) findViewById(R.id.nested_profile);
        toolbar = (Toolbar) findViewById(R.id.profilegame_toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_profile_games);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void loadGames(final int pages, final boolean isPaginate) {

        if (PlaythoraUtility.checkInternetConnection(ProfileGames.this)) {
            try {
                if (!isPaginate)
                    PlaythoraUtility.showProgressDialog(ProfileGames.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                httpClient.get(Config.GAME_LISTING + "?user_id=" + id + "&page=" + pages + "&is_completed=true", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.e("URL", Config.GAME_LISTING + "?user_id=" + id + "&page=" + pages);
                        progressBar.setVisibility(View.GONE);
                        if (!isPaginate)
                            PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("gameList", s);
                            gameListModel = new GameListModel();
                            gson = new Gson();
                            gameListModel = gson.fromJson(s, GameListModel.class);

                            Log.e("$$$$$$$", String.valueOf(gameListModel.getSuccess()));

                            myGameLists = gameListModel.getSuccess().getMy_games();
                            if (gameListModel.getCurrentPage() > 1) {
                                for (int i = 0; i < gameListModel.getSuccess().getMy_games().size(); i++) {
                                    tempList.add(gameListModel.getSuccess().getMy_games().get(i));
                                }
                                adapter.Refresh(tempList);
                            } else if (myGameLists.size() != 0) {
                                tempList = myGameLists;
                                adapter = new ProfileGamesAdapter(getApplicationContext(), myGameLists);
                                recyclerView.setAdapter(adapter);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable error) {
                        progressBar.setVisibility(View.GONE);
                        if (!isPaginate)
                            PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(bytes);
                            Log.e("myGamefailure", s);
                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                mPref.setAccessToken("");
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            } else
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                String s = new String(bytes);
                                Log.d("myGamefailure", s);

                                JSONObject object = new JSONObject(s);
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
                if (!isPaginate)
                    PlaythoraUtility.hideProgressDialog();
            }

        } else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 4) {
            pages = 1;
            endlessParentScrollListener.currentPage = 1;
            endlessParentScrollListener.previousTotalItemCount = 0;
            endlessParentScrollListener.loading = true;
            loadGames(pages, true);
        }
    }

    private class ProfileGamesAdapter extends RecyclerView.Adapter<ProfileGamesAdapter.AllGamesHolder> {
        Context context;
        List<AllGameList> allGameLists;


        public ProfileGamesAdapter(Context context, List<AllGameList> allGameLists) {
            this.context = context;
            this.allGameLists = allGameLists;
        }

        public void Refresh(List<AllGameList> arrayList) {
            Log.e("LISTING", "LISTING");
            this.allGameLists = arrayList;
            notifyDataSetChanged();
        }

        @Override
        public AllGamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.game_item_layout, parent, false);
            AllGamesHolder listHolder = new AllGamesHolder(mainGroup);

            return listHolder;
        }

        @Override
        public void onBindViewHolder(AllGamesHolder holder, int position) {

            final AllGameList model = allGameLists.get(position);

            final AllGamesHolder mainHolder = (AllGamesHolder) holder;

            holder.mGamename.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
            holder.location.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
            holder.duration.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));
            holder.mButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
            // holder.teamname.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

            // holder.teamname.setText(homeGamesModel.get(position).getTeamname());
            holder.mGamename.setText(model.getName());
            holder.location.setText(model.getLocation());
            holder.duration.setText(model.getMatch_status());
            holder.members.setText(model.getNo_members() + "/" + model.getMax_members());
            //s = String.valueOf(model.is_private());

            // Log.e("isPrivate",s+"");
            holder.mButton.setText("Completed");

            try {
                Glide.with(context).load(model.getCover_pic()).centerCrop().into(holder.mImage);
            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.circled_user);
            }

            holder.layoutGame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent toGame = new Intent(getApplicationContext(), GameMain.class);
                    toGame.putExtra("ACTIVITY", "PROFILEGAME");
                    toGame.putExtra("name", model.getName());
                    toGame.putExtra("location", model.getLocation());
                    toGame.putExtra("date", model.getDate());
                    toGame.putExtra("coverPic", model.getCover_pic());
                    toGame.putExtra("discription", model.getDiscription());
                    toGame.putExtra("sportsName", model.getSport_name());
                    toGame.putExtra("type", String.valueOf(model.is_private()));
                    toGame.putExtra("isComplete", String.valueOf(model.is_completed()));
                    toGame.putExtra("start_time", model.getStart_time());
                    toGame.putExtra("end_time", model.getEnd_time());
                    toGame.putExtra("id", String.valueOf(model.getId()));

                    startActivityForResult(toGame, 4);
                }
            });

        }

        @Override
        public int getItemCount() {
            return allGameLists.size();
        }

        public class AllGamesHolder extends RecyclerView.ViewHolder {
            CircleImageView mImage;
            TextView mGamename, duration, location, teamname, members;
            Button mButton;
            RelativeLayout layoutGame;

            public AllGamesHolder(View itemView) {
                super(itemView);

                mImage = (CircleImageView) itemView.findViewById(R.id.game_image);
                mGamename = (TextView) itemView.findViewById(R.id.tv_game_name);
                location = (TextView) itemView.findViewById(R.id.tv_place);
                teamname = (TextView) itemView.findViewById(R.id.tv_team_name);
                duration = (TextView) itemView.findViewById(R.id.tv_duration);
                members = (TextView) itemView.findViewById(R.id.textmember);
                mButton = (Button) itemView.findViewById(R.id.btn);
                layoutGame = (RelativeLayout) itemView.findViewById(R.id.gameItemLayout);

            }
        }
    }
}
