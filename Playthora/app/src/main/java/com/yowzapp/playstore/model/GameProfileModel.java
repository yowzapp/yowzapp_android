package com.yowzapp.playstore.model;

/**
 * Created by nakul on 18/11/16.
 */
public class GameProfileModel {
    private String name;
    private String id;
    private String image;

    public GameProfileModel(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
