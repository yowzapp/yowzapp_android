package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.fragment.ChatAllFragment.updateMyChatList;


/**
 * Created by hemanth on 23/11/16.
 */

public class ChattingActivity extends BaseActivity {

    public static String name;
    public static String Imageurl, id, type;
    public static int convId, convType;
    Toolbar toolbar;
    TextView toolbarTitle;
    CircleImageView profilePic;
    RelativeLayout relativeLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatting_layout);

        getWindow().setBackgroundDrawableResource(R.drawable.playthora_doodle);

        toolbar = (Toolbar) findViewById(R.id.chattingToolbar);
        profilePic = (CircleImageView) findViewById(R.id.chat_profile_image);
        relativeLayout = (RelativeLayout) findViewById(R.id.image_text_layout);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();

            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        initializeAll();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                PlaythoraUtility.hideSoftKeyboard(ChattingActivity.this);
            }
        });

        Intent i = getIntent();
        if (i.hasExtra("notification_type")) {
            Log.e("Coming from: ", "PUSH NOTIFICATION");
            name = i.getStringExtra("name");
            Imageurl = i.getStringExtra("profile_pic");
            type = i.getStringExtra("type");
            id = i.getStringExtra("id");
            convId = i.getIntExtra("Conv_id", 0);
            convType = i.getIntExtra("Conv_type", 0);
            Config.CHAT_ID = id;
            Config.CHAT_TYPE = type;
        } else {
            Log.e("Coming from: ", "IDK");
            name = i.getStringExtra("name");
            Imageurl = i.getStringExtra("image");
            type = i.getStringExtra("type");
            id = i.getStringExtra("id");
            convId = i.getIntExtra("Conv_id", 0);
            convType = i.getIntExtra("Conv_type", 0);
            Config.CHAT_ID = id;
            Config.CHAT_TYPE = type;
        }

        Log.e("VALUES", id + "," + type + "," + name + "," + convId + "," + convType);
        //toolbarTitle.setText(name);
        if (name != null && !name.isEmpty()) {
            toolbarTitle.setText(name.substring(0, 1).toUpperCase() + name.substring(1));
        }
        Log.e("IMAGE_URL", Imageurl + "");
        try {
            if (type.equalsIgnoreCase("player") || convType==3) {
                Glide.with(ChattingActivity.this).load(Imageurl).error(R.drawable.circled_user).centerCrop().into(profilePic);
            } else if (type.equalsIgnoreCase("team") || convType==2) {
                Glide.with(ChattingActivity.this).load(Imageurl).error(R.drawable.no_team).centerCrop().into(profilePic);
            } else if (type.equalsIgnoreCase("game") || convType==1) {
                Glide.with(ChattingActivity.this).load(Imageurl).error(R.drawable.game_null).centerCrop().into(profilePic);
            }
        } catch (Exception e) {
            if (type.equalsIgnoreCase("player") || convType==3) {
                profilePic.setImageResource(R.drawable.circled_user);
            } else if (type.equalsIgnoreCase("team") || convType==2) {
                profilePic.setImageResource(R.drawable.no_team);
            } else if (type.equalsIgnoreCase("game") || convType==1) {
                profilePic.setImageResource(R.drawable.game_null);
            }
        }

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked();
            }
        });
    }

    private void clicked() {

        if (type.equalsIgnoreCase("player")
                || convType == 3) {
            Intent intent = new Intent(ChattingActivity.this, PlayerProfileViewActivity.class);
            intent.putExtra("Id", id);
            startActivity(intent);
        } else if (type.equalsIgnoreCase("team")
                || convType == 2) {
            Intent intent = new Intent(ChattingActivity.this, OtherTeamViewActivity.class);
            intent.putExtra("ID", Integer.parseInt(id));
            startActivity(intent);
        } else if (type.equalsIgnoreCase("game")
                || convType == 1) {
            Intent intent = new Intent(ChattingActivity.this, GameMain.class);
            intent.putExtra("GAME_ID", id);
            startActivity(intent);
        }
    }

    private void initializeAll() {

        toolbarTitle = (TextView) findViewById(R.id.chatting_user_title);

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("notification_type")) {
            Intent homeIntent = new Intent(ChattingActivity.this, HomeScreenActivity.class);
            startActivity(homeIntent);
            finish();
        } else {
            updateMyChatList(ChattingActivity.this, "GOT THE CONTENT RECEIVER");
            super.onBackPressed();
        }
    }

}
