package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 2/1/17.
 */
public class CreateGameModelSuccess {
    private String name;
    private String id;
    private List<CreateGameMembers> members;
    private List<CreateGameMembersInvited> invited_members;

    public List<CreateGameMembersInvited> getInvited_members() {
        return invited_members;
    }

    public void setInvited_members(List<CreateGameMembersInvited> invited_members) {
        this.invited_members = invited_members;
    }

    public List<CreateGameMembers> getMembers() {
        return members;
    }

    public void setMembers(List<CreateGameMembers> members) {
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
