package com.yowzapp.playstore.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LocationModel;
import com.yowzapp.playstore.activty.PrefferedLocationActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

/**
 * Created by nakul on 18/11/16.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ProfileListHolder> {

    public static ArrayList<String> location;
    Context mContext;
    ArrayList<LocationModel> playerList;
    Boolean[] selectedlocation;
    Activity mActivity;


    public LocationAdapter(Context context, ArrayList<LocationModel> arrayList, Activity activity) {
        mContext = context;
        playerList = arrayList;
        selectedlocation = new Boolean[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            selectedlocation[i] = false;
        }
        this.mActivity = activity;
        location = new ArrayList();
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.perfered_location_list_item, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final ProfileListHolder holder, final int position) {

        holder.teamName.setText(playerList.get(position).getName());

        if (!selectedlocation[position]) {
            holder.selectedImage.setVisibility(View.GONE);
        } else {
            holder.selectedImage.setVisibility(View.VISIBLE);
        }


        holder.locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedlocation[position]) {
                    holder.selectedImage.setVisibility(View.GONE);
                    selectedlocation[position] = false;
                    location.remove(playerList.get(position).getName());

                } else {
                    holder.selectedImage.setVisibility(View.VISIBLE);
                    selectedlocation[position] = true;
                    location.add(playerList.get(position).getName());
                }
                PrefferedLocationActivity.sectedNo.setText(location.size() + " Location Selected");
                notifyDataSetChanged();

                Intent intent = new Intent();
//                intent.putExtra("preferrredlocation", playerList.get(position).getName() );
                mActivity.setResult(2, intent);
                mActivity.finish();

            }
        });


    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {
        TextView teamName;
        RelativeLayout locationLayout;
        ImageView selectedImage;

        public ProfileListHolder(View itemView) {
            super(itemView);
            teamName = (TextView) itemView.findViewById(R.id.perfer_loc_name);
            locationLayout = (RelativeLayout) itemView.findViewById(R.id.perefe_loc_lay);
            selectedImage = (ImageView) itemView.findViewById(R.id.loc_select);
            teamName.setTypeface(PlaythoraUtility.getFont(mContext, Config.RALEWAY_REGULAR));
        }

    }
}