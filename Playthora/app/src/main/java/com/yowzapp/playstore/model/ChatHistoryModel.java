package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by nakul on 07/02/17.
 */

public class ChatHistoryModel {
    private int currentPage;
    private boolean hasMorePages;
    private List<ChatListModel> success;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

    public List<ChatListModel> getSuccess() {
        return success;
    }

    public void setSuccess(List<ChatListModel> success) {
        this.success = success;
    }
}
