package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.ChattingActivity;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.model.PlayerChatDetailModel;
import com.yowzapp.playstore.model.PlayerChatObjectModel;
import com.yowzapp.playstore.model.TeamChatModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 23/11/16.
 */
public class TeamChatFragment extends Fragment {

    public ArrayList<TeamChatModel> teamChatArrayList;
    RecyclerView recyclerView;
    ProgressDialog dialog;
    String response;
    Gson mGson;
    PreferenceManager mPref;
    SwipeRefreshLayout swipeContainer;
    //    ProgressBar progressBar;
    TextView emptyText;
    NestedScrollView nested;
    LinearLayoutManager linearLayoutManager;
    PlayerChatDetailModel playerchat;
    int pages = 1;
    List<PlayerChatObjectModel> tempList;
    TeamChatAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.player_chat_layout, container, false);
        mPref = PreferenceManager.instance(getActivity());

        nested = (NestedScrollView) contentView.findViewById(R.id.nested_player_chat_list);
        emptyText = (TextView) contentView.findViewById(R.id.empty_player_chat_list);
        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        recyclerView = (RecyclerView) contentView.findViewById(R.id.playerChatRecyclerView);
//        progressBar = (ProgressBar) contentView.findViewById(R.id.player_chat_progress);
        swipeContainer = (SwipeRefreshLayout) contentView.findViewById(R.id.swipe_container);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        try {
            populateTeamChat(pages, false);
        } catch (Exception e) {
            e.printStackTrace();
//            progressBar.setVisibility(View.GONE);
        }

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    if (playerchat.isHasMorePages()) {
                        Log.e("page_DONEG", page + "");
                        pages = page;
                        populateTeamChat(pages, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
                //HomeFragment.relativeLayout.animate().translationY(HomeFragment.relativeLayout.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }

            @Override
            public void scrollUp() {
                //HomeFragment.relativeLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    populateTeamChat(1, true);
                    endlessParentScrollListener.currentPage = 1;
                    endlessParentScrollListener.previousTotalItemCount = 0;
                    endlessParentScrollListener.loading = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    swipeContainer.setRefreshing(false);
                }
            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);


        return contentView;

    }

    private void populateTeamChat(int pages, boolean isRefresh) {

        if (!mPref.getAccessToken().isEmpty()) {
            if (PlaythoraUtility.checkInternetConnection(getActivity())) {
                try {
                    if (isRefresh)
                        swipeContainer.setRefreshing(true);
//                    else progressBar.setVisibility(View.VISIBLE);
                    AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                    httpClient.addHeader("accessToken", mPref.getAccessToken());
                    Log.e("TEAM_CHAT", Config.PLAYER_CHAT_LISTING + "?team=true" + "&page=" + pages);
                    httpClient.get(Config.PLAYER_CHAT_LISTING + "?team=true" + "&page=" + pages, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            swipeContainer.setRefreshing(false);
//                            progressBar.setVisibility(View.GONE);
                            try {
                                String response = new String(responseBody);
                                Log.e("TEAMCHATRESPONSE", response);
                                playerchat = new PlayerChatDetailModel();
                                mGson = new Gson();
                                playerchat = mGson.fromJson(response, PlayerChatDetailModel.class);
                                if (playerchat.getSuccess().size() != 0) {
                                    if (playerchat.getCurrentPage() > 1) {
                                        emptyText.setVisibility(View.GONE);
                                        for (int i = 0; i < playerchat.getSuccess().size(); i++) {
                                            tempList.add(playerchat.getSuccess().get(i));
                                        }

                                        Log.e("venueList", String.valueOf(playerchat.getCurrentPage()));
                                        adapter.Refresh(tempList);
                                    } else {
                                        tempList = playerchat.getSuccess();
                                        emptyText.setVisibility(View.GONE);
                                        adapter = new TeamChatAdapter(getActivity(), playerchat.getSuccess());
                                        recyclerView.setAdapter(adapter);
                                    }
                                } else {
                                    emptyText.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable error) {
                            swipeContainer.setRefreshing(false);
//                            progressBar.setVisibility(View.GONE);

                            try {
                                String s = new String(bytes);
                                Log.e("player", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finishAffinity();
                                } else
                                    Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                try {
                                    String s = new String(bytes);
                                    Log.d("myGamefailure", s);

                                    JSONObject object = new JSONObject(s);
                                    Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    swipeContainer.setRefreshing(false);
//                    progressBar.setVisibility(View.GONE);
                }
            }
        }
    }


    public class TeamChatAdapter extends RecyclerView.Adapter<TeamChatViewHolder> {

        private List<PlayerChatObjectModel> arrayList;
        private Context context;

        public TeamChatAdapter(Context context, List<PlayerChatObjectModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }

        public void Refresh(List<PlayerChatObjectModel> arrayList) {
            this.arrayList = arrayList;
            notifyDataSetChanged();
        }

        @Override
        public TeamChatViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.player_chat_item_layout, viewGroup, false);
            TeamChatViewHolder listHolder = new TeamChatViewHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final TeamChatViewHolder holder, final int position) {
            final PlayerChatObjectModel model = arrayList.get(position);


            final TeamChatViewHolder mainHolder = (TeamChatViewHolder) holder;

            try {
                mainHolder.userName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.subText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.time.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

                mainHolder.userName.setText(model.getUser_name());
                mainHolder.subText.setText(model.getLast_message());
                mainHolder.time.setText(model.getDate());

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                Glide.with(context).load(model.getUser_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.circleImageView);
            } catch (Exception e) {
                mainHolder.circleImageView.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPref.getAccessToken().isEmpty()) {
                        PlaythoraUtility.LoginDialog(getActivity());
                    } else {
                        Intent chat = new Intent(getActivity(), ChattingActivity.class);
                        chat.putExtra("name", model.getUser_name());
                        chat.putExtra("image", model.getUser_pic());
                        chat.putExtra("id", model.getId() + "");
                        chat.putExtra("type", "team");
                        chat.putExtra("Conv_id", model.getUser_id());
                        chat.putExtra("Conv_type", 2);
                        startActivity(chat);
                    }

                }
            });

            mainHolder.itemView.setTag(model);


        }
    }

    public class TeamChatViewHolder extends RecyclerView.ViewHolder {

        TextView userName, time, subText;
        CircleImageView circleImageView;

        public TeamChatViewHolder(View view) {
            super(view);
            this.userName = (TextView) view.findViewById(R.id.chat_user_name);
            this.time = (TextView) view.findViewById(R.id.chatTime);
            this.circleImageView = (CircleImageView) view.findViewById(R.id.chat_image);
            this.subText = (TextView) view.findViewById(R.id.chat_sub_name);


        }
    }
}
