package com.yowzapp.playstore.fragment;

import com.yowzapp.playstore.model.ActivityLogList;

import java.util.List;

/**
 * Created by vaishakha on 16/11/16.
 */
public class HomeActivityLogModel {
    private String status;
    private List<ActivityLogList> success;
    private int currentPage;
    private boolean hasMorePages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ActivityLogList> getSuccess() {
        return success;
    }

    public void setSuccess(List<ActivityLogList> success) {
        this.success = success;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }
}
