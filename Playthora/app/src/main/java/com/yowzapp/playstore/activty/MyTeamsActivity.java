package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.HomeMyTeamList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 10/1/17.
 */
public class MyTeamsActivity extends BaseActivity {

    public LinearLayoutManager mLayoutManager;
    Toolbar toolbar;
    TextView toolBarTitle;
    TextView toolbarDone;
    ProgressBar progressBar;
    PreferenceManager mPref;
    RecyclerView recyclerView;
    Gson gson;
    MyTeamAdapter myTeamAdapter;
    String primTeamId, name, coverPic, memCount, followerCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_teams_layout);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        initializeAllComponents();

        Intent intent = getIntent();
        primTeamId = intent.getStringExtra("primaryID");
        name = intent.getStringExtra("pName");
        coverPic = intent.getStringExtra("pCoverPic");
        memCount = intent.getStringExtra("pMemCount");
        followerCount = intent.getStringExtra("pFollowCount");

        Log.e("primTeamId", primTeamId + "");
        Log.e("name", name + "");
        Log.e("coverPic", coverPic + "");
        Log.e("memCount", memCount + "");
        Log.e("followerCount", followerCount + "");

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent();
            intent.putExtra("primID", primTeamId);
            intent.putExtra("name", name);
            intent.putExtra("coverPic", coverPic);
            intent.putExtra("memCount", memCount);
            intent.putExtra("followerCount", followerCount);
            setResult(6, intent);
            finish();
            }
        });
        mLayoutManager = new LinearLayoutManager(MyTeamsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        try {
            populateMyTeams();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateMyTeams() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            httpClient.get(Config.MY_TEAM_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("myTeams", s);
                        gson = new Gson();
                        JSONObject jsonObject = new JSONObject(s);
                        JSONArray jsonArray = jsonObject.getJSONArray("success");
                        Log.e("@@@@@@@@", jsonArray.toString());
                        ArrayList<HomeMyTeamList> arrayList = new ArrayList<HomeMyTeamList>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            arrayList.add(gson.fromJson(jsonArray.get(i).toString(), HomeMyTeamList.class));
                            myTeamAdapter = new MyTeamAdapter(MyTeamsActivity.this, arrayList, primTeamId);
                            recyclerView.setAdapter(myTeamAdapter);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.myTeamsToolBar);
        toolBarTitle = (TextView) findViewById(R.id.my_teams_toolbar_title);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView = (RecyclerView) findViewById(R.id.my_teams_recycler);
        toolbarDone = (TextView) findViewById(R.id.toolbar_done);
        toolbarDone.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.fav_sports_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.fav_sports_selected) {
//            Intent intent = new Intent();
//            intent.putExtra("primID", primTeamId);
//            intent.putExtra("name", name);
//            intent.putExtra("coverPic", coverPic);
//            intent.putExtra("memCount", memCount);
//            intent.putExtra("followerCount", followerCount);
//            setResult(6, intent);
//            finish();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public class MyTeamAdapter extends RecyclerView.Adapter<MyTeamAdapter.TeamHolder> {

        List<HomeMyTeamList> arrayList;
        private Context context;
        private boolean[] mCheckedState;

        public MyTeamAdapter(MyTeamsActivity myTeamsActivity, ArrayList<HomeMyTeamList> arrayList, String primTeamId) {

            this.context = myTeamsActivity;
            this.arrayList = arrayList;
            mCheckedState = new boolean[arrayList.size()];
            for (int i = 0; i < arrayList.size(); i++) {
                if (primTeamId.equalsIgnoreCase(arrayList.get(i).getId() + "")) {
                    mCheckedState[i] = true;
                } else
                    mCheckedState[i] = false;
            }
        }

        @Override
        public TeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.invite_player_item_layout, parent, false);
            TeamHolder listHolder = new TeamHolder(mainGroup);

            return listHolder;
        }

        @Override
        public void onBindViewHolder(TeamHolder holder, final int position) {
            final HomeMyTeamList model = arrayList.get(position);

            TeamHolder mainHolder = (TeamHolder) holder;
            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());
                mainHolder.count.setText("Members: " + String.valueOf(model.getMembers_count()));


                if (!mCheckedState[position]) {
                    mainHolder.checkBox.setChecked(false);
                } else {
                    mainHolder.checkBox.setChecked(true);
                }

                try {
                    Glide.with(context).load(model.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
                } catch (Exception e) {
                    mainHolder.userImage.setImageResource(R.drawable.circled_user);
                }

                try {
                    mainHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            primTeamId = model.getId() + "";
                            name = model.getName();
                            coverPic = model.getCover_pic();
                            memCount = model.getMembers_count() + "";
                            followerCount = model.getFollowers_count() + "";


                            Log.e("id", primTeamId);
                            Log.e("name", name);
                            Log.e("coverPic", coverPic);
                            Log.e("memCount", memCount);
                            Log.e("followerCount", followerCount);

                            if (!mCheckedState[position]) {
                                mCheckedState[position] = true;
                            } else {
                                mCheckedState[position] = false;
                            }

                            for (int i = 0; i < arrayList.size(); i++) {
                                if (i == position) {
                                    mCheckedState[i] = true;
                                } else {
                                    mCheckedState[i] = false;
                                }
                            }
                            notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class TeamHolder extends RecyclerView.ViewHolder {
            CircleImageView userImage;
            TextView name, count;
            CheckBox checkBox;
            RelativeLayout makeAdmin;

            public TeamHolder(View view) {
                super(view);
                this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
                this.name = (TextView) view.findViewById(R.id.userName);
                this.count = (TextView) view.findViewById(R.id.count);
                this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                this.makeAdmin = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);
            }
        }
    }
}
