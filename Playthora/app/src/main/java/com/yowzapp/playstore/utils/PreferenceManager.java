package com.yowzapp.playstore.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hemanth on 8/12/16.
 */

public class PreferenceManager {

    private static final String USER_PREF_FILE_NAME = "UserDataPrefs";
    private static PreferenceManager sInstance;
    String gameId, rawDate, transactionId;
    String selectedSlotIds;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;


    private PreferenceManager(Context ctx) {
        mPreferences = ctx.getSharedPreferences(USER_PREF_FILE_NAME, Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
    }

    public static PreferenceManager instance(Context ctx) {
        if (sInstance == null)
            sInstance = new PreferenceManager(ctx);
        return sInstance;
    }

    public String getName() {
        return mPreferences.getString("Name", "");
    }

    public void setName(String UserName) {
        mEditor.putString("Name", UserName).commit();
    }

    public String getBio() {
        return mPreferences.getString("bio", "");
    }

    public void setBio(String bio) {
        mEditor.putString("bio", bio).commit();
    }

    public String getGcmToken() {
        return mPreferences.getString("gcmToken", "");
    }

    public void setGcmToken(String gcmToken) {
        mEditor.putString("gcmToken", gcmToken).commit();
    }

    public String getId() {
        return mPreferences.getString("id", "");
    }

    public void setId(String id) {
        mEditor.putString("id", id).commit();
    }

    public String getMobilVerified() {
        return mPreferences.getString("mobile_verified", "");
    }

    public void setMobilVerified(String mobileVerified) {
        mEditor.putString("mobile_verified", mobileVerified).commit();

    }

    public String getProfileType() {
        return mPreferences.getString("profile_type", "");
    }

    public void setProfileType(String profileType) {
        mEditor.putString("profile_type", profileType).commit();

    }

    public String getAccessToken() {
        return mPreferences.getString("accesstoken", "");
    }

    public void setAccessToken(String accessToken) {
        mEditor.putString("accesstoken", accessToken).commit();
    }

    public String getAndroid_id() {
        return mPreferences.getString("AndroidId", "");
    }

    public void setAndroid_id(String AndroidId) {
        mEditor.putString("AndroidId", AndroidId).commit();
    }

    public String getProfilePic() {
        return mPreferences.getString("profile_pic", "");
    }

    public void setProfilePic(String profile_pic) {
        mEditor.putString("profile_pic", profile_pic).commit();
    }

    public String getCoverPic() {
        return mPreferences.getString("cover_pic", "");
    }

    public void setCoverPic(String coverPic) {
        mEditor.putString("cover_pic", coverPic).commit();
    }

    public String getLocationName() {
        return mPreferences.getString("location_name", "");
    }

    public void setLocationName(String location_name) {
        mEditor.putString("location_name", location_name).commit();
    }

    public String getCityName() {
        return mPreferences.getString("city", "");
    }

    public void setCityName(String cityName) {
        mEditor.putString("city", cityName).commit();
    }

    public String getProfileUpdated() {
        return mPreferences.getString("profile_updated", "");
    }

    public void setProfileUpdated(String profileUpdated) {
        mEditor.putString("profile_updated", profileUpdated).commit();

    }

    public String getProfilePicUpdated() {
        return mPreferences.getString("profile_pic_updated", "");
    }

    public void setProfilePicUpdated(String profilePicUpdated) {
        mEditor.putString("profile_pic_updated", profilePicUpdated).commit();

    }

    public String getInterestUpdated() {
        return mPreferences.getString("interest_updated", "");
    }

    public void setInterestUpdated(String interestUpdated) {
        mEditor.putString("interest_updated", interestUpdated).commit();

    }

    public String getLocationUpdated() {
        return mPreferences.getString("location_updated", "");
    }

    public void setLocationUpdated(String locationUpdated) {
        mEditor.putString("location_updated", locationUpdated).commit();

    }

    public String getUserEmail() {
        return mPreferences.getString("userEmail", "");
    }

    public void setUserEmail(String userEmail) {
        mEditor.putString("userEmail", userEmail).commit();

    }

    public String getUserMobileNumber() {
        return mPreferences.getString("userMobileNumber", "");
    }

    public void setUserMobileNumber(String userMobileNumber) {
        mEditor.putString("userMobileNumber", userMobileNumber).commit();

    }

    public String getInterestList() {
        return mPreferences.getString("interest_list", "");
    }

    public void setInterestList(String interestList) {
        mEditor.putString("interest_list", interestList).commit();
    }

    public String getIsSocial() {
        return mPreferences.getString("isSocial", "");
    }

    public void setIsSocial(String isSocial) {
        mEditor.putString("isSocial", isSocial).commit();
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getRawDate() {
        return rawDate;
    }

    public void setRawDate(String rawDate) {
        this.rawDate = rawDate;
    }

    public void setTransactionID(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getSelectedSlotIds() {
        return mPreferences.getString("selectedSlotIds", "");
    }

    public void setSelectedSlotIds(String selectedSlotIds) {
        mEditor.putString("selectedSlotIds", selectedSlotIds).commit();
    }
}
