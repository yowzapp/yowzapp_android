package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.viewpagerindicator.CirclePageIndicator;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.GroundDetailModel;
import com.yowzapp.playstore.model.GroundSlots;
import com.yowzapp.playstore.model.Grounds;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.LoopViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

/**
 * Created by pramod on 23/11/16.
 */

public class CreateGameGroundSlots extends BaseActivity {
    private static String LOG_TAG = "RecyclerViewActivity";
    MyRecyclerViewAdapter mAdapter;
    Calendar c;
    SimpleDateFormat df;
    TextView groundName, groundLocationName, whenToPlay, groundText, pickTimeText, maxPlayersText, totalCost, howMany, slotPrice, noSlots, actualPrice;
    String getGroundNameString, getLocationNameString, getGroundImage;
    //ImageView groundImageView;
    int j, k;
    Toolbar toolbar;
    String groundId, coverPic, vName, vLocation, buttonPay, sportsId;
    int sportsIdPosition;
    int ratingStar;
    RatingBar rating;
    GroundDetailModel groundDetailModel = new GroundDetailModel();
    List<GroundSlots> groundSlots;
    Gson gson;
    PreferenceManager mPref;
    View timeSlotsViewAbove, timeSlotsViewBelow;
    String price, activity, aed;
    String selectedDate = "";
    ArrayList<String> startArrayList = new ArrayList<>();
    ArrayList<String> endArrayList = new ArrayList<>();
    ArrayList<String> slotIdArrayList = new ArrayList<>();
    ArrayList<String> slotPriceArrayList = new ArrayList<>();
    ArrayList<String> slotMemberArrayList = new ArrayList<>();
    ArrayList<String> slotDurationArrayList = new ArrayList<>();
    HashMap<Integer, Integer> minNumOfPlayersArray = new HashMap<>();
    int priceCount, total;
    Button pay;
    EditText numPlayers;
    String groundList = "", venueImages = "";
    ArrayList<Grounds> groundsArray;
    RecyclerView groundRecyclerView;
    GroundAdapter groundAdapter;
    LoopViewPager imageViewPager;
    int page;
    Timer timer;
    ArrayList<String> images;
    CirclePageIndicator indicator;
    boolean value;
    String SPORTSID;
    Toast toast;
    RelativeLayout relativeslot, relativeplayer;
    private RecyclerView mRecyclerView, tRecyclerView;
    private RecyclerView.Adapter tAdapter;
    private RecyclerView.LayoutManager mLayoutManager, tLayoutManager;
    private String sGroundName = "", sGroundId = "";
    private int currentNumOfPlayers = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_game_ground_slots);
        mPref = PreferenceManager.instance(getApplicationContext());
        activity = "";
        SPORTSID = "";

        imageViewPager = (LoopViewPager) findViewById(R.id.image_viewpager);
        indicator = (CirclePageIndicator) findViewById(R.id.circle_indicator_image);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        relativeslot = (RelativeLayout) findViewById(R.id.relative_sec);
        relativeplayer = (RelativeLayout) findViewById(R.id.recycler_third);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Grounds and slots");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        //   progressBar = (ProgressBar) findViewById(R.id.progressBarInterest);
        //   showText = (TextView) findViewById(com.feetapart.yowzapp.R.id.noItems);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        imageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                Log.e("POSITION", position + "");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Intent intent = getIntent();
        groundId = intent.getStringExtra("gID");
        vName = intent.getStringExtra("vName");
        vLocation = intent.getStringExtra("vLocation");
        coverPic = intent.getStringExtra("venueCover");
        ratingStar = intent.getIntExtra("Rating", 0);
        buttonPay = intent.getStringExtra("venue");

        //  groundList = intent.getStringExtra("GroundArrayList");

        if (intent.hasExtra("ACTIVITY")) {
            activity = intent.getStringExtra("ACTIVITY");
            SPORTSID = intent.getStringExtra("SPORTSID");
        }

        Log.e("groundList", groundList);
        Log.e("buttonPay " + buttonPay, " ");
        Log.e("IMAGES " + buttonPay, " ");


        Log.e("groundId ", groundId + "");
        Log.e("vName ", vName + "");
        Log.e("vLocation ", vLocation + "");
        Log.e("coverPic ", coverPic + "");
        Log.e("rating ", String.valueOf(ratingStar));

        c = Calendar.getInstance();
        df = new SimpleDateFormat("EEE, dd-MM-yyyy");

        Log.v("Time", String.valueOf(df.format(c.getTime())));
        mRecyclerView = (RecyclerView) findViewById(R.id.whenRecyclerView);
        tRecyclerView = (RecyclerView) findViewById(R.id.pickTimeRecycler);
        groundName = (TextView) findViewById(R.id.groundName);
        groundLocationName = (TextView) findViewById(R.id.placeName);
        whenToPlay = (TextView) findViewById(R.id.whenToPlay);
        groundText = (TextView) findViewById(R.id.groundsText);
        pickTimeText = (TextView) findViewById(R.id.pickTimeText);
        maxPlayersText = (TextView) findViewById(R.id.maxPlayersText);
        totalCost = (TextView) findViewById(R.id.totalCost);
        howMany = (TextView) findViewById(R.id.howMany);
        slotPrice = (TextView) findViewById(R.id.slot_price);
        price = getResources().getString(R.string.Aed);
        aed = getResources().getString(R.string.Aed);
        slotPrice.setText(aed + " 0");
        noSlots = (TextView) findViewById(R.id.noSlots);
        actualPrice = (TextView) findViewById(R.id.actualPrice);
        // groundImageView = (ImageView) findViewById(R.id.selectLocationImage);
        rating = (RatingBar) findViewById(R.id.rating);
        timeSlotsViewAbove = findViewById(R.id.recycleAboveTime);
        timeSlotsViewBelow = findViewById(R.id.recycleBelowTime);
        pay = (Button) findViewById(R.id.proceedToPay);
        setUpUI(findViewById(R.id.gameLayoutCreateGame));
        numPlayers = (EditText) findViewById(R.id.noOfPlayers);
        groundRecyclerView = (RecyclerView) findViewById(R.id.grounds);

        if (buttonPay != null && buttonPay.equalsIgnoreCase("pay")) {
            //  pay.setVisibility(View.VISIBLE);
            pay.setText("Create game");
            //   groundList="";
            // groundRecyclerView.setVisibility(View.GONE);
            // groundText.setVisibility(View.GONE);
            //   getGroundDeatil();
        } else {
            // pay.setVisibility(View.GONE);
            pay.setText("Save");

            // groundRecyclerView.setVisibility(View.VISIBLE);
            //  groundText.setVisibility(View.VISIBLE);

        }
        groundList = intent.getStringExtra("GroundArrayList");
        venueImages = intent.getStringExtra("IMAGES");

        Log.e("groundList", groundList + "");
        Log.e("venueImages", venueImages + "");
        images = new ArrayList<>();

        groundRecyclerView.setHasFixedSize(true);
        groundRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        try {
            JSONArray categoryArray = new JSONArray(groundList);
            JSONArray imageArray = new JSONArray(venueImages);
            gson = new Gson();
            groundsArray = new ArrayList<Grounds>();

            for (int i = 0; i < categoryArray.length(); i++) {
                groundsArray.add(gson.fromJson(categoryArray.get(i).toString(), Grounds.class));
                Log.e("JsonArray", categoryArray.get(i).toString());
            }

            for (int i = 0; i < imageArray.length(); i++) {
                images.add(imageArray.get(i).toString());
            }

            if (!groundsArray.isEmpty()) {
                if (groundRecyclerView.getAdapter() == null) {
                    if (groundsArray.size() > 1) {
                        relativeslot.setVisibility(View.GONE);
                        relativeplayer.setVisibility(View.GONE);
                        groundText.setText("Select ground");
                    } else {
                        relativeslot.setVisibility(View.VISIBLE);
                        relativeplayer.setVisibility(View.VISIBLE);
                        groundText.setText("Select ground");
                    }
                    groundAdapter = new GroundAdapter(groundsArray);
                    groundRecyclerView.setAdapter(groundAdapter);

                    // groundAdapter.notifyDataSetChanged();
                }
            }

            Log.e("IMAGE_LENGTH", images.size() + "");
            if (images.size() != 0) {
                ImageAdapter adapter = new ImageAdapter(CreateGameGroundSlots.this, images);
                imageViewPager.setAdapter(adapter);
                imageViewPager.setCurrentItem(0);
                indicator.setViewPager(imageViewPager);
                indicator.setStrokeWidth(0);
                value = false;
                pageSwitcher();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

     /*   try{
            Intent getName = getIntent();
            getGroundNameString = getName.getStringExtra("GroundName");
            getLocationNameString = getName.getStringExtra("GroundLocation");
            getGroundImage = getName.getStringExtra("GroundImage");
        }catch (Exception e){
            e.printStackTrace();
        }*/


        groundName.setText(vName);
        groundLocationName.setText(vLocation);
        rating.setRating(ratingStar);
       /* try{
            Glide.with(this).load(coverPic).centerCrop().into(groundImageView);
        }catch (Exception e){
            e.printStackTrace();
        }
*/

        mRecyclerView.setHasFixedSize(true);
        tRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        tLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        tRecyclerView.setLayoutManager(tLayoutManager);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new MyRecyclerViewAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);


        LinearLayoutManager layoutManagerOne = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        tRecyclerView.setLayoutManager(layoutManagerOne);


        groundName.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        groundLocationName.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        whenToPlay.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        groundText.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        pickTimeText.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        maxPlayersText.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        totalCost.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        howMany.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        pay.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this, Config.RALEWAY_REGULAR));
        //  numPlayers.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this,RALEWAY_REGULAR));


        // Code to Add an item with default animation
        //((MyRecyclerViewAdapter) mAdapter).addItem(obj, index);

        // Code to remove an item with default animation
        //((MyRecyclerViewAdapter) mAdapter).deleteItem(index);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pay.getText().toString().equalsIgnoreCase("Create game")) {

                    int max = 0;
                    try {
                        Log.e("length@@", minNumOfPlayersArray.values()+"");
                        if(minNumOfPlayersArray.values().size() > 0){
                            max = Collections.min(minNumOfPlayersArray.values());
                            Log.e("length@@", minNumOfPlayersArray.values()+"");
                        }
                        Log.e("length", String.valueOf(max));
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    if (toast != null) {
                        toast.cancel();
                    }
                    if (selectedDate.isEmpty()) {
                        toast = Toast.makeText(CreateGameGroundSlots.this, "Select date to book this place", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (startArrayList.size() == 0 || endArrayList.size() == 0) {
                        Log.e("checkslot1",startArrayList.size()+"" +endArrayList.size()+"");
                        toast = Toast.makeText(CreateGameGroundSlots.this, "Select slots to book this place", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (numPlayers.getText().toString().isEmpty()) {
                        toast = Toast.makeText(CreateGameGroundSlots.this, "please enter the number of players", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (Integer.parseInt(numPlayers.getText().toString()) > max) {
                        toast = Toast.makeText(CreateGameGroundSlots.this, "You have entered more than maximum number of players", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        save();
                    }

                } else {

                    int max = 0;
                    try {
                        if(minNumOfPlayersArray.values().size() > 0) {
                             max = Collections.min(minNumOfPlayersArray.values());
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    if (toast != null) {
                        toast.cancel();
                    }
                    if (selectedDate.isEmpty()) {
                        toast = Toast.makeText(CreateGameGroundSlots.this, "Select date to book this place", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (startArrayList.size() == 0 || endArrayList.size() == 0) {
                        Log.e("checkslot",startArrayList.size()+"" +endArrayList.size()+"");
                        toast = Toast.makeText(CreateGameGroundSlots.this, "Select slots to book this place", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (numPlayers.getText().toString().isEmpty()) {
                        toast = Toast.makeText(CreateGameGroundSlots.this, "please enter the number of players", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (Integer.parseInt(numPlayers.getText().toString()) > max) {
                        toast = Toast.makeText(CreateGameGroundSlots.this, "You have entered more than maximum number of players", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        saveSlots();
                    }

                }


            }
        });


    }

    /* @Override
     protected void onResume() {
         super.onResume();
         if(!images.isEmpty()) {
             pageSwitcher();
         }
     }*/
    public void pageSwitcher() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, 5000); // delay
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("ONSTOP", "ONSTOP");
        try {
            timer.cancel();
        } catch (Exception e) {
        }

    }

    private void setUpUI(View viewById) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(CreateGameGroundSlots.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                setUpUI(innerView);
            }
        }
    }

    private void save() {
        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put("ground_id", groundId);
            mainObj.put("vName", vName);
//            mainObj.put("slot_id", groundDetailModel.getSuccess().getSlot_id() + "");
            mainObj.put("total_price", total + "");
            mainObj.put("no_players", numPlayers.getText().toString());


            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < startArrayList.size(); i++) {
                JSONObject object = new JSONObject();
                object.put("id", slotIdArrayList.get(i));
                object.put("date", selectedDate);
                object.put("start_time", startArrayList.get(i));
                object.put("end_time", endArrayList.get(i));
                object.put("price", slotPriceArrayList.get(i));
                object.put("total_players", slotMemberArrayList.get(i));
                object.put("duration", slotDurationArrayList.get(i));
                jsonArray.put(object);
            }
            mainObj.put("slots", jsonArray);

            Log.e("JSONSTRING", mainObj.toString());


            Intent intent = new Intent(CreateGameGroundSlots.this, CreateGame.class);
            intent.putExtra("JsonObj", mainObj.toString());
            intent.putExtra("groundName", sGroundName);
            intent.putExtra("sportsId", sportsId);
            intent.putExtra("position", sportsIdPosition);
            intent.putExtra("vName", vName);
            intent.putExtra("venuList", "create");
            intent.putExtra("sportIdFrom", "slots");
            Log.e("groundName", sGroundName);
            Log.e("sportsId", sportsId);
            Log.e("position", sportsIdPosition+"");
            Log.e("vName", vName);
            startActivity(intent);


        } catch (JSONException e) {
            e.printStackTrace();
        }



/*
        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(CreateGameGroundSlots.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken",mPref.getAccessToken());

            try {
                StringEntity stringEntity = new StringEntity(mainObj.toString());


            httpClient.post(CreateGameGroundSlots.this,Config.BOOK_GROUND, stringEntity,"application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.v("BookBround",s);
                        Toast.makeText(CreateGameGroundSlots.this,"Congrats your slots has been booked",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(CreateGameGroundSlots.this,CreateGame.class);
                        startActivity(intent);
                        finish();

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure",s);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    String s = new String(responseBody);
                }
            });

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/
    }

    private void getGroundDeatil() {
        Log.e("getGroundDeatil", "getGroundDeatil");
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(CreateGameGroundSlots.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("ground_id", groundId);
            httpClient.post(Config.GROUND_DETAILS, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
//                        String s = "{\n" +
//                                "\t\"status\": \"success\",\n" +
//                                "\t\"statusCode\": 200,\n" +
//                                "\t\"message\": \"Ground details\",\n" +
//                                "\t\"success\": {\n" +
//                                "\t\t\"id\": 51,\n" +
//                                "\t\t\"name\": \"Badminton 002\",\n" +
//                                "\t\t\"max_member\": 4,\n" +
//                                "\t\t\"is_disabled\": false,\n" +
//                                "\t\t\"venue_id\": 14,\n" +
//                                "\t\t\"sports_id\": 20,\n" +
//                                "\t\t\"sports_name\": \"Badminton\",\n" +
//                                "\t\t\"created_at\": \"2017-09-21 08:12:22\",\n" +
//                                "\t\t\"updated_at\": \"2017-09-21 08:12:22\",\n" +
//                                "\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\"slot_id\": 51,\n" +
//                                "\t\t\"slot_price\": \"200\",\n" +
//                                "\t\t\"slot_duration\": \"60 mins\",\n" +
//                                "\t\t\"slots\": [{\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"9:00 am\",\n" +
//                                "\t\t\t\"end_time\": \"10:00 am\",\n" +
//                                "\t\t\t\"price\": 50,\n" +
//                                "\t\t\t\"total_players\": 8,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"10:00 am\",\n" +
//                                "\t\t\t\"end_time\": \"11:00 am\",\n" +
//                                "\t\t\t\"price\": 100,\n" +
//                                "\t\t\t\"total_players\": 5,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"11:00 am\",\n" +
//                                "\t\t\t\"end_time\": \"12:00 pm\",\n" +
//                                "\t\t\t\"price\": 20,\n" +
//                                "\t\t\t\"total_players\": 2,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"12:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"1:00 pm\",\n" +
//                                "\t\t\t\"price\": 10,\n" +
//                                "\t\t\t\"total_players\": 10,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"1:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"2:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"2:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"3:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"3:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"4:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"4:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"5:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"5:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"6:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"6:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"7:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"7:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"8:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"8:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"9:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"9:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"10:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}, {\n" +
//                                "\t\t\t\"date\": \"26-03-2018\",\n" +
//                                "\t\t\t\"start_time\": \"10:00 pm\",\n" +
//                                "\t\t\t\"end_time\": \"11:00 pm\",\n" +
//                                "\t\t\t\"price\": 0,\n" +
//                                "\t\t\t\"total_players\": 0,\n" +
//                                "\t\t\t\"is_booked\": false\n" +
//                                "\t\t}]\n" +
//                                "\t}\n" +
//                                "}";

                        Log.e("groundDetail", s);
                        groundDetailModel = new GroundDetailModel();
                        gson = new Gson();
                        groundDetailModel = gson.fromJson(s, GroundDetailModel.class);
                        if (null != groundDetailModel.getSuccess()) {

                            price = getResources().getString(R.string.Aed);
                            aed = getResources().getString(R.string.Aed);
                            slotPrice.setText("0 " + aed);
//                            maxPlayersText.setText("Max " + String.valueOf(groundDetailModel.getSuccess().getMax_member()) + " players");
//                            actualPrice.setText("* " + "price per slots is" + " " + groundDetailModel.getSuccess().getSlot_price() + " " + aed);
                            actualPrice.setText("*price may vary according to slots");
//                            numPlayers.setText(String.valueOf(groundDetailModel.getSuccess().getMax_member()));

//                            Log.e("price", groundDetailModel.getSuccess().getSlot_price());
//                            Log.e("maxMembers", String.valueOf(groundDetailModel.getSuccess().getMax_member()));

                            if (groundDetailModel.getSuccess().getSlots().size() != 0) {
                                tAdapter = new DateTimeAdapter(groundDetailModel.getSuccess().getSlots());
                                tRecyclerView.setAdapter(tAdapter);
                                timeSlotsViewAbove.setVisibility(View.VISIBLE);
                                timeSlotsViewBelow.setVisibility(View.VISIBLE);
                                tRecyclerView.setVisibility(View.VISIBLE);
                                noSlots.setVisibility(View.GONE);
                            } else {
                                tRecyclerView.setVisibility(View.GONE);
                                noSlots.setVisibility(View.VISIBLE);
                                noSlots.setText("No time slots available");
                                timeSlotsViewAbove.setVisibility(View.GONE);
                                timeSlotsViewBelow.setVisibility(View.GONE);
                            }


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 400 || object.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(CreateGameGroundSlots.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(CreateGameGroundSlots.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(CreateGameGroundSlots.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(CreateGameGroundSlots.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(CreateGameGroundSlots.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private ArrayList<DataObject> getDataSet() {


        ArrayList results = new ArrayList<DataObject>();
        for (int index = 0; index < 20; index++) {

            if (index == 0) {
                c.add(Calendar.DATE, 0);
            } else {
                c.add(Calendar.DATE, 1);
            }

            Log.v("Index", String.valueOf(index));
            DataObject obj = new DataObject(String.valueOf(df.format(c.getTime())));
            Log.e("format", String.valueOf(df.format(c.getTime())));
            results.add(index, obj);
        }
        return results;
    }

    private void populateDate(String dateDay) {
        Log.v("sDate", dateDay);
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(CreateGameGroundSlots.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("ground_id", groundId);
            params.add("date", dateDay);
            Log.e("groundParams", "groundId: "+groundId+"   date: "+dateDay);
            httpClient.post(Config.GROUND_DETAILS, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("selectedDateResponse", s);
                        groundDetailModel = new GroundDetailModel();
                        gson = new Gson();
                        groundDetailModel = gson.fromJson(s, GroundDetailModel.class);
                        if (null != groundDetailModel.getSuccess()) {

                            if (groundDetailModel.getSuccess().getSlots().size() != 0) {
                                tAdapter = new DateTimeAdapter(groundDetailModel.getSuccess().getSlots());
                                tRecyclerView.setAdapter(tAdapter);
                                timeSlotsViewAbove.setVisibility(View.VISIBLE);
                                timeSlotsViewBelow.setVisibility(View.VISIBLE);
                                tRecyclerView.setVisibility(View.VISIBLE);
                                noSlots.setVisibility(View.GONE);
                            } else {
                                tRecyclerView.setVisibility(View.GONE);
                                noSlots.setVisibility(View.VISIBLE);
                                noSlots.setText("No time slots available");
                                timeSlotsViewAbove.setVisibility(View.GONE);
                                timeSlotsViewBelow.setVisibility(View.GONE);
                            }

                            String price = getResources().getString(R.string.Aed);
                            aed = getResources().getString(R.string.Aed);
                            slotPrice.setText("0 " + aed);
//                            maxPlayersText.setText("Max " + String.valueOf(groundDetailModel.getSuccess().getMax_member()) + " players");
                            maxPlayersText.setText("");
                            actualPrice.setText("*price may vary according to slots");

//                            Log.e("price", groundDetailModel.getSuccess().getSlot_price());
//                            Log.e("maxMembers", String.valueOf(groundDetailModel.getSuccess().getMax_member()));

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 400 || object.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(CreateGameGroundSlots.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(CreateGameGroundSlots.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(CreateGameGroundSlots.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(CreateGameGroundSlots.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(CreateGameGroundSlots.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private ArrayList<DataObject> getData() {
        ArrayList results = new ArrayList<DataObject>();
        for (int index = 0; index < 20; index++) {
            DataObject obj = new DataObject("faimt");
            results.add(index, obj);
        }
        return results;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ground_slots, menu);

        MenuItem item = menu.findItem(R.id.action_done);

       /* if(buttonPay!=null && buttonPay.equalsIgnoreCase("pay")){
            item.setVisible(false);
        }else {
           item.setVisible(true);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

/*        if(id == R.id.action_done){
            int max = 0;
           try {
               max = groundDetailModel.getSuccess().getMax_member();
           }catch (NullPointerException e){
               e.printStackTrace();
           }

            if(toast!=null){
                toast.cancel();
            }
            if(selectedDate.isEmpty()){
                toast = Toast.makeText(CreateGameGroundSlots.this,"Select date to book this place",Toast.LENGTH_SHORT);
                toast.show();
            }else if(startArrayList.size()==0 || endArrayList.size()==0) {
               toast =  Toast.makeText(CreateGameGroundSlots.this,"Select slots to book this place",Toast.LENGTH_SHORT);
                toast.show();
            } else if(numPlayers.getText().toString().isEmpty()) {
                toast = Toast.makeText(CreateGameGroundSlots.this,"please enter the number of players",Toast.LENGTH_SHORT);
                toast.show();
            }else  if(Integer.parseInt(numPlayers.getText().toString()) > max){
                toast = Toast.makeText(CreateGameGroundSlots.this,"You have entered more than maximum number of players",Toast.LENGTH_SHORT);
                toast.show();
            }else {
               saveSlots();

            }

        }*/

        return super.onOptionsItemSelected(item);
    }

    private void saveSlots() {
        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put("ground_id", groundId);
            mainObj.put("vName", vName);
//            mainObj.put("slot_id", groundDetailModel.getSuccess().getSlot_id() + "");
            mainObj.put("total_price", total + "");
            mainObj.put("no_players", numPlayers.getText().toString());

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < startArrayList.size(); i++) {
                JSONObject object = new JSONObject();
//                object.put("date", selectedDate);
//                mainObj.put("slot_id", groundDetailModel.getSuccess().getSlots().get(i).getId() + "");
//                object.put("start_time", startArrayList.get(i));
//                object.put("end_time", endArrayList.get(i));
                object.put("id", slotIdArrayList.get(i));
                object.put("date", selectedDate);
                object.put("start_time", startArrayList.get(i));
                object.put("end_time", endArrayList.get(i));
                object.put("price", slotPriceArrayList.get(i));
                object.put("total_players", slotMemberArrayList.get(i));
                object.put("duration", slotDurationArrayList.get(i));
                jsonArray.put(object);
            }
            mainObj.put("slots", jsonArray);

            Log.e("JSONSTRING", mainObj.toString());

            if (activity.equalsIgnoreCase("NOTIFIATION")) {
                Intent intent = new Intent(CreateGameGroundSlots.this, CreateGame.class);
                intent.putExtra("JsonObj", mainObj.toString());
                intent.putExtra("groundName", sGroundName);
                intent.putExtra("vName", vName);
                intent.putExtra("ACTIVITY", "NOTIFIATION");
                intent.putExtra("SPORTSID", SPORTSID);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra("JsonObj", mainObj.toString());
                intent.putExtra("groundName", sGroundName);
                intent.putExtra("vName", vName);
                setResult(4, intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    class RemindTask extends TimerTask {
        @Override
        public void run() {
            try {
                CreateGameGroundSlots.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final int lastPosition = imageViewPager.getAdapter().getCount();
                        if (page < lastPosition) {
                            if (!value) {
                                Log.e("ONSTOP", value + "");
                                imageViewPager.setCurrentItem(0, false);
                                value = true;
                            } else {
                                Log.e("ONSTOP", value + ",");
                                imageViewPager.setCurrentItem(page++, false);
                                value = true;
                            }
                        } else if (page == lastPosition) {
                            imageViewPager.setCurrentItem(0, false);
                            value = true;
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    if (CreateGameGroundSlots.this == null) {
                        return;
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

        Boolean[] selectedCatogorie;
        private String LOG_TAG = "MyRecyclerViewAdapter";
        private ArrayList<DataObject> mDataset;

        public MyRecyclerViewAdapter(ArrayList<DataObject> myDataset) {
            mDataset = myDataset;
            j = myDataset.size();
            selectedDate = mDataset.get(0).getDateDay();
            selectedCatogorie = new Boolean[j];
            for (int i = 0; i < myDataset.size(); i++) {
                if (i == 0) {
                    selectedCatogorie[i] = true;
                } else {
                    selectedCatogorie[i] = false;
                }

            }
        }

        public void Referesh() {

            selectedDate = mDataset.get(0).getDateDay();
            selectedCatogorie = new Boolean[mDataset.size()];
            for (int i = 0; i < mDataset.size(); i++) {
                if (i == 0) {
                    selectedCatogorie[i] = true;
                } else {
                    selectedCatogorie[i] = false;
                }

            }
            notifyDataSetChanged();
        }

        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.create_game_ground_slots_items, parent, false);

            DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(final DataObjectHolder holder, final int position) {

            // holder.dateTime.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this,RALEWAY_REGULAR));
            holder.dateTime.setText(mDataset.get(position).getDateDay());
            Log.v("date", mDataset.get(position).getDateDay());

            if (position == 0) {
                holder.dateTime.setText("Today");


            }
            if (position == 1) {
                holder.dateTime.setText("Tomorrow");

            }

            if (!selectedCatogorie[position]) {
                holder.dateTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_unselected));
            } else {
                holder.dateTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_selected));
                // mainHolder.sportsCategoryName.setTextColor(getResources().getColor(R.color.white));
            }

   /*         holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   populateDate(mDataset.get(position).getDateDay());
                }
            });*/

            holder.dateTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    total = 0;
                    priceCount = 0;
                    minNumOfPlayersArray.clear();
                    numPlayers.setText("");
                    maxPlayersText.setText("");
                    startArrayList = new ArrayList<>();
                    endArrayList = new ArrayList<>();
                    slotIdArrayList = new ArrayList<>();
                    slotPriceArrayList = new ArrayList<>();
                    slotMemberArrayList = new ArrayList<>();
                    slotDurationArrayList = new ArrayList<>();

                    selectedDate = mDataset.get(position).getDateDay();

                    if (sGroundName.isEmpty()) {
                        Toast.makeText(CreateGameGroundSlots.this, "Select ground book this slot", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    Log.v("Booleas", String.valueOf(selectedCatogorie[position]));
                    if (!selectedCatogorie[position]) {
                        //  Toast.makeText(getApplicationContext(),"SelectedFilter"+model.getName(),Toast.LENGTH_SHORT).show();
                        selectedCatogorie[position] = true;
                        Log.v("Booleasf", String.valueOf(selectedCatogorie[position]));
                        //  holder.dateTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_unselected));
                        populateDate(mDataset.get(position).getDateDay());

                    } else {
                        selectedCatogorie[position] = false;
                        Log.v("Booleast", String.valueOf(selectedCatogorie[position]));
                        //  holder.dateTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_selected));


                    }
                    for (int i = 0; i < mDataset.size(); i++) {
                        if (i == position) {
                            selectedCatogorie[i] = true;
                        } else {
                            selectedCatogorie[i] = false;
                        }


                    }
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView label;
            TextView dateTime;

            public DataObjectHolder(View itemView) {
                super(itemView);
                dateTime = (TextView) itemView.findViewById(R.id.slotItems);
                Log.i(LOG_TAG, "Adding Listener");

            }
        }
    }

    public class DateTimeAdapter extends RecyclerView.Adapter<DateTimeAdapter.DataObjectHolder> {
        Boolean[] selectedTime;
        private String LOG_TAG = "DateTimeAdapter";
        private List<GroundSlots> mTimeSet;

        public DateTimeAdapter(List<GroundSlots> myDataset) {
            mTimeSet = myDataset;
            j = myDataset.size();

            selectedTime = new Boolean[j];
            for (int i = 0; i < j; i++) {
                selectedTime[i] = false;
            }
        }

        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.create_game_ground_slots_items, parent, false);

            DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(final DataObjectHolder holder, final int position) {

            // holder.time.setTypeface(PlaythoraUtility.getFont(CreateGameGroundSlots.this,RALEWAY_REGULAR));
            final String startTime, endTime;
            startTime = mTimeSet.get(position).getStart_time();
            endTime = mTimeSet.get(position).getEnd_time();
            holder.time.setText(startTime + " - " + endTime);


            if (!selectedTime[position]) {
                holder.time.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_unselected));
            } else {
                holder.time.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_selected));
                // mainHolder.sportsCategoryName.setTextColor(getResources().getColor(R.color.white));
            }
            if (mTimeSet.get(position).is_booked()) {
                holder.time.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slot_booked));
            }

            holder.time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* for(int i=0; i<j; i++){
                        selectedTime[i]=false;
                        if(!selectedTime[i]){
                            holder.time.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_unselected));
                        }

                    }*/

                    if (mTimeSet.get(position).is_booked()) {
                        Toast.makeText(getApplicationContext(), "This slot has been booked already, try other slot!", Toast.LENGTH_SHORT).show();
                        return;
                    }

//                    Log.e("slotPrice", groundDetailModel.getSuccess().getSlot_price());
//                    priceCount = Integer.parseInt(groundDetailModel.getSuccess().getSlot_price());
                    priceCount = Integer.parseInt(groundDetailModel.getSuccess().getSlots().get(position).getPrice());
//                    numPlayers.setText(String.valueOf(groundDetailModel.getSuccess().getMax_member()));
                    int minNumOfTotalMembers = groundDetailModel.getSuccess().getSlots().get(position).getTotal_players();


                    Log.v("Boolean", String.valueOf(selectedTime[position]));

                    if (!selectedTime[position]) {
                        //  Toast.makeText(getApplicationContext(),"SelectedFilter"+model.getName(),Toast.LENGTH_SHORT).show();
                        selectedTime[position] = true;
                        Log.v("Booleantrue", String.valueOf(selectedTime[position]));
                        notifyDataSetChanged();
                        startArrayList.add(startTime);
                        endArrayList.add(endTime);
                        slotIdArrayList.add(groundDetailModel.getSuccess().getSlots().get(position).getId());
                        slotPriceArrayList.add(groundDetailModel.getSuccess().getSlots().get(position).getPrice());
                        slotMemberArrayList.add(String.valueOf(groundDetailModel.getSuccess().getSlots().get(position).getTotal_players()));
                        slotDurationArrayList.add(String.valueOf(groundDetailModel.getSuccess().getSlots().get(position).getDuration()));


                        minNumOfPlayersArray.put(position, minNumOfTotalMembers);
                        int minimumMember = Collections.min(minNumOfPlayersArray.values());
                        numPlayers.setText(String.valueOf(minimumMember));
                        maxPlayersText.setText("Max " + String.valueOf(minimumMember) + " players");



                        total = total + priceCount;
                        Log.v("count", String.valueOf(total));
                        slotPrice.setText(String.valueOf(total) + " " + aed);

                    } else {
                        selectedTime[position] = false;
                        Log.v("Booleanfalse", String.valueOf(selectedTime[position]));
                        notifyDataSetChanged();
                        startArrayList.remove(startTime);
                        endArrayList.remove(endTime);
                        slotIdArrayList.remove(groundDetailModel.getSuccess().getSlots().get(position).getId());
                        slotPriceArrayList.remove(groundDetailModel.getSuccess().getSlots().get(position).getPrice());
                        slotMemberArrayList.remove(String.valueOf(groundDetailModel.getSuccess().getSlots().get(position).getTotal_players()));
                        slotDurationArrayList.remove(String.valueOf(groundDetailModel.getSuccess().getSlots().get(position).getDuration()));

                        minNumOfPlayersArray.remove(position);
                        if(minNumOfPlayersArray.size()!=0) {
                            int minimumMember = Collections.min(minNumOfPlayersArray.values());
                            numPlayers.setText(String.valueOf(minimumMember));
                            maxPlayersText.setText("Max " + String.valueOf(minimumMember) + " players");
                        } else {
                            numPlayers.setText("");
                            maxPlayersText.setText("");
                        }


                        if (total != 0) {
                            total = total - priceCount;
                        }
                        Log.v("count", String.valueOf(total));
                        slotPrice.setText(String.valueOf(total) + " " + aed);

                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return mTimeSet.size();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView label;
            TextView time;

            public DataObjectHolder(View itemView) {
                super(itemView);
                time = (TextView) itemView.findViewById(R.id.slotItems);
                Log.i(LOG_TAG, "Adding Listener");

            }
        }
    }

    private class GroundAdapter extends RecyclerView.Adapter<GroundAdapter.GroundHolder> {

        ArrayList<Grounds> gList;
        Boolean[] selectedGround;


        public GroundAdapter(ArrayList<Grounds> groundsArray) {
            this.gList = groundsArray;
            k = groundsArray.size();
            selectedGround = new Boolean[k];
            for (int i = 0; i < k; i++) {
                selectedGround[i] = false;
            }
            if (gList.size() == 1) {
                selectedGround[0] = true;
                groundId = String.valueOf(gList.get(0).getId());
                sportsId = String.valueOf(gList.get(0).getSports_id());
                sGroundName = gList.get(0).getName();
                getGroundDeatil();
            }
        }

        @Override
        public GroundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_item, parent, false);
            GroundHolder listHolder = new GroundHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(final GroundHolder holder, final int position) {
            if (!gList.get(0).toString().isEmpty()) {
                holder.groundName.setText(gList.get(position).getName());
            } else {
                holder.groundName.setVisibility(View.GONE);
            }
            Log.e("groundName", gList.get(position).getName());

            if (!selectedGround[position]) {
                holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                holder.groundName.setTextColor(getResources().getColor(R.color.text_one));
            } else {
                holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                holder.groundName.setTextColor(getResources().getColor(R.color.white));
            }

            holder.groundName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    total = 0;
                    priceCount = 0;
                    minNumOfPlayersArray.clear();
                    numPlayers.setText("");
                    maxPlayersText.setText("");

                    startArrayList = new ArrayList<>();
                    endArrayList = new ArrayList<>();
                    slotIdArrayList = new ArrayList<>();
                    slotPriceArrayList = new ArrayList<>();
                    slotMemberArrayList = new ArrayList<>();
                    slotDurationArrayList = new ArrayList<>();

                    if (selectedGround[position]) {
                        return;
                    }

                    relativeslot.setVisibility(View.VISIBLE);
                    relativeplayer.setVisibility(View.VISIBLE);

                    mAdapter.Referesh();
                    sGroundName = gList.get(position).getName();
                    groundId = String.valueOf(gList.get(position).getId());
                    sportsId = String.valueOf(gList.get(position).getSports_id());
                    // sportsIdPosition = (5);


                    Log.e("gId", groundId);
                    Log.e("sportsId", sportsId);
                    Log.e("sportsIdPosition", sportsIdPosition + "");


                    if (!selectedGround[position]) {
                        selectedGround[position] = true;
                        holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                        holder.groundName.setTextColor(getResources().getColor(R.color.text_one));
                        getGroundDeatil();

                        // selectGroundsArray.add(String.valueOf(gList.get(position).getId()));
                        //  Log.e("GroundID", String.valueOf(gList.get(position).getId()));

                    } else {
                        selectedGround[position] = false;
                        holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                        holder.groundName.setTextColor(getResources().getColor(R.color.white));


                    }

                    for (int i = 0; i < gList.size(); i++) {
                        if (i == position) {
                            selectedGround[i] = true;
                        } else {
                            selectedGround[i] = false;
                        }
                    }
                    notifyDataSetChanged();
                }
            });

        }


        @Override
        public int getItemCount() {
            return gList.size();
        }

        public class GroundHolder extends RecyclerView.ViewHolder {
            TextView groundName;

            public GroundHolder(View itemView) {
                super(itemView);
                groundName = (TextView) itemView.findViewById(R.id.groundName);
                groundName.setTypeface(PlaythoraUtility.getFont(itemView.getContext(), Config.RALEWAY_REGULAR));
            }
        }
    }


    private class ImageAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        ArrayList<String> images;
        Context context;
        String imageUrl;

        public ImageAdapter(Context context, ArrayList<String> images) {
            this.context = context;
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            View itemView = mLayoutInflater.inflate(R.layout.image_view_layout, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.images);

            if (PlaythoraUtility.checkInternetConnection(context)) {
                try {
                    imageUrl = images.get(position);
                    Glide.with(context).load(imageUrl).centerCrop().into(imageView);
                    container.addView(itemView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }
}
