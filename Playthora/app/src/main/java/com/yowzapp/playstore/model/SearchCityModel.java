package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 14/11/16.
 */
public class SearchCityModel {
    private String locname;
    private int id;

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
