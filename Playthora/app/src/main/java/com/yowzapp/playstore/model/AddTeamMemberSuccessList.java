package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by vaishakha on 27/12/16.
 */
public class AddTeamMemberSuccessList {
    private List<AddTeamMembersList> data;

    public List<AddTeamMembersList> getData() {
        return data;
    }

    public void setData(List<AddTeamMembersList> data) {
        this.data = data;
    }
}
