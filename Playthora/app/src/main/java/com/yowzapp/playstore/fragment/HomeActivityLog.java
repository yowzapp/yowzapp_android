package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.ActivityLogDetail;
import com.yowzapp.playstore.activty.GameMain;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.activty.OtherTeamViewActivity;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.activty.SignupLoginActivity;
import com.yowzapp.playstore.model.ActivityLogList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 15/11/16.
 */
public class HomeActivityLog extends Fragment {
    View contentView;
    TextView emptyText, loginText;
    Button login;
    RecyclerView recyclerView;
    Gson gson;
    HomeActivityLogModel model;
    ActivityAdapter adapter;
    String response;
    PreferenceManager mPref;
    JSONObject jsonObject;
    String comments = "";
    SwipeRefreshLayout swipeContainer;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<ActivityLogList> tempList;
    ProgressBar progressBar, paginateProgressBar;
    private int visibleThreshold = 10;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.home_activity_layout, container, false);

        initializeAllComponents();
        mPref = PreferenceManager.instance(getActivity());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // Log.e("ACTIVITYLOGS","SCROL11111");
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                try {
                    if (model.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                pages += 1;
                                try {
                                    loadActivity(pages, true);
                                    paginateProgressBar.setVisibility(View.VISIBLE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    swipeContainer.setRefreshing(false);
                                    paginateProgressBar.setVisibility(View.GONE);
                                }
                                adapter.notifyDataSetChanged();

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }
            }
        });

        swipeContainer = (SwipeRefreshLayout) contentView.findViewById(R.id.activityLog_refresh);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    previousTotal = 0;
                    pages = 1;
                    loading = false;
                    loadActivity(1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);

        if (!mPref.getAccessToken().isEmpty())
            loadActivity(pages, false);
        else {
            loginText.setVisibility(View.VISIBLE);
            login.setVisibility(View.VISIBLE);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), SignupLoginActivity.class);
                    startActivity(intent);
                }
            });

        }

        return contentView;
    }

    private void loadActivity(final int pages, final boolean swipe) {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!swipe)
                            progressBar.setVisibility(View.VISIBLE);
                        AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                        httpClient.addHeader("accessToken", mPref.getAccessToken());
                        httpClient.get(Config.ACTIVITY_LOG + "?page=" + pages, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                swipeContainer.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                String s = new String(responseBody);
                                Log.e("AcivityLogSuccess", s);
                                Log.e("RANK", "4");
                                try {
                                    jsonObject = new JSONObject(s);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    gson = new Gson();
                                    model = new HomeActivityLogModel();
                                    model = gson.fromJson(s, HomeActivityLogModel.class);
                                    if (model.getStatus().equalsIgnoreCase("success")) {
                                        if (model.getSuccess().size() != 0) {
                                            if (model.getCurrentPage() > 1) {
                                                for (int i = 0; i < model.getSuccess().size(); i++) {
                                                    tempList.add(model.getSuccess().get(i));
                                                }
                                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                                adapter.Refresh(tempList, 2);
                                            } else {
                                                tempList = model.getSuccess();
                                                emptyText.setVisibility(View.GONE);
                                                adapter = new ActivityAdapter(getActivity(), model.getSuccess());
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        recyclerView.setAdapter(adapter);
                                                    }
                                                });
                                            }
                                        } else {
                                            emptyText.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable error) {
                                swipeContainer.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("AcivityLogfailure", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), SignupLoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();
                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("myGamefailure", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        paginateProgressBar.setVisibility(View.GONE);
                    }
                }
            });
        } else {
            swipeContainer.setRefreshing(false);
            paginateProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == 5) {
                boolean islike = data.getBooleanExtra("ISLIKE", false);
                int position = data.getIntExtra("POSITION", 0);
                int cmt = data.getIntExtra("COMMENTS", 0);
                String likeCount = data.getStringExtra("LIKESCOUNT");

                int i = Integer.parseInt(likeCount);
                Log.e("LIKE_COUNT", i + "");

                Log.e("LIKE_COUNT", islike + ":islike");
                Log.e("LIKE_COUNT", position + ":position");
                Log.e("LIKE_COUNT", cmt + ":cmt");
                Log.e("LIKE_COUNT", likeCount + ":likeCount");

                Log.e("LIKE_COUNT", tempList.get(position).getLike_count() + ",");
                Log.e("LIKE_COUNT", tempList.get(position).is_liked() + "");

                tempList.get(position).setComment_count(cmt);
                //  tempList.get(position).setLike_count(likeCount);

                if (tempList.get(position).is_liked()) {
                    if (!islike) {
                        Log.e("LIKE_COUNT", "islike");
                        tempList.get(position).setIs_liked(false);
                        tempList.get(position).setLike_count(tempList.get(position).getLike_count() - 1);
                    }
                } else {
                    if (islike) {
                        tempList.get(position).setIs_liked(true);
                        tempList.get(position).setLike_count(tempList.get(position).getLike_count() + 1);
                    }
                }
                adapter.Refresh(tempList, 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializeAllComponents() {
        progressBar = (ProgressBar) contentView.findViewById(R.id.progress_bar_activityLog);
        paginateProgressBar = (ProgressBar) contentView.findViewById(R.id.paginate_activity_log);
        emptyText = (TextView) contentView.findViewById(R.id.activity_log_empty);
        loginText = (TextView) contentView.findViewById(R.id.activity_login);
        login = (Button) contentView.findViewById(R.id.activity_login_click);
        recyclerView = (RecyclerView) contentView.findViewById(R.id.activity_recycler);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setFocusable(false);

        loginText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        login.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

    }

    public class ActivityAdapter extends RecyclerView.Adapter {
        private final int ITEM_VIEW_TYPE_BASIC = 0;
        private final int ITEM_VIEW_TYPE_FOOTER = 1;
        Context context;
        List<ActivityLogList> model;
        Boolean[] likeArray;


        public ActivityAdapter(FragmentActivity activity, List<ActivityLogList> model) {
            context = activity;
            this.model = model;
            likeArray = new Boolean[this.model.size()];
            for (int i = 0; i < this.model.size(); i++) {
                if (model.get(i).is_liked())
                    likeArray[i] = true;
                else likeArray[i] = false;
                //Log.e("length","####"+i+likeArray[i]);
            }
        }

        public void Refresh(List<ActivityLogList> arrayList, int j) {
            model = arrayList;
            likeArray = new Boolean[this.model.size()];
            for (int i = 0; i < this.model.size(); i++) {
                if (model.get(i).is_liked())
                    likeArray[i] = true;
                else likeArray[i] = false;
                //Log.e("length","####"+i+likeArray[i]);
            }
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;

            if (viewType == ITEM_VIEW_TYPE_BASIC) {

                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_log_status_item, null);
                vh = new ActivityListHolder(layoutView);
            } else {
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_log_item, null);
                vh = new ActivityStatusListHolder(layoutView);
            }
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ActivityLogList data = (ActivityLogList) model.get(position);

            if (holder instanceof ActivityListHolder) {
                final ActivityListHolder activityListHolder = (ActivityListHolder) holder;
                activityListHolder.mType.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_SEMIBOLD));
                activityListHolder.mStatus.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
                activityListHolder.mLevelName.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
//                activityListHolder.mText.setTypeface(PlaythoraUtility.getFont(context,RALEWAY_REGULAR));
                activityListHolder.timeago.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

                try {
                    activityListHolder.mType.setText(data.getTitle());
                    activityListHolder.mStatus.setText(data.getContent());
                    activityListHolder.mLevelName.setText(data.getName());
                    activityListHolder.mCmd.setText(data.getComment_count() + "");
                    activityListHolder.mLikes.setText(data.getLike_count() + "");
                    activityListHolder.timeago.setText(data.getDate());

                    try {
                        if (data.getType().equalsIgnoreCase("player")
                                || data.getType().equalsIgnoreCase("social_connect"))
                            Glide.with(context).load(data.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(activityListHolder.mImageView);
                        else if (data.getType().equalsIgnoreCase("game"))
                            Glide.with(context).load(data.getCover_pic()).error(R.drawable.game_null).centerCrop().into(activityListHolder.mImageView);
                        else
                            Glide.with(context).load(data.getCover_pic()).error(R.drawable.error_center_x).centerCrop().into(activityListHolder.mImageView);
                    } catch (Exception e) {
                        if (data.getType().equalsIgnoreCase("player")
                                || data.getType().equalsIgnoreCase("social_connect"))
                            activityListHolder.mImageView.setImageResource(R.drawable.circled_user);
                        else if (data.getType().equalsIgnoreCase("game"))
                            activityListHolder.mImageView.setImageResource(R.drawable.game_null);
                        else
                            activityListHolder.mImageView.setImageResource(R.drawable.error_center_x);
                    }

                    if (!likeArray[position]) {
                        activityListHolder.likeImg.setVisibility(View.VISIBLE);
                        activityListHolder.unlikeImg.setVisibility(View.INVISIBLE);
                    } else {
                        activityListHolder.likeImg.setVisibility(View.INVISIBLE);
                        activityListHolder.unlikeImg.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                    activityListHolder.mWriteCmd.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        //toActivityLogOne(true); //true, to show keyboard
//                        Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
//                        intent.putExtra("TYPE",data.getTitle());
//                        intent.putExtra("ID",data.getId());
//                        intent.putExtra("STATUS",data.getContent());
//                        intent.putExtra("LEVEL",data.getName());
//                        intent.putExtra("COMMENTS_COUNT",data.getComment_count());
//                        intent.putExtra("LIKES",String.valueOf(data.getLike_count()));
//                        intent.putExtra("IMAGE",data.getCover_pic());
//                        intent.putExtra("LIKEBYUSER",data.is_liked());
//                        intent.putExtra("KEYBOARD",true);
//                        intent.putExtra("POSITION",position);
//                        intent.putExtra("HASMOREPAGE",data.isHasMoreComments());
//                        startActivityForResult(intent,5);
//                    }
//                });

                activityListHolder.mComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //toActivityLogOne(true); //true, to show keyboard
                        Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
                        intent.putExtra("TYPE", data.getTitle());
                        intent.putExtra("DATA_TYPE", data.getType());
                        intent.putExtra("DATA_TYPE_ID", data.getType_id());
                        intent.putExtra("ID", data.getId());
                        intent.putExtra("STATUS", data.getContent());
                        intent.putExtra("LEVEL", data.getName());
                        intent.putExtra("COMMENTS_COUNT", data.getComment_count());
                        intent.putExtra("LIKES", String.valueOf(data.getLike_count()));
                        intent.putExtra("IMAGE", data.getCover_pic());
                        intent.putExtra("LIKEBYUSER", data.is_liked());
                        intent.putExtra("KEYBOARD", true);
                        intent.putExtra("POSITION", position);
                        intent.putExtra("HASMOREPAGE", data.isHasMoreComments());
                        startActivityForResult(intent, 5);
                    }
                });

                /*activityListHolder.profileRelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //toActivityLogOne(false); //false, to hide keyboard
                        Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
                        intent.putExtra("TYPE",data.getTitle());
                        intent.putExtra("ID",data.getId());
                        intent.putExtra("STATUS",data.getContent());
                        intent.putExtra("LEVEL",data.getName());
                        intent.putExtra("COMMENTS_COUNT",data.getComment_count());
                        intent.putExtra("LIKES",String.valueOf(data.getLike_count()));
                        intent.putExtra("IMAGE",data.getCover_pic());
                        intent.putExtra("LIKEBYUSER",data.is_liked());
                        intent.putExtra("KEYBOARD",false);
                        intent.putExtra("POSITION",position);
                        intent.putExtra("HASMOREPAGE",data.isHasMoreComments());
                        startActivityForResult(intent,5);
                    }
                });
*/
                activityListHolder.noOfLikes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
                            if (!likeArray[position]) {
                                likeActivity(model.get(position).getId(), true, position);
                                int i = model.get(position).getLike_count();
                                i += 1;
                                model.get(position).setLike_count(i);
                                model.get(position).setIs_liked(true);
                                activityListHolder.mLikes.setText(model.get(position).getLike_count() + "");
                                activityListHolder.likeImg.setVisibility(View.INVISIBLE);
                                activityListHolder.unlikeImg.setVisibility(View.VISIBLE);
                                likeArray[position] = true;
                            } else {
                                likeActivity(model.get(position).getId(), false, position);
                                int j = model.get(position).getLike_count();
                                j -= 1;
                                model.get(position).setLike_count(j);
                                model.get(position).setIs_liked(false);
                                activityListHolder.mLikes.setText(model.get(position).getLike_count() + "");
                                activityListHolder.likeImg.setVisibility(View.VISIBLE);
                                activityListHolder.unlikeImg.setVisibility(View.INVISIBLE);
                                likeArray[position] = false;
                            }
                        }
//                        else
//                            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
                    }
                });

                activityListHolder.mImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (data.getType().equalsIgnoreCase("game")) {
                            Intent i = new Intent(getActivity(), GameMain.class);
                            i.putExtra("GAME_ID", String.valueOf(data.getType_id()));
                            startActivity(i);
                        } else {
                            Intent i = new Intent(getActivity(), PlayerProfileViewActivity.class);
                            i.putExtra("Id", data.getType_id() + "");
                            i.putExtra("SEARCH", "");
                            getActivity().startActivity(i);
                        }

                    }
                });


            } else {
                final ActivityStatusListHolder activityStatusListHolder = (ActivityStatusListHolder) holder;
                activityStatusListHolder.mType.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_SEMIBOLD));
                activityStatusListHolder.mStatus.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
//                activityStatusListHolder.mText.setTypeface(PlaythoraUtility.getFont(context,RALEWAY_REGULAR));
                activityStatusListHolder.timeago.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

                try {
                    activityStatusListHolder.mType.setText(data.getTitle());
                    activityStatusListHolder.mStatus.setText(data.getContent());
                    activityStatusListHolder.mCmd.setText(data.getComment_count() + "");
                    activityStatusListHolder.mLikes.setText(data.getLike_count() + "");
                    activityStatusListHolder.timeago.setText(data.getDate());

                    try {
                        Glide.with(context).load(data.getCover_pic()).error(R.drawable.no_team).centerCrop().into(activityStatusListHolder.mImageView);
                    } catch (Exception e) {
                        activityStatusListHolder.mImageView.setImageResource(R.drawable.no_team);
                    }

                    if (!likeArray[position]) {
                        activityStatusListHolder.likeImg.setVisibility(View.VISIBLE);
                        activityStatusListHolder.unlikeImg.setVisibility(View.INVISIBLE);
                    } else {
                        activityStatusListHolder.likeImg.setVisibility(View.INVISIBLE);
                        activityStatusListHolder.unlikeImg.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

//                activityStatusListHolder.mWriteCmd.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        //toActivityLogTwo(true); //true, to show keyboard
//                        Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
//                        intent.putExtra("TYPE",data.getTitle());
//                        intent.putExtra("ID",data.getId());
//                        intent.putExtra("STATUS",data.getContent());
//                        intent.putExtra("COMMENTS_COUNT",data.getComment_count());
//                        intent.putExtra("LIKES",String.valueOf(data.getLike_count()));
//                        intent.putExtra("IMAGE",data.getCover_pic());
//                        intent.putExtra("LIKEBYUSER",data.is_liked());
//                        intent.putExtra("KEYBOARD",true);
//                        intent.putExtra("POSITION",position);
//                        intent.putExtra("HASMOREPAGE",data.isHasMoreComments());
//                        startActivityForResult(intent,5);
//                    }
//                });

                activityStatusListHolder.mComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* try {
                            comments = jsonObject.getJSONArray("success").getJSONObject(position).getJSONObject("comments").toString();
                            Log.e("JSONCOMMENT",comments);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        //toActivityLogTwo(true); //true, to show keyboard
                        Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
                        intent.putExtra("TYPE", data.getTitle());
                        intent.putExtra("ID", data.getId());
                        intent.putExtra("STATUS", data.getContent());
                        // intent.putExtra("COMMENTS",comments);
                        intent.putExtra("COMMENTS_COUNT", data.getComment_count());
                        intent.putExtra("LIKES", String.valueOf(data.getLike_count()));
                        intent.putExtra("IMAGE", data.getCover_pic());
                        intent.putExtra("LIKEBYUSER", data.is_liked());
                        intent.putExtra("KEYBOARD", true);
                        intent.putExtra("POSITION", position);
                        intent.putExtra("HASMOREPAGE", data.isHasMoreComments());
                        startActivityForResult(intent, 5);
                    }
                });

                /*activityStatusListHolder.profileRelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //toActivityLogTwo(false); //false, to hide keyboard
                        Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
                        intent.putExtra("TYPE",data.getTitle());
                        intent.putExtra("ID",data.getId());
                        intent.putExtra("STATUS",data.getContent());
                        intent.putExtra("COMMENTS_COUNT",data.getComment_count());
                        intent.putExtra("LIKES",String.valueOf(data.getLike_count()));
                        intent.putExtra("IMAGE",data.getCover_pic());
                        intent.putExtra("LIKEBYUSER",data.is_liked());
                        intent.putExtra("KEYBOARD",false);
                        intent.putExtra("POSITION",position);
                        intent.putExtra("HASMOREPAGE",data.isHasMoreComments());
                        startActivityForResult(intent,5);
                    }
                });
*/
                activityStatusListHolder.noOfLikes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
                            if (!likeArray[position]) {
                                likeActivity(data.getId(), true, position);
                                int i = data.getLike_count();
                                i = i + 1;
                                data.setLike_count(i);
                                data.setIs_liked(true);
                                activityStatusListHolder.mLikes.setText(data.getLike_count() + "");
                                activityStatusListHolder.likeImg.setVisibility(View.INVISIBLE);
                                activityStatusListHolder.unlikeImg.setVisibility(View.VISIBLE);
                                likeArray[position] = true;
                            } else {
                                likeActivity(data.getId(), false, position);
                                int j = data.getLike_count();
                                j -= 1;
                                data.setLike_count(j);
                                data.setIs_liked(false);
                                activityStatusListHolder.mLikes.setText(data.getLike_count() + "");
                                activityStatusListHolder.likeImg.setVisibility(View.VISIBLE);
                                activityStatusListHolder.unlikeImg.setVisibility(View.INVISIBLE);
                                likeArray[position] = false;
                            }
                        }
//                        else
//                            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
                    }
                });

                activityStatusListHolder.mImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (data.getType().equalsIgnoreCase("team")) {
                            Intent i = new Intent(getActivity(), OtherTeamViewActivity.class);
                            i.putExtra("ID", data.getType_id());
                            startActivity(i);

                        }
                    }
                });

            }
        }

        private void likeActivity(int id, final boolean like, int position) {
            String URL = like ? Config.ACTIVITY_LIKE : Config.ACTIVITY_UNLIKE;
            try {
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("id", String.valueOf(id));
                Log.e("PARAMMS", String.valueOf(params));
                Log.e("URL", URL);
                mHttpClient.post(URL, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);

                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                                        //if(like)
                                        //data.setIs_liked(false);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();
                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*private void toActivityLogTwo(boolean b) {
            Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
            intent.putExtra("TYPE",data.getType());
            intent.putExtra("STATUS",data.getStatus());
            intent.putExtra("COMMENTS",String.valueOf(data.getComments()));
            intent.putExtra("LIKES",String.valueOf(data.getLikes()));
            intent.putExtra("IMAGE",data.getImageurl());
            intent.putExtra("LIKEBYUSER",data.isSetIsLikedByLoggedInUser());
            intent.putExtra("KEYBOARD",b);
            startActivity(intent);
        }*/

       /* private void toActivityLogOne(boolean b) {
            Intent intent = new Intent(getActivity(), ActivityLogDetail.class);
            intent.putExtra("TYPE",data.getType());
            intent.putExtra("STATUS",data.getStatus());
            intent.putExtra("LEVEL",data.getLevelName());
            intent.putExtra("COMMENTS",String.valueOf(data.getComments()));
            intent.putExtra("LIKES",String.valueOf(data.getLikes()));
            intent.putExtra("IMAGE",data.getImageurl());
            intent.putExtra("LIKEBYUSER",data.isSetIsLikedByLoggedInUser());
            intent.putExtra("KEYBOARD",b);
            startActivity(intent);
        }*/

        @Override
        public int getItemCount() {
            return model.size();
        }

        @Override
        public int getItemViewType(int position) {
            return model.get(position).getType().equalsIgnoreCase("team") ? ITEM_VIEW_TYPE_FOOTER : ITEM_VIEW_TYPE_BASIC;
        }

        public class ActivityListHolder extends RecyclerView.ViewHolder {
            TextView mType, mStatus, mLevelName, mLikes, mCmd;
            RelativeLayout noOfLikes, mComment, profileRelative;
            CircleImageView mImageView;
            TextView timeago;
            ImageView likeImg;
            ImageView unlikeImg;

            public ActivityListHolder(View itemView) {
                super(itemView);
                mType = (TextView) itemView.findViewById(R.id.activity_status_name);
                mStatus = (TextView) itemView.findViewById(R.id.activity_status_text);
                mLevelName = (TextView) itemView.findViewById(R.id.tv_activity_status);
                mLikes = (TextView) itemView.findViewById(R.id.tv_activity_status_likes);
                mCmd = (TextView) itemView.findViewById(R.id.activity_status_cmd);
                mImageView = (CircleImageView) itemView.findViewById(R.id.activity_profile);
                noOfLikes = (RelativeLayout) itemView.findViewById(R.id.like_status_relative);
//                mText = (TextView) itemView.findViewById(R.id.status_cmd_text);
//                mWriteCmd = (RelativeLayout) itemView.findViewById(R.id.write_relative);
                mComment = (RelativeLayout) itemView.findViewById(R.id.activity_cmd_relative);
                profileRelative = (RelativeLayout) itemView.findViewById(R.id.relative_one);
                likeImg = (ImageView) itemView.findViewById(R.id.activity_status_like_button);
                unlikeImg = (ImageView) itemView.findViewById(R.id.activity_status_unlike_button);
                timeago = (TextView) itemView.findViewById(R.id.timeago);
            }
        }

        public class ActivityStatusListHolder extends RecyclerView.ViewHolder {
            TextView mType, mStatus, mLikes, mCmd;
            RelativeLayout noOfLikes, mComment, profileRelative;
            ImageView mImageView;
            TextView timeago;
            ImageView likeImg;
            ImageView unlikeImg;

            public ActivityStatusListHolder(View itemView) {
                super(itemView);
                mType = (TextView) itemView.findViewById(R.id.activity_name);
                mStatus = (TextView) itemView.findViewById(R.id.activity_status);
                mLikes = (TextView) itemView.findViewById(R.id.activity_like);
                mCmd = (TextView) itemView.findViewById(R.id.activity_cmd);
                mImageView = (ImageView) itemView.findViewById(R.id.activity_img);
                noOfLikes = (RelativeLayout) itemView.findViewById(R.id.like_relative);
//                mWriteCmd = (RelativeLayout) itemView.findViewById(R.id.relative_cmd);
//                mText = (TextView) itemView.findViewById(R.id.cmd_text);
                mComment = (RelativeLayout) itemView.findViewById(R.id.commend_relative);
                profileRelative = (RelativeLayout) itemView.findViewById(R.id.relative_two);
                likeImg = (ImageView) itemView.findViewById(R.id.activity_like_button);
                unlikeImg = (ImageView) itemView.findViewById(R.id.activity_unlike_button);
                timeago = (TextView) itemView.findViewById(R.id.timeago_two);
            }
        }

    }
}
