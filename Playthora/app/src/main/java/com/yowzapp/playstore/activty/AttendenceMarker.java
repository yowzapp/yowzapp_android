package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 23/11/16.
 */
public class AttendenceMarker extends BaseActivity {

    public static ArrayList<AddTeamMembersList> stringList;
    Toolbar toolbar;
    TextView toolBarTitle, attenceText;
    String response, gameId, maxMem;
    Gson mGson;
    PlayersAdapter playersAdapter;
    GridView gridView;
    Button editOrAdd, attendence;
    int j;
    PreferenceManager mPref;
    AddTeamMembersModel model;
    ProgressBar progressBar;
    List<Boolean> select;
    Toast toast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendence_marker_layout);
        mPref = PreferenceManager.instance(this);
        toolbar = (Toolbar) findViewById(R.id.attendenceMarkerToolBar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        InitializeAll();
        stringList = new ArrayList<>();
        select = new ArrayList<>();

        gameId = getIntent().getStringExtra("gameId");
        maxMem = getIntent().getStringExtra("MAX_MEMBER");

        Log.e("maxMem", maxMem + "");

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        try {
            populateTeamMembers();
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

        editOrAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add = new Intent(AttendenceMarker.this, AddPlayersActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("mylist", stringList);
                add.putExtras(bundle);
                startActivityForResult(add, 6);
            }
        });

        attendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmDialog();
            }
        });

    }

    private void populateAttendence(JSONObject jsonObject) {
        if (PlaythoraUtility.checkInternetConnection(AttendenceMarker.this)) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = null;
                se = new StringEntity(jsonObject.toString());
                Log.e("JSONOBJ", se.toString());
                httpClient.post(getApplicationContext(), Config.ATTENDENCE, se, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            String s = new String(responseBody);
                            Log.e("ATTENDENCE", s);

                            mGson = new Gson();
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 200) {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                              /*  Intent intent = new Intent();
                                //intent.putExtra("GAME_ID", gameId);
                                intent.putExtra("SUCCESS", gameId);
                                setResult(15);
                                finish();
                                */
                                Intent intent1 = new Intent(AttendenceMarker.this,GameMain.class);
                                intent1.putExtra("SUCCESS", gameId);
                                startActivity(intent1);
                                finish();
//
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            String s = new String(responseBody);
                            Log.e("ATTENDENCE_failure", s);
                            JSONObject object = new JSONObject(s);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        }
    }


    private void populateTeamMembers() throws Exception {

        if (PlaythoraUtility.checkInternetConnection(AttendenceMarker.this)) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("game_id", gameId);
            httpClient.post(Config.PLAYER_LIST, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("following", s);

                        mGson = new Gson();
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("success");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                stringList.add(mGson.fromJson(jsonArray.get(i).toString(), AddTeamMembersList.class));
                            }
                            if (gridView.getAdapter() == null) {
                                Log.e("SUCCEss", String.valueOf(stringList.size()));
                                playersAdapter = new PlayersAdapter(AttendenceMarker.this, stringList);
                                gridView.setAdapter(playersAdapter);
                                // adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void InitializeAll() {

        toolBarTitle = (TextView) findViewById(R.id.attendence_toolbar_title);
        gridView = (GridView) findViewById(R.id.grid_view);
        editOrAdd = (Button) findViewById(R.id.editOrAddPlayer);
        attendence = (Button) findViewById(R.id.btn_attend);
        attenceText = (TextView) findViewById(R.id.attendenceText);
        progressBar = (ProgressBar) findViewById(R.id.player_bar);

        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        editOrAdd.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        attendence.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        attenceText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        if (requestCode == 6) {
            stringList = new ArrayList<>();
            Bundle bundle = data.getExtras();
            stringList = bundle.getParcelableArrayList("mylist");
            if (playersAdapter != null) {
                playersAdapter.refresh(stringList);
            }


        } else {
            finish();
        }

    }

    private void showConfirmDialog() {
        final Dialog dialog = new Dialog(AttendenceMarker.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.ask_login_dialog);
        TextView dialogStatus = (TextView) dialog.findViewById(R.id.login_confirm);
        TextView dialogCancel = (TextView) dialog.findViewById(R.id.cancel_login);
        TextView dialogoK = (TextView) dialog.findViewById(R.id.tv_login);

        dialogStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        dialogCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        dialogoK.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        dialogStatus.setText("Confirm the attendance?");
        dialogoK.setText("YES");
        dialogCancel.setText("NO");

        dialogoK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attendanceConfirm();
                dialog.dismiss();
            }
        });
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void attendanceConfirm() {
        try {
            JSONArray jsonArray = new JSONArray();
            Log.e("SELECT", select.size() + "");
            Log.e("stringList", stringList.size() + "");
            for (int i = 0; i < select.size(); i++) {
                if (select.get(i)) {
                    jsonArray.put(stringList.get(i).getId());
                }
            }
            if (jsonArray.length() > 0) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("game_id", gameId);
                    jsonObject.put("members", jsonArray);

                    Log.e("JSONOBJ", jsonObject.toString());
                    populateAttendence(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(getApplicationContext(), "Select atleast one member", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class PlayersAdapter extends BaseAdapter {
        ArrayList<AddTeamMembersList> arrayList;
        int count = 0;
        private LayoutInflater inflater = null;
        private Context context;

        public PlayersAdapter(Context context, ArrayList<AddTeamMembersList> userslist) {
            this.context = context;
            this.arrayList = userslist;
            j = userslist.size();

            select = new ArrayList<>();

            for (int i = 0; i < j; i++) {
                select.add(false);
            }
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public void refresh(ArrayList<AddTeamMembersList> usersArrayList) {
            this.arrayList = usersArrayList;
            select = new ArrayList<>();
            attendence.setText("ATTENDED");
            count = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                select.add(false);
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view;

            final Holder holder = new Holder();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item_layout, null);

            holder.circleImageView = (CircleImageView) view.findViewById(R.id.members_image);
            holder.name = (TextView) view.findViewById(R.id.members_name);

            try {
                Glide.with(context).load(arrayList.get(position).getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(holder.circleImageView);
            } catch (Exception e) {
                holder.circleImageView.setImageResource(R.drawable.circled_user);
            }
            Log.e("player_name",arrayList.get(position).getName()+" name");
            holder.name.setText(arrayList.get(position).getName());
            holder.name.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

            if (select.get(position)) {
                holder.circleImageView.setImageResource(R.drawable.attendace_marker);
            } else {
                try {
                    Glide.with(context).load(arrayList.get(position).getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(holder.circleImageView);
                } catch (Exception e) {
                    holder.circleImageView.setImageResource(R.drawable.circled_user);
                }
            }


            holder.circleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (toast != null) {
                        toast.cancel();
                    }
                    if (!select.get(position)) {
                        Log.e("MAXXXXXXX", (Integer.parseInt(maxMem) - 1) + " " + maxMem + count);
                        if ((Integer.parseInt(maxMem) - 1) != count) {
                            select.set(position, true);
                            //    holder.circleImageView.setImageResource(R.drawable.attendace_marker);
                            count = count + 1;
                            attendence.setText(String.valueOf(count) + " " + "OF" + " " + String.valueOf(arrayList.size()) + " " + "ATTENDED");
                            try {
                                holder.circleImageView.setImageResource(R.drawable.attendace_marker);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            toast = Toast.makeText(getApplicationContext(), "Maximum limit mark attendence is reached", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                    } else {
                        select.set(position, false);
                        try {
                            Glide.with(context).load(arrayList.get(position).getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(holder.circleImageView);
                        } catch (Exception e) {
                            holder.circleImageView.setImageResource(R.drawable.circled_user);
                        }
                        count = count - 1;
                        attendence.setText(String.valueOf(count) + " " + "OF" + " " + String.valueOf(arrayList.size()) + " " + "ATTENDED");
                    }

                }
            });
            return view;
        }

        public class Holder {
            CircleImageView circleImageView;
            TextView name;
        }
    }
}
