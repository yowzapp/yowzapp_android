package com.yowzapp.playstore.activty;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.SportsProfileAdapter;
import com.yowzapp.playstore.adapter.TeamsProfileAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.TeamDetailSportsModel;
import com.yowzapp.playstore.model.TeamSearchModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

import static com.yowzapp.playstore.activty.FavouriteSportsActivity.sportslist;
import static com.yowzapp.playstore.adapter.LocationAdapter.location;
import static com.yowzapp.playstore.adapter.SportsProfileAdapter.sports;
import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class PlayerProfileEditActivity extends BaseActivity implements View.OnClickListener {
    private static final int PICK_FROM_CAM = 1;
    public TeamsProfileAdapter recyclerAdapter;
    public List<String> day;
    public List<String> favSportsArray;
    Toolbar toolbar;
    RecyclerView recyclerViewfav, recyclerViewassoc;
    ImageView favSport, preferredLocation, profileImage, bannerImg;
    CircleImageView primaryCoverPic;
    Boolean mon = false, tue = false, wed = false, thu = false, fri = false, sat = false, sun = false, profile = false, banner = false;
    CircleImageView userImage;
    String selectedImagePath, profile_pic, cover_pic;
    SportsProfileAdapter sportAdapter;
    EditText playerName, playerBio;
    View nextsport, nextLocation;
    PreferenceManager mPref;
    int serverResponseCode = 0;
    String nameValue, bio, mobileNumber, primaryTeamResponse, favSportResponse, associatedTeams, pId, pName, pCoverPic, pMemCount, pFollowCount;
    StringEntity entity;
    Toast toast;
    TextView fromTime, toTime, cahangeCover, done, topgameSelected, favGame, favGameSelected, title, monday, tuesday, wednesday, thursday, friday, saturday, sunday, primaryname, primaryTeamMembers, members, primaryTeamFollowers, desc, change;
    String mfromValue, mToValue, tuefromVal, tueToValue, value = "false";
    RelativeLayout favSportsLayout, locationLayout;
    TextView preferLocationText;
    ArrayList<TeamDetailSportsModel> arrayList;
    JSONObject jsonMonday, jsonTue, jsonWed, jsonThu, jsonFri, jsonSat, jsonSun;
    String locations = "";
    String[] loc;
    RelativeLayout primLayout;
    TextView noPrimary, noSports, noTeams;
    String fTimeValue, tTimeValue, locValue;
    String daysSelected;
    private int PICK_IMAGE_REQUEST = 2;
    private NestedScrollView scrollview;

    TextView playerNumberText;
    ImageView editNumberImage;
    RelativeLayout mobileNumberView;
    private File output;

    public static int EDIT_NUMBER = 90;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_profile_edit);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));

        }
        toolbar = (Toolbar) findViewById(R.id.toolbar_profile_edit);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title = (TextView) findViewById(R.id.toolbar_title);
        done = (TextView) findViewById(R.id.profile_done);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        title.setText("Profile Edit");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if(toast!=null){
                    toast.cancel();
                }
                if(fromTime.getText().toString().contains("00:00") || toTime.getText().toString().contains("00:00")) {
                    toast = Toast.makeText(PlayerProfileEditActivity.this, "select time", Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    editUser();
                }*/

                editUser();


            }
        });

        InitializeViews();


        Intent intent = getIntent();
        primaryTeamResponse = intent.getStringExtra("primaryTeam");
        Log.e("primaryTeamResponse", primaryTeamResponse + "");

        if (primaryTeamResponse.isEmpty()) {
            primaryTeamResponse = "";
        } else {
            primaryTeamResponse = intent.getStringExtra("primaryTeam");
        }

        favSportResponse = intent.getStringExtra("favSports");
        Log.e("favSportResponse", favSportResponse + "");

        if (favSportResponse.isEmpty()) {
            favSportResponse = "";
        } else {
            favSportResponse = intent.getStringExtra("favSports");
        }

        associatedTeams = intent.getStringExtra("AssociateTeams");
        Log.e("associatedTeams", associatedTeams + "");

        if (associatedTeams.isEmpty()) {
            associatedTeams = "";
        } else {
            associatedTeams = intent.getStringExtra("AssociateTeams");
        }

        daysSelected = intent.getStringExtra("days");
        Log.e("daysSelected", daysSelected);


        fTimeValue = intent.getStringExtra("from");
        tTimeValue = intent.getStringExtra("to");

        Log.e("timeFrom", fTimeValue + "");
        Log.e("timeTo", tTimeValue + "");

        if (fTimeValue.equals("") || fTimeValue.equals("null")) {
            fromTime.setText("00:00");
        } else {
            fromTime.setText(fTimeValue);
        }

        if (tTimeValue.equals("") || tTimeValue.equals("null")) {
            toTime.setText("00:00");
        } else {
            toTime.setText(tTimeValue);
        }


        locations = intent.getStringExtra("location");
        Log.e("locValue", locations);

        if (!locations.isEmpty()) {
            preferLocationText.setText(locations);
        } else {
            preferLocationText.setText("Please select your location");
        }


        try {
            JSONArray jsonArray = new JSONArray(daysSelected);
            Log.e("jsonArray", jsonArray.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.get(i).equals("monday")) {
                    monday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    monday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back));
                    mon = true;
                    day.add("monday");
                }
                if (jsonArray.get(i).equals("tuesday")) {
                    tuesday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    tuesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_two));
                    tue = true;
                    day.add("tuesday");
                }
                if (jsonArray.get(i).equals("wednesday")) {
                    wednesday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    wednesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_three));
                    wed = true;
                    day.add("wednesday");
                }

                if (jsonArray.get(i).equals("thursday")) {
                    thursday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    thursday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_four));
                    thu = true;
                    day.add("thursday");
                }

                if (jsonArray.get(i).equals("friday")) {
                    friday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    friday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
                    fri = true;
                    day.add("friday");
                }

                if (jsonArray.get(i).equals("saturday")) {
                    saturday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    saturday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_six));
                    sat = true;
                    day.add("saturday");
                }

                if (jsonArray.get(i).equals("sunday")) {
                    sunday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    sunday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_seven));
                    sun = true;
                    day.add("sunday");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        populatesports();
        populateTeams();

        try {
            if (!primaryTeamResponse.isEmpty()) {

                JSONObject object = new JSONObject(primaryTeamResponse);
                pId = object.getInt("id") + "";
                pName = object.getString("name");
                pCoverPic = object.getString("cover_pic");
                pMemCount = object.getInt("members_count") + "";
                pFollowCount = object.getInt("followers_count") + "";

                Log.e("pId", pId);
                Log.e("pName", pName);
                Log.e("pCoverPic", pCoverPic);
                Log.e("pMemCount", pMemCount);
                Log.e("pFollowCount", pFollowCount);

                noPrimary.setVisibility(View.GONE);
                primLayout.setVisibility(View.VISIBLE);
                primaryname.setText(pName);
                primaryTeamMembers.setText(pMemCount);
                primaryTeamFollowers.setText(pFollowCount);

                try {
                    Glide.with(getApplicationContext()).load(pCoverPic).into(primaryCoverPic);
                } catch (Exception e) {
                    primaryCoverPic.setImageResource(R.drawable.circled_user);
                }
            } else {
                noPrimary.setVisibility(View.VISIBLE);
                primLayout.setVisibility(View.GONE);
                noPrimary.setText("No primary team");

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        playerName.setText(mPref.getName());
        playerBio.setText(mPref.getBio());
        playerNumberText.setText(mPref.getUserMobileNumber());

        int pos = playerName.length();
        Editable editable = playerName.getText();
        Selection.setSelection(editable, pos);

        int posi = playerBio.length();
        Editable editable1 = playerBio.getText();
        Selection.setSelection(editable1, posi);

        try {
            Glide.with(PlayerProfileEditActivity.this).load(mPref.getProfilePic()).error(R.drawable.circled_user).centerCrop().into(userImage);
        } catch (Exception e) {
            userImage.setImageResource(R.drawable.circled_user);
        }

        try {
            Glide.with(PlayerProfileEditActivity.this).load(mPref.getCoverPic()).error(R.drawable.playthora_doodle).centerCrop().into(bannerImg);
        } catch (Exception e) {
            bannerImg.setImageResource(R.drawable.playthora_doodle);
        }

        setupListeners();
    }

    private void editUser() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileEditActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

         /*   RequestParams params= new RequestParams();
            params.add("name",playerName.getText().toString());
            params.add("bio",playerBio.getText().toString());*/

            try {
                JSONObject mainObj = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONArray locaArray = new JSONArray(location);
                JSONObject object1 = new JSONObject();
                JSONArray daysArray = new JSONArray(day);
/*
                 if(daysArray.length()==0){
                     fTimeValue="00:00";
                     tTimeValue="00:00";
                 }*/

//                if (sportslist.isEmpty()) {
//                    Toast.makeText(PlayerProfileEditActivity.this, "Select your favourite sports", Toast.LENGTH_SHORT).show();
//                    PlaythoraUtility.hideProgressDialog();
//                    return;
//                } else
                if (playerName.getText().toString().isEmpty()) {
                    Toast.makeText(PlayerProfileEditActivity.this, "Username can't be empty", Toast.LENGTH_SHORT).show();
                    PlaythoraUtility.hideProgressDialog();
                    return;
                } else {
                    for (int i = 0; i < sportslist.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("id", sportslist.get(i).getId());
                        jsonArray.put(jsonObject);
                    }
                    Log.e("JSONARRAY", jsonArray.toString());

                    object1.put("days", daysArray);
                    object1.put("available_from", fTimeValue);
                    object1.put("available_to", tTimeValue);
                    /* for (int j=0;j<location.size();j++){
                        locaArray.put(location);
                     }*/

                    mainObj.put("name", playerName.getText().toString());
                    mainObj.put("bio", playerBio.getText().toString());
                    mainObj.put("team_id", pId);
                    mainObj.put("interests", jsonArray);
                    mainObj.put("availability", object1);
                    mainObj.put("prefer_location", locaArray);
                }
                Log.e("EDTJSONOBJ", mainObj.toString());
                entity = new StringEntity(mainObj.toString());


                httpClient.post(PlayerProfileEditActivity.this, Config.EDIT_PROFILE, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("editProfile", s);

                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statuscode") == 200) {

                                Intent intent = new Intent(PlayerProfileEditActivity.this, PlayerProfileViewActivity.class);
                                intent.putExtra("Id", mPref.getId());
                                intent.putExtra("admin", "true");
                                startActivity(intent);
                                finishAffinity();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                        }


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);

                            JSONObject object = new JSONObject(s);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onBackPressed() {
//        sports = new String[arrayList.size()];
        super.onBackPressed();
    }

    private void InitializeViews() {

        jsonMonday = new JSONObject();
        jsonTue = new JSONObject();
        jsonWed = new JSONObject();
        jsonThu = new JSONObject();
        jsonFri = new JSONObject();
        jsonSat = new JSONObject();
        jsonSun = new JSONObject();
        day = new ArrayList<>();
        favSportsArray = new ArrayList<>();
        mPref = PreferenceManager.instance(getApplicationContext());
        recyclerViewfav = (RecyclerView) findViewById(R.id.profile_fav_sports);
        recyclerViewassoc = (RecyclerView) findViewById(R.id.profile_associated_teams);
        recyclerViewfav.setHasFixedSize(true);
        recyclerViewassoc.setHasFixedSize(true);
        recyclerViewfav.setLayoutManager(new LinearLayoutManager(PlayerProfileEditActivity.this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewassoc.setLayoutManager(new LinearLayoutManager(PlayerProfileEditActivity.this, LinearLayoutManager.HORIZONTAL, false));
        monday = (TextView) findViewById(R.id.monday);
        tuesday = (TextView) findViewById(R.id.tuesday);
        wednesday = (TextView) findViewById(R.id.wednsday);
        thursday = (TextView) findViewById(R.id.thursday);
        friday = (TextView) findViewById(R.id.friday);
        saturday = (TextView) findViewById(R.id.saturday);
        sunday = (TextView) findViewById(R.id.sunday);
        toTime = (TextView) findViewById(R.id.to_time);
        cahangeCover = (TextView) findViewById(R.id.cange_cover);
        fromTime = (TextView) findViewById(R.id.from_time);

        primaryname = (TextView) findViewById(R.id.search_name);
        primaryTeamMembers = (TextView) findViewById(R.id.primaryTeamMembers);
        members = (TextView) findViewById(R.id.typename);
        primaryTeamFollowers = (TextView) findViewById(R.id.primaryTeamFollowers);
        desc = (TextView) findViewById(R.id.description);
        change = (TextView) findViewById(R.id.change);
        preferredLocation = (ImageView) findViewById(R.id.all_loc);
        favSport = (ImageView) findViewById(R.id.all_sports);
        profileImage = (ImageView) findViewById(R.id.profile_image_change);
        scrollview = (NestedScrollView) findViewById(R.id.nested_profile);
        userImage = (CircleImageView) findViewById(R.id.imageView_player_edit);
        bannerImg = (ImageView) findViewById(R.id.plyer_banner);
        preferLocationText = (TextView) findViewById(R.id.prefered_location_text_value);
        nextsport = findViewById(R.id.image_next_sport);
        nextLocation = findViewById(R.id.image_next_location);
        primaryCoverPic = (CircleImageView) findViewById(R.id.imageView_player_search);
        primLayout = (RelativeLayout) findViewById(R.id.primaryLayout);
        noPrimary = (TextView) findViewById(R.id.noPrimaryTeam);
        noSports = (TextView) findViewById(R.id.noSports);
        noTeams = (TextView) findViewById(R.id.noTeams);
        favSportsLayout = (RelativeLayout) findViewById(R.id.categoriesOFSports);
        locationLayout = (RelativeLayout) findViewById(R.id.prefered_location);
        playerNumberText = (TextView) findViewById(R.id.mobile_number);
        editNumberImage = (ImageView) findViewById(R.id.edit_number);

        TextView primarytext = (TextView) findViewById(R.id.primary_text);
        TextView availabilitytext = (TextView) findViewById(R.id.avilabilty_text);
        TextView sportstext = (TextView) findViewById(R.id.sports_text);
        TextView associatedtext = (TextView) findViewById(R.id.associated_text);
        TextView preferedText = (TextView) findViewById(R.id.prefered_location_text);
        TextView fromtext = (TextView) findViewById(R.id.from_text);
        TextView totext = (TextView) findViewById(R.id.to_text);

        playerName = (EditText) findViewById(R.id.player_name);
        TextInputLayout playerNameLayout = (TextInputLayout) findViewById(R.id.input_layout_name);
        playerBio = (EditText) findViewById(R.id.player_bio);
        TextInputLayout playerBioLayout = (TextInputLayout) findViewById(R.id.input_layout_bio);

        mobileNumberView = (RelativeLayout) findViewById(R.id.mobile_number_view);

        playerName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        playerBio.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        playerBioLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        preferedText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        primarytext.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        sportstext.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        availabilitytext.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        associatedtext.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        noPrimary.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noSports.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noTeams.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        cahangeCover.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        preferLocationText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        primaryname.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        primaryTeamMembers.setTypeface(Typeface.DEFAULT);
        primaryTeamFollowers.setTypeface(Typeface.DEFAULT);
        desc.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        members.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        change.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        //fromTime.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        //toTime.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        fromtext.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        totext.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


    }


    private void setupListeners() {


        monday.setOnClickListener(this);
        tuesday.setOnClickListener(this);
        thursday.setOnClickListener(this);
        friday.setOnClickListener(this);
        saturday.setOnClickListener(this);
        sunday.setOnClickListener(this);
        wednesday.setOnClickListener(this);
        preferredLocation.setOnClickListener(this);
        // favSport.setOnClickListener(this);
        fromTime.setOnClickListener(this);
        toTime.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        cahangeCover.setOnClickListener(this);
        nextsport.setOnClickListener(this);
        // nextLocation.setOnClickListener(this);
        change.setOnClickListener(this);
        favSportsLayout.setOnClickListener(this);
        noSports.setOnClickListener(this);
        locationLayout.setOnClickListener(this);
        editNumberImage.setOnClickListener(this);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                scrollview.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                        if (scrollY > oldScrollY) {
                            title.setVisibility(View.VISIBLE);
                            toolbar.setBackground(getResources().getDrawable(R.drawable.whitw_back));
                            toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
                            //done.setImageResource(R.drawable.ic_user_selection_done);
                        }

                        if (scrollY == 0) {
                            title.setVisibility(View.GONE);
                            toolbar.setBackgroundColor(Color.parseColor("#00000000"));
                            toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
                            //done.setImageResource(R.drawable.ic_tick_profile_white);
                            done.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void populateTeams() {
        Gson gson = new Gson();
        try {
            if (!associatedTeams.isEmpty()) {
                JSONArray jsonArray = new JSONArray(associatedTeams);
                ArrayList<TeamSearchModel> arrayList = new ArrayList<TeamSearchModel>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    arrayList.add(gson.fromJson(jsonArray.get(i).toString(), TeamSearchModel.class));
                    TeamsProfileAdapter teamAdapter = new TeamsProfileAdapter(PlayerProfileEditActivity.this, arrayList);
                    recyclerViewassoc.setAdapter(teamAdapter);
                }
            } else {
                noTeams.setVisibility(View.VISIBLE);
                noTeams.setText("No associated teams");
            }

        } catch (JSONException e) {
            Toast.makeText(PlayerProfileEditActivity.this, "Error fetching Associated Teams", Toast.LENGTH_LONG).show();
            noTeams.setVisibility(View.VISIBLE);
            noTeams.setText("No associated teams");
        }

    }

    private void populatesports() {
        Gson gson = new Gson();
        try {
            sportslist = new ArrayList<>();
            if (!favSportResponse.isEmpty()) {
                JSONArray jsonArray = new JSONArray(favSportResponse);
                for (int i = 0; i < jsonArray.length(); i++) {
                    sportslist.add(gson.fromJson(jsonArray.get(i).toString(), TeamDetailSportsModel.class));
                }

                Log.e("sportslist", sportslist + "");
                sportAdapter = new SportsProfileAdapter(PlayerProfileEditActivity.this, sportslist);
                recyclerViewfav.setAdapter(sportAdapter);

            } else {
                noSports.setVisibility(View.VISIBLE);
                noSports.setText("Favourite sports list is empty");
            }

        } catch (JSONException e) {
            e.printStackTrace();
            noSports.setVisibility(View.VISIBLE);
            noSports.setText("Favourite sports list is empty");
        }
    }

    @Override
    public void onClick(View v) {

        if (toast != null) {
            toast.cancel();
        }
        switch (v.getId()) {
            case R.id.monday:
                if (mon) {
                    monday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    monday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back));
                    mon = false;
                    day.remove("monday");

                } else {
                    monday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    monday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back));
                    mon = true;
                    day.add("monday");

                }

                break;

            case R.id.tuesday:
                if (tue) {
                    tuesday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    tuesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_bak_two));
                    tue = false;
                    day.remove("tuesday");
                } else {
                    tuesday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    tuesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_two));
                    tue = true;
                    day.add("tuesday");

                }

                break;
            case R.id.wednsday:
                if (wed) {
                    wednesday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    wednesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_bak_three));
                    wed = false;
                    day.remove("wednesday");

                } else {

                    wednesday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    wednesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_three));
                    wed = true;
                    day.add("wednesday");
                }


                break;
            case R.id.thursday:

                if (thu) {

                    thursday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    thursday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_four));
                    thu = false;
                    day.remove("thursday");

                } else {
                    thursday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    thursday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_four));
                    thu = true;
                    day.add("thursday");
                }

                break;
            case R.id.friday:
                if (fri) {
                    friday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    friday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_five));
                    fri = false;
                    day.remove("friday");
                } else {
                    friday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    friday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
                    fri = true;
                    day.add("friday");
                }


                break;

            case R.id.saturday:
                if (sat) {

                    saturday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    saturday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_six));
                    sat = false;
                    day.remove("saturday");
                } else {
                    saturday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    saturday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_six));
                    sat = true;
                    day.add("saturday");
                }


                break;

            case R.id.sunday:
                if (sun) {

                    sunday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.indicator_page_color));
                    sunday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_seven));
                    sun = false;
                    day.remove("sunday");
                } else {
                    sunday.setTextColor(ContextCompat.getColor(PlayerProfileEditActivity.this, R.color.white));
                    sunday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_seven));
                    sun = true;
                    day.add("sunday");
                }


                break;

            case R.id.cange_cover:
                banner = true;
                profile = false;

                if (shouldAskPermission()) {
                    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                    int permsRequestCode = 200;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    UploadImage();
                }

                break;
            case R.id.profile_image_change:
                profile = true;
                banner = false;

                if (shouldAskPermission()) {
                    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                    int permsRequestCode = 200;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    UploadImage();
                }

                break;
            case R.id.all_sports:

               /* Intent i = new Intent(PlayerProfileEditActivity.this,FavouriteSportsActivity.class);
                startActivityForResult(i,1);*/

                break;

            case R.id.categoriesOFSports:
            case R.id.noSports:
            case R.id.image_next_sport:
                Intent next = new Intent(PlayerProfileEditActivity.this, FavouriteSportsActivity.class);
                next.putExtra("SPORTSLIST", sports);
                startActivityForResult(next, 3);
                break;

            case R.id.prefered_location:
                Intent j = new Intent(PlayerProfileEditActivity.this, PrefferedLocationActivity.class);
                startActivityForResult(j, 2);
                break;

            case R.id.from_time:
                showTime("From");
                /*if(mon || tue || wed || thu || fri || sat || sun){
                    showTime("From");
                }else {
                    toast = Toast.makeText(PlayerProfileEditActivity.this,"select day to choose slot",Toast.LENGTH_SHORT);
                    toast.show();
                }*/


                break;
            case R.id.to_time:
                showTime("To");
              /*  if(mon || tue || wed || thu || fri || sat || sun){
                    showTime("To");
                }else {
                    toast = Toast.makeText(PlayerProfileEditActivity.this,"select day to choose slot",Toast.LENGTH_SHORT);
                    toast.show();
                }*/


                break;

            case R.id.change:
                Intent myTeams = new Intent(PlayerProfileEditActivity.this, MyTeamsActivity.class);
                myTeams.putExtra("primaryID", pId);
                myTeams.putExtra("pName", pName);
                myTeams.putExtra("pCoverPic", pCoverPic);
                myTeams.putExtra("pMemCount", pMemCount);
                myTeams.putExtra("pFollowCount", pFollowCount);
                startActivityForResult(myTeams, 6);

                break;

            case R.id.edit_number:
//                mobileNumberView.setVisibility(View.INVISIBLE);
//                mobileNumberEdit.setVisibility(View.VISIBLE);
//                playerNumberEdit.requestFocus();
//                playerNumberEdit.selectAll();
//                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(playerNumberEdit, InputMethodManager.SHOW_IMPLICIT);
                Intent changeNumber = new Intent(PlayerProfileEditActivity.this, NumberActivity.class);
                changeNumber.putExtra("numberEditing", true);
                startActivityForResult(changeNumber, EDIT_NUMBER);
                break;

            default:
                break;

        }
    }

    private void callSaveNumber() {
    }

    private boolean shouldAskPermission() {

        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    private void showTime(final String timeStr) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(PlayerProfileEditActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                //setTime.setText(selectedHour + " : " + selectedMinute);
                int selectHour = selectedHour;
                int selectMinute = selectedMinute;
                String am_pm;
                if (selectHour >= 12)         //hourofDay =13
                {
                    int hour = selectHour - 12;
                    if (hour == 0) {
                        hour = 12;
                    }//hour=1
                    am_pm = "PM";
                    if (selectedMinute >= 10) {
                        // pickTimeAppointment.setText(""+hour+":"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {
                            tTimeValue = "" + hour + ":" + selectedMinute + am_pm;
                            toTime.setText(tTimeValue);

                        } else {
                            fTimeValue = "" + hour + ":" + selectedMinute + am_pm;
                            fromTime.setText(fTimeValue);

                        }

                    } else {
                        //  pickTimeAppointment.setText(""+hour+":0"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {
                            tTimeValue = "" + hour + ":0" + selectedMinute + am_pm;
                            toTime.setText(tTimeValue);

                        } else {
                            fTimeValue = "" + hour + ":0" + selectedMinute + am_pm;
                            fromTime.setText(fTimeValue);

                        }
                    }
                    //  setTime.setText(""+hour+":"+selectedMinute+am_pm);//PM
                } else {
                    int hour = selectHour;
                    if (hour == 0) {
                        hour = 12;
                    }
                    am_pm = "AM";
                    if (selectedMinute >= 10) {
                        // pickTimeAppointment.setText(""+hour+":"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {
                            tTimeValue = "" + hour + ":" + selectedMinute + am_pm;
                            toTime.setText(tTimeValue);
                        } else {
                            fTimeValue = "" + hour + ":" + selectedMinute + am_pm;
                            fromTime.setText(fTimeValue);
                        }
                    } else {
                        //  pickTimeAppointment.setText(""+hour+":0"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {
                            tTimeValue = "" + hour + ":0" + selectedMinute + am_pm;
                            toTime.setText(tTimeValue);
                        } else {
                            fTimeValue = "" + hour + ":0" + selectedMinute + am_pm;
                            fromTime.setText(fTimeValue);
                        }
                    }
                }

            }
        }, 12, 0, false);
        mTimePicker.setTitle("Select Time");

        mTimePicker.show();
    }

    public void UploadImage() {


        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PlayerProfileEditActivity.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(PlayerProfileEditActivity.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(PlayerProfileEditActivity.this,
                                    getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (data == null) {
//            return;
//        }
        if (requestCode == 3) {
            if (sportAdapter == null) {
                sportAdapter = new SportsProfileAdapter(PlayerProfileEditActivity.this, sportslist);
                recyclerViewfav.setAdapter(sportAdapter);
            }
            if (!sportslist.isEmpty()) {
                sportAdapter.refresh(sportslist);
                noSports.setVisibility(View.GONE);
                recyclerViewfav.setVisibility(View.VISIBLE);
            } else {
                sports = new String[0];
                recyclerViewfav.setVisibility(View.GONE);
                noSports.setVisibility(View.VISIBLE);
                noSports.setText("Favourite sports list is empty");
            }
        }
        if (requestCode == 2) {
            if (location != null) {
                if (location.size() != 0) {
                    for (int i = 0; i < location.size(); i++) {
                        if (i == 0) {
                            locations = location.get(i);
                        } else {
                            locations = locations + ", " + location.get(i);
                        }
                    }
                    preferLocationText.setText(locations);
                }
            }
        }


        if (requestCode == 6) {
            if (data != null) {
                pId = data.getStringExtra("primID");
                pName = data.getStringExtra("name");
                pCoverPic = data.getStringExtra("coverPic");
                pMemCount = data.getStringExtra("memCount");
                pFollowCount = data.getStringExtra("followerCount");

                Log.e("pIdvalue", pId + "");
                Log.e("pNamevalue", pName + "");
                Log.e("pCoverPicvalue", pCoverPic + "");
                Log.e("pMemCountvalue", pMemCount + "");
                Log.e("pFollowCountvalue", pFollowCount + "");


                primaryname.setText(pName);
                primaryTeamMembers.setText(pMemCount);
                primaryTeamFollowers.setText(pFollowCount);

                try {
                    Glide.with(getApplicationContext()).load(pCoverPic).error(R.drawable.circled_user).into(primaryCoverPic);
                } catch (Exception e) {
                    primaryCoverPic.setImageResource(R.drawable.circled_user);
                }


            }
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

                Uri selectedImageUri = data.getData();
                try {
                    selectedImagePath = getPath(selectedImageUri);
                } catch (NullPointerException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    PlaythoraUtility.showProgressDialog(PlayerProfileEditActivity.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (profile) {
                                UploadFile(selectedImagePath);
                                profile = true;
                                banner = false;
                            } else {
                                UploadFile(selectedImagePath);
                                profile = false;
                                banner = true;
                            }

                        }
                    }).start();

                 /*   if(profile) {
                        userImage.setImageBitmap(bm);
                    }else if(banner){
                        bannerImg.setImageBitmap(bm);
                    }
*/


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(), "Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), "Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {
                selectedImagePath = getRealPathFromURI(Uri.fromFile(output).toString(), PlayerProfileEditActivity.this);
                Log.e("selected Image", selectedImagePath);

                File myFile = new File(selectedImagePath);
                try {
                    myFile = new Compressor(this).compressToFile(myFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedImagePath = myFile.getAbsolutePath();

//                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
//                final Uri mImageCaptureUri = Uri.fromFile(destination);
//
//                System.out.println("Image Path : " + mImageCaptureUri);
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                selectedImagePath = getRealPathFromURI(mImageCaptureUri.toString(), PlayerProfileEditActivity.this);
//                try {
//                    ExifInterface exif = new ExifInterface(selectedImagePath);
//                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//                    Log.e("ROTATE", orientation + "");
//                    Bitmap bm;
//                    int rotation = 0;
//                    if (rotation == 0) {
//                        if (orientation == 6) {
//                            rotation = 90;
//                        }
//                        if (orientation == 3) {
//                            rotation = 180;
//                        }
//                        if (orientation == 8) {
//                            rotation = 270;
//                        }
//                        if (orientation == 4) {
//                            rotation = 180;
//                        }
//
//                    }
//                    Log.e("ooooooo", String.valueOf(currentapiVersion));
//                    Matrix matrix = new Matrix();
//                    matrix.postRotate(rotation);
//                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
//                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                try {
                    PlaythoraUtility.showProgressDialog(PlayerProfileEditActivity.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (profile) {
                                UploadFile(selectedImagePath);
                                profile = true;
                                banner = false;
                            } else {
                                UploadFile(selectedImagePath);
                                profile = false;
                                banner = true;
                            }

                        }
                    }).start();

                  /*  if(profile) {
                        userImage.setImageBitmap(bm);
                    }else if(banner){
                        bannerImg.setImageBitmap(bm);
                    }*/


//                } catch (IOException e) {
//                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            }
        }

        if (requestCode == EDIT_NUMBER) {
            Log.e("REACHED HERE", "request code 404");
            playerNumberText.setText(mPref.getUserMobileNumber());
        }
    }

    private void UploadFile(final String selectedImagePath) {

        String fileName = selectedImagePath;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(compressImage(selectedImagePath, PlayerProfileEditActivity.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :" + selectedImagePath);

            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "Source File not exist :" + selectedImagePath, Toast.LENGTH_SHORT).show();
                }
            });

            return;

        } else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.EDIT_PROFILE);
                trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                if (profile) {
                    Log.e("profile", "profilePic");
                    profile = true;
                    banner = false;
                    dos.writeBytes("Content-Disposition: form-data; name=\"profile_pic\";filename=\""
                            + fileName + "\"" + lineEnd); //profile_pic is a parameter

                } else {
                    Log.e("cover", "coverPic");
                    profile = false;
                    banner = true;
                    dos.writeBytes("Content-Disposition: form-data; name=\"cover_pic\";filename=\""
                            + fileName + "\"" + lineEnd); //profile_pic is a parameter
                }


                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS", "Content-Disposition: form-data; name=\"profile_pic\";filename=\"" + fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);
                final String serverError = serverResponseMessage;
                if (serverResponseCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                JSONObject object = new JSONObject(total.toString());
                                profile_pic = object.getJSONObject("success").getString("profile_pic");
                                cover_pic = object.getJSONObject("success").getString("cover_pic");
                                Log.e("profilePic", profile_pic + "!!!!!!");
                                Log.e("coverPic", cover_pic + "#######");
                                mPref.setProfilePic(profile_pic);
                                mPref.setCoverPic(cover_pic);
                                if (!mPref.getProfilePic().isEmpty()) {
                                    Glide.with(PlayerProfileEditActivity.this).load(mPref.getProfilePic()).centerCrop().into(userImage);
                                }
                                if (!mPref.getCoverPic().isEmpty()) {
                                    Glide.with(PlayerProfileEditActivity.this).load(mPref.getCoverPic()).centerCrop().into(bannerImg);
                                }

                                Toast.makeText(PlayerProfileEditActivity.this, "Upload image Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            Toast.makeText(PlayerProfileEditActivity.this, serverError + ": " + serverResponseCode, Toast.LENGTH_LONG).show();
                            // Glide.with(PlayerProfileEditActivity.this).load(mPref.getProfilePic()).centerCrop().into(userImage);

                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (ProtocolException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (IOException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        }
    }


    private String compressImage(String selectedImagePath, Context context) {

        String filePath = getRealPathFromURI(selectedImagePath, context);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
        options.inDither = false;
//      this options allow yowzapp to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Log.d("EXIF", "ratioX: " + ratioX);
        Log.d("EXIF", "ratioY: " + ratioY);
        Log.d("EXIF", "middleX: " + middleX);
        Log.d("EXIF", "middleY: " + middleY);
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.e("MMMMM", "re " + filename);
        return filename;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        Log.e("SIZE", "" + inSampleSize);
        return inSampleSize;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        Log.e("MMMMM", uriSting);
        return uriSting;

    }

    private void trustAllHosts() {

        // Create a trust manager that does not validate certificate chains

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                return new java.security.cert.X509Certificate[]{};

            }

            public void checkClientTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

            public void checkServerTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

        }};

        // Install the all-trusting trust manager

        try {

            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection

                    .setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeAccepted) {
                    UploadImage();
                }
                break;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
