package com.yowzapp.playstore.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.ChattingActivity;
import com.yowzapp.playstore.activty.CreateGameSelectLocation;
import com.yowzapp.playstore.activty.GameMain;
import com.yowzapp.playstore.activty.HomeScreenActivity;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.activty.NotificationActivity;
import com.yowzapp.playstore.activty.OtherTeamViewActivity;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;

import java.util.Map;

import static com.yowzapp.playstore.activty.HomeScreenActivity.notifyFlag;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    PreferenceManager mPref;
    int icon;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        mPref = PreferenceManager.instance(getApplicationContext());
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
        }


        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification: " + remoteMessage.getNotification());
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());

            if (remoteMessage.getData().containsKey("notification_type")) {

                Log.e("notifyFlag",notifyFlag+"");


                    sendNotification(remoteMessage.getNotification().getTitle(),
                            remoteMessage.getNotification().getBody(),
                            remoteMessage.getData());


            }
        }
    }

    private void sendNotification(String messageTitle, String messageBody, Map<String, String> data) {

        Log.e("FirebaseNotification", messageTitle);
        if (mPref.getAccessToken().isEmpty()) {
            Intent intent = new Intent(com.yowzapp.playstore.utils.MyFirebaseMessagingService.this, LoginActivity.class);
            startActivity(intent);
        } else {
            icon = R.mipmap.ic_launcher;

            if (data.containsKey("notification_type")) {
                Log.e("FirebaseData", data.toString());
                String notificationType = data.get("notification_type");
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                Intent intent = new Intent();

                try {
                    if (notificationType.equalsIgnoreCase("2")) {
                        intent = new Intent(this, ChattingActivity.class);

                        String convId = data.get("id");
                        Log.e("convId", convId);
                        String profileName = data.get("name");
                        Log.e("name", profileName);
                        String convType = data.get("type");
                        Log.e("type", convType);
                        String profilePic = data.get("profile_pic");
                        Log.e("profile_pic", profilePic);

                        String type = "";
                        if (convType.equalsIgnoreCase("1"))
                            type = "game";
                        else if (convType.equalsIgnoreCase("2"))
                            type = "team";
                        else if (convType.equalsIgnoreCase("3"))
                            type = "player";
                        Log.e("type", type);

                        intent.putExtra("name", profileName);
                        intent.putExtra("image", profilePic);
                        intent.putExtra("id", convId + "");
                        intent.putExtra("type", type);
                        intent.putExtra("Conv_id", Integer.parseInt(convId));
                        intent.putExtra("Conv_type", Integer.parseInt(convType));

                    } else {
                        if (notificationType.equalsIgnoreCase("4") ||
                                notificationType.equalsIgnoreCase("6")) {

                            intent = new Intent(this, GameMain.class);

                            String id = data.get("game_id");
                            Log.e("game_id", id);

                            intent.putExtra("GAME_ID", id);

                        } else if (notificationType.equalsIgnoreCase("7")) {

                            intent = new Intent(this, OtherTeamViewActivity.class);

                            int id = Integer.parseInt(data.get("team_id"));
                            Log.e("team_id", String.valueOf(id));

                            intent.putExtra("TEAM_ID", id);

                        } else if (notificationType.equalsIgnoreCase("8")) {

                            intent = new Intent(this, PlayerProfileViewActivity.class);

                            String id = data.get("user_id");
                            Log.e("user_id", id);

                            intent.putExtra("Id", id);
                        } else if (notificationType.equalsIgnoreCase("10")) {

                            intent = new Intent(this, CreateGameSelectLocation.class);

                            String id = data.get("sport_id");
                            Log.e("sport_id", String.valueOf(id));

                            intent.putExtra("sport_id", id);
                            intent.putExtra("ACTIVITY", "NOTIFIATION");

                        } else {
                            intent = new Intent(this, NotificationActivity.class);
                        }
//                        updateNotificationBellCount(getApplicationContext(), "GOT THE NOTIFICAITON RECEIVER");
                    }
                } catch (Exception e) {
                    intent = new Intent(this, NotificationActivity.class);
                }
                intent.putExtra("notification_type", notificationType);
                Log.e("notification_type", notificationType);

//                intent.setAction(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Adds the back stack
                stackBuilder.addParentStack(HomeScreenActivity.class);

                // Adds the Intent to the top of the stack
                stackBuilder.addNextIntent(intent);

                // Gets a PendingIntent containing the entire back stack
//                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(icon)
                        .setContentTitle(messageTitle)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

            } else {
                Intent intent = new Intent(this, NotificationActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                //intent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(icon)
                        .setContentTitle(messageTitle)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            }
        }
    }
}