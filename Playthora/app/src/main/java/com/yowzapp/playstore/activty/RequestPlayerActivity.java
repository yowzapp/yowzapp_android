package com.yowzapp.playstore.activty;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.SportsListModel;
import com.yowzapp.playstore.model.SportsSuccessList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

//public class RequestPlayerActivity extends BaseActivity implements  View.OnClickListener, AdapterView.OnItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
public class RequestPlayerActivity extends BaseActivity implements View.OnClickListener {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String API_KEY = "AIzaSyCPKwNMVuBU070GTZBWimbfI8hVUNM9ojY";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    public RecyclerViewAdapter recyclerAdapter;
    Toolbar toolbar;
    ProgressBar progressBar;
    PreferenceManager mPref;
    SportsListModel sportsListModel;
    Gson mGson;
    RecyclerView recyclerView;
    int j;
    String sportsId = "", gameName = "";
    TextView fromTime, toTime;
    String fTimeValue, tTimeValue;
    TextView requestText;
    //    AutoCompleteTextView autoCompView;
    Spinner autoCompView;
    Toast toast;
    String user_id;
    TextView dateText;
    Calendar calendar;
    int year, month, day;
    String toDate = "", dateToApi;
    com.yowzapp.playstore.model.LocationModel locationModel;
    ArrayAdapter<String> cityAdapter;
    String dateInString;
    Button requestForGame;
    private LocationListener listener;
    private double latitudevalue, curlatitudevalue;
    private double longitudevalue, curlongitudevalue;
    private LocationManager locationManager;
    private GoogleApiClient googleApiClient;
    private List<Address> addresses;
    private Geocoder geocoder;
    private String address = "", subLocality = "", city = "", country = "", state = "", LocationName = "";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_player);
        mPref = PreferenceManager.instance(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        geocoder = new Geocoder(this, Locale.ENGLISH);

        InitializeViews();
        setupListeners();

        Intent intent = getIntent();
        user_id = intent.getStringExtra("ID");

        Log.e("user_id", user_id);

        toolbar = (Toolbar) findViewById(R.id.favouriteSportsToolbar);
        TextView toolBarTitle = (TextView) findViewById(R.id.fav_sports_toolbar_title);

        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        toolBarTitle.setText("Request a game");

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        requestText.setText("Hey man, it’s been long since we played, wanna\n play " + gameName + " this weekend with the guys?");
        autoCompView = (Spinner) findViewById(R.id.location_text);
//        autoCompView.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        populateSports();
        loadCity();

//        autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
//        autoCompView.setOnItemClickListener(this);
//        autoCompView.setThreshold(0);

//        autoCompView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (autoCompView.getText().toString().trim().isEmpty())
//                    autoCompView.setText(" ");
//            }
//        });


//        listener = new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//
//                curlatitudevalue = location.getLatitude();
//                curlongitudevalue = location.getLongitude();
//                //Toast.makeText(getApplicationContext(), "" + latitudevalue + longitudevalue, Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onStatusChanged(String s, int i, Bundle bundle) {
//
//            }
//
//            @Override
//            public void onProviderEnabled(String s) {
//            }
//
//            @Override
//            public void onProviderDisabled(String s) {
//
//            }
//        };
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 1, listener);
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();


    }


//    @Override
//    protected void onResume() {
//        super.onResume();

//        if (!isLocationEnabled(getApplicationContext())) {
//            Log.e("TESTING1", "TESTING");
//            enableLoc();
//        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            checkLocationPermission();
//        }
//    }

//    private void enableLoc() {
//
//        if (googleApiClient == null) {
//            googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
//                    .addApi(LocationServices.API)
//                    .addScope(Drive.SCOPE_FILE)
//                    .addConnectionCallbacks(this)
//                    .addOnConnectionFailedListener(this).build();
//            googleApiClient.connect();
//
//            LocationRequest locationRequest = LocationRequest.create();
//            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//            locationRequest.setInterval(30 * 1000);
//            locationRequest.setFastestInterval(5 * 1000);
//            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                    .addLocationRequest(locationRequest);
//
//            //**************************
//            builder.setAlwaysShow(true); //this is the key ingredient
//            //**************************
//
//            PendingResult<LocationSettingsResult> result =
//                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
//            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//                @Override
//                public void onResult(LocationSettingsResult result) {
//                    final Status status = result.getStatus();
//                    final LocationSettingsStates state = result.getLocationSettingsStates();
//                    Log.e("STATUS", String.valueOf(status.getStatusCode()));
//                    switch (status.getStatusCode()) {
//                        case LocationSettingsStatusCodes.SUCCESS:
//                            // All location settings are satisfied. The client can initialize location
//                            // requests here.
//                            if (ActivityCompat.checkSelfPermission(RequestPlayerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RequestPlayerActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                                // TODO: Consider calling
//                                //    ActivityCompat#requestPermissions
//                                // here to request the missing permissions, and then overriding
//                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                //                                          int[] grantResults)
//                                // to handle the case where the user grants the permission. See the documentation
//                                // for ActivityCompat#requestPermissions for more details.
//                                return;
//                            }
//                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 1, listener);
//                            break;
//                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            // Location settings are not satisfied. But could be fixed by showing the user
//                            // a dialog.
//                            try {
//                                // Show the dialog by calling startResolutionForResult(),
//                                // and check the result in onActivityResult().
//                                status.startResolutionForResult(
//                                        RequestPlayerActivity.this, 1000);
//                            } catch (IntentSender.SendIntentException e) {
//                                // Ignore the error.
//                                Log.e("EEEEEE", e.toString());
//                            }
//                            break;
//                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            // Location settings are not satisfied. However, we have no way to fix the
//                            // settings so we won't show the dialog.
//                            break;
//                    }
//                }
//            });
//        }
//
//
//    }


//    @Override
//    public void onConnected(Bundle bundle) {
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//
//    }


//    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
//        String str = (String) adapterView.getItemAtPosition(position);
//        if (str.equalsIgnoreCase("Use current location")) {
//            //configure_button();
//
//            try {
//                addresses = geocoder.getFromLocation(curlatitudevalue, curlongitudevalue, 1);
//                Log.e("ADDRESS", String.valueOf(addresses));// Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            } catch (IOException e) {
//                e.printStackTrace();
////                autoCompView.setText("");
//                Toast.makeText(RequestPlayerActivity.this,
//                        "Please try again - Current loc",
//                        Toast.LENGTH_SHORT).show();
//            }
//
//            try {
//                if (addresses != null) {
//
//                   /* if (!(addresses.get(0).getAddressLine(0) == null))
//                        address = addresses.get(0).getAddressLine(0) + ", "; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                    else address = "";*/
//
//                    if (!(addresses.get(0).getSubLocality() == null))
//                        subLocality = addresses.get(0).getSubLocality() + ", ";
//                    else subLocality = "";
//
//                    if (!(addresses.get(0).getLocality() == null))
//                        city = addresses.get(0).getLocality() + ", ";
//                    else city = "";
//
//                    if (!(addresses.get(0).getAdminArea() == null))
//                        state = addresses.get(0).getAdminArea() + ", ";
//                    else state = "";
//
//                    /*if (!(addresses.get(0).getCountryName() == null))
//                        country = addresses.get(0).getCountryName();
//                    else country = "";*/
//
//                    /*if (!(addresses.get(0).getSubLocality() == null))
//                        subLocality = addresses.get(0).getSubLocality() + ", ";
//                    else subLocality = "";*/
//
////                    autoCompView.setText(subLocality + city + state);
//                    latitudevalue = curlatitudevalue;
//                    longitudevalue = curlongitudevalue;
//
//                    //String postalCode = addresses.get(0).getPostalCode();
//                    // String knownName = addresses.get(0).getFeatureName();
//                    Log.e("Adressss", " subLocality:" + subLocality + " city:" + city + "state: " + state);
//                    //Log.e("Adressss", addresses.get(0).getSubLocality() + " " + addresses.get(0).getSubAdminArea());
//                } else {
////                    autoCompView.setText("");
//                    Toast.makeText(RequestPlayerActivity.this,
//                            "Please try again - Current loc 2",
//                            Toast.LENGTH_LONG).show();
//                }
//            } catch (Exception e) {
////                autoCompView.setText("");
//                Toast.makeText(getApplicationContext(), "Couldn't fetch current location", Toast.LENGTH_SHORT).show();
//            }
//
//
//        } else {
//            try {
//                LatLng p1 = getLocationFromAddress(RequestPlayerActivity.this, str);
//                latitudevalue = p1.latitude;
//                longitudevalue = p1.longitude;
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(RequestPlayerActivity.this,
//                        "Please try again - Address loc",
//                        Toast.LENGTH_SHORT).show();
////                autoCompView.setText("");
//                latitudevalue = 0.0;
//                longitudevalue = 0.0;
//            }
//            //Toast.makeText(this, "Latitude" + latitudevalue + " Longitude" + longitudevalue, Toast.LENGTH_SHORT).show();
//        }
//
//    }

//    public LatLng getLocationFromAddress(Context context, String strAddress) {
//
//        Geocoder coder = new Geocoder(context);
//        List<Address> address;
//        LatLng p1 = null;
//
//        try {
//            address = coder.getFromLocationName(strAddress, 5);
//            if (address == null) {
//                return null;
//            }
//            Address location = address.get(0);
////            location.getLatitude();
////            location.getLongitude();
//
//            p1 = new LatLng(location.getLatitude(), location.getLongitude());
//
//        } catch (Exception ex) {
//
//            ex.printStackTrace();
//        }
//
//        return p1;
//    }

//    public boolean checkLocationPermission() {
//        Log.e("TESTING6", "TESTING");
//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.ACCESS_FINE_LOCATION)) {
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//                //  TODO: Prompt with explanation!
//
//                //Prompt the user once explanation has been shown
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                        MY_PERMISSIONS_REQUEST_LOCATION);
//
//            } else {
//                // No explanation needed, we can request the permission.
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                        MY_PERMISSIONS_REQUEST_LOCATION);
//            }
//            return false;
//        } else {
//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 1, listener);
//            return true;
//        }
//    }
//
//    public static boolean isLocationEnabled(Context context) {
//        int locationMode = 0;
//        String locationProviders;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            try {
//                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
//
//            } catch (Settings.SettingNotFoundException e) {
//                e.printStackTrace();
//            }
//            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
//
//        } else {
//            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//            return !TextUtils.isEmpty(locationProviders);
//        }
//
//
//    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    public Action getIndexApiAction() {
//        Thing object = new Thing.Builder()
//                .setName("RequestPlayer Page") // TODO: Define a title for the content shown.
//                // TODO: Make sure this auto-generated URL is correct.
//                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
//                .build();
//        return new Action.Builder(Action.TYPE_VIEW)
//                .setObject(object)
//                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
//                .build();
//    }

/*
    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
        client.connect();
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
*/


//    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
//        private ArrayList resultList;
//        private Context context;
//        private int layout;
//        String text = "";
//        ArrayList<String> queryResults;
//
//        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
//            super(context, textViewResourceId);
//            this.context = context;
//            layout = textViewResourceId;
//        }
//
//        @Override
//        public int getCount() {
//            try {
//                return resultList.size();
//            } catch (Exception e) {
//                return 1;
//            }
//        }
//
//        @Override
//        public String getItem(int index) {
//            return String.valueOf(resultList.get(index));
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            LayoutInflater inflater = (LayoutInflater) context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View rowView = inflater.inflate(layout, parent, false);
//
//            TextView suggestion = (TextView) rowView.findViewById(R.id.text_box);
//            suggestion.setText(getItem(position).toString());
//            suggestion.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
//            if (position == 0)
//                suggestion.setTextColor(getResources().getColor(R.color.login_color));
//
//            suggestion.setText(highlight(text, getItem(position), context));
//            return rowView;
//        }
//
//        public CharSequence highlight(String search, String originalText, Context context) {
//            // ignore case and accents
//            // the same thing should have been done for the search_activity_items text
//            String normalizedText = Normalizer
//                    .normalize(originalText, Normalizer.Form.NFD)
//                    .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
//                    .toLowerCase(Locale.ENGLISH);
//
//            int start = normalizedText.indexOf(search.toLowerCase(Locale.ENGLISH));
//            if (start < 0) {
//                // not found, nothing to to
//                return originalText;
//            } else {
//                // highlight each appearance in the original text
//                // while searching in normalized text
//                Spannable highlighted = new SpannableString(originalText);
//                while (start >= 0) {
//                    int spanStart = Math.min(start, originalText.length());
//                    int spanEnd = Math.min(start + search.length(),
//                            originalText.length());
//
//                    highlighted.setSpan(new StyleSpan(Typeface.BOLD),
//                            spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//
//                    start = normalizedText.indexOf(search, spanEnd);
//                }
//
//                return highlighted;
//            }
//        }
//
//        @Override
//        public Filter getFilter() {
//            Filter filter = new Filter() {
//                @Override
//                protected FilterResults performFiltering(CharSequence constraint) {
//                    FilterResults filterResults = new FilterResults();
//
//                    if (constraint != null) {
//                        // Retrieve the autocomplete results.
//                        text = constraint.toString();
//                        queryResults = autocomplete(constraint.toString());
//
//                        // Assign the data to the FilterResults
//                        filterResults.values = queryResults;
//                        filterResults.count = queryResults.size();
//                    }
//                    return filterResults;
//                }
//
//                @Override
//                protected void publishResults(CharSequence constraint, FilterResults results) {
//
//                    resultList = (ArrayList<String>) results.values;
//
//                    if (results != null && results.count > 0) {
//                        notifyDataSetChanged();
//                        progressBar.setVisibility(View.GONE);
//                    } else {
//                        notifyDataSetInvalidated();
//                        progressBar.setVisibility(View.GONE);
//                    }
//                }
//            };
//            return filter;
//        }
//    }


//    public static ArrayList autocomplete(String input) {
//        ArrayList resultList = null;
//
//        HttpURLConnection conn = null;
//        StringBuilder jsonResults = new StringBuilder();
//        try {
//            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
//            sb.append("?key=" + API_KEY);
//            sb.append("&types=geocode");
//            //sb.append("&types=(cities)");
//            //sb.append("&location=12.9545163,77.3500463");
//            //sb.append("&radius=50000");
//            //sb.append("&components=country:in");
//            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
//
//            URL url = new URL(sb.toString());
//            conn = (HttpURLConnection) url.openConnection();
//            InputStreamReader in = new InputStreamReader(conn.getInputStream());
//
//            // Load the results into a StringBuilder
//            int read;
//            char[] buff = new char[1024];
//            while ((read = in.read(buff)) != -1) {
//                jsonResults.append(buff, 0, read);
//            }
//        } catch (MalformedURLException e) {
//            Log.e(LOG_TAG, "Error processing Places API URL", e);
//            return resultList;
//        } catch (IOException e) {
//            Log.e(LOG_TAG, "Error connecting to Places API", e);
//            return resultList;
//        } finally {
//            if (conn != null) {
//                conn.disconnect();
//            }
//        }
//
//        try {
//
//            // Create a JSON object hierarchy from the results
//            JSONObject jsonObj = new JSONObject(jsonResults.toString());
//            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
//            Log.e("@@@@@@@@@@@@@@@@@", jsonResults.toString());
//            // Extract the Place descriptions from the results
//            resultList = new ArrayList(predsJsonArray.length());
//            resultList.add(0, "Use current location");
//            for (int i = 0; i < predsJsonArray.length(); i++) {
//                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
//                System.out.println("============================================================");
//                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
//                Log.e("ERROR", predsJsonArray.getJSONObject(i).getString("description"));
//            }
//        } catch (JSONException e) {
//            Log.e(LOG_TAG, "Cannot process JSON results", e);
//        }
//
//        return resultList;
//    }
    private void InitializeViews() {

        requestText = (TextView) findViewById(R.id.text_request);
        progressBar = (ProgressBar) findViewById(R.id.sports_progress_bar);
        recyclerView = (RecyclerView) findViewById(R.id.sports_recycler);
        toTime = (TextView) findViewById(R.id.to_time);
        fromTime = (TextView) findViewById(R.id.from_time);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        dateText = (TextView) findViewById(R.id.date);
        requestForGame = (Button) findViewById(R.id.requestForGame);
        requestText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        requestForGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    private void setupListeners() {
        fromTime.setOnClickListener(this);
        toTime.setOnClickListener(this);
        dateText.setOnClickListener(this);
        requestForGame.setOnClickListener(this);
    }

    private void populateSports() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            httpClient.get(Config.SPORTS_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("GameList", s);

                        sportsListModel = new SportsListModel();
                        mGson = new Gson();
                        sportsListModel = mGson.fromJson(s, SportsListModel.class);
                        Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));

                        if (sportsListModel.getSuccess().size() != 0) {
                            Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                            if (recyclerView.getAdapter() == null) {
                                Log.e("adapter", "adapter");
                                recyclerAdapter = new RecyclerViewAdapter(RequestPlayerActivity.this, sportsListModel.getSuccess(), sportsId);
                                recyclerView.setAdapter(recyclerAdapter);
                                recyclerAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 400 || object.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(RequestPlayerActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RequestPlayerActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(RequestPlayerActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(RequestPlayerActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(RequestPlayerActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private void loadCity() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(RequestPlayerActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            httpClient.get(Config.LOCATION_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("locationList", s);

                        locationModel = new com.yowzapp.playstore.model.LocationModel();
                        mGson = new Gson();
                        locationModel = mGson.fromJson(s, com.yowzapp.playstore.model.LocationModel.class);
                        if (locationModel.getSuccess().size() != 0) {
                            String[] cityArray = new String[locationModel.getSuccess().size() + 1];
                            cityArray[0] = "Where?";
                            for (int i = 1; i <= locationModel.getSuccess().size(); i++) {
                                cityArray[i] = locationModel.getSuccess().get(i - 1).getName();
                            }
                            cityAdapter = new ArrayAdapter<String>(RequestPlayerActivity.this, android.R.layout.simple_spinner_dropdown_item, cityArray);
                            cityAdapter.setNotifyOnChange(true);
                            cityAdapter.notifyDataSetChanged();
                            autoCompView.setAdapter(cityAdapter);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    @Override
    public void onClick(View v) {

        if (toast != null) {
            toast.cancel();
        }

        switch (v.getId()) {

            case R.id.from_time:
                if (!toDate.isEmpty()) {
                    showTime("From");
                } else {
                    toast = Toast.makeText(RequestPlayerActivity.this, "select date", Toast.LENGTH_SHORT);
                    toast.show();
                }


                break;
            case R.id.to_time:

                if (!toDate.isEmpty()) {
                    showTime("To");
                } else {
                    toast = Toast.makeText(RequestPlayerActivity.this, "select date", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;

            case R.id.date:
                showDate();
                break;

            case R.id.requestForGame:
                Log.e("REQUEST", "clicked");
                requestForGame();
                break;

            default:
                break;
        }
    }

    private void requestForGame() {
        Log.e("REACHED", "requestForGame()");
        if (toast != null)
            toast.cancel();

        if (sportsId.isEmpty()) {
            toast = Toast.makeText(RequestPlayerActivity.this, "Select type of game", Toast.LENGTH_SHORT);
            toast.show();
        } else if (dateText.getText().toString().isEmpty()) {
            toast = Toast.makeText(RequestPlayerActivity.this, "Please select date", Toast.LENGTH_SHORT);
            toast.show();
        } else if (fromTime.getText().toString().contains("00:00") || toTime.getText().toString().contains("00:00")) {
            toast = Toast.makeText(RequestPlayerActivity.this, "Please select time", Toast.LENGTH_SHORT);
            toast.show();
        }
//            else if (autoCompView.getText().toString().isEmpty() || autoCompView.getText().toString().equals(" ")) {
//                toast = Toast.makeText(RequestPlayerActivity.this, "please select the location", Toast.LENGTH_SHORT);
//                toast.show();
//            }
        else if (autoCompView.getSelectedItemPosition() == 0) {
            toast = Toast.makeText(RequestPlayerActivity.this, "Please select the location", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            request();
        }
           /* Intent intent=new Intent();
            setResult(3,intent);
            finish();*/
    }


    private void showDate() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE");
                Date date = new Date(year, monthOfYear, dayOfMonth - 1);
                String weekDay = simpledateformat.format(date);
                if ((monthOfYear + 1) < 10) {
                    toDate = dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                    dateToApi = year + "-" + "0" + (monthOfYear + 1) + "-" + dayOfMonth;

                } else {
                    toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    dateToApi = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                }
                dateText.setText(weekDay + ", " + toDate);
                //  update(toDate);

                String pattern = "EEE, dd/MM/yyyy";
                dateInString = new SimpleDateFormat(pattern).format(new Date());

                Log.e("$$$$$$$$$$$$$$$$$$", dateInString);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(RequestPlayerActivity.this, R.style.DialogTheme, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        dpDialog.show();
    }


    private void showTime(final String timeStr) {
        String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        Log.e("mydate", mydate);
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(RequestPlayerActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                //setTime.setText(selectedHour + " : " + selectedMinute);
                if (toast != null) {
                    toast.cancel();
                }
                Calendar datetime = Calendar.getInstance();
                Calendar c = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);


                int selectHour = selectedHour;
                int selectMinute = selectedMinute;
                String am_pm;
                if (selectHour >= 12)         //hourofDay =13
                {
                    int hour = selectHour - 12;
                    if (hour == 0) {
                        hour = 12;
                    }//hour=1
                    am_pm = "PM";
                    if (selectedMinute >= 10) {
                        // pickTimeAppointment.setText(""+hour+":"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {


                            //  it's after current
                            Log.e("current", "current");
                            tTimeValue = "" + hour + ":" + selectedMinute + " " + am_pm;
                            toTime.setText(tTimeValue);

                            // it's before current'
                            Log.e("before", "before");


                        } else {

                            if (dateInString.equals(toDate)) {
                                if (datetime.getTimeInMillis() > c.getTimeInMillis()) {
                                    fTimeValue = "" + hour + ":" + selectedMinute + " " + am_pm;
                                    fromTime.setText(fTimeValue);
                                    if (hour == 11)
                                        tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + "AM";
                                    else if (hour == 12)
                                        tTimeValue = "01" + ":" + selectedMinute + " " + am_pm;
                                    else
                                        tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + am_pm;
                                    toTime.setText(tTimeValue);
                                } else {
                                    fromTime.setText("00:00");
                                    toast = Toast.makeText(RequestPlayerActivity.this, "wrong time", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            } else {
                                fTimeValue = "" + hour + ":" + selectedMinute + " " + am_pm;
                                fromTime.setText(fTimeValue);
                                if (hour == 11)
                                    tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + "AM";
                                else if (hour == 12)
                                    tTimeValue = "01" + ":" + selectedMinute + " " + am_pm;
                                else
                                    tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + am_pm;
                                toTime.setText(tTimeValue);
                            }

                        }

                    } else {
                        //  pickTimeAppointment.setText(""+hour+":0"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {

                            tTimeValue = "" + hour + ":0" + selectedMinute + " " + am_pm;
                            toTime.setText(tTimeValue);


                        } else {

                            if (dateInString.equals(toDate)) {
                                if (datetime.getTimeInMillis() > c.getTimeInMillis()) {
                                    fTimeValue = "" + hour + ":0" + selectedMinute + " " + am_pm;
                                    fromTime.setText(fTimeValue);
                                    if (hour == 11)
                                        tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + "AM";
                                    else if (hour == 12)
                                        tTimeValue = "01" + ":0" + selectedMinute + " " + am_pm;
                                    else
                                        tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + am_pm;
                                    toTime.setText(tTimeValue);
                                } else {
                                    fromTime.setText("00:00");
                                    toast = Toast.makeText(RequestPlayerActivity.this, "wrong time", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            } else {
                                fTimeValue = "" + hour + ":0" + selectedMinute + " " + am_pm;
                                fromTime.setText(fTimeValue);
                                if (hour == 11)
                                    tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + "AM";
                                else if (hour == 12)
                                    tTimeValue = "01" + ":0" + selectedMinute + " " + am_pm;
                                else
                                    tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + am_pm;
                                toTime.setText(tTimeValue);
                            }

                        }
                    }
                    //  setTime.setText(""+hour+":"+selectedMinute+am_pm);//PM
                } else {
                    int hour = selectHour;
                    if (hour == 0) {
                        hour = 12;
                    }
                    am_pm = "AM";
                    if (selectedMinute >= 10) {
                        // pickTimeAppointment.setText(""+hour+":"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {
                            tTimeValue = "" + hour + ":" + selectedMinute + " " + am_pm;
                            toTime.setText(tTimeValue);

                        } else {

                            if (dateInString.equals(toDate)) {
                                if (datetime.getTimeInMillis() > c.getTimeInMillis()) {
                                    fTimeValue = "" + hour + ":" + selectedMinute + " " + am_pm;
                                    fromTime.setText(fTimeValue);
                                    if (hour == 11)
                                        tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + "PM";
                                    else if (hour == 12)
                                        tTimeValue = "01" + ":" + selectedMinute + " " + am_pm;
                                    else
                                        tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + am_pm;
                                    toTime.setText(tTimeValue);
                                } else {
                                    fromTime.setText("00:00");
                                    toast = Toast.makeText(RequestPlayerActivity.this, "wrong time", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            } else {
                                fTimeValue = "" + hour + ":" + selectedMinute + " " + am_pm;
                                fromTime.setText(fTimeValue);
                                if (hour == 11)
                                    tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + "PM";
                                else if (hour == 12)
                                    tTimeValue = "01" + ":" + selectedMinute + " " + am_pm;
                                else
                                    tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + am_pm;
                                toTime.setText(tTimeValue);
                            }

                        }
                    } else {
                        //  pickTimeAppointment.setText(""+hour+":0"+selectedMinute+am_pm);
                        if (timeStr.equalsIgnoreCase("To")) {

                            tTimeValue = "" + hour + ":0" + selectedMinute + " " + am_pm;
                            toTime.setText(tTimeValue);

                        } else {

                            if (dateInString.equals(toDate)) {
                                if (datetime.getTimeInMillis() > c.getTimeInMillis()) {
                                    fTimeValue = "" + hour + ":0" + selectedMinute + " " + am_pm;
                                    fromTime.setText(fTimeValue);
                                    if (hour == 11)
                                        tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + "PM";
                                    else if (hour == 12)
                                        tTimeValue = "01" + ":0" + selectedMinute + " " + am_pm;
                                    else
                                        tTimeValue = "" + (hour + 1) + ":" + selectedMinute + " " + am_pm;
                                    toTime.setText(tTimeValue);
                                } else {
                                    fromTime.setText("00:00");
                                    toast = Toast.makeText(RequestPlayerActivity.this, "wrong time", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            } else {
                                fTimeValue = "" + hour + ":0" + selectedMinute + " " + am_pm;
                                fromTime.setText(fTimeValue);
                                if (hour == 11)
                                    tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + "PM";
                                else if (hour == 12)
                                    tTimeValue = "01" + ":0" + selectedMinute + " " + am_pm;
                                else
                                    tTimeValue = "" + (hour + 1) + ":0" + selectedMinute + " " + am_pm;
                                toTime.setText(tTimeValue);
                            }

                        }
                    }
                }

            }
        }, hour, minute, false);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

//------------------------Tick button in toolbar for sending Request
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.fav_sports_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.fav_sports_selected) {
//            requestForGame();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void request() {
        Log.e("REACHED", "request()");
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(fTimeValue);
            date = parseFormat.parse(tTimeValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //System.out.println("from"+parseFormat.format(date) + " = " + displayFormat.format(date));
        //System.out.println("to"+parseFormat.format(date) + " = " + displayFormat.format(date));

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(RequestPlayerActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("sport_id", sportsId);
            params.add("user_id", user_id);
//            params.add("location_name", autoCompView.getText().toString());
            params.add("location_name", autoCompView.getSelectedItem().toString());
            params.add("from", fTimeValue);
            params.add("to", tTimeValue);
            params.add("date", dateToApi);
            Log.e("PARAMS", params + "");
            httpClient.post(Config.REQUEST_GAME, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("requestGame", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(RequestPlayerActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RequestPlayerActivity.this, HomeScreenActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 400 || object.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(RequestPlayerActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RequestPlayerActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(RequestPlayerActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(RequestPlayerActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(RequestPlayerActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.GameViewHolder> {

        public List<SportsSuccessList> arrayList;
        public Context context;
        Boolean[] selectedCatogorie;
        private ArrayList<Integer> mDisabledRows;
        // String sportsID;


        public RecyclerViewAdapter(Context context, List<SportsSuccessList> success, String sportsId) {

            this.context = context;
            this.arrayList = success;
            j = arrayList.size();
            selectedCatogorie = new Boolean[j];
            for (int i = 0; i < j; i++) {
                if (sportsId.equalsIgnoreCase(arrayList.get(i).getId() + "")) {
                    selectedCatogorie[i] = true;
                } else {
                    selectedCatogorie[i] = false;

                }
            }
        }


        @Override
        public GameViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.create_game_items, viewGroup, false);
            GameViewHolder listHolder = new GameViewHolder(mainGroup);
            return listHolder;
        }


        @Override
        public int getItemCount() {
            Log.e("size", String.valueOf(arrayList.size()));
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(GameViewHolder holder, final int position) {
            final SportsSuccessList model = arrayList.get(position);


            final GameViewHolder mainHolder = (GameViewHolder) holder;

            try {
                mainHolder.sportsName.setTypeface(PlaythoraUtility.getFont(RequestPlayerActivity.this, RALEWAY_REGULAR));
                mainHolder.sportsName.setText(model.getName());
                Log.v("MainHolder", model.getName());

            } catch (NullPointerException e) {

            }

            if (!selectedCatogorie[position]) {
                mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                mainHolder.sportsName.setTextColor(getResources().getColor(R.color.text_one));
            } else {
                mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                mainHolder.sportsName.setTextColor(getResources().getColor(R.color.white));
            }

            mainHolder.sportsLayout.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {

                    gameName = model.getName();
                    sportsId = String.valueOf(model.getId());
                    Log.e("!!!!!!", gameName);
                    requestText.setText("Hey man, it’s been long since we played, wanna\n play " + gameName + " this weekend with the guys?");


                    if (!selectedCatogorie[position]) {
                        //  Toast.makeText(getApplicationContext(),"SelectedFilter"+model.getName(),Toast.LENGTH_SHORT).show();
                        selectedCatogorie[position] = true;
                        mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                        mainHolder.sportsName.setTextColor(getResources().getColor(R.color.white));

                    } else {
                        selectedCatogorie[position] = false;
                        mainHolder.sportsLayout.setBackground(getResources().getDrawable(R.drawable.leader_board_items));


                    }
                    for (int i = 0; i < j; i++) {
                        if (i == position) {
                            selectedCatogorie[i] = true;
                        } else {
                            selectedCatogorie[i] = false;
                        }

                    }
                    notifyDataSetChanged();
                }
            });

            mainHolder.itemView.setTag(model);


        }

        public class GameViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout sportsLayout;
            TextView sportsName;

            public GameViewHolder(View view) {
                super(view);
                this.sportsName = (TextView) view.findViewById(R.id.leaderBoardChipCloud);
                this.sportsLayout = (RelativeLayout) view.findViewById(R.id.sportCatogories);
            }

        }

    }

//    public class CityAdapter extends BaseAdapter {
//        List<LocationList> arrayList;
//        Context context;
//
//        public CityAdapter(Context context, List<LocationList> cityArrayList) {
//            this.context = context;
//            this.arrayList = cityArrayList;
//
//        }
//
//        @Override
//        public int getCount() {
//            return arrayList.size();
//        }
//
//        @Override
//        public Object getItem(int i) {
//            return i;
//        }
//
//        @Override
//        public long getItemId(int i) {
//            return i;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View row = convertView;
//            final CityHolder holder;
//            final LocationList model = arrayList.get(position);
//
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(
//                    Context.LAYOUT_INFLATER_SERVICE);
//
//            if (row == null) {
//                row = inflater.inflate(R.layout.search_city_item, null);
//
//                holder = new CityAdapter.CityHolder();
//
//                holder.mCity = (TextView) row.findViewById(R.id.tv_city);
//                holder.relativeLayout = (RelativeLayout) row.findViewById(R.id.city_item_layout);
//
//                holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent();
//                        intent.putExtra("LOCATION",model.getName());
//                        intent.putExtra("NCOUNT",notify_count.getText().toString());
//                        setResult(5, intent);
//                        finish();
//                    }
//                });
//
//                row.setTag(holder);
//            } else holder = (CityHolder) row.getTag();
//
//            try {
//                holder.mCity.setTypeface(PlaythoraUtility.getFont(RequestPlayerActivity.this, RALEWAY_REGULAR));
//                holder.mCity.setText(model.getName());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            return row;
//        }
//
//        public void update(List<LocationList> cityArrayList) {
//            arrayList = cityArrayList;
//        }
//
//        class CityHolder {
//            TextView mCity;
//            RelativeLayout relativeLayout;
//        }
//    }


}
