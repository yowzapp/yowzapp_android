package com.yowzapp.playstore.application;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.HomeScreenActivity;
import com.yowzapp.playstore.utils.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by nakul on 05/02/17.
 */
public class SocketMainClass {
    private static final int NOTIFY_ME_ID = 1337;
    public static NotificationManager notifyMgr = null;
    public static SocketMainClass instance = null;
    private static Socket mSocket;
    Context context;
    Notification myNotication;
    String offerId;
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("Key3", "workkkkkk");
            // try {

            // The first in the list of RunningTasks is always the foreground task.
            /*ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
            String foregroundTaskPackageName = foregroundTaskInfo .topActivity.getPackageName();
            PackageManager pm = context.getPackageManager();
            PackageInfo foregroundAppPackageInfo = null;

                foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0);

            String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
                 Log.e("NNNNN", foregroundTaskAppName);
            if(foregroundTaskAppName.equals("Xchange")){
                notifyMgr.cancel(NOTIFY_ME_ID);
            }else{
                triggerNotification(chat.getThreadID(), "Xchange");
            }*/
            //  ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
            JSONObject data = (JSONObject) args[0];

            String message;
            try {
                offerId = data.getString("offerId");
                message = data.getString("message");
            } catch (JSONException e) {
                return;
            }
            ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            Log.e("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            componentInfo.getPackageName();
            if ("com.designstring.xchange.activity.NewChatActivity".equalsIgnoreCase(taskInfo.get(0).topActivity.getClassName())) {
                notifyMgr.cancel(NOTIFY_ME_ID);
                //if(!offerId.equalsIgnoreCase(AppConstants.OFFERID)){
                //triggerNotification( offerId,message);
                sendNotification(message, offerId);
                Log.e("OFFERIDDDDINSIDDE", offerId);
                //}
            } else {
                // triggerNotification(offerId,message);
                Log.e("OFFERIDDDDOUT", offerId);
                sendNotification(message, offerId);
            }

            /*} catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }*/

        }
    };

    public SocketMainClass(MyService context) {

        // this.loginUser = logiUser;
        //  this.passwordUser = passwordser;
        this.context = context;
        notifyMgr = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        init();
    }

    public static SocketMainClass getInstance(MyService context) {

        //  if (instance == null) {
        // instance = new MyXMPP(context, AppPreferences.instance(context).getUserId(), AppPreferences.instance(context).getUserPassword());
        //instanceCreated = true;
        // xChangeDatabase = XChangeDatabase.getDatabaseManagerInstance(context);

        instance = new SocketMainClass(context);
        // }

        // AppUtils.printMe("returning instance of MyXMPP");

        return instance;
    }

    public static Socket getSocket() {
        return mSocket;
    }

    public void init() {
        try {

            mSocket = IO.socket(Config.CHAT_SERVER_URL);

            mSocket.on("message", onNewMessage);
            //mSocket.
            //attemptShowMe();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private void sendNotification(String message, String id) {
        Log.e("NotificationSocket", message);
        int icon;
        if (android.os.Build.VERSION.SDK_INT > 20) {
            icon = R.mipmap.ic_launcher;
        } else {
            icon = R.mipmap.ic_launcher;
        }
        String getResponse = message;
        Gson gson = new Gson();
        //offerList=  gson.fromJson(id, NewOutGoingModel.class);
        //   Log.e("GGGGGCCCCMMMM", offerList.getOfferor().getName());
        Intent chat;

        chat = new Intent(context, HomeScreenActivity.class);
        // chat.putExtra("offerId", offerId);


        chat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, chat,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(icon)
                .setContentTitle("Xchange")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
