package com.yowzapp.playstore.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.CustomViewPager;

/**
 * Created by hemanth on 24/11/16.
 */

public class VenueLocationFragment extends Fragment implements View.OnClickListener {

    public CustomViewPager customViewPager;
    public MyPagerAdapter myPagerAdapter;
    TextView map, mapSelected, list, listSelected;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ground_location_layout, container, false);

        customViewPager = (CustomViewPager) view.findViewById(R.id.groundLocationFragment);
        map = (TextView) view.findViewById(R.id.map);
        mapSelected = (TextView) view.findViewById(R.id.mapSelected);
        list = (TextView) view.findViewById(R.id.list);
        listSelected = (TextView) view.findViewById(R.id.listSelected);

        myPagerAdapter = new MyPagerAdapter(getChildFragmentManager());
        customViewPager.setAdapter(myPagerAdapter);
        customViewPager.setPagingEnabled(false);

        map.setOnClickListener(this);
        list.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.map:
                mapSelected.setVisibility(View.VISIBLE);
                map.setVisibility(View.GONE);
                list.setVisibility(View.VISIBLE);
                listSelected.setVisibility(View.GONE);
                customViewPager.setCurrentItem(0);
                break;
            case R.id.list:
                listSelected.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
                mapSelected.setVisibility(View.GONE);
                map.setVisibility(View.VISIBLE);
                customViewPager.setCurrentItem(1);
                break;

        }

    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Map", "List"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new VenueMapFragment();

                case 1:
                    return new VenueListFragment();

                default:
                    return new VenueMapFragment();

            }

        }
    }
}
