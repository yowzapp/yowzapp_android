package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.InvitePlayersFollowers;
import com.yowzapp.playstore.fragment.InvitePlayersFollowing;
import com.yowzapp.playstore.fragment.InvitePlayersFreeAgents;
import com.yowzapp.playstore.fragment.InvitePlayersTeams;
import com.yowzapp.playstore.model.GamePlayers;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.CustomViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.yowzapp.playstore.activty.CreateGame.MAX_PLAYERS;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 24/11/16.
 */

public class CreateGameInviteFriends extends BaseActivity implements View.OnClickListener {


    public static CustomViewPager customViewPager;
    public static MyPagerAdapter myPagerAdapter;
    public static TextView toolbarTitle;
    public TextView toolbarDone;
    public static int addPlayers;
    public static ArrayList<GamePlayers> stringArrayList;
    public static String playersNumber = "";
    TextView following, followingSelected, followers, followersSelected, freeAgents, freeAgentsSelected, teamNormal, teamSelected;
    Toolbar toolbar;
    TextView invite;
    PreferenceManager mPref;
    ArrayList<GamePlayers> creatGameList;
    List<String> players;
    List<String> teams;
    String gameId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_game_invite_friends);
        mPref = PreferenceManager.instance(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        intializeAllComponents();
        setSupportActionBar(toolbar);

        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarDone = (TextView) findViewById(R.id.toolbar_done);
        toolbarDone.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   if (new Integer(CreateGameInviteFriends.playersNumber) >= stringArrayList.size()) {

                // intent.putExtra("members", stringArrayList);
                if (getCallingActivity().getClassName().equalsIgnoreCase(getString(R.string.package_name) + ".activty.CreateGame")) {
                    Log.e("CREATE_GAME", "CREATE_GAME");
                  /*  if (stringArrayList.size() > (Integer.parseInt(MAX_PLAYERS) + 1)) {
                        Toast.makeText(CreateGameInviteFriends.this, "Maximum " + (Integer.parseInt(MAX_PLAYERS) + 1) + " players allowed", Toast.LENGTH_LONG).show();
                    } else {*/
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("mylist", stringArrayList);
                        intent.putExtras(bundle);
                        setResult(8, intent);
                        finish();
                   // }
                } else {
                    Log.e("GAME_DETAIL", getCallingActivity().getClassName());
                    add();
                }
            }
        });


        stringArrayList = new ArrayList<>();

        if (getCallingActivity() != null) {
            Log.e("CREATE_GAME", getCallingActivity().getClassName());
            if (getCallingActivity().getClassName().equalsIgnoreCase(getString(R.string.package_name) + ".activty.CreateGame")) {
                Log.e("CREATE_GAME", "CREATE_GAME");
                Bundle bundle = getIntent().getExtras();
                stringArrayList = bundle.getParcelableArrayList("mylist");
                playersNumber = MAX_PLAYERS;
            } else {
                playersNumber = GameMain.MAX_PLAYERS;
                Intent intent = getIntent();
                gameId = intent.getStringExtra("gameId");
                Log.e("######", gameId + "************");

            }
        }

        //stringArrayList=CreateGame.creatGameList;
        following.setOnClickListener(this);
        followers.setOnClickListener(this);
        freeAgents.setOnClickListener(this);
        teamNormal.setOnClickListener(this);

        following.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        followingSelected.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        followers.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        followersSelected.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        freeAgents.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        freeAgentsSelected.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        teamNormal.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        teamSelected.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));
        invite.setTypeface(PlaythoraUtility.getFont(CreateGameInviteFriends.this, RALEWAY_REGULAR));


        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toFollowers = new Intent(CreateGameInviteFriends.this, InviteFriendsFromGame.class);
                startActivity(toFollowers);
            }
        });

        customViewPager = (CustomViewPager) findViewById(R.id.customFragment);
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        customViewPager.setAdapter(myPagerAdapter);
        customViewPager.setPagingEnabled(true);
        customViewPager.setCurrentItem(0);
        customViewPager.setOffscreenPageLimit(3);

        customViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.v("Position", String.valueOf(position));
                if (position == 1) {
                    followersMethod();
                    if (InvitePlayersFollowers.adapter != null) {
                        InvitePlayersFollowers.adapter.Refresh(stringArrayList);
                    }

                } else if (position == 2) {
                    teamMethod();
                } else if (position == 3) {
                    freeAgentsMethod();
                } else {
                    followingMethod();
                    if (InvitePlayersFollowing.adapter != null) {
                        InvitePlayersFollowing.adapter.Refresh(stringArrayList);
                    }

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void intializeAllComponents() {
        toolbar = (Toolbar) findViewById(R.id.team_member_toolbar);
        following = (TextView) findViewById(R.id.following);
        followingSelected = (TextView) findViewById(R.id.followingSelected);
        followers = (TextView) findViewById(R.id.followers);
        followersSelected = (TextView) findViewById(R.id.followersSelected);
        freeAgents = (TextView) findViewById(R.id.freeAgents);
        freeAgentsSelected = (TextView) findViewById(R.id.freeAgentsSelected);
        teamNormal = (TextView) findViewById(R.id.teams);
        teamSelected = (TextView) findViewById(R.id.teamsSelectedFour);
        invite = (TextView) findViewById(R.id.invitePlayers);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.following:

                followingMethod();
                customViewPager.setCurrentItem(0);
                if (InvitePlayersFollowing.adapter != null) {
                    InvitePlayersFollowing.adapter.Refresh(stringArrayList);
                }
                break;

            case R.id.followers:

                followersMethod();
                customViewPager.setCurrentItem(1);

                if (InvitePlayersFollowers.adapter != null) {
                    InvitePlayersFollowers.adapter.Refresh(stringArrayList);
                }
                break;

            case R.id.teams:

                teamMethod();


                customViewPager.setCurrentItem(2);

                break;

            case R.id.freeAgents:

                freeAgentsMethod();

                customViewPager.setCurrentItem(3);

                break;

            default:
                break;

        }

    }

    private void freeAgentsMethod() {
        freeAgentsSelected.setVisibility(View.VISIBLE);
        freeAgents.setVisibility(View.GONE);

        followersSelected.setVisibility(View.GONE);
        followers.setVisibility(View.VISIBLE);

        followingSelected.setVisibility(View.GONE);
        following.setVisibility(View.VISIBLE);

        teamNormal.setVisibility(View.VISIBLE);
        teamSelected.setVisibility(View.GONE);
    }

    private void teamMethod() {
        teamNormal.setVisibility(View.GONE);
        teamSelected.setVisibility(View.VISIBLE);

        freeAgentsSelected.setVisibility(View.GONE);
        freeAgents.setVisibility(View.VISIBLE);

        followersSelected.setVisibility(View.GONE);
        followers.setVisibility(View.VISIBLE);

        followingSelected.setVisibility(View.GONE);
        following.setVisibility(View.VISIBLE);
    }

    private void followersMethod() {
        followers.setVisibility(View.GONE);
        followersSelected.setVisibility(View.VISIBLE);

        following.setVisibility(View.VISIBLE);
        followingSelected.setVisibility(View.GONE);

        teamNormal.setVisibility(View.VISIBLE);
        teamSelected.setVisibility(View.GONE);

        freeAgents.setVisibility(View.VISIBLE);
        freeAgentsSelected.setVisibility(View.GONE);
    }

    private void followingMethod() {
        followingSelected.setVisibility(View.VISIBLE);
        following.setVisibility(View.GONE);

        followersSelected.setVisibility(View.GONE);
        followers.setVisibility(View.VISIBLE);

        freeAgentsSelected.setVisibility(View.GONE);
        freeAgents.setVisibility(View.VISIBLE);

        teamNormal.setVisibility(View.VISIBLE);
        teamSelected.setVisibility(View.GONE);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_invite_players, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_select) {
//
//            //   if (new Integer(CreateGameInviteFriends.playersNumber) >= stringArrayList.size()) {
//
//            // intent.putExtra("members", stringArrayList);
//            if (getCallingActivity().getClassName().equalsIgnoreCase(getString(R.string.package_name) + ".activty.CreateGame")) {
//                Log.e("CREATE_GAME", "CREATE_GAME");
//                if (stringArrayList.size() > (Integer.parseInt(MAX_PLAYERS) + 1)) {
//                    Toast.makeText(this, "Maximum " + (Integer.parseInt(MAX_PLAYERS) + 1) + " players allowed", Toast.LENGTH_LONG).show();
//                } else {
//                    Intent intent = new Intent();
//                    Bundle bundle = new Bundle();
//                    bundle.putParcelableArrayList("mylist", stringArrayList);
//                    intent.putExtras(bundle);
//                    setResult(8, intent);
//                    finish();
//                }
//            } else {
//                Log.e("GAME_DETAIL", getCallingActivity().getClassName());
//                add();
//            }
//
//           /* } else {
//                Toast.makeText(getApplicationContext(), "Sorry you can add only " + CreateGameInviteFriends.playersNumber + " number of players.", Toast.LENGTH_SHORT).show();
//            }*/
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void add() {

        creatGameList = new ArrayList<>();
        players = new ArrayList<>();
        teams = new ArrayList<>();
        // Log.e("membersList", String.valueOf(strings));
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).getType().equalsIgnoreCase("player")) {
                players.add(stringArrayList.get(i).getId() + "");
            } else {
                teams.add(stringArrayList.get(i).getId() + "");
            }
        }
        Log.e("PPLAYERS", "" + players.size());
        Log.e("TEAMSS", "" + teams.size());


        try {

            JSONObject mainObj = new JSONObject();
            JSONObject object = new JSONObject();

            JSONArray jsonArray = new JSONArray(players);
            JSONArray jsonArray1 = new JSONArray(teams);
            object.put("teams", jsonArray1);
            object.put("players", jsonArray);
            mainObj.put("members", object);
            mainObj.put("game_id", gameId);

            Log.e("JSONOBJ", mainObj.toString());

            StringEntity entity = new StringEntity(mainObj.toString());

            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(CreateGameInviteFriends.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());

                httpClient.post(CreateGameInviteFriends.this, Config.EDIT_GAME, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("addedPlayers", s);
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 200) {
                                Toast.makeText(CreateGameInviteFriends.this, "Game updated successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent();
                                setResult(8, intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                        }


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Following", "Followers", "Teams", "Free Agents"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new InvitePlayersFollowing(toolbarTitle);

                case 1:
                    return new InvitePlayersFollowers(toolbarTitle);

                case 2:
                    return new InvitePlayersTeams(toolbarTitle);

                case 3:
                    return new InvitePlayersFreeAgents(toolbarTitle);

                default:
                    return new InvitePlayersFollowing(toolbarTitle);

            }

        }
    }
}

