package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 22/11/16.
 */
public class TeamPhotoModel {
    private String image;
    private int id;
    private int team_id;
    //private  int position;

    public TeamPhotoModel(int id, String image, int team_id) {
        this.id = id;
        this.image = image;
        this.team_id = team_id;
       // this.position = pos;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }
   /* public int getPosition(){
        return  position;
    }
    public void setPosition(int pos){
        this.position = pos;
    }*/
}
