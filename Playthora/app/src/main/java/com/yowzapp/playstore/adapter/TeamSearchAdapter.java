package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.OtherTeamViewActivity;
import com.yowzapp.playstore.model.SearchRecentModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.List;

/**
 * Created by nakul on 15/11/16.
 */
public class TeamSearchAdapter extends RecyclerView.Adapter<TeamSearchAdapter.ProfileListHolder> {
    Context mContext;
    List<SearchRecentModel.SearchRecentTeam> teamList;
    TextView teamName;

    public TeamSearchAdapter(Context context, List<SearchRecentModel.SearchRecentTeam> arrayList) {
        mContext = context;
        teamList = arrayList;
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_view, null);
        teamName = layoutView.findViewById(R.id.team_name);

        teamName.setTypeface(PlaythoraUtility.getFont(parent.getContext(), Config.RALEWAY_REGULAR));
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, final int position) {
        teamName.setText(teamList.get(position).getName());
        try {
            Glide.with(mContext).load(teamList.get(position).getCover_pic())
                    .error(R.drawable.no_team)
                    .centerCrop()
                    .into(holder.teamImage);
        } catch (Exception e) {
            holder.teamImage.setImageResource(R.drawable.no_team);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teamList.get(position).getType().equalsIgnoreCase("team")) {
                    Intent i = new Intent(mContext, OtherTeamViewActivity.class);
                    i.putExtra("ID", teamList.get(position).getId());
                    i.putExtra("SEARCH", "?q=search");
                    mContext.startActivity(i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {
        ImageView teamImage;

        public ProfileListHolder(View itemView) {
            super(itemView);
            teamImage = (ImageView) itemView.findViewById(R.id.team_image);
        }
    }

}