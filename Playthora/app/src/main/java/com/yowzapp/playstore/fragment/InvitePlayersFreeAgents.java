package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGameInviteFriends;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.model.GamePlayers;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hemanth on 26/12/16.
 */
public class InvitePlayersFreeAgents extends Fragment {

    RecyclerView recyclerView;
    PreferenceManager mPref;
    TextView s;
    AddTeamMembersModel model;
    Gson gson;
    PlayersAdapter adapter;
    ProgressBar progressBar;
    TextView noPlayers;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<AddTeamMembersList> tempList;
    NestedScrollView nested;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    public InvitePlayersFreeAgents(TextView toolbarTitle) {
        s = toolbarTitle;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.invite_player_layout, container, false);
        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.invite_players_recycler);
        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        noPlayers = (TextView) contentView.findViewById(R.id.noPlayers);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested_player);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        //  invite = (TextView) contentView.findViewById(R.id.invitePlaythora);
        //  invite.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    Log.e("RESPONSEPAGonLoadMoreE", String.valueOf(model.isHasMorePages()));
                    if (model.isHasMorePages()) {
                        Log.e("page_DONE", page + "");
                        pages = page;
                        playerListRecycler(pages);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);


        try {
            playerListRecycler(pages);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

        return contentView;
    }

    private void playerListRecycler(int pages) {
        Log.e("playerList", "@@@@@@");
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            // params.add("user_id",mPref.getId());
            httpClient.get(Config.PLAYERS_LIST + "&page=" + pages, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);

                    try {
                        String s = new String(responseBody);
                        Log.e("playerList", s);

                        model = new AddTeamMembersModel();
                        gson = new Gson();
                        model = gson.fromJson(s, AddTeamMembersModel.class);

                        if (model.getSuccess().getData().size() != 0) {
                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }

                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerView.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                adapter = new PlayersAdapter(getActivity(), model.getSuccess().getData());
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                noPlayers.setText("Players list is empty");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            noPlayers.setText("Players list is empty");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.v("failure", s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.PlayersHolder> {

        private List<AddTeamMembersList> arrayList;
        private Context context;
        private boolean[] mCheckedState;

        public PlayersAdapter(Context context, List<AddTeamMembersList> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
            mCheckedState = new boolean[this.arrayList.size()];
            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                    if (this.arrayList.get(i).getId() == CreateGameInviteFriends.stringArrayList.get(j).getId()) {
                        //AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                        mCheckedState[i] = true;
                        //break;
                    }
                }
            }

        }

        public void RefreshPagination(List<AddTeamMembersList> newString, boolean pagination) {
            if (pagination) {
                mCheckedState = new boolean[this.arrayList.size()];
                this.arrayList = newString;
                for (int i = 0; i < this.arrayList.size(); i++) {
                    mCheckedState[i] = false;
                }
                Log.e("stringList", CreateGameInviteFriends.stringArrayList.size() + "," + this.arrayList.size());
                for (int i = 0; i < this.arrayList.size(); i++) {
                    for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                        if (this.arrayList.get(i).getId() == CreateGameInviteFriends.stringArrayList.get(j).getId()) {
                            Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                            // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                            mCheckedState[i] = true;
                            break;
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }


        @Override
        public PlayersHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.following_item_layout, viewGroup, false);
            PlayersHolder listHolder = new PlayersHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final PlayersHolder holder, final int position) {
            final AddTeamMembersList model = arrayList.get(position);


            final PlayersHolder mainHolder = (PlayersHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());
                if (mCheckedState[position])
                    mainHolder.checkBox.setChecked(true);
                else mainHolder.checkBox.setChecked(false);

                mainHolder.checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mCheckedState[position]) {
                            //Toast.makeText(context,"checked",Toast.LENGTH_SHORT).show();
                            Log.e("CheckedFree", CreateGameInviteFriends.stringArrayList.size() + "");
                           /* if(new Integer(CreateGameInviteFriends.playersNumber)<=CreateGameInviteFriends.stringArrayList.size()){
                                mCheckedState[position] = false;
                                callAlertMaxPlayers();
                                notifyDataSetChanged();
                                return;
                            }*/
                            if (CreateGameInviteFriends.stringArrayList.size() > 0) {
                                for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                                    if (CreateGameInviteFriends.stringArrayList.get(j).getId() == arrayList.get(position).getId()) {

                                    } else {
                                        CreateGameInviteFriends.stringArrayList.add(new GamePlayers(arrayList.get(position).getId(), "player", arrayList.get(position).getProfile_pic()));
                                        // mCheckedState[position]=true;
                                        mCheckedState[position] = true;
                                        return;
                                    }
                                }
                            } else {
                                CreateGameInviteFriends.stringArrayList.add(new GamePlayers(arrayList.get(position).getId(), "player", arrayList.get(position).getProfile_pic()));
                                // mCheckedState[position]=true;
                                mCheckedState[position] = true;
                            }

                            notifyDataSetChanged();

                        } else {
                            //Toast.makeText(context,"unchecked",Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < CreateGameInviteFriends.stringArrayList.size(); i++) {
                                if (CreateGameInviteFriends.stringArrayList.get(i).getId() == arrayList.get(position).getId()) {
                                    CreateGameInviteFriends.stringArrayList.remove(i);

                                }
                            }
                            //notifyDataSetChanged();
                            mCheckedState[position] = false;
                            notifyDataSetChanged();
                        }

                    }


                });

            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setTag(model);


        }

        private void callAlertMaxPlayers() {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
            View mView = layoutInflaterAndroid.inflate(R.layout.user_limit_pop_up, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
            alertDialogBuilderUserInput.setView(mView);
            final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

            final TextView maxPlayer = (TextView) mView.findViewById(R.id.maxPlayer);
            final TextView okay = (TextView) mView.findViewById(R.id.ok);
            final TextView unSelect = (TextView) mView.findViewById(R.id.adminDetails);

            maxPlayer.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
            okay.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
            unSelect.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));


            okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogAndroid.dismiss();
                }
            });


            alertDialogAndroid.show();
        }


        private void showPopUp(String name) {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
            View mView = layoutInflaterAndroid.inflate(R.layout.make_admin_pop_up, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
            alertDialogBuilderUserInput.setView(mView);
            final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

            final TextView userName = (TextView) mView.findViewById(R.id.userName);
            final TextView select = (TextView) mView.findViewById(R.id.select);
            final TextView unSelect = (TextView) mView.findViewById(R.id.unSelect);

            userName.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
            select.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
            unSelect.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));


            userName.setText(name);

            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogAndroid.dismiss();
                }
            });

            unSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogAndroid.dismiss();
                }
            });


            alertDialogAndroid.show();
        }

        public class PlayersHolder extends RecyclerView.ViewHolder {
            CircleImageView userImage;
            TextView name;
            CheckBox checkBox;
            RelativeLayout makeAdmin;

            public PlayersHolder(View view) {
                super(view);
                this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
                this.name = (TextView) view.findViewById(R.id.userName);
                this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                this.makeAdmin = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);
            }
        }
    }
}
