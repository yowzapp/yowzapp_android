package com.yowzapp.playstore.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.viewpagerindicator.CirclePageIndicator;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGameGroundSlots;
import com.yowzapp.playstore.model.GroundListItem;
import com.yowzapp.playstore.model.VenueListModel;
import com.yowzapp.playstore.utils.ClusterRenderer;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.LoopViewPager;
import com.yowzapp.playstore.utils.MyItem;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 24/11/16.
 */
public class VenueMapFragment extends Fragment implements LocationListener, OnMapReadyCallback {

    //    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 200;
    public static int REQUEST_CHECK_SETTINGS = 96;
    public static boolean shouldLoadMap = false;
    public double latitudevalue;
    public double longitudevalue;
    SupportMapFragment mapFragment;
    boolean isGPSEnabled;
    String locationName = "";
    Location mLastLocation;
    Marker mCurrLocationMarker;
    PreferenceManager mPref;
    ImageView venueImage, locationGps;
    LoopViewPager imageViewPager;
    CirclePageIndicator indicator;
    Button book, cancel;
    RatingBar rating;
    String sGround = "", sGroundName;
    int j;
    String VenueResponse;
    int PLACE_PICKER_REQUEST = 1;
    MarkerOptions markerOptions = new MarkerOptions();
    VenueListModel groundListModel;
    Gson gson;
    LatLng center;
    TextView groundName, groundLocation, groundDistance, txtamenities, rateThisPlace, txtGround, amenitiesText;
    TextView noVenue;
    RecyclerView recyclerView, recyclerGame, recyclerAmenities, recyclerGrounds;
    Dialog dialog;
    JSONObject jsonObject;
    JSONArray jsonArray;
    String Requirestring;
    BitmapDescriptor icon;
    ProgressBar progressBar;
    FragmentManager fm;
    private GoogleMap map;
    private LocationManager locationManager;
    private Geocoder geocoder;
    // private LatLng center;
    private List<Address> addresses;
    private ArrayList<LatLng> latlngs;
    private ClusterManager<MyItem> mClusterManager;
    View view;
    Status status;


    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.map_layout, container, false);
            fm = getChildFragmentManager();
            progressBar = (ProgressBar) view.findViewById(R.id.location_progressbar);
            noVenue = view.findViewById(R.id.no_venue_text);
            noVenue.setVisibility(View.VISIBLE);
            noVenue.setTypeface(PlaythoraUtility.getFont(getContext(), RALEWAY_REGULAR));
        }

        noVenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(getActivity(),
//                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                                MY_PERMISSIONS_REQUEST_LOCATION);
                        String[] perms = {"android.permission.ACCESS_FINE_LOCATION"};
                        int permsRequestCode = 200;
                        requestPermissions(perms, permsRequestCode);
                    } else if (!isLocationEnabled(getActivity())) {
                        askForLocationPermission(getActivity());
                    } else {
                        shouldLoadMap = true;
                    }
                } else {
                    if (!isLocationEnabled(getActivity())) {
                        askForLocationPermission(getActivity());
                    } else {
                        shouldLoadMap = true;
                    }
                }
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(getActivity(),
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                        MY_PERMISSIONS_REQUEST_LOCATION);
                String[] perms = {"android.permission.ACCESS_FINE_LOCATION"};
                int permsRequestCode = 200;
                requestPermissions(perms, permsRequestCode);
            } else if (!isLocationEnabled(getActivity())) {
                askForLocationPermission(getActivity());
            } else {
                shouldLoadMap = true;
            }
        } else {
            if (!isLocationEnabled(getActivity())) {
                askForLocationPermission(getActivity());
            } else {
                shouldLoadMap = true;
            }
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    private void loadVenueMap() {
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mPref = PreferenceManager.instance(getActivity());

        //To setup location manager
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);

//        try {
//            LatLng p1 = getLocationFromAddress(getActivity(), locationName);
//            latitudevalue = p1.latitude;
//            longitudevalue = p1.longitude;
//            Log.e("AAAAAAAAAAAA", locationName);
//            Log.e("AAAAAAAAAAAA", String.valueOf(p1));
//            MarkerOptions markerOptions = new MarkerOptions();
//            markerOptions.position(p1);
//            markerOptions.icon(icon);
//            map.addMarker(markerOptions);
//            map.animateCamera(CameraUpdateFactory.newLatLngZoom(p1, 7));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

      /*  PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            getActivity().startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }*/
    }

    private void askForLocationPermission(final Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("LOCATION LOG", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("LOCATION LOG", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("LOCATION LOG", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("LOCATION LOG", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    private void addItems() {

        // Set some lat/lng coordinates to start with.
        double lat;
        double lng;
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(icon);
        // Add ten cluster items in close proximity, for purposes of this example.
        Log.e("LATLONGSIZE", groundListModel.getSuccess().size() + "");
        for (int i = 0; i < groundListModel.getSuccess().size(); i++) {
            // double offset = i / 60d;
            lat = groundListModel.getSuccess().get(i).getLat();
            lng = groundListModel.getSuccess().get(i).getLng();
            MyItem offsetItem = new MyItem(markerOptions, lat, lng);
            mClusterManager.addItem(offsetItem);
        }
    }

    //    private void getLocationList(final double latitudevalue, final double longitudevalue) {
    private void getLocationList() {
        try {
            if (PlaythoraUtility.checkInternetConnection(getActivity())) {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
//            RequestParams params = new RequestParams();
//            params.add("lat", String.valueOf(latitudevalue));
//            params.add("lng", String.valueOf(longitudevalue));
//            httpClient.get(Config.LOCATION_LIST,params, new AsyncHttpResponseHandler() {
//            httpClient.get(Config.VENUE_LIST, params, new AsyncHttpResponseHandler() {
                httpClient.get(Config.VENUE_LIST, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        progressBar.setVisibility(View.GONE);
                        noVenue.setVisibility(View.GONE);
                        try {
                            VenueResponse = new String(responseBody);
                            Log.e("respSuccess", VenueResponse);
                            groundListModel = new VenueListModel();
                            gson = new Gson();
                            groundListModel = gson.fromJson(VenueResponse, VenueListModel.class);

                            latlngs = new ArrayList<>();
                            LatLng latLng;
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (int i = 0; i < groundListModel.getSuccess().size(); i++) {
                                Log.e("LIST", String.valueOf(groundListModel.getSuccess().get(i).getLat()) + " " + String.valueOf(groundListModel.getSuccess().get(i).getLng()));
                                //latLng = new LatLng(groundListModel.getSuccess().get(i).getLat(), groundListModel.getSuccess().get(i).getLng());
                                latlngs.add(new LatLng(groundListModel.getSuccess().get(i).getLat(), groundListModel.getSuccess().get(i).getLng()));
                                builder.include(new LatLng(groundListModel.getSuccess().get(i).getLat(), groundListModel.getSuccess().get(i).getLng()));
                                Log.e("LAT", String.valueOf(latlngs));
                            }
//                            LatLngBounds bounds = builder.build();
                            int padding = 64; // offset from edges of the map in pixels
//                            int paddingDp = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 16, new DisplayMetrics());
//                            Log.e("Map Padding", String.valueOf(paddingDp));
//                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);


                            if (!groundListModel.getSuccess().isEmpty()) {
                                noVenue.setVisibility(View.GONE);
                                if (mClusterManager != null) {
                                    Log.e("NULLL", "NULLLL");
                                    if (mClusterManager.getMarkerCollection().getMarkers().size() == 0) {
                                        Log.e("NULLL", "NULL 0");
                                        mClusterManager = new ClusterManager<MyItem>(getActivity(), map);
                                        ClusterRenderer clusterRenderer = new ClusterRenderer(getActivity(), map, mClusterManager); // not needed to use clusterManager.setRenderer method since i made it in constructor

                                        map.setOnCameraIdleListener(mClusterManager);
//                                        latLng = new LatLng(groundListModel.getSuccess().get(0).getLat(), groundListModel.getSuccess().get(0).getLng());
//                                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
//                                        map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
                                        addItems();
                                    } else {
                                        Log.e("NULLL", "NULL 1");
                                        mClusterManager.clearItems();
                                    /*mClusterManager = new ClusterManager<MyItem>(getActivity(), map);
                                    ClusterRenderer clusterRenderer = new ClusterRenderer(getActivity(), map, mClusterManager); // not needed to use clusterManager.setRenderer method since i made it in constructor

                                    map.setOnCameraIdleListener(mClusterManager);
                                    //map.setOnMarkerClickListener(mClusterManager);
                                    latLng = new LatLng(groundListModel.getSuccess().get(0).getLat(), groundListModel.getSuccess().get(0).getLng());
                                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18);*/
//                                        latLng = new LatLng(groundListModel.getSuccess().get(0).getLat(), groundListModel.getSuccess().get(0).getLng());
//                                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 7));
                                    }
                                } else {
                                    Log.e("NULLL", "NOT NULL");
                                    mClusterManager = new ClusterManager<MyItem>(getActivity(), map);
                                    ClusterRenderer clusterRenderer = new ClusterRenderer(getActivity(), map, mClusterManager); // not needed to use clusterManager.setRenderer method since i made it in constructor

                                    map.setOnCameraIdleListener(mClusterManager);
                                    //map.setOnMarkerClickListener(mClusterManager);
//                                    latLng = new LatLng(groundListModel.getSuccess().get(0).getLat(), groundListModel.getSuccess().get(0).getLng());
//                                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 7));
                                }
                                //-------To Show all Markers under zoom
//                                map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
//                                map.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
                                if (latlngs.size() == 1) {
                                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlngs.get(0), 10.0f));
//                                    new Thread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            try {
//                                                Thread.sleep(5000);
//                                                getActivity().runOnUiThread(new Runnable() {
//                                                    @Override
//                                                    public void run() {
////                                                        map.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
////                                                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlngs.get(0), 10.0f));
//                                                    }
//                                                });
//                                            } catch (InterruptedException e) {
//                                                e.printStackTrace();
//                                            }
//                                        }
//                                    }).start();
                                } else {
                                    map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
                                }
                                addItems();
                            } else {
                                Log.e("NULL", "EMPTY");
                                noVenue.setVisibility(View.VISIBLE);
                                if (mClusterManager != null) {
                                    Log.e("NULL", "EMPTY NULL");
                                    mClusterManager.clearItems();
                                }
//                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudevalue, longitudevalue), 5));
//                                map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
//                                Toast.makeText(getActivity(), "No near by venues", Toast.LENGTH_SHORT).show();
                            }
 /* latlngs.add(new LatLng(12.9250, 77.5938));
        latlngs.add(new LatLng(12.9226, 77.6174));
        latlngs.add(new LatLng(12.8984, 77.6179));
        latlngs.add(new LatLng(12.9082, 77.6074));
        for (LatLng point : latlngs) {
            markerOptions.position(point);
            map.addMarker(markerOptions);
        }*/
                            //latlngs.add(new LatLng(12.9250, 77.5938));
                       /* for (LatLng point : latlngs) {
                            Log.e("LONG", String.valueOf(point));
                            markerOptions.position(point);
                            markerOptions.icon(icon);
                            map.addMarker(markerOptions);
                        }*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        progressBar.setVisibility(View.GONE);
                        noVenue.setVisibility(View.VISIBLE);
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Some error occured while loading Venues", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            } else {
//                Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(String.valueOf(strAddress), 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (isLocationEnabled(getActivity())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //User has previously accepted this permission
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    initializeMap();
                    map.setMyLocationEnabled(true);
                }
            } else {
                //Not in api-23, no need to prompt
                initializeMap();
                map.setMyLocationEnabled(true);
            }
            noVenue.setVisibility(View.GONE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults);

        if (permsRequestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("LocationPermission", "GRANTED");
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (!isLocationEnabled(getActivity())) {
                        askForLocationPermission(getActivity());
                    } else {
                        loadVenueMap();
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Location permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void initializeMap() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Location permission denied", Toast.LENGTH_LONG).show();
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

        map.getUiSettings().setMyLocationButtonEnabled(true);


        geocoder = new Geocoder(getActivity(), Locale.ENGLISH);
        try {

            List addressList = geocoder.getFromLocationName(locationName, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = (Address) addressList.get(0);
                StringBuilder sb = new StringBuilder();
                sb.append(address.getLatitude()).append("\n");
                sb.append(address.getLongitude()).append("\n");
                String result = sb.toString();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                Log.e("latitude", String.valueOf(latLng.latitude));
//                Log.e("latitude", String.valueOf(latLng.longitude));
//                latitudevalue = latLng.latitude;
//                longitudevalue = latLng.longitude;
//                getLocationList(latitudevalue, longitudevalue);
                getLocationList();

            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                // Toast.makeText(getContext(),"Lat: "+marker.getPosition().latitude+" Long: "+marker.getPosition().longitude,Toast.LENGTH_SHORT).show();

               /* List<Address> address = null;
                Geocoder geocode;
                // TODO Auto-generated method stub
                geocode = new Geocoder(getActivity(),Locale.getDefault());
                try {
                    address=geocode.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String address1=address.get(0).getSubLocality();
               // Toast.makeText(getActivity(),address1, Toast.LENGTH_LONG).show();

*/
                try {
                    jsonObject = new JSONObject(VenueResponse);
                    Log.e("########", jsonObject.toString());
                    jsonArray = jsonObject.getJSONArray("success");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < groundListModel.getSuccess().size(); i++) {

                    if (marker.getPosition().latitude == groundListModel.getSuccess().get(i).getLat() && marker.getPosition().longitude == groundListModel.getSuccess().get(i).getLng()) {
                        //  Log.e("&&&&&&&&&&&&&&&&", String.valueOf(groundListModel.getSuccess().get(i)));
                        try {
                            Requirestring = jsonArray.get(i).toString();
                            Log.e("&&&&&&&&&&&&&&&&", Requirestring);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showDialog(i, Requirestring);
                    }


                }


                //showDialog(address1);


                return false;
            }
        });
    }

    private void showDialog(int address1, final String requirestring) {

        Log.e("JJJJJJJ", String.valueOf(address1));
        Log.e("request", requirestring);

//        TextView locName;
//        RatingBar rating;
        final String venueCover, venueName, locaName, distance, amenities, sportsType;
        final int rate;
//        RelativeLayout mapRelativeLayout;
        final List<GroundListItem> groundListItems;
        final ArrayList<String> venuePictures;

//        dialog = new Dialog(getContext());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.map_dialog_layout);

//        locName = (TextView) dialog.findViewById(R.id.locationName);
//        rating = (RatingBar) dialog.findViewById(R.id.rating);
//        mapRelativeLayout = (RelativeLayout) dialog.findViewById(R.id.map_dialog);

//        locName.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        locaName = groundListModel.getSuccess().get(address1).getLocation_name();
        rate = groundListModel.getSuccess().get(address1).getRating();
        venueCover = groundListModel.getSuccess().get(address1).getCover_pic();
        venueName = groundListModel.getSuccess().get(address1).getName();
        distance = groundListModel.getSuccess().get(address1).getDistance();
        amenities = groundListModel.getSuccess().get(address1).getAmenities();
        sportsType = groundListModel.getSuccess().get(address1).getSports_types();
        groundListItems = groundListModel.getSuccess().get(address1).getGrounds();
        venuePictures = groundListModel.getSuccess().get(address1).getImages();
        //----------If want to add Venue Cover also in SwipeViewPager
//        venuePictures.add(0, venueCover);

//        int picturesCount = venuePictures.size();
//        for(int i=0; i<picturesCount; i++){
//
//        }

//        locName.setText(venueName);
//        rating.setRating(rate);

//        mapRelativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
        mapVenueDialog(venueName, venueCover, locaName, rate, distance, amenities, sportsType, groundListItems, requirestring, venuePictures);
//            }
//        });


//        dialog.show();

    }

    private void mapVenueDialog(final String venueName, final String venueCover, final String locaName, final int rate, String distance,
                                String amenities, final String sportsType, final List<GroundListItem> groundListItems, final String requirestring, final ArrayList<String> venuePictures) {

        final Dialog phoneDialog = new Dialog(getActivity());
        phoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // phoneDialog.getWindow().setBackgroundDrawable(new ColorDrawable(yowzapp.graphics.Color.TRANSPARENT));
//        phoneDialog.setContentView(R.layout.map_venue_detail_dialog_layout);
        phoneDialog.setContentView(R.layout.ground_detail_dialog_layout);

        final Window window = phoneDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);

        sGround = "";
        Log.e("vName", venueName);
        Log.e("vCover", venueCover);
        Log.e("vLocation", locaName);
        Log.e("vRate", String.valueOf(rate));
        Log.e("vDist", distance);
        Log.e("groundList", requirestring);

        venueImage = (ImageView) phoneDialog.findViewById(R.id.gImage);
        imageViewPager = (LoopViewPager) phoneDialog.findViewById(R.id.image_viewpager);
        indicator = (CirclePageIndicator) phoneDialog.findViewById(R.id.circle_indicator_image);
        groundName = (TextView) phoneDialog.findViewById(R.id.gName);
        rateThisPlace = (TextView) phoneDialog.findViewById(R.id.rateThisPlace);
        groundLocation = (TextView) phoneDialog.findViewById(R.id.gLocation);
        book = (Button) phoneDialog.findViewById(R.id.button_book);
        cancel = (Button) phoneDialog.findViewById(R.id.cancel_button);
        txtamenities = (TextView) phoneDialog.findViewById(R.id.amenities);
        txtGround = (TextView) phoneDialog.findViewById(R.id.ground_text);
        recyclerGame = (RecyclerView) phoneDialog.findViewById(R.id.ground_games);
        recyclerAmenities = (RecyclerView) phoneDialog.findViewById(R.id.ground_amenities);
        recyclerGrounds = (RecyclerView) phoneDialog.findViewById(R.id.grounds);
        rating = (RatingBar) phoneDialog.findViewById(R.id.dialog_rating);
        groundDistance = (TextView) phoneDialog.findViewById(R.id.distance);
        amenitiesText = (TextView) phoneDialog.findViewById(R.id.amenitiesText);
        locationGps = (ImageView) phoneDialog.findViewById(R.id.locationGps);

        amenitiesText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

        if (distance.toString().equalsIgnoreCase("0.0 km")) {
            groundDistance.setVisibility(View.GONE);
        } else {
            groundDistance.setVisibility(View.VISIBLE);
            groundDistance.setText(distance);
        }


        rating.setRating(rate);

        groundName.setText(venueName);
        groundLocation.setText(locaName);
        if (locaName.isEmpty())
            locationGps.setVisibility(View.GONE);
        else locationGps.setVisibility(View.VISIBLE);

        try {
            Glide.with(getContext()).load(venueCover).error(R.drawable.test_location).centerCrop().into(venueImage);
        } catch (Exception e) {
            venueImage.setImageResource(R.drawable.test_location);
        }


        groundName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
        groundLocation.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        book.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        txtamenities.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        txtGround.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        rateThisPlace.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final String[] names = sportsType.split(",");
                GamesAdapter adapter = new GamesAdapter(names);
                recyclerGame.setHasFixedSize(true);
                recyclerGame.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                recyclerGame.setAdapter(adapter);
            }
        });

        String[] amenitiesList = amenities.split(",");

        for (int i = 0; i < amenitiesList.length; i++) {
            if ((i % 2) == 0) {
                amenitiesText.setText(amenitiesText.getText().toString() + amenitiesList[i] + "   ");
            } else {
                amenitiesText.setText(amenitiesText.getText().toString() + amenitiesList[i] + "\n");
            }

        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GroundAdapter groundAdapter = new GroundAdapter(groundListItems);
                recyclerGrounds.setHasFixedSize(true);
                recyclerGrounds.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                recyclerGrounds.setAdapter(groundAdapter);
            }
        });

        if (venuePictures.size() != 0) {
            ImageAdapter adapter = new ImageAdapter(getContext(), venuePictures);
            imageViewPager.setAdapter(adapter);
            imageViewPager.setCurrentItem(0);
            indicator.setViewPager(imageViewPager);
            indicator.setStrokeWidth(0);
        }

        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(getActivity());
                } else {
                    phoneDialog.dismiss();
                    Intent intent = new Intent(getActivity(), CreateGameGroundSlots.class);
                    intent.putExtra("gID", sGround);
                    intent.putExtra("gName", sGroundName);
                    intent.putExtra("vName", venueName);
                    intent.putExtra("vLocation", locaName);
                    intent.putExtra("venueCover", venueCover);
                    intent.putExtra("Rating", rate);
                    intent.putExtra("venue", "pay");

                    try {
                        JSONObject jsonObject = new JSONObject(requirestring);
                        intent.putExtra("GroundArrayList", jsonObject.getJSONArray("grounds").toString());
                        intent.putExtra("IMAGES", jsonObject.getJSONArray("images").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // intent.putExtra("GroundArrayList",requirestring);
              /*  try {
                    JSONObject joj= new JSONObject(VenueResponse);
                    JSONArray jarray=  joj.getJSONArray("success");
                    JSONObject venueObject= jarray.getJSONArray("grounds");
                    intent.putExtra("GroundArrayList", venueObject.getJSONArray("grounds").toString());
                    Log.e("%%%%%%%%%%",venueObject.getJSONArray("grounds").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                    startActivity(intent);


                }
            }


        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneDialog.dismiss();
            }
        });


        phoneDialog.show();

    }

    @Override
    public void onLocationChanged(Location location) {

        //To clear map data
        map.clear();
        //To hold location

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        latitudevalue = location.getLatitude();
        longitudevalue = location.getLongitude();
        //To create marker in map
        /*MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(icon);*/
        //adding marker to the map
        //map.addMarker(markerOptions);
        //opening position with some zoom level in the map
        //getLocationList(latitudevalue, longitudevalue);
//        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 7));

       /* latlngs.add(new LatLng(12.9250, 77.5938));
        latlngs.add(new LatLng(12.9226, 77.6174));
        latlngs.add(new LatLng(12.8984, 77.6179));
        latlngs.add(new LatLng(12.9082, 77.6074));
        for (LatLng point : latlngs) {
            markerOptions.position(point);
            map.addMarker(markerOptions);
        }*/
        //MarkerItem markerItem = new MarkerItem(markerOptions);
        // mClusterManager.addItem(markerOptions);
        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
       /* mClusterManager = new ClusterManager<MyItem>(getActivity(), map);
        ClusterRenderer clusterRenderer = new ClusterRenderer(getActivity(), map, mClusterManager); // not needed to use clusterManager.setRenderer method since i made it in constructor

        map.setOnCameraIdleListener(mClusterManager);
        map.setOnMarkerClickListener(mClusterManager);*/


        /*for (LatLng point : latlngs) {
            markerOptions.position(point);
            map.addMarker(markerOptions);
        }*/


        float zoomLevel = 11.0f; //This goes up to 21
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
        locationManager.removeUpdates(this);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("Location Services Disabled");
        // Setting Dialog Message
        alertDialog.setMessage("Please enable location services");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Your location has not been updated!", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldLoadMap) {
            loadVenueMap();
            shouldLoadMap = false;
        }
/*
        if(!isLocationEnabled(getActivity())) {
            showSettingsAlert();
        }else{
            if (yowzapp.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkLocationPermission();
            }
        }*/

    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                //  TODO: Prompt with explanation!

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.GamesHolder> {
        String[] names;

        public GamesAdapter(String[] names) {
            this.names = names;
        }

        @Override
        public GamesAdapter.GamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_games_list, parent, false);
            GamesHolder listHolder = new GamesHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(GamesAdapter.GamesHolder holder, int position) {
            if (!names[0].toString().isEmpty())
                holder.sportsname.setText(names[position]);
            else holder.sportsname.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return names.length;
        }

        public class GamesHolder extends RecyclerView.ViewHolder {
            TextView sportsname;

            public GamesHolder(View itemView) {
                super(itemView);
                sportsname = (TextView) itemView.findViewById(R.id.gameName);
                sportsname.setTypeface(PlaythoraUtility.getFont(itemView.getContext(), RALEWAY_REGULAR));
            }
        }

    }

    public class GroundAdapter extends RecyclerView.Adapter<GroundAdapter.GroundHolder> {
        List<GroundListItem> gList;
        Boolean[] selectedGround;

        public GroundAdapter(List<GroundListItem> gList) {
            this.gList = gList;
            j = gList.size();
            selectedGround = new Boolean[j];
            for (int i = 0; i < j; i++) {
                selectedGround[i] = false;
            }
        }

        @Override
        public GroundAdapter.GroundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_item, parent, false);
            GroundHolder listHolder = new GroundHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(final GroundHolder holder, final int position) {
            if (!gList.get(0).toString().isEmpty()) {
                holder.groundName.setText(gList.get(position).getName());
            } else {
                holder.groundName.setVisibility(View.GONE);
            }

            if (!selectedGround[position]) {
                holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_name_back));
                holder.groundName.setTextColor(getResources().getColor(R.color.white));
            } else {
                holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                holder.groundName.setTextColor(getResources().getColor(R.color.white));
            }

            holder.groundName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    for (int i = 0; i < j; i++) {
                        selectedGround[i] = false;
                        if (!selectedGround[i]) {
                            holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_name_back));
                        }
                    }
                    sGround = String.valueOf(gList.get(position).getId());
                    sGroundName = gList.get(position).getName();


                    if (!selectedGround[position]) {
                        selectedGround[position] = true;
                        holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_name_back));
                        holder.groundName.setTextColor(getResources().getColor(R.color.white));
                        notifyDataSetChanged();

                        Log.e("GroundID", String.valueOf(gList.get(position).getId()));

                    } else {
                        selectedGround[position] = false;
                        holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                        holder.groundName.setTextColor(getResources().getColor(R.color.white));
                        notifyDataSetChanged();

                        Log.e("GroundID", String.valueOf(gList.get(position).getId()));
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return gList.size();
        }

        public class GroundHolder extends RecyclerView.ViewHolder {
            TextView groundName;

            public GroundHolder(View itemView) {
                super(itemView);
                groundName = (TextView) itemView.findViewById(R.id.groundName);
                groundName.setTypeface(PlaythoraUtility.getFont(itemView.getContext(), RALEWAY_REGULAR));
            }
        }
    }

    private class ImageAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        ArrayList<String> images;
        Context context;
        String imageUrl;

        public ImageAdapter(Context context, ArrayList<String> images) {
            this.context = context;
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            View itemView = mLayoutInflater.inflate(R.layout.image_view_layout, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.images);

            if (PlaythoraUtility.checkInternetConnection(context)) {
                try {
                    imageUrl = images.get(position);
                    Glide.with(context).load(imageUrl).centerCrop().into(imageView);
                    container.addView(itemView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

}