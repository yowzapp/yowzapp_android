package com.yowzapp.playstore.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vaishakha on 26/12/16.
 */
public class AddTeamMembersList implements Parcelable {
    public static final Creator<AddTeamMembersList> CREATOR = new Creator<AddTeamMembersList>() {
        @Override
        public AddTeamMembersList createFromParcel(Parcel in) {
            return new AddTeamMembersList(in);
        }

        @Override
        public AddTeamMembersList[] newArray(int size) {
            return new AddTeamMembersList[size];
        }
    };
    private int id;
    private String name;
    private String lat;
    private String lng;
    private String bio;
    private String profile_pic;
    private String cover_pic;
    private boolean is_private;
    private boolean is_social;
    private String email;
    private String mobile;
    private String points;
    private String location_name;
    private boolean mobile_verified;
    private PivotModel pivot;

    public AddTeamMembersList(String name, int id, String profile_pic) {
        this.name = name;
        this.id = id;
        this.profile_pic = profile_pic;
    }

    public AddTeamMembersList(PivotModel pivot) {

    }

    protected AddTeamMembersList(Parcel in) {
        id = in.readInt();
        name = in.readString();
        lat = in.readString();
        lng = in.readString();
        bio = in.readString();
        profile_pic = in.readString();
        cover_pic = in.readString();
        is_private = in.readByte() != 0;
        is_social = in.readByte() != 0;
        email = in.readString();
        mobile = in.readString();
        points = in.readString();
        location_name = in.readString();
        mobile_verified = in.readByte() != 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public boolean is_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }

    public boolean is_social() {
        return is_social;
    }

    public void setIs_social(boolean is_social) {
        this.is_social = is_social;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public boolean isMobile_verified() {
        return mobile_verified;
    }

    public void setMobile_verified(boolean mobile_verified) {
        this.mobile_verified = mobile_verified;
    }

    public PivotModel getPivot() {
        return pivot;
    }

    public void setPivot(PivotModel pivot) {
        this.pivot = pivot;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(bio);
        dest.writeString(profile_pic);
        dest.writeString(cover_pic);
        dest.writeByte((byte) (is_private ? 1 : 0));
        dest.writeByte((byte) (is_social ? 1 : 0));
        dest.writeString(email);
        dest.writeString(mobile);
        dest.writeString(points);
        dest.writeString(location_name);
        dest.writeByte((byte) (mobile_verified ? 1 : 0));
    }
}
