package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.BadgeModel;
import com.yowzapp.playstore.model.BadgeSuccess;
import com.yowzapp.playstore.model.GameListModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vaishakha on 16/1/17.
 */
public class ProfileBadges extends BaseActivity {
    RecyclerView recyclerView;
    PreferenceManager mPref;
    GameListModel gameListModel;
    Gson gson;
    Toolbar toolbar;
    BadgeImage adapter;
    NestedScrollView nested;
    ProgressBar progressBar;
    String userId;
    BadgeModel badgeModel;
    List<BadgeSuccess> badgeSuccessList;
    Gson getGson;
    String userName, admin;
    String getUserName;
    private GridLayoutManager mGridLayoutManager;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_location_layout);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        mPref = PreferenceManager.instance(this);

        initializeAllComponent();

        Intent intent = getIntent();
        if (intent.hasExtra("user_id")) {
            userId = intent.getStringExtra("user_id");
            userName = intent.getStringExtra("name");
            admin = intent.getStringExtra("admin");
        }
        Log.e("userId", userId + "");
        Log.e("userName", userName + "");
        Log.e("admin", admin + "");

        if (userName.length() > 15) {
            getUserName = userName.substring(0, 15) + "...";
        } else {
            getUserName = userName;
        }
        TextView toolBarTitle = (TextView) findViewById(R.id.followers_toolbar_title);

        if (admin.equalsIgnoreCase("true")) {
            toolBarTitle.setText("My Badges");
        } else {
            toolBarTitle.setText(getUserName + "'s" + " Badges");
        }


        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        populateBadges();


        // nested.setOnScrollChangeListener();


    }

    private void populateBadges() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(ProfileBadges.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("user_id", userId);
            httpClient.get(Config.BADGES, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("badges", s);
                    badgeModel = new BadgeModel();
                    getGson = new Gson();
                    badgeModel = getGson.fromJson(s, BadgeModel.class);
                    if (badgeModel.getSuccess().size() != 0) {

                        // mGridLayoutManager = new GridLayoutManager(ProfileBadges.this, 2);
                        linearLayoutManager = new LinearLayoutManager(ProfileBadges.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setFocusable(false);
                        recyclerView.setNestedScrollingEnabled(false);
                        adapter = new BadgeImage(ProfileBadges.this, badgeModel.getSuccess());
                        recyclerView.setAdapter(adapter);

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                }
            });

        }
    }

    private void initializeAllComponent() {
        progressBar = (ProgressBar) findViewById(R.id.paginate_profile_bar);
        nested = (NestedScrollView) findViewById(R.id.nested_profile);
        toolbar = (Toolbar) findViewById(R.id.profilegame_toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_profile_games);
        recyclerView.setHasFixedSize(true);
    }

    public class BadgeImage extends RecyclerView.Adapter<BadgeImage.AddMembersFollowHolder> {

        private List<BadgeSuccess> arrayList;
        private Context context;

        public BadgeImage(Context context, List<BadgeSuccess> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }

        @Override
        public AddMembersFollowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.badge_layout, parent, false);
            AddMembersFollowHolder listHolder = new AddMembersFollowHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(AddMembersFollowHolder holder, final int position) {
            final BadgeSuccess model = arrayList.get(position);

            final AddMembersFollowHolder mainHolder = (AddMembersFollowHolder) holder;


            try {
                Glide.with(context).load(model.getBadge_cover()).centerCrop().into(mainHolder.cover);
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Glide.with(context).load(model.getBadge_icon()).centerCrop().into(mainHolder.icon);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mainHolder.itemView.setTag(model);

        }

        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        public class AddMembersFollowHolder extends RecyclerView.ViewHolder {
            CircleImageView icon;
            ImageView cover;

            public AddMembersFollowHolder(View view) {
                super(view);
                this.icon = (CircleImageView) view.findViewById(R.id.badge_icon);
                this.cover = (ImageView) view.findViewById(R.id.badge_cover);

            }
        }

    }
}
