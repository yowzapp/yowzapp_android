package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 26/12/16.
 */
public class VenueListSportsTypeItem {
    private int id;
    private String name;
    private String cover_pic;
    private String description;
    private String sports_types;
    private boolean is_verified;
    private boolean is_disabled;
    private int is_available;
    private int vendor_id;
    private double lat;
    private double lng;
    private String location_name;


    private List<Grounds> grounds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSports_types() {
        return sports_types;
    }

    public void setSports_types(String sports_types) {
        this.sports_types = sports_types;
    }

    public boolean is_verified() {
        return is_verified;
    }

    public void setIs_verified(boolean is_verified) {
        this.is_verified = is_verified;
    }

    public boolean is_disabled() {
        return is_disabled;
    }

    public void setIs_disabled(boolean is_disabled) {
        this.is_disabled = is_disabled;
    }

    public int getIs_available() {
        return is_available;
    }

    public void setIs_available(int is_available) {
        this.is_available = is_available;
    }

    public int getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(int vendor_id) {
        this.vendor_id = vendor_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }


    public List<Grounds> getGrounds() {
        return grounds;
    }

    public void setGrounds(List<Grounds> grounds) {
        this.grounds = grounds;
    }


}
