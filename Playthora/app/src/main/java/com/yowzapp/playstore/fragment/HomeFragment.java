package com.yowzapp.playstore.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.viewpagerindicator.CirclePageIndicator;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGame;
import com.yowzapp.playstore.activty.CreateTeamActivity;
import com.yowzapp.playstore.model.HomeImageModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.LoopViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

/**
 * Created by vaishakha on 15/11/16.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    public static FloatingActionButton fabAdd;
    public static RelativeLayout relativeLayout;
    public static CoordinatorLayout coordinatorLayout;
    TextView mGamesSelected, mGames, mTeams, mTeamSelected, mTopLoc, mTopLocSelected, mActivityLog, mActivitySelected, mCreateTeam, mCreateGame;
    LoopViewPager collapsingViewPager;
    ViewPager tabViewPager;
    View contentView;
    CirclePageIndicator indicator;
    TabAdapter tabAdapter;
    //NestedScrollView nestedScrollView;
    Gson gson;
    ArrayList<HomeImageModel> imageList;
    ImageView removeDialog;
    RelativeLayout createTeamBox, createGameBox;
    int page;
    Timer timer;
    String response;
    PreferenceManager mPref;
    ProgressBar progressBar;
    int value;
    boolean pos;

    public HomeFragment() {

    }

    @SuppressLint("ValidFragment")
    public HomeFragment(int value) {
        this.value = value;
    }


    public static FloatingActionButton getFloatingActionButton() {
        return fabAdd;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (contentView == null) {
            contentView = inflater.inflate(R.layout.activity_main, container, false);
            mPref = PreferenceManager.instance(getActivity());
            initializeAllComponents();
            setupListeners();

            imageList = new ArrayList<HomeImageModel>();
            loadPhotos();

            tabAdapter = new TabAdapter(getChildFragmentManager());
            tabViewPager.setAdapter(tabAdapter);
            tabViewPager.setCurrentItem(value);
            tabViewPager.setOffscreenPageLimit(3);
            //nestedScrollView.setFillViewport(true);
            if (value == 0)
                selectGames();
            else if (value == 1)
                selectTeams();
            else if (value == 2)
                selectLocation();
            else if (value == 3)
                selectActivity();
        }


        collapsingViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               /* try {
                    timer.cancel();
                    pos = true;
                }catch (Exception e){
                    e.printStackTrace();
                }
                pageSwitcher();*/
                //Log.e("POSITIONN",position+"");
            }

            @Override
            public void onPageSelected(int position) {
                page = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        selectGames();
                        break;
                    case 1:
                        selectTeams();
                        break;
                    case 2:
                        selectLocation();
                        break;
                    case 3:
                        selectActivity();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return contentView;
    }

    @Override
    public void onDestroyView() {
        if(contentView.getParent()!=null) {
            ((ViewGroup) contentView.getParent()).removeView(contentView);
        }
        super.onDestroyView();
    }

    private void loadPhotos() {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                //progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                httpClient.get(Config.BANNER, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        //progressBar.setVisibility(View.GONE);
                        try {
                            String s = new String(responseBody);
                            Log.e("intrestList", s);
                            Log.e("RANK", "2");
                            JSONObject object = new JSONObject(s);

                            if (object.getString("status").equalsIgnoreCase("success")) {
                                JSONArray banners = object.getJSONArray("success");
                                gson = new Gson();

                                for (int i = 0; i < banners.length(); i++) {
                                    imageList.add(gson.fromJson(banners.get(i).toString(), HomeImageModel.class));
                                    Log.e("BANEERS", imageList.get(i).getImage());
                                }

                                if (!imageList.isEmpty()) {

                                    ImageAdapter adapter = new ImageAdapter(getActivity(), imageList);
                                    collapsingViewPager.setAdapter(adapter);
                                    collapsingViewPager.setCurrentItem(0);
                                    indicator.setViewPager(collapsingViewPager);
                                    indicator.setStrokeWidth(0);

                                    //------In OnResume
//                                    if (!imageList.isEmpty()) {
                                    pos = false;
                                    pageSwitcher();
                                    Log.e("ONSTOP", "onResume!!!!!");
//                                    }
//                                    onResume();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        //progressBar.setVisibility(View.GONE);
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                // progressBar.setVisibility(View.GONE);
            }
        }

    }

    public void pageSwitcher() {
        try {
            if(timer!=null) {
                timer.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, 5000); // delay
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("ONSTOP", "ONSTOP");
        try {
            timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("ONSTOP", "onResume");
    }

    private void selectGames() {
        mGames.setVisibility(View.GONE);
        mGamesSelected.setVisibility(View.VISIBLE);
        mTeams.setVisibility(View.VISIBLE);
        mTeamSelected.setVisibility(View.GONE);
        mTopLoc.setVisibility(View.VISIBLE);
        mTopLocSelected.setVisibility(View.GONE);
        mActivityLog.setVisibility(View.VISIBLE);
        mActivitySelected.setVisibility(View.GONE);
        fabAdd.setVisibility(View.VISIBLE);
    }

    private void selectTeams() {
        mGames.setVisibility(View.VISIBLE);
        mGamesSelected.setVisibility(View.GONE);
        mTeams.setVisibility(View.GONE);
        mTeamSelected.setVisibility(View.VISIBLE);
        mTopLoc.setVisibility(View.VISIBLE);
        mTopLocSelected.setVisibility(View.GONE);
        mActivityLog.setVisibility(View.VISIBLE);
        mActivitySelected.setVisibility(View.GONE);
        fabAdd.setVisibility(View.VISIBLE);
    }

    private void selectLocation() {
        mGames.setVisibility(View.VISIBLE);
        mGamesSelected.setVisibility(View.GONE);
        mTeams.setVisibility(View.VISIBLE);
        mTeamSelected.setVisibility(View.GONE);
        mTopLoc.setVisibility(View.GONE);
        mTopLocSelected.setVisibility(View.VISIBLE);
        mActivityLog.setVisibility(View.VISIBLE);
        mActivitySelected.setVisibility(View.GONE);
        fabAdd.setVisibility(View.VISIBLE);
    }

    private void selectActivity() {
        mGames.setVisibility(View.VISIBLE);
        mGamesSelected.setVisibility(View.GONE);
        mTeams.setVisibility(View.VISIBLE);
        mTeamSelected.setVisibility(View.GONE);
        mTopLoc.setVisibility(View.VISIBLE);
        mTopLocSelected.setVisibility(View.GONE);
        mActivityLog.setVisibility(View.GONE);
        mActivitySelected.setVisibility(View.VISIBLE);
        fabAdd.setVisibility(View.GONE);
    }

    private void setupListeners() {
        mGames.setOnClickListener(this);
        mTeams.setOnClickListener(this);
        mTopLoc.setOnClickListener(this);
        mActivityLog.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
    }

    private void initializeAllComponents() {
        mGames = contentView.findViewById(R.id.tv_games);
        mGamesSelected = contentView.findViewById(R.id.tv_games_selected);
        mTeams = contentView.findViewById(R.id.tv_my_team);
        mTeamSelected = contentView.findViewById(R.id.tv_my_team_selected);
        mTopLoc = contentView.findViewById(R.id.tv_top_loc);
        mTopLocSelected = contentView.findViewById(R.id.tv_top_loc_selected);
        mActivityLog = contentView.findViewById(R.id.tv_activity_log);
        mActivitySelected = contentView.findViewById(R.id.tv_activity_log_selected);
        collapsingViewPager = contentView.findViewById(R.id.collapse_viewpager);
        tabViewPager = contentView.findViewById(R.id.tab_viewpager);
        indicator = contentView.findViewById(R.id.circle_indicator);
        fabAdd = contentView.findViewById(R.id.fab_add);
        //nestedScrollView = (NestedScrollView) contentView.findViewById(R.id.nestedscroll);
        progressBar = contentView.findViewById(R.id.banner);
        relativeLayout = contentView.findViewById(R.id.home_relative);
        coordinatorLayout = contentView.findViewById(R.id.co);


        mGames.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mGamesSelected.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mTeams.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mTeamSelected.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mTopLoc.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mTopLocSelected.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mActivityLog.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
        mActivitySelected.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_top_loc:
                selectLocation();
                tabViewPager.setCurrentItem(2);
                break;

            case R.id.tv_games:
                selectGames();
                tabViewPager.setCurrentItem(0);
                break;

            case R.id.tv_my_team:
                selectTeams();
                tabViewPager.setCurrentItem(1);
                break;

            case R.id.tv_activity_log:
                selectActivity();
                tabViewPager.setCurrentItem(3);
                break;

            case R.id.fab_add:
                if (mPref.getAccessToken().isEmpty())
                    PlaythoraUtility.LoginDialog(getActivity());
                else showDialog();
        }

    }

    private void showDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.floating_button_dialog);

        Window window = dialog.getWindow();
//        window.setBackgroundDrawableResource(android.R.color.transparent);
//        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if (window != null) {
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.BOTTOM;
            wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
            //wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
        }

        mCreateGame = dialog.findViewById(R.id.tv_create_game);
        mCreateTeam = dialog.findViewById(R.id.tv_create_team);
        removeDialog = dialog.findViewById(R.id.cross_image);
        createGameBox = dialog.findViewById(R.id.relative_create_game);
        createTeamBox = dialog.findViewById(R.id.relative_create_team);

        mCreateGame.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        mCreateTeam.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        createGameBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateGame.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        createTeamBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateTeamActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        removeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            try {

                final int lastPosition = collapsingViewPager.getAdapter().getCount();
                if (page < lastPosition) {
                    if (!pos) {
                        pos = true;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                collapsingViewPager.setCurrentItem(0, true);
                            }
                        });
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                collapsingViewPager.setCurrentItem(page++, true);
                            }
                        });
                        pos = true;
                    }
                } else if (page == lastPosition) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            collapsingViewPager.setCurrentItem(0, true);
                        }
                    });
                    pos = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    if (getActivity() == null) {
                        return;
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private class ImageAdapter extends PagerAdapter {

        ArrayList<HomeImageModel> imageList;
        String imageUrl;
        Context context;

        ImageAdapter(FragmentActivity activity, ArrayList<HomeImageModel> imageList) {
            this.imageList = imageList;
            context = activity;
        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.image_view_layout, container, false);

            ImageView imageView = itemView.findViewById(R.id.images);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imageList.get(position).getUrl() != null &&
                            !imageList.get(position).getUrl().equals("") &&
                            !imageList.get(position).getUrl().equals(" ")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imageList.get(position).getUrl()));
                        getContext().startActivity(intent);
                    }

                }
            });

            if (PlaythoraUtility.checkInternetConnection(context)) {
                try {
                    imageUrl = imageList.get(position).getImage();
                    Glide.with(context).load(imageUrl).error(R.drawable.circled_user).centerCrop().into(imageView);
                    container.addView(itemView);
                } catch (Exception e) {
                    imageView.setImageResource(R.drawable.circled_user);
                }
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    private class TabAdapter extends FragmentPagerAdapter {
        int count = 4;

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new HomeGames();

                case 1:
                    return new HomeMyTeam();

                case 2:
                    return new HomeTopLocation();

                case 3:
                    return new HomeActivityLog();

                default:
                    return new HomeGames();

            }
        }

        @Override
        public int getCount() {
            return count;
        }
    }
}
