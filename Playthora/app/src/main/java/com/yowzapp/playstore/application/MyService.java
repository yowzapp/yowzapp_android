package com.yowzapp.playstore.application;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by nakul on 05/02/17.
 */
public class MyService extends Service {

    public SocketMainClass sc;
    private BroadcastReceiver receiver;
    String text = "";

    @Override
    public IBinder onBind(final Intent intent) {
        return new LocalBinder<MyService>(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("SERVICE", "Service Created");
        IntentFilter filter = new IntentFilter();
        filter.addAction("SOME_ACTION");
        filter.addAction("SOME_OTHER_ACTION");

        //Network check receiver
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                //CONNECTION code here
                Toast.makeText(context, "CLICKED'", Toast.LENGTH_SHORT).show();
                Log.e("Received", "ON BROADCAST RECEIVER");
            }
        };
        registerReceiver(receiver, filter);
        sc.getInstance(MyService.this);
    }


    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        Log.e("SERVICE", "SService Started");
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        Log.e("SERVICE", "SERVICE ONBIND");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        //Destroy network check receiver
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
        Log.e("SERVICE", "SERVICE DESTROY");
        SocketMainClass.getSocket().disconnect();
        SocketMainClass.notifyMgr.cancelAll();
    }

//	public static boolean isNetworkConnected() {
//		return cm.getActiveNetworkInfo() != null;
//	}
}