package com.yowzapp.playstore.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;


/**
 * Created by hemanth on 23/11/16.
 */

public class ChatFragment extends Fragment implements View.OnClickListener {

    public ViewPager viewPager;
    public MyPagerAdapter myPagerAdapter;
    TextView teamChat, teamChatSelected, gameChat, gameChatSelected, playerChat, playerChatSelected;
    PreferenceManager mPref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_activity, container, false);
        mPref = PreferenceManager.instance(getActivity());
        viewPager = (ViewPager) view.findViewById(R.id.chatFragment);
        teamChat = (TextView) view.findViewById(R.id.teamChat);
        teamChatSelected = (TextView) view.findViewById(R.id.teamChatSelected);
        gameChat = (TextView) view.findViewById(R.id.gameChat);
        gameChatSelected = (TextView) view.findViewById(R.id.gameChatSelected);
        playerChat = (TextView) view.findViewById(R.id.playerChat);
        playerChatSelected = (TextView) view.findViewById(R.id.playerChatSelected);

        myPagerAdapter = new MyPagerAdapter(getChildFragmentManager());

        viewPager.setAdapter(myPagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        player();
                        break;
                    case 1:
                        team();
                        break;

                    case 2:
                        game();
                        break;

                    default:
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        teamChat.setOnClickListener(this);
        gameChat.setOnClickListener(this);
        playerChat.setOnClickListener(this);

        return view;
    }

    private void team() {
        teamChatSelected.setVisibility(View.VISIBLE);
        teamChat.setVisibility(View.GONE);
        gameChat.setVisibility(View.VISIBLE);
        gameChatSelected.setVisibility(View.GONE);
        playerChat.setVisibility(View.VISIBLE);
        playerChatSelected.setVisibility(View.GONE);
    }

    private void game() {
        gameChatSelected.setVisibility(View.VISIBLE);
        gameChat.setVisibility(View.GONE);
        teamChatSelected.setVisibility(View.GONE);
        teamChat.setVisibility(View.VISIBLE);
        playerChat.setVisibility(View.VISIBLE);
        playerChatSelected.setVisibility(View.GONE);
    }

    private void player() {
        teamChatSelected.setVisibility(View.GONE);
        teamChat.setVisibility(View.VISIBLE);
        gameChat.setVisibility(View.VISIBLE);
        gameChatSelected.setVisibility(View.GONE);
        playerChat.setVisibility(View.GONE);
        playerChatSelected.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playerChat:
                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(getActivity());
                } else {
                    player();
                    viewPager.setCurrentItem(0);
                }

                break;

            case R.id.teamChat:
                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(getActivity());
                } else {
                    team();
                    viewPager.setCurrentItem(1);
                }
                break;

            case R.id.gameChat:
                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(getActivity());
                } else {
                    game();
                    viewPager.setCurrentItem(2);
                }
                break;
        }


    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Player Chat", "Team Chat", "Game Chat"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new PlayerChatFragment();

                case 1:
                    return new TeamChatFragment();

                case 2:
                    return new GameChatFragment();

                default:
                    return new PlayerChatFragment();

            }

        }
    }


}
