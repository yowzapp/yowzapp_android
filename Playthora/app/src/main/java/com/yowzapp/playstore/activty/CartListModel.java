package com.yowzapp.playstore.activty;

/**
 * Created by nakul on 11/11/16.
 */
public class CartListModel {
    private String product;
    private String image;
    private String rewardXp;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRewardXp() {
        return rewardXp;
    }

    public void setRewardXp(String rewardXp) {
        this.rewardXp = rewardXp;
    }
}
