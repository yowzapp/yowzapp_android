package com.yowzapp.playstore.activty;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
}
