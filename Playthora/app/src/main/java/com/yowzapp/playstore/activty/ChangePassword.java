package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 15/2/17.
 */
public class ChangePassword extends BaseActivity {

    private static final String passPattern = "(^(?=.*\\d)[0-9a-zA-Z]{8,}$)";
    Toolbar toolbar;
    TextView title;
    PreferenceManager mPref;
    Button save;
    EditText oldPassword, newPassword, confirmPassword;
    TextInputLayout oldPasswordInputLayout, newPasswordInputLayout, confirmPasswordInputLayout;
    private Pattern pattern;
    private Matcher matcher;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_layout);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar = (Toolbar) findViewById(R.id.changePasswordToolbar);
        title = (TextView) findViewById(R.id.change_password_title);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        initialize();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForValidation()) {
                    updatePassword();
                }
            }
        });

        oldPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                oldPasswordInputLayout.setError(null);
            }
        });

        newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                newPasswordInputLayout.setError(null);
            }
        });

        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                confirmPasswordInputLayout.setError(null);
            }
        });


    }

    private void updatePassword() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(ChangePassword.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("oldPassword", oldPassword.getText().toString());
            params.add("newPassword", newPassword.getText().toString());

            httpClient.post(Config.CHANGE_PASSWORD, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("changePasswordSuccess", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(ChangePassword.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ChangePassword.this, AppSettingsActivity.class);
                            intent.putExtra("setting", "change");
                            startActivity(intent);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(ChangePassword.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ChangePassword.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(ChangePassword.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);
                            Toast.makeText(ChangePassword.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(ChangePassword.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private boolean checkForValidation() {

        if (oldPassword.getText().toString().trim().isEmpty() || oldPassword.getText().toString().contains(" ")) {
            oldPasswordInputLayout.setError("Old password is required");
            oldPasswordInputLayout.setErrorEnabled(true);
        } else {
            oldPasswordInputLayout.setErrorEnabled(false);
        }

        Log.e("oldPassword", oldPassword.getText().toString());

        if (newPassword.getText().toString().length() <= 7) {
            newPasswordInputLayout.setError("password should be atleast 8 characters");
            newPasswordInputLayout.setErrorEnabled(true);
        } else if (!(validate(newPassword.getText().toString()))) {
            newPasswordInputLayout.setError("Psst, your password should contain a numerical");
            newPasswordInputLayout.setErrorEnabled(true);
        } else {
            newPasswordInputLayout.setErrorEnabled(false);
        }
        Log.e("newPassword", newPassword.getText().toString());

        if (confirmPassword.getText().toString().trim().isEmpty() || confirmPassword.getText().toString().contains(" ")) {
            confirmPasswordInputLayout.setError("Confirm password is required");
            confirmPasswordInputLayout.setErrorEnabled(true);
        } else {
            confirmPasswordInputLayout.setErrorEnabled(false);
        }

        if (!(newPassword.getText().toString().equals(confirmPassword.getText().toString()))) {
            confirmPasswordInputLayout.setError("Password does not match");
            confirmPasswordInputLayout.setErrorEnabled(true);
        } else {
            confirmPasswordInputLayout.setErrorEnabled(false);
        }

        Log.e("confirmPassword", confirmPassword.getText().toString());

        if (oldPasswordInputLayout.isErrorEnabled() || newPasswordInputLayout.isErrorEnabled() || confirmPasswordInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }


    public boolean validate(final String password) {
        Log.e("pass", "" + password);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    private void initialize() {
        mPref = PreferenceManager.instance(getApplicationContext());
        pattern = Pattern.compile(passPattern);
        save = (Button) findViewById(R.id.save);
        oldPassword = (EditText) findViewById(R.id.oldPassword);
        newPassword = (EditText) findViewById(R.id.newPassword);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        oldPasswordInputLayout = (TextInputLayout) findViewById(R.id.input_layout_old_password);
        newPasswordInputLayout = (TextInputLayout) findViewById(R.id.input_layout_new_password);
        confirmPasswordInputLayout = (TextInputLayout) findViewById(R.id.input_layout_confirm_password);

        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        oldPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        newPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        confirmPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        oldPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        newPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        confirmPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }
}
