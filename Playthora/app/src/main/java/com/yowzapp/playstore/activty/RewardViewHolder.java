package com.yowzapp.playstore.activty;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yowzapp.playstore.R;


/**
 * Created by pramod on 3/8/16.
 */
public class RewardViewHolder extends RecyclerView.ViewHolder {


    public TextView productName, xp, categoryName, searchCategoryName, groundName, groundLocationName, kms;
    public ImageView imageview, categoryImage;
    public RelativeLayout rewardLayout, sportsDrawableLayout, gameLocationLayout;
    public LinearLayout lastItem;
    public TextView rank, personName, points, level, orderStatusText;
    public ImageView sportsLogo, orderImage, groundImage;
    public TextView sportsCategoryName, orderName, orderId, orderXp, orderDate, orderStatus;
    public View searchView;
    RatingBar ratingBar;

    public RewardViewHolder(View view) {
        super(view);

        this.imageview = (ImageView) view
                .findViewById(R.id.image);
        this.productName = (TextView) view
                .findViewById(R.id.title);
        this.xp = (TextView) view
                .findViewById(R.id.xp);
        this.categoryName = (TextView) view
                .findViewById(R.id.categoryName);
        this.categoryImage = (ImageView) view
                .findViewById(R.id.categoryImage);
        this.rewardLayout = (RelativeLayout) view
                .findViewById(R.id.rewardTrendingProductLayout);
        this.sportsLogo = (ImageView) view
                .findViewById(R.id.sportsLogo);
        this.rank = (TextView) view
                .findViewById(R.id.rank);
        this.level = (TextView) view
                .findViewById(R.id.level);
        this.personName = (TextView) view
                .findViewById(R.id.personName);
        this.points = (TextView) view
                .findViewById(R.id.points);
        this.lastItem = (LinearLayout) view
                .findViewById(R.id.myPorifleLeaderBoard);
        this.sportsCategoryName = (TextView) view
                .findViewById(R.id.leaderBoardChipCloud);
        this.sportsDrawableLayout = (RelativeLayout) view
                .findViewById(R.id.sportCatogories);
        this.orderImage = (ImageView) view
                .findViewById(R.id.orderImage);
        this.orderName = (TextView) view
                .findViewById(R.id.orderProductName);
        this.orderStatus = (TextView) view
                .findViewById(R.id.orderStatus);
        this.orderDate = (TextView) view
                .findViewById(R.id.orderDate);
        this.orderXp = (TextView) view
                .findViewById(R.id.orderXpValue);
        this.orderId = (TextView) view
                .findViewById(R.id.orderNumber);
        this.orderStatusText = (TextView) view
                .findViewById(R.id.orderStatusText);
        this.searchCategoryName = (TextView) view
                .findViewById(R.id.catogoryMembers);
        this.searchView = (View) view
                .findViewById(R.id.catogoryView);
        this.groundName = (TextView) view
                .findViewById(R.id.groundName);
        this.groundLocationName = (TextView) view
                .findViewById(R.id.placeName);
        this.kms = (TextView) view
                .findViewById(R.id.kms);
        this.gameLocationLayout = (RelativeLayout) view
                .findViewById(R.id.gameLayoutCreateGame);
        this.groundImage = (ImageView) view
                .findViewById(R.id.selectLocationImage);
        this.ratingBar = (RatingBar) view
                .findViewById(R.id.rating);

    }


}
