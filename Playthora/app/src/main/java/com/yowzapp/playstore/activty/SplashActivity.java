package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PreferenceManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Nakul on 8/10/2016.
 */
public class SplashActivity extends Activity {

    PreferenceManager mpref;

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.yowzapp.playstore", PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        mpref = PreferenceManager.instance(getApplicationContext());
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();

        //showHashKey(SplashActivity.this);
//        try {
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        Thread timeThread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Log.e("token", mpref.getAccessToken());

                    Intent bundleIntent = getIntent();

                    if (bundleIntent != null && bundleIntent.hasExtra("notification_type")) {
                        Intent intent;
                        try {
                            if (bundleIntent.getStringExtra("notification_type").equalsIgnoreCase("2")) {
                                intent = new Intent(SplashActivity.this, ChattingActivity.class);
                            } else if (bundleIntent.getStringExtra("notification_type").equalsIgnoreCase("4") ||
                                    bundleIntent.getStringExtra("notification_type").equalsIgnoreCase("6")) {
                                intent = new Intent(SplashActivity.this, GameMain.class);
                            } else if (bundleIntent.getStringExtra("notification_type").equalsIgnoreCase("7")) {
                                intent = new Intent(SplashActivity.this, OtherTeamViewActivity.class);
                            } else if (bundleIntent.getStringExtra("notification_type").equalsIgnoreCase("8")) {
                                intent = new Intent(SplashActivity.this, PlayerProfileViewActivity.class);
                            } else if (bundleIntent.getStringExtra("notification_type").equalsIgnoreCase("10")) {
                                intent = new Intent(SplashActivity.this, CreateGameSelectLocation.class);
                                intent.putExtra("ACTIVITY", "NOTIFIATION");
                            } else {
                                intent = new Intent(SplashActivity.this, NotificationActivity.class);
                            }
                            intent.putExtras(bundleIntent);
                            startActivity(intent);
                            finish();
                        } catch (Exception e) {
                            intent = new Intent(SplashActivity.this, NotificationActivity.class);
                            intent.putExtras(bundleIntent);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        if (!mpref.getAccessToken().isEmpty()) {
                            Log.e("mobileVerified", mpref.getMobilVerified());
                            if (mpref.getMobilVerified().equals("true")) {
                                Intent intent = new Intent(SplashActivity.this, HomeScreenActivity.class);
                                startActivity(intent);
                                finish();
//                                } else if(mpref.getName().isEmpty()){
//                                    Intent home = new Intent(SplashActivity.this, ProfileSetUpActivity.class);
//                                    startActivity(home);
//                                    finish();
//                                }else if(mpref.getInterestUpdated().equalsIgnoreCase("false")) {
//                                    Intent intent = new  Intent(SplashActivity.this,InterestsActivity.class);
//                                    intent.putExtra("name",mpref.getName());
//                                    startActivity(intent);
//                                    finish();
//                                }else if(mpref.getLocationUpdated().equalsIgnoreCase("false")){
//                                    Intent intent = new  Intent(SplashActivity.this,LocationActivity.class);
//                                    startActivity(intent);
//                                    finish();
                            } else {
                                Intent intent = new Intent(SplashActivity.this, SignupLoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Intent intent = new Intent(SplashActivity.this, OnBoardingActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }
        };
        timeThread.start();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4) {
            if (!mpref.getAccessToken().isEmpty()) {
                Log.e("mobileVerified", mpref.getMobilVerified());
                if (mpref.getMobilVerified().equals("false")) {
                    Intent intent = new Intent(SplashActivity.this, NumberActivity.class);
                    startActivity(intent);
                    finish();
//                } else if(mpref.getName().isEmpty()){
//                    Intent home = new Intent(SplashActivity.this, ProfileSetUpActivity.class);
//                    startActivity(home);
//                    finish();
//                }else if(mpref.getInterestUpdated().equalsIgnoreCase("false")) {
//                    Intent intent = new  Intent(SplashActivity.this,InterestsActivity.class);
//                    intent.putExtra("name",mpref.getName());
//                    startActivity(intent);
//                    finish();
//                }else if(mpref.getLocationUpdated().equalsIgnoreCase("false")){
//                    Intent intent = new  Intent(SplashActivity.this,LocationActivity.class);
//                    startActivity(intent);
//                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, HomeScreenActivity.class);
                    startActivity(intent);
                    finish();
                }

            } else {
                Intent intent = new Intent(SplashActivity.this, OnBoardingActivity.class);
                startActivity(intent);
                finish();
            }

        }
    }
}
