package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.AddmemberFreeAdapter;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by nakul on 28/12/16.
 */
public class AddMembersFree extends Fragment {

    public static AddmemberFreeAdapter adapter;
    static int j;
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    AddTeamMembersModel model;
    List<AddTeamMembersList> tempList;
    TextView invite;
    PreferenceManager mPref;
    ProgressBar progressBar;
    String url;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    NestedScrollView nested;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    public AddMembersFree(int i) {
        j = i;
        if (i == 0)
            url = Config.PLAYERS_FOLLOWING;
        else if (i == 1)
            url = Config.PLAYERS_FOLLOWER;
        else url = Config.PLAYERS_LIST;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.invite_player_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.invite_players_recycler);
        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested_player);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView.setNestedScrollingEnabled(false);

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    Log.e("RESPONSEPAGonLoadMoreE", String.valueOf(model.isHasMorePages()));
                    if (model.isHasMorePages()) {
                        Log.e("page_DONE", page + "");
                        pages = page;
                        try {
                            followingRecycler(pages);
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);

        try {
            followingRecycler(pages);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

        return contentView;
    }

    private void followingRecycler(int pages) {
        Log.e("PAGE", pages + "");

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(url + "&page=" + pages,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE_OTHER", s);

                                    model = new AddTeamMembersModel();
                                    mGson = new Gson();
                                    model = mGson.fromJson(s, AddTeamMembersModel.class);
                                    Log.e("RESPONSE", String.valueOf(model.getSuccess().getData().size()));

                                    if (model.getSuccess().getData().size() != 0) {
                                        Log.e("RESPONSE", String.valueOf(model.getSuccess().getData().size()));

                                        if (model.getCurrentPage() > 1) {
                                            for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                                tempList.add(model.getSuccess().getData().get(i));
                                            }

                                            Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                            adapter.RefreshPagination(tempList, true);
                                        } else {
                                            tempList = model.getSuccess().getData();
                                            Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                            adapter = new AddmemberFreeAdapter(getActivity(), model.getSuccess().getData());
                                            recyclerView.setAdapter(adapter);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);

            }
        } else {

//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }
}
