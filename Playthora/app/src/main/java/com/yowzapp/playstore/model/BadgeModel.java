package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 19/1/17.
 */

public class BadgeModel {
    private String status;
    private int statusCode;
    private List<BadgeSuccess> success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<BadgeSuccess> getSuccess() {
        return success;
    }

    public void setSuccess(List<BadgeSuccess> success) {
        this.success = success;
    }
}
