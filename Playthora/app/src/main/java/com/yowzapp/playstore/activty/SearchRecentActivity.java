package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.SearchPlayerAdapter;
import com.yowzapp.playstore.adapter.TeamSearchAdapter;
import com.yowzapp.playstore.model.TeamSearchModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SearchRecentActivity extends Fragment {
    //  public static RecyclerViewAdapter adapter;
    // public static RecyclerViewCategoriesAdapter categoriesAdapter;
    public static ArrayList<RewardModel> categoryArrayList;
    Gson mGson;
    ProgressDialog dialog;
    TextView search;
    TextView recent;
    View contentView;
    Gson gson;
    PreferenceManager mPref;
    private RecyclerView recyclerView, recyclerViewteams, recyclerViewsearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        contentView = inflater.inflate(R.layout.activity_search_recent, container, false);
        initialize();
        mPref = PreferenceManager.instance(getActivity());
        try {
            populateRecyclerViewteams();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            populateRecyclerViewplayers();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mPref.getAccessToken().isEmpty())
            populateSearch();

        return contentView;
    }


    private void populateSearch() {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                //progressBar.setVisibility(View.VISIBLE);
                //.setVisibility(View.GONE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SEARCH_RECENT,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                //progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("SEARCH_RESPONSE", s);
                                   /* Response = s;
                                    model = new SearchAllModel();
                                    mGson = new Gson();
                                    model = mGson.fromJson(s, SearchAllModel.class);*/
                                    //Log.e("RESPONSE", String.valueOf(model.getSuccess().size()));


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // progressBar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                //  progressBar.setVisibility(View.GONE);

                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    }//else Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                //progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }


    private void populateRecyclerViewplayers() {

        gson = new Gson();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONArray jsonArray = new JSONArray();
                            ArrayList arrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                arrayList.add(gson.fromJson(jsonArray.get(i).toString(), TeamMembersModel.class));
                                SearchPlayerAdapter searchAdapter = new SearchPlayerAdapter(getActivity(), arrayList);
                                recyclerView.setAdapter(searchAdapter);


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        thread.start();


    }

    private void populateRecyclerViewteams() {
        gson = new Gson();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONArray jsonArray = new JSONArray();
                            ArrayList arrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                arrayList.add(gson.fromJson(jsonArray.get(i).toString(), TeamSearchModel.class));
                                TeamSearchAdapter teamAdapter = new TeamSearchAdapter(getActivity(), arrayList);
                                recyclerViewteams.setAdapter(teamAdapter);


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        thread.start();
    }

    private void initialize() {
        recent = (TextView) contentView.findViewById(R.id.recent_text);
        search = (TextView) contentView.findViewById(R.id.edit_search);

        recyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_view_horizontal_players);
        recyclerViewteams = (RecyclerView) contentView.findViewById(R.id.recycler_view_horizontal_teams);


        recent.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        //  availableForYourPOints.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        search.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
//        whatHot.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        recyclerView.setHasFixedSize(true);
        recyclerViewteams.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewteams.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(getActivity(), MainSearchActivity.class);
                startActivity(searchIntent);
            }
        });
    }
}
