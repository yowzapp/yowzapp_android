package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.TeamDetailSportsModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import id.zelory.compressor.Compressor;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 23/11/16.
 */
public class EditTeamActivity extends BaseActivity {
    private static final int PICK_FROM_CAM = 1;
    public static ArrayList<AddTeamMembersList> addMemberList;
    public static ArrayList<AddTeamMembersList> arrayList;
    public static boolean success = false;
    Toolbar toolbar;
    TextView toolbarTitle, teamMember, teamSports, dialogTitle, dialogStatus,
            dialogCancel, dialogDelete, dialogRefundPolicy;
    Button mAdd, mSave;
    EditText teamName, teamStatus;
    RecyclerView memberRecyclerView, sportsRecyclerView;
    TeamMemberAdapter adapter;
    TeamSportsAdapter sportsAdapter;
    Gson gson;
    String mImage, mTeamName, memberList, sportsList;
    ArrayList<TeamDetailSportsModel> arrayListSports, adapterListsports;
    ImageView image;
    String ImagePath, imageURL, teamDisp;
    ProgressBar imageProgressBar;
    PreferenceManager mPref;
    int serverResponseCode = 0, id;
    TextInputLayout nameBoxInputLayout;
    boolean uploading = true;
    ImageView cameraImage;
    ImageView addMore;
    private int PICK_IMAGE_REQUEST = 2;
    private File output;
    private String selectedImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_team_layout);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        success = false;

        mPref = PreferenceManager.instance(this);
        initialize();
        setFont();

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Edit Team");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        arrayList = new ArrayList<>();
        addMemberList = new ArrayList<>();
        adapterListsports = new ArrayList<>();

        Intent i = getIntent();
        mImage = i.getStringExtra("IMAGEPATH");
        mTeamName = i.getStringExtra("TEAMNAME");
        memberList = i.getStringExtra("MEMBERS_LIST");
        Log.e("MEMBER_LIST", memberList);
        sportsList = i.getStringExtra("SPORTS_LIST");
        teamDisp = i.getStringExtra("TEAM_DISP");
        id = i.getIntExtra("TEAM_ID", 0);

        teamName.setText(mTeamName);
        teamStatus.setText(teamDisp);

        try {
            Glide.with(getApplicationContext()).load(mImage).error(R.drawable.no_team).centerCrop().into(image);
        } catch (Exception e) {
            image.setImageResource(R.drawable.no_team);
        }

        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EditTeamActivity.this, AddMembersActivity.class);
                i.putExtra("TEMPLIST", "EDIT");
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("mylist", arrayList);
                i.putExtras(bundle);
                startActivityForResult(i, 3);
            }
        });

        memberRecyclerView.setHasFixedSize(true);
        memberRecyclerView.setLayoutManager(new LinearLayoutManager(EditTeamActivity.this, LinearLayoutManager.HORIZONTAL, false));
        sportsRecyclerView.setHasFixedSize(true);
        sportsRecyclerView.setLayoutManager(new LinearLayoutManager(EditTeamActivity.this, LinearLayoutManager.HORIZONTAL, false));

        arrayListSports = new ArrayList<TeamDetailSportsModel>();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shouldAskPermission()) {
                    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                    int permsRequestCode = 200;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    CoverPhoto();
                }
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONArray jsonArraySports = new JSONArray();
                JSONArray jsonArrayMembers = new JSONArray();
                if (checkForAllValidation()) {
                    if (!uploading)
                        Toast.makeText(EditTeamActivity.this, "Image loading", Toast.LENGTH_SHORT).show();
                    else if (arrayList.isEmpty()) {
                        Toast.makeText(EditTeamActivity.this, "Add members", Toast.LENGTH_SHORT).show();
                    } else if (adapterListsports.size() <= 1) {
                        Toast.makeText(EditTeamActivity.this, "Add sports", Toast.LENGTH_SHORT).show();
                    } else {

                        for (int i = 0; i < adapterListsports.size() - 1; i++) {
                            jsonArraySports.put(adapterListsports.get(i).getId());
                            Log.e("SPORTSNAME", adapterListsports.get(i).getName());
                        }
                        for (int i = 0; i < arrayList.size(); i++) {
                            jsonArrayMembers.put(arrayList.get(i).getId());
                        }
                        Log.e("MEMBERIDS", jsonArrayMembers.toString());
                        Log.e("SPORTIDS", jsonArraySports.toString());
                        Log.e("SPORTSSIZE", String.valueOf(adapterListsports.size()));

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("members", jsonArrayMembers);
                            jsonObject.put("team_id", id);
                            jsonObject.put("sports", jsonArraySports);
                            jsonObject.put("name", teamName.getText().toString().trim());
                            jsonObject.put("cover_pic", mImage);
                            jsonObject.put("discription", teamStatus.getText().toString().trim());
                            Log.e("JSON", jsonObject.toString());
                            EditTeam(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EditTeamActivity.this, AddTeamMember.class);
                i.putExtra("TEAM_ID", id);
                startActivityForResult(i, 5);
            }
        });

        teamName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                nameBoxInputLayout.setError(null);
            }
        });

        loadMember();

        loadSports();

    }

    private void EditTeam(JSONObject jsonObject) {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(EditTeamActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("JSONOBJ", se.toString());
                mHttpClient.post(getApplicationContext(), Config.EDIT_TEAM, se, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                try {
                                    success = true;
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Intent i = getIntent();
                                        setResult(3, i);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private boolean checkForAllValidation() {
        if (teamName.getText().toString().isEmpty()) {
            nameBoxInputLayout.setError("Please provide team name");
            nameBoxInputLayout.setErrorEnabled(true);
        } else {
            nameBoxInputLayout.setErrorEnabled(false);
        }

//        if (mImage.isEmpty()) {
//            Toast.makeText(EditTeamActivity.this, "Please add image", Toast.LENGTH_SHORT).show();
//            return false;
//        }
       /* if(description.getText().toString().isEmpty() || description.getText().toString().trim().isEmpty()){
            descriptionBoxInputLayout.setError("Please provide the description of team");
            descriptionBoxInputLayout.setErrorEnabled(true);
        }else {
            descriptionBoxInputLayout.setErrorEnabled(false);
        }*/

        if (nameBoxInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }


    }

    private void CoverPhoto() {

        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditTeamActivity.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditTeamActivity.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(EditTeamActivity.this,
                                    getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }


    private boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if (permsRequestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm", "request");
                CoverPhoto();
            } else {
                Toast.makeText(EditTeamActivity.this, "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }


    private void loadMember() {

        try {
            JSONArray gsmesList = new JSONArray(memberList);
            gson = new Gson();
            for (int i = 0; i < gsmesList.length(); i++) {
                arrayList.add(gson.fromJson(gsmesList.get(i).toString(), AddTeamMembersList.class));
            }
            if (arrayList!=null && arrayList.size() > 0) {
                adapter = new TeamMemberAdapter(EditTeamActivity.this, arrayList);
                memberRecyclerView.setAdapter(adapter);
            }

            for (int j = 0; j < arrayList.size(); j++) {
                Log.e("IDDD", mPref.getId() + " " + arrayList.get(j).getId());
                //if(!mPref.getId().equalsIgnoreCase(String.valueOf(arrayList.get(j).getId()))){
                addMemberList.add(arrayList.get(j));
                //}
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void loadSports() {

        try {
            JSONArray gsmesList = new JSONArray(sportsList);
            gson = new Gson();
            for (int i = 0; i < gsmesList.length(); i++) {
                arrayListSports.add(gson.fromJson(gsmesList.get(i).toString(), TeamDetailSportsModel.class));
            }
            Log.e("SPORTS", arrayListSports.toString());
            sportsAdapter = new TeamSportsAdapter(EditTeamActivity.this, arrayListSports);
            sportsRecyclerView.setAdapter(sportsAdapter);

        } catch (JSONException e) {
            Log.e("SPORTS", "SPORTS");
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_team_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.delete_team) {
            deleteDialog();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (data == null) {
//            Log.e("DATA", "DATA");
//            return;
//        }

        Log.e("DATA", "requestCode:" + requestCode + "resultCode:" + resultCode);
        try {
            if (requestCode == 3) {
                //Log.e("#######", AddMembersActivity.stringList.size() + "");

                arrayList = new ArrayList<>();

                Bundle bundle = data.getExtras();
                arrayList = bundle.getParcelableArrayList("mylist");
                if (adapter != null) {
                    adapter.refresh(arrayList);
                }
            }
            if (requestCode == 6) {
                if (sportsAdapter != null) {
                    sportsAdapter.refresh(FavouriteSportsActivity.sportslist);
                }
            }

            if (requestCode == 5) {
                Log.e("INT", "INT");
                if (adapter != null) {
                    ArrayList<Integer> temp;
                    Bundle bundle = data.getExtras();
                    temp = bundle.getIntegerArrayList("NEWLIST");
                    for (int j = 0; j < temp.size(); j++) {
                        for (int k = 0; k < arrayList.size(); k++) {
                            if (temp.get(j) == arrayList.get(k).getId()) {
                                Log.e("LIstID", arrayList.get(k).getId() + "");
                                arrayList.remove(k);
                            }
                        }
                    }
                    adapter.refresh(arrayList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                try {
                    ImagePath = getPath(selectedImageUri);
                } catch (NullPointerException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {
                    ExifInterface exif = new ExifInterface(ImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }
                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(ImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
//                    image.setImageBitmap(bm);
                    imageProgressBar.setVisibility(View.VISIBLE);
                    cameraImage.setVisibility(View.GONE);
                    uploading = false;
                    image.setClickable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(ImagePath);
                            } catch (Exception e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        image.setClickable(true);
                                        cameraImage.setVisibility(View.VISIBLE);
                                        imageProgressBar.setVisibility(View.GONE);
                                        Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                                        uploading = true;
                                    }
                                });
                            }

                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {

                selectedImage = getRealPathFromURI(Uri.fromFile(output).toString(), EditTeamActivity.this);
                Log.e("selected Image", selectedImage);

                File myFile = new File(selectedImage);
                try {
                    myFile = new Compressor(this).compressToFile(myFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedImage = myFile.getAbsolutePath();

                Glide.with(EditTeamActivity.this).load(selectedImage).into(image);

                try{
//                Log.e("thumbnail", data.getExtras().get("data").toString());
//                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
//
//                final Uri mImageCaptureUri = Uri.fromFile(destination);
//
//                System.out.println("Image Path : " + mImageCaptureUri);
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                ImagePath = getRealPathFromURI(mImageCaptureUri.toString(), EditTeamActivity.this);
//                try {
//                    ExifInterface exif = new ExifInterface(ImagePath);
//                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//                    Log.e("ROTATE", orientation + "");
//                    Bitmap bm;
//                    int rotation = 0;
//                    if (orientation == 6) {
//                        rotation = 90;
//                    }
//                    if (orientation == 3) {
//                        rotation = 180;
//                    }
//                    if (orientation == 8) {
//                        rotation = 270;
//                    }
//                    if (orientation == 4) {
//                        rotation = 180;
//                    }
//                    Log.e("ooooooo", String.valueOf(currentapiVersion));
//                    Matrix matrix = new Matrix();
//                    matrix.postRotate(rotation);
//                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(ImagePath), 100, 100, true);
//                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
//                    //image.setImageBitmap(bm);
                    imageProgressBar.setVisibility(View.VISIBLE);
                    cameraImage.setVisibility(View.GONE);
                    uploading = false;
                    image.setClickable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
//                                uploadFile(ImagePath);
                                uploadFile(selectedImage);
                            } catch (Exception e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        image.setClickable(true);
                                        cameraImage.setVisibility(View.VISIBLE);
                                        imageProgressBar.setVisibility(View.GONE);
                                        Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                                        uploading = true;
                                    }
                                });
                            }

                        }
                    }).start();

//                } catch (IOException e) {
//                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            }
        }


    }

    private void uploadFile(final String sourceFileUri) {

        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File sourceFile = new File(PlaythoraUtility.compressImage(sourceFileUri, EditTeamActivity.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :" + sourceFileUri);

            runOnUiThread(new Runnable() {
                public void run() {
                    image.setClickable(true);
                    cameraImage.setVisibility(View.VISIBLE);
                    imageProgressBar.setVisibility(View.GONE);
                    Toast.makeText(EditTeamActivity.this, "Source File not exist :" + sourceFileUri, Toast.LENGTH_SHORT).show();
                    uploading = true;
                }
            });

            return;

        } else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.UPLOAD_IMAGE);
                PlaythoraUtility.trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                        + fileName + "\"" + lineEnd); //image is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS", "Content-Disposition: form-data; name=\"profile_pic\";filename=\"" + fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            image.setClickable(true);
                            uploading = true;
                            JSONObject response = null;
                            try {
                                response = new JSONObject(total.toString());
                                imageURL = response.getString("success");
                                mImage = response.getString("success");
                                Glide.with(EditTeamActivity.this).load(imageURL).error(R.drawable.no_team).centerCrop().into(image);
                                imageProgressBar.setVisibility(View.GONE);
                                cameraImage.setVisibility(View.VISIBLE);
                                //Toast.makeText(EditTeamActivity.this, "Upload image Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else if (serverResponseCode == 400 || serverResponseCode == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image.setClickable(true);
                            uploading = true;
                            imageProgressBar.setVisibility(View.GONE);
                            cameraImage.setVisibility(View.VISIBLE);
                        }
                    });
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    /*JSONObject object = null;
                    mPref.setAccessToken("");
                    Intent intent = new Intent(CreateTeamActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();*/

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image.setClickable(true);
                            uploading = true;
                            imageProgressBar.setVisibility(View.GONE);
                            cameraImage.setVisibility(View.VISIBLE);
                            Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                            //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        image.setClickable(true);
                        uploading = true;
                        imageProgressBar.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        image.setClickable(true);
                        uploading = true;
                        imageProgressBar.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            } catch (ProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        image.setClickable(true);
                        uploading = true;
                        imageProgressBar.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        image.setClickable(true);
                        uploading = true;
                        imageProgressBar.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        Toast.makeText(EditTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            }
        }

    }

    private String getRealPathFromURI(String s, Context context) {
        Uri contentUri = Uri.parse(s);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private String getPath(Uri selectedImageUri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }

    private void deleteDialog() {
        final Dialog dialog = new Dialog(EditTeamActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_team_dialog);

        dialogTitle = (TextView) dialog.findViewById(R.id.delete_title);
        dialogStatus = (TextView) dialog.findViewById(R.id.delete_confirm);
        dialogCancel = (TextView) dialog.findViewById(R.id.cancel_text);
        dialogDelete = (TextView) dialog.findViewById(R.id.delete_text);
        dialogRefundPolicy = (TextView) dialog.findViewById(R.id.refund_policy);
        dialogRefundPolicy.setVisibility(View.GONE);

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogDelete.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                deleteTeam();
            }
        });

        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void deleteTeam() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(EditTeamActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("team_id", String.valueOf(id));
                Log.e("PARAMMS", String.valueOf(params));
                mHttpClient.post(Config.DELETE_TEAM, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSETEAMDETAILS", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(EditTeamActivity.this, HomeScreenActivity.class);
                                        startActivity(i);
                                        finishAffinity();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();

                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private void setFont() {
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        teamName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        teamStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        teamMember.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        teamSports.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mAdd.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mSave.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        nameBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    private void initialize() {
        toolbar = (Toolbar) findViewById(R.id.editTeamToolbar);
        toolbarTitle = (TextView) findViewById(R.id.create_team_toolbar_title);
        teamName = (EditText) findViewById(R.id.edit_team_name);
        teamStatus = (EditText) findViewById(R.id.edit_team_status);
        teamMember = (TextView) findViewById(R.id.edit_team_member);
        teamSports = (TextView) findViewById(R.id.edit_team_sports);
        mAdd = (Button) findViewById(R.id.edit_team_add_btn);
        mSave = (Button) findViewById(R.id.btn_edit_team);
        memberRecyclerView = (RecyclerView) findViewById(R.id.team_member_recycler);
        sportsRecyclerView = (RecyclerView) findViewById(R.id.team_sport_recycler);
        image = (ImageView) findViewById(R.id.edit_team_img);
        imageProgressBar = (ProgressBar) findViewById(R.id.progress_bar_edit);
        nameBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_edit_name);
        cameraImage = (ImageView) findViewById(R.id.cam_img_edit);
        addMore = (ImageView) findViewById(R.id.add_more);
    }

    public class TeamMemberAdapter extends RecyclerView.Adapter<TeamMemberAdapter.TeamMemberList> {
        Context context;
        ArrayList<AddTeamMembersList> model;

        public TeamMemberAdapter(EditTeamActivity activity, ArrayList<AddTeamMembersList> model) {
            context = activity;
            this.model = model;
        }

        public void refresh(ArrayList<AddTeamMembersList> usersArrayList) {
            this.model = usersArrayList;
            notifyDataSetChanged();
        }

        @Override
        public TeamMemberList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_team_member_item, null);
            return new TeamMemberList(layoutView);
        }

        @Override
        public void onBindViewHolder(TeamMemberList holder, final int position) {
            try {
                Glide.with(context).load(model.get(position).getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(holder.mImage);
            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.circled_user);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, PlayerProfileViewActivity.class);
                    i.putExtra("Id", model.get(position).getId() + "");
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class TeamMemberList extends RecyclerView.ViewHolder {
            ImageView mImage;

            public TeamMemberList(View itemView) {
                super(itemView);
                mImage = (ImageView) itemView.findViewById(R.id.imageView_team_member);
            }
        }
    }

    public class TeamSportsAdapter extends RecyclerView.Adapter<TeamSportsAdapter.SportsList> {
        Context context;
        String[] sports;

        public TeamSportsAdapter(EditTeamActivity activity, ArrayList<TeamDetailSportsModel> arrayList) {
            context = activity;
            adapterListsports = arrayList;
            adapterListsports.add(new TeamDetailSportsModel(""));
            sports = new String[arrayList.size()];
            for (int i = 0; i < arrayList.size(); i++) {
                sports[i] = arrayList.get(i).getName();
            }
        }

        @Override
        public SportsList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_items, null);
            return new SportsList(layoutView);
        }

        @Override
        public void onBindViewHolder(SportsList holder, int position) {
            holder.sports.setBackgroundResource(R.drawable.oval_back);
            holder.sports.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
            holder.sports.setTextColor(getResources().getColor(R.color.text_one));
            if (adapterListsports.get(position).getName().isEmpty()) {
                holder.addSports.setVisibility(View.VISIBLE);
                holder.sports.setVisibility(View.GONE);
            } else {
                holder.addSports.setVisibility(View.GONE);
                holder.sports.setVisibility(View.VISIBLE);
                holder.sports.setText(adapterListsports.get(position).getName());
            }
            Log.e("SPORTS", adapterListsports.get(position).getName());

            holder.addSports.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, FavouriteSportsActivity.class);
                    i.putExtra("SPORTS", sports);
                    ((Activity) context).startActivityForResult(i, 6);
                }
            });
        }

        @Override
        public int getItemCount() {
            return adapterListsports.size();
        }

        public void refresh(ArrayList<TeamDetailSportsModel> sportslist) {
            adapterListsports = sportslist;
            sports = new String[adapterListsports.size()];
            for (int i = 0; i < adapterListsports.size(); i++) {
                sports[i] = adapterListsports.get(i).getName();
            }
            adapterListsports.add(new TeamDetailSportsModel(""));
            notifyDataSetChanged();
        }

        public class SportsList extends RecyclerView.ViewHolder {
            TextView sports;
            ImageView addSports;

            public SportsList(View itemView) {
                super(itemView);
                sports = (TextView) itemView.findViewById(R.id.text_games);
                addSports = (ImageView) itemView.findViewById(R.id.add_sports);
            }
        }
    }

}
