package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.model.TeamDetailMemberModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vaishakha on 22/11/16.
 */
public class TeamMembers extends Fragment {

    View contentView;
    RecyclerView recyclerView;
    TeamMemberRecyclerAdapter adapter;
    ArrayList<TeamDetailMemberModel> model;
    String response;
    Gson gson;
    TextView emptyText;

    public TeamMembers(ArrayList<TeamDetailMemberModel> memberModels) {
        model = memberModels;
        Log.v("model", String.valueOf(model));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.other_team_member_layout, container, false);
        recyclerView = (RecyclerView) contentView.findViewById(R.id.team_member_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        emptyText = (TextView) contentView.findViewById(R.id.emptyText);
        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        loadMember();

        return contentView;
    }

    private void loadMember() {
        if (model.size() != 0) {
            adapter = new TeamMemberRecyclerAdapter(getActivity(), model);
            recyclerView.setAdapter(adapter);
            emptyText.setVisibility(View.GONE);
        } else {
            recyclerView.setAdapter(null);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText("No members");
        }

    }

    public class TeamMemberRecyclerAdapter extends RecyclerView.Adapter<TeamMemberRecyclerAdapter.MemberList> {
        Context context;
        ArrayList<TeamDetailMemberModel> model;
        MemberList mainHolder;
        TeamDetailMemberModel data;

        public TeamMemberRecyclerAdapter(FragmentActivity activity, ArrayList<TeamDetailMemberModel> model) {
            context = activity;
            this.model = model;
        }

        @Override
        public MemberList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_team_member_item, null);
            return new MemberList(layoutView);
        }

        @Override
        public void onBindViewHolder(MemberList holder, final int position) {
            data = model.get(position);
            mainHolder = (MemberList) holder;

            try {
                mainHolder.tvCount.setText("/" + model.size());
                mainHolder.tvPosition.setText(1 + position + "");
                mainHolder.tvName.setText(data.getName());
                mainHolder.tvStatus.setText(data.getBio());
                mainHolder.tvLabel.setText(data.getLevel_name());
                Log.e("level_name",data.getLevel_name()+"");
                Log.e("location_name",data.getLocation_name()+"");
                Log.v("PROFILE_PIC",data.getProfile_pic()+"");
                if (data.getPrefer_location().isEmpty() || data.getPrefer_location().equals(""))
                    mainHolder.tvLocation.setText("Not available");
                else
                    mainHolder.tvLocation.setText(data.getPrefer_location());
                Log.e("locationmember",data.getPrefer_location());

                if (PlaythoraUtility.checkInternetConnection(getActivity())) {
                    if (data.getProfile_pic()!=null && !data.getProfile_pic().trim().isEmpty()) {
                        Glide.with(context).load(data.getProfile_pic()).error(R.drawable.circled_user).centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(holder.mImage);
                    }else {
                        holder.mImage.setImageResource(R.drawable.circled_user);
                    }
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, PlayerProfileViewActivity.class);
                        i.putExtra("Id", model.get(position).getId() + "");
                        context.startActivity(i);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class MemberList extends RecyclerView.ViewHolder {
            TextView tvCount, tvName, tvStatus, tvLabel, tvLocation, tvPosition;
            CircleImageView mImage;
            LinearLayout locationLayout;

            public MemberList(View itemView) {
                super(itemView);
                tvCount = (TextView) itemView.findViewById(R.id.member_count);
                tvName = (TextView) itemView.findViewById(R.id.member_name);
                tvStatus = (TextView) itemView.findViewById(R.id.member_status);
                tvLabel = (TextView) itemView.findViewById(R.id.player_status);
                tvLocation = (TextView) itemView.findViewById(R.id.member_location);
                mImage = (CircleImageView) itemView.findViewById(R.id.member_image);
                tvPosition = (TextView) itemView.findViewById(R.id.member_position);
                locationLayout = itemView.findViewById(R.id.location_layout);
                tvName.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_BOLD));
                tvStatus.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
                tvLabel.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_SEMIBOLD));
                tvLocation.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_SEMIBOLD));
            }
        }
    }
}
