package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.Serializable;

import com.google.gson.Gson;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.TeamGame;
import com.yowzapp.playstore.fragment.TeamMembers;
import com.yowzapp.playstore.fragment.TeamPhotos;
import com.yowzapp.playstore.fragment.TeamTimeLine;
import com.yowzapp.playstore.model.TeamDetailMemberModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by vaishakha on 22/11/16.
 */
public class OtherTeamActivity extends BaseActivity implements  View.OnClickListener {
    Toolbar toolbar;
    TextView members, memebersSelected, timeline, timelineSelected, games, gamesSelected, photos, photosSelected;
    ViewPager viewPager;
    OtherTeamAdapter adapter;
    String mTeamName = "", membersList, image = "";
    Gson gson;
    ArrayList<TeamDetailMemberModel> memberModels;
    int teamId;
    boolean isMember;
    PreferenceManager mPref;
    Toast toast;
    ImageView navigationBackButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_team_layout);
        mPref = PreferenceManager.instance(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        initialize();
        setFont();
        setupListeners();

        getSupportActionBar().setElevation(0);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbar_team_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_REGULAR));

        Intent i = getIntent();
        if (i.hasExtra("TEAMNAME")) {
            mTeamName = i.getStringExtra("TEAMNAME");
            image = i.getStringExtra("image");
            membersList = i.getStringExtra("MEMBERS_LIST");
            teamId = i.getIntExtra("TEAM_ID", 0);
            isMember = i.getBooleanExtra("isMember", false);
            toolbarTitle.setText(mTeamName);
            Log.e("membersList", membersList + "");
        }

        if (membersList != null) {
            gson = new Gson();
            JSONArray jsonArray;
            memberModels = new ArrayList<>();
            try {
                jsonArray = new JSONArray(membersList);
                for (int j = 0; j < jsonArray.length(); j++) {
                    memberModels.add(gson.fromJson(jsonArray.get(j).toString(), TeamDetailMemberModel.class));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter = new OtherTeamAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        setMember();
                        break;

                    case 1:
                        setTimeline();
                        break;

                    case 2:
                        setGame();
                        break;

                    case 3:
                        setPhoto();
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setupListeners() {
        members.setOnClickListener(this);
        timeline.setOnClickListener(this);
        games.setOnClickListener(this);
        photos.setOnClickListener(this);
        navigationBackButton.setOnClickListener(this);
    }

    private void setFont() {
        members.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        memebersSelected.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        timeline.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        timelineSelected.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        games.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        gamesSelected.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        photos.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
        photosSelected.setTypeface(PlaythoraUtility.getFont(OtherTeamActivity.this, Config.RALEWAY_SEMIBOLD));
    }

    private void initialize() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_team);
        members = (TextView) findViewById(R.id.members);
        memebersSelected = (TextView) findViewById(R.id.members_selected);
        timeline = (TextView) findViewById(R.id.timeline);
        timelineSelected = (TextView) findViewById(R.id.timeline_selected);
        games = (TextView) findViewById(R.id.games);
        gamesSelected = (TextView) findViewById(R.id.games_selected);
        photos = (TextView) findViewById(R.id.photos);
        photosSelected = (TextView) findViewById(R.id.photos_selected);
        viewPager = (ViewPager) findViewById(R.id.team_viewpager);
        navigationBackButton = (ImageView) findViewById(R.id.navigation_back_button);
        setSupportActionBar(toolbar);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.members:
                setMember();
                break;

            case R.id.timeline:
                setTimeline();
                break;

            case R.id.games:
                setGame();
                break;

            case R.id.photos:
                setPhoto();
                break;

            case R.id.navigation_back_button:
                onBackPressed();
                break;


        }

    }

    private void setMember() {
        members.setVisibility(View.GONE);
        memebersSelected.setVisibility(View.VISIBLE);
        timeline.setVisibility(View.VISIBLE);
        timelineSelected.setVisibility(View.GONE);
        games.setVisibility(View.VISIBLE);
        gamesSelected.setVisibility(View.GONE);
        photos.setVisibility(View.VISIBLE);
        photosSelected.setVisibility(View.GONE);
        viewPager.setCurrentItem(0);
    }

    private void setTimeline() {
        members.setVisibility(View.VISIBLE);
        memebersSelected.setVisibility(View.GONE);
        timeline.setVisibility(View.GONE);
        timelineSelected.setVisibility(View.VISIBLE);
        games.setVisibility(View.VISIBLE);
        gamesSelected.setVisibility(View.GONE);
        photos.setVisibility(View.VISIBLE);
        photosSelected.setVisibility(View.GONE);
        viewPager.setCurrentItem(1);
    }

    private void setGame() {
        members.setVisibility(View.VISIBLE);
        memebersSelected.setVisibility(View.GONE);
        timeline.setVisibility(View.VISIBLE);
        timelineSelected.setVisibility(View.GONE);
        games.setVisibility(View.GONE);
        gamesSelected.setVisibility(View.VISIBLE);
        photos.setVisibility(View.VISIBLE);
        photosSelected.setVisibility(View.GONE);
        viewPager.setCurrentItem(2);
    }

    private void setPhoto() {
        members.setVisibility(View.VISIBLE);
        memebersSelected.setVisibility(View.GONE);
        timeline.setVisibility(View.VISIBLE);
        timelineSelected.setVisibility(View.GONE);
        games.setVisibility(View.VISIBLE);
        gamesSelected.setVisibility(View.GONE);
        photos.setVisibility(View.GONE);
        photosSelected.setVisibility(View.VISIBLE);
        viewPager.setCurrentItem(3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.chat_notification) {
            if (mPref.getAccessToken().isEmpty()) {
                PlaythoraUtility.LoginDialog(OtherTeamActivity.this);
            } else {

                if (toast != null) {
                    toast.cancel();
                }
                if (isMember) {
                    Intent chat = new Intent(OtherTeamActivity.this, ChattingActivity.class);
                    chat.putExtra("name", mTeamName);
                    chat.putExtra("image", image);
                    chat.putExtra("id", teamId + "");
                    chat.putExtra("type", "team");
                    chat.putExtra("Conv_id", teamId);
                    chat.putExtra("Conv_type", 2);
                    startActivity(chat);
                } else {
                    toast = Toast.makeText(getApplicationContext(), "You should be member of this team to chat", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down_enter, R.anim.slide_down_exit);
    }

    private class OtherTeamAdapter extends FragmentPagerAdapter {
        int count = 4;

        public OtherTeamAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new TeamMembers(memberModels);

                case 1:
                    return new TeamTimeLine(teamId);

                case 2:
                    return new TeamGame(teamId, isMember, membersList);

                case 3:
                    return new TeamPhotos(teamId, isMember);

                default:
                    return new TeamMembers(memberModels);

            }
        }

        @Override
        public int getCount() {
            return count;
        }
    }
}
