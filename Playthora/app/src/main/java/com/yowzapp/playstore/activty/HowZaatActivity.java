package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 23/11/16.
 */

public class HowZaatActivity extends BaseActivity {

    Toolbar toolbar;
    TextView title, gameTitle, gameName, description, location, txtGameType, gameType, txtPlayers;
    Button btnMarkAttendence;
    ImageView more;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.how_zaat_layout);

        toolbar = (Toolbar) findViewById(R.id.howZaatToolBar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        InitializeAll();

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent attendence = new Intent(HowZaatActivity.this, AttendenceMarker.class);
                startActivity(attendence);
            }
        });
        btnMarkAttendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent attendence = new Intent(HowZaatActivity.this, AttendenceMarker.class);
                startActivity(attendence);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.how_zaat_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_chat) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void InitializeAll() {

        title = (TextView) findViewById(R.id.howzaat_toolbar_title);
        gameTitle = (TextView) findViewById(R.id.title);
        gameName = (TextView) findViewById(R.id.sportsName);
        description = (TextView) findViewById(R.id.sportsDescription);
        location = (TextView) findViewById(R.id.locationDetail);
        txtGameType = (TextView) findViewById(R.id.gameTypeText);
        gameType = (TextView) findViewById(R.id.gameType);
        txtPlayers = (TextView) findViewById(R.id.playersText);
        more = (ImageView) findViewById(R.id.imageMore);
        btnMarkAttendence = (Button) findViewById(R.id.mark_attendence_button);


        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        gameTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        gameName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        description.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        location.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        txtGameType.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        gameType.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        txtPlayers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        btnMarkAttendence.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }
}
