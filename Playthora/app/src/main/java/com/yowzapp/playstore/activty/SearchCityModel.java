package com.yowzapp.playstore.activty;

/**
 * Created by vaishakha on 14/11/16.
 */
public class SearchCityModel {
    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
