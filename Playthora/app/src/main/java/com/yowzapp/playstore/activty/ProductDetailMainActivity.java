package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

public class ProductDetailMainActivity extends BaseActivity {
    private static final Integer[] IMAGES = {R.drawable.tick_next, R.drawable.tick_next, R.drawable.tick_next, R.drawable.tick_next};
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Button addToCart;
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private TextView nameOfProduct, xpOfProduct, rupeesOfProduct, aboutText, aboutTextDescription, dimensionText, textview;
    private ImageView unFavouritedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail_main);
        init();

        aboutText = (TextView) findViewById(R.id.aboutThisProduct);
        aboutTextDescription = (TextView) findViewById(R.id.aboutProduct);
        dimensionText = (TextView) findViewById(R.id.dimensions);
        addToCart = (Button) findViewById(R.id.add_to_cart);
        nameOfProduct = (TextView) findViewById(R.id.title);
        xpOfProduct = (TextView) findViewById(R.id.xp);
        rupeesOfProduct = (TextView) findViewById(R.id.rupees);
        unFavouritedImage = (ImageView) findViewById(R.id.unfavouritedHeart);
        textview = (TextView) findViewById(R.id.id_text);

        nameOfProduct.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        xpOfProduct.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        rupeesOfProduct.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        addToCart.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        textview.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));


        aboutText.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        aboutTextDescription.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        dimensionText.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(ProductDetailMainActivity.this, RALEWAY_REGULAR));
        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(ProductDetailMainActivity.this, CartActivity.class);
                startActivity(login);
            }
        });


    }

    private void init() {
        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(ProductDetailMainActivity.this, ImagesArray));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        /*final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };*/
       /* Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*/
        // Pager listener over indicator


        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public class SlidingImage_Adapter extends PagerAdapter {


        private ArrayList<Integer> IMAGES;
        private LayoutInflater inflater;
        private Context context;


        public SlidingImage_Adapter(Context context, ArrayList<Integer> IMAGES) {
            this.context = context;
            this.IMAGES = IMAGES;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return IMAGES.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.product_detail_item, view, false);

            assert imageLayout != null;
            final ImageView imageView = (ImageView) imageLayout
                    .findViewById(R.id.imageProduct);


            imageView.setImageResource(IMAGES.get(position));

            view.addView(imageLayout, 0);

            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }


    }

}