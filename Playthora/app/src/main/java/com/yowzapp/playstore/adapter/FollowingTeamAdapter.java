package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.model.TeamSearchModel;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 24/11/16.
 */
public class FollowingTeamAdapter extends RecyclerView.Adapter<FollowingTeamAdapter.ProfileListHolder> {
    Context mContext;


    ArrayList<TeamSearchModel> playerList;

    public FollowingTeamAdapter(Context context, ArrayList<TeamSearchModel> arrayList) {
        mContext = context;
        playerList = arrayList;
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.followers_layout, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, int position) {
        try {
            Glide.with(mContext).load(playerList.get(position).getImage()).error(R.drawable.no_team).into(holder.teamImage);
        } catch (Exception e){
            holder.teamImage.setImageResource(R.drawable.no_team);
        }
        holder.teamName.setText(playerList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView teamImage;
        TextView teamName;
        RelativeLayout lytEditDeleteWrapper, alreadyExchanged;

        public ProfileListHolder(View itemView) {
            super(itemView);


            teamImage = (ImageView) itemView.findViewById(R.id.followers_user_image);

            teamName = (TextView) itemView.findViewById(R.id.userName_follower);


            teamName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));


        }
    }


}