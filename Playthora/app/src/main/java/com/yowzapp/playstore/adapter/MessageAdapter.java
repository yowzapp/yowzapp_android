package com.yowzapp.playstore.adapter;

/**
 * Created by nakul on 03/02/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.kevinsawicki.timeago.TimeAgo;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.model.Message;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.List;

import static com.yowzapp.playstore.utils.PlaythoraUtility.getFormattedDateDay;

/**
 * Created by nakul on 26/03/16.
 */


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    static int inputType = 0;
    String type;
    TimeAgo timeAgo = null;
    Context context;
    private List<Message> mMessages;
    private int[] mUsernameColors;


    public MessageAdapter(Context context, List<Message> messages, String type) {
        this.context = context;
        mMessages = messages;
        mUsernameColors = context.getResources().getIntArray(R.array.username_colors);
        timeAgo = new TimeAgo();
        this.type = type;
    }

    public void RefreshList(List<Message> messages, String type) {

        mMessages.addAll(messages);
        this.type = type;
        notifyDataSetChanged();
    }

//    private class MineHolder {
//        RelativeLayout lytChatBubbleWrapper;
//        RelativeLayout parent_layout;
//        TextView msg, txtMsgTime;
//        ImageView imgUserImage;
//    }
//
//    private class OtherHolder {
//        RelativeLayout lytChatBubbleWrapper;
//        RelativeLayout parent_layout;
//        TextView msg, txtMsgTime;
//        ImageView imgOtherImage;
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;

        switch (viewType) {
            case Message.TYPE_MESSAGE:
                layout = R.layout.item_message;
                break;
            case Message.TYPE_LOG:
                layout = R.layout.item_log;
                break;
            case Message.TYPE_DATE:
                layout = R.layout.item_date;
                break;
        }
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Message message = mMessages.get(position);
        if (Message.TYPE_DATE == message.getType())
            viewHolder.setDate(message.getDate());
        else {
            viewHolder.setMessage(message.getMessage());
            viewHolder.setUsername(message.getUsername());
            viewHolder.setName(message.getName());
        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).getType();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mUsernameView;
        private TextView mMessageView;
        private TextView mName;
//        private CircleImageView mUserImage;


        public ViewHolder(View itemView) {
            super(itemView);


            mUsernameView = (TextView) itemView.findViewById(R.id.username);
            mMessageView = (TextView) itemView.findViewById(R.id.messagee);
            mName = (TextView) itemView.findViewById(R.id.messagee_name);
//            mUserImage = itemView.findViewById(R.id.img_user_image);

            if (mUsernameView != null) {
                mUsernameView.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            }
            if (mMessageView != null) {
                mMessageView.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            }

            if (mName != null) {
                mName.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_SEMIBOLD));
            }

        }

        public void setUsername(String username) {
            if (null == mUsernameView) return;
            try {
                mUsernameView.setText(PlaythoraUtility.convertDateFormat(PlaythoraUtility.getUtctime(username), context));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }

        public void setName(String username) {
            if (mName == null) return;

            if (type!=null && (type.equalsIgnoreCase("team") || type.equalsIgnoreCase("game")
                    || type.equalsIgnoreCase("2") || type.equalsIgnoreCase("1"))) {
                try {
                    mName.setVisibility(View.VISIBLE);
                    mName.setText(username.trim());
                    mName.setTextColor(getUsernameColor(username));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

        }

        public void setMessage(String message) {
            if (null == mMessageView) return;
            mMessageView.setText(message);

        }

        public void setDate(String date) {
            if (null == mUsernameView) return;
            mUsernameView.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_SEMIBOLD));
            mUsernameView.setText(getFormattedDateDay(date));
        }

        private int getUsernameColor(String username) {
            int hash = 7;
            for (int i = 0, len = username.length(); i < len; i++) {
                hash = username.codePointAt(i) + (hash << 5) - hash;
            }
            int index = Math.abs(hash % mUsernameColors.length);
            return mUsernameColors[index];
        }
    }


//    private void setMineUI(MineHolder mineHolder, int position) {
//        //Glide.with(mContext).load(AppPreferences.instance(mContext).getUserImageUrl()).centerCrop().into(mineHolder.imgUserImage);
//        mineHolder.msg.setText(mMessages.get(position).getMessage());
//        // Log.e("NAKUL", chatMessageList.get(position).getCreated());
//        /*try {
//            mineHolder.txtMsgTime.setText(timeAgo.timeAgo(AppUtils.dateFormatnew.parse(chatMessageList.get(position).getCreated())));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        } catch (NullPointerException e){
//            e.printStackTrace();
//        }*/
//    }
//
//    private void setOtherUI(OtherHolder otherHolder, int position) {
//        //Glide.with(mContext).load(offerorAvatar).centerCrop().into(otherHolder.imgOtherImage);
//        otherHolder.msg.setText(mMessages.get(position).getMessage());
//
//        /*try {
//            otherHolder.txtMsgTime.setText(timeAgo.timeAgo(AppUtils.dateFormatnew.parse(chatMessageList.get(position).getCreated())));
//        } catch (ParseException e) {
//            e.printStackTrace();
//            Toast.makeText(mContext, "Date parse issue", Toast.LENGTH_SHORT).show();
//        } catch (NullPointerException e){
//            e.printStackTrace();
//            Toast.makeText(mContext, "Date issue", Toast.LENGTH_SHORT).show();
//        }*/
//    }
}