package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by nakul on 06/02/17.
 */
public class PlayerChatDetailModel {
    private List<PlayerChatObjectModel> success;
    private int currentPage;
    private boolean hasMorePages;

    public List<PlayerChatObjectModel> getSuccess() {
        return success;
    }

    public void setSuccess(List<PlayerChatObjectModel> success) {
        this.success = success;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }
}
