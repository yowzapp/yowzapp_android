package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.AddPlayerFollowers;
import com.yowzapp.playstore.fragment.AddPlayersFollowing;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 23/11/16.
 */
public class AddPlayersActivity extends BaseActivity implements View.OnClickListener {

    public static ViewPager viewPager;
    public static MyPagerAdapter myPagerAdapter;
    public static ArrayList<AddTeamMembersList> stringList;
    TextView following, followingSelected, followers, followersSelected, invite;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_players_main_layout);


        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        intializeAllComponents();
        setSupportActionBar(toolbar);

        stringList = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        stringList = bundle.getParcelableArrayList("mylist");
        Log.e("STRINGLIST", stringList.size() + "");

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(AddPlayersActivity.this, RALEWAY_REGULAR));
        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        following.setOnClickListener(this);
        followers.setOnClickListener(this);

        viewPager = (ViewPager) findViewById(R.id.customFragment);
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(myPagerAdapter);
        viewPager.setCurrentItem(0);
        //  viewPager.setPagingEnabled(false);

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent invi = new Intent(AddPlayersActivity.this, InviteFriendsActivity.class);
                startActivity(invi);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        playersFollowing();
                        if (AddPlayersFollowing.adapter != null)
                            AddPlayersFollowing.adapter.Refresh(stringList);
                        break;
                    case 1:
                        playersFollowers();
                        if (AddPlayerFollowers.adapter != null)
                            AddPlayerFollowers.adapter.Refresh(stringList);
                        break;
                    default:
                        playersFollowing();
                        if (AddPlayersFollowing.adapter != null)
                            AddPlayersFollowing.adapter.Refresh(stringList);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void playersFollowing() {

        followingSelected.setVisibility(View.VISIBLE);
        following.setVisibility(View.GONE);
        followersSelected.setVisibility(View.GONE);
        followers.setVisibility(View.VISIBLE);
    }

    private void playersFollowers() {

        followingSelected.setVisibility(View.GONE);
        following.setVisibility(View.VISIBLE);
        followersSelected.setVisibility(View.VISIBLE);
        followers.setVisibility(View.GONE);

    }

    private void intializeAllComponents() {
        toolbar = (Toolbar) findViewById(R.id.team_member_toolbar);
        following = (TextView) findViewById(R.id.following);
        followingSelected = (TextView) findViewById(R.id.followingSelected);
        followers = (TextView) findViewById(R.id.followers);
        followersSelected = (TextView) findViewById(R.id.followersSelected);
        invite = (TextView) findViewById(R.id.invitePlaythora);
        invite.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        followingSelected.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        following.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.following:
                playersFollowing();
                if (AddPlayersFollowing.adapter != null)
                    AddPlayersFollowing.adapter.Refresh(stringList);
                viewPager.setCurrentItem(0);

                break;

            case R.id.followers:
                playersFollowers();
                if (AddPlayerFollowers.adapter != null)
                    AddPlayerFollowers.adapter.Refresh(stringList);
                viewPager.setCurrentItem(1);

                break;

            default:
                break;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_players_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_user_selected) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("mylist", stringList);
            intent.putExtras(bundle);
            setResult(6, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Following", "Followers"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new AddPlayersFollowing();

                case 1:
                    return new AddPlayerFollowers();

                default:
                    return new AddPlayersFollowing();

            }

        }
    }
}
