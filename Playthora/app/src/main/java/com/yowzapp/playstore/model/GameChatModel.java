package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 23/11/16.
 */
public class GameChatModel {

    private String gameName;
    private String gameType;
    private String id;
    private String location;
    private String teamname;
    private String message;
    private String image;

    public GameChatModel(String gameName, String gameType, String id, String location, String teamname, String message, String image) {
        this.gameName = gameName;
        this.gameType = gameType;
        this.id = id;
        this.location = location;
        this.teamname = teamname;
        this.message = message;
        this.image = image;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTeamname() {
        return teamname;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
