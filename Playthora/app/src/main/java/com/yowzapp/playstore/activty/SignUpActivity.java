package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Nakul on 8/9/2016.
 */
public class SignUpActivity extends AppCompatActivity {

    private static final String passPattern = "(^(?=.*\\d)[0-9a-zA-Z]{8,}$)";
    TextView toolbarTitle, verify;
    EditText nameBox, emailBox, passwordBox, phoneNumberBox;
    TextInputLayout nameBoxInputLayout, emailBoxInputLayout, passwordBoxInputLayout, phoneNumberBoxInputLayout;
    ImageView eye;
    boolean isVisible = true;
    TextView change, looksGood, verifynum, verifySubText, number, doesLooksOk, signUpText;
    Button signUp;
    String phoneNum;
    String value = "true";
    ProgressBar progressBar;
    PreferenceManager mPref;
    int id;
    String mobile_verified, accToken, profile_pic_updated, interest_updated, location_updated, userName, type, userEmail;
    private Pattern pattern;
    private Matcher matcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        initializeAllComponents();


        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        //  passwordBox.setHint(Html.fromHtml("<big>" + getString(R.string.hint1)+" </big>" + "<small>" + getString(R.string.hint2)+ "<small>"));

        // passwordBox.setHint(Html.fromHtml("<html><body><font  size=\"6\" color="+ getResources().getColor(R.color.skip_color) + "> " + getString(R.string.hint1)+ "<small>" +  getString(R.string.hint2)+ "</small>" + "  </font> </body><html>"));

   /*     String s = "Password";
        String s1 = "(8";
        String s2 = " characters";
        String s3 = " with";
        String s4 = " atleast";
        String s5 = " a";
        String s6 = " number)";

        int textsize1 = getResources().getDimensionPixelSize(R.dimen.hint1);
        int textsize2 = getResources().getDimensionPixelSize(R.dimen.hint2);

        final SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString span1 = new SpannableString(s);
        span1.setSpan(new AbsoluteSizeSpan(textsize1), 0, s.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span1);

        SpannableString span2 = new SpannableString(s1);
        span2.setSpan(new AbsoluteSizeSpan(textsize2),0,s1.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s1.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span2);

        SpannableString span3 = new SpannableString(s2);
        span3.setSpan(new AbsoluteSizeSpan(textsize2),0,s2.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span3.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s2.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span3);

        SpannableString span4 = new SpannableString(s3);
        span4.setSpan(new AbsoluteSizeSpan(textsize2),0,s3.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span4.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s3.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span4);

        SpannableString span5 = new SpannableString(s4);
        span5.setSpan(new AbsoluteSizeSpan(textsize2),0,s4.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span5.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s4.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span5);

        SpannableString span6 = new SpannableString(s5);
        span6.setSpan(new AbsoluteSizeSpan(textsize2),0,s5.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span6.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s5.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span6);

        SpannableString span7 = new SpannableString(s6);
        span7.setSpan(new AbsoluteSizeSpan(textsize2),0,s6.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        span7.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.skip_color)),0,s6.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        builder.append(span7);

        passwordBox.setHint(builder);*/


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (value.equalsIgnoreCase("true")) {
                    if (checkForAllValidation()) {
                        phoneNum = phoneNumberBox.getText().toString();
                        Log.v("PHONENUMBER", phoneNum);
                        phoneVerification(phoneNum);
                    }

                } else if (value.equalsIgnoreCase("exist")) {
                    emailExist(true);
                }

            }
        });

        eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isVisible) {
                    eye.setImageResource(R.drawable.ic_eye);
                    passwordBox.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordBox.setTypeface(Typeface.DEFAULT);
                    passwordBox.setSelection(passwordBox.length());
                    isVisible = true;
                } else {
                    eye.setImageResource(R.drawable.ic_hide_eye);
                    passwordBox.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordBox.setTypeface(Typeface.DEFAULT);
                    passwordBox.setSelection(passwordBox.length());
                    isVisible = false;
                }
            }
        });

        nameBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                nameBoxInputLayout.setError(null);

            }
        });

        emailBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                value = "exist";

            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailBoxInputLayout.setError(null);

            }
        });

        emailBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailBox.getText().toString()).matches()) {
                        emailBoxInputLayout.setError("Please provide valid email address");
                        emailBoxInputLayout.setErrorEnabled(true);
                    } else {
                        emailExist(false);
                    }
                }
            }
        });

        passwordBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                passwordBoxInputLayout.setError(null);


            }
        });

        phoneNumberBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                phoneNumberBoxInputLayout.setError(null);
            }
        });
    }


    private void emailExist(final boolean b) {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);

            mHttpClient.get(Config.EMAIL_EXIST + emailBox.getText().toString() + "&type=player", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("SUCCESS", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {
                            emailBoxInputLayout.setErrorEnabled(false);
                            value = "true";
                            if (b) {
                                if (checkForAllValidation()) {
                                    signUpEmail();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        value = "false";
                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    if (null != responseBody) {
                        try {
                            String failure = new String(responseBody);
                            Log.e("failure", failure);
                            JSONObject object = new JSONObject(failure);
                            emailBoxInputLayout.setError(object.getString("message"));
                            emailBoxInputLayout.setErrorEnabled(true);
                            value = "false";
                            //Toast.makeText(SignUpActivity.this,object.getString("message"),Toast.LENGTH_SHORT).show();
                            //Intent login = new Intent(SignUpActivity.this,LoginActivity.class);
                            //startActivity(login);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            emailBoxInputLayout.setErrorEnabled(true);
                            value = "false";
                        }
                    }


                }
            });

        }
        else {
            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    private void signUpEmail() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(SignUpActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("name", nameBox.getText().toString());
            params.add("email", emailBox.getText().toString());
            params.add("password", passwordBox.getText().toString());
            params.add("device_type", "android");
            params.add("device_id", mPref.getAndroid_id());
            params.add("fcm_token", mPref.getGcmToken());
            params.add("mobile", phoneNumberBox.getText().toString());

            httpClient.post(Config.EMAIL_REGISTER, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String signUpSuccess = new String(responseBody);
                    Log.e("signUpSuccess", signUpSuccess);

                    try {
                        JSONObject object = new JSONObject(signUpSuccess);
                        if (object.getInt("statusCode") == 200) {
                            id = object.getJSONObject("success").getInt("id");
                            userName = object.getJSONObject("success").getString("name");
                            mobile_verified = String.valueOf(object.getJSONObject("success").getBoolean("mobile_verified"));
                            type = String.valueOf(object.getJSONObject("success").getBoolean("is_private"));
                            accToken = object.getString("accessToken");
                            profile_pic_updated = String.valueOf(object.getJSONObject("success").getBoolean("profile_pic_updated"));
                            interest_updated = String.valueOf(object.getJSONObject("success").getBoolean("interest_updated"));
                            location_updated = String.valueOf(object.getJSONObject("success").getBoolean("location_updated"));
                            userEmail = object.getJSONObject("success").getString("email");

                            mPref.setId(String.valueOf(id));
                            mPref.setMobilVerified(mobile_verified);
                            mPref.setAccessToken(accToken);
                            mPref.setName(userName);
                            mPref.setProfileType(type);
                            mPref.setProfilePicUpdated(profile_pic_updated);
                            mPref.setInterestUpdated(interest_updated);
                            mPref.setLocationUpdated(location_updated);
                            mPref.setUserEmail(userEmail);
                            mPref.setProfilePic("");
                            PlaythoraUtility.hideProgressDialog();
                            if (mPref.getMobilVerified().equalsIgnoreCase("true")
                                    || mPref.getMobilVerified().toString().equalsIgnoreCase("1")) {
//                               Intent home = new Intent(SignUpActivity.this,ProfileSetUpActivity.class);
                                Intent home = new Intent(SignUpActivity.this, HomeScreenActivity.class);
                                startActivity(home);
                                finishAffinity();
                            } else {
//                                Intent verify = new Intent(SignUpActivity.this, VerifyActivity.class);
                                Intent verify = new Intent(SignUpActivity.this, VerifyNumberActivity.class);
//                                verify.putExtra("verifyNum", phoneNum);
                                verify.putExtra("Number", phoneNum);
                                startActivity(verify);

                            }

                        }


                    } catch (JSONException e) {
                        PlaythoraUtility.hideProgressDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("signUpFailure", s);

                        JSONObject object = new JSONObject(s);
                        if (object.getString("status").equalsIgnoreCase("error")) {
                            Toast.makeText(SignUpActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent login = new Intent(SignUpActivity.this, LoginActivity.class);
                            startActivity(login);
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    public boolean validate(final String password) {

        Log.e("pass", "" + password);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    private void phoneVerification(final String phoneNum) {

        final Dialog phoneDialog = new Dialog(SignUpActivity.this);
        phoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        phoneDialog.setContentView(R.layout.verify_dialog_layout);

        change = (TextView) phoneDialog.findViewById(R.id.text_change_number);
        looksGood = (TextView) phoneDialog.findViewById(R.id.text_looks_good);
        verifynum = (TextView) phoneDialog.findViewById(R.id.verify_number);
        verifySubText = (TextView) phoneDialog.findViewById(R.id.verify_sutext);
        number = (TextView) phoneDialog.findViewById(R.id.number);
        doesLooksOk = (TextView) phoneDialog.findViewById(R.id.doesLooksOk);
        verify = (TextView) phoneDialog.findViewById(R.id.verify_text);

        verifySubText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        number.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        change.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        looksGood.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        doesLooksOk.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        verify.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));

        verifynum.setText(phoneNum + ".");

        looksGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneDialog.dismiss();
                signUpEmail();

            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneDialog.dismiss();
            }
        });

        phoneDialog.show();
    }

    private boolean checkForAllValidation() {

        if (nameBox.getText().toString().isEmpty() || nameBox.getText().toString().trim().isEmpty()) {
            nameBoxInputLayout.setError("Please provide valid name");
            nameBoxInputLayout.setErrorEnabled(true);
        } else {
            nameBoxInputLayout.setErrorEnabled(false);

            // remove extra spaces at beginning or end of user's name
            nameBox.setText(nameBox.getText().toString().trim());
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailBox.getText().toString()).matches()) {
            emailBoxInputLayout.setError("Please provide valid email address");
            emailBoxInputLayout.setErrorEnabled(true);
        } else {
            emailBoxInputLayout.setErrorEnabled(false);
        }

      /*  if(passwordBox.getText().toString().isEmpty() || passwordBox.getText().toString().contains(" ")) {
            passwordBoxInputLayout.setError("Please provide password");
            passwordBoxInputLayout.setErrorEnabled(true);
        } else {
                passwordBoxInputLayout.setErrorEnabled(false);
        }*/

        if (passwordBox.getText().toString().length() <= 7) {
            passwordBoxInputLayout.setError("Password should be atleast 8 characters");
            passwordBoxInputLayout.setErrorEnabled(true);
        } else if (!(validate(passwordBox.getText().toString()))) {
            passwordBoxInputLayout.setError("Psst, your password should contain a numerical");
            passwordBoxInputLayout.setErrorEnabled(true);
        } else {
            passwordBoxInputLayout.setErrorEnabled(false);
        }


        if (phoneNumberBox.getText().toString().isEmpty() || phoneNumberBox.getText().toString().contains(" ") || phoneNumberBox.getText().length() < 10) {
            phoneNumberBoxInputLayout.setError("Enter valid mobile number");
            phoneNumberBoxInputLayout.setErrorEnabled(true);
        } else {
            phoneNumberBoxInputLayout.setErrorEnabled(false);

        }
        if (nameBoxInputLayout.isErrorEnabled() || emailBoxInputLayout.isErrorEnabled() || passwordBoxInputLayout.isErrorEnabled() || phoneNumberBoxInputLayout.isErrorEnabled())
            return false;
        else return true;
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        pattern = Pattern.compile(passPattern);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        nameBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_name);
        emailBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_email);
        passwordBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_password);
        phoneNumberBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_phonenumber);
        nameBox = (EditText) findViewById(R.id.signup_name);
        emailBox = (EditText) findViewById(R.id.signup_email);
        passwordBox = (EditText) findViewById(R.id.signup_password);
        phoneNumberBox = (EditText) findViewById(R.id.signup_phonenumber);
        signUp = (Button) findViewById(R.id.signUpNext);
        setupUI(findViewById(R.id.signup_activity));
        eye = (ImageView) findViewById(R.id.password_visibility);
        signUpText = (TextView) findViewById(R.id.signup_text);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        nameBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        emailBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        passwordBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        phoneNumberBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        nameBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        emailBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        passwordBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        //  phoneNumberBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        signUp.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        signUpText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

    }

    private void setupUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(SignUpActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                setupUI(innerView);
            }
        }
    }
}
