package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.model.MyProfileLeaderBoardModel;
import com.yowzapp.playstore.model.PlayerDetailSuccessAssTeams;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 17/11/16.
 */
public class PlayerProfileTeamView extends Fragment {

    public static ArrayList<MyProfileLeaderBoardModel> myProfileEveryOne;
    public List<PlayerDetailSuccessAssTeams> teamses;
    public RelativeLayout previousButton, nextButton;
    Gson mGson;
    ProgressDialog dialog;
    Gson gson;
    String responseString = "", responseString2 = "", responseString1 = "";
    int dummySize = 10;
    String teams;
    TextView noTeam;
    private RecyclerView recyclerView;


//    public PlayerProfileTeamView(String s) {
//        teams = s;
//        Log.e("teamList", s);
//    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        teams = getArguments().getString("teams");
        Log.e("teamList", teams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.player_profile_team_player, container, false);

        recyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_view_horizontal_teams_profile);
        noTeam = (TextView) contentView.findViewById(R.id.noTeam);
        noTeam.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


        try {
            populateRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentView;
    }


    private void populateRecyclerView() {
        gson = new Gson();
        try {
            if (!teams.isEmpty()) {
                JSONArray jsonArray = new JSONArray(teams);
                ArrayList arrayList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    arrayList.add(gson.fromJson(jsonArray.get(i).toString(), PlayerDetailSuccessAssTeams.class));
                    ProfileTeamAdapter teamAdapter = new ProfileTeamAdapter(getActivity(), arrayList);
                    recyclerView.setAdapter(teamAdapter);
                    noTeam.setVisibility(View.GONE);
                }
            } else {
                noTeam.setVisibility(View.VISIBLE);
                noTeam.setText("No associated teams");
            }
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Error fetching Associated Teams", Toast.LENGTH_LONG).show();
            noTeam.setVisibility(View.VISIBLE);
            noTeam.setText("No associated teams");
        }


    }


    public class ProfileTeamAdapter extends RecyclerView.Adapter<ProfileTeamAdapter.ProfileListHolder> {
        Context mContext;
        List<PlayerDetailSuccessAssTeams> playerList;

        public ProfileTeamAdapter(Context context, List<PlayerDetailSuccessAssTeams> arrayList) {
            mContext = context;
            playerList = arrayList;
        }

        @Override
        public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.player_teams, parent, false);
            ProfileListHolder listHolder = new ProfileListHolder(mainGroup);
            return listHolder;

        }

        @Override
        public void onBindViewHolder(ProfileListHolder holder, int position) {
            final PlayerDetailSuccessAssTeams model = playerList.get(position);


            holder.teamName.setText(model.getName());

            try {
                Glide.with(mContext).load(model.getCover_pic()).centerCrop().error(R.drawable.no_team).into(holder.teamImage);
            } catch (Exception e) {
                holder.teamImage.setImageResource(R.drawable.no_team);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, OtherTeamViewActivity.class);
                    i.putExtra("ID", model.getId());
                    startActivity(i);
                }
            });
        }

        @Override
        public int getItemCount() {
            return playerList.size();
        }

        public class ProfileListHolder extends RecyclerView.ViewHolder {

            ImageView teamImage;
            TextView teamName;

            public ProfileListHolder(View itemView) {
                super(itemView);
                teamImage = (ImageView) itemView.findViewById(R.id.imageView_player);
                teamName = (TextView) itemView.findViewById(R.id.player_name);

                teamName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));


            }
        }
    }
}