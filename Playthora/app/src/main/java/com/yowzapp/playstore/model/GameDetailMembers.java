package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 28/12/16.
 */
public class GameDetailMembers {
    private int id;
    private String name;
    private String profile_pic;
    private PivotModel pivot;

    public PivotModel getPivot() {
        return pivot;
    }

    public void setPivot(PivotModel pivot) {
        this.pivot = pivot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
