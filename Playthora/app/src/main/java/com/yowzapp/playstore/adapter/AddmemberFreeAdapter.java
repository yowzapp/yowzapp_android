package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.AddMembersActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 28/12/16.
 */
public class AddmemberFreeAdapter extends RecyclerView.Adapter<AddmemberFreeAdapter.AddMembersFreerHolder> {

    private List<AddTeamMembersList> arrayList;
    private Context context;
    private boolean[] mCheckedState;

    public AddmemberFreeAdapter(Context context, List<AddTeamMembersList> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        mCheckedState = new boolean[this.arrayList.size()];

        Log.e("ARRAYLIST", String.valueOf(this.arrayList.size()));
        // Log.e("TEMPLIST", String.valueOf(AddMembersActivity.stringList.size()));
        Log.e("stringList", String.valueOf(AddMembersActivity.stringList.size()));

        for (int i = 0; i < this.arrayList.size(); i++) {
            mCheckedState[i] = false;
        }

        for (int i = 0; i < this.arrayList.size(); i++) {
            for (int j = 0; j < AddMembersActivity.stringList.size(); j++) {
                if (this.arrayList.get(i).getId() == AddMembersActivity.stringList.get(j).getId()) {
                    // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                    mCheckedState[i] = true;
                    //break;
                }
            }
        }

    }

    public void RefreshPagination(List<AddTeamMembersList> newString, boolean pagination) {
        if (pagination) {
            mCheckedState = new boolean[this.arrayList.size()];
            this.arrayList = newString;
            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }
            Log.e("stringList", AddMembersActivity.stringList.size() + "," + this.arrayList.size());
            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < AddMembersActivity.stringList.size(); j++) {
                    if (this.arrayList.get(i).getId() == AddMembersActivity.stringList.get(j).getId()) {
                        Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                        // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                        mCheckedState[i] = true;
                        break;
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

   /* public void Refresh(List<AddTeamMembersList> newString){
        for(int i = 0;i<this.arrayList.size();i++){
            mCheckedState[i]=false;
        }
        Log.e("stringList", String.valueOf(AddMembersActivity.stringList.size()+","+this.arrayList.size()));
        for(int i = 0;i<this.arrayList.size();i++){
            for(int j=0;j<newString.size();j++){
                if(this.arrayList.get(i).getId()==newString.get(j).getId()){
                    Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                    // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                    mCheckedState[i]=true;
                    //break;
                }
            }
        }
        notifyDataSetChanged();
    }
*/

    public void Refresh(List<AddTeamMembersList> newString) {
        for (int i = 0; i < this.arrayList.size(); i++) {
            mCheckedState[i] = false;
        }
        Log.e("stringList", String.valueOf(AddMembersActivity.stringList.size() + "," + this.arrayList.size()));
        for (int i = 0; i < this.arrayList.size(); i++) {
            for (int j = 0; j < newString.size(); j++) {
                if (this.arrayList.get(i).getId() == newString.get(j).getId()) {
                    Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                    // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                    mCheckedState[i] = true;
                    //break;
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public AddmemberFreeAdapter.AddMembersFreerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.following_item_layout, parent, false);
        AddmemberFreeAdapter.AddMembersFreerHolder listHolder = new AddmemberFreeAdapter.AddMembersFreerHolder(mainGroup);
        return listHolder;
    }

    @Override
    public void onBindViewHolder(AddmemberFreeAdapter.AddMembersFreerHolder holder, final int position) {
        final AddTeamMembersList model = arrayList.get(position);

        final AddmemberFreeAdapter.AddMembersFreerHolder mainHolder = holder;

        try {
            mainHolder.name.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            mainHolder.name.setText(model.getName());
            if (mCheckedState[position])
                mainHolder.checkBox.setChecked(true);
            else mainHolder.checkBox.setChecked(false);

            holder.checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mCheckedState[position]) {
                        //Toast.makeText(context,"checked",Toast.LENGTH_SHORT).show();
                        Log.e("CheckedFree", AddMembersActivity.stringList.size() + "");
                        if (AddMembersActivity.stringList.size() > 0) {
                            for (int j = 0; j < AddMembersActivity.stringList.size(); j++) {
                                if (AddMembersActivity.stringList.get(j).getId() == arrayList.get(position).getId()) {

                                } else {
                                    AddMembersActivity.stringList.add(arrayList.get(position));
                                    // mCheckedState[position]=true;
                                    mCheckedState[position] = true;
                                    return;
                                }
                            }
                        } else {
                            AddMembersActivity.stringList.add(arrayList.get(position));
                            // mCheckedState[position]=true;
                            mCheckedState[position] = true;
                        }

                        notifyDataSetChanged();

                    } else {
                        //Toast.makeText(context,"unchecked",Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < AddMembersActivity.stringList.size(); i++) {
                            if (AddMembersActivity.stringList.get(i).getId() == arrayList.get(position).getId()) {
                                AddMembersActivity.stringList.remove(i);

                            }
                        }
                        //notifyDataSetChanged();
                        mCheckedState[position] = false;
                        notifyDataSetChanged();
                    }

                }
            });

        } catch (NullPointerException e) {

        }

        try {
            Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
        } catch (Exception e) {
            mainHolder.userImage.setImageResource(R.drawable.circled_user);
        }
        mainHolder.itemView.setTag(model);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    public class AddMembersFreerHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView name;
        CheckBox checkBox;
        RelativeLayout relative;

        public AddMembersFreerHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
            this.name = (TextView) view.findViewById(R.id.userName);
            this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            this.relative = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);
        }
    }
}
