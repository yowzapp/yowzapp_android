package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.model.MyTransactionCashSuccess;
import com.yowzapp.playstore.model.MyTransactionsCashModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by hemanth on 16/11/16.
 */

public class MyTransactionCashFragment extends Fragment {

    public ArrayList<MyTransactionCashSuccess> transactionCashArrayList;
    public MyTransactionsCashAdapter myTransactionsCashAdapterpter;
    RecyclerView recyclerView;
    ProgressDialog dialog;
    MyTransactionsCashModel myTransactionsCashModel;
    String response;
    Gson mGson;
    ProgressBar progressBar;
    PreferenceManager mPref;
    TextView noTransaction;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.my_transaction_cash_layout, container, false);
        mPref = PreferenceManager.instance(getActivity());

        recyclerView = (RecyclerView) contentView.findViewById(R.id.myTransactionCashRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        progressBar = (ProgressBar) contentView.findViewById(R.id.progress);
        noTransaction = (TextView) contentView.findViewById(R.id.no_transaction);
        noTransaction.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        try {
            cashRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contentView;

    }

    private void cashRecycler() {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            httpClient.get(Config.MY_TRANSACTIONS, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("MyTransactions", s);

                        myTransactionsCashModel = new MyTransactionsCashModel();
                        mGson = new Gson();
                        myTransactionsCashModel = mGson.fromJson(s, MyTransactionsCashModel.class);

                        if (!myTransactionsCashModel.getSuccess().isEmpty()) {
                            if (recyclerView.getAdapter() == null) {
                                myTransactionsCashAdapterpter = new MyTransactionsCashAdapter(getActivity(), myTransactionsCashModel.getSuccess());
                                recyclerView.setAdapter(myTransactionsCashAdapterpter);// set adapter on recyclerview
                                myTransactionsCashAdapterpter.notifyDataSetChanged();
                                noTransaction.setVisibility(View.GONE);
                            } else {
                                noTransaction.setVisibility(View.VISIBLE);
                                noTransaction.setText("You haven't had any cash transactions yet");
                            }

                        } else {
                            noTransaction.setVisibility(View.VISIBLE);
                            noTransaction.setText("You haven't had any cash transactions yet");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                        JSONObject jsonObject = new JSONObject(s);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
        }
/*
        Thread thread=new Thread(new Runnable(){

            public void run() {

                try {
                    response = "[{\n" +
                            "    \"transactionName\": \"royal challengers banglore game\",\n" +
                            "    \"transactionid\": 1,\n" +
                            "    \"amount\": \"3450\",\n" +
                            "    \"venue\": \"Freedom park ground\"\n" +
                            "}, {\n" +
                            "    \"transactionName\": \"Chennai express game\",\n" +
                            "    \"transactionid\": 2,\n" +
                            "    \"amount\": \"350\",\n" +
                            "    \"venue\": \"BTM ground\"\n" +
                            "}, {\n" +
                            "    \"transactionName\": \"Mumbai game\",\n" +
                            "    \"transactionid\": 7,\n" +
                            "    \"amount\": \"2250\",\n" +
                            "    \"venue\": \"HSR ground\"\n" +
                            "}, {\n" +
                            "    \"transactionName\": \"KKR game\",\n" +
                            "    \"transactionid\": 6,\n" +
                            "    \"amount\": \"1450\",\n" +
                            "    \"venue\": \"National park ground\"\n" +
                            "}, {\n" +
                            "    \"transactionName\": \"Delhi dare devils game\",\n" +
                            "    \"transactionid\": 5,\n" +
                            "    \"amount\": \"6458\",\n" +
                            "    \"venue\": \"Freedom park ground\"\n" +
                            "}]";

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                try{
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if(dialog.isShowing())
                                        {
                                            dialog.dismiss();
                                        }
                                    }
                                }catch (Exception e){
                                    if(dialog.isShowing())
                                    {
                                        dialog.dismiss();
                                    }
                                    e.printStackTrace();
                                }

                                JSONArray jsonArray = new JSONArray(response);
                                mGson = new Gson();
                                transactionCashArrayList = new ArrayList<MyTransactionsCashModel>();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    transactionCashArrayList.add(mGson.fromJson(jsonArray.get(i).toString(), MyTransactionsCashModel.class));

                                }

                                if (!transactionCashArrayList.isEmpty()) {
                                    if(recyclerView.getAdapter()==null) {

                                        myTransactionsCashAdapterpter = new MyTransactionsCashAdapter(getActivity(), transactionCashArrayList);
                                        recyclerView.setAdapter(myTransactionsCashAdapterpter);// set adapter on recyclerview
                                        myTransactionsCashAdapterpter.notifyDataSetChanged();


                                    }

                                } else {

                                }


                            } catch (JsonParseException e) {

                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                }catch (Exception ee){
                                    ee.printStackTrace();
                                }

                                e.printStackTrace();
                            } catch (JSONException e) {

                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                }catch (Exception eee){
                                    eee.printStackTrace();
                                }

                                e.printStackTrace();
                            }

                        }
                    });
                }catch (Exception e) {}
            }});//
        thread.start();*/
    }


    public class MyTransactionsCashAdapter extends RecyclerView.Adapter<MyTransactionCashHolder> {

        private List<MyTransactionCashSuccess> arrayList;
        private Context context;

        public MyTransactionsCashAdapter(Context context, List<MyTransactionCashSuccess> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public MyTransactionCashHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.cash_item_layout, viewGroup, false);
            MyTransactionCashHolder listHolder = new MyTransactionCashHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final MyTransactionCashHolder holder, final int position) {
            final MyTransactionCashSuccess model = arrayList.get(position);


            final MyTransactionCashHolder mainHolder = (MyTransactionCashHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
                mainHolder.date.setTypeface(Typeface.DEFAULT_BOLD);
                mainHolder.venue.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.status.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

                mainHolder.name.setText(model.getGame_name());
                mainHolder.venue.setText(model.getVenue_name());
                mainHolder.price.setText(model.getAmount() + " " + "AED");

                mainHolder.date.setText(model.getDate().toUpperCase());

                if (model.getDetails().equalsIgnoreCase("payment completed")) {
                    mainHolder.status.setText("SUCCESSFUL");
                    mainHolder.transactionId.setText("TID" + " " + model.getTransaction_number());
                } else {
                    mainHolder.status.setText("FAILED");
                    mainHolder.transactionId.setText("");
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                //  Glide.with(context).load(model.getImage()).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mainHolder.itemView.setTag(model);


        }
    }

    public class MyTransactionCashHolder extends RecyclerView.ViewHolder {

        TextView name, venue, date, status, transactionId, price;

        public MyTransactionCashHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.transactionName);
            date = (TextView) view.findViewById(R.id.transactionDate);
            venue = (TextView) view.findViewById(R.id.transactionVenue);
            status = (TextView) view.findViewById(R.id.transactionStatus);
            transactionId = (TextView) view.findViewById(R.id.transactionId);
            price = (TextView) view.findViewById(R.id.transactionPrice);

        }
    }

}
