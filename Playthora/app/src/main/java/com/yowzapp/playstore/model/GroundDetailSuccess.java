package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 22/12/16.
 */
public class GroundDetailSuccess {

    private int id;
    private String name;
//    private int max_member;
    private boolean is_disabled;
    private int venue_id;
    private int sports_id;
    private String sports_name;
    private String date;
//    private String slot_price;
    private String slot_duration;
    private int slot_id;
    private List<GroundSlots> slots;

    public int getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(int slot_id) {
        this.slot_id = slot_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public int getMax_member() {
//        return max_member;
//    }

//    public void setMax_member(int max_member) {
//        this.max_member = max_member;
//    }

    public boolean is_disabled() {
        return is_disabled;
    }

    public void setIs_disabled(boolean is_disabled) {
        this.is_disabled = is_disabled;
    }

    public int getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(int venue_id) {
        this.venue_id = venue_id;
    }

    public int getSports_id() {
        return sports_id;
    }

    public void setSports_id(int sports_id) {
        this.sports_id = sports_id;
    }

    public String getSports_name() {
        return sports_name;
    }

    public void setSports_name(String sports_name) {
        this.sports_name = sports_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

//    public String getSlot_price() {
//        return slot_price;
//    }

//    public void setSlot_price(String slot_price) {
//        this.slot_price = slot_price;
//    }

    public String getSlot_duration() {
        return slot_duration;
    }

    public void setSlot_duration(String slot_duration) {
        this.slot_duration = slot_duration;
    }

    public List<GroundSlots> getSlots() {
        return slots;
    }

    public void setSlots(List<GroundSlots> slots) {
        this.slots = slots;
    }


}
