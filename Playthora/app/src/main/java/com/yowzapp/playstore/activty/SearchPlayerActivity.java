package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.SearchAllModel;
import com.yowzapp.playstore.model.SearchAllModelList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchPlayerActivity extends BaseActivity {
    RecyclerView recyclerViewsearch;
    ImageView done, back;
    EditText search;
    ProgressBar progressBar;
    TextView noResult;
    Editable sText;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<SearchAllModelList> tempList;
    SearchPlayerAdapter adapter;
    Gson gson;
    PreferenceManager mPref;
    SearchAllModel model;
    SearchPlayerAdapter searchAdapter;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_search);
        initialize();

        mPref = PreferenceManager.instance(this);
        mLayoutManager = new LinearLayoutManager(SearchPlayerActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewsearch.setLayoutManager(mLayoutManager);
        recyclerViewsearch.setHasFixedSize(true);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(3, intent);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("TEXT", s + "," + count + "," + start + "," + after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("TEXT1", s + "," + count + "," + start + "," + before);
            }

            @Override
            public void afterTextChanged(Editable s) {
                s.toString().replace("%20", " ");
                s.toString().replace("%", "");
                s.toString().replace(" ", "%20");
                if (s.toString().length() > 2) {
                    populateSearch(s, pages);
                    sText = s;
                } else if (s.toString().length() == 0) {
                    recyclerViewsearch.setAdapter(null);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        recyclerViewsearch.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("ONSCROLL", "onScrollStateChanged");
                Log.e("ONSCROLL", "onScrollStateChanged");
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (model.isHasMorePages()) {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                            pages += 1;
                            try {
                                populateSearch(sText, pages);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            searchAdapter.notifyDataSetChanged();

                        }
                    }
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("ONSCROLL", "onScrolled");
            }
        });


    }

    private void populateSearch(Editable s, int pages) {

        if (PlaythoraUtility.checkInternetConnection(SearchPlayerActivity.this)) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                done.setVisibility(View.GONE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SEARCH_PLAYER + "?q=" + s + "&page=" + pages,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                done.setVisibility(View.VISIBLE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("SEARCH_RESPONSE", s);
                                    model = new SearchAllModel();
                                    gson = new Gson();
                                    model = gson.fromJson(s, SearchAllModel.class);
                                    Log.e("RESPONSE", String.valueOf(model.getSuccess().size()));

                                    if (model.getCurrentPage() > 1) {
                                        noResult.setVisibility(View.GONE);
                                        recyclerViewsearch.setVisibility(View.VISIBLE);
                                        for (int i = 0; i < model.getSuccess().size(); i++) {
                                            tempList.add(model.getSuccess().get(i));
                                        }
                                        Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                        searchAdapter.RefreshPagination(tempList, true);
                                    } else if (model.getSuccess().size() != 0) {
                                        noResult.setVisibility(View.GONE);
                                        recyclerViewsearch.setVisibility(View.VISIBLE);
                                        tempList = model.getSuccess();
                                        searchAdapter = new SearchPlayerAdapter(SearchPlayerActivity.this, model.getSuccess(), AddMembersActivity.stringList);
                                        recyclerViewsearch.setAdapter(searchAdapter);
                                    } else {
                                        noResult.setVisibility(View.VISIBLE);
                                        //recyclerViewsearch.setVisibility(View.GONE);
                                        recyclerViewsearch.setAdapter(null);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressBar.setVisibility(View.GONE);
                                    done.setVisibility(View.VISIBLE);
                                    noResult.setVisibility(View.VISIBLE);
                                    //recyclerViewsearch.setVisibility(View.GONE);
                                    recyclerViewsearch.setAdapter(null);
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                done.setVisibility(View.VISIBLE);
                                noResult.setVisibility(View.VISIBLE);
                                recyclerViewsearch.setAdapter(null);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(SearchPlayerActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(SearchPlayerActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    }//else Toast.makeText(SearchPlayerActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(SearchPlayerActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(SearchPlayerActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
                done.setVisibility(View.VISIBLE);
                noResult.setVisibility(View.VISIBLE);
                recyclerViewsearch.setAdapter(null);
            }
        } else {
//            Toast.makeText(SearchPlayerActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }


    }

    private void initialize() {
        recyclerViewsearch = (RecyclerView) findViewById(R.id.search_recycler_player);
        done = (ImageView) findViewById(R.id.cancel_icon_player);
        back = (ImageView) findViewById(R.id.back_icon_player);
        search = (EditText) findViewById(R.id.edit_search_player);
        noResult = (TextView) findViewById(R.id.empty_search_player);
        progressBar = (ProgressBar) findViewById(R.id.search_progress_player);
        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        noResult.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
    }

    public class SearchPlayerAdapter extends RecyclerView.Adapter<SearchPlayerAdapter.PlayerHolder> {
        ArrayList<AddTeamMembersList> selectedList;
        private List<SearchAllModelList> arrayList;
        private Context context;
        private boolean[] mCheckedState;

        public SearchPlayerAdapter(Context context, List<SearchAllModelList> success, ArrayList<AddTeamMembersList> selectedList) {
            this.arrayList = success;
            this.context = context;
            this.selectedList = selectedList;

            mCheckedState = new boolean[this.arrayList.size()];

            Log.e("ARRAYLIST", String.valueOf(this.arrayList.size()));

            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }
            for (int k = 0; k < this.arrayList.size(); k++) {
                for (int j = 0; j < this.selectedList.size(); j++) {
                    if (this.selectedList.get(j).getId() == this.arrayList.get(k).getId()) {
                        mCheckedState[k] = true;
                        break;
                    }
                }
            }
        }

        public void RefreshPagination(List<SearchAllModelList> tempList, boolean b) {
            arrayList = tempList;
            mCheckedState = new boolean[this.arrayList.size()];

            Log.e("ARRAYLIST", String.valueOf(this.arrayList.size()));

            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }
            for (int k = 0; k < this.arrayList.size(); k++) {
                for (int j = 0; j < this.selectedList.size(); j++) {
                    if (this.selectedList.get(j).getId() == this.arrayList.get(k).getId()) {
                        mCheckedState[k] = true;
                        break;
                    }
                }
            }
            notifyDataSetChanged();
        }

        @Override
        public PlayerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.following_item_layout, parent, false);
            PlayerHolder listHolder = new PlayerHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(PlayerHolder holder, final int position) {

            final SearchAllModelList model = arrayList.get(position);

            try {
                Glide.with(context).load(arrayList.get(position).getCover_pic()).error(R.drawable.circled_user).centerCrop().into(holder.userImage);
            } catch (Exception e) {
                holder.userImage.setImageResource(R.drawable.circled_user);
            }

            holder.name.setText(model.getName());
            if (mCheckedState[position])
                holder.checkBox.setChecked(true);
            else holder.checkBox.setChecked(false);

            holder.checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mCheckedState[position]) {
                        //Toast.makeText(context,"checked",Toast.LENGTH_SHORT).show();
                        Log.e("CheckedFree", AddMembersActivity.stringList.size() + "");
                        if (AddMembersActivity.stringList.size() > 0) {
                            for (int j = 0; j < AddMembersActivity.stringList.size(); j++) {
                                if (AddMembersActivity.stringList.get(j).getId() == arrayList.get(position).getId()) {
                                } else {
                                    AddMembersActivity.stringList.add(arrayList.get(position).getPlayer_details());
                                    mCheckedState[position] = true;
                                    return;
                                }
                            }
                        } else {
                            AddMembersActivity.stringList.add(arrayList.get(position).getPlayer_details());
                            mCheckedState[position] = true;
                        }
                        notifyDataSetChanged();

                    } else {
                        for (int i = 0; i < AddMembersActivity.stringList.size(); i++) {
                            if (AddMembersActivity.stringList.get(i).getId() == arrayList.get(position).getId()) {
                                AddMembersActivity.stringList.remove(i);
                            }
                        }
                        mCheckedState[position] = false;
                        notifyDataSetChanged();
                    }

                }
            });


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class PlayerHolder extends RecyclerView.ViewHolder {
            CircleImageView userImage;
            TextView name;
            CheckBox checkBox;

            public PlayerHolder(View view) {
                super(view);
                this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
                this.name = (TextView) view.findViewById(R.id.userName);
                this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);

                name.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));

            }
        }
    }

}
