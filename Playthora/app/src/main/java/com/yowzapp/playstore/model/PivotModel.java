package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 30/12/16.
 */
public class PivotModel {
    private int team_id;
    private int user_id;
    private int is_super_admin;
    private int is_admin;

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getIs_super_admin() {
        return is_super_admin;
    }

    public void setIs_super_admin(int is_super_admin) {
        this.is_super_admin = is_super_admin;
    }

    public int getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(int is_admin) {
        this.is_admin = is_admin;
    }
}
