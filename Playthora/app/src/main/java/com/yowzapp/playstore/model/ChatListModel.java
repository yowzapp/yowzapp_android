package com.yowzapp.playstore.model;

/**
 * Created by nakul on 07/02/17.
 */
public class ChatListModel {
    private String sender_profile_pic;
    private int user_id;
    private String message;
    private String sender_name;
    private String date;

    public String getSender_profile_pic() {
        return sender_profile_pic;
    }

    public void setSender_profile_pic(String sender_profile_pic) {
        this.sender_profile_pic = sender_profile_pic;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
