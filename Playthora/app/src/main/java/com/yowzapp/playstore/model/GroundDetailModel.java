package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 22/12/16.
 */

public class GroundDetailModel {

    private String status;
    private String message;
    private GroundDetailSuccess success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GroundDetailSuccess getSuccess() {
        return success;
    }

    public void setSuccess(GroundDetailSuccess success) {
        this.success = success;
    }


}
