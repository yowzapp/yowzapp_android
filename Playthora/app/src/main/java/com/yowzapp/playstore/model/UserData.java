package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 10/1/17.
 */
public class UserData {
    private String profile_pic;

    public UserData(String profilePic) {
        this.profile_pic = profilePic;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
