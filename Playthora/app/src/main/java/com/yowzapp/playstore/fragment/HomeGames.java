package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.GameMain;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.model.AllGameList;
import com.yowzapp.playstore.model.GameListModel;
import com.yowzapp.playstore.model.GameListSuccess;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 15/11/16.
 */
public class HomeGames extends Fragment {
    View contentView;
    RecyclerView myGameRecyclerView, allGameRecyclerView;
    AllGamesAdapter adapter, allAdapter;
    TextView myGame, allGame;
    String response;
    GameListModel gameListModel;
    GameListSuccess gameListSuccess;
    List<AllGameList> myGameLists;
    List<AllGameList> allGameLists;
    Gson gson;
    ProgressBar progressBar, paginateProgressBar;
    TextView noGames;
    String s = "";
    PreferenceManager mPref;
    int pages = 1;
    NestedScrollView nested;
    LinearLayoutManager linearLayoutManager;
    List<AllGameList> allTeam;
    int fabMargin;
    RelativeLayout relativeLayout;
    private SwipeRefreshLayout swipeContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.games_layout, container, false);

        initializeAllComponents();
        fabMargin = 16;

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        allGameRecyclerView.setHasFixedSize(true);
        allGameRecyclerView.setLayoutManager(linearLayoutManager);

        myGameRecyclerView.setHasFixedSize(true);
        myGameRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        myGameRecyclerView.setFocusable(false);
        allGameRecyclerView.setFocusable(false);

        myGame.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
        allGame.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));

        swipeContainer = (SwipeRefreshLayout) contentView.findViewById(R.id.swipeContainer);

        try {
            loadGameList(pages, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final FloatingActionButton floatingActionButton = HomeFragment.getFloatingActionButton();

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    if (gameListModel.isHasMorePages()) {
                        Log.e("page_DONEG", page + "");
                        paginateProgressBar.setVisibility(View.VISIBLE);
                        pages = page;
                        loadGameList(pages, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    paginateProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
                if (floatingActionButton != null) {
                    floatingActionButton.hide();
                }
                //HomeFragment.relativeLayout.animate().translationY(HomeFragment.relativeLayout.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }

            @Override
            public void scrollUp() {
                if (floatingActionButton != null) {
                    Log.e("Scroll_UP", "Scroll_UP" + floatingActionButton.isShown());
                    if (!floatingActionButton.isShown())
                        floatingActionButton.hide();
                    floatingActionButton.show();
                }
                //HomeFragment.relativeLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    loadGameList(1, true);
                    endlessParentScrollListener.currentPage = 1;
                    endlessParentScrollListener.previousTotalItemCount = 0;
                    endlessParentScrollListener.loading = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    swipeContainer.setRefreshing(false);
                }
            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);

        return contentView;
    }


    private void loadGameList(int page, final boolean swipe) {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                if (!swipe)
                    progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                httpClient.get(Config.GAME_LISTING + "?page=" + page, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        hideLoaders();
                        String s = new String(responseBody);
                        Log.e("gameList", s);
                        Log.e("RANK", "3");
                        gameListModel = new GameListModel();
                        gson = new Gson();
                        gameListModel = gson.fromJson(s, GameListModel.class);

                        Log.e("$$$$$$$", String.valueOf(gameListModel.getSuccess()));

                        if (gameListModel.getCurrentPage() > 1) {
                            allGame.setVisibility(View.VISIBLE);
                            for (int i = 0; i < gameListModel.getSuccess().getAll_games().size(); i++) {
                                allTeam.add(gameListModel.getSuccess().getAll_games().get(i));
                            }
                            Log.e("venueList", String.valueOf(gameListModel.getCurrentPage()));
                            allAdapter.loadData(allTeam);

                        } else {
                            if (gameListModel.getSuccess().getMy_games().size() == 0 && gameListModel.getSuccess().getAll_games().size() == 0) {
                                noGames.setVisibility(View.VISIBLE);
                                noGames.setText("Start adding games");

                            } else {
                                noGames.setVisibility(View.GONE);
                            }
                            myGameLists = gameListModel.getSuccess().getMy_games();
                            allGameLists = gameListModel.getSuccess().getAll_games();

                            if (myGameLists.size() == 0) {
                                myGame.setVisibility(View.GONE);
                                myGameRecyclerView.setAdapter(null);
                            } else {
                                myGame.setVisibility(View.VISIBLE);
                                adapter = new AllGamesAdapter(getActivity(), myGameLists);
                                myGameRecyclerView.setAdapter(adapter);
                            }

                            if (allGameLists.size() == 0) {
                                allGame.setVisibility(View.GONE);
                                allGameRecyclerView.setAdapter(null);
                            } else {
                                allTeam = allGameLists;
                                allGame.setVisibility(View.VISIBLE);
                                allAdapter = new AllGamesAdapter(getActivity(), allGameLists);
                                allGameRecyclerView.setAdapter(allAdapter);
                            }
                        }


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable error) {
                        hideLoaders();
                        try {
                            String s = new String(bytes);
                            Log.e("myGamefailure", s);
                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                mPref.setAccessToken("");
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                getActivity().finishAffinity();
                            } else
                                Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            try {
                                String s = new String(bytes);
                                Log.d("myGamefailure", s);
                                JSONObject object = new JSONObject(s);
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                hideLoaders();
            }

        } else
            hideLoaders();
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getActivity());
        myGameRecyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_my_games);
        allGameRecyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_all_games);
        myGame = (TextView) contentView.findViewById(R.id.tv_my_games);
        allGame = (TextView) contentView.findViewById(R.id.tv_all_games);
        myGameRecyclerView.setNestedScrollingEnabled(false);
        allGameRecyclerView.setNestedScrollingEnabled(false);
        progressBar = (ProgressBar) contentView.findViewById(R.id.progress_bar);
        noGames = (TextView) contentView.findViewById(R.id.noGames);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested);
        paginateProgressBar = (ProgressBar) contentView.findViewById(R.id.pagination_progress_bar_two);
        noGames.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
    }


/*
    private class MyGamesAdapter extends RecyclerView.Adapter<MyGamesAdapter.MYGamesHolder>{
        Context context;
        List<MyGameList> myGameLists;


        public MyGamesAdapter(FragmentActivity activity, List<MyGameList> myGameLists) {
            this.context=activity;
            this.myGameLists=myGameLists;
        }

        @Override
        public MYGamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.my_game_item, parent, false);
            MYGamesHolder listHolder = new MYGamesHolder(mainGroup);

            return listHolder;
        }

        @Override
        public void onBindViewHolder(MYGamesHolder holder, int position) {

            final  MyGameList model = myGameLists.get(position);

            final MYGamesHolder mainHolder = (MYGamesHolder) holder;

            holder.mGamename.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
            holder.location.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
            //holder.duration.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
            holder.mButton.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));




            holder.layoutGame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent toGame = new Intent(getActivity(), GameMain.class);
                    startActivity(toGame);
                }
            });

        }

        @Override
        public int getItemCount() {
            return allGameLists.size();
        }

        public class MYGamesHolder extends RecyclerView.ViewHolder{
            CircleImageView mImage;
            TextView mGamename,duration,location,teamname;
            Button mButton;
            RelativeLayout layoutGame;
            public MYGamesHolder(View itemView) {
                super(itemView);

                mImage = (CircleImageView) itemView.findViewById(R.id.game_image);
                mGamename = (TextView) itemView.findViewById(R.id.tv_game_name);
                location = (TextView) itemView.findViewById(R.id.tv_place);
                teamname = (TextView) itemView.findViewById(R.id.tv_team_name);
                duration = (TextView) itemView.findViewById(R.id.tv_duration);
                mButton = (Button) itemView.findViewById(R.id.btn);
                layoutGame = (RelativeLayout) itemView.findViewById(R.id.gameItemLayout);

            }
        }
    }*/

    void hideLoaders() {
        if (swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);
        if (progressBar.getVisibility() == View.VISIBLE)
            progressBar.setVisibility(View.GONE);
        if (paginateProgressBar.getVisibility() == View.VISIBLE)
            paginateProgressBar.setVisibility(View.GONE);
    }

    private class AllGamesAdapter extends RecyclerView.Adapter<AllGamesAdapter.AllGamesHolder> {
        Context context;
        List<AllGameList> allGameLists;


        public AllGamesAdapter(FragmentActivity activity, List<AllGameList> allGameLists) {
            this.context = activity;
            this.allGameLists = allGameLists;
        }

        public void loadData(List<AllGameList> arrayList) {
            Log.e("LISTING", "LISTING");
            this.allGameLists = arrayList;
            notifyDataSetChanged();
        }

        @Override
        public AllGamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.game_item_layout, parent, false);
            AllGamesHolder listHolder = new AllGamesHolder(mainGroup);

            return listHolder;
        }

        @Override
        public void onBindViewHolder(AllGamesHolder holder, int position) {

            final AllGameList model = allGameLists.get(position);

            final AllGamesHolder mainHolder = (AllGamesHolder) holder;

            holder.mGamename.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            holder.location.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            holder.duration.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
            holder.mButton.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            // holder.teamname.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

            // holder.teamname.setText(homeGamesModel.get(position).getTeamname());
            holder.mGamename.setText(model.getName());
            holder.location.setText(model.getVenue()+ "\n" +model.getLocation());
            holder.duration.setText(model.getMatch_status());
            holder.members.setText(model.getNo_members() + "/" + model.getMax_members());
            s = String.valueOf(model.is_private());

            // Log.e("isPrivate",s+"");

            if (s.equalsIgnoreCase("false")) {
                holder.mButton.setText("Open Game");
                holder.mButton.setVisibility(View.VISIBLE);
            } else {
                holder.mButton.setVisibility(View.GONE);
            }

            try {
                Glide.with(context).load(model.getCover_pic()).error(R.drawable.game_null).centerCrop().into(holder.mImage);
            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.game_null);
            }

            holder.layoutGame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent toGame = new Intent(getActivity(), GameMain.class);
                    toGame.putExtra("name", model.getName());
                    toGame.putExtra("location", model.getLocation());
                    toGame.putExtra("date", model.getDate());
                    toGame.putExtra("coverPic", model.getCover_pic());
                    toGame.putExtra("discription", model.getDiscription());
                    toGame.putExtra("sportsName", model.getSport_name());
                    toGame.putExtra("type", String.valueOf(model.is_private()));
                    toGame.putExtra("isComplete", String.valueOf(model.is_completed()));
                    toGame.putExtra("start_time", model.getStart_time());
                    toGame.putExtra("end_time", model.getEnd_time());
                    toGame.putExtra("id", String.valueOf(model.getId()));

                    startActivity(toGame);
                }
            });

        }

        @Override
        public int getItemCount() {
            return allGameLists.size();
        }

        public class AllGamesHolder extends RecyclerView.ViewHolder {
            CircleImageView mImage;
            TextView mGamename, duration, location, teamname, members;
            Button mButton;
            RelativeLayout layoutGame;

            public AllGamesHolder(View itemView) {
                super(itemView);

                mImage = (CircleImageView) itemView.findViewById(R.id.game_image);
                mGamename = (TextView) itemView.findViewById(R.id.tv_game_name);
                location = (TextView) itemView.findViewById(R.id.tv_place);
                teamname = (TextView) itemView.findViewById(R.id.tv_team_name);
                duration = (TextView) itemView.findViewById(R.id.tv_duration);
                members = (TextView) itemView.findViewById(R.id.textmember);
                mButton = (Button) itemView.findViewById(R.id.btn);
                layoutGame = (RelativeLayout) itemView.findViewById(R.id.gameItemLayout);

            }
        }
    }
}
