package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.CartListAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class CartActivity extends BaseActivity {
    RecyclerView recyclerView;
    LinearLayoutManager mLayoutManager;
    Toolbar toolbar;
    Gson gson;
    String response = "[{\n" +
            "    \"product\": \"nike blue\",\n" +
            "    \"productId\": 1,\n" +
            "    \"rewardXp\": \"9000\",\n" +
            "    \"categories\": \"nike\",\n" +
            "    \"categoryId\": \"20\",\n" +
            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
            "}, {\n" +
            "    \"product\": \"nike blak\",\n" +
            "    \"productId\": 2,\n" +
            "    \"rewardXp\": \"9000\",\n" +
            "    \"categories\": \"nike\",\n" +
            "    \"categoryId\": \"22\",\n" +
            "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
            "}, {\n" +
            "    \"product\": \"nike white\",\n" +
            "    \"productId\": 3,\n" +
            "    \"rewardXp\": \"9000\",\n" +
            "    \"categories\": \"nike\",\n" +
            "    \"categoryId\": \"23\",\n" +
            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
            "}]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        toolbar = (Toolbar) findViewById(R.id.toolbar_cart);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView title = (TextView) findViewById(R.id.toolbar_title);


        TextView cartTitle = (TextView) findViewById(R.id.cart_title);
        Button next = (Button) findViewById(R.id.button_next);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        cartTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        next.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));

        title.setText("Cart");
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(CartActivity.this, AddressDetActivity.class);
                startActivity(login);
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.cart_recycler);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        gson = new Gson();
        try {
            JSONArray jsonArray = new JSONArray(response);
            ArrayList arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add(gson.fromJson(jsonArray.get(i).toString(), CartListModel.class));
                CartListAdapter cartAdapter = new CartListAdapter(this, arrayList);
                recyclerView.setAdapter(cartAdapter);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
