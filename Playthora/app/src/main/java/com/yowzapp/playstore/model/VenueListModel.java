package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by vaishakha on 16/12/16.
 */
public class VenueListModel {
    private String status;
    private String message;
    private List<VenueListItem> success;
    private int currentPage;
    private boolean hasMorePages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VenueListItem> getSuccess() {
        return success;
    }

    public void setSuccess(List<VenueListItem> success) {
        this.success = success;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }
}
