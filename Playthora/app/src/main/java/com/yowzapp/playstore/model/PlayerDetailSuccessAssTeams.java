package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 7/1/17.
 */
public class PlayerDetailSuccessAssTeams {

    private int id;
    private String name;
    private String discription;
    private String cover_pic;
    private boolean activity_meter;
    private int points;

    public PlayerDetailSuccessAssTeams(int id, String name, String discription, String cover_pic, boolean activity_meter, int points) {
        this.id = id;
        this.name = name;
        this.discription = discription;
        this.cover_pic = cover_pic;
        this.activity_meter = activity_meter;
        this.points = points;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public boolean isActivity_meter() {
        return activity_meter;
    }

    public void setActivity_meter(boolean activity_meter) {
        this.activity_meter = activity_meter;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
