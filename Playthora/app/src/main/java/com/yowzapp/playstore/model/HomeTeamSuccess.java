package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by vaishakha on 27/12/16.
 */
public class HomeTeamSuccess {
    private List<HomeMyTeamList> my_teams;
    private List<HomeMyTeamList> all_teams;

    public List<HomeMyTeamList> getMy_teams() {
        return my_teams;
    }

    public void setMy_teams(List<HomeMyTeamList> my_teams) {
        this.my_teams = my_teams;
    }

    public List<HomeMyTeamList> getAll_teams() {
        return all_teams;
    }

    public void setAll_teams(List<HomeMyTeamList> all_teams) {
        this.all_teams = all_teams;
    }
}
