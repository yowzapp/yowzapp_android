package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.MyProfileLeaderBoardEveryOne;
import com.yowzapp.playstore.fragment.MyProfileLeaderBoardFollowing;
import com.yowzapp.playstore.fragment.MyProfileLeaderBoardTeam;
import com.yowzapp.playstore.model.MyProfileLeaderBoardModel;
import com.yowzapp.playstore.model.SportsListModel;
import com.yowzapp.playstore.model.SportsSuccessList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 15/11/16.
 */

public class MyProfileLeaderBoardMain extends BaseActivity implements  View.OnClickListener {

    public ViewPager pager;
    public MyPagerAdapter adapter;
    public RecyclerViewAdapter recyclerAdapter;
    public ArrayList<MyProfileLeaderBoardModel> myProfileEveryOne;
    TextView everyone, everyoneSelected, following, followingSelected, team, teamSelected,
            rankText, playerText, pointsText, levelText, everyOneSelectedText, everyOneText, followingText,
            followingSelectedText;
    Toolbar toolbar;
    ProgressDialog dialog;
    String responseString;
    Gson mGson;
    int j;
    PreferenceManager mPref;
    SportsListModel sportsListModel;
    MyProfileLeaderBoardEveryOne myProfileLeaderBoardEveryOne;
    MyProfileLeaderBoardFollowing myProfileLeaderBoardFollowing;
    MyProfileLeaderBoardTeam myProfileLeaderBoardTeam;
    ProgressBar progressBar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_leader_board);
        mPref = PreferenceManager.instance(this);

        myProfileLeaderBoardEveryOne = new MyProfileLeaderBoardEveryOne();
        myProfileLeaderBoardFollowing = new MyProfileLeaderBoardFollowing();
        myProfileLeaderBoardTeam = new MyProfileLeaderBoardTeam();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        everyone = (TextView) findViewById(R.id.everyOne);
        everyoneSelected = (TextView) findViewById(R.id.everyOneSelected);
        following = (TextView) findViewById(R.id.following);
        followingSelected = (TextView) findViewById(R.id.followingSelected);
        team = (TextView) findViewById(R.id.team);
        teamSelected = (TextView) findViewById(R.id.teamSelected);
        progressBar = (ProgressBar) findViewById(R.id.progress_sport);

        rankText = (TextView) findViewById(R.id.rank);
        playerText = (TextView) findViewById(R.id.player);
        pointsText = (TextView) findViewById(R.id.points);
        levelText = (TextView) findViewById(R.id.level);


        rankText.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        playerText.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        pointsText.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        levelText.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        everyone.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        everyoneSelected.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        following.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        followingSelected.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        teamSelected.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        team.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
        teamSelected.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));


        recyclerView = (RecyclerView) findViewById(R.id.my_profile_leader_board_chips);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(MyProfileLeaderBoardMain.this, LinearLayoutManager.HORIZONTAL, false));

        setSupportActionBar(toolbar);


        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));


        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        setupListeners();
        try {
            populateRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        pager = (ViewPager) findViewById(R.id.fragment);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(2);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        FirstFragment();
                        break;

                    case 1:
                        SecondFragment();
                        break;

                    case 2:
                        ThirdFragment();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupListeners() {
        everyone.setOnClickListener(this);
        everyoneSelected.setOnClickListener(this);
        following.setOnClickListener(this);
        followingSelected.setOnClickListener(this);
        team.setOnClickListener(this);
        teamSelected.setOnClickListener(this);

    }

    private void populateRecyclerView() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SPORTS_LIST,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("SPORTS_LIST", s);

                                    sportsListModel = new SportsListModel();
                                    mGson = new Gson();
                                    sportsListModel = mGson.fromJson(s, SportsListModel.class);
                                    Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                                    sportsListModel.getSuccess().add(0, new SportsSuccessList(999, "All sports"));
                                    if (sportsListModel.getSuccess().size() != 0) {
                                        Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                                        if (recyclerView.getAdapter() == null) {
                                            Log.e("SUCCEss", String.valueOf(sportsListModel.getSuccess()));
                                            recyclerAdapter = new RecyclerViewAdapter(MyProfileLeaderBoardMain.this, sportsListModel.getSuccess());
                                            recyclerView.setAdapter(recyclerAdapter);
                                            recyclerAdapter.notifyDataSetChanged();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.everyOne:
                FirstFragment();
                pager.setCurrentItem(0);
                break;

            case R.id.following:
                SecondFragment();
                pager.setCurrentItem(1);
                break;

            case R.id.team:
                ThirdFragment();
                pager.setCurrentItem(2);
                break;

            default:
                break;
        }

    }

    private void FirstFragment() {
        playerText.setText("PLAYER");
        everyoneSelected.setVisibility(View.VISIBLE);
        everyone.setVisibility(View.GONE);
        following.setVisibility(View.VISIBLE);
        followingSelected.setVisibility(View.GONE);
        teamSelected.setVisibility(View.GONE);
        team.setVisibility(View.VISIBLE);
    }

    private void SecondFragment() {
        playerText.setText("PLAYER");
        followingSelected.setVisibility(View.VISIBLE);
        following.setVisibility(View.GONE);
        everyoneSelected.setVisibility(View.GONE);
        everyone.setVisibility(View.VISIBLE);
        team.setVisibility(View.VISIBLE);
        teamSelected.setVisibility(View.GONE);
    }

    private void ThirdFragment() {
        playerText.setText("TEAM");
        teamSelected.setVisibility(View.VISIBLE);
        team.setVisibility(View.GONE);
        everyoneSelected.setVisibility(View.GONE);
        followingSelected.setVisibility(View.GONE);
        everyone.setVisibility(View.VISIBLE);
        following.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public class RecyclerViewAdapter extends
            RecyclerView.Adapter<RecyclerViewAdapter.SportsViewHolder> {

        Boolean[] selectedCatogorie;
        private List<SportsSuccessList> arrayList;
        private Context context;

        public RecyclerViewAdapter(Context context,
                                   List<SportsSuccessList> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
            j = arrayList.size();

            selectedCatogorie = new Boolean[j];
            for (int i = 0; i < j; i++) {
                if (i == 0) {
                    selectedCatogorie[i] = true;
                } else {
                    selectedCatogorie[i] = false;
                }

                Log.e("length", "#######" + arrayList.size());
                Log.e("length", "#######" + i + selectedCatogorie[i]);
            }

        }

        @Override
        public SportsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.sports_item_layout, viewGroup, false);
            SportsViewHolder listHolder = new SportsViewHolder(mainGroup);
            return listHolder;
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final SportsViewHolder holder, final int position) {
            final SportsSuccessList model = arrayList.get(position);

            final SportsViewHolder mainHolder = (SportsViewHolder) holder;

            try {
                mainHolder.sportsCategoryName.setTypeface(PlaythoraUtility.getFont(MyProfileLeaderBoardMain.this, RALEWAY_REGULAR));
                mainHolder.sportsCategoryName.setText(model.getName());
                Log.v("MainHolder", model.getName());

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (!selectedCatogorie[position]) {
                mainHolder.sportsDrawableLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items_normal));
                mainHolder.sportsCategoryName.setTextColor(getResources().getColor(R.color.text_one));
            } else {
                mainHolder.sportsDrawableLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                mainHolder.sportsCategoryName.setTextColor(getResources().getColor(R.color.white));
            }

            mainHolder.sportsCategoryName.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {
                    if (selectedCatogorie[position]) {
                        return;
                    }
                    Log.e("sportsid",model.getId()+"");
                    myProfileLeaderBoardEveryOne.filterSports(model.getId());
                    myProfileLeaderBoardFollowing.filterSports(model.getId());
                    myProfileLeaderBoardTeam.filterSports(model.getId());

                    for (int i = 0; i < j; i++) {
                        selectedCatogorie[i] = false;
                        if (!selectedCatogorie[i]) {
                            mainHolder.sportsDrawableLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items_normal));
                        }
                    }
                    Log.v("Booleas", String.valueOf(selectedCatogorie[position]));
                    if (!selectedCatogorie[position]) {
                        selectedCatogorie[position] = true;
                        Log.v("Booleas", String.valueOf(selectedCatogorie[position]));
                        mainHolder.sportsDrawableLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items_normal));
                        mainHolder.sportsCategoryName.setTextColor(getResources().getColor(R.color.white));
                        notifyDataSetChanged();
                    } else {
                        selectedCatogorie[position] = false;
                        Log.v("Booleas", String.valueOf(selectedCatogorie[position]));
                        mainHolder.sportsDrawableLayout.setBackground(getResources().getDrawable(R.drawable.leader_board_items));
                        notifyDataSetChanged();
                    }
                }
            });
            mainHolder.itemView.setTag(model);
        }

        public class SportsViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout sportsDrawableLayout;
            TextView sportsCategoryName;

            public SportsViewHolder(View view) {
                super(view);
                this.sportsCategoryName = (TextView) view.findViewById(R.id.sportsName);
                this.sportsDrawableLayout = (RelativeLayout) view.findViewById(R.id.sportsLayout);
            }

        }

    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Everyone", "Following", "Team"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    myProfileLeaderBoardEveryOne = new MyProfileLeaderBoardEveryOne();
                    return myProfileLeaderBoardEveryOne;

                case 1:
                    myProfileLeaderBoardFollowing = new MyProfileLeaderBoardFollowing();
                    return myProfileLeaderBoardFollowing;

                case 2:
                    myProfileLeaderBoardTeam = new MyProfileLeaderBoardTeam();
                    return myProfileLeaderBoardTeam;

                default:
                    myProfileLeaderBoardEveryOne = new MyProfileLeaderBoardEveryOne();
                    return myProfileLeaderBoardEveryOne;

            }
        }
    }
}

