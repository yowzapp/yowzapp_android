package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.AddPlayersActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hemanth on 23/11/16.
 */
public class AddPlayersFollowing extends Fragment {

    public static AddPlayersFollowingAdapter adapter;
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    TextView invite;
    PreferenceManager mPref;
    NestedScrollView nested;
    TextView noPlayers;
    ProgressBar progressBar;
    AddTeamMembersModel model;
    List<AddTeamMembersList> tempList;
    LinearLayoutManager mLayoutManager;
    private int pages = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.invite_player_layout, container, false);
        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.invite_players_recycler);
        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        noPlayers = (TextView) contentView.findViewById(R.id.noPlayers);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested_player);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    Log.e("RESPONSEPAGonLoadMoreE", String.valueOf(model.isHasMorePages()));
                    if (model.isHasMorePages()) {
                        Log.e("page_DONE", page + "");
                        pages = page;
                        followingRecycler(pages);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);

        try {
            followingRecycler(pages);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

        return contentView;

    }


    private void followingRecycler(int pages) {
        Log.e("following", "#####");
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            // params.add("user_id",mPref.getId());
            httpClient.get(Config.PLAYERS_FOLLOWING + "&page=" + pages, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);

                    try {
                        String s = new String(responseBody);
                        Log.e("following", s);

                        model = new AddTeamMembersModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, AddTeamMembersModel.class);
                        Log.e("RESPONSE", String.valueOf(model.getSuccess().getData().size()));

                        if (model.getSuccess().getData().size() != 0) {
                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }

                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerView.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                adapter = new AddPlayersFollowingAdapter(getActivity(), model.getSuccess().getData());
                                recyclerView.setAdapter(adapter);
                                // adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);

                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                noPlayers.setText("You are not following any players");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            noPlayers.setText("You are not following any players");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public class AddPlayersFollowingAdapter extends RecyclerView.Adapter<AddPlayersFollowingHolder> {

        private List<AddTeamMembersList> arrayList;
        private Context context;
        private boolean[] mCheckedState;

        public AddPlayersFollowingAdapter(Context context, List<AddTeamMembersList> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
            mCheckedState = new boolean[this.arrayList.size()];

            Log.e("ARRAYLIST", String.valueOf(this.arrayList.size()));
            //  Log.e("TEMPLIST", String.valueOf(AddMembersActivity.stringList.size()));
            Log.e("stringList", String.valueOf(AddPlayersActivity.stringList.size()));

            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < AddPlayersActivity.stringList.size(); j++) {
                    if (this.arrayList.get(i).getId() == AddPlayersActivity.stringList.get(j).getId()) {
                        //AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                        mCheckedState[i] = true;
                        //break;
                    }
                }
            }

        }

        public void Refresh(List<AddTeamMembersList> newString) {
            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }
            Log.e("stringList", String.valueOf(AddPlayersActivity.stringList.size() + "," + this.arrayList.size()));
            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < newString.size(); j++) {
                    if (this.arrayList.get(i).getId() == newString.get(j).getId()) {
                        Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                        // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                        mCheckedState[i] = true;
                        //break;
                    }
                }
            }
            notifyDataSetChanged();
        }

        public void RefreshPagination(List<AddTeamMembersList> newString, boolean pagination) {
            if (pagination) {
                mCheckedState = new boolean[this.arrayList.size()];
                this.arrayList = newString;
                for (int i = 0; i < this.arrayList.size(); i++) {
                    mCheckedState[i] = false;
                }
                Log.e("stringList", AddPlayersActivity.stringList.size() + "," + this.arrayList.size());
                for (int i = 0; i < this.arrayList.size(); i++) {
                    for (int j = 0; j < AddPlayersActivity.stringList.size(); j++) {
                        if (this.arrayList.get(i).getId() == AddPlayersActivity.stringList.get(j).getId()) {
                            Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                            // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                            mCheckedState[i] = true;
                            break;
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }


        @Override
        public AddPlayersFollowingHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.following_item_layout, viewGroup, false);
            AddPlayersFollowingHolder listHolder = new AddPlayersFollowingHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final AddPlayersFollowingHolder holder, final int position) {
            final AddTeamMembersList model = arrayList.get(position);


            final AddPlayersFollowingHolder mainHolder = (AddPlayersFollowingHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());
                if (mCheckedState[position])
                    mainHolder.checkBox.setChecked(true);
                else mainHolder.checkBox.setChecked(false);

                holder.checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mCheckedState[position]) {
                            //Toast.makeText(context,"checked",Toast.LENGTH_SHORT).show();
                            Log.e("CheckedFree", AddPlayersActivity.stringList.size() + "");
                            if (AddPlayersActivity.stringList.size() > 0) {
                                for (int j = 0; j < AddPlayersActivity.stringList.size(); j++) {
                                    if (AddPlayersActivity.stringList.get(j).getId() == arrayList.get(position).getId()) {

                                    } else {
                                        AddPlayersActivity.stringList.add(arrayList.get(position));
                                        mCheckedState[position] = true;
                                        return;
                                    }
                                }
                            } else {
                                AddPlayersActivity.stringList.add(arrayList.get(position));
                                mCheckedState[position] = true;
                            }

                            notifyDataSetChanged();

                        } else {
                            for (int i = 0; i < AddPlayersActivity.stringList.size(); i++) {
                                if (AddPlayersActivity.stringList.get(i).getId() == arrayList.get(position).getId()) {
                                    AddPlayersActivity.stringList.remove(i);
                                }
                            }
                            mCheckedState[position] = false;
                            notifyDataSetChanged();
                        }
                    }
                });

            } catch (NullPointerException e) {

            }

            try {
                if (!model.getProfile_pic().isEmpty())
                    Glide.with(context).load(model.getProfile_pic()).centerCrop().into(mainHolder.userImage);
                else holder.userImage.setImageResource(R.drawable.circled_user);
            } catch (Exception e) {
                e.printStackTrace();
                holder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setTag(model);

        }
    }

    public class AddPlayersFollowingHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView name;
        CheckBox checkBox;

        public AddPlayersFollowingHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
            this.name = (TextView) view.findViewById(R.id.userName);
            this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        }
    }
}
