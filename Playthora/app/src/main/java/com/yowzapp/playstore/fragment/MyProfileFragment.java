package com.yowzapp.playstore.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.AppSettingsActivity;
import com.yowzapp.playstore.activty.MyProfileLeaderBoardMain;
import com.yowzapp.playstore.activty.MyTransactionsActivity;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.activty.ProfileBadges;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 16/11/16.
 */

public class MyProfileFragment extends Fragment {

    TextView txtProfileName, txtProfileLevel, txtMyTransactions, txtMyPoints, txtMyPreferences, txtAppSettings;
    RelativeLayout myTransactions, myLeaderBoard, myPreference, settings;
    Button viewProfile;
    CircleImageView profileImage;
    PreferenceManager mPref;
    String string;
    View view;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view==null) {
            view = inflater.inflate(R.layout.my_profile_layout, container, false);
            mPref = PreferenceManager.instance(getActivity());

            txtProfileName = (TextView) view.findViewById(R.id.profileName);
            txtProfileLevel = (TextView) view.findViewById(R.id.profileLevel);
            txtMyTransactions = (TextView) view.findViewById(R.id.myTransactions);
            txtMyPoints = (TextView) view.findViewById(R.id.myPoints);
            txtMyPreferences = (TextView) view.findViewById(R.id.myPreferences);
            txtAppSettings = (TextView) view.findViewById(R.id.appSettings);
            myTransactions = (RelativeLayout) view.findViewById(R.id.myTransactionLayout);
            myLeaderBoard = (RelativeLayout) view.findViewById(R.id.myPointsLayout);
            myPreference = (RelativeLayout) view.findViewById(R.id.myPreferencesLayout);
            settings = (RelativeLayout) view.findViewById(R.id.appSettingsLayout);
            viewProfile = (Button) view.findViewById(R.id.btn_view_profile);
            profileImage = (CircleImageView) view.findViewById(R.id.profile_image);

            txtProfileName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
            txtProfileLevel.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            txtMyTransactions.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            txtMyPoints.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            txtMyPreferences.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            txtAppSettings.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            viewProfile.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

            myTransactions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent transaction = new Intent(getActivity(), MyTransactionsActivity.class);
                    startActivity(transaction);
                }
            });

            myLeaderBoard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent leaderboard = new Intent(getActivity(), MyProfileLeaderBoardMain.class);
                    startActivity(leaderboard);
                }
            });

            myPreference.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent preference = new Intent(getActivity(), ProfileBadges.class);
                    preference.putExtra("user_id", mPref.getId());
                    preference.putExtra("name", mPref.getName());
                    preference.putExtra("admin", "true");
                    startActivity(preference);
                }
            });

            viewProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent preference = new Intent(getActivity(), PlayerProfileViewActivity.class);
                    preference.putExtra("Id", mPref.getId());
                    preference.putExtra("admin", "true");
                    startActivity(preference);
                }
            });

            settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent preference = new Intent(getActivity(), AppSettingsActivity.class);
                    startActivity(preference);
                }
            });
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        if(view.getParent()!=null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            Glide.with(getActivity()).load(mPref.getProfilePic()).error(R.drawable.circled_user).centerCrop().into(profileImage);
        } catch (Exception e){
            profileImage.setImageResource(R.drawable.circled_user);
        }
        txtProfileName.setText(mPref.getName());
    }
}
