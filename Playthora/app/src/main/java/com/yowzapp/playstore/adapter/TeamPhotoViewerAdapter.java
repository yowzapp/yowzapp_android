package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.TeamPhotoViewer;
import com.yowzapp.playstore.model.TeamPhotoModel;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by bharat on 16/4/18.
 *
 *
 */
public class TeamPhotoViewerAdapter extends PagerAdapter {
    Context ctx;
    int imgpos;
    ArrayList<TeamPhotoModel> image;
    private  LayoutInflater layoutInflater;

    public TeamPhotoViewerAdapter(TeamPhotoViewer teamPhotoViewer, ArrayList<TeamPhotoModel> model) {
        this.ctx = teamPhotoViewer;
        this.image = model;
    }

    @Override
    public int getCount() {
        Log.e("imgrespsiz",image.size()+"");
        return image.size();
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final TeamPhotoModel album = image.get(position);
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View item_view = layoutInflater.inflate(R.layout.teamphoto_viewer_item, container, false);

        ImageView recycler_img = (ImageView)item_view.findViewById(R.id.img_full_items);
       // ImageView delete_img = (ImageView)item_view.findViewById(R.id.clear_image);

        Log.e("imgresp",album.getImage());
//        Glide.with(ctx).load(album.getImage()).fitCenter().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(recycler_img);
        Glide.with(ctx).load(album.getImage()).fitCenter().into(recycler_img);
        container.addView(item_view);
        return item_view;
       // delete_img.setOnClickListener(new View.OnClickListener() {
           // @Override
           // public void onClick(View v) {
             //   Toast.makeText(ctx,position,Toast.LENGTH_SHORT).show();
                /*Log.e("delpos",position+"");
                image.remove(position);
                notifyDataSetChanged();
                Intent deleted = new Intent();
                deleted.putExtra("image_id",String.valueOf(album.getId()));
                Log.e("deletedsecond",album.getId()+"");
                ((TeamPhotoViewer)ctx).setResult(19,deleted);
                ((TeamPhotoViewer)ctx).finish();*/
           // }
        //});



    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
