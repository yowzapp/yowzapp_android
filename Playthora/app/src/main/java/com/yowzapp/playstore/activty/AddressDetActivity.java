package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class AddressDetActivity extends BaseActivity {
    Toolbar toolbar;
    TextView title, editaddress, removeaddress, addAddress, addressTitle, addressdetail;
    private Button redeem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_det);
        toolbar = (Toolbar) findViewById(R.id.toolbar_address_det);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ImageView cartGo = (ImageView) findViewById(R.id.cart_go);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        cartGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(AddressDetActivity.this, CartActivity.class);
                startActivity(login);
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Initialize();
        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(AddressDetActivity.this, RewardSuccessActivity.class);
                startActivity(login);
            }
        });
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(AddressDetActivity.this, CreateAddressActivity.class);
                startActivity(login);
            }
        });

    }

    private void Initialize() {

        title = (TextView) findViewById(R.id.address_text);
        editaddress = (TextView) findViewById(R.id.edit_address);
        removeaddress = (TextView) findViewById(R.id.remove_address);
        addAddress = (TextView) findViewById(R.id.add_address);
        addressTitle = (TextView) findViewById(R.id.address_title);
        addressdetail = (TextView) findViewById(R.id.address_detail);
        redeem = (Button) findViewById(R.id.redeem_btn);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        editaddress.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        removeaddress.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        addAddress.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        addressTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        addressdetail.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        redeem.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));

    }
}
