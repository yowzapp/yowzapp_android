package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 26/12/16.
 */
public class AddTeamMembersModel {
    private String status;
    private int statusCode;
    private String message;
    private AddTeamMemberSuccessList success;
    private int currentPage;
    private boolean hasMorePages;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddTeamMemberSuccessList getSuccess() {
        return success;
    }

    public void setSuccess(AddTeamMemberSuccessList success) {
        this.success = success;
    }
}
