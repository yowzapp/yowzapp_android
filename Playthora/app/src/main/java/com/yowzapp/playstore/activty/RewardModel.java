package com.yowzapp.playstore.activty;

/**
 * Created by pramod on 9/11/16.
 */

public class RewardModel {

    private String product;
    private String image;
    private String rewardXp;
    private String categories;
    private String productId;
    private String categoryId;


    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRewardXp() {
        return rewardXp;
    }

    public void setRewardXp(String rewardXp) {
        this.rewardXp = rewardXp;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
