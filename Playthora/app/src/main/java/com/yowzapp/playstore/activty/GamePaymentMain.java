package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.razorpay.PaymentResultListener;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.request.payment.Address;
import com.telr.mobile.sdk.entity.request.payment.App;
import com.telr.mobile.sdk.entity.request.payment.Billing;
import com.telr.mobile.sdk.entity.request.payment.MobileRequest;
import com.telr.mobile.sdk.entity.request.payment.Name;
import com.telr.mobile.sdk.entity.request.payment.Tran;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 25/11/16.
 */

public class GamePaymentMain extends BaseActivity implements  PaymentResultListener {

    public static final String KEY = "dsczW@4L5s3#hvWd";           // TODO: Insert your Key here
    public static final String STORE_ID = "19312 - Zoe FZ LLC";         // TODO: Insert your Store ID here
    public static final boolean isSecurityEnabled = true;      // Mark false to test on simulator, True to test on actual device and Production
    private static final String TAG = "GamePaymentMain";
    TextView howWouldyou, payFull, payFullDescription, inviteUserToPay, inviteUserDescription;
    RelativeLayout payFullLayout, inviteUserToPayLayout;
    Toolbar toolbar;
    PreferenceManager mPref;
    String getDate, getGameId, getAmount, getResponse, howToPay, rawdate;
    int sendAmount = 0, finalGroundAmount;
    String admin, name, sportsName, location, date, startTime, endTime, discription, coverPic, isPrivate, gameDetailResponse;
    Intent i;
    String firstName, lastName;
    String isInvitePay = "";
    boolean isUserAlone;
    private String amount = "0"; // Just for testing

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_payment_main);

        mPref = PreferenceManager.instance(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Payment");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(GamePaymentMain.this, RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        //   progressBar = (ProgressBar) findViewById(R.id.progressBarInterest);
        //   showText = (TextView) findViewById(com.feetapart.yowzapp.R.id.noItems);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        howWouldyou = (TextView) findViewById(R.id.howWouldYouLikeToPay);
        payFull = (TextView) findViewById(R.id.payFull);
        payFullDescription = (TextView) findViewById(R.id.payFullDescription);
        inviteUserToPay = (TextView) findViewById(R.id.inviteUser);
        inviteUserDescription = (TextView) findViewById(R.id.inviteUserDescription);
        inviteUserToPayLayout = (RelativeLayout) findViewById(R.id.inviteUserToPay);
        payFullLayout = (RelativeLayout) findViewById(R.id.payFullLayout);

        try {
            i = getIntent();
            admin = i.getStringExtra("admin");
            getResponse = i.getStringExtra("response");
            JSONObject getResponseJSON = new JSONObject(getResponse);


            if (i.hasExtra("date"))
                getDate = i.getStringExtra("date");
            else getDate = "";

            if (i.hasExtra("game_id")) {
                getGameId = i.getStringExtra("game_id");
            } else if (i.hasExtra("gameId")) {
                getGameId = i.getStringExtra("gameId");
            }

            if (i.hasExtra("rawdate"))
                rawdate = i.getStringExtra("rawdate");
            else rawdate = "";

            if (i.hasExtra("gameName"))
                name = i.getStringExtra("gameName");
            else name = "";

            if (i.hasExtra("isInvitePay"))
                isInvitePay = i.getStringExtra("isInvitePay");
            else isInvitePay = "";

            mPref.setGameId(getGameId);
            mPref.setRawDate(rawdate);
            //  getGameId = i.getStringExtra("gameId");
            getAmount = i.getStringExtra("amount");
            howToPay = i.getStringExtra("payment");
            Log.v("Date", getDate + getGameId + getAmount);
            sendAmount = Integer.parseInt(getAmount);
            finalGroundAmount = sendAmount * 100;
            Log.v("FinalGround", String.valueOf(finalGroundAmount));

            Log.e("getResponse", getResponse + "");
            Log.e("date", getDate + "");
            Log.e("gameId", getGameId + "");
            Log.e("amount", getAmount + "");
            Log.e("admin", admin + "");

            if (howToPay.equalsIgnoreCase("gameDeatil")) {
                if (admin.equalsIgnoreCase("true")) {
                    inviteUserToPayLayout.setVisibility(View.VISIBLE);
                } else {
                    inviteUserToPayLayout.setVisibility(View.GONE);
                }
            } else if (isInvitePay.equalsIgnoreCase("true")
                    || isInvitePay.equalsIgnoreCase("1")) {
                inviteUserToPayLayout.setVisibility(View.GONE);
            } else {
                inviteUserToPayLayout.setVisibility(View.VISIBLE);
            }

            if (getResponseJSON.getJSONObject("success").getJSONArray("members").length() == 0
                    || (i.hasExtra("isAlone") && i.getBooleanExtra("isAlone", false))
                    || isInvitePay.equalsIgnoreCase("true")
                    || isInvitePay.equalsIgnoreCase("1")) {
                if(getIntent().hasExtra("canInvite")
                        && getIntent().getBooleanExtra("canInvite", false)){
                    inviteUserToPayLayout.setVisibility(View.VISIBLE);
                } else {
                    inviteUserToPayLayout.setVisibility(View.GONE);
                }
            } else {
                inviteUserToPayLayout.setVisibility(View.VISIBLE);
            }
            if (i.hasExtra("gameTitle")) {
                name = i.getStringExtra("gameTitle");
                sportsName = i.getStringExtra("sportsName");
                location = i.getStringExtra("location");
                date = i.getStringExtra("date");
                startTime = i.getStringExtra("startTime");
                endTime = i.getStringExtra("endTime");
                discription = i.getStringExtra("description");
                //coverPic = i.getStringExtra("coverPic");
                isPrivate = i.getStringExtra("private");
                gameDetailResponse = i.getStringExtra("response");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        howWouldyou.setTypeface(PlaythoraUtility.getFont(GamePaymentMain.this, RALEWAY_REGULAR));
        payFull.setTypeface(PlaythoraUtility.getFont(GamePaymentMain.this, RALEWAY_REGULAR));
        payFullDescription.setTypeface(PlaythoraUtility.getFont(GamePaymentMain.this, RALEWAY_REGULAR));
        inviteUserToPay.setTypeface(PlaythoraUtility.getFont(GamePaymentMain.this, RALEWAY_REGULAR));
        inviteUserDescription.setTypeface(PlaythoraUtility.getFont(GamePaymentMain.this, RALEWAY_REGULAR));


        inviteUserToPayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toIndividual = new Intent(GamePaymentMain.this, GamePaymentRequestUser.class);
                toIndividual.putExtra("RESPONSE", getResponse);
                toIndividual.putExtra("gameId", getGameId);
                startActivity(toIndividual);
            }
        });

        payFullLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                    sendMessage();
                }
            }
        });


    }

    public void sendMessage() {
        Intent intent = new Intent(this, WebviewActivity.class);
        amount = getAmount;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String v = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        intent.putExtra(WebviewActivity.EXTRA_MESSAGE, getMobileRequest());
        intent.putExtra(WebviewActivity.SUCCESS_ACTIVTY_CLASS_NAME, "com.yowzapp.playstore.activty.PaymentSuccessActivity");
        intent.putExtra(WebviewActivity.FAILED_ACTIVTY_CLASS_NAME, "com.yowzapp.playstore.activty.PaymentFailureActivity");
        intent.putExtra(WebviewActivity.IS_SECURITY_ENABLED, isSecurityEnabled);
        startActivity(intent);
    }

    private MobileRequest getMobileRequest() {
        MobileRequest mobile = new MobileRequest();
        mobile.setStore(STORE_ID);                       // Store ID
        mobile.setKey(KEY);                              // Authentication Key : The Authentication Key will be supplied by Telr as part of the Mobile API setup process after you request that this integration type is enabled for your account. This should not be stored permanently within the App.
        App app = new App();
        app.setId("123456789");                          // Application installation ID
        app.setName("Yowzapp");                          // Application name
        app.setUser(mPref.getId());                      // Application user ID : Your reference for the customer/user that is running the App. This should relate to their account within your systems.
        app.setVersion("1.0.1");                         // Application version
        mobile.setApp(app);
        Tran tran = new Tran();
        tran.setTest("0");                              // Test mode : Test mode of zero indicates a live transaction. If this is set to any other value the transaction will be treated as a test.
        tran.setType("sale");                           /* Transaction type
                                                            'auth'   : Seek authorisation from the card issuer for the amount specified. If authorised, the funds will be reserved but will not be debited until such time as a corresponding capture command is made. This is sometimes known as pre-authorisation.
                                                            'sale'   : Immediate purchase request. This has the same effect as would be had by performing an auth transaction followed by a capture transaction for the full amount. No additional capture stage is required.
                                                            'verify' : Confirm that the card details given are valid. No funds are reserved or taken from the card.
                                                        */
        tran.setClazz("paypage");                       // Transaction class only 'paypage' is allowed on mobile, which means 'use the hosted payment page to capture and process the card details'
        tran.setCartid(String.valueOf(new BigInteger(128, new Random()))); //// Transaction cart ID : An example use of the cart ID field would be your own transaction or order reference.
        tran.setDescription(name);                      // Transaction description
        tran.setCurrency("AED");                        // Transaction currency : Currency must be sent as a 3 character ISO code. A list of currency codes can be found at the end of this document. For voids or refunds, this must match the currency of the original transaction.
        tran.setAmount(amount);                         // Transaction amount : The transaction amount must be sent in major units, for example 9 dollars 50 cents must be sent as 9.50 not 950. There must be no currency symbol, and no thousands separators. Thedecimal part must be separated using a dot.
//        tran.setRef();                                // (Optinal) Previous transaction reference : The previous transaction reference is required for any continuous authority transaction. It must contain the reference that was supplied in the response for the original transaction.
        mobile.setTran(tran);
        Billing billing = new Billing();
        Address address = new Address();
        address.setCity(mPref.getCityName());                       // City : the minimum required details for a transaction to be processed
        address.setCountry("AE");                       // Country : Country must be sent as a 2 character ISO code. A list of country codes can be found at the end of this document. the minimum required details for a transaction to be processed
        address.setRegion("Dubai");                     // Region
        address.setLine1(mPref.getLocationName());                 // Street address – line 1: the minimum required details for a transaction to be processed
        //address.setLine2("SIT G=Towe");               // (Optinal)
        //address.setLine3("SIT G=Towe");               // (Optinal)
        //address.setZip("SIT G=Towe");                 // (Optinal)
        billing.setAddress(address);
//        String userName = mPref.getName();
//        String[] splitName = userName.split(" ");
        if (mPref.getName().split("\\w+").length > 1) {
            lastName = mPref.getName().substring(mPref.getName().lastIndexOf(" ") + 1);
            firstName = mPref.getName().substring(0, mPref.getName().lastIndexOf(' '));
        } else {
            lastName = "";
            firstName = mPref.getName();
        }
        Name name = new Name();
        name.setFirst(firstName);                        // Forename : the minimum required details for a transaction to be processed
        if (lastName.isEmpty() || lastName.equals(""))
            name.setLast("");                            // Surname : the minimum required details for a transaction to be processed
        else
            name.setLast(lastName);
//        name.setFirst("Ryan");
//        name.setLast("Cooper");
        name.setTitle("");                              // Title
        billing.setName(name);

        // TODO insert customer email

        billing.setEmail(mPref.getUserEmail()); // : the minimum required details for a transaction to be processed
        mobile.setBilling(billing); // : the minimum required details for a transaction to be processed

        return mobile;
    }


    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(GamePaymentMain.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());

                try {
                    JSONObject object = new JSONObject();
                    object.put("game_id", getGameId);
                    object.put("date", rawdate);
                    object.put("transaction_id", razorpayPaymentID);

                    Log.e("JSONOBJ", object.toString());
                    StringEntity entity = new StringEntity(object.toString());
                    Log.v("Razor", Config.RAZORPAY_SUCCESS);
                    httpClient.post(GamePaymentMain.this, Config.RAZORPAY_SUCCESS, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.v("RazorRuccess", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 200) {
                                    Toast.makeText(getApplicationContext(), "Payment success", Toast.LENGTH_SHORT).show();
                                    Intent toHomeScreen = new Intent(GamePaymentMain.this, HomeScreenActivity.class);
                                    startActivity(toHomeScreen);
                                    finishAffinity();

                                } else {
                                    Toast.makeText(getApplicationContext(), "Some error occured", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.v("failure", s);
                                JSONObject object = new JSONObject(s);
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    private void razorPaymentSuccessfull(String razorpayPaymentID) throws Exception {
        Log.v("Date", getDate + getGameId);

    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */

    @Override
    public void onPaymentError(int code, String response) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
        try {
            if (code == 0) {
                Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
            }
            razorPaymentFailure();


        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private void razorPaymentFailure() throws Exception {
        Log.v("Date", getDate + getGameId);
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GamePaymentMain.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            try {
                JSONObject object = new JSONObject();
                object.put("game_id", getGameId);
                object.put("date", rawdate);
                object.put("transaction_id", "NOTAVAILABLE");

                Log.e("JSONOBJ", object.toString());
                StringEntity entity = new StringEntity(object.toString());

                httpClient.post(GamePaymentMain.this, Config.RAZORPAY_FAILURE, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.v("RAZORFAILURE", s);
                        try {
                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statusCode") == 200) {

                            } else {
                                Toast.makeText(getApplicationContext(), "Some error occured", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.v("failure", s);

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        if (i.hasExtra("gameId")) {
            //   Toast.makeText(GamePaymentMain.this,"onBackPressed",Toast.LENGTH_SHORT).show();
            onBackPressed();

        } else {

            //   Toast.makeText(GamePaymentMain.this,"home",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(GamePaymentMain.this, HomeScreenActivity.class);
            startActivity(intent);
            finish();

        }


        return super.onOptionsItemSelected(item);
    }

}
