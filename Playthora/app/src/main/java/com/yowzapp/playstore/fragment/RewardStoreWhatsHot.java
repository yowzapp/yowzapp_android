package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.ProductDetailMainActivity;
import com.yowzapp.playstore.activty.RewardModel;
import com.yowzapp.playstore.activty.RewardStoreViewAll;
import com.yowzapp.playstore.activty.RewardViewHolder;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 14/11/16.
 */

public class RewardStoreWhatsHot extends Fragment {

    public static RecyclerViewAdapter adapter;
    public static RecyclerViewCategoriesAdapter categoriesAdapter;
    public static ArrayList<RewardModel> categoryArrayList;
    Gson mGson;
    ProgressDialog dialog;
    String responseString = "", responsePoints = "";
    TextView trendingRewards, availableForYourPOints, moreCategories,
            availablePointsText, userNameText, pointsText, whatHot, emptyState;
    Button viewAll;
    private RecyclerView recyclerView, recyclerViewPoints, recyclerViewCategories;

    public RewardStoreWhatsHot() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.reward_content_main_items, container, false);

        trendingRewards = (TextView) contentView.findViewById(R.id.trendingRewards);
        availableForYourPOints = (TextView) contentView.findViewById(R.id.availableForYourPoints);
        moreCategories = (TextView) contentView.findViewById(R.id.moreCatagories);
        whatHot = (TextView) contentView.findViewById(R.id.whatsHot);
        viewAll = (Button) contentView.findViewById(R.id.viewAll);
        emptyState = (TextView) contentView.findViewById(R.id.emptyState);

        recyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_view_horizontal);
        recyclerViewPoints = (RecyclerView) contentView.findViewById(R.id.recycler_view_available_points);
        recyclerViewCategories = (RecyclerView) contentView.findViewById(R.id.recycler_view_for_categories);

        trendingRewards.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        availableForYourPOints.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        moreCategories.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        emptyState.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        viewAll.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
//        whatHot.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        recyclerView.setHasFixedSize(true);
        recyclerViewPoints.setHasFixedSize(true);
        recyclerViewCategories.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewPoints.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        try {
            populateRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            populateRecyclerViewPoints();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            populateCategories();
        } catch (Exception e) {

        }

        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toViewAll = new Intent(getActivity(), RewardStoreViewAll.class);
                startActivity(toViewAll);
            }
        });

        return contentView;
    }


    private void populateRecyclerView() throws Exception {


        try {
            responseString = "[{\n" +
                    "    \"product\": \"nike blue\",\n" +
                    "    \"productId\": 1,\n" +
                    "    \"rewardXp\": \"9000\",\n" +
                    "    \"categories\": \"nike\",\n" +
                    "    \"categoryId\": \"20\",\n" +
                    "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                    "}, {\n" +
                    "    \"product\": \"nike blak\",\n" +
                    "    \"productId\": 2,\n" +
                    "    \"rewardXp\": \"9000\",\n" +
                    "    \"categories\": \"nike\",\n" +
                    "    \"categoryId\": \"22\",\n" +
                    "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                    "}, {\n" +
                    "    \"product\": \"nike white\",\n" +
                    "    \"productId\": 3,\n" +
                    "    \"rewardXp\": \"9000\",\n" +
                    "    \"categories\": \"nike\",\n" +
                    "    \"categoryId\": \"23\",\n" +
                    "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                    "}]";

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        try {
                            if (!getActivity().isFinishing() && dialog != null) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        } catch (Exception e) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            e.printStackTrace();
                        }

                        JSONArray categoryArray = new JSONArray(responseString);
                        mGson = new Gson();
                        categoryArrayList = new ArrayList<RewardModel>();

                        for (int i = 0; i < categoryArray.length(); i++) {
                            categoryArrayList.add(mGson.fromJson(categoryArray.get(i).toString(), RewardModel.class));

                        }

                        if (!categoryArrayList.isEmpty()) {

                            if (recyclerView.getAdapter() == null) {

                                adapter = new RecyclerViewAdapter(getActivity(), categoryArrayList);
                                recyclerView.setAdapter(adapter);// set adapter on recyclerview
                                adapter.notifyDataSetChanged();

                            } else {


                            }

                        } else {

                        }


                    } catch (JsonParseException e) {

                        try {
                            if (!getActivity().isFinishing() && dialog != null) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }

                        e.printStackTrace();
                    } catch (JSONException e) {

                        try {
                            if (!getActivity().isFinishing() && dialog != null) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        } catch (Exception eee) {
                            eee.printStackTrace();
                        }

                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
        }


    }


    private void populateRecyclerViewPoints() throws Exception {


        Thread thread = new Thread(new Runnable() {

            public void run() {

                try {
                    responsePoints = "[{\n" +
                            "    \"product\": \"Nike blue\",\n" +
                            "    \"productId\": 1,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"20\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"Nike blak\",\n" +
                            "    \"productId\": 2,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"22\",\n" +
                            "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"Nike white\",\n" +
                            "    \"productId\": 3,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"23\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}]";

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                    e.printStackTrace();
                                }

                                JSONArray categoryArray = new JSONArray(responsePoints);
                                mGson = new Gson();
                                categoryArrayList = new ArrayList<RewardModel>();

                                for (int i = 0; i < categoryArray.length(); i++) {
                                    categoryArrayList.add(mGson.fromJson(categoryArray.get(i).toString(), RewardModel.class));

                                }

                                if (!categoryArrayList.isEmpty()) {

                                    if (recyclerViewPoints.getAdapter() == null) {

                                        adapter = new RecyclerViewAdapter(getActivity(), categoryArrayList);
                                        recyclerViewPoints.setAdapter(adapter);// set adapter on recyclerview
                                        adapter.notifyDataSetChanged();

                                    } else {

                                    }

                                } else {
                                    emptyState.setVisibility(View.VISIBLE);
                                    recyclerViewPoints.setBackgroundColor(getResources().getColor(R.color.white));
                                }


                            } catch (JsonParseException e) {

                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                        emptyState.setVisibility(View.VISIBLE);
                                        recyclerViewPoints.setBackgroundColor(getResources().getColor(R.color.white));
                                    }
                                } catch (Exception ee) {
                                    ee.printStackTrace();
                                }

                                e.printStackTrace();
                            } catch (JSONException e) {

                                try {
                                    emptyState.setVisibility(View.VISIBLE);
                                    recyclerViewPoints.setBackgroundColor(getResources().getColor(R.color.white));
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception eee) {
                                    eee.printStackTrace();
                                }

                                e.printStackTrace();
                            }

                        }
                    });
                } catch (Exception e) {
                }
            }
        });//
        thread.start();


    }

    private void populateCategories() throws Exception {

        Thread thread = new Thread(new Runnable() {

            public void run() {

                try {
                    responseString = "[{\n" +
                            "    \"product\": \"nike blue\",\n" +
                            "    \"productId\": 1,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"20\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike blak\",\n" +
                            "    \"productId\": 2,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"22\",\n" +
                            "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike white\",\n" +
                            "    \"productId\": 3,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"23\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}]";

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                    e.printStackTrace();
                                }


                                JSONArray categoryArray = new JSONArray(responseString);
                                mGson = new Gson();
                                categoryArrayList = new ArrayList<RewardModel>();

                                for (int i = 0; i < categoryArray.length(); i++) {
                                    categoryArrayList.add(mGson.fromJson(categoryArray.get(i).toString(), RewardModel.class));

                                }

                                if (!categoryArrayList.isEmpty()) {

                                    if (recyclerViewCategories.getAdapter() == null) {

                                        categoriesAdapter = new RecyclerViewCategoriesAdapter(getActivity(), categoryArrayList);
                                        recyclerViewCategories.setAdapter(categoriesAdapter);// set adapter on recyclerview
                                        categoriesAdapter.notifyDataSetChanged();

                                    } else {


                                    }

                                } else {

                                }


                            } catch (JsonParseException e) {

                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception ee) {
                                    ee.printStackTrace();
                                }

                                e.printStackTrace();
                            } catch (JSONException e) {

                                try {
                                    if (!getActivity().isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception eee) {
                                    eee.printStackTrace();
                                }

                                e.printStackTrace();
                            }

                        }
                    });
                } catch (Exception e) {
                }
            }
        });//
        thread.start();


    }


    public class RecyclerViewAdapter extends
            RecyclerView.Adapter<RewardViewHolder> {

        private ArrayList<RewardModel> arrayList;
        private Context context;

        public RecyclerViewAdapter(Context context,
                                   ArrayList<RewardModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.reward_trending_product_items, viewGroup, false);
            RewardViewHolder listHolder = new RewardViewHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final RewardViewHolder holder, final int position) {
            final RewardModel model = arrayList.get(position);


            final RewardViewHolder mainHolder = (RewardViewHolder) holder;

            try {
                mainHolder.productName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.xp.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.productName.setText(model.getProduct());
                mainHolder.xp.setText(model.getRewardXp() + "XP");
            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getImage()).into(mainHolder.imageview);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mainHolder.rewardLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent toProductDetail = new Intent(getActivity(), ProductDetailMainActivity.class);
                    startActivity(toProductDetail);
                }
            });


            mainHolder.itemView.setTag(model);


        }
    }


    public class RecyclerViewCategoriesAdapter extends
            RecyclerView.Adapter<RewardViewHolder> {

        private ArrayList<RewardModel> arrayList;
        private Context context;

        public RecyclerViewCategoriesAdapter(Context context,
                                             ArrayList<RewardModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.reward_more_categories, viewGroup, false);
            RewardViewHolder listHolder = new RewardViewHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final RewardViewHolder holder, final int position) {
            final RewardModel model = arrayList.get(position);


            final RewardViewHolder mainHolder = (RewardViewHolder) holder;

            try {
                mainHolder.categoryName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.categoryName.setText(model.getCategories());

            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getImage()).into(mainHolder.categoryImage);
            } catch (Exception e) {
                e.printStackTrace();
            }


            mainHolder.itemView.setTag(model);


        }


    }

}