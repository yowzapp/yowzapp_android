package com.yowzapp.playstore.activty;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.NetBankingFragment;
import com.yowzapp.playstore.utils.CustomViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 25/11/16.
 */

public class GamePayMentBanking extends BaseActivity implements  View.OnClickListener {


    public static CustomViewPager pager;
    public static MyPagerAdapter adapter;
    TextView availablePointsText, userNameText, pointsText, credit,
            creditSelected, netBanking, netBankingSelected, wallet, walletSelected;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_payment_banking);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        credit = (TextView) findViewById(R.id.credit);
        creditSelected = (TextView) findViewById(R.id.creditSelected);
        netBanking = (TextView) findViewById(R.id.netBanking);
        netBankingSelected = (TextView) findViewById(R.id.netBankingSelected);
        wallet = (TextView) findViewById(R.id.wallet);
        walletSelected = (TextView) findViewById(R.id.walletSelected);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));

        credit.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));
        creditSelected.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));
        netBanking.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));
        netBankingSelected.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));
        wallet.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));
        walletSelected.setTypeface(PlaythoraUtility.getFont(GamePayMentBanking.this, RALEWAY_REGULAR));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        setupListeners();


        pager = (CustomViewPager) findViewById(R.id.fragment);
        adapter = new MyPagerAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);
        pager.setPagingEnabled(true);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    netBankingAccessorites();
                } else if (position == 2) {
                    walletMethod();
                } else {
                    creditMethod();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupListeners() {
        credit.setOnClickListener(this);
        creditSelected.setOnClickListener(this);
        netBanking.setOnClickListener(this);
        netBankingSelected.setOnClickListener(this);
        wallet.setOnClickListener(this);
        walletSelected.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.credit:
                creditMethod();
                pager.setCurrentItem(0);

                break;

            case R.id.netBanking:
                // Toast.makeText(getApplicationContext(),"SportAccessories",Toast.LENGTH_SHORT).show();
                netBankingAccessorites();

                pager.setCurrentItem(1);

                break;

            case R.id.wallet:
                walletMethod();

                pager.setCurrentItem(2);

                break;


            default:
                break;

        }


    }

    private void walletMethod() {
        walletSelected.setVisibility(View.VISIBLE);
        wallet.setVisibility(View.GONE);
        creditSelected.setVisibility(View.GONE);
        netBankingSelected.setVisibility(View.GONE);
        credit.setVisibility(View.VISIBLE);
        netBanking.setVisibility(View.VISIBLE);
    }

    private void netBankingAccessorites() {
        netBankingSelected.setVisibility(View.VISIBLE);
        netBanking.setVisibility(View.GONE);
        creditSelected.setVisibility(View.GONE);
        credit.setVisibility(View.VISIBLE);
        wallet.setVisibility(View.VISIBLE);
        walletSelected.setVisibility(View.GONE);
    }

    private void creditMethod() {
        creditSelected.setVisibility(View.VISIBLE);
        credit.setVisibility(View.GONE);
        netBanking.setVisibility(View.VISIBLE);
        netBankingSelected.setVisibility(View.GONE);
        walletSelected.setVisibility(View.GONE);
        wallet.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }


        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"What's hot", "Sport accessories", "wallet"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new NetBankingFragment();

                case 1:
                    return new NetBankingFragment();

                case 2:
                    return new NetBankingFragment();

                default:
                    return new NetBankingFragment();

            }

        }
    }
}
