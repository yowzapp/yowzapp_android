package com.yowzapp.playstore.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.ChattingActivity;
import com.yowzapp.playstore.activty.DeleteThisActivity;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.adapter.MessageAdapter;
import com.yowzapp.playstore.application.ChatApplication;
import com.yowzapp.playstore.model.ChatHistoryModel;
import com.yowzapp.playstore.model.Message;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.yowzapp.playstore.activty.ChattingActivity.convId;
import static com.yowzapp.playstore.activty.ChattingActivity.convType;
import static com.yowzapp.playstore.activty.ChattingActivity.id;
import static com.yowzapp.playstore.activty.ChattingActivity.type;
import static com.yowzapp.playstore.utils.PlaythoraUtility.checkInternetConnection;

/**
 * Created by hemanth on 23/11/16.
 */

public class ChattingFragment extends Fragment {
    private static final int REQUEST_LOGIN = 0;
    public static EditText mInputMessageView;
    private static Socket mSocket;
    RecyclerView recyclerView;
    //int FirstLoad=30;
    //int FirstSkip=0;
    int tempToScroll = 0;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    int page = 1, previousTotal = 0;
    boolean value, value2 = true;
    TextView emptyText;
    String tempDate = "";
    String latestDate = "";
    private MessageAdapter mAdapter;
    private List<Message> mMessages = new ArrayList<Message>();
    private String mUsername;
    private PreferenceManager mpref;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean loading = true;
    private boolean temploading = true;
    private ChatHistoryModel chatHistory;
    private int visibleThreshold = 5;
    private int currentPage;
    private boolean hasMorePages;
    private JSONObject success;
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("INCOMING", args[0].toString());
                    JSONObject data = (JSONObject) args[0];
                    String userId;
                    String message;
                    String name;
                    String typetext = "";
                    String type_id;
                    try {
                        message = data.getString("message");
                        userId = data.getString("userid");
                        name = data.getString("sender_name");
                        type_id = data.getString("type_id");
                        if (data.getInt("type") == 2) {
                            typetext = "team";
                        }
                        if (data.getInt("type") == 3) {
                            typetext = "player";
                        }
                        if (data.getInt("type") == 1) {
                            typetext = "game";
                        }
                        Log.e("message", message);
                        Log.e("userid", userId);
                        Log.e("sender_name", name);
                        Log.e("type_id", type_id);
                        Log.e("type", typetext);
                    } catch (JSONException e) {
                        return;
                    }

                    // removeTyping("punith");
                    Log.e("VAAAAL", Config.CHAT_TYPE + ",,,,," + Config.CHAT_ID + ".,,,,," + typetext + ",,," + type_id);
//                    if (typetext.equalsIgnoreCase(Config.CHAT_TYPE) && type_id.equalsIgnoreCase(Config.CHAT_ID)) {
                    if (type_id.equalsIgnoreCase(Config.CHAT_ID)) {
                        if (!userId.equalsIgnoreCase(mpref.getId())) {
                            addMessage(name, message);
                        }
                    }
                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("EmitConnectErrorCht", args[0].toString());
        }
    };
    private Emitter.Listener connected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            if (!mpref.getAccessToken().isEmpty()) {
                //  final JSONObject data = (JSONObject) args[0];

                Log.e("SOSCKEKEiii", args[0].toString());
//                Log.e("SOSCKEKEiii", mPref.getAccessToken());
                mSocket.emit("connect user", mpref.getAccessToken());
//                if (mSocket.connected()) {
//                    Log.e("SOCKETIO", "TRUE");
//                    HomeScreenActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                              Toast.makeText(getApplicationContext(),"emitter connected",Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//                } else {
//                    Log.e("SOCKETIO", "FALSE");
//                    HomeScreenActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                             Toast.makeText(getApplicationContext(),"emitter failed",Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//                }
//                try {
//                    HomeScreenActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                             Toast.makeText(getApplicationContext(), "emitter Data:" , Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                } catch (Exception e) {
//
//                }


            }
        }
    };
    private Emitter.Listener userConnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!mpref.getAccessToken().isEmpty()) {

                //  final JSONObject data = (JSONObject) args[0];

                Log.e("SOSCKEKE", args[0].toString());
//                mSocket.emit("user", mPref.getAccessToken());
//                if (mSocket.connected()) {
//                    Log.e("SOCKETIO", "TRUE");
//                    HomeScreenActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getApplicationContext(),"user connected",Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//                } else {
//                    Log.e("SOCKETIO", "FALSE");
//                    HomeScreenActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                             Toast.makeText(getApplicationContext(),"user connect failed",Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//                }
//                try {
//                    HomeScreenActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                             Toast.makeText(getApplicationContext(), "user Data:" , Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                } catch (Exception e) {
//
//                }
            }

        }
    };

    public ChattingFragment() {
        super();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mAdapter = new MessageAdapter(activity, mMessages, type);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mpref = PreferenceManager.instance(getActivity());

        // SocketMainClass app = new SocketMainClass(getActivity());
        ChatApplication app = (ChatApplication) getActivity().getApplication();
        mSocket = app.getSocket();

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);

        if (!mSocket.connected()) {
            mSocket.connect();
        }

        mSocket.on("connected", connected);
        mSocket.on("user connected", userConnected);
        mSocket.emit("connect user", mpref.getAccessToken());

        GetDetails();
    }

    private void GetDetails() {
        Intent intent = new Intent(getActivity(), DeleteThisActivity.class);
        startActivityForResult(intent, REQUEST_LOGIN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chatting_fragment_layout, container, false);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK != resultCode) {
            getActivity().finish();
            return;
        }
        getProductsList(page, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        emptyText = (TextView) view.findViewById(R.id.empty_chat);
        recyclerView = (RecyclerView) view.findViewById(R.id.chattingRecyclerView);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        mLinearLayoutManager.setReverseLayout(true);
        //mLinearLayoutManager.setStackFromEnd(false);

        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (value) {
                    Log.e("GGGGGGGGGGG", "VVVVVVVVVV" + (mMessages.size() - 1));
                    Log.e("GGGGGGGGGGG", "VVVVVVVVVV" + value2);
                    //recyclerView.scrollToPosition(mMessages.size() - 1);
                    /*if(value2) {
                       // recyclerView.scrollToPosition(10);
                        value2 = false;
                    }
                    else {
                        value2 = false;
                        //recyclerView.scrollToPosition(mMessages.size() - 1);
                    }*/
                } else {
                    Log.e("GGGGGGGGGGG", "EEEEEEEEEEE");
                    recyclerView.scrollToPosition(0);
                }
            }
        });
        mInputMessageView = (EditText) view.findViewById(R.id.message_input);
        mInputMessageView.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    attemptSend();
                    return true;
                }
                return false;
            }
        });
        Button sendButton = (Button) view.findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Log.e("SCROLL","SCROLL");
                /*visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

                *//*Log.e("I  visiblecounr" + visibleItemCount, "OKies");
                Log.e("I  totalItemCount" + totalItemCount, "OKies");
                Log.e("I  firstVisibleItem" + firstVisibleItem, "OKies");*//*

                if(chatHistory.isHasMorePages()) {
                    if (loading) {
                        if (temploading) {
                            if (firstVisibleItem == 0) {
                                firstVisibleItem = 10;
                                //FirstLoad = FirstLoad + 30;
                                //FirstSkip = FirstSkip + 20;
                                loading = false;
                                //mMessages = new ArrayList<Message>();
                                page += 1;
                                getProductsList( page,true);
                            }
                        }
                    }
                }*/
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("SCROLL", "SCROLLED");

                /*visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                //Log.e("isHasMorePages", String.valueOf(notificationArrayList.isHasMorePages()));
                try {
                    if (chatHistory.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                page += 1;
                                try {
                                    //paginateProgressBar.setVisibility(View.VISIBLE);
                                    getProductsList( page,true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //paginateProgressBar.setVisibility(View.GONE);
                                }
                                //adapter.notifyDataSetChanged();

                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }
*/

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();

               /* Log.e("I  visiblecounr" + visibleItemCount, "OKies");
                Log.e("I  totalItemCount" + totalItemCount, "OKies");
                Log.e("I  firstVisibleItem" + firstVisibleItem, "OKies");*/

                if (hasMorePages) {
                    if (loading) {
                        if (temploading) {
                            if (firstVisibleItem == totalItemCount - 1) {
                                //firstVisibleItem = 10;
                                //FirstLoad = FirstLoad + 30;
                                //FirstSkip = FirstSkip + 20;
                                loading = false;
                                //mMessages = new ArrayList<Message>();
                                page++;
                                value = true;
                                getProductsList(page, true);
                            }
                        }
                    }
                }

            }
        });
    }

    private void getProductsList(int page, final boolean isPagination) {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading..Please wait!");
//                Log.e("URL", Config.CHAT_HISTORY + "?type=" + ChattingActivity.convType + "&id=" + ChattingActivity.convId + "&page=" + page);
//                Log.e("URL2", Config.CHAT_HISTORY + "?type=" + type + "&id=" + id + "&page=" + page);
                String URL;
                if (convId == 0 && convType == 0) {
                    URL = Config.CHAT_HISTORY + "?type=" + type + "&id=" + id + "&page=" + page;
                } else {
                    URL = Config.CHAT_HISTORY + "?type=" + convType + "&id=" + convId + "&page=" + page;
                }
                Log.e("URL", URL);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mpref.getAccessToken());

                httpClient.get(URL, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        dialog.dismiss();
                        try {
                            String response = new String(responseBody);
                            Log.e("CHAT_HISTORY_RESPO", response);

                            try {
                                //temploading = false;

                                // ArrayList<NewProductModel> productArrayList = new ArrayList<>();
                                Gson gson = new Gson();
                                chatHistory = gson.fromJson(response, ChatHistoryModel.class);
                                Log.e("isHasMorePages", chatHistory.isHasMorePages() + "");

                                if (!chatHistory.isHasMorePages()) {
                                    hasMorePages = false;
                                    loading = false;
                                    temploading = false;
                                } else {
                                    hasMorePages = true;
                                    loading = true;
                                    temploading = true;
                                }


                                if (chatHistory.getSuccess().size() > 0) {
                                    emptyText.setVisibility(View.GONE);
                                    if (chatHistory.getCurrentPage() > 1) {
                                        List<Message> mMessagesNew = new ArrayList<Message>();
                                        for (int i = 0; i < chatHistory.getSuccess().size(); i++) {
                                            // Log.e("Val",chatHistory.getSuccess().get(i).getUser_id()+","+mpref.getId());
                                            if (checkDates(tempDate, chatHistory.getSuccess().get(i).getDate())) {
                                                mMessagesNew.add(new Message.Builder(Message.TYPE_DATE)
                                                        .date(tempDate).build());
                                                tempDate = chatHistory.getSuccess().get(i).getDate();
                                            }
                                            if (Integer.toString(chatHistory.getSuccess().get(i).getUser_id()).equalsIgnoreCase(mpref.getId())) {
                                                mMessagesNew.add(new Message.Builder(Message.TYPE_MESSAGE)
                                                        .username(chatHistory.getSuccess().get(i).getDate()).message(chatHistory.getSuccess().get(i).getMessage()).build());
                                            } else {
                                                mMessagesNew.add(new Message.Builder(Message.TYPE_LOG).name(chatHistory.getSuccess().get(i).getSender_name())
                                                        .username(chatHistory.getSuccess().get(i).getDate()).message(chatHistory.getSuccess().get(i).getMessage()).build());
                                            }
                                        }
                                        if (!hasMorePages) {
                                            mMessagesNew.add(new Message.Builder(Message.TYPE_DATE)
                                                    .date(tempDate).build());
                                        }
                                        mAdapter.RefreshList(mMessagesNew, type);
                                        mAdapter.notifyItemInserted(mMessagesNew.size() - 1);
                                        //temploading = true;
                                        if (chatHistory.getCurrentPage() == 2) {
                                            tempToScroll = mMessages.size() + mMessagesNew.size();
                                            recyclerView.scrollToPosition(tempToScroll);
                                        }
                                    } else {
                                        mSocket.on("chat message", onNewMessage);
                                        for (int i = 0; i < chatHistory.getSuccess().size(); i++) {
                                            // Log.e("Val",chatHistory.getSuccess().get(i).getUser_id()+","+mpref.getId());
                                            if (i == 0) {
                                                tempDate = chatHistory.getSuccess().get(0).getDate();
                                                latestDate = tempDate;
                                            }
                                            if (checkDates(tempDate, chatHistory.getSuccess().get(i).getDate())) {
                                                mMessages.add(new Message.Builder(Message.TYPE_DATE)
                                                        .date(tempDate).build());
                                                tempDate = chatHistory.getSuccess().get(i).getDate();
                                            }

                                            if (Integer.toString(chatHistory.getSuccess().get(i).getUser_id()).equalsIgnoreCase(mpref.getId())) {
                                                mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                                                        .username(chatHistory.getSuccess().get(i).getDate()).message(chatHistory.getSuccess().get(i).getMessage()).build());
                                            } else {
                                                mMessages.add(new Message.Builder(Message.TYPE_LOG).name(chatHistory.getSuccess().get(i).getSender_name())
                                                        .username(chatHistory.getSuccess().get(i).getDate()).message(chatHistory.getSuccess().get(i).getMessage()).build());

                                            }
                                        }
                                        if (!hasMorePages) {
                                            mMessages.add(new Message.Builder(Message.TYPE_DATE)
                                                    .date(tempDate).build());
                                        }
                                        mAdapter = new MessageAdapter(getActivity(), mMessages, type);
                                        recyclerView.setLayoutManager(mLinearLayoutManager);
                                        recyclerView.setAdapter(mAdapter);
                                        tempToScroll = mMessages.size() - tempToScroll;
                                        recyclerView.scrollToPosition(tempToScroll);
                                    }
                                } else {
                                    mSocket.on("chat message", onNewMessage);
                                    emptyText.setVisibility(View.VISIBLE);
                                }


                            } catch (JsonParseException e) {
                                e.printStackTrace();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable error) {
                        dialog.dismiss();
                        mSocket.on("chat message", onNewMessage);
                        try {
                            String s = new String(bytes);
                            Log.e("player", s);
                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                mpref.setAccessToken("");
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                getActivity().finishAffinity();
                            } else
                                Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                String s = new String(bytes);
                                Log.d("myGamefailure", s);

                                JSONObject object = new JSONObject(s);
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    public boolean checkDates(String datestring, String datestring2) {
        Date date = null;
        try {
            if (!datestring.isEmpty() && !datestring.equals("")) {
                date = PlaythoraUtility.dateFormatnew.parse(datestring);
            } else {
                date = PlaythoraUtility.dateFormatnew.parse(datestring2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date date2 = null;
        try {
            date2 = PlaythoraUtility.dateFormatnew.parse(datestring2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return daysBetween(date2, date) != 0;
    }

    public int daysBetween(Date d2, Date d1) {
        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = targetFormat.format(d2);
        String formattedDate1 = targetFormat.format(d1);
        try {
            d2 = targetFormat.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            d1 = targetFormat.parse(formattedDate1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    private void addMessage(String username, String message) {
        value = false;
//        if (checkDates(latestDate, PlaythoraUtility.getCurrentDate())) {
        if (latestDate.equals("") || checkDates(latestDate, PlaythoraUtility.getCurrentDate())) {
            mMessages.add(0, new Message.Builder(Message.TYPE_DATE)
                    .date(PlaythoraUtility.getCurrentDate()).build());
            mAdapter.notifyItemInserted(0);
            latestDate = PlaythoraUtility.getCurrentDate();
        }
        mMessages.add(0, new Message.Builder(Message.TYPE_LOG).name(username)
                .username(PlaythoraUtility.getCurrentDate()).message(message).build());
        mAdapter.notifyItemInserted(0);
        emptyText.setVisibility(View.GONE);
        scrollToBottom();
    }

    private void addMyMessage(String username, String message) {
        Log.e("Its Here", "" + PlaythoraUtility.getCurrentDate());
        value = false;
//        if (checkDates(latestDate, PlaythoraUtility.getCurrentDate())) {
        if (latestDate.equals("") || checkDates(latestDate, PlaythoraUtility.getCurrentDate())) {
            mMessages.add(0, new Message.Builder(Message.TYPE_DATE)
                    .date(PlaythoraUtility.getCurrentDate()).build());
            mAdapter.notifyItemInserted(0);
            latestDate = PlaythoraUtility.getCurrentDate();
        }
        mMessages.add(0, new Message.Builder(Message.TYPE_MESSAGE)
                .username(PlaythoraUtility.getCurrentDate()).message(message).build());
        //mAdapter.notifyItemInserted(mMessages.size() - 1);
        mAdapter.notifyItemInserted(0);
        emptyText.setVisibility(View.GONE);
        scrollToBottom();
    }

    private void scrollToBottom() {
        recyclerView.scrollToPosition(0);
    }

    private void attemptSend() {
        if (checkInternetConnection(getContext())) {

            if (!mSocket.connected()) {
                mSocket.connect();
            }

            String message = mInputMessageView.getText().toString().trim();
            if (TextUtils.isEmpty(message)) {
                mInputMessageView.requestFocus();
                return;
            }
            JSONObject jo = new JSONObject();
            try {
                jo.put("message", message);
                Log.e("message", message);
                if (type.equalsIgnoreCase("player") || type.equalsIgnoreCase("3")) {
                    jo.put("type", 3);
                    Log.e("type", "3");
                    jo.put("type_id", ChattingActivity.id);
                }
                if (type.equalsIgnoreCase("team") || type.equalsIgnoreCase("2")) {
                    jo.put("type", 2);
                    Log.e("type", "2");
                    jo.put("type_id", ChattingActivity.id);
                }
                if (type.equalsIgnoreCase("game") || type.equalsIgnoreCase("1")) {
                    jo.put("type", 1);
                    Log.e("type", "1");
                    jo.put("type_id", ChattingActivity.id);
                }
                Log.e("type_id", ChattingActivity.id);
            /*jo.put("type", 3);
            jo.put("type_id",6);*/

                jo.put("userid", mpref.getId());
                Log.e("user_id", mpref.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //  Log.e("Key", key);

            mInputMessageView.setText("");
            addMyMessage(mpref.getName(), message);

            // perform the sending message attempt.
            mSocket.emit("chat message", jo, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("Emitter ACKNOWLEDGEMENT", args[0].toString());
                }
            });
            Log.e("CHAT OBJECT", jo.toString());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("chat message", onNewMessage);

        // mSocket.off("newchatroom", newChatRoom);
        // mSocket.off("message", newChatRoom);
    }

   /* private Emitter.Listener connected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //  final JSONObject data = (JSONObject) args[0];

            Log.e("SOSCKEKEiii",args[0].toString());
            Log.e("SOSCKEKEiii", mpref.getAccessToken());
            mSocket.emit("connect user", mpref.getAccessToken());
            if(mSocket.connected()){
                Log.e("SOCKETIO","TRUE") ;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //  Toast.makeText(getApplicationContext(),"connected",Toast.LENGTH_SHORT).show();
                    }
                });

            }else{
                Log.e("SOCKETIO","FALSE") ;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Toast.makeText(getApplicationContext(),"failed",Toast.LENGTH_SHORT).show();
                    }
                });

            }
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Toast.makeText(getApplicationContext(), "Data:" , Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (Exception e){

            }



        }
    };*/

    private void scrollToMid() {
        recyclerView.scrollToPosition(mAdapter.getItemCount() - tempToScroll);
        new CountDownTimer(3000, 1000) {
            public void onFinish() {
                // When timer is finished
                // Execute your code here
                //loading=true;
            }

            public void onTick(long millisUntilFinished) {
                // millisUntilFinished    The amount of time until finished.
            }
        }.start();

    }

}