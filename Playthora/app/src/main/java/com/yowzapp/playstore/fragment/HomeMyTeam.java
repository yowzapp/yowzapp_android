package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.activty.OtherTeamViewActivity;
import com.yowzapp.playstore.model.HomeMyTeamList;
import com.yowzapp.playstore.model.HomeTeamModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import okhttp3.MediaType;

/**
 * Created by vaishakha on 15/11/16.
 */
public class


HomeMyTeam extends Fragment {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    View contentView;
    RecyclerView myTeamRecyclerView, allTeamRecyclerView;
    Gson gson;
    HomeTeamModel model;
    TeamAdapter adapterMyTeam, adapterAllTeam;
    String response;
    TextView myTeam, allTeam, empty_text;
    PreferenceManager mPref;
    ProgressBar progressBar, paginateProgressBar;
    List<HomeMyTeamList> allteam;
    NestedScrollView nested;
    private GridLayoutManager mGridLayoutManager;
    private int pages = 1;
    private SwipeRefreshLayout swipeContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.games_layout, container, false);

        initializeAllComponents();
        mPref = PreferenceManager.instance(getActivity());

        myTeamRecyclerView.setPadding(8, 0, 8, 0);
        allTeamRecyclerView.setPadding(8, 0, 8, 0);

        myTeam.setVisibility(View.GONE);
        allTeam.setVisibility(View.GONE);

        myTeam.setText("My Teams");
        allTeam.setText("All Teams");

        myTeam.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_BOLD));
        allTeam.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_BOLD));
        empty_text.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        // recyclerView = (RecyclerView) contentView.findViewById(R.id.team_recycler);
        //emptyText = (TextView) contentView.findViewById(R.id.tv_empty_state);

        //recyclerView.setHasFixedSize(true);
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        mGridLayoutManager = new GridLayoutManager(getActivity(), 2);
        myTeamRecyclerView.setHasFixedSize(true);
        myTeamRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        allTeamRecyclerView.setHasFixedSize(true);
        allTeamRecyclerView.setLayoutManager(mGridLayoutManager);
        myTeamRecyclerView.setFocusable(false);
        allTeamRecyclerView.setFocusable(false);

        //HomeFragment.fabAdd.attachToRecyclerView(myTeamRecyclerView);

        swipeContainer = (SwipeRefreshLayout) contentView.findViewById(R.id.swipeContainer);

        final FloatingActionButton floatingActionButton = HomeFragment.getFloatingActionButton();
        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(mGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    Log.e("RESPONSEPAGonLoadMoreE", String.valueOf(model.isHasMorePages()));
                    if (model.isHasMorePages()) {
                        Log.e("page_DONE", page + "");
                        paginateProgressBar.setVisibility(View.VISIBLE);
                        pages = page;
                        LoadAllTeam(pages, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    paginateProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
                if (floatingActionButton != null) {
                    floatingActionButton.hide();
                }
            }

            @Override
            public void scrollUp() {
                if (floatingActionButton != null) {
                    floatingActionButton.show();
                }
            }
        };
        nested.setOnScrollChangeListener(endlessParentScrollListener);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    LoadAllTeam(1, true);
                    endlessParentScrollListener.currentPage = 1;
                    endlessParentScrollListener.previousTotalItemCount = 0;
                    endlessParentScrollListener.loading = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    swipeContainer.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    paginateProgressBar.setVisibility(View.GONE);
                }
            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);
        try {
            LoadAllTeam(pages, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contentView;
    }

    private void initializeAllComponents() {
        myTeamRecyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_my_games);
        allTeamRecyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_all_games);
        myTeam = (TextView) contentView.findViewById(R.id.tv_my_games);
        allTeam = (TextView) contentView.findViewById(R.id.tv_all_games);
        myTeamRecyclerView.setNestedScrollingEnabled(false);
        allTeamRecyclerView.setNestedScrollingEnabled(false);
        empty_text = (TextView) contentView.findViewById(R.id.empty_team);
        progressBar = (ProgressBar) contentView.findViewById(R.id.progress_bar_game);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested);
        paginateProgressBar = (ProgressBar) contentView.findViewById(R.id.pagination_progress_bar_two);
        empty_text.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

    }

    private void LoadAllTeam(final int pages, final boolean swipe) {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                if (!swipe)
                    progressBar.setVisibility(View.VISIBLE);
                else Log.e("RELOAD", "RELOAD");
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.TEAM_LIST_TWO + "?page=" + pages,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                swipeContainer.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSETEAM", s);
                                    Log.e("RANK", "6");

                                    gson = new Gson();
                                    model = new HomeTeamModel();
                                    model = gson.fromJson(s, HomeTeamModel.class);
                                    Log.e("RESPONSEALLs", String.valueOf(model.getSuccess().getAll_teams().size()));
                                    Log.e("RESPONSEMYs", String.valueOf(model.getSuccess().getMy_teams().size()));
                                    Log.e("RESPONSEPAGE", String.valueOf(model.isHasMorePages()));

                                    if (model.getCurrentPage() > 1) {
                                        allTeam.setVisibility(View.VISIBLE);
                                        for (int i = 0; i < model.getSuccess().getAll_teams().size(); i++) {
                                            allteam.add(model.getSuccess().getAll_teams().get(i));
                                        }
                                        Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                        adapterAllTeam.loadData(allteam);

                                    } else {
                                        if (model.getSuccess().getAll_teams().size() == 0 && model.getSuccess().getMy_teams().size() == 0) {
                                            empty_text.setVisibility(View.VISIBLE);
                                            empty_text.setText("Start adding teams");
                                        } else empty_text.setVisibility(View.GONE);

                                        if (model.getSuccess().getAll_teams().size() == 0) {
                                            allTeam.setVisibility(View.GONE);
                                            allTeamRecyclerView.setAdapter(null);
                                        } else {
                                            allTeam.setVisibility(View.VISIBLE);
                                            if (allTeamRecyclerView.getAdapter() == null) {
                                                allteam = model.getSuccess().getAll_teams();
                                                adapterAllTeam = new TeamAdapter(getActivity(), model.getSuccess().getAll_teams(), false);
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        allTeamRecyclerView.setAdapter(adapterAllTeam);
                                                    }
                                                });
                                            } else {
                                                allteam = model.getSuccess().getAll_teams();
                                                adapterAllTeam.Refresh(model.getSuccess().getAll_teams());
                                            }
                                        }

                                        if (model.getSuccess().getMy_teams().size() == 0) {
                                            myTeam.setVisibility(View.GONE);
                                            myTeamRecyclerView.setAdapter(null);
                                        } else {
                                            myTeam.setVisibility(View.VISIBLE);
                                            if (myTeamRecyclerView.getAdapter() == null) {
                                                adapterMyTeam = new TeamAdapter(getActivity(), model.getSuccess().getMy_teams(), true);
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        myTeamRecyclerView.setAdapter(adapterMyTeam);
                                                    }
                                                });
                                            } else {
                                                adapterMyTeam.Refresh(model.getSuccess().getMy_teams());
                                            }
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                swipeContainer.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
                paginateProgressBar.setVisibility(View.GONE);
            }
        } else {
            swipeContainer.setRefreshing(false);
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
            paginateProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("RESULT", requestCode + " " + resultCode);
        if (requestCode == 3) {
            Log.e("EDITED", String.valueOf(OtherTeamViewActivity.edited));
            if (OtherTeamViewActivity.edited)
                LoadAllTeam(pages, false);
        }
    }

   /* @Override
    public void onResume() {
        super.onResume();
        try {
            if(OtherTeamViewActivity.edited)
                LoadAllTeam();
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamListHolder> {
        Context context;
        List<HomeMyTeamList> model;
        boolean isAllTeam;

        public TeamAdapter(FragmentActivity activity, List<HomeMyTeamList> model, boolean b) {
            context = activity;
            this.model = model;
            isAllTeam = b;
        }

        @Override
        public TeamAdapter.TeamListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_item_layout, null);
            return new TeamListHolder(layoutView);
        }

        public void Refresh(List<HomeMyTeamList> model) {
            this.model = model;
            notifyDataSetChanged();
        }

        public void loadData(List<HomeMyTeamList> arrayList) {
            Log.e("LISTING", "LISTING");
            this.model = arrayList;
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(TeamAdapter.TeamListHolder holder, final int position) {

            holder.mTeamName.setText(model.get(position).getName());
            holder.mMembers.setText(model.get(position).getMembers_count() + "");
            holder.mFollowers.setText(model.get(position).getFollowers_count() + "");

            try {
                Glide.with(context).load(model.get(position).getCover_pic()).error(R.drawable.no_team).centerCrop().into(holder.mImage);
            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.no_team);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("IDDDDDD", String.valueOf(model.get(position).getId()));
                    Intent i = new Intent(context, OtherTeamViewActivity.class);
                    i.putExtra("ID", model.get(position).getId());
                    startActivityForResult(i, 3);
                }
            });

        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class TeamListHolder extends RecyclerView.ViewHolder {
            TextView mTeamName, mMembers, mFollowers, member, follower;
            ImageView mImage;

            public TeamListHolder(View itemView) {
                super(itemView);
                mTeamName = (TextView) itemView.findViewById(R.id.team_name);
                mMembers = (TextView) itemView.findViewById(R.id.tv_members);
                mFollowers = (TextView) itemView.findViewById(R.id.tv_followers);
                mImage = (ImageView) itemView.findViewById(R.id.team_image);
                member = (TextView) itemView.findViewById(R.id.team_member);
                follower = (TextView) itemView.findViewById(R.id.team_followers);

                mTeamName.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
                member.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
                follower.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            }
        }
    }
}
