package com.yowzapp.playstore.activty;

/**
 * Created by pramod on 25/11/16.
 */

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;
import static com.yowzapp.playstore.utils.Config.SHARE_LINK;

/**
 * Created by hemanth on 24/11/16.
 */

public class InviteFriendsFromGame extends BaseActivity implements  View.OnClickListener {


    Toolbar toolbar;
    TextView toolbarTitle, inviteText, description;
    ImageView whatsApp, email, message, more;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite_friends);

        toolbar = (Toolbar) findViewById(R.id.inviteToolBar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        InitializeAll();
        setupListeners();

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    private void setupListeners() {
        whatsApp.setOnClickListener(this);
        email.setOnClickListener(this);
        message.setOnClickListener(this);
        more.setOnClickListener(this);
    }

    private void InitializeAll() {

        toolbarTitle = (TextView) findViewById(R.id.invite_toolbar_title);
        inviteText = (TextView) findViewById(R.id.text_invite);
        description = (TextView) findViewById(R.id.invite_description);
        whatsApp = (ImageView) findViewById(R.id.whatsApp);
        email = (ImageView) findViewById(R.id.email);
        message = (ImageView) findViewById(R.id.message);
        more = (ImageView) findViewById(R.id.more);


        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        inviteText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        description.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.whatsApp:

                PackageManager pm = getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "Hey check out \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: "+ SHARE_LINK;

                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");

                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case R.id.email:

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL, "emailaddress@emailaddress.com");
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "Hey check out \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: "+ SHARE_LINK);

                startActivity(Intent.createChooser(intent, "Send Email"));
                /*Intent gmailIntent = new Intent();
                gmailIntent.setClassName("com.google.yowzapp.gm", "com.google.yowzapp.gm.ComposeActivityGmail");
                gmailIntent.putExtra(Intent.EXTRA_EMAIL, "designstring.com");
                try {
                    startActivity(gmailIntent);
                    finish();
                } catch (ActivityNotFoundException ex) {

                }*/

                break;

            case R.id.message:
                /*Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", "");
                startActivity(sendIntent);*/


                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", "Hey check out \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: "+ SHARE_LINK);
               // sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;

            case R.id.more:
                break;
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
