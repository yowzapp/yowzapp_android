package com.yowzapp.playstore.utils;

/**
 * Created by hemanth on 7/12/16.
 */

public class Config {

    //public static final String PLAYTHORA_BASEURL = "http://54.254.198.182/api"; // testing server

    //new servers
//    public static final String PLAYTHORA_BASEURL = "http://54.254.198.182/api";         //Test mobiler number edit
//    public static final String PLAYTHORA_BASEURL = "http://54.169.110.122/api";        // NEW
//    public static final String PLAYTHORA_BASEURL = "http://54.202.83.129/api";        ////// AFTER VENDOR CHANGES
//    public static final String PLAYTHORA_BASEURL = "http://192.168.1.50:8000/api";        // LOCAL API
//    public static final String PLAYTHORA_BASEURL = "http://192.168.1.5:9000/api";        // LOCAL API
//    public static final String PLAYTHORA_BASEURL = "httpla://ba4f110b.ngrok.io/api";

    public static final String PLAYTHORA_BASEURL = "https://dev.yowzapp.co/api";      //For release
    //public static final String PLAYTHORA_BASEURL = "http://192.168.1.17:9000/api";

//    public static final String CHAT_SERVER_URL = "http://54.169.110.12:2020";
//    public static final String CHAT_SERVER_URL = "http://ba4f110b.ngrok.io:2020";
    public static final String CHAT_SERVER_URL = "https://dev.yowzapp.co:2020";
//    public static final String CHAT_SERVER_URL = "http://54.254.198.182:2020";
    public static final String CHAT_HISTORY = PLAYTHORA_BASEURL + "/player/chat/history";


    public static final String TERMS_CONDITIONS = "http://54.254.249.44/terms";
    public static final String CANCELLATION_REFUND = "http://54.254.249.44/cancellation";
    public static final String PRIVACY_POLICY = "http://54.254.249.44/privacy";
    public static final String ABOUT = "http://54.254.249.44/about";

    public static final String SOCIAL_LOGIN = PLAYTHORA_BASEURL + "/socialAuth";
    public static final String EMAIL_EXIST = PLAYTHORA_BASEURL + "/emailExits?email=";
    public static final String EMAIL_REGISTER = PLAYTHORA_BASEURL + "/emailRegister";
    public static final String OTP_VERIFICATION = PLAYTHORA_BASEURL + "/otpVerification";
    public static final String RESEND_OTP = PLAYTHORA_BASEURL + "/resendOtp";
    public static final String LOGIN = PLAYTHORA_BASEURL + "/login?type=player";
    public static final String MOBILE_VEROFICATION = PLAYTHORA_BASEURL + "/mobileVerification";
    public static final String LOCATION_LIST = PLAYTHORA_BASEURL + "/locationList";
    public static final String GET_USER = PLAYTHORA_BASEURL + "/getUser";
    public static final String EDIT_PROFILE = PLAYTHORA_BASEURL + "/editUser?player=true";
    public static final String INTEREST_LIST = PLAYTHORA_BASEURL + "/insterestList";
    public static final String VENUE_LIST = PLAYTHORA_BASEURL + "/player/venuesList";
    public static final String TOP_LOCATION = PLAYTHORA_BASEURL + "/player/venuesList/hot";
    public static final String GROUND_DETAILS = PLAYTHORA_BASEURL + "/player/groundDetails";
    public static final String BOOK_GROUND = PLAYTHORA_BASEURL + "/player/bookGround";

    public static final String PLAYERS_LIST = PLAYTHORA_BASEURL + "/player/list?me=others";
    public static final String PLAYERS_FOLLOWING = PLAYTHORA_BASEURL + "/player/list?me=following";
    public static final String PLAYERS_FOLLOWER = PLAYTHORA_BASEURL + "/player/list?me=followers";
    public static final String MAKE_ADMIN_GAME = PLAYTHORA_BASEURL + "/game/player/makeAdmin";
    public static final String REMOVE_PLAYER_GAME = PLAYTHORA_BASEURL + "/game/player/remove";
    public static final String EDIT_GAME = PLAYTHORA_BASEURL + "/player/editGame";
    public static final String PLAYER_FOLLOW = PLAYTHORA_BASEURL + "/user/player/follow";
    public static final String PLAYER_UNFOLLOW = PLAYTHORA_BASEURL + "/user/player/unfollow";
    public static final String PLAYER_ACCEPT = PLAYTHORA_BASEURL + "/player/accept";
    public static final String PLAYER_REJECT = PLAYTHORA_BASEURL + "/player/reject";
    public static final String PLAYER_CANCEL_REQUEST = PLAYTHORA_BASEURL + "/player/rejectRequest";
    public static final String REQUEST_GAME = PLAYTHORA_BASEURL + "/player/requestGame";
    public static final String FOLLOWERS_ME = PLAYTHORA_BASEURL + "/player/list?me=followers";
    public static final String FOLLOWING_ME = PLAYTHORA_BASEURL + "/player/list?me=following";
    public static final String FOLLOWING_TEAM_ME = PLAYTHORA_BASEURL + "/player/list?me=followingTeams";
    public static final String RATING = PLAYTHORA_BASEURL + "/player/venue/submitRating";
    public static final String BADGES = PLAYTHORA_BASEURL + "/player/badges";
    public static final String SEARCH_FOLLOWING_PLAYER_TEAM = PLAYTHORA_BASEURL + "/search/following";
    public static final String GAME_SHOUT = PLAYTHORA_BASEURL + "/player/game/shout";


    public static final String SPORTS_LIST = PLAYTHORA_BASEURL + "/sportsList";
    public static final String SPORTS_LIST_GAME = PLAYTHORA_BASEURL + "/sportsListGround";
    public static final String INVITE_TO_PAY = PLAYTHORA_BASEURL + "/player/inviteToPay";
    public static final String ACCEPT_GAME_REQUEST = PLAYTHORA_BASEURL + "/game/notification/accept";
    public static final String REJECT_GAME_REQUEST = PLAYTHORA_BASEURL + "/game/notification/reject";


    public static final String VENUE_LIST_SPORTS_TYPE = PLAYTHORA_BASEURL + "/player/SportsVenueList";

    public static final String UPLOAD_IMAGE = PLAYTHORA_BASEURL + "/player/uploadImage";
    public static final String CREATE_TEAM = PLAYTHORA_BASEURL + "/player/createTeam";
    public static final String TEAM_LIST = PLAYTHORA_BASEURL + "/player/teamList?invite=yes";
    public static final String TEAM_LIST_TWO = PLAYTHORA_BASEURL + "/player/teamList";
    public static final String TEAM_DETAIL = PLAYTHORA_BASEURL + "/player/team/detail";
    public static final String TEAM_GALLERY = PLAYTHORA_BASEURL + "/player/teamGallery/detail";
    public static final String CREATE_GAME = PLAYTHORA_BASEURL + "/player/createGame";
    public static final String ADD_TEAM_IMAGE = PLAYTHORA_BASEURL + "/player/add/teamImage";
    public static final String DELETE_TEAM_IMAGE = PLAYTHORA_BASEURL + "/player/delete/teamImage";
    public static final String EDIT_TEAM = PLAYTHORA_BASEURL + "/player/editTeam";
    public static final String FOLLOW_TEAM = PLAYTHORA_BASEURL + "/user/team/follow";
    public static final String UNFOLLOW_TEAM = PLAYTHORA_BASEURL + "/user/team/unfollow";
    public static final String DELETE_TEAM = PLAYTHORA_BASEURL + "/player/deleteTeam";
    public static final String NOTIFICATION_SETTING_LIST = PLAYTHORA_BASEURL + "/player/settings/notification/list";
    public static final String NOTIFICATION_SETTING_UPDATE = PLAYTHORA_BASEURL + "/player/settings/notification";
    public static final String CHANGE_PASSWORD = PLAYTHORA_BASEURL + "/resetPassword";
    public static final String FORGOT_PASSWORD = PLAYTHORA_BASEURL + "/forgotpassword";
    public static final String FORGOT_PASSWORD_VERIFY = PLAYTHORA_BASEURL + "/forgotpasswordverify";


    public static final String GAME_LISTING = PLAYTHORA_BASEURL + "/player/gameList";
    public static final String PLAYER_CHAT_LISTING = PLAYTHORA_BASEURL + "/player/chat/list";
    public static final String GAME_DETAIL = PLAYTHORA_BASEURL + "/player/gameDetails";
    public static final String GAME_REQUEST = PLAYTHORA_BASEURL + "/player/game/request";
    public static final String MY_TRANSACTIONS = PLAYTHORA_BASEURL + "/player/myTransactions";
    public static final String PLAYERS_DETAILS = PLAYTHORA_BASEURL + "/player/details";
    public static final String MY_TEAM_LIST = PLAYTHORA_BASEURL + "/player/teamList?mine=true";

    public static final String BOOKING_AVAILABLE = PLAYTHORA_BASEURL + "/player/bookingAvailable";
    public static final String RAZORPAY_SUCCESS = PLAYTHORA_BASEURL + "/player/transaction/success";
    public static final String RAZORPAY_FAILURE = PLAYTHORA_BASEURL + "/player/transaction/failure";

    public static final String TEAM_PLAYER_REMOVE = PLAYTHORA_BASEURL + "/team/player/remove";
    public static final String TEAM_PLAYER_MAKEADMIN = PLAYTHORA_BASEURL + "/team/player/makeAdmin";
    public static final String TEAM_GAME_DETAIL = PLAYTHORA_BASEURL + "/player/teamGames/detail";
    public static final String BANNER = PLAYTHORA_BASEURL + "/banner";
    public static final String NOTIFICATION_LIST = PLAYTHORA_BASEURL + "/player/notificationList";
    public static final String GAME_ACCEPT = PLAYTHORA_BASEURL + "/game/notification/accept";
    public static final String GAME_REJECT = PLAYTHORA_BASEURL + "/game/notification/reject";
    public static final String NOTIFY_READ = PLAYTHORA_BASEURL + "/player/notification/read";
    public static final String REQUEST_TEAM = PLAYTHORA_BASEURL + "/player/team/request";
    public static final String TEAM_ACCEPT = PLAYTHORA_BASEURL + "/team/notification/accept";
    public static final String TEAM_REJECT = PLAYTHORA_BASEURL + "/team/notification/reject";
    public static final String PAYMENT_REJECT = PLAYTHORA_BASEURL + "/player/game/rejectPayment";
    public static final String SEARCH_ALL = PLAYTHORA_BASEURL + "/search/all";
    public static final String SEARCH_PLAYER = PLAYTHORA_BASEURL + "/search/player";
    public static final String SEARCH_RECENT = PLAYTHORA_BASEURL + "/search/player/recent";
    public static final String ACTIVITY_LOG = PLAYTHORA_BASEURL + "/player/activites";
    public static final String ACTIVITY_LIKE = PLAYTHORA_BASEURL + "/player/activites/like";
    public static final String ACTIVITY_UNLIKE = PLAYTHORA_BASEURL + "/player/activites/unlike";
    public static final String ACTIVITY_COMMENT = PLAYTHORA_BASEURL + "/player/activites/comment";
    public static final String ACTIVITY_COMMENT_LIST = PLAYTHORA_BASEURL + "/player/activites/getComments";
    public static final String TEAM_TIMELINE = PLAYTHORA_BASEURL + "/player/timeline/detail";
    public static final String LOGOUT = PLAYTHORA_BASEURL + "/logout";
    public static final String PLAYER_LIST = PLAYTHORA_BASEURL + "/player/game/playersList";
    public static final String ATTENDENCE = PLAYTHORA_BASEURL + "/player/game/markAttendance";
    public static final String DELETE_GAME = PLAYTHORA_BASEURL + "/player/game/delete";
    public static final String MY_POINTS = PLAYTHORA_BASEURL + "/player/myPoints";
    public static final String LEADER_BOARD = PLAYTHORA_BASEURL + "/player/leaderboard";

    public static String CHAT_ID = "";
    public static String CHAT_TYPE = "";

    public static String RALEWAY_REGULAR = "fonts/Raleway-Regular.ttf";
    public static String HELVETICA_CONDENSED = "fonts/Helvetica-Condensed.otf";
    public static String HELVETICA_CONDENSED_BOLD = "fonts/Helvetica-Condensed Bold.ttf";
    public static String RALEWAY_BOLD = "fonts/Raleway-Bold.ttf";
    public static String RALEWAY_SEMIBOLD = "fonts/Raleway-SemiBold.ttf";
    public static String MYRIAD_PRO_REGULAR = "fonts/Myriad Pro Regular.ttf";


    public static String SHARE_LINK = "https://yowzapp.co";
}
