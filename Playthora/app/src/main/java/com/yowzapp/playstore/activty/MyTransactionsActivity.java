package com.yowzapp.playstore.activty;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.MyTransactionCashFragment;
import com.yowzapp.playstore.fragment.MyTransactionPointsFragment;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

/**
 * Created by hemanth on 16/11/16.
 */

public class MyTransactionsActivity extends BaseActivity implements  View.OnClickListener {

    public ViewPager customViewPager;
    public MyPagerAdapter myPagerAdapter;
    Toolbar toolbar;
    TextView cash, cashSelected, points, pointsSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_transactions_layout);


        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        intializeAllComponents();
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.my_transactions_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(MyTransactionsActivity.this, Config.RALEWAY_REGULAR));
        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        customViewPager.setAdapter(myPagerAdapter);

        customViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        Cash();
                        break;

                    case 1:
                        Points();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        cash.setOnClickListener(this);
        points.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cash:
                Cash();
                customViewPager.setCurrentItem(0);
                break;

            case R.id.points:
                Points();
                customViewPager.setCurrentItem(1);
                break;
        }

    }

    private void Cash() {
        cashSelected.setVisibility(View.VISIBLE);
        cash.setVisibility(View.GONE);
        points.setVisibility(View.VISIBLE);
        pointsSelected.setVisibility(View.GONE);
    }

    private void Points() {
        pointsSelected.setVisibility(View.VISIBLE);
        points.setVisibility(View.GONE);
        cashSelected.setVisibility(View.GONE);
        cash.setVisibility(View.VISIBLE);
    }

    private void intializeAllComponents() {

        toolbar = (Toolbar) findViewById(R.id.my_transactions_toolbar);
        customViewPager = (ViewPager) findViewById(R.id.myTransactionFragment);
        cash = (TextView) findViewById(R.id.cash);
        cashSelected = (TextView) findViewById(R.id.cashSelected);
        points = (TextView) findViewById(R.id.points);
        pointsSelected = (TextView) findViewById(R.id.pointsSelected);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Cash", "Points"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new MyTransactionCashFragment();

                case 1:
                    return new MyTransactionPointsFragment();

                default:
                    return new MyTransactionCashFragment();

            }

        }
    }
}
