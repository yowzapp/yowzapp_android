package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.SHARE_LINK;

/**
 * Created by vaishakha on 13/1/17.
 */
public class AppSettingsActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {
    public static Activity mActivity;
    RelativeLayout logout, changePassword, notifySettings, termsLayout, privacyLayout, aboutLayout, cancellationLayout;
    TextView toolbarTitle, basicText, notifyText, Logout, share, mail, rate, made, playthoraText, versionText,
            termsConditions, privacyPolicy, aboutUs, cancellationAndRefund;
    Toolbar toolbar;
    long size = 0;
    PreferenceManager mPref;
    LoginManager mLoginManager;
    View view;
    String changePass = "";
    private GoogleApiClient mGoogleApiClient;
    private String TAG = "AppSettingActivity";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.app_settings_layout);
        mLoginManager = LoginManager.getInstance();
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        mPref = PreferenceManager.instance(this);
        initialize();

        try {

            if (getIntent().hasExtra("setting")) {
                changePass = getIntent().getStringExtra("setting");
                Log.e("changePass", changePass + "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            callGmail();
        } catch (Exception e) {

            e.printStackTrace();
        }

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (changePass.equalsIgnoreCase("change")) {
                    Intent intent = new Intent(AppSettingsActivity.this, HomeScreenActivity.class);
                    intent.putExtra("FRAGMENT", "Profile");
                    startActivity(intent);
                    finishAffinity();
                } else {
                    onBackPressed();
                }


            }
        });


        if (mPref.getIsSocial().equalsIgnoreCase("true")) {
            changePassword.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
            changePassword.setVisibility(View.VISIBLE);
        }

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);

                intent.setType("text/html");
//                intent.setData(Uri.parse("mailto:feedback@designstring.co")); // only email apps should handle this
                intent.setData(Uri.parse("mailto:support@yowzapp.co")); // only email apps should handle this
//                intent.putExtra(Intent.EXTRA_EMAIL, "feedback@designstring.co");
                intent.putExtra(Intent.EXTRA_EMAIL, "support@yowzapp.co");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                intent.putExtra(Intent.EXTRA_TEXT, "Hi there");

                // startActivity(Intent.createChooser(intent, "Send Email"));

                try {
                    startActivity(intent);
                    //finish();
                } catch (ActivityNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
//                        "Hey check out this awesome app named \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: https://play.google.com/store/apps/details?id=" + getPackageName());
                        "Hey check out \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: "+ SHARE_LINK);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialoglogout();
            }
        });

        notifySettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent preference = new Intent(AppSettingsActivity.this, NotificationSettingsActivity.class);
                startActivity(preference);
                overridePendingTransition(R.anim.animation_enter,
                        R.anim.animation_leave);
            }
        });

        termsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettingsActivity.this, AdditionalInformation.class);
                intent.putExtra("URL", Config.TERMS_CONDITIONS);
                intent.putExtra("title", "terms");
                startActivity(intent);

            }
        });

        cancellationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettingsActivity.this, AdditionalInformation.class);
                intent.putExtra("URL", Config.CANCELLATION_REFUND);
                intent.putExtra("title", "cancellation");
                startActivity(intent);

            }
        });


        privacyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettingsActivity.this, AdditionalInformation.class);
                intent.putExtra("URL", Config.PRIVACY_POLICY);
                intent.putExtra("title", "privacy");
                startActivity(intent);
            }
        });

        aboutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettingsActivity.this, AdditionalInformation.class);
                intent.putExtra("URL", Config.ABOUT);
                intent.putExtra("title", "about");
                startActivity(intent);
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettingsActivity.this, ChangePassword.class);
                startActivity(intent);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void callGmail() {
        String serverClientId = getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void showDialoglogout() {
        final Dialog dialog = new Dialog(AppSettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.ask_login_dialog);
        TextView dialogStatus = (TextView) dialog.findViewById(R.id.login_confirm);
        TextView dialogCancel = (TextView) dialog.findViewById(R.id.cancel_login);
        TextView dialogoK = (TextView) dialog.findViewById(R.id.tv_login);

        dialogStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        dialogCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        dialogoK.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        dialogStatus.setText("Do you want to logout?");
        dialogoK.setText("YES");
        dialogCancel.setText("NO");

        dialogoK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populatelogout();
                dialog.dismiss();
            }
        });
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void initialize() {
        logout = (RelativeLayout) findViewById(R.id.prefRelativeLayoutTwo);
        changePassword = (RelativeLayout) findViewById(R.id.changePasswordLayout);
        notifySettings = (RelativeLayout) findViewById(R.id.prefnotifyRelativeLayoutOne);
        toolbarTitle = (TextView) findViewById(R.id.app_preferences_toolbar_title);
        basicText = (TextView) findViewById(R.id.basicsettingsText);
        notifyText = (TextView) findViewById(R.id.notifyText);
        Logout = (TextView) findViewById(R.id.logout);
        toolbar = (Toolbar) findViewById(R.id.appSettingsToolbar);
        share = (TextView) findViewById(R.id.share);
        mail = (TextView) findViewById(R.id.support);
        rate = (TextView) findViewById(R.id.rate);
        made = (TextView) findViewById(R.id.made);
        playthoraText = (TextView) findViewById(R.id.playthora);
        versionText = (TextView) findViewById(R.id.version);
        termsConditions = (TextView) findViewById(R.id.terms_condition);
        privacyPolicy = (TextView) findViewById(R.id.privacy_policy);
        aboutUs = (TextView) findViewById(R.id.about);
        cancellationAndRefund = (TextView) findViewById(R.id.cancellation);
        termsLayout = (RelativeLayout) findViewById(R.id.terms_condition_layout);
        privacyLayout = (RelativeLayout) findViewById(R.id.privacy_policy_layout);
        aboutLayout = (RelativeLayout) findViewById(R.id.about_layout);
        cancellationLayout = (RelativeLayout) findViewById(R.id.cancellation_layout);
        view = findViewById(R.id.prefView2);

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        basicText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        notifyText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        Logout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        share.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        mail.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        rate.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        made.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        playthoraText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        versionText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        termsConditions.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        privacyPolicy.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        aboutUs.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        cancellationAndRefund.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
    }

    private void populatelogout() {

        if (mGoogleApiClient.isConnected()) {
            Log.v("Connected", "Google");
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            mGoogleApiClient.disconnect();
                            mGoogleApiClient.connect();
                        }
                    }
            );
        }

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(AppSettingsActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("device_id", mPref.getAndroid_id());
                mHttpClient.post(Config.LOGOUT, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.e("Logout", s);
                        try {
                            JSONObject object = new JSONObject(s);
                            if (object.getString("status").equalsIgnoreCase("success")) {
                                mPref.setAccessToken("");
                                mPref.setProfilePic("");
                                mPref.setIsSocial("");
                                mPref.setName("");
                                mPref.setProfilePic("");
                                mPref.setBio("");
                                mPref.setCoverPic("");
                                logoutFromFacebook();
                                mLoginManager.logOut();
//                                Toast.makeText(AppSettingsActivity.this, "logged out successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(AppSettingsActivity.this, SignupLoginActivity.class);
                                startActivity(intent);
                                finishAffinity();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(bytes);
                            Log.d("RESPONSE_FAIL", s);
                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                mPref.setAccessToken("");
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                            } else
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {
//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private void logoutFromFacebook() {

        GraphRequest delPermRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                if (graphResponse != null) {
                    FacebookRequestError error = graphResponse.getError();
                    if (error != null) {
                        Log.e(TAG, error.toString());
                    } else {

                        finish();
                    }
                }
            }
        });
        Log.d(TAG, "Executing revoke permissions with graph path" + delPermRequest.getGraphPath());
        delPermRequest.executeAsync();
    }

    @Override
    public void onStart() {
        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(AppSettingsActivity.this);
        }
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("AppSettings Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (changePass.equalsIgnoreCase("change")) {
                Intent intent = new Intent(AppSettingsActivity.this, HomeScreenActivity.class);
                intent.putExtra("FRAGMENT", "Profile");
                startActivity(intent);
                finishAffinity();
            } else {
                onBackPressed();
            }

            return true;
        }
        return false;
    }


}
