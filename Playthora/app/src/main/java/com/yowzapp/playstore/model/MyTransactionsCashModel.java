package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 16/11/16.
 */
public class MyTransactionsCashModel {


    private String status;
    private String statusCode;
    private List<MyTransactionCashSuccess> success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<MyTransactionCashSuccess> getSuccess() {
        return success;
    }

    public void setSuccess(List<MyTransactionCashSuccess> success) {
        this.success = success;
    }


}
