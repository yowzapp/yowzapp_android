package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.TeamDetailSportsModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by vaishakha on 22/11/16.
 */
public class OtherTeamViewActivity extends BaseActivity {
    static final int MIN_DISTANCE = 100;// TODO change this runtime based on screen resolution. for 1920x1080 is to small the 100 distance
    public static boolean edited = false;
    Toolbar toolbar;
    RecyclerView recyclerView;
    OtherTeamViewAdapter adapter;
    Button mRequest, mFollowing;
    TextView teamName, teamStatus, activityText, member, status, points;
    ImageView goUp, mImage;
    String image, mTeamName, members, teamDisp;
    View view;
    RelativeLayout relativeLayout;
    int id, point, teamId;
    PreferenceManager mPref;
    boolean value, following;
    boolean isMember;
    Gson sGson, mGson;
    ArrayList<TeamDetailSportsModel> sportsModel;
    JSONArray membersList;
    String sports, level;
    Button item;
    boolean is_requested;
    ImageView activityMeter;
    String search = "";
    Toast toast;
    ProgressBar progressBar;
    private float downX, downY, upX, upY;
    private boolean isNotificationTriggered = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_team_view);
        edited = false;
        search = "";

        mPref = PreferenceManager.instance(this);
        initialize();
        setFont();
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent i = getIntent();
        if (i.hasExtra("ID")) {
//            int idString = i.getIntExtra("ID", 0);
//            id = idString;
            id = i.getIntExtra("ID", 0);
            Log.e("TEAM_ID", String.valueOf(id));
        } else if (i.hasExtra("team_id")) {
            id = i.getIntExtra("team_id", 0);
            Log.e("team_id", String.valueOf(id));
            isNotificationTriggered = true;
        } else if (i.hasExtra("TEAM_ID")) {
            id = i.getIntExtra("TEAM_ID", 0);
            Log.e("TEAM_ID", String.valueOf(id));
        }

        if (i.hasExtra("SEARCH"))
            search = i.getStringExtra("SEARCH");

        if (mPref.getAccessToken().isEmpty())
            search = "";

        loadDetail(id);

        //teamName.setText(mTeamName);
        //member.setText(members + " Members");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        goUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OtherTeamViewActivity.this, OtherTeamActivity.class);
                intent.putExtra("TEAMNAME", mTeamName);
                intent.putExtra("MEMBERS_LIST", membersList.toString());
                Log.e("membetest",membersList.toString());
                intent.putExtra("TEAM_ID", id);
                Log.e("TEAM_ID",id+"");
                intent.putExtra("isMember", isMember);
                intent.putExtra("image", image);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up_enter, R.anim.slide_up_exit);
            }
        });

        member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtherTeamViewActivity.this, OtherTeamActivity.class);
                intent.putExtra("TEAMNAME", mTeamName);
                intent.putExtra("MEMBERS_LIST", membersList.toString());
                intent.putExtra("TEAM_ID", id);
                intent.putExtra("isMember", isMember);
                intent.putExtra("image", image);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up_enter, R.anim.slide_up_exit);
            }
        });

        mRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mPref.getAccessToken().isEmpty())
                    RequestTeam();
                else PlaythoraUtility.LoginDialog(OtherTeamViewActivity.this);
            }
        });

        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        downX = event.getX();
                        downY = event.getY();
                        return true;
                    }
                    case MotionEvent.ACTION_UP: {
                        upX = event.getX();
                        upY = event.getY();

                        float deltaX = downX - upX;
                        float deltaY = downY - upY;

                       /* // swipe horizontal?
                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            // left or right
                            if (deltaX < 0) {
                                this.onLeftToRightSwipe();
                                return true;
                            }
                            if (deltaX > 0) {
                                this.onRightToLeftSwipe();
                                return true;
                            }
                        } else {
                            Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long horizontally, need at least " + MIN_DISTANCE);
                            // return false; // We don't consume the event
                        }
*/
                        // swipe vertical?
                        if (Math.abs(deltaY) > MIN_DISTANCE) {
                            // top or down
                            if (deltaY < 0) {
                                //this.onTopToBottomSwipe();
                                return true;
                            }
                            if (deltaY > 0) {
                                onBottomToTopSwipe();
                                return true;
                            }
                        } else {
                            // return false; // We don't consume the event
                        }

                        return false; // no swipe horizontally and no swipe vertically
                    }// case MotionEvent.ACTION_UP:
                }
                return false;
            }

        });

    }

    private void RequestTeam() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(OtherTeamViewActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("team_id", String.valueOf(id));

                Log.e("PARAMMS", String.valueOf(params));
                Log.e("team_id", String.valueOf(id));
                mHttpClient.post(Config.REQUEST_TEAM, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSEREQUESTTEAM", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Toast.makeText(OtherTeamViewActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        mRequest.setVisibility(View.VISIBLE);
                                        mRequest.setText("Requested");
                                        //mRequest.setBackgroundResource(R.drawable.requested_back);
                                        //mRequest.setTextColor(getResources().getColor(R.color.text_one));
                                        mRequest.setClickable(false);
                                        // mRequest.setVisibility(View.INVISIBLE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(OtherTeamViewActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private void loadDetail(int id) {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(OtherTeamViewActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                Log.e("PARAMMS", String.valueOf(params));
                params.add("team_id", String.valueOf(id));
                Log.e("TEAM_ID", String.valueOf(id));
                mHttpClient.post(Config.TEAM_DETAIL + search, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("URL_TEAM_DETAIL", Config.TEAM_DETAIL + search + "");
                                Log.e("RESPONSETEAMDETAILS", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        JSONObject sObj = object.getJSONObject("success");
                                        teamId = sObj.getInt("id");

                                        mTeamName = sObj.getString("name");
                                        teamName.setText(mTeamName);
                                        member.setVisibility(View.VISIBLE);
                                        member.setText(sObj.getString("members_count") + " Members");

                                        teamDisp = sObj.getString("discription");
                                        teamStatus.setText(teamDisp);

                                        point = sObj.getInt("points");
                                        points.setText(point + " Points");

                                        level = sObj.getString("level_name");
                                        status.setText(level);

                                        following = sObj.getBoolean("is_following");

                                        if (following) {
                                            mFollowing.setText("Following");
                                        } else {
                                            mFollowing.setText("Follow");
                                        }

                                        try {
                                            is_requested = sObj.getBoolean("is_requested");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        value = sObj.getBoolean("is_mine");

                                        if (value) {
                                            mRequest.setVisibility(View.GONE);
                                            mFollowing.setVisibility(View.GONE);
                                            status.setVisibility(View.GONE);
                                            view.setVisibility(View.GONE);
                                            points.setVisibility(View.GONE);
                                            item.setVisibility(View.VISIBLE);
                                        } else {
                                            mRequest.setClickable(true);
                                            mRequest.setVisibility(View.VISIBLE);
                                            mFollowing.setVisibility(View.VISIBLE);
                                            status.setVisibility(View.VISIBLE);
                                            view.setVisibility(View.VISIBLE);
                                            points.setVisibility(View.VISIBLE);
                                            item.setVisibility(View.GONE);
                                        }

                                        isMember = sObj.getBoolean("is_member");
                                        if (isMember) {
                                            mRequest.setVisibility(View.INVISIBLE);
                                            mFollowing.setVisibility(View.INVISIBLE);
                                        } else {
                                            if (is_requested) {
                                                mRequest.setVisibility(View.VISIBLE);
                                                mRequest.setText("Requested");
                                                //mRequest.setBackgroundResource(R.drawable.requested_back);
                                                // mRequest.setTextColor(getResources().getColor(R.color.text_one));
                                                mRequest.setClickable(false);
                                                // mRequest.setVisibility(View.INVISIBLE);
                                            } else {
                                                mRequest.setText("Request to join");
                                                //mRequest.setBackgroundResource(R.drawable.button_back);
                                                //mRequest.setTextColor(getResources().getColor(R.color.white));
                                                mRequest.setVisibility(View.VISIBLE);
                                                mRequest.setClickable(true);
                                            }
                                            //mFollowing.setVisibility(View.VISIBLE);
                                        }

                                        if (sObj.getBoolean("activity_meter"))
                                            activityMeter.setImageResource(R.drawable.activity_meter_up);
                                        else
                                            activityMeter.setImageResource(R.drawable.activity_down);

                                        sports = sObj.getJSONArray("sports").toString();
                                        sGson = new Gson();
                                        if (sports != null) {
                                            sGson = new Gson();
                                            JSONArray jsonArray = null;
                                            sportsModel = new ArrayList<>();
                                            try {
                                                jsonArray = new JSONArray(sports);
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    sportsModel.add(sGson.fromJson(jsonArray.get(i).toString(), TeamDetailSportsModel.class));
                                                }
                                                adapter = new OtherTeamViewAdapter(OtherTeamViewActivity.this, sportsModel);
                                                recyclerView.setAdapter(adapter);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        membersList = new JSONArray();
                                        membersList = sObj.getJSONArray("members");
                                        image = sObj.getString("cover_pic");

                                        try {
                                            Glide.with(getApplicationContext()).load(image).error(R.drawable.no_team).centerCrop().into(mImage);
                                        } catch (Exception e) {
                                            mImage.setImageResource(R.drawable.no_team);
                                        }

                                       /* Intent intent = new Intent(CreateTeamActivity.this, CreateTeamSuccessActivity.class);
                                        startActivity(intent);
                                        finish();*/
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(OtherTeamViewActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private void onBottomToTopSwipe() {
        Intent intent = new Intent(OtherTeamViewActivity.this, OtherTeamActivity.class);
        intent.putExtra("TEAMNAME", mTeamName);
        if(membersList==null)
            intent.putExtra("MEMBERS_LIST", "");
        else
            intent.putExtra("MEMBERS_LIST", membersList.toString());
        Log.e("membertest",membersList.toString());
        intent.putExtra("TEAM_ID", id);
        intent.putExtra("isMember", isMember);
        intent.putExtra("image", image);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_up_enter, R.anim.slide_up_exit);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 3) {
            if (EditTeamActivity.success) {
                edited = true;
                loadDetail(id);
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (isNotificationTriggered) {
            Intent homeIntent = new Intent(OtherTeamViewActivity.this, HomeScreenActivity.class);
            startActivity(homeIntent);
            finish();
        } else {
            super.onBackPressed();
            Intent i = getIntent();
            setResult(3, i);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

           /* mRequest.setVisibility(View.GONE);
            mFollowing.setVisibility(View.GONE);
            status.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            points.setVisibility(View.GONE);*/

        getMenuInflater().inflate(R.menu.team_view_chat, menu);
        final Menu mMenu = menu;
        item = (Button) menu.findItem(R.id.action_custom_button).getActionView().findViewById(R.id.button_edit);
        try {
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(OtherTeamViewActivity.this, EditTeamActivity.class);
                    i.putExtra("IMAGEPATH", image);
                    i.putExtra("TEAMNAME", mTeamName);
                    i.putExtra("TEAM_ID", id);
                    i.putExtra("MEMBERS_LIST", membersList.toString());
                    Log.e("membertest",membersList.toString());
                    i.putExtra("SPORTS_LIST", sports);
                    i.putExtra("TEAM_DISP", teamDisp);
                    startActivityForResult(i, 3);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
           /* mRequest.setVisibility(View.VISIBLE);
            mFollowing.setVisibility(View.VISIBLE);*/
        mFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mPref.getAccessToken().isEmpty()) {
                    if (mFollowing.getText().toString().equalsIgnoreCase("Follow")) {
                        followTeam(false);//follow false
                    } else followTeam(true);//un follow
                } else PlaythoraUtility.LoginDialog(OtherTeamViewActivity.this);
            }
        });
        // getMenuInflater().inflate(R.menu.chat_white_icon_main, menu);
        return true;

    }

    private void followTeam(final boolean following) {
        String url;
        if (!following)
            url = Config.FOLLOW_TEAM;
        else url = Config.UNFOLLOW_TEAM;

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(OtherTeamViewActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("id", String.valueOf(teamId));
                Log.e("PARAMMS", String.valueOf(params));
                mHttpClient.post(url, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("FOLLOWDETAILS", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        if (!following)
                                            mFollowing.setText("Following");
                                        else
                                            mFollowing.setText("Follow");
                                        Toast.makeText(getApplicationContext(), object.getString("success"), Toast.LENGTH_SHORT).show();
                                        edited = true;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(OtherTeamViewActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /*if(id == R.id.chat_white_icon){
            Intent toChat = new Intent(OtherTeamViewActivity.this,ChattingActivity.class);
            toChat.putExtra("name",mTeamName);
            startActivity(toChat);
        }*/
        if (id == R.id.chat_white_view_icon) {
            if (mPref.getAccessToken().isEmpty()) {
                PlaythoraUtility.LoginDialog(OtherTeamViewActivity.this);
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                try {
                    if (value || isMember) {
                        //  Toast.makeText(OtherTeamViewActivity.this,"chat",Toast.LENGTH_SHORT).show();
                        Intent chat = new Intent(OtherTeamViewActivity.this, ChattingActivity.class);
                        chat.putExtra("name", mTeamName);
                        chat.putExtra("image", image);
                        chat.putExtra("id", teamId + "");
                        chat.putExtra("type", "team");
                        chat.putExtra("Conv_id", teamId);
                        chat.putExtra("Conv_type", 2);
                        startActivity(chat);
                    } else {
                        toast = Toast.makeText(getApplicationContext(), "You should be member of this team to chat", Toast.LENGTH_SHORT);
                        toast.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }

        return super.onOptionsItemSelected(item);

    }

    private void initialize() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_other_team);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        recyclerView = (RecyclerView) findViewById(R.id.team_game_recycler);
        mRequest = (Button) findViewById(R.id.request_button);
        mFollowing = (Button) findViewById(R.id.following_button);
        teamName = (TextView) findViewById(R.id.team_name_one);
        teamStatus = (TextView) findViewById(R.id.team_status);
        activityText = (TextView) findViewById(R.id.tv_activity);
        member = (TextView) findViewById(R.id.game_members);
        status = (TextView) findViewById(R.id.tv_game_status);
        points = (TextView) findViewById(R.id.game_points);
        goUp = (ImageView) findViewById(R.id.go_up_img);
        view = (View) findViewById(R.id.view_point);
        mImage = (ImageView) findViewById(R.id.game_img);
        relativeLayout = (RelativeLayout) findViewById(R.id.bottom_relative);
        activityMeter = (ImageView) findViewById(R.id.activity_meter);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void setFont() {
        mRequest.setTypeface(PlaythoraUtility.getFont(OtherTeamViewActivity.this, Config.RALEWAY_REGULAR));
        mFollowing.setTypeface(PlaythoraUtility.getFont(OtherTeamViewActivity.this, Config.RALEWAY_REGULAR));
        teamName.setTypeface(PlaythoraUtility.getFont(OtherTeamViewActivity.this, Config.RALEWAY_SEMIBOLD));
        teamStatus.setTypeface(PlaythoraUtility.getFont(OtherTeamViewActivity.this, Config.RALEWAY_REGULAR));
        activityText.setTypeface(PlaythoraUtility.getFont(OtherTeamViewActivity.this, Config.RALEWAY_REGULAR));

    }

    public class OtherTeamViewAdapter extends RecyclerView.Adapter<OtherTeamViewAdapter.GameList> {
        Context context;
        ArrayList<TeamDetailSportsModel> tags;

        public OtherTeamViewAdapter(OtherTeamViewActivity activity, ArrayList<TeamDetailSportsModel> tags) {
            context = activity;
            this.tags = tags;
        }

        @Override
        public GameList onCreateViewHolder(ViewGroup parent, int viewType) {
            View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_items, null);
            return new GameList(contentView);
        }

        @Override
        public void onBindViewHolder(GameList holder, int position) {
            holder.gameName.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            holder.gameName.setText(tags.get(position).getName());

        }

        @Override
        public int getItemCount() {
            return tags.size();
        }

        public class GameList extends RecyclerView.ViewHolder {
            TextView gameName;

            public GameList(View itemView) {
                super(itemView);
                gameName = (TextView) itemView.findViewById(R.id.text_games);
            }
        }
    }
}
