package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 28/12/16.
 */
public class GameListSuccess {

    private List<AllGameList> my_games;
    private List<AllGameList> all_games;

    public List<AllGameList> getAll_games() {
        return all_games;
    }

    public void setAll_games(List<AllGameList> all_games) {
        this.all_games = all_games;
    }


    public List<AllGameList> getMy_games() {
        return my_games;
    }

    public void setMy_games(List<AllGameList> my_games) {
        this.my_games = my_games;
    }
}
