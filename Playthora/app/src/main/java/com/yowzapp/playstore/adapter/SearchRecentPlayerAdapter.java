package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.model.SearchRecentModel;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.List;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 15/11/16.
 */
public class SearchRecentPlayerAdapter extends RecyclerView.Adapter<SearchRecentPlayerAdapter.ProfileListHolder> {
    Context mContext;
    List<SearchRecentModel.SearchRecentTeam> playerList;
    String nameValue;

    public SearchRecentPlayerAdapter(Context context, List<SearchRecentModel.SearchRecentTeam> arrayList) {
        mContext = context;
        playerList = arrayList;
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_view, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, final int position) {

        try {

            try {
                Glide.with(mContext).load(playerList.get(position).getCover_pic()).error(R.drawable.circled_user).centerCrop().into(holder.playerImage);
            } catch (Exception e) {
                holder.playerImage.setImageResource(R.drawable.circled_user);
            }

            nameValue = playerList.get(position).getName();
            Log.e("name", nameValue);

            if (nameValue.length() > 15) {
                String s = nameValue.substring(0, 15) + "...";
                holder.playerName.setText(s);
            } else {
                holder.playerName.setText(nameValue);
            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playerList.get(position).getType().equalsIgnoreCase("player")) {
                        Intent i = new Intent(mContext, PlayerProfileViewActivity.class);
                        i.putExtra("Id", playerList.get(position).getId() + "");
                        i.putExtra("SEARCH", "?q=search");
                        mContext.startActivity(i);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView playerImage;
        TextView playerName;

        public ProfileListHolder(View itemView) {
            super(itemView);

            playerImage = (ImageView) itemView.findViewById(R.id.imageView_player);

            playerName = (TextView) itemView.findViewById(R.id.player_name);

            playerName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));

        }
    }

}