package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.RewardViewHolder;
import com.yowzapp.playstore.model.TeamDetailSportsModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

/**
 * Created by nakul on 18/11/16.
 */
public class SportsProfileAdapter extends
        RecyclerView.Adapter<RewardViewHolder> {

    public static String[] sports;
    private ArrayList<TeamDetailSportsModel> arrayLists;
    private Context context;


    public SportsProfileAdapter(Context context, ArrayList<TeamDetailSportsModel> arrayList) {
        this.context = context;
        this.arrayLists = arrayList;
        // arrayLists.add(new TeamDetailSportsModel(""));
        sports = new String[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            sports[i] = arrayList.get(i).getName();

        }

    }


    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.profile_edit_team_lay, viewGroup, false);
        RewardViewHolder listHolder = new RewardViewHolder(mainGroup);

        return listHolder;
    }


    @Override
    public int getItemCount() {
        return arrayLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final RewardViewHolder holder, final int position) {
        final TeamDetailSportsModel model = arrayLists.get(position);
        final RewardViewHolder mainHolder = (RewardViewHolder) holder;

        try {
            mainHolder.sportsCategoryName.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            mainHolder.sportsCategoryName.setText(model.getName());
            Log.v("MainHolder", model.getName());
            // PlayerProfileEditActivity.favSportsArray.add(model.getId()+"");

            // Log.e("$$$$$$", String.valueOf(PlayerProfileEditActivity.favSportsArray));

        } catch (NullPointerException e) {

        }

    }

    public void refresh(ArrayList<TeamDetailSportsModel> sportslist) {
        this.arrayLists = sportslist;
        sports = new String[arrayLists.size()];
        for (int i = 0; i < arrayLists.size(); i++) {
            sports[i] = arrayLists.get(i).getName();
        }
        // arrayLists.add(new TeamDetailSportsModel(""));
        notifyDataSetChanged();
    }
}