package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.GameDetailMembers;
import com.yowzapp.playstore.model.GameDetailModel;
import com.yowzapp.playstore.model.GameDetailSuccess;
import com.yowzapp.playstore.model.GamePlayers;
import com.yowzapp.playstore.model.MyProfileLeaderBoardModel;
import com.yowzapp.playstore.model.SportsListModel;
import com.yowzapp.playstore.model.SportsSuccessList;
import com.yowzapp.playstore.model.TeamDetailMemberModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 22/11/16.
 */

public class CreateGame extends BaseActivity {

    private static final int PICK_FROM_CAM = 1;
    private static final String TAG = "CreateGame";
    public static String MAX_PLAYERS = "";
    public RecyclerViewAdapter recyclerAdapter;
    public ArrayList<MyProfileLeaderBoardModel> myProfileEveryOne;
    String responseString;
    Gson mGson;
    ProgressDialog dialog;
    RelativeLayout attachImage, groundAndSlotsLayout;
    int j;
    ArrayList<GamePlayers> creatGameList;
    ImageView gameImage, uploadImageIcon;
    TextView uploadTextGone, typeOfGame, groundAndSlots, whoDoYouWantToPlayWith, thisIsAPrivateGame, maxPlayer;
    TextInputLayout nameOFGameLayout, descriptionLayout;
    EditText nameOfGame, descriptionOFGame;
    Button proceedPay;
    Toolbar toolbar;
    PreferenceManager mPref;
    String selectedImagePath;
    SportsListModel sportsListModel;
    ProgressBar progressBar;
    int serverResponseCode = 0;
    //String image ="";
    String gameName = "", sportsId = "";
    int posi;
    ArrayList<String> strings = new ArrayList<>();
    String jsonOb = "";
    ToggleButton toggleButton;
    boolean isPrivate = false;
    String totolAmount;
    ProgressBar getProgressBar;
    String groundName = "";
    String slots = "";
    List<String> players;
    List<String> teams;
    String repeat, gameTitle, descriptValue, gameType = "", player, privateGame, loc, coverImage, date, sTime, eTime, cTeam, teamMembers, getTeamMembers, teamId="";
    String getResponseString, sportIdFrom = null;
    GameDetailModel gameDetailModel;
    Gson gson;
    String response, getGameId;
    JSONObject object;
    Toast toastObject;
    RelativeLayout numPlayer;
    TeamDetailMemberModel memberModel;
    ArrayList<TeamDetailMemberModel> memberModels;
    GameDetailSuccess gameDetailSuccess;
    List<GameDetailMembers> gameDetailMemberses;
    int scrollPosition;
    String noOfPlayers;
    private RecyclerView recyclerView;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private int PICK_IMAGE_REQUEST = 2;
    private String vName;
    private boolean isFromTeam = false;
    int playersSelected = 0;


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.create_game);
        mPref = PreferenceManager.instance(getApplicationContext());

        // CreateGame.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d("INTENT", String.format("%s %s (%s)", key,
                        value.toString(), value.getClass().getName()));
            }
        }
        repeat = intent.getStringExtra("game");
        getResponseString = intent.getStringExtra("response");
        privateGame = intent.getStringExtra("private");
        sportIdFrom = intent.getStringExtra("sportIdFrom");


        try {
            teamMembers = intent.getStringExtra("memList");
            getTeamMembers = intent.getStringExtra("teamGameFragment");
            if(intent.hasExtra("teamId")) {
                teamId = intent.getStringExtra("teamId");
            }

            Log.e("memList", teamMembers + "");
            Log.e("fromTeamMembers", getTeamMembers + "");
            Log.e("teamId@@@@@", teamId + "");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        if (intent.hasExtra("getGameId"))
            getGameId = intent.getStringExtra("getGameId");
        else
            getGameId = "";

        jsonOb = intent.getStringExtra("JsonObj");
        Log.e("jsonResp", jsonOb + "");

        groundName = intent.getStringExtra("groundName");
        Log.e("groundName", groundName + "");

        cTeam = intent.getStringExtra("venuList");
        Log.e("cTeam", cTeam + "");


        Log.e("sportIdFrom", sportIdFrom + "");

        Log.e("getResponseString", getResponseString + "");
        Log.e("privateGame", privateGame + "");


       /* gameTitle = intent.getStringExtra("gameTitle");
        gameType = intent.getStringExtra("sportsName");
        descriptValue = intent.getStringExtra("description");
        loc = intent.getStringExtra("location");
        coverImage = intent.getStringExtra("coverPic");
        date = intent.getStringExtra("date");

        sTime = intent.getStringExtra("startTime");
        eTime = intent.getStringExtra("endTime");

        Log.e("gameRepeat",repeat+"");
        Log.e("gameTitle",gameTitle+"");
        Log.e("gameType",gameType+"");
        Log.e("descriptValue",descriptValue+"");
        Log.e("loc",loc+"");
        Log.e("coverImage",coverImage+"");
        Log.e("date",date+"");

        Log.e("sTime",sTime+"");
        Log.e("eTime",eTime+"");
*/


        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Create a Game");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(CreateGame.this, RALEWAY_REGULAR));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((gameType == null && sportsId == null) || (gameType.isEmpty() && sportsId.isEmpty())) {
                    PlaythoraUtility.hideSoftKeyboard(CreateGame.this);
                    onBackPressed();
                } else {
                    popUp();
                }
            }
        });

        MAX_PLAYERS = "";
        recyclerView = (RecyclerView) findViewById(R.id.my_profile_leader_board_chips);
        gameImage = (ImageView) findViewById(R.id.createTeamImage);
        creatGameList = new ArrayList<>();
        players = new ArrayList<>();
        teams = new ArrayList<>();
        uploadImageIcon = (ImageView) findViewById(R.id.uploadIcon);
        uploadTextGone = (TextView) findViewById(R.id.uploadText);
        typeOfGame = (TextView) findViewById(R.id.typeOfgame);
        groundAndSlots = (TextView) findViewById(R.id.groundsAndSLots);
        whoDoYouWantToPlayWith = (TextView) findViewById(R.id.whoDoYouWant);
        thisIsAPrivateGame = (TextView) findViewById(R.id.privateGame);
        proceedPay = (Button) findViewById(R.id.proceedToPay);
        attachImage = (RelativeLayout) findViewById(R.id.imageLayout);
        nameOfGame = (EditText) findViewById(R.id.game_name);
        nameOFGameLayout = (TextInputLayout) findViewById(R.id.game_layout_game);
        descriptionOFGame = (EditText) findViewById(R.id.descriptionOfGame);
        descriptionLayout = (TextInputLayout) findViewById(R.id.descriptionLayout);
        groundAndSlotsLayout = (RelativeLayout) findViewById(R.id.input_layout_street);
        progressBar = (ProgressBar) findViewById(R.id.sports_progress_bar);
        toggleButton = (ToggleButton) findViewById(R.id.createTeamPrivateToggle);
        toggleButton.setChecked(true);
        isPrivate = true;
        getProgressBar = (ProgressBar) findViewById(R.id.game_progress_bar);
        numPlayer = (RelativeLayout) findViewById(R.id.input_layout_area);
        maxPlayer = (TextView) findViewById(R.id.max_player);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        // recyclerView.getLayoutManager().scrollToPosition(6);

        //  recyclerView.setLayoutManager(new LinearLayoutManager(CreateGame.this, LinearLayoutManager.HORIZONTAL, false));

        nameOfGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        nameOFGameLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        uploadTextGone.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        descriptionOFGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        descriptionLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        typeOfGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        groundAndSlots.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        whoDoYouWantToPlayWith.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        thisIsAPrivateGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        proceedPay.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        maxPlayer.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        if (intent.hasExtra("ACTIVITY")) {
            jsonOb = intent.getStringExtra("JsonObj");
            sportsId = intent.getStringExtra("SPORTSID");
            Log.e("jsonResp", jsonOb + "");


            try {
                object = new JSONObject(jsonOb);
                vName = object.getString("vName");
                MAX_PLAYERS = object.getString("no_players");
                maxPlayer.setText("(Max " + (Integer.parseInt(MAX_PLAYERS) - 1) + " players*)");
                if (!MAX_PLAYERS.equals("0")) {
                   // maxPlayer.setVisibility(View.VISIBLE);
                }
                MAX_PLAYERS = (Integer.parseInt(MAX_PLAYERS) - 1) + "";
                JSONArray slotArray = object.getJSONArray("slots");

                for (int i = 0; i < slotArray.length(); i++) {

                    if (slots.isEmpty()) {
                        slots = slotArray.getJSONObject(i).getString("start_time") + "-" + slotArray.getJSONObject(i).getString("end_time");
                    } else {
                        slots = slots + ", " + slotArray.getJSONObject(i).getString("start_time") + "-" + slotArray.getJSONObject(i).getString("end_time");
                    }
                }
                Log.e("*****", slots);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            groundName = intent.getStringExtra("groundName");
            Log.e("groundName", groundName);

            String s = groundName + " " + "[ " + slots + " ]";

            if (s.length() > 45) {
                String s1 = s.substring(0, 45);
                groundAndSlots.setText(vName + "\n" + s1 + "" + "..." + "");
            } else {
                groundAndSlots.setText(vName + "\n" + s);
            }
        }

        if (cTeam != null && cTeam.equalsIgnoreCase("create")) {
            try {
                object = new JSONObject(jsonOb);
                vName = object.getString("vName");
                MAX_PLAYERS = object.getString("no_players");
                maxPlayer.setText("(Max " + MAX_PLAYERS + " players*)");
                if (!MAX_PLAYERS.equals("0")) {
                  //  maxPlayer.setVisibility(View.VISIBLE);
                }
                MAX_PLAYERS = (Integer.parseInt(MAX_PLAYERS) - 1) + "";
                JSONArray slotArray = object.getJSONArray("slots");


                for (int i = 0; i < slotArray.length(); i++) {

                    if (slots.isEmpty()) {
                        slots = slotArray.getJSONObject(i).getString("start_time") + "-" + slotArray.getJSONObject(i).getString("end_time");
                    } else {
                        slots = slots + ", " + slotArray.getJSONObject(i).getString("start_time") + "-" + slotArray.getJSONObject(i).getString("end_time");
                    }
                }
                Log.e("*****", slots);

                String s = groundName + " [ " + slots + " ]";

                if (s.length() > 45) {
                    String s1 = s.substring(0, 45);
                    groundAndSlots.setText(vName + "\n" + s1 + "" + "..." + "");
                } else {
                    groundAndSlots.setText(vName + "\n" + s);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (repeat != null && repeat.equalsIgnoreCase("repeat")) {

            try {
                // JSONObject jsonObject = new JSONObject(getResponseString);
                gameDetailModel = new GameDetailModel();
                gson = new Gson();
                gameDetailModel = gson.fromJson(getResponseString, GameDetailModel.class);


                descriptValue = gameDetailModel.getSuccess().getDiscription();

                gameName = gameDetailModel.getSuccess().getName();
                gameType = gameDetailModel.getSuccess().getSport_name();
                loc = gameDetailModel.getSuccess().getLocation();
                //  privateGame = String.valueOf(gameDetailModel.getSuccess().is_private());
                //image = gameDetailModel.getSuccess().getCover_pic();
                sportsId = String.valueOf(gameDetailModel.getSuccess().getSport_id());


                Log.e("gameRepeat", repeat + "");
                Log.e("gameName", gameName + "");
                Log.e("gameType", gameType + "");
                Log.e("descriptValue", descriptValue + "");
                Log.e("loc", loc + "");
                // Log.e("coverImage", image + "");
                //  Log.e("date",date+"");
                //  Log.e("privateGame",privateGame+"");
                Log.e("sportId", sportsId + "");

                //  Log.e("sTime",sTime+"");
                //   Log.e("eTime",eTime+"");

                gameDetailMemberses = gameDetailModel.getSuccess().getMembers();
                for (int i = 0; i < gameDetailMemberses.size(); i++) {
                    players.add(gameDetailMemberses.get(i).getId() + "");
                    creatGameList.add(new GamePlayers(gameDetailMemberses.get(i).getId(), "players", gameDetailMemberses.get(i).getProfile_pic()));
                }

                Log.e("size", String.valueOf(players.size()));
                playersSelected = players.size();
                whoDoYouWantToPlayWith.setText(playersSelected + " players invited");

                nameOfGame.setText(gameName);
                descriptionOFGame.setText(descriptValue);

//                if (loc.length() > 45) {
//                    String s1 = loc.substring(0, 45);
//                    groundAndSlots.setText(s1 + "" + "..." + "");
//                } else {
//                    groundAndSlots.setText(loc);
//                }

                if (privateGame.equalsIgnoreCase("true")) {
                    isPrivate = true;
                    toggleButton.setChecked(true);
                } else {
                    isPrivate = false;
                    toggleButton.setChecked(false);
                }

                try {

                    //Glide.with(CreateGame.this).load(image).centerCrop().into(gameImage);
                    uploadImageIcon.setVisibility(View.GONE);
                    uploadTextGone.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (getTeamMembers != null && getTeamMembers.equalsIgnoreCase("members")) {

            if (teamMembers != null) {
                gson = new Gson();
                JSONArray jsonArray;
                memberModels = new ArrayList<>();
                try {
                    jsonArray = new JSONArray(teamMembers);
                    playersSelected = jsonArray.length();
                    for (int j = 0; j < playersSelected; j++) {

                        memberModels.add(gson.fromJson(jsonArray.get(j).toString(), TeamDetailMemberModel.class));
                        Log.e("memID", String.valueOf(memberModels.get(j).getId()));
                        players.add(memberModels.get(j).getId() + "");
//                        teams.add(teamId);
//                        creatGameList.add(new GamePlayers(memberModels.get(j).getId(), "players", memberModels.get(j).getProfile_pic()));
                    }
                    isFromTeam = true;
                    creatGameList.add(new GamePlayers(Integer.parseInt(teamId), "team"));
                    Log.e("size", String.valueOf(jsonArray.length()));
                    whoDoYouWantToPlayWith.setText(playersSelected + " players invited");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }


        try {


            if (sportIdFrom.equalsIgnoreCase("slots")) {
                sportsId = intent.getStringExtra("sportsId");
                //  posi = intent.getIntExtra("position",0);

                Log.e("!!!!!!!", sportsId + "");
               /* Log.e("!!!!!!!", posi + "");
                recyclerView.getLayoutManager().scrollToPosition(posi);*/
            }


            if (sportIdFrom.equalsIgnoreCase("gameDetail")) {
                sportsId = String.valueOf(gameDetailModel.getSuccess().getSport_id());
            }
            if (sportIdFrom == null) {
                sportsId = "";
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

      /*  if(cTeam!=null && cTeam.equalsIgnoreCase("create")){
            sportsId = intent.getStringExtra("sportsId");
        }else  if(repeat!=null && repeat.equalsIgnoreCase("repeat")){
            sportsId = String.valueOf(gameDetailModel.getSuccess().getSport_id());
        }else {
            sportsId="";
        }*/


        try {
            populateRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        attachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (shouldAskPermission()) {
                    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                    int permsRequestCode = 200;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    Log.e("popup", "popup");
                    CoverPhoto();

                }
            }


        });

        proceedPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkForValidation()) {
                    pay();
                }
            }
        });

        groundAndSlots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(cTeam != null && cTeam.equalsIgnoreCase("create"))) {
                    if (toastObject != null) {
                        toastObject.cancel();
                    }
                    if (gameType.isEmpty() && sportsId.isEmpty()) {
                        toastObject = Toast.makeText(CreateGame.this, "Select game to choose grounds and slots", Toast.LENGTH_SHORT);
                        toastObject.show();
                    } else {
                        slots = "";
                        Intent goToSelectLocation = new Intent(CreateGame.this, CreateGameSelectLocation.class);
                        goToSelectLocation.putExtra("sport_id", sportsId);
                        startActivityForResult(goToSelectLocation, 3);
                    }
                }
            }
        });

        numPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toastObject != null) {
                    toastObject.cancel();
                }
                if (!isFromTeam) {
                    if (!MAX_PLAYERS.isEmpty()) {
                        Intent goToSelectLocation = new Intent(CreateGame.this, CreateGameInviteFriends.class);
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("mylist", creatGameList);
                        goToSelectLocation.putExtras(bundle);
                        startActivityForResult(goToSelectLocation, 8);
                    } else {
                        toastObject = Toast.makeText(getApplicationContext(), "Please select the ground to invite the players", Toast.LENGTH_LONG);
                        toastObject.show();
                    }
                }
            }
        });

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isPrivate = true;

                } else {
                    isPrivate = false;
                }
            }
        });


        nameOfGame.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                nameOFGameLayout.setError(null);

            }
        });

        descriptionOFGame.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                descriptionLayout.setError(null);

            }
        });

       /* recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                try{
                    Log.e("DONE","DONE1" + scrollPosition);

                    for(int i=0 ; i<sportsListModel.getSuccess().size();i++){
                        if (sportsId.equalsIgnoreCase(sportsListModel.getSuccess().get(i).getId() + "")) {
                           // recyclerView.scrollToPosition(i);
                            Log.e("DONE","DONE2 " +i);
                        } else{

                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });*/

    }

    private void popUp() {

        final Dialog dialog = new Dialog(CreateGame.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancel_layout);
        TextView dialogTitle, dialogNo, dialogYes;

        dialogTitle = (TextView) dialog.findViewById(R.id.title);
        dialogNo = (TextView) dialog.findViewById(R.id.no);
        dialogYes = (TextView) dialog.findViewById(R.id.yes);

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogNo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogYes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if (permsRequestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm", "request");
                CoverPhoto();
            } else {
                Toast.makeText(CreateGame.this, "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private void pay() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(CreateGame.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            try {
                JSONObject object = new JSONObject();
                JSONObject grounObj = new JSONObject(jsonOb);

                final JSONObject memObj = new JSONObject();

                JSONArray jsonArray = new JSONArray(players);
                JSONArray jsonArray1 = new JSONArray(teams);
                memObj.put("teams", jsonArray1);
                memObj.put("players", jsonArray);
                Log.v("objarr", jsonArray1+"");
                if (!getGameId.isEmpty())
                    object.put("game_id", getGameId);
                if (!teamId.isEmpty())
                    object.put("team_id", teamId);
                object.put("name", nameOfGame.getText().toString());
                object.put("discription", descriptionOFGame.getText().toString());
                //object.put("cover_pic",image);
                object.put("sport_id", sportsId);
                object.put("members", memObj);
                object.put("is_private", String.valueOf(isPrivate));
                object.put("ground_details", grounObj);
                totolAmount = grounObj.getString("total_price");
                Log.v("Amount", totolAmount);
                Log.v("obj@@@", memObj+"");


//                String selectedSlotsIds = "";
//                int numberOfSlots = grounObj.getJSONArray("slots").length();
//                for (int i = 0; i < numberOfSlots; i++) {
//                    selectedSlotsIds = selectedSlotsIds + grounObj.getJSONArray("slots").getJSONObject(i).getString("id") + ",";
//                }
//                mPref.setSelectedSlotIds(selectedSlotsIds);

                Log.e("JSONOBJ", object.toString());
                StringEntity entity = new StringEntity(object.toString());

                httpClient.post(CreateGame.this, Config.CREATE_GAME, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        response = new String(responseBody);
                        Log.v("gameCreate", response);
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object.getInt("statusCode") == 200) {
                                String date = object.getString("date");//date is rawdate
                                String id = String.valueOf(object.getJSONObject("success").getInt("id"));
                                Intent toGamePaymentMain = new Intent(CreateGame.this, GamePaymentMain.class);
                                toGamePaymentMain.putExtra("gameName", nameOfGame.getText().toString());
                                toGamePaymentMain.putExtra("rawdate", date);
                                toGamePaymentMain.putExtra("game_id", id);
                                toGamePaymentMain.putExtra("amount", totolAmount);
                                toGamePaymentMain.putExtra("response", response);
                                toGamePaymentMain.putExtra("payment", "creatGame");
                                if (players.size() > 0)
                                    toGamePaymentMain.putExtra("canInvite", true);
                                startActivity(toGamePaymentMain);
                                finish();
                                // startPayment();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                                mPref.setAccessToken("");
                                Toast.makeText(CreateGame.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(CreateGame.this, LoginActivity.class);
                                startActivity(intent);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    finishAffinity();
                                }
                            } else {
                                Toast.makeText(CreateGame.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                String s = new String(responseBody);
                                Log.e("failure", s);
                                JSONObject jsonObject = new JSONObject(s);
                                Toast.makeText(CreateGame.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkForValidation() {
        if (toastObject != null) {
            toastObject.cancel();
        }

        /*if(image.isEmpty()){
            toastObject = Toast.makeText(CreateGame.this,"Please select cover image",Toast.LENGTH_SHORT);
            toastObject.show();
            return false;
        }*/

        if (nameOfGame.getText().toString().isEmpty()) {
            nameOFGameLayout.setError("Enter game name");
            return false;
        } else {
            nameOFGameLayout.setErrorEnabled(false);
        }


        if (gameType.isEmpty() && sportsId.isEmpty()) {
            toastObject = Toast.makeText(CreateGame.this, "Select type of game to choose ground and slots", Toast.LENGTH_SHORT);
            toastObject.show();
            return false;
        }
        if (MAX_PLAYERS.isEmpty()) {
            toastObject = Toast.makeText(CreateGame.this, "Select ground and slots", Toast.LENGTH_SHORT);
            toastObject.show();
            return false;
        }

     /*   if (playersSelected > Integer.parseInt(MAX_PLAYERS)) {
            toastObject = Toast.makeText(CreateGame.this, "Number of players exceeded the Maximum limit", Toast.LENGTH_SHORT);
            toastObject.show();
            return false;
        }*/

      /*  if(players.size()>0 || teams.size()>0){

        }else{
            toastObject =Toast.makeText(CreateGame.this,"Select players",Toast.LENGTH_SHORT);
            toastObject.show();
            return false;
        }*/

        return true;
    }

    private void CoverPhoto() {
        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateGame.this, android.R.layout.select_dialog_item, items);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CreateGame.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        startActivityForResult(intent, PICK_FROM_CAM);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    private boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }

        if (requestCode == 3) {
            jsonOb = data.getStringExtra("JsonObj");
            Log.e("jsonResp", jsonOb + "");
            try {
                object = new JSONObject(jsonOb);
                vName = object.getString("vName");
                MAX_PLAYERS = object.getString("no_players");
                maxPlayer.setText("(*Max " + MAX_PLAYERS + " players)");
                if (!MAX_PLAYERS.equals("0")) {
                  //  maxPlayer.setVisibility(View.VISIBLE);
                }
                MAX_PLAYERS = (Integer.parseInt(MAX_PLAYERS) - 1) + "";
                JSONArray slotArray = object.getJSONArray("slots");

                for (int i = 0; i < slotArray.length(); i++) {

                    if (slots.isEmpty()) {
                        slots = slotArray.getJSONObject(i).getString("start_time") + "-" + slotArray.getJSONObject(i).getString("end_time");
                    } else {
                        slots = slots + ", " + slotArray.getJSONObject(i).getString("start_time") + "-" + slotArray.getJSONObject(i).getString("end_time");
                    }
                }
                Log.e("*****", slots);

            } catch (JSONException e) {
                e.printStackTrace();
            }

//                vName = data.getStringExtra("vName");
            groundName = data.getStringExtra("groundName");
            Log.e("groundName", groundName);

            String s = groundName + " " + "[ " + slots + " ]";

            if (s.length() > 45) {
                String s1 = s.substring(0, 45);
                groundAndSlots.setText(vName + "\n" + s1 + "" + "..." + "");
            } else {
                groundAndSlots.setText(vName + "\n" + s);
            }

              /*  if(slots.length()>20){
                    String s = slots.substring(0,20);
                    groundAndSlots.setText(groundName+" "+"["+s+"]"+"...");
                }else {
                    groundAndSlots.setText(groundName+" "+"["+slots+"]");
                }*/


        } else {
            Log.e("data", "Null");
        }

        if (requestCode == 8) {
            // strings =  data.getStringArrayListExtra("members");
            creatGameList = new ArrayList<>();
            Bundle bundle = data.getExtras();
            creatGameList = bundle.getParcelableArrayList("mylist");
            players = new ArrayList<>();
            teams = new ArrayList<>();
            // Log.e("membersList", String.valueOf(strings));
            for (int i = 0; i < creatGameList.size(); i++) {
                if (creatGameList.get(i).getType().equalsIgnoreCase("player") || creatGameList.get(i).getType().equalsIgnoreCase("players")) {
                    players.add(creatGameList.get(i).getId() + "");
                } else {
                    teams.add(creatGameList.get(i).getId() + "");
                }
            }
            Log.e("PPLAYERS", "" + players.size());
            Log.e("TEAMSS", "" + teams.size());
            Set<String> hs = new HashSet<>();
            hs.addAll(teams);
            teams.clear();
            teams.addAll(hs);
            Log.e("TEAMSS HAsH", "" + teams.size());
            playersSelected = creatGameList.size();
            whoDoYouWantToPlayWith.setText(playersSelected + " players selected");
        }


        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                try {
                    selectedImagePath = getPath(selectedImageUri);
                } catch (NullPointerException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //gameImage.setImageBitmap(bm);
                    uploadImageIcon.setVisibility(View.GONE);
                    uploadTextGone.setVisibility(View.GONE);
                    getProgressBar.setVisibility(View.VISIBLE);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                //uploadFile(selectedImagePath);
                            } catch (Exception e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        gameImage.setVisibility(View.GONE);
                                        uploadImageIcon.setVisibility(View.VISIBLE);
                                        uploadTextGone.setVisibility(View.VISIBLE);
                                        getProgressBar.setVisibility(View.GONE);
                                        attachImage.setClickable(true);
                                        Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                final Uri mImageCaptureUri = Uri.fromFile(destination);

                System.out.println("Image Path : " + mImageCaptureUri);
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedImagePath = getRealPathFromURI(mImageCaptureUri.toString(), CreateGame.this);
                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }
                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //gameImage.setImageBitmap(bm);
                    uploadImageIcon.setVisibility(View.GONE);
                    uploadTextGone.setVisibility(View.GONE);
                    getProgressBar.setVisibility(View.VISIBLE);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(selectedImagePath);
                            } catch (Exception e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        //gameImage.setVisibility(View.GONE);
                                        uploadImageIcon.setVisibility(View.VISIBLE);
                                        uploadTextGone.setVisibility(View.VISIBLE);
                                        getProgressBar.setVisibility(View.GONE);
                                        attachImage.setClickable(true);
                                        Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    }).start();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private void uploadFile(final String selectedImagePath) {

        attachImage.setClickable(false);

        String fileName = selectedImagePath;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File sourceFile = new File(PlaythoraUtility.compressImage(selectedImagePath, CreateGame.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :" + selectedImagePath);

            runOnUiThread(new Runnable() {
                public void run() {
                    gameImage.setVisibility(View.GONE);
                    getProgressBar.setVisibility(View.GONE);
                    uploadImageIcon.setVisibility(View.VISIBLE);
                    uploadTextGone.setVisibility(View.VISIBLE);
                    attachImage.setClickable(true);
                    Toast.makeText(CreateGame.this, "Source File not exist :" + selectedImagePath, Toast.LENGTH_SHORT).show();
                }
            });

            return;

        } else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.UPLOAD_IMAGE);
                PlaythoraUtility.trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                        + fileName + "\"" + lineEnd); //image is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS", "Content-Disposition: form-data; name=\"profile_pic\";filename=\"" + fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {

                            JSONObject response = null;
                            try {
                                response = new JSONObject(total.toString());
                                //image = response.getString("success");
                                // Glide.with(CreateGame.this).load(image).centerCrop().into(gameImage);
                                getProgressBar.setVisibility(View.GONE);
                                //  Toast.makeText(CreateGame.this, "Upload image Complete.", Toast.LENGTH_SHORT).show();
                                attachImage.setClickable(true);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else if (serverResponseCode == 400 || serverResponseCode == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gameImage.setVisibility(View.GONE);
                            getProgressBar.setVisibility(View.GONE);
                            uploadImageIcon.setVisibility(View.VISIBLE);
                            uploadTextGone.setVisibility(View.VISIBLE);
                            attachImage.setClickable(true);
                        }
                    });
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    /*JSONObject object = null;
                    mPref.setAccessToken("");
                    Intent intent = new Intent(CreateTeamActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();*/

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gameImage.setVisibility(View.GONE);
                            getProgressBar.setVisibility(View.GONE);
                            uploadImageIcon.setVisibility(View.VISIBLE);
                            uploadTextGone.setVisibility(View.VISIBLE);
                            attachImage.setClickable(true);
                            Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();

                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                gameImage.setVisibility(View.GONE);
                getProgressBar.setVisibility(View.GONE);
                uploadImageIcon.setVisibility(View.VISIBLE);
                uploadTextGone.setVisibility(View.VISIBLE);
                attachImage.setClickable(true);
                Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();


            } catch (MalformedURLException e) {
                e.printStackTrace();
                gameImage.setVisibility(View.GONE);
                getProgressBar.setVisibility(View.GONE);
                uploadImageIcon.setVisibility(View.VISIBLE);
                uploadTextGone.setVisibility(View.VISIBLE);
                attachImage.setClickable(true);
                Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();


            } catch (ProtocolException e) {
                e.printStackTrace();
                gameImage.setVisibility(View.GONE);
                getProgressBar.setVisibility(View.GONE);
                uploadImageIcon.setVisibility(View.VISIBLE);
                uploadTextGone.setVisibility(View.VISIBLE);
                attachImage.setClickable(true);
                Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();


            } catch (IOException e) {
                e.printStackTrace();
                gameImage.setVisibility(View.GONE);
                getProgressBar.setVisibility(View.GONE);
                uploadImageIcon.setVisibility(View.VISIBLE);
                uploadTextGone.setVisibility(View.VISIBLE);
                attachImage.setClickable(true);
                Toast.makeText(CreateGame.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();


            }
        }

    }

    private String getRealPathFromURI(String s, Context context) {
        Uri contentUri = Uri.parse(s);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private String getPath(Uri selectedImageUri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }

    private void populateRecyclerView() {
        Log.e("SportsList", "!!!!!!");

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            httpClient.get(Config.SPORTS_LIST_GAME, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("GameList", s);

                        sportsListModel = new SportsListModel();
                        mGson = new Gson();
                        sportsListModel = mGson.fromJson(s, SportsListModel.class);
                        Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));

                        if (sportsListModel.getSuccess().size() != 0) {
                            Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                            if (recyclerView.getAdapter() == null) {
                                Log.e("adapter", "adapter");

//                                if (cTeam != null && cTeam.equalsIgnoreCase("create")) {
//                                    for (int i = 0; i < j; i++) {
//                                        if (sportsId.equalsIgnoreCase(sportsListModel.getSuccess().get(i).getId() + "")) {
//
//                                        } else {
//
//                                            //Log.e("length", "#######" + arrayList.size());
//                                            //Log.e("length", "#######" + i + selectedCatogorie[i]);
//                                        }
//                                    }
//                                } else {
//                                }

                                recyclerAdapter = new RecyclerViewAdapter(CreateGame.this, sportsListModel.getSuccess(), sportsId);
                                recyclerView.setAdapter(recyclerAdapter);
                                recyclerAdapter.notifyDataSetChanged();
                                for (int i = 0; i < sportsListModel.getSuccess().size(); i++) {
                                    if (sportsId.equalsIgnoreCase(sportsListModel.getSuccess().get(i).getId() + "")) {
                                        scrollPosition = i;
                                        recyclerView.scrollToPosition(scrollPosition);
                                        Log.e("DONE", "DONE2 " + i);
                                    } else {

                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 400 || object.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(CreateGame.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(CreateGame.this, LoginActivity.class);
                            startActivity(intent);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                finishAffinity();
                            }
                        } else {
                            Toast.makeText(CreateGame.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(CreateGame.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(CreateGame.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            hideSoftKeyboard(this);
            onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (gameType.isEmpty() && sportsId.isEmpty()) {
                onBackPressed();
            } else {
                popUp();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.GameViewHolder> {

        public List<SportsSuccessList> arrayList;
        public Context context;
        Boolean[] selectedCatogorie;
        // String sportsID;
        int count;
        private ArrayList<Integer> mDisabledRows;


        public RecyclerViewAdapter(CreateGame context, List<SportsSuccessList> success, String sportsId) {

            this.context = context;
            this.arrayList = success;
            j = arrayList.size();
            selectedCatogorie = new Boolean[j];

            for (int i = 0; i < j; i++) {
                if (sportsId.equalsIgnoreCase(arrayList.get(i).getId() + "")) {
                    selectedCatogorie[i] = true;
                } else {
                    selectedCatogorie[i] = false;
                    //Log.e("length", "#######" + arrayList.size());
                    //Log.e("length", "#######" + i + selectedCatogorie[i]);
                }
            }

        }


        @Override
        public GameViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.create_game_items, viewGroup, false);
            GameViewHolder listHolder = new GameViewHolder(mainGroup);
            mDisabledRows = new ArrayList<>();
            return listHolder;
        }


        @Override
        public int getItemCount() {
            //Log.e("size", String.valueOf(arrayList.size()));
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final GameViewHolder holder, final int position) {
            final SportsSuccessList model = arrayList.get(position);


            final GameViewHolder mainHolder = (GameViewHolder) holder;

            try {
                mainHolder.sportsName.setTypeface(PlaythoraUtility.getFont(CreateGame.this, RALEWAY_REGULAR));
                mainHolder.sportsName.setText(model.getName());
                //Log.v("MainHolder",model.getName());

            } catch (NullPointerException e) {
            }

            if ((cTeam != null && cTeam.equalsIgnoreCase("create")) && !selectedCatogorie[position]) {
                mainHolder.sportsLayout.setVisibility(View.GONE);
                mainHolder.sportsName.setVisibility(View.GONE);
            } else if (!selectedCatogorie[position]) {
                mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                mainHolder.sportsName.setTextColor(getResources().getColor(R.color.text_one));
            } else {
                mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                mainHolder.sportsName.setTextColor(getResources().getColor(R.color.white));
                //recyclerView.smoothScrollToPosition(recyclerView, null,position);
               /* new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.scrollToPosition(position);
                    }
                }, 2000);*/
            }


            mainHolder.sportsLayout.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {
                    if (!(cTeam != null && cTeam.equalsIgnoreCase("create"))) {
                        gameType = model.getName();
                        sportsId = String.valueOf(model.getId());


                        if (!selectedCatogorie[position]) {
                            //  Toast.makeText(getApplicationContext(),"SelectedFilter"+model.getName(),Toast.LENGTH_SHORT).show();
                            selectedCatogorie[position] = true;
                            mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                            mainHolder.sportsName.setTextColor(getResources().getColor(R.color.white));

                            MAX_PLAYERS = "";
//                            players = new ArrayList<String>();
//                            teams = new ArrayList<String>();
                            if (getTeamMembers != null) {
                                if (getTeamMembers.equalsIgnoreCase("members")) {
                                    groundAndSlots.setText("Grounds and slots");
                                    maxPlayer.setText("");
                                    maxPlayer.setVisibility(View.GONE);
                                }
                            } else {
                                creatGameList.clear();
                                whoDoYouWantToPlayWith.setText("Who do you want to play with?");
                                groundAndSlots.setText("Grounds and slots");
                                maxPlayer.setText("");
                                maxPlayer.setVisibility(View.GONE);
                            }


                        } else {
                            selectedCatogorie[position] = false;
                            mainHolder.sportsLayout.setBackground(getResources().getDrawable(R.drawable.leader_board_items));
                        }
                        for (int i = 0; i < j; i++) {
                            if (i == position) {
                                selectedCatogorie[i] = true;
                            } else {
                                selectedCatogorie[i] = false;
                            }

                        }
                        notifyDataSetChanged();
                    }
                }
            });

            mainHolder.itemView.setTag(model);


        }

        public class GameViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout sportsLayout;
            TextView sportsName;

            public GameViewHolder(View view) {
                super(view);
                this.sportsName = (TextView) view.findViewById(R.id.leaderBoardChipCloud);
                this.sportsLayout = (RelativeLayout) view.findViewById(R.id.sportCatogories);
            }

        }

    }
}
