package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 28/12/16.
 */
public class GameDetailSuccess {

    private boolean is_mine;
    private boolean is_member;
    private boolean is_invited_pay;
    private boolean showRSVP;
    private boolean payment_required;
    private boolean is_super_admin;
    private boolean is_requested;
    private int id;
    private int price;

    private String name;
    private String cover_pic;
    private boolean is_private;
    private String location;
    private String max_members;
    private String no_members;
    private String discription;
    private String sport_name;
    private String date;
    private String raw_date;
    private int sport_id;
    private int ground_id;
    private boolean is_completed;
    private String start_time;
    private String end_time;
    private List<GameDetailMembers> members;
    private List<GameDeatailInvitedMembers> invited_members;
    private Boolean can_join;
    private boolean mark_attendance;
    private boolean is_refundable;
    private boolean can_shout;

    public GameDetailSuccess() {
    }

    public boolean is_requested() {
        return is_requested;
    }

    public void setIs_requested(boolean is_requested) {
        this.is_requested = is_requested;
    }

    public List<GameDeatailInvitedMembers> getInvited_members() {
        return invited_members;
    }

    public void setInvited_members(List<GameDeatailInvitedMembers> invited_members) {
        this.invited_members = invited_members;
    }

    public int getPrice() {
        return price;

    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public boolean is_completed() {
        return is_completed;
    }

    public void setIs_completed(boolean is_completed) {
        this.is_completed = is_completed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRaw_date() {
        return raw_date;
    }

    public void setRaw_date(String raw_date) {
        this.raw_date = raw_date;
    }

    public boolean isShowRSVP() {
        return showRSVP;
    }

    public void setShowRSVP(boolean showRSVP) {
        this.showRSVP = showRSVP;
    }

    public boolean isPayment_required() {
        return payment_required;
    }

    public void setPayment_required(boolean payment_required) {
        this.payment_required = payment_required;
    }

    public boolean is_invited_pay() {
        return is_invited_pay;
    }

    public void setIs_invited_pay(boolean is_invited_pay) {
        this.is_invited_pay = is_invited_pay;
    }

    public boolean is_mine() {
        return is_mine;
    }

    public void setIs_mine(boolean is_mine) {
        this.is_mine = is_mine;
    }

    public boolean is_member() {
        return is_member;
    }

    public void setIs_member(boolean is_member) {
        this.is_member = is_member;
    }

    public List<GameDetailMembers> getMembers() {
        return members;
    }

    public void setMembers(List<GameDetailMembers> members) {
        this.members = members;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public boolean is_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMax_members() {
        return max_members;
    }

    public void setMax_members(String max_members) {
        this.max_members = max_members;
    }

    public String getNo_members() {
        return no_members;
    }

    public void setNo_members(String no_members) {
        this.no_members = no_members;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getSport_name() {
        return sport_name;
    }

    public void setSport_name(String sport_name) {
        this.sport_name = sport_name;
    }

    public int getSport_id() {
        return sport_id;
    }

    public void setSport_id(int sport_id) {
        this.sport_id = sport_id;
    }

    public int getGround_id() {
        return ground_id;
    }

    public void setGround_id(int ground_id) {
        this.ground_id = ground_id;
    }


    public Boolean getCan_join() {
        return can_join;
    }

    public void setCan_join(Boolean can_join) {
        this.can_join = can_join;
    }

    public boolean is_super_admin() {
        return is_super_admin;
    }

    public void setIs_super_admin(boolean is_super_admin) {
        this.is_super_admin = is_super_admin;
    }

    public boolean isMark_attendance() {
        return mark_attendance;
    }

    public void setMark_attendance(boolean mark_attendance) {
        this.mark_attendance = mark_attendance;
    }

    public boolean is_refundable() {
        return is_refundable;
    }

    public void setIs_refundable(boolean is_refundable) {
        this.is_refundable = is_refundable;
    }

    public boolean isCan_shout() {
        return can_shout;
    }

    public void setCan_shout(boolean can_shout) {
        this.can_shout = can_shout;
    }
}
