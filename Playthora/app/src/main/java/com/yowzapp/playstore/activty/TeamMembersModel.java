package com.yowzapp.playstore.activty;

/**
 * Created by hemanth on 14/11/16.
 */
public class TeamMembersModel {

    private String name;
    private String id;
    private String image;
    private String status;

    public TeamMembersModel(String name, String id, String image) {
        this.name = name;
        this.id = id;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
