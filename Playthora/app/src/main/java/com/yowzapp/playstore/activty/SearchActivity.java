package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 18/11/16.
 */

public class SearchActivity extends BaseActivity {

    public static RecyclerViewAdapter searchAdapter;
    public static ArrayList<RewardModel> searchCategory;
    EditText searchEditText;
    String responseString = "", responseString2 = "", responseString1 = "";
    ProgressDialog dialog;
    Gson mGson;
    Toolbar toolbar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);


        searchEditText = (EditText) findViewById(R.id.searchEdit);
        recyclerView = (RecyclerView) findViewById(R.id.searchCategories);

        searchEditText.setTypeface(PlaythoraUtility.getFont(SearchActivity.this, RALEWAY_REGULAR));
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL, false));

        responseString1 = "[{\n" +
                "    \"product\": \"nike blue\",\n" +
                "    \"productId\": 1,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"Sports\",\n" +
                "    \"categoryId\": \"20\",\n" +
                "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike blak\",\n" +
                "    \"productId\": 2,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"Clothes\",\n" +
                "    \"categoryId\": \"22\",\n" +
                "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 3,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"Accessories\",\n" +
                "    \"categoryId\": \"23\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 3,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"Jewels\",\n" +
                "    \"categoryId\": \"23\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 3,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"Tools\",\n" +
                "    \"categoryId\": \"23\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 3,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"Kits\",\n" +
                "    \"categoryId\": \"23\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "}]";
        try {
            populateRecyclerView(responseString1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void populateRecyclerView(String getResponse) throws Exception {

        responseString = getResponse;
        Thread thread = new Thread(new Runnable() {

            public void run() {

                try {


                    SearchActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                try {
                                    if (!SearchActivity.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                    e.printStackTrace();
                                }

                                JSONArray categoryArray = new JSONArray(responseString);
                                mGson = new Gson();
                                searchCategory = new ArrayList<RewardModel>();

                                for (int i = 0; i < categoryArray.length(); i++) {
                                    searchCategory.add(mGson.fromJson(categoryArray.get(i).toString(), RewardModel.class));

                                }

                                if (!searchCategory.isEmpty()) {

                                    if (recyclerView.getAdapter() == null) {

                                        searchAdapter = new RecyclerViewAdapter(SearchActivity.this, searchCategory);
                                        recyclerView.setAdapter(searchAdapter);// set searchAdapter on recyclerview
                                        searchAdapter.notifyDataSetChanged();


                                    } else {

                                    }

                                } else {

                                }


                            } catch (JsonParseException e) {

                                try {
                                    if (!SearchActivity.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception ee) {
                                    ee.printStackTrace();
                                }

                                e.printStackTrace();
                            } catch (JSONException e) {

                                try {
                                    if (!SearchActivity.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception eee) {
                                    eee.printStackTrace();
                                }

                                e.printStackTrace();
                            }

                        }
                    });
                } catch (Exception e) {
                }
            }
        });//
        thread.start();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    public class RecyclerViewAdapter extends
            RecyclerView.Adapter<RewardViewHolder> {

        private ArrayList<RewardModel> arrayList;
        private Context context;

        public RecyclerViewAdapter(Context context,
                                   ArrayList<RewardModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.search_activity_items, viewGroup, false);
            RewardViewHolder listHolder = new RewardViewHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {

            return arrayList.size();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final RewardViewHolder holder, final int position) {
            final RewardModel model = arrayList.get(position);


            final RewardViewHolder mainHolder = (RewardViewHolder) holder;

            try {
                mainHolder.searchCategoryName.setTypeface(PlaythoraUtility.getFont(SearchActivity.this, RALEWAY_REGULAR));
                mainHolder.searchCategoryName.setText(model.getCategories());

                if (position == arrayList.size() - 1) {
                    mainHolder.searchView.setVisibility(View.INVISIBLE);
                }
            } catch (NullPointerException e) {

            }

            mainHolder.itemView.setTag(model);


        }

    }

}
