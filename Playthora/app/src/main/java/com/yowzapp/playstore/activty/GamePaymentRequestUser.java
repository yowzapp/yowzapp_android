package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.CreateGameMembers;
import com.yowzapp.playstore.model.CreateGameMembersInvited;
import com.yowzapp.playstore.model.CreateGameModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by pramod on 25/11/16.
 */

public class GamePaymentRequestUser extends BaseActivity {

    public ArrayList<CreateGameMembers> teamMembersArrayList;
    public AddMembersFollowingAdapter adapter;
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    TextView invite;
    Toolbar toolbar;
    String membResponse;
    CreateGameModel createGameModel;
    TextView textView;
    PreferenceManager mPref;
    ;
    private String gameID, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_player_admin_recycler);
        mPref = PreferenceManager.instance(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Request Payment");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(GamePaymentRequestUser.this, Config.RALEWAY_REGULAR));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        try {
            Intent intent = getIntent();
            membResponse = intent.getStringExtra("RESPONSE");
            gameID = intent.getStringExtra("gameId");

            Log.e("RESPONSE", membResponse + "");
            Log.e("gameId", gameID + "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        recyclerView = (RecyclerView) findViewById(R.id.game_admin_recycler);
        textView = (TextView) findViewById(R.id.membersEmpty);

        recyclerView.setLayoutManager(new LinearLayoutManager(GamePaymentRequestUser.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);


        try {
            followingRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void followingRecycler() {
        try {
            createGameModel = new CreateGameModel();
            mGson = new Gson();
            Log.e("!!!!!", membResponse + "");
            createGameModel = mGson.fromJson(membResponse, CreateGameModel.class);
            gameID = createGameModel.getSuccess().getId();

            if (!createGameModel.getSuccess().getMembers().isEmpty()) {
                if (recyclerView.getAdapter() == null) {
                    adapter = new AddMembersFollowingAdapter(GamePaymentRequestUser.this, createGameModel.getSuccess().getInvited_members());
                    recyclerView.setAdapter(adapter);// set adapter on recyclerview
                    adapter.notifyDataSetChanged();
                    textView.setVisibility(View.GONE);
                } else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("no members");
                }

            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText("no members");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPopUp(final String name, String id) {

        Log.e("popUp", name + "");
        Log.e("popUp", id + "");

        user_id = id;

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(GamePaymentRequestUser.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.game_make_payment_request_pop_up, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(GamePaymentRequestUser.this);
        alertDialogBuilderUserInput.setView(mView);
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

        final TextView userName = (TextView) mView.findViewById(R.id.userName);
        final TextView cancel = (TextView) mView.findViewById(R.id.cancel);
        final TextView yes = (TextView) mView.findViewById(R.id.yes);

        String description = "Do you want to add " + name + " to pay for the game?";
        userName.setText(description);

        userName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        cancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        yes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPay();
                alertDialogAndroid.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });


        alertDialogAndroid.show();
    }

    private void requestPay() {

        Log.e("###########", user_id + "");

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GamePaymentRequestUser.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("user_id", user_id);
            params.add("game_id", gameID);

            httpClient.post(Config.INVITE_TO_PAY, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("invitePay", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(GamePaymentRequestUser.this, "Player invited to pay", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GamePaymentRequestUser.this, HomeScreenActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        if (id == R.id.action_openSearch) {
            Intent toSearchActivity = new Intent(GamePaymentRequestUser.this, SearchActivity.class);
            startActivity(toSearchActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    public class AddMembersFollowingAdapter extends RecyclerView.Adapter<AddMembersFollowingHolder> {

        private List<CreateGameMembersInvited> arrayList;
        private Context context;


        public AddMembersFollowingAdapter(GamePaymentRequestUser context, List<CreateGameMembersInvited> members) {
            this.context = context;
            this.arrayList = members;
        }


        @Override
        public AddMembersFollowingHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.game_player_items, viewGroup, false);
            AddMembersFollowingHolder listHolder = new AddMembersFollowingHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final AddMembersFollowingHolder holder, final int position) {
            final CreateGameMembersInvited model = arrayList.get(position);


            final AddMembersFollowingHolder mainHolder = (AddMembersFollowingHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(GamePaymentRequestUser.this, Config.RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());
                // user_id = String.valueOf(model.getId());

                Log.e("name", model.getName() + "");
                // Log.e("user_id",user_id+"");

            } catch (NullPointerException e) {

            }

            mainHolder.playerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    String name = arrayList.get(position).getName();
                    String id = arrayList.get(position).getId() + "";

                    Log.e("name", name);
                    Log.e("id", id);

                    showPopUp(name, id);

                }
            });


            try {
                Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setTag(model);


        }
    }

    public class AddMembersFollowingHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView name;
        RelativeLayout playerLayout;

        public AddMembersFollowingHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.playerImageView);
            this.name = (TextView) view.findViewById(R.id.userName);
            this.playerLayout = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);

        }
    }
}

