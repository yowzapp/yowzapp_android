package com.yowzapp.playstore.model;

/**
 * Created by nakul on 15/11/16.
 */
public class SearchAllModelList {
    private String name;
    private int id;
    private String cover_pic;
    private String type;
    private int no_players;
    private boolean is_player;
    private boolean is_team;
    private boolean is_game;
    private boolean is_venue;
    private int points;
    private VenueListItem venue_details;
    private AddTeamMembersList player_details;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public int getNo_players() {
        return no_players;
    }

    public void setNo_players(int no_players) {
        this.no_players = no_players;
    }

    public boolean is_player() {
        return is_player;
    }

    public void setIs_player(boolean is_player) {
        this.is_player = is_player;
    }

    public boolean is_team() {
        return is_team;
    }

    public void setIs_team(boolean is_team) {
        this.is_team = is_team;
    }

    public boolean is_game() {
        return is_game;
    }

    public void setIs_game(boolean is_game) {
        this.is_game = is_game;
    }

    public boolean is_venue() {
        return is_venue;
    }

    public void setIs_venue(boolean is_venue) {
        this.is_venue = is_venue;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public VenueListItem getVenue_details() {
        return venue_details;
    }

    public void setVenue_details(VenueListItem venue_details) {
        this.venue_details = venue_details;
    }

    public AddTeamMembersList getPlayer_details() {
        return player_details;
    }

    public void setPlayer_details(AddTeamMembersList player_details) {
        this.player_details = player_details;
    }
}
