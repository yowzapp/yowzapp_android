package com.yowzapp.playstore.application;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PlaythoraUtility;

/**
 * Created by maheshprajapat on 2/20/18.
 */

public class BaseActivity extends AppCompatActivity {

    boolean mIsReceiverRegistered = false;
    MyBroadcastReceiver mReceiver = null;
    boolean hasInternet = false;
    Context currentContext;

    @Override
    protected void onResume() {
        if (PlaythoraUtility.checkInternetConnection(BaseActivity.this)) {
            Log.e("DIALOG", "resume DISMISS");
            hideDialog();
        } else {
            Log.e("DIALOG", "resume SHOW");
            showDialog();
        }


        super.onResume();


        if (!mIsReceiverRegistered) {
            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("BaseActivityIntent"));
            mIsReceiverRegistered = true;
        }
    }


    @Override
    protected void onPause() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }

        super.onPause();

    }

    private void updateUI() {
        Log.e("Method CALLED", "UPDATE UI");
        hideDialog();
        onResume();
    }

    private class MyBroadcastReceiver extends BroadcastReceiver {

        private static final String LOG_TAG = "NetworkChangeReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("Activity", "Base Activity");
            currentContext = context;
            if (intent.hasExtra("status")) {
                if (intent.getStringExtra("status").equals("CONNECTED")) {
                    hasInternet = true;
                    updateUI();
                } else {
                    hasInternet = false;
//                    getActionBar().hide();
                    showDialog();
                }
            }
        }
    }

    public AlertDialog internetDialog;

    public void showDialog() {
        Log.e("Dialog STATUS", "SHOWING");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(View.inflate(this, R.layout.no_internet, null))
                .setCancelable(false);

        hideDialog();

        internetDialog = builder.create();
        internetDialog.show();
    }

    public void hideDialog() {
        if (internetDialog != null && internetDialog.isShowing())
            internetDialog.dismiss();
    }
}