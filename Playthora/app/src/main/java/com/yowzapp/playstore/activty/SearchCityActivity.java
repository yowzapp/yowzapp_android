package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.LocationList;
import com.yowzapp.playstore.model.LocationModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 14/11/16.
 */
public class SearchCityActivity extends BaseActivity {

    public List<LocationList> cityArrayList;
    SearchCityAdapter adapter;
    RelativeLayout relativeLayout;
    Gson mGson;
    PreferenceManager mPref;
    LocationModel locationModel;
    String mSearchQuery, notifyCount;
    ImageView notification, rewartStore;
    TextView notify_count;
    private ListView mSearchList;
    private EditText mSearchText;
    private TextView mTitle, mLocation, emptyCity;
    private Toolbar mSearchToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_city_layout);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        initializeAllComponents();

        if (mPref.getLocationName().isEmpty()) {
            mLocation.setText("Update location");
        } else {
            mLocation.setText(mPref.getLocationName());
        }

        notifyCount = "";
        if (getIntent().hasExtra("NCOUNT")) {
            notifyCount = getIntent().getStringExtra("NCOUNT");
            if (!notifyCount.isEmpty()) {
                notify_count.setText(notifyCount);
                notify_count.setVisibility(View.VISIBLE);
            } else {
                notify_count.setText("");
                notify_count.setVisibility(View.GONE);
            }
        }

        mSearchText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mLocation.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        emptyCity.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        loadCity();


        mSearchText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mSearchQuery = mSearchText.getText().toString();
                cityArrayList = performSearch(locationModel.getSuccess(), mSearchQuery);

                if (cityArrayList.size() == 0) {
                    emptyCity.setVisibility(View.VISIBLE);
                } else {
                    emptyCity.setVisibility(View.GONE);
                }

                getListAdapter().update(cityArrayList);
                getListAdapter().notifyDataSetChanged();

            }
        });

        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    return true; // Focus will do whatever you put in the logic.
                }
                return false;  // Focus will change according to the actionId
            }
        });


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPref.getAccessToken().isEmpty())
                    PlaythoraUtility.LoginDialog(SearchCityActivity.this);
                else {
                    notify_count.setText("");
                    notify_count.setVisibility(View.GONE);
                    Intent notification = new Intent(SearchCityActivity.this, NotificationActivity.class);
                    startActivity(notification);
                }
            }
        });

        rewartStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showDeleteDialog();
                onBackPressed();
            }
        });
   /*     mSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("location", cityArrayList.get(i).getName());
                Intent intent = new Intent();
                intent.putExtra("LOCATION", cityArrayList.get(i).getName());
                setResult(5, intent);
                finish();
            }
        });*/

    }

    private void showDeleteDialog() {
        final Dialog dialog = new Dialog(SearchCityActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_team_dialog);
        TextView dialogTitle, dialogStatus, dialogCancel, dialogOk;

        dialogTitle = (TextView) dialog.findViewById(R.id.delete_title);
        dialogStatus = (TextView) dialog.findViewById(R.id.delete_confirm);
        dialogCancel = (TextView) dialog.findViewById(R.id.cancel_text);
        dialogOk = (TextView) dialog.findViewById(R.id.delete_text);
        dialogCancel.setVisibility(View.INVISIBLE);
        dialogOk.setText("OK");
        dialogTitle.setText("Hey, that's Reward Store");

        dialogStatus.setText("It's getting ready for you.");

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogOk.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private List<LocationList> performSearch(List<LocationList> success, String query) {
        // First we split the query so that we're able
        // to search word by word (in lower case).
        String[] queryByWords = query.toLowerCase().split("\"([\\w\\s]+)\"");

        // Empty list to fill with matches.
        List<LocationList> mFiltered = new ArrayList<LocationList>();

        // Go through initial releases and perform search.
        for (LocationList movie : locationModel.getSuccess()) {

            // Content to search through (in lower case).
            String content = (
                    movie.getName()).toLowerCase();

            for (String word : queryByWords) {

                // There is a match only if all of the words are contained.
                int numberOfMatches = queryByWords.length;

                // All query words have to be contained,
                // otherwise the release is filtered out.
                if (content.contains(word)) {
                    numberOfMatches--;
                } else {
                    break;
                }

                // They all match.
                if (numberOfMatches == 0) {
                    mFiltered.add(movie);
                }

            }

        }

        return mFiltered;
    }

    private SearchCityAdapter getListAdapter() {
        return (SearchCityAdapter) mSearchList.getAdapter();
    }

    private void loadCity() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(SearchCityActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            httpClient.get(Config.LOCATION_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("locationList", s);

                        locationModel = new LocationModel();
                        mGson = new Gson();
                        locationModel = mGson.fromJson(s, LocationModel.class);
                        if (locationModel.getSuccess().size() != 0) {
                            adapter = new SearchCityAdapter(SearchCityActivity.this, locationModel.getSuccess());
                            mSearchList.setAdapter(adapter);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        mSearchList = (ListView) findViewById(R.id.search_list);
        mSearchText = (EditText) findViewById(R.id.search_box);
        mSearchToolbar = (Toolbar) findViewById(R.id.search_toolbar);
        setSupportActionBar(mSearchToolbar);
        mTitle = (TextView) findViewById(R.id.home_toolbar_title);
        emptyCity = (TextView) findViewById(R.id.no_city);
        mLocation = (TextView) findViewById(R.id.city_name);
        relativeLayout = (RelativeLayout) findViewById(R.id.main_search);
        notification = (ImageView) findViewById(R.id.home_notification);
        rewartStore = (ImageView) findViewById(R.id.home_go);
        notify_count = (TextView) findViewById(R.id.notification_count);
        setupUI(relativeLayout);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(SearchCityActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            intent.putExtra("NCOUNT", notify_count.getText().toString());
            setResult(5, intent);
            finish();
            return true;
        }
        return false;
    }

    public class SearchCityAdapter extends BaseAdapter {
        List<LocationList> arrayList;
        Context context;

        public SearchCityAdapter(Context context, List<LocationList> cityArrayList) {
            this.context = context;
            this.arrayList = cityArrayList;

        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            final CityHolder holder;
            final LocationList model = arrayList.get(position);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            if (row == null) {
                row = inflater.inflate(R.layout.search_city_item, null);

                holder = new CityHolder();

                holder.mCity = (TextView) row.findViewById(R.id.tv_city);
                holder.relativeLayout = (RelativeLayout) row.findViewById(R.id.city_item_layout);

                holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.putExtra("LOCATION", model.getName());
                        intent.putExtra("NCOUNT", notify_count.getText().toString());
                        setResult(5, intent);
                        finish();
                    }
                });

                row.setTag(holder);
            } else holder = (CityHolder) row.getTag();

            try {
                holder.mCity.setTypeface(PlaythoraUtility.getFont(SearchCityActivity.this, RALEWAY_REGULAR));
                holder.mCity.setText(model.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return row;
        }

        public void update(List<LocationList> cityArrayList) {
            arrayList = cityArrayList;
        }

        class CityHolder {
            TextView mCity;
            RelativeLayout relativeLayout;
        }
    }
}


