package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

/**
 * Created by Nakul on 7/27/2016.
 */
public class ProfileSuccessActivity extends BaseActivity {

    TextView mTitle, mSubTitle;
    ImageView successImage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_success_layout);

        mTitle = (TextView) findViewById(R.id.awesome_text);
        mSubTitle = (TextView) findViewById(R.id.sec_text);
        successImage = (ImageView) findViewById(R.id.profile_success_image);
        successImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileSuccessActivity.this, HomeScreenActivity.class);
                startActivity(i);
                finish();
            }
        });

        mTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_BOLD));
        mSubTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
