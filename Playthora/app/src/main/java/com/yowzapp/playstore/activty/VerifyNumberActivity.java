package com.yowzapp.playstore.activty;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.activty.PlayerProfileEditActivity.EDIT_NUMBER;


public class VerifyNumberActivity extends BaseActivity {
    Toolbar mToolbar;
    TextView mPhoneNum, mTitle, mSubText, otpdoesntmatch, timeText;
    EditText mCode;
    Button mResend;
    String phoneNumber;
    PreferenceManager mPref;
    String[] permissionsSms = new String[]{
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
    };
    CountDownTimer waitingTimer, retryTimer;
    private String marshmallow = "lollipop";
    private boolean isSMSRecevied = false;
    private boolean writeAccepted;
    private int PERMISSION_REQUEST_CODE = 200;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private boolean numberEditing = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_num_layout);

        initializeAll();

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(getApplicationContext(),"onBackPressed",Toast.LENGTH_SHORT).show();
                onBackPressed();
                waitingTimer.cancel();
            }
        });

        Intent intent = getIntent();
        phoneNumber = intent.getStringExtra("Number");
        mPhoneNum.setText(phoneNumber);
        //  mPref.setUserMobileNumber(phoneNumber);

        if(intent.hasExtra("numberEditing")){
            numberEditing = intent.getBooleanExtra("numberEditing", false);
        }

        if (shouldAskPermission()) {
            writeAccepted = true;
            String[] perms = {"android.permission.RECEIVE_SMS", "android.permission.READ_SMS"};

            int permsRequestCode = 200;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, permsRequestCode);
            }
        } else {
            writeAccepted = false;
            // timerstart();
            mobileVerify();
        }

        mResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCode.setText(" ");
                resendOtp();
            }
        });

        mCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
                    if (checkForValidation()) {
                        if (mCode.getText().toString().contains(" ")) {
                            mCode.setText(mCode.getText().toString().replaceAll(" ", ""));
                            mCode.setSelection(mCode.getText().length());

                            Toast.makeText(getApplicationContext(), "No Spaces Allowed", Toast.LENGTH_LONG).show();
                        } else {
                            verifyOtp();
                        }

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                otpdoesntmatch.setVisibility(View.GONE);
            }
        });

        // timerstart();
        //registerSMSReceiver();

    }

    private boolean shouldAskPermission() {

        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                Log.e("RERERE", writeAccepted + "");
                if (writeAccepted) {
                    //timerstart();
                    mobileVerify();

                } else {
                    marshmallow = "MarshMallow";
                    SmsRadar.stopSmsRadarService(VerifyNumberActivity.this);
                    mobileVerify();

                }
                break;

        }

    }

    private void mobileVerify() {
        if (!marshmallow.equalsIgnoreCase("MarshMallow")) {
            registerSMSReceiver();
            timerstart();
        } else {
            timerstart();
            Toast.makeText(VerifyNumberActivity.this, "Please enter manually", Toast.LENGTH_SHORT).show();
        }
//        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
//            PlaythoraUtility.showProgressDialog(VerifyNumberActivity.this);
//            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
//            RequestParams params = new RequestParams();
//            params.add("mobile", phoneNumber);
//            params.add("user_id", mPref.getId());
//
//            httpClient.post(Config.MOBILE_VEROFICATION, params, new AsyncHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                    PlaythoraUtility.hideProgressDialog();
//                    String s = new String(responseBody);
//                    Log.e("mobileSuccess", s);
//
//                    try {
//                        JSONObject object = new JSONObject(s);
//                        if (object.getInt("statusCode") == 200) {
//                            Toast.makeText(VerifyNumberActivity.this, object.getString("message"), Toast.LENGTH_LONG).show();
//
//                            if (!marshmallow.equalsIgnoreCase("MarshMallow")) {
//                                registerSMSReceiver();
//                                timerstart();
//                            } else {
//                                timerstart();
//                                Toast.makeText(VerifyNumberActivity.this, "Please enter manually", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                    PlaythoraUtility.hideProgressDialog();
//
//                    if (null != responseBody) {
//                        String s = new String(responseBody);
//                        Log.e("mobileFail", s);
//                    }
//
//                }
//            });
//        }
    }

//    private void triggerLocalNotification(String otp) {
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.ic_launcher)
//                        .setContentTitle("OTP")
//                        .setContentText(otp);
//        mBuilder.build();
//
//        NotificationManager notificationManager =
//                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(9 /* ID of notification */, mBuilder.build());
//    }

    private void timerstart() {
        waitingTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.e("time", String.valueOf(millisUntilFinished / 1000));
                if (String.valueOf(millisUntilFinished / 1000).length() == 2) {
                    timeText.setText("0:" + millisUntilFinished / 1000);
                } else {
                    timeText.setText("0:0" + millisUntilFinished / 1000);
                }

                mResend.setEnabled(false);
                timeText.setTextColor(getResources().getColor(R.color.text_one));
                timeText.setTypeface(Typeface.DEFAULT);
                timeText.setTextSize(16);


            }

            public void onFinish() {

                if (!isSMSRecevied) {
                    retryTimer = new CountDownTimer(1000, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            timeText.setText("0:" + millisUntilFinished / 1000);
                            try {
                                //  registerSMSReceiver();
                            } catch (NullPointerException e) {

                            }

                        }

                        @Override
                        public void onFinish() {
                            // otpdoesntmatch.setVisibility(View.VISIBLE);
                            SmsRadar.stopSmsRadarService(VerifyNumberActivity.this);
                            mResend.setEnabled(true);
                            timeText.setText("Please enter the code in SMS manually");
                            timeText.setTextColor(getResources().getColor(R.color.red));
                            timeText.setTextSize(10);
                            timeText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));


                        }

                    };
                    retryTimer.start();
                }

            }
        };
        waitingTimer.start();
    }

    private void resendOtp() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerifyNumberActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("user_id", mPref.getId());
            params.add("mobile", phoneNumber);

            httpClient.post(Config.RESEND_OTP, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("resendSuccess", s);

                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statuscode") == 200) {
                            Toast.makeText(VerifyNumberActivity.this, "OTP resent successful", Toast.LENGTH_SHORT).show();
                            if (!marshmallow.equalsIgnoreCase("MarshMallow")) {
                                registerSMSReceiver();

                            } else {
                                timerstart();
                                Toast.makeText(VerifyNumberActivity.this, "Please enter manually", Toast.LENGTH_SHORT).show();
                            }
                            // timerstart();
                            //registerSMSReceiver();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    if (null != responseBody) {
                        String s = new String(responseBody);
                        Log.e("resendFailure", s);
                    }


                }
            });


        }
    }

    private void registerSMSReceiver() {

        SmsRadar.initializeSmsRadarService(VerifyNumberActivity.this, new SmsListener() {

            @Override
            public void onSmsSent(Sms sms) {

            }

            @Override
            public void onSmsReceived(Sms sms) {
                Log.e("SMS", String.valueOf(sms));
                String val = String.valueOf(sms);
                if (val.contains("PLYTHR") || val.contains("PAZZOM")) {
                    if (val.contains("msg")) {
                        String id = new String(val);
                        int startindex = id.indexOf("OTP");
                        int endindex = id.indexOf("to");
                        String number = id.substring(startindex, endindex);
                        String numberOnly = number.replaceAll("[^0-9]", "");
                        Log.v("Number", numberOnly);
                        mCode.setText(numberOnly);
                        isSMSRecevied = true;
                        try {
                            waitingTimer.cancel();
                        } catch (Exception e) {
                        }
                        try {
                            verifyOtp();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private boolean checkForValidation() {

        if (mCode.getText().toString().trim().isEmpty() || mCode.getText().length() < 6) {
            otpdoesntmatch.setVisibility(View.VISIBLE);
            otpdoesntmatch.setText("Enter 6 digit OTP");
            return false;
        } else {
            otpdoesntmatch.setVisibility(View.GONE);
            return true;
        }
    }


    private void verifyOtp() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerifyNumberActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("user_id", mPref.getId());
            params.add("otp", mCode.getText().toString());
            params.add("mobile", phoneNumber);

            httpClient.post(Config.OTP_VERIFICATION, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("otpSuccess", s);

                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statuscode") == 200) {

                            mPref.setMobilVerified("true");
                            mPref.setUserMobileNumber(phoneNumber);
                            Log.e("verified", mPref.getMobilVerified());

                            waitingTimer.cancel();
                            //  Log.e("time", String.valueOf(millisUntilFinished / 1000));
                            Toast.makeText(VerifyNumberActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
//                            Intent home = new Intent(VerifyNumberActivity.this, ProfileSetUpActivity.class);
                            if(numberEditing){
                                Intent profileEdit = new Intent(VerifyNumberActivity.this, PlayerProfileEditActivity.class);
                                setResult(EDIT_NUMBER, profileEdit);
                                finish();
                            }
                            else {
                                Intent home = new Intent(VerifyNumberActivity.this, HomeScreenActivity.class);
                                startActivity(home);
                                finishAffinity();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("2", s);
                        mCode.setTextColor(Color.parseColor("#dd4b39"));
                        otpdoesntmatch.setVisibility(View.VISIBLE);
                        otpdoesntmatch.setText("Verification code doesn’t match");
                        //Toast.makeText(VerifyNumberActivity.this, "OTP mismatch", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    }


    private void initializeAll() {
        mPref = PreferenceManager.instance(getApplicationContext());
        mToolbar = (Toolbar) findViewById(R.id.toolbar_verify_num);
        mPhoneNum = (TextView) findViewById(R.id.tv_num);
        mCode = (EditText) findViewById(R.id.otp_num);
        otpdoesntmatch = (TextView) findViewById(R.id.otp_desnt_match);
        mResend = (Button) findViewById(R.id.resend_button);
        mTitle = (TextView) findViewById(R.id.verify_phone);
        mSubText = (TextView) findViewById(R.id.verify_sub_text);
        timeText = (TextView) findViewById(R.id.verificationCoundDownTime);

        mTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));
        mSubText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        mResend.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

    }


}
