package com.yowzapp.playstore.model;

/**
 * Created by nakul on 07/02/17.
 */
public class DateObjectModel {
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
