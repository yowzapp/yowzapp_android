package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.Grounds;
import com.yowzapp.playstore.model.HomeTopLocationModel;
import com.yowzapp.playstore.model.VenueListSportsTypeItem;
import com.yowzapp.playstore.model.VenueListSportsTypeModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by pramod on 23/11/16.
 */

public class CreateGameSelectLocation extends BaseActivity {

    public ArrayList<HomeTopLocationModel> selectLocationArrayList;
    public RecyclerViewAdapter locationAdapter;
    ProgressDialog dialog;
    String responseString;
    Gson mGson;
    Toolbar toolbar;
    PreferenceManager mPref;
    VenueListSportsTypeModel venueListSportsTypeModel;
    String sportId;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    Gson gson;
    List<VenueListSportsTypeItem> venueListSportsTypeItems;
    List<Grounds> groundsList;
    ArrayList<String> stringArrayList = new ArrayList<>();
    String venuResponse;
    TextView noVenues;
    String activity;
    private RecyclerView recyclerView;
    private int previousTotal = 0, pages = 1;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = false;
    private int visibleThreshold = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_game_select_location);
        mPref = PreferenceManager.instance(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity = "";

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Select a location");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(CreateGameSelectLocation.this, RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        //   progressBar = (ProgressBar) findViewById(R.id.progressBarInterest);
        //   showText = (TextView) findViewById(com.feetapart.yowzapp.R.id.noItems);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Intent intent = getIntent();
        sportId = intent.getStringExtra("sport_id");

        if (intent.hasExtra("ACTIVITY")) {
            activity = intent.getStringExtra("ACTIVITY");
        }
        Log.e("sportId", sportId + "");


        recyclerView = (RecyclerView) findViewById(R.id.createGameRecyclerView);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(CreateGameSelectLocation.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(CreateGameSelectLocation.this, LinearLayoutManager.VERTICAL, false));
        noVenues = (TextView) findViewById(R.id.noVenues);

        noVenues.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        try {
            populateRecyclerView(pages);
        } catch (Exception e) {
            e.printStackTrace();
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                try {
                    visibleItemCount = recyclerView.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                    lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (venueListSportsTypeModel.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                pages += 1;
                                //venueListAdapter.loadData(venueListModel.getSuccess(),1);
                                try {
                                    populateRecyclerView(pages);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                locationAdapter.notifyDataSetChanged();

                            }
                        }
                    }
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        Log.e("SIZE", "end called");
                        loading = true;
                        // Do something
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == 4) {
                String jsonOb = data.getStringExtra("JsonObj");
                Log.e("response", jsonOb + "");
                String gName = data.getStringExtra("groundName");
                Log.e("gName", gName);

                Intent intent = new Intent();
                intent.putExtra("JsonObj", jsonOb);
                intent.putExtra("groundName", gName);
                setResult(3, intent);
                finish();

            }
        } else {
            Log.e("data", "data");
        }
    }

    private void populateRecyclerView(int pages) {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(CreateGameSelectLocation.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("sport_id", sportId);

            httpClient.post(Config.VENUE_LIST_SPORTS_TYPE + "?page=" + pages, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    venuResponse = new String(responseBody);
                    Log.v("vList", venuResponse);

                    venueListSportsTypeModel = new VenueListSportsTypeModel();
                    gson = new Gson();
                    venueListSportsTypeModel = gson.fromJson(venuResponse, VenueListSportsTypeModel.class);
                    if (venueListSportsTypeModel.getSuccess().size() != 0) {

                        if (venueListSportsTypeModel.getCurrentPage() > 1) {
                            for (int i = 0; i < venueListSportsTypeModel.getSuccess().size(); i++) {
                                venueListSportsTypeItems.add(venueListSportsTypeModel.getSuccess().get(i));
                            }

                            Log.e("venueList", String.valueOf(venueListSportsTypeModel.getCurrentPage()));
                            locationAdapter.loadData(venueListSportsTypeItems, 2);
                        } else {
                            venueListSportsTypeItems = venueListSportsTypeModel.getSuccess();
                            locationAdapter = new RecyclerViewAdapter(CreateGameSelectLocation.this, venueListSportsTypeModel.getSuccess());
                            recyclerView.setAdapter(locationAdapter);
                        }

                        noVenues.setVisibility(View.GONE);
                    } else {
                        noVenues.setVisibility(View.VISIBLE);
                        noVenues.setText("Venue list is empty");
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.v("failure", s);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RewardViewHolder> {

        private List<VenueListSportsTypeItem> arrayList;
        private Context context;


        public RecyclerViewAdapter(CreateGameSelectLocation context, List<VenueListSportsTypeItem> success) {
            this.context = context;
            this.arrayList = success;
        }


        @Override
        public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.create_game_select_location_items, viewGroup, false);
            RewardViewHolder listHolder = new RewardViewHolder(mainGroup);
            return listHolder;

        }

        public void loadData(List<VenueListSportsTypeItem> arrayList, int i) {
            Log.e("LISTING", "LISTING");
            this.arrayList = arrayList;
            notifyDataSetChanged();
            // else this.arrayList = null;
        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final RewardViewHolder holder, final int position) {
            final VenueListSportsTypeItem model = arrayList.get(position);


            final RewardViewHolder mainHolder = (RewardViewHolder) holder;

            try {
                mainHolder.groundName.setTypeface(PlaythoraUtility.getFont(CreateGameSelectLocation.this, RALEWAY_SEMIBOLD));
                mainHolder.groundLocationName.setTypeface(PlaythoraUtility.getFont(CreateGameSelectLocation.this, RALEWAY_REGULAR));
                mainHolder.kms.setTypeface(PlaythoraUtility.getFont(CreateGameSelectLocation.this, RALEWAY_REGULAR));
                mainHolder.groundName.setText(model.getName());
                mainHolder.groundLocationName.setText(model.getLocation_name());
                mainHolder.kms.setText("");
                // mainHolder.ratingBar.setRating("");
            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getCover_pic()).centerCrop().into(mainHolder.groundImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mainHolder.gameLocationLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int i = 0; i < model.getGrounds().size(); i++) {
                        stringArrayList.add(model.getGrounds().get(i).getName());
                        Log.e("$$$$$$$", String.valueOf(model.getGrounds().get(i)));

                    }
                    Intent toProductDetail = new Intent(CreateGameSelectLocation.this, CreateGameGroundSlots.class);
                    toProductDetail.putExtra("vName", model.getName());
                    toProductDetail.putExtra("vLocation", model.getLocation_name());
                    toProductDetail.putExtra("venueCover", model.getCover_pic());
                    toProductDetail.putExtra("ACTIVITY", activity);
                    toProductDetail.putExtra("SPORTSID", sportId);
                    //   toProductDetail.putExtra("GroundArrayList", stringArrayList);
                    try {
                        JSONObject joj = new JSONObject(venuResponse);
                        JSONArray jarray = joj.getJSONArray("success");
                        JSONObject venueObject = jarray.getJSONObject(position);
                        toProductDetail.putExtra("GroundArrayList", venueObject.getJSONArray("grounds").toString());
                        Log.e("%%%%%%%%%%", venueObject.getJSONArray("grounds").toString() + "images");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject joj = new JSONObject(venuResponse);
                        JSONArray jarray = joj.getJSONArray("success");
                        JSONObject venueObject = jarray.getJSONObject(position);
                        toProductDetail.putExtra("IMAGES", venueObject.getJSONArray("images").toString());
                        Log.e("%%%%%%%%%%", venueObject.getJSONArray("images").toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // toProductDetail.putExtra("GroundArrayList", String.valueOf(model.getGrounds()));
                    startActivityForResult(toProductDetail, 4);
                    if (activity.equalsIgnoreCase("NOTIFIATION")) {
                        finish();
                    }

                }
            });


            mainHolder.itemView.setTag(model);


        }
    }
}
