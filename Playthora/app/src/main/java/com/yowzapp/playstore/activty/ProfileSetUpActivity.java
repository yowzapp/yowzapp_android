package com.yowzapp.playstore.activty;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vaishakha on 27/7/16.
 */
public class ProfileSetUpActivity extends BaseActivity {
    private static final int PICK_FROM_CAM = 1;
    TextInputLayout usernameInputLayout;
    TextView mProfile;
    EditText usernameBox;
    ImageView continueImage, setProfile;
    RelativeLayout userImagerelaytive;
    CircleImageView userImage;
    String selectedImagePath, profile_pic;
    PreferenceManager mPref;
    int id;
    String nameValue, profile_pic_update, interest_update, location_update;
    int serverResponseCode = 0;
    private int PICK_IMAGE_REQUEST = 2;
    private File output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_setup_layout);

        initializeAllComponents();

        try {
            Glide.with(ProfileSetUpActivity.this).load(mPref.getProfilePic()).error(R.drawable.image_upload).centerCrop().into(userImage);
        } catch (Exception e) {
            userImage.setImageResource(R.drawable.image_upload);
        }
        nameValue = mPref.getName();
        usernameBox.setText(nameValue);

        int pos = usernameBox.length();
        Editable editable = usernameBox.getText();
        Selection.setSelection(editable, pos);

        mProfile.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));
        usernameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        usernameBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile_set);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        usernameBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                usernameInputLayout.setError(null);
            }
        });


        continueImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usernameBox.getText().toString().length() == 0) {
                    Intent intent = new Intent(ProfileSetUpActivity.this, InterestsActivity.class);
                    intent.putExtra("name", mPref.getName());
                    startActivity(intent);
                    finish();
                } else {
                    // editProfile();
                    Intent intent = new Intent(ProfileSetUpActivity.this, InterestsActivity.class);
                    intent.putExtra("name", usernameBox.getText().toString());
                    startActivity(intent);
                    finish();
                }
            }
        });

        userImagerelaytive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shouldAskPermission()) {
                    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                    int permsRequestCode = 200;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    UploadImage();
                }
            }
        });

    }


 /*   private void getUser() {
       if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
           PlaythoraUtility.showProgressDialog(ProfileSetUpActivity.this);
           AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
           httpClient.addHeader("accessToken",mPref.getAccessToken());
           httpClient.get(Config.GET_USER, new AsyncHttpResponseHandler() {
               @Override
               public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                   PlaythoraUtility.hideProgressDialog();
                   try {
                       String s = new String(responseBody);
                       Log.e("userDetail",s);

                       JSONObject object = new JSONObject(s);
                       if(object.getString("status").equalsIgnoreCase("success")){
                           id = object.getJSONObject("success").getInt("id");
                           nameValue = object.getJSONObject("success").getString("name");
                           profile_pic = object.getJSONObject("success").getString("profile_pic");

                           Log.e("userImage",profile_pic);

                           if(!profile_pic.isEmpty()){
                               Glide.with(ProfileSetUpActivity.this).load(profile_pic).centerCrop().into(userImage);
                           }else {
                               Glide.with(ProfileSetUpActivity.this).load(R.drawable.image_upload).centerCrop().into(userImage);

                           }
                           mPref.setProfilePic(profile_pic);

                           Log.e("userName",nameValue);
                           mPref.setName(nameValue);
                           usernameBox.setText(nameValue);

                          int pos = usernameBox.length();
                           Editable editable = usernameBox.getText();
                           Selection.setSelection(editable,pos);

                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                       Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                   }

               }

               @Override
               public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                   PlaythoraUtility.hideProgressDialog();

                   try {
                       String s = new String(responseBody);
                       Log.e("failure",s);
                       JSONObject object = new JSONObject(s);

                   }catch (Exception e){
                       e.printStackTrace();
                       Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                   }

               }
           });


        }
    }*/

    private void editProfile() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(ProfileSetUpActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("name", usernameBox.getText().toString());

            httpClient.post(Config.EDIT_PROFILE, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("editProfile", s);

                        JSONObject object = new JSONObject(s);
                        if (object.getString("status").equalsIgnoreCase("success")) {
                            nameValue = object.getJSONObject("success").getString("name");

                            Log.e("editName", nameValue);
                            mPref.setName(nameValue);

                            Toast.makeText(ProfileSetUpActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ProfileSetUpActivity.this, InterestsActivity.class);
                            startActivity(intent);
                            finish();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                        JSONObject object = new JSONObject(s);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        setUpUI(findViewById(R.id.profile_setup));
        usernameBox = (EditText) findViewById(R.id.tv_username);
        usernameInputLayout = (TextInputLayout) findViewById(R.id.input_layout_username);
        continueImage = (ImageView) findViewById(R.id.profile_setup_circle_image);
        userImage = (CircleImageView) findViewById(R.id.profile_image);
        userImagerelaytive = (RelativeLayout) findViewById(R.id.relaytive_image);
        setProfile = (ImageView) findViewById(R.id.text_set_profile);
        mProfile = (TextView) findViewById(R.id.tv_profile);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeAccepted) {
                    UploadImage();
                }
                break;

        }

    }

    private boolean shouldAskPermission() {

        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    public void UploadImage() {


        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileSetUpActivity.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileSetUpActivity.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(ProfileSetUpActivity.this,
                                    getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                try {
                    selectedImagePath = getPath(selectedImageUri);
                } catch (NullPointerException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    // userImage.setImageBitmap(bm);
                    setProfile.setVisibility(View.INVISIBLE);
                    PlaythoraUtility.showProgressDialog(ProfileSetUpActivity.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            UploadFile(selectedImagePath);
                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(), "Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), "Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                final Uri mImageCaptureUri = Uri.fromFile(destination);

                System.out.println("Image Path : " + mImageCaptureUri);
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedImagePath = getRealPathFromURI(mImageCaptureUri.toString(), ProfileSetUpActivity.this);
                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //  userImage.setImageBitmap(bm);
                    setProfile.setVisibility(View.INVISIBLE);
                    PlaythoraUtility.showProgressDialog(ProfileSetUpActivity.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            UploadFile(selectedImagePath);
                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private void UploadFile(final String selectedImagePath) {
        String fileName = selectedImagePath;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(compressImage(selectedImagePath, ProfileSetUpActivity.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :" + selectedImagePath);

            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "Source File not exist :" + selectedImagePath, Toast.LENGTH_SHORT).show();
                }
            });

            return;

        } else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.EDIT_PROFILE);
                trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"profile_pic\";filename=\""
                        + fileName + "\"" + lineEnd); //profile_pic is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS", "Content-Disposition: form-data; name=\"profile_pic\";filename=\"" + fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                JSONObject object = new JSONObject(total.toString());
                                profile_pic = object.getJSONObject("success").getString("profile_pic");
                                Log.e("profilePic", profile_pic);
                                mPref.setProfilePic(profile_pic);
                                Glide.with(ProfileSetUpActivity.this).load(mPref.getProfilePic()).error(R.drawable.circled_user).centerCrop().into(userImage);
                                Toast.makeText(ProfileSetUpActivity.this, "Upload image Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            Glide.with(ProfileSetUpActivity.this).load(mPref.getProfilePic()).error(R.drawable.circled_user).centerCrop().into(userImage);

                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (ProtocolException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (IOException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        }
    }

    private String compressImage(String selectedImagePath, Context context) {

        String filePath = getRealPathFromURI(selectedImagePath, context);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
        options.inDither = false;
//      this options allow yowzapp to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Log.d("EXIF", "ratioX: " + ratioX);
        Log.d("EXIF", "ratioY: " + ratioY);
        Log.d("EXIF", "middleX: " + middleX);
        Log.d("EXIF", "middleY: " + middleY);
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.e("MMMMM", "re " + filename);
        return filename;
    }


    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        Log.e("MMMMM", uriSting);
        return uriSting;

    }


    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        Log.e("SIZE", "" + inSampleSize);
        return inSampleSize;
    }

    private void trustAllHosts() {

        // Create a trust manager that does not validate certificate chains

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                return new java.security.cert.X509Certificate[]{};

            }

            public void checkClientTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

            public void checkServerTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

        }};

        // Install the all-trusting trust manager

        try {

            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection

                    .setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void setUpUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(ProfileSetUpActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                setUpUI(innerView);
            }
        }
    }

    private boolean checkForAllValidation() {
        if (usernameBox.getText().toString().isEmpty()) {
            usernameInputLayout.setError("Please provide username");
            return false;
        } else {
            return true;
        }
    }
}
