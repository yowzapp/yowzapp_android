package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGame;
import com.yowzapp.playstore.activty.GameMain;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.model.AllGameList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by vaishakha on 22/11/16.
 */
public class TeamGame extends Fragment {
    View contentView;
    RecyclerView recyclerView;
    TeamGameAdapter adapter;
    String response;
    Gson gson;
    ArrayList<AllGameList> homeGamesModel;
    int teamId;
    PreferenceManager mPref;
    ProgressBar progressBar;
    boolean isMember;
    FloatingActionButton addImage;
    TextView emptyText;
    String getResponse;
    String model;

    public TeamGame(int id, boolean isMember, String memberModels) {
        teamId = id;
        this.isMember = isMember;
        model = memberModels;
        Log.e("model", model);
        Log.e("idteam", teamId+"");
    }

    public TeamGame(){

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.team_game_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());

        initializeAllComponent();

        if (isMember) {
            addImage.setVisibility(View.VISIBLE);
        } else {
            addImage.setVisibility(View.GONE);
        }

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(getContext(),"hi",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity(), CreateGame.class);
                i.putExtra("memList", model);
                i.putExtra("teamGameFragment", "members");
                i.putExtra("teamId", teamId + "");
                Log.e("idteam!!!!", teamId+"");
                startActivity(i);
            }
        });

        TeamGames();

        return contentView;
    }

    private void initializeAllComponent() {
        emptyText = (TextView) contentView.findViewById(R.id.emptyText);
        addImage = (FloatingActionButton) contentView.findViewById(R.id.fab_add_game);
        progressBar = (ProgressBar) contentView.findViewById(R.id.progress_bar_team);
        recyclerView = (RecyclerView) contentView.findViewById(R.id.timeline_recycler);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
    }

    private void TeamGames() {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("team_id", String.valueOf(teamId));
                Log.e("PARAMMS", String.valueOf(params));
                mHttpClient.post(Config.TEAM_GAME_DETAIL, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                getResponse = new String(responseBody);
                                Log.e("RESPONSETEAMGAME", getResponse);

                                try {
                                    JSONObject object = new JSONObject(getResponse);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        JSONArray games = object.getJSONObject("success").getJSONArray("games");
                                        gson = new Gson();
                                        homeGamesModel = new ArrayList<>();
                                        for (int i = 0; i < games.length(); i++) {
                                            homeGamesModel.add(gson.fromJson(games.get(i).toString(), AllGameList.class));
                                        }

                                        adapter = new TeamGameAdapter(getActivity(), homeGamesModel);
                                        recyclerView.setAdapter(adapter);

                                        if (homeGamesModel.size() == 0) {
                                            emptyText.setVisibility(View.VISIBLE);
                                            if (isMember)
                                                emptyText.setText("Create a game just for this team by clicking on the plus button below");
                                            else emptyText.setText("No games");
                                        } else emptyText.setVisibility(View.GONE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL_GALLERY", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL_GALLERY", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    public class TeamGameAdapter extends RecyclerView.Adapter<TeamGameAdapter.GameList> {
        Context context;
        ArrayList<AllGameList> model;

        public TeamGameAdapter(FragmentActivity activity, ArrayList<AllGameList> homeGamesModel) {
            context = activity;
            model = homeGamesModel;
        }

        @Override
        public GameList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_game_item, null);
            return new GameList(layoutView);
        }

        @Override
        public void onBindViewHolder(GameList holder, final int position) {
            holder.mButton.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            holder.mGameName.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            holder.mLocation.setTypeface(PlaythoraUtility.getFont(context, Config.RALEWAY_REGULAR));
            holder.mGameName.setText(model.get(position).getName());
            holder.mLocation.setText(model.get(position).getLocation());
            holder.mDate.setText(model.get(position).getMatch_status());
            holder.mPlayers.setText(model.get(position).getNo_members() + "");
            holder.mTotal.setText("/" + model.get(position).getMax_members());

            if (model.get(position).is_completed()) {
                holder.mButton.setText("Completed");
            } else holder.mButton.setText("Upcoming");
            try {
                Glide.with(context).load(model.get(position).getCover_pic()).error(R.drawable.game_null).into(holder.mImage);
            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.game_null);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, GameMain.class);
                    i.putExtra("GAME_ID", String.valueOf(model.get(position).getId()));
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class GameList extends RecyclerView.ViewHolder {
            Button mButton;
            TextView mGameName, mLocation, mDate, mPlayers, mTotal;
            ImageView mImage;

            public GameList(View itemView) {
                super(itemView);
                mButton = (Button) itemView.findViewById(R.id.button_one);
                mGameName = (TextView) itemView.findViewById(R.id.team_game_title);
                mLocation = (TextView) itemView.findViewById(R.id.team_game_location);
                mDate = (TextView) itemView.findViewById(R.id.game_date);
                mPlayers = (TextView) itemView.findViewById(R.id.game_players);
                mImage = (ImageView) itemView.findViewById(R.id.team_game_image);
                mTotal = (TextView) itemView.findViewById(R.id.game_players_total);
            }
        }
    }

}
