package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.MainSearchAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.SearchAllModel;
import com.yowzapp.playstore.model.SearchAllModelList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

public class MainSearchActivity extends BaseActivity {

    Gson mGson;
    ProgressDialog dialog;
    EditText search;
    TextView recent;
    View contentView;
    ImageView cancel, back;
    Gson gson;
    PreferenceManager mPref;
    SearchAllModel model;
    MainSearchAdapter searchAdapter;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<SearchAllModelList> tempList;
    Editable sText;
    ProgressBar progressBar, paginateProgressBar;
    TextView noResult;
    String Response;
    JSONArray jsonArray = new JSONArray();
    private RecyclerView recyclerViewsearch;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        mPref = PreferenceManager.instance(this);
        initialize();


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("TEXT", s + "," + count + "," + start + "," + after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("TEXT1", s + "," + count + "," + start + "," + before);
            }

            @Override
            public void afterTextChanged(Editable s) {
                s.toString().replace("%20", " ");
                s.toString().replace("%", "");
                s.toString().replace(" ", "%20");
                if (s.toString().length() > 2) {
                    populateSearch(s, 1, false);
                    sText = s;
                    recent.setText("Search results");
                    recyclerViewsearch.setVisibility(View.VISIBLE);
                } else if (s.toString().length() == 0) {
                    recyclerViewsearch.setAdapter(null);
                    cancel.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    noResult.setVisibility(View.GONE);
                }
            }
        });

        recyclerViewsearch.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("ONSCROLL", "onScrollStateChanged");
                Log.e("ONSCROLL", "onScrollStateChanged");
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                try {
                    if (model.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                pages += 1;
                                //venueListAdapter.loadData(venueListModel.getSuccess(),1);
                                try {
                                    populateSearch(sText, pages, true);
                                    paginateProgressBar.setVisibility(View.VISIBLE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    paginateProgressBar.setVisibility(View.GONE);
                                }
                                searchAdapter.notifyDataSetChanged();

                            }
                        }
                    }
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        Log.e("SIZE", "end called");
                        loading = true;
                        // Do something
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("ONSCROLL", "onScrolled");
            }
        });


    }


    private void initialize() {
        recent = (TextView) findViewById(R.id.recent_text);
        recent.setVisibility(View.GONE);
        search = (EditText) findViewById(R.id.edit_search);
        cancel = (ImageView) findViewById(R.id.cancel_icon);
        back = (ImageView) findViewById(R.id.back_icon);
        progressBar = (ProgressBar) findViewById(R.id.search_progress);
        paginateProgressBar = (ProgressBar) findViewById(R.id.paginate_bar);
        noResult = (TextView) findViewById(R.id.empty_search);

        //  availableForYourPOints.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noResult.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
//        whatHot.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));


        recyclerViewsearch = (RecyclerView) findViewById(R.id.search_recycler);
        recyclerViewsearch.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewsearch.setLayoutManager(mLayoutManager);


    }

    private void populateSearch(Editable s, int pages, boolean isPaginate) {

        if (PlaythoraUtility.checkInternetConnection(MainSearchActivity.this)) {
            try {
                if (!isPaginate)
                    progressBar.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.GONE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SEARCH_ALL + "?q=" + s + "&page=" + pages,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                cancel.setVisibility(View.VISIBLE);
                                paginateProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("SEARCH_RESPONSE", s);
                                    Response = s;
                                    model = new SearchAllModel();
                                    mGson = new Gson();
                                    model = mGson.fromJson(s, SearchAllModel.class);
                                    Log.e("RESPONSE", String.valueOf(model.getSuccess().size()));

                                    if (model.getCurrentPage() > 1) {
                                        noResult.setVisibility(View.GONE);
                                        recyclerViewsearch.setVisibility(View.VISIBLE);
                                        for (int i = 0; i < model.getSuccess().size(); i++) {
                                            tempList.add(model.getSuccess().get(i));
                                        }
                                        JSONObject jsonObject = new JSONObject(Response);
                                        JSONArray tempJsonArray = new JSONArray();
                                        tempJsonArray = jsonObject.getJSONArray("success");
                                        for (int j = 0; j < tempJsonArray.length(); j++) {
                                            jsonArray.put(tempJsonArray.get(j));
                                        }
                                        Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                        searchAdapter.RefreshPagination(tempList, true, jsonArray);
                                    } else if (model.getSuccess().size() != 0) {
                                        noResult.setVisibility(View.GONE);
                                        recyclerViewsearch.setVisibility(View.VISIBLE);
                                        tempList = model.getSuccess();
                                        JSONObject jsonObject = new JSONObject(Response);
                                        jsonArray = jsonObject.getJSONArray("success");
                                        searchAdapter = new MainSearchAdapter(MainSearchActivity.this, model.getSuccess(), jsonArray);
                                        recyclerViewsearch.setAdapter(searchAdapter);
                                    } else {
                                        noResult.setVisibility(View.VISIBLE);
                                        //recyclerViewsearch.setVisibility(View.GONE);
                                        recyclerViewsearch.setAdapter(null);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressBar.setVisibility(View.GONE);
                                    cancel.setVisibility(View.VISIBLE);
                                    noResult.setVisibility(View.VISIBLE);
                                    //recyclerViewsearch.setVisibility(View.GONE);
                                    recyclerViewsearch.setAdapter(null);
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                cancel.setVisibility(View.VISIBLE);
                                noResult.setVisibility(View.VISIBLE);
                                recyclerViewsearch.setAdapter(null);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(MainSearchActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(MainSearchActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    }//else Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(MainSearchActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
                paginateProgressBar.setVisibility(View.GONE);
                cancel.setVisibility(View.VISIBLE);
                noResult.setVisibility(View.VISIBLE);
                recyclerViewsearch.setAdapter(null);
            }
        } else {
//            Toast.makeText(MainSearchActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
            paginateProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PlaythoraUtility.hideSoftKeyboard(this);
    }
}




