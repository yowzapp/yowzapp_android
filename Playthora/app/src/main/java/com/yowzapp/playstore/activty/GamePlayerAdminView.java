package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.GameDetailMembers;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 25/11/16.
 */

public class GamePlayerAdminView extends BaseActivity {

    public List<GameDetailMembers> gameDetailMemberses;
    public MembersAdapter adapter;
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    TextView invite;
    Toolbar toolbar;
    String members, admin, isMember, groundId;
    PreferenceManager mPref;
    String memberID;
    EditText search;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_player_admin_recycler);
        mPref = PreferenceManager.instance(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        search = (EditText) findViewById(R.id.edit_search_game);
        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        search.setText("");

        Intent intent = getIntent();
        //   members = intent.getStringExtra("membersList");
        admin = intent.getStringExtra("admin");
        isMember = intent.getStringExtra("otherMembers");
        groundId = intent.getStringExtra("groundId");

        Log.e("membersList", members + "");
        Log.e("admin", admin + "");
        Log.e("isMember", isMember + "");
        Log.e("groundId", groundId + "");

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (admin.equalsIgnoreCase("true")) {
            toolbarTitle.setText("Players (Admin view)");
        } else {
            toolbarTitle.setText("Players");
        }

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(GamePlayerAdminView.this, RALEWAY_REGULAR));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        recyclerView = (RecyclerView) findViewById(R.id.game_admin_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(GamePlayerAdminView.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);


        try {
            followingRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() >= 0) {
                    try {
                        adapter.getFilter().filter(s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("STRING", "EDIT" + s);
                }
            }
        });

    }

    private void followingRecycler() throws Exception {


      /*  JSONArray teamMembersArray = new JSONArray(teamMembersArrayList);
        mGson = new Gson();
        teamMembersArrayList = new ArrayList<GameDetailMembers>();

        for (int i = 0; i < teamMembersArray.length(); i++) {
            teamMembersArrayList.add(mGson.fromJson(teamMembersArray.get(i).toString(), GameDetailMembers.class));
        }*/
        if (!GameMain.gameDetailMemberses.isEmpty()) {
            if (recyclerView.getAdapter() == null) {
                adapter = new MembersAdapter(GamePlayerAdminView.this, GameMain.gameDetailMemberses);
                recyclerView.setAdapter(adapter);// set adapter on recyclerview
                adapter.notifyDataSetChanged();
            } else {
               /*  adapter.refresh(GameMain.gameDetailMemberses);
                adapter.notifyDataSetChanged();*/
            }
        } else {

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_all_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        if (id == R.id.action_openSearch) {
            search.setVisibility(View.VISIBLE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        search.setText("");
    }

    public class MembersAdapter extends RecyclerView.Adapter<AddMembersFollowingHolder> implements Filterable {

        private List<GameDetailMembers> arrayList;
        private Context context;
        private UserFilter userFilter;
        private List<GameDetailMembers> filteredUserList;

        public MembersAdapter(GamePlayerAdminView context, List<GameDetailMembers> gameDetailMemberses) {
            this.context = context;
            this.arrayList = gameDetailMemberses;
            this.filteredUserList = gameDetailMemberses;
        }

        @Override
        public AddMembersFollowingHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.game_player_admin_view_item, viewGroup, false);
            AddMembersFollowingHolder listHolder = new AddMembersFollowingHolder(mainGroup);
            return listHolder;

        }

        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        public void refresh(List<GameDetailMembers> gameDetailMemberses) {
            this.arrayList = gameDetailMemberses;
            notifyDataSetChanged();

        }

        @Override
        public void onBindViewHolder(final AddMembersFollowingHolder holder, final int position) {
            final GameDetailMembers model = arrayList.get(position);


            final AddMembersFollowingHolder mainHolder = (AddMembersFollowingHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(GamePlayerAdminView.this, RALEWAY_REGULAR));
                mainHolder.admin.setTypeface(PlaythoraUtility.getFont(GamePlayerAdminView.this, RALEWAY_REGULAR));
                //mainHolder.name.setText(arrayList.get(position).getName());
                mainHolder.name.setText(arrayList.get(position).getName().substring(0, 1).toUpperCase() + arrayList.get(position).getName().substring(1));
                memberID = String.valueOf(model.getId());

                if (arrayList.get(position).getPivot().getIs_admin() == 1) {
                    holder.admin.setVisibility(View.VISIBLE);
                    holder.admin.setText("Admin");
                }

            } catch (NullPointerException e) {

            }


/*
            mainHolder.playerLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mPref.getAccessToken().isEmpty()) {
                        PlaythoraUtility.LoginDialog(GamePlayerAdminView.this);
                    } else {
                        if (toast != null) {
                            toast.cancel();
                        }
                        if (admin.equalsIgnoreCase("true")) {
                            showPopUp(arrayList.get(position).getName(), position);
                        } else {
                            toast = Toast.makeText(GamePlayerAdminView.this, "Admin only can access", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                    return true;
                }
            });*/

            mainHolder.playerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPref.getAccessToken().isEmpty()) {
                        PlaythoraUtility.LoginDialog(GamePlayerAdminView.this);
                    } else {
                        if (toast != null) {
                            toast.cancel();
                        }
                        if (admin.equalsIgnoreCase("true")) {
                            showPopUp(arrayList.get(position).getName(), position);
                        } else {
                            toast = Toast.makeText(GamePlayerAdminView.this, "Admin only can access", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }

                }
            });

          /*  mainHolder.playerLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(admin.equalsIgnoreCase("true")) {
                        showPopUp(arrayList.get(position).getName(),position);
                    }else {
                        Toast.makeText(GamePlayerAdminView.this,"Admin only can access",Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });*/


            try {
                Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.userImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, PlayerProfileViewActivity.class);
                    i.putExtra("Id", model.getId() + "");
                    startActivity(i);
                }
            });

            mainHolder.itemView.setTag(model);

        }

        @Override
        public Filter getFilter() {
            if (userFilter == null)
                userFilter = new UserFilter(this, arrayList);
            return userFilter;
        }

        private void showPopUp(String name, final int position) {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(GamePlayerAdminView.this);
            View mView = layoutInflaterAndroid.inflate(R.layout.game_make_admin_or_remove, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(GamePlayerAdminView.this);
            alertDialogBuilderUserInput.setView(mView);
            final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

            final TextView userName = (TextView) mView.findViewById(R.id.userName);
            final TextView makeAdmin = (TextView) mView.findViewById(R.id.makeAdmin);
            final TextView removeAdmin = (TextView) mView.findViewById(R.id.remove);

            userName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
            makeAdmin.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
            removeAdmin.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

            userName.setText(name);

            if (arrayList.get(position).getPivot().getIs_admin() == 1) {
                makeAdmin.setVisibility(View.GONE);
            }


            makeAdmin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adminPopulate(position);
                    alertDialogAndroid.dismiss();
                }
            });

            removeAdmin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePopulate(position);
                    alertDialogAndroid.dismiss();
                }
            });


            alertDialogAndroid.show();
        }

        private void removePopulate(final int position) {

            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(GamePlayerAdminView.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("user_id", String.valueOf(arrayList.get(position).getId()));
                params.add("game_id", groundId);
                httpClient.post(Config.REMOVE_PLAYER_GAME, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();

                        String s = new String(responseBody);
                        Log.e("removeAdmin", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 200) {
                                Toast.makeText(GamePlayerAdminView.this, "Player removed from the game", Toast.LENGTH_SHORT).show();

                                if (GameMain.gameDetailMemberses.get(position).getId() == arrayList.get(position).getId()) {
                                    GameMain.gameDetailMemberses.remove(position);
                                }
                                //arrayList.remove(position);
                                notifyDataSetChanged();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                                Toast.makeText(GamePlayerAdminView.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        }

        private void adminPopulate(final int position) {
            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(GamePlayerAdminView.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("user_id", String.valueOf(arrayList.get(position).getId()));
                params.add("game_id", groundId);
                httpClient.post(Config.MAKE_ADMIN_GAME, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();

                        String s = new String(responseBody);
                        Log.e("makeAdmin", s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 200) {
                                Toast.makeText(GamePlayerAdminView.this, "Player changed to admin", Toast.LENGTH_SHORT).show();

                                if (GameMain.gameDetailMemberses.get(position).getId() == arrayList.get(position).getId()) {
                                    GameMain.gameDetailMemberses.get(position).getPivot().setIs_admin(1);
                                }

                                arrayList.get(position).getPivot().setIs_admin(1);
                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    }
                });
            }
        }

        public class UserFilter extends Filter {
            private MembersAdapter adapter;

            private List<GameDetailMembers> originalList;

            private List<GameDetailMembers> filteredList;

            public UserFilter(MembersAdapter adapter, List<GameDetailMembers> originalList) {
                super();
                this.adapter = adapter;
                this.originalList = new ArrayList<>(originalList);
                this.filteredList = new ArrayList<>();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filteredList.clear();
                final FilterResults results = new FilterResults();
                Log.e("STRING", constraint + "");
                Log.e("LIST", constraint.length() + "");

                if (constraint.length() == 0) {
                    filteredList.addAll(originalList);
                    Log.e("STRING", "0");
                } else {
                    Log.e("STRING", constraint.length() + "");
                    final String filterPattern = constraint.toString().toLowerCase().trim();
                    for (final GameDetailMembers user : originalList) {
                        if (user.getName().toLowerCase().contains(filterPattern)) {
                            filteredList.add(user);
                        }
                    }
                }
                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                adapter.filteredUserList.clear();
                adapter.filteredUserList.addAll((List<GameDetailMembers>) results.values);
                adapter.notifyDataSetChanged();
            }
        }

    }

    public class AddMembersFollowingHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView name, admin;
        RelativeLayout playerLayout;

        public AddMembersFollowingHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.playerImageView);
            this.name = (TextView) view.findViewById(R.id.userName);
            this.admin = (TextView) view.findViewById(R.id.adminOrNot);
            this.playerLayout = (RelativeLayout) view.findViewById(R.id.makeAdminLayout_one);

        }
    }
}

