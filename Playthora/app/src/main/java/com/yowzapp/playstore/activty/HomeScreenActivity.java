package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.application.ChatApplication;
import com.yowzapp.playstore.fragment.ChatAllFragment;
import com.yowzapp.playstore.fragment.HomeFragment;
import com.yowzapp.playstore.fragment.MyProfileFragment;
import com.yowzapp.playstore.fragment.SearchRecentFragment;
import com.yowzapp.playstore.fragment.VenueMapFragment;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.CustomViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.yowzapp.playstore.fragment.VenueMapFragment.REQUEST_CHECK_SETTINGS;
import static com.yowzapp.playstore.fragment.VenueMapFragment.isLocationEnabled;
import static com.yowzapp.playstore.fragment.VenueMapFragment.shouldLoadMap;
import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;
import static com.yowzapp.playstore.utils.Config.SHARE_LINK;

public class HomeScreenActivity extends BaseActivity implements View.OnClickListener {

    PreferenceManager mPref;
    TextView notificationCount;
    String levelUp, rate;
    ImageView rateOne, rateTwo, rateThree, rateFour, rateFive;
    int rating = 0;
    boolean one = false, two = false, three = false, four = false, five = false;
    String fragmentName = "FRAGMENT";
    Fragment fragment;
    ChatApplication app;
    private CustomViewPager customViewPager;
    private ImageView mChat, mLocation, mHome, mSearch, mProfile, mStore, notification;
    private MyPagerAdapter adapter;
    private TextView mToolbarLoc;
    private RelativeLayout relativeLayout, chatRelativeLayout, locationRelativeLayout, profileRelativeLayout;
    private View toolbarView;
    private Boolean exit = false;
    private int value = 0;
    private Socket mSocket;
    public static boolean notifyFlag = false;
    private Emitter.Listener userConnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!mPref.getAccessToken().isEmpty()) {
                Log.e("SOSCKEKE", args[0].toString());
            }
        }
    };
    private Emitter.Listener connected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!mPref.getAccessToken().isEmpty()) {
                Log.e("SOSCKEKEiii", args[0].toString());
                mSocket.emit("connect user", mPref.getAccessToken());
            }
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("EmitterConnectionError", args[0].toString());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen_layout);
        PlaythoraUtility.showProgressDialog(this);
        mPref = PreferenceManager.instance(getApplicationContext());
        app = (ChatApplication) getApplication();

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.yowzapp.playstore",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        initializeAllComponents();
        setupListeners();

        if (getIntent().hasExtra("ACTIVITY")) {
            value = getIntent().getIntExtra("ACTIVITY", 0);
        }

        if (getIntent().hasExtra("FRAGMENT")) {
            fragmentName = getIntent().getStringExtra("FRAGMENT");
            Log.e("fragmentName", fragmentName);
        }


        if (fragmentName.equalsIgnoreCase("profile")) {
            fragmentName = "notProfile";
            mChat.setImageResource(R.drawable.ic_chat_bottom);
            mLocation.setImageResource(R.drawable.ic_loc_bottom);
            mHome.setImageResource(R.drawable.ic_home_botm_dark);
            mSearch.setImageResource(R.drawable.ic_search_bottom);
            mProfile.setImageResource(R.drawable.profile);
            toolbarView.setVisibility(View.VISIBLE);
            chatRelativeLayout.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.GONE);
            locationRelativeLayout.setVisibility(View.GONE);
            profileRelativeLayout.setVisibility(View.VISIBLE);
            customViewPager.setCurrentItem(4);
        } else {
            fragmentName = "notProfile";
            mChat.setImageResource(R.drawable.ic_chat_bottom);
            mLocation.setImageResource(R.drawable.ic_loc_bottom);
            mHome.setImageResource(R.drawable.hot);
            mSearch.setImageResource(R.drawable.ic_search_bottom);
            mProfile.setImageResource(R.drawable.ic_profile_bottom);
            toolbarView.setVisibility(View.VISIBLE);
            chatRelativeLayout.setVisibility(View.GONE);
            locationRelativeLayout.setVisibility(View.GONE);
            profileRelativeLayout.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
            customViewPager.setCurrentItem(2);
        }


        if (mPref.getLocationName().isEmpty()) {
            mToolbarLoc.setText(R.string.update_location);
        } else {
            mToolbarLoc.setText(mPref.getLocationName());
        }

        if (!mPref.getAccessToken().isEmpty()) {
            mSocket = app.getSocket();
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            if (!mSocket.connected())
                mSocket.connect();
            mSocket.on("connected", connected);
            mSocket.on("user connected", userConnected);


            JSONArray jo = new JSONArray();

            jo.put(mPref.getAccessToken());
            jo.put(mPref.getId());

            Log.e("JSONOBJ", jo.toString());


            if (mSocket.connected()) {
                Log.e("SOCKETIO", "TRUE");

            } else {
                Log.e("SOCKETIO", "FALSE");
            }

        }


//        if (!mPref.getAccessToken().isEmpty())
//            getUserDetail();
    }


    private void getUserDetail() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            httpClient.get(Config.GET_USER, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String s = new String(responseBody);
                        Log.e("userDetail", s);
                        Log.e("RANK", "1");

                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {
                            int notification = object.getJSONObject("success").getInt("notification_count");
                            levelUp = object.getJSONObject("success").getBoolean("level_up") + "";
                            rate = object.getJSONObject("success").getBoolean("rating") + "";
                            Log.e("level_up", levelUp);
                            Log.e("rating", rate);
                            Log.e("is_social", object.getJSONObject("success").getBoolean("is_social") + "");
                            mPref.setIsSocial(object.getJSONObject("success").getBoolean("is_social") + "");

                            if (levelUp.equalsIgnoreCase("true")) {
                                if (object.getJSONObject("success").getJSONObject("level_details").length() != 0) {
                                    String cont = object.getJSONObject("success").getJSONObject("level_details").getString("content");
                                    String badgeIcon = object.getJSONObject("success").getJSONObject("level_details").getString("badge_icon");
                                    // String badgeCover=object.getJSONObject("success").getJSONObject("level_details").getString("badge_cover");

                                    final Dialog dialog = new Dialog(HomeScreenActivity.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.achievement_unlocked_layout);

                                    final Window window = dialog.getWindow();
                                    if (window != null) {
                                        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                                        window.setGravity(Gravity.CENTER);
                                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        // window.setBackgroundDrawableResource(android.R.color.transparent);
                                    }

                                    TextView content = (TextView) dialog.findViewById(R.id.achievement_description);
                                    CircleImageView badge_icon = (CircleImageView) dialog.findViewById(R.id.achievement_logo);
                                    ImageView badge_cover = (ImageView) dialog.findViewById(R.id.badge_cover);
                                    TextView title = (TextView) dialog.findViewById(R.id.achievement_text);
                                    TextView status = (TextView) dialog.findViewById(R.id.achievement_status);
                                    Button viewLeaderBoard = (Button) dialog.findViewById(R.id.viewLeaderBoard);
                                    TextView letTheWorld = (TextView) dialog.findViewById(R.id.letTheWorldKnow);
                                    TextView tapToShare = (TextView) dialog.findViewById(R.id.tapToShare);
                                    ImageView whatsApp = (ImageView) dialog.findViewById(R.id.whatsaap);
                                    ImageView twitter = (ImageView) dialog.findViewById(R.id.twitter);
                                    ImageView facebook = (ImageView) dialog.findViewById(R.id.facebook);

                                    content.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                                    title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
                                    status.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                                    viewLeaderBoard.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                                    letTheWorld.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
                                    tapToShare.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

                                    content.setText(cont);
                                    Glide.with(getApplicationContext()).load(badgeIcon).centerCrop().into(badge_icon);
                                    // Glide.with(getApplicationContext()).load(badgeCover).centerCrop().into(badge_cover);
                                    viewLeaderBoard.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(HomeScreenActivity.this, com.yowzapp.playstore.activty.MyProfileLeaderBoardMain.class);
                                            startActivity(intent);
                                            dialog.dismiss();
                                        }
                                    });

                                    whatsApp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            PackageManager pm = getPackageManager();
                                            try {

                                                Intent waIntent = new Intent(Intent.ACTION_SEND);
                                                waIntent.setType("text/plain");
                                                String text = "Hey check out \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: " + SHARE_LINK;

                                                PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);

                                                //Check if package exists or not. If not then code
                                                //in catch block will be called
                                                waIntent.setPackage("com.whatsapp");

                                                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                                                startActivity(Intent.createChooser(waIntent, "Share with"));

                                            } catch (PackageManager.NameNotFoundException e) {
                                                Toast.makeText(HomeScreenActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                                                        .show();
                                            }
                                        }
                                    });

                                    twitter.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            try {
                                                // Check if the Twitter app is installed on the phone.
                                                getApplicationContext().getPackageManager().getPackageInfo("com.twitter.yowzapp", 0);
                                                Intent intent = new Intent(Intent.ACTION_SEND);
                                                intent.setClassName("com.twitter.yowzapp", "com.twitter.yowzapp.composer.ComposerActivity");
                                                intent.setType("text/plain");
                                                intent.putExtra(Intent.EXTRA_TEXT, "Hey check out \"Yowzapp\". Using this we can play along and make our own squad. Download and Join me: " + SHARE_LINK);
                                                startActivity(intent);

                                            } catch (Exception e) {
                                                Toast.makeText(getApplicationContext(), "Twitter is not installed on this device", Toast.LENGTH_LONG).show();

                                            }

                                        }
                                    });

                                    String fileName = "image-3116.jpg";
                                    String externalStorageDirectory = Environment.getExternalStorageDirectory().toString();
                                    String myDir = externalStorageDirectory + "/saved_images/"; // the

                                    // file will be in saved_images
                                    final Uri uri = Uri.parse("file:///" + myDir + fileName);

                                    facebook.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                                            shareIntent.setType("text/plain");
                                            shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, (String) v.getTag(R.string.app_name));
                                            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, (String) v.getTag(R.drawable.ic_launcher));
                                            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

                                            PackageManager pm = v.getContext().getPackageManager();
                                            List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                                            for (final ResolveInfo app : activityList) {
                                                if ((app.activityInfo.name).startsWith("com.facebook.katana")) {
                                                    final ActivityInfo activity = app.activityInfo;
                                                    final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                                                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                                                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                                    shareIntent.setComponent(name);
                                                    v.getContext().startActivity(shareIntent);
                                                    break;
                                                }
                                            }
                                        }
                                    });


                                    dialog.show();

                                }

                            }


                            if (rate.equalsIgnoreCase("true")) {
                                if (object.getJSONObject("success").getJSONObject("rating_details").length() != 0) {
                                    final String venueId = object.getJSONObject("success").getJSONObject("rating_details").getInt("venue_id") + "";
                                    final String gameId = object.getJSONObject("success").getJSONObject("rating_details").getInt("game_id") + "";
                                    String ratingContent = object.getJSONObject("success").getJSONObject("rating_details").getString("content");
                                    String venueName = object.getJSONObject("success").getJSONObject("rating_details").getString("venue_name");

                                    Log.e("venueId", venueId);
                                    Log.e("gameId", gameId);
                                    Log.e("ratingContent", ratingContent);
                                    Log.e("venueName", venueName);

                                    final Dialog dialog = new Dialog(HomeScreenActivity.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.rate_games_layout);
                                    dialog.setCancelable(true);


                                    final Window window = dialog.getWindow();
                                    if (window != null) {
                                        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                                        window.setGravity(Gravity.CENTER);
                                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    }

                                    TextView location = (TextView) dialog.findViewById(R.id.rateLocation);
                                    TextView venue = (TextView) dialog.findViewById(R.id.rateText);
                                    Button submit = (Button) dialog.findViewById(R.id.submit_rate);
                                    Button cancel = (Button) dialog.findViewById(R.id.cancel_button);
                                    rateOne = (ImageView) dialog.findViewById(R.id.rating_one);
                                    rateTwo = (ImageView) dialog.findViewById(R.id.rating_two);
                                    rateThree = (ImageView) dialog.findViewById(R.id.rating_three);
                                    rateFour = (ImageView) dialog.findViewById(R.id.rating_four);
                                    rateFive = (ImageView) dialog.findViewById(R.id.rating_five);


                                    rateOne.setOnClickListener(HomeScreenActivity.this);
                                    rateTwo.setOnClickListener(HomeScreenActivity.this);
                                    rateThree.setOnClickListener(HomeScreenActivity.this);
                                    rateFour.setOnClickListener(HomeScreenActivity.this);
                                    rateFive.setOnClickListener(HomeScreenActivity.this);


                                    location.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                                    venue.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                                    submit.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

                                    location.setText(ratingContent + ".");
                                    venue.setText(R.string.rate_experience);

                                    submit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                                                PlaythoraUtility.showProgressDialog(HomeScreenActivity.this);
                                                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                                                httpClient.addHeader("accessToken", mPref.getAccessToken());
                                                RequestParams params = new RequestParams();
                                                params.add("venue_id", venueId);
                                                params.add("game_id", gameId);
                                                params.add("rating", rating + "");
                                                httpClient.post(Config.RATING, params, new AsyncHttpResponseHandler() {
                                                    @Override
                                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                        PlaythoraUtility.hideProgressDialog();
                                                        String s1 = new String(responseBody);
                                                        Log.e("rateSuccess", s1);
                                                        try {
                                                            JSONObject jsonObject = new JSONObject(s1);
                                                            if (jsonObject.getInt("statusCode") == 200) {
                                                                Toast.makeText(HomeScreenActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }

                                                    @Override
                                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                        PlaythoraUtility.hideProgressDialog();
                                                        try {
                                                            String s1 = new String(responseBody);
                                                            Log.e("failure", s1);
                                                            JSONObject object = new JSONObject(s1);

                                                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                                                mPref.setAccessToken("");
                                                                Toast.makeText(HomeScreenActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                                                Intent intent = new Intent(HomeScreenActivity.this, LoginActivity.class);
                                                                startActivity(intent);
                                                                finishAffinity();

                                                            } else {
                                                                Toast.makeText(HomeScreenActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            try {
                                                                String s1 = new String(responseBody);
                                                                Log.e("failure", s1);
                                                                JSONObject object = new JSONObject(s1);
                                                                Toast.makeText(HomeScreenActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                                            } catch (Exception e1) {
                                                                e1.printStackTrace();
                                                                Toast.makeText(HomeScreenActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                                            }

                                                        }
                                                    }
                                                });

                                            }
                                        }
                                    });


                                    cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                                                PlaythoraUtility.showProgressDialog(HomeScreenActivity.this);
                                                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                                                httpClient.addHeader("accessToken", mPref.getAccessToken());
                                                RequestParams params = new RequestParams();
                                                params.add("venue_id", venueId);
                                                params.add("game_id", gameId);
                                                params.add("rating", "0");
                                                httpClient.post(Config.RATING, params, new AsyncHttpResponseHandler() {
                                                    @Override
                                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                        PlaythoraUtility.hideProgressDialog();
                                                        String s1 = new String(responseBody);
                                                        Log.e("rateCancel", s1);
                                                        try {
                                                            JSONObject jsonObject = new JSONObject(s1);
                                                            if (jsonObject.getInt("statusCode") == 200) {
                                                                //  Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                        PlaythoraUtility.hideProgressDialog();

                                                        try {
                                                            String s1 = new String(responseBody);
                                                            Log.e("failure", s1);
                                                            JSONObject object = new JSONObject(s1);
                                                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                                                mPref.setAccessToken("");
                                                                Toast.makeText(HomeScreenActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                                                Intent intent = new Intent(HomeScreenActivity.this, com.yowzapp.playstore.activty.LoginActivity.class);
                                                                startActivity(intent);
                                                                finishAffinity();

                                                            } else
                                                                Toast.makeText(HomeScreenActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            try {
                                                                String s1 = new String(responseBody);
                                                                Log.e("failure", s1);
                                                                JSONObject object = new JSONObject(s1);
                                                                Toast.makeText(HomeScreenActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                                            } catch (Exception e1) {
                                                                e1.printStackTrace();
                                                                Toast.makeText(HomeScreenActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                                            }

                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });

                                    dialog.show();

                                }
                            }

                            if (notification > 0) {
                                notificationCount.setVisibility(View.VISIBLE);
                                notificationCount.setText(notification + "");
                            }
                            Log.e("userName", object.getJSONObject("success").getString("name"));
                            Log.e("userImage", object.getJSONObject("success").getString("profile_pic"));


                            mPref.setName(object.getJSONObject("success").getString("name"));
                            mPref.setBio(object.getJSONObject("success").getString("bio"));
                            mPref.setUserMobileNumber(object.getJSONObject("success").getString("mobile"));
                            mPref.setMobilVerified(object.getJSONObject("success").getString("mobile_verified"));
                            mPref.setProfilePic(object.getJSONObject("success").getString("profile_pic"));
                            mPref.setCoverPic(object.getJSONObject("success").getString("cover_pic"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);

                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(HomeScreenActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(HomeScreenActivity.this, com.yowzapp.playstore.activty.LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();

                        } else {
                            Toast.makeText(HomeScreenActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(HomeScreenActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(HomeScreenActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home_chat:
                if (mPref.getAccessToken().isEmpty())
                    PlaythoraUtility.LoginDialog(HomeScreenActivity.this);
                else {
                    mChat.setImageResource(R.drawable.chat);
                    mLocation.setImageResource(R.drawable.ic_loc_bottom);
                    mHome.setImageResource(R.drawable.ic_home_botm_dark);
                    mSearch.setImageResource(R.drawable.ic_search_bottom);
                    mProfile.setImageResource(R.drawable.ic_profile_bottom);
                    toolbarView.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);
                    chatRelativeLayout.setVisibility(View.VISIBLE);
                    locationRelativeLayout.setVisibility(View.GONE);
                    profileRelativeLayout.setVisibility(View.GONE);
                    customViewPager.setCurrentItem(0, true);
                }
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();
                break;

            case R.id.home_location:
                mChat.setImageResource(R.drawable.ic_chat_bottom);
                mLocation.setImageResource(R.drawable.location);
                mHome.setImageResource(R.drawable.ic_home_botm_dark);
                mSearch.setImageResource(R.drawable.ic_search_bottom);
                mProfile.setImageResource(R.drawable.ic_profile_bottom);
                toolbarView.setVisibility(View.VISIBLE);
                chatRelativeLayout.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.GONE);
                locationRelativeLayout.setVisibility(View.VISIBLE);
                profileRelativeLayout.setVisibility(View.GONE);
                customViewPager.setCurrentItem(1, true);
                break;

            case R.id.home_button:
                mChat.setImageResource(R.drawable.ic_chat_bottom);
                mLocation.setImageResource(R.drawable.ic_loc_bottom);
                mHome.setImageResource(R.drawable.hot);
                mSearch.setImageResource(R.drawable.ic_search_bottom);
                mProfile.setImageResource(R.drawable.ic_profile_bottom);
                toolbarView.setVisibility(View.VISIBLE);
                chatRelativeLayout.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.VISIBLE);
                locationRelativeLayout.setVisibility(View.GONE);
                profileRelativeLayout.setVisibility(View.GONE);
                customViewPager.setCurrentItem(2, true);
                break;

            case R.id.home_search:
                mChat.setImageResource(R.drawable.ic_chat_bottom);
                mLocation.setImageResource(R.drawable.ic_loc_bottom);
                mHome.setImageResource(R.drawable.ic_home_botm_dark);
                mSearch.setImageResource(R.drawable.search);
                mProfile.setImageResource(R.drawable.ic_profile_bottom);
                toolbarView.setVisibility(View.GONE);
                chatRelativeLayout.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.VISIBLE);
                locationRelativeLayout.setVisibility(View.GONE);
                profileRelativeLayout.setVisibility(View.GONE);
                customViewPager.setCurrentItem(3, true);
                break;

            case R.id.home_profile:
                if (mPref.getAccessToken().isEmpty())
                    PlaythoraUtility.LoginDialog(HomeScreenActivity.this);
                else {
                    mChat.setImageResource(R.drawable.ic_chat_bottom);
                    mLocation.setImageResource(R.drawable.ic_loc_bottom);
                    mHome.setImageResource(R.drawable.ic_home_botm_dark);
                    mSearch.setImageResource(R.drawable.ic_search_bottom);
                    mProfile.setImageResource(R.drawable.profile);
                    toolbarView.setVisibility(View.VISIBLE);
                    chatRelativeLayout.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.GONE);
                    locationRelativeLayout.setVisibility(View.GONE);
                    profileRelativeLayout.setVisibility(View.VISIBLE);
                    customViewPager.setCurrentItem(4, true);
                }
                break;

            case R.id.relative_title:
                Intent intent = new Intent(HomeScreenActivity.this, com.yowzapp.playstore.activty.SearchCityActivity.class);
                intent.putExtra("NCOUNT", notificationCount.getText().toString());
                startActivityForResult(intent, 5);
                break;

            case R.id.home_go:
                /*Intent toRewardStore = new Intent(HomeScreenActivity.this,RewardStore.class);
                startActivity(toRewardStore);*/
                showDeleteDialog();
                break;

            case R.id.home_notification:
                if (mPref.getAccessToken().isEmpty())
                    PlaythoraUtility.LoginDialog(HomeScreenActivity.this);
                else {
                    notificationCount.setText("");
                    notificationCount.setVisibility(View.GONE);
                    Intent notification = new Intent(HomeScreenActivity.this, com.yowzapp.playstore.activty.NotificationActivity.class);
                    startActivity(notification);
                }
                break;

            case R.id.rating_one:
                if (one) {
                    rating = 0;
                    rateOne.setImageResource(R.drawable.star_unfilled);
                    rateTwo.setImageResource(R.drawable.star_unfilled);
                    rateThree.setImageResource(R.drawable.star_unfilled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    one = false;
                } else {
                    rating = 1;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_unfilled);
                    rateThree.setImageResource(R.drawable.star_unfilled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    one = true;
                }
                break;

            case R.id.rating_two:
                if (two) {
                    rating = 1;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_unfilled);
                    rateThree.setImageResource(R.drawable.star_unfilled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    two = false;
                } else {
                    rating = 2;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_unfilled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    two = true;
                }

                break;

            case R.id.rating_three:
                if (three) {
                    rating = 2;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_unfilled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    three = false;
                } else {
                    rating = 3;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_filled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    three = true;
                }


                break;

            case R.id.rating_four:
                if (four) {
                    rating = 3;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_filled);
                    rateFour.setImageResource(R.drawable.star_unfilled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    four = false;
                } else {
                    rating = 4;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_filled);
                    rateFour.setImageResource(R.drawable.star_filled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    four = true;
                }
                break;

            case R.id.rating_five:
                if (five) {
                    rating = 4;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_filled);
                    rateFour.setImageResource(R.drawable.star_filled);
                    rateFive.setImageResource(R.drawable.star_unfilled);
                    five = false;
                } else {
                    rating = 5;
                    rateOne.setImageResource(R.drawable.star_filled);
                    rateTwo.setImageResource(R.drawable.star_filled);
                    rateThree.setImageResource(R.drawable.star_filled);
                    rateFour.setImageResource(R.drawable.star_filled);
                    rateFive.setImageResource(R.drawable.star_filled);
                    five = true;
                }
                break;

        }
    }

    private void showDeleteDialog() {
        final Dialog dialog = new Dialog(HomeScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_team_dialog);
        TextView dialogTitle, dialogStatus, dialogCancel, dialogOk;

        dialogTitle = (TextView) dialog.findViewById(R.id.delete_title);
        dialogStatus = (TextView) dialog.findViewById(R.id.delete_confirm);
        dialogCancel = (TextView) dialog.findViewById(R.id.cancel_text);
        dialogOk = (TextView) dialog.findViewById(R.id.delete_text);
        dialogCancel.setVisibility(View.INVISIBLE);
        dialogOk.setText("OK");
        dialogTitle.setText("Hey, that's Reward Store");

        dialogStatus.setText("It's getting ready for you.");

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogOk.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == 5) {
                if (data != null) {
                    if (data.hasExtra("NCOUNT")) {
                        String count = data.getStringExtra("NCOUNT");
                        if (count.isEmpty()) {
                            notificationCount.setText("");
                            notificationCount.setVisibility(View.GONE);
                        } else {
                            notificationCount.setText(count);
                            notificationCount.setVisibility(View.VISIBLE);
                        }
                    }
                    if (data.hasExtra("LOCATION")) {
                        Log.e("location", data.getStringExtra("LOCATION"));
                        String locName = data.getStringExtra("LOCATION");
                        mToolbarLoc.setText(locName);
                        mPref.setLocationName(locName);
                    }
                }
            } else if (requestCode == REQUEST_CHECK_SETTINGS) {
                if (!isLocationEnabled(this)) {
                    Toast.makeText(app, "Location service not enabled", Toast.LENGTH_SHORT).show();
                } else {
//                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (ContextCompat.checkSelfPermission(HomeScreenActivity.this,
//                                Manifest.permission.ACCESS_FINE_LOCATION)
//                                != PackageManager.PERMISSION_GRANTED) {
//                            String[] perms = {"android.permission.ACCESS_FINE_LOCATION"};
//                            int permsRequestCode = 200;
//                            Log.e("LocationPermissionM", "GRANTED");
//                            requestPermissions(perms, permsRequestCode);
//                        }
//                    }
//                    else{
//                        Log.e("LocationPermissionL", "GRANTED");
//                        customViewPager.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    shouldLoadMap = true;
                    customViewPager.notify();
//                    adapter.getItem(1).getTargetFragment().onResume();
//                        customViewPager.setAdapter(adapter);
//                        adapter.getItem(1).getTargetFragment().notifyAll();
//                        customViewPager.setCurrentItem(2);
//                    }
                }
            }
        } catch (Exception e) {
            Log.e("Loc Exception", e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults);

        if (permsRequestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("LocationPermission", "GRANTED");
//                customViewPager.setAdapter(adapter);
                adapter.notifyDataSetChanged();
//                adapter.getItem(1).notify();
            }
        }
    }

    private void setupListeners() {
        mChat.setOnClickListener(this);
        mLocation.setOnClickListener(this);
        mHome.setOnClickListener(this);
        mSearch.setOnClickListener(this);
        mProfile.setOnClickListener(this);
        relativeLayout.setOnClickListener(this);
        mStore.setOnClickListener(this);
        notification.setOnClickListener(this);
    }

    private void initializeAllComponents() {

        customViewPager = (CustomViewPager) findViewById(R.id.home_view_pager);
        mChat = (ImageView) findViewById(R.id.home_chat);
        mLocation = (ImageView) findViewById(R.id.home_location);
        mHome = (ImageView) findViewById(R.id.home_button);
        mSearch = (ImageView) findViewById(R.id.home_search);
        mProfile = (ImageView) findViewById(R.id.home_profile);
        TextView mToolbarTitle = (TextView) findViewById(R.id.home_toolbar_title);
        mToolbarLoc = (TextView) findViewById(R.id.city_name);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_title);
        chatRelativeLayout = (RelativeLayout) findViewById(R.id.chat_relative_layout);
        locationRelativeLayout = (RelativeLayout) findViewById(R.id.location_relative_layout);
        profileRelativeLayout = (RelativeLayout) findViewById(R.id.profile_relative_layout);
        toolbarView = (View) findViewById(R.id.home_screen_toolbar_toolbar);
        mStore = (ImageView) findViewById(R.id.home_go);
        TextView chats = (TextView) findViewById(R.id.chat);
        TextView locations = (TextView) findViewById(R.id.location);
        TextView profile = (TextView) findViewById(R.id.profile);
        notification = (ImageView) findViewById(R.id.home_notification);
        notificationCount = (TextView) findViewById(R.id.notification_count);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        customViewPager.setAdapter(adapter);
        customViewPager.setPagingEnabled(false);
        customViewPager.setCurrentItem(2);
        customViewPager.setOffscreenPageLimit(4);

        chats.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        locations.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        profile.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mToolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mToolbarLoc.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }

    @Override
    public void onBackPressed() {
        if (mPref.getAccessToken().isEmpty()) {
            Intent intent = new Intent(HomeScreenActivity.this, com.yowzapp.playstore.activty.SignupLoginActivity.class);
            startActivity(intent);
            finishAffinity();
        } else {
            if (exit) {
                finish(); // finish activity
            } else {
                Toast.makeText(this, "Press back button again to exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);// 3 sec

            }
        }
    }


    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        int count = 5;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
//                    return new ChatFragment();
                    return new ChatAllFragment();

                case 1:
//                    return new VenueLocationFragment();
//                    return new VenueListFragment();
                    return new VenueMapFragment();

                case 2:
                    return new HomeFragment(value);

                case 3:
                    return new SearchRecentFragment();

                case 4:
                    return new MyProfileFragment();

                default:
                    return new HomeFragment(value);

            }
        }

        @Override
        public int getCount() {
            return count;
        }
    }

    /*   @Override
    public void onBackPressed() {
        if(backButtonCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }*/

    public Context getHomeActivityContext() {
        return HomeScreenActivity.this;
    }

//    //This is the handler that will manager to process the broadcast intent
//    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
////            String message = intent.getStringExtra("message");
//
//            //do other stuff here
//            if(notificationCount.getText().toString().trim().equalsIgnoreCase("")) {
//                notificationCount.setText("1");
//            }
//            else {
//                notificationCount.setText(String.valueOf(Integer.parseInt(notificationCount.getText().toString()) + 1));
//            }
//            notificationCount.setVisibility(View.VISIBLE);
////            Toast.makeText(context, intent.getStringExtra("message"), Toast.LENGTH_SHORT).show();
//        }
//    };


//    // This function will create an intent. This intent must take as parameter the "notification_count_screeen" that you registered your activity with
//    public static void updateNotificationBellCount(Context context, String message) {
//
//        Intent intent = new Intent("notification_count_screeen");
//
//        //put whatever data you want to send, if any
////        intent.putExtra("message", message);
//        Log.e("MESSAGE", message);
//
//        //send broadcast
//        context.sendBroadcast(intent);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        unregisterReceiver(mNotificationReceiver);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        PlaythoraUtility.hideProgressDialog();
        if (!PlaythoraUtility.checkInternetConnection(getBaseContext())) {
            Intent i = new Intent("BaseActivityIntent");
            i.putExtra("status", "DISCONNECTED");
            getBaseContext().sendBroadcast(i);
        } else {
            if (!mPref.getAccessToken().isEmpty())
                getUserDetail();
//            adapter.notifyDataSetChanged();
        }
//        registerReceiver(mNotificationReceiver, new IntentFilter("notification_count_screeen"));
    }
}
