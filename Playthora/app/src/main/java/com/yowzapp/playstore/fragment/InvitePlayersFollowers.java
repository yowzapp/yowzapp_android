package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGameInviteFriends;
import com.yowzapp.playstore.activty.TeamMembersModel;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.model.GamePlayers;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 24/11/16.
 */

public class InvitePlayersFollowers extends Fragment {

    public static ArrayList<TeamMembersModel> teamMembersArrayList;
    public static InvitePlayersFollowersAdapter adapter;
    final Context c = getActivity();
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    Toolbar toolbar;
    TextView s;
    PreferenceManager mPref;
    ProgressBar progressBar;
    AddTeamMembersModel model;
    TextView noPlayers;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<AddTeamMembersList> tempList;
    NestedScrollView nested;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    public InvitePlayersFollowers(TextView toolbarTitle) {
        s = toolbarTitle;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.invite_player_layout, container, false);


        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.invite_players_recycler);
        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        noPlayers = (TextView) contentView.findViewById(R.id.noPlayers);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested_player);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        //  invite = (TextView) contentView.findViewById(R.id.invitePlaythora);
        //  invite.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    Log.e("RESPONSEPAGonLoadMoreE", String.valueOf(model.isHasMorePages()));
                    if (model.isHasMorePages()) {
                        Log.e("page_DONE", page + "");
                        pages = page;
                        followersRecycler(pages);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);


        try {
            followersRecycler(pages);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

        return contentView;

    }

    private void followersRecycler(int pages) {
        Log.e("followers", "#####");
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            // params.add("user_id",mPref.getId());
            httpClient.get(Config.PLAYERS_FOLLOWER + "&page=" + pages, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.v("followers", s);
                        model = new AddTeamMembersModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, AddTeamMembersModel.class);
                        if (model.getSuccess().getData().size() != 0) {
                            Log.e("RESPONSE", String.valueOf(model.getSuccess().getData().size()));
                            Log.e("RESPONSE", String.valueOf(model.getSuccess().getData().size()));
                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }
                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerView.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                adapter = new InvitePlayersFollowersAdapter(getActivity(), model.getSuccess().getData());
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                noPlayers.setText("No followers");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            noPlayers.setText("No followers");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.v("failure", s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void callAlertMaxPlayers() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        View mView = layoutInflaterAndroid.inflate(R.layout.user_limit_pop_up, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setView(mView);
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

        final TextView maxPlayer = (TextView) mView.findViewById(R.id.maxPlayer);
        final TextView okay = (TextView) mView.findViewById(R.id.ok);
        final TextView unSelect = (TextView) mView.findViewById(R.id.adminDetails);

        maxPlayer.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        okay.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        unSelect.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));


        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });


        alertDialogAndroid.show();
    }

    private void showPopUp(String name) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        View mView = layoutInflaterAndroid.inflate(R.layout.make_admin_pop_up, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setView(mView);
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

        final TextView userName = (TextView) mView.findViewById(R.id.userName);
        final TextView select = (TextView) mView.findViewById(R.id.select);
        final TextView unSelect = (TextView) mView.findViewById(R.id.unSelect);

        userName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        select.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        unSelect.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));


        userName.setText(name);

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });

        unSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });


        alertDialogAndroid.show();
    }

    public class InvitePlayersFollowersAdapter extends RecyclerView.Adapter<InvitePlayersFollowersHolder> {

        private List<AddTeamMembersList> arrayList;
        private Context context;
        private boolean[] mCheckedState;

        public InvitePlayersFollowersAdapter(FragmentActivity activity, List<AddTeamMembersList> success) {
            this.context = activity;
            this.arrayList = success;
            mCheckedState = new boolean[this.arrayList.size()];
            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                    if (this.arrayList.get(i).getId() == CreateGameInviteFriends.stringArrayList.get(j).getId()) {
                        //AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                        mCheckedState[i] = true;
                        //break;
                    }
                }
            }
        }

        public void RefreshPagination(List<AddTeamMembersList> newString, boolean pagination) {
            if (pagination) {
                mCheckedState = new boolean[this.arrayList.size()];
                this.arrayList = newString;
                for (int i = 0; i < this.arrayList.size(); i++) {
                    mCheckedState[i] = false;
                }
                Log.e("stringList", CreateGameInviteFriends.stringArrayList.size() + "," + this.arrayList.size());
                for (int i = 0; i < this.arrayList.size(); i++) {
                    for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                        if (this.arrayList.get(i).getId() == CreateGameInviteFriends.stringArrayList.get(j).getId()) {
                            Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                            // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                            mCheckedState[i] = true;
                            break;
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }


        public void Refresh(List<GamePlayers> newString) {
            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }
            Log.e("stringList", String.valueOf(CreateGameInviteFriends.stringArrayList.size() + "," + this.arrayList.size()));
            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < newString.size(); j++) {
                    if (this.arrayList.get(i).getId() == newString.get(j).getId()) {
                        Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                        // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                        mCheckedState[i] = true;
                        //break;
                    }
                }
            }
            notifyDataSetChanged();
        }

        @Override
        public InvitePlayersFollowersHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.following_item_layout, viewGroup, false);
            InvitePlayersFollowersHolder listHolder = new InvitePlayersFollowersHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final InvitePlayersFollowersHolder holder, final int position) {
            final AddTeamMembersList model = arrayList.get(position);


            final InvitePlayersFollowersHolder mainHolder = (InvitePlayersFollowersHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());

                if (mCheckedState[position])
                    mainHolder.checkBox.setChecked(true);
                else mainHolder.checkBox.setChecked(false);
               /* if(addPlayers>5){
                    mainHolder.checkBox.setClickable(false);
                    callAlertMaxPlayers();
                }else {
                    mainHolder.checkBox.setClickable(true);
                }*/

                mainHolder.checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mCheckedState[position]) {
                            //Toast.makeText(context,"checked",Toast.LENGTH_SHORT).show();
                            Log.e("CheckedFree", CreateGameInviteFriends.stringArrayList.size() + "");
                           /* if(new Integer(CreateGameInviteFriends.playersNumber)<=CreateGameInviteFriends.stringArrayList.size()){
                                mCheckedState[position] = false;
                                callAlertMaxPlayers();
                                notifyDataSetChanged();
                                return;
                            }*/
                            if (CreateGameInviteFriends.stringArrayList.size() > 0) {
                                for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                                    if (CreateGameInviteFriends.stringArrayList.get(j).getId() == arrayList.get(position).getId()) {

                                    } else {
                                        CreateGameInviteFriends.stringArrayList.add(new GamePlayers(arrayList.get(position).getId(), "player", arrayList.get(position).getProfile_pic()));
                                        // mCheckedState[position]=true;
                                        mCheckedState[position] = true;
                                        return;
                                    }
                                }
                            } else {
                                CreateGameInviteFriends.stringArrayList.add(new GamePlayers(arrayList.get(position).getId(), "player", arrayList.get(position).getProfile_pic()));
                                // mCheckedState[position]=true;
                                mCheckedState[position] = true;
                            }

                            notifyDataSetChanged();

                        } else {
                            //Toast.makeText(context,"unchecked",Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < CreateGameInviteFriends.stringArrayList.size(); i++) {
                                if (CreateGameInviteFriends.stringArrayList.get(i).getId() == arrayList.get(position).getId()) {
                                    CreateGameInviteFriends.stringArrayList.remove(i);

                                }
                            }
                            //notifyDataSetChanged();
                            mCheckedState[position] = false;
                            notifyDataSetChanged();
                        }

                    }


                });


            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }
            mainHolder.itemView.setTag(model);


        }
    }

    public class InvitePlayersFollowersHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView name;
        CheckBox checkBox;
        RelativeLayout makeAdmin;

        public InvitePlayersFollowersHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
            this.name = (TextView) view.findViewById(R.id.userName);
            this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            this.makeAdmin = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);
        }
    }
}
