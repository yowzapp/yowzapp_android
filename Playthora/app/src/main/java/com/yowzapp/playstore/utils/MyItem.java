package com.yowzapp.playstore.utils;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by vaishakha on 5/1/17.
 */
public class MyItem implements ClusterItem {
    private final LatLng mPosition;
    private BitmapDescriptor icon;

    public MyItem(MarkerOptions markerOptions, double lat, double lng) {
        mPosition = new LatLng(lat, lng);
        this.icon = markerOptions.getIcon();
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public BitmapDescriptor getIcon() {
        return icon;
    }

    public void setIcon(BitmapDescriptor icon) {
        this.icon = icon;
    }
}
