package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.viewpagerindicator.CirclePageIndicator;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.fragment.SlidesFragmentAdapter;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 25/7/16.
 */
public class OnBoardingActivity extends FragmentActivity {
    ViewPager viewPager;
    CirclePageIndicator indicator;
    Button mButton;
    private SlidesFragmentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.on_boarding);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        viewPager = (ViewPager) findViewById(R.id.pager);
        mButton = (Button) findViewById(R.id.go_button);

        mAdapter = new SlidesFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);

        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        indicator.setStrokeWidth(0);

        mButton.setTypeface(PlaythoraUtility.getFont(OnBoardingActivity.this, RALEWAY_REGULAR));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mButton.setVisibility(View.INVISIBLE);
                        break;

                    case 1:
                        mButton.setVisibility(View.INVISIBLE);
                        break;

                    case 2:
                        mButton.setVisibility(View.INVISIBLE);
                        break;

                    case 3:
                        mButton.setVisibility(View.VISIBLE);

                        mButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(OnBoardingActivity.this, SignupLoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });

                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}
