package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.RewardStoreWhatsHot;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.CustomTypefaceSpan;
import com.yowzapp.playstore.utils.CustomViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.R.id.equipmentsSelected;

public class RewardStore extends BaseActivity implements View.OnClickListener {

    public static CustomViewPager pager;
    public static MyPagerAdapter adapter;
    DrawerLayout drawer;
    TextView availablePointsText, userNameText, pointsText, whatsHot,
            whatsHotSelected, sports, sportsSelected, equipments, epuipmentsSelectedText;
    Toolbar toolbar;
    NavigationView rightNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reward_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        whatsHot = (TextView) findViewById(R.id.whatsHot);
        whatsHotSelected = (TextView) findViewById(R.id.whatsHotSelected);
        sports = (TextView) findViewById(R.id.sportsAccesories);
        sportsSelected = (TextView) findViewById(R.id.sportsAccesoriesSelected);
        equipments = (TextView) findViewById(R.id.equipments);
        epuipmentsSelectedText = (TextView) findViewById(equipmentsSelected);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));

        whatsHot.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));
        whatsHotSelected.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));
        sports.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));
        sportsSelected.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));
        equipments.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));
        epuipmentsSelectedText.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        setupListeners();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rightNavigationView = (NavigationView) findViewById(R.id.nav_right_view);
        View hView = rightNavigationView.getHeaderView(0);
        Menu m = rightNavigationView.getMenu();

        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        userNameText = (TextView) hView.findViewById(R.id.userName);
        pointsText = (TextView) hView.findViewById(R.id.numberOfPoints);
        availablePointsText = (TextView) hView.findViewById(R.id.availablePoints);

        userNameText.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));
        pointsText.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_BOLD));
        availablePointsText.setTypeface(PlaythoraUtility.getFont(RewardStore.this, Config.RALEWAY_REGULAR));

        rightNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle Right navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.wishList) {
                    Toast.makeText(RewardStore.this, "WishList", Toast.LENGTH_SHORT).show();
                    Intent toWishList = new Intent(RewardStore.this, WishListActivity.class);
                    startActivity(toWishList);
                } else if (id == R.id.myOrders) {
                    Toast.makeText(RewardStore.this, "MyOrders", Toast.LENGTH_SHORT).show();
                    Intent toOrderList = new Intent(RewardStore.this, MyOrders.class);
                    startActivity(toOrderList);
                } else if (id == R.id.support) {
                    Toast.makeText(RewardStore.this, "Support", Toast.LENGTH_SHORT).show();
                    Intent toWishList = new Intent(RewardStore.this, Support.class);
                    startActivity(toWishList);
                }

                drawer.closeDrawer(GravityCompat.END); /*Important Line*/
                return true;
            }
        });


        pager = (CustomViewPager) findViewById(R.id.fragment);
        adapter = new MyPagerAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);
        pager.setPagingEnabled(true);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    sportsAccessorites();
                } else if (position == 2) {
                    equipmentsMethod();
                } else {
                    whatsHotMethod();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupListeners() {
        whatsHot.setOnClickListener(this);
        whatsHotSelected.setOnClickListener(this);
        sports.setOnClickListener(this);
        sportsSelected.setOnClickListener(this);
        equipments.setOnClickListener(this);
        epuipmentsSelectedText.setOnClickListener(this);

    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), Config.RALEWAY_REGULAR);
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.whatsHot:
                whatsHotMethod();
                pager.setCurrentItem(0);

                break;

            case R.id.sportsAccesories:
                // Toast.makeText(getApplicationContext(),"SportAccessories",Toast.LENGTH_SHORT).show();
                sportsAccessorites();

                pager.setCurrentItem(1);

                break;

            case R.id.equipments:
                equipmentsMethod();

                pager.setCurrentItem(2);

                break;


            default:
                break;

        }


    }

    private void equipmentsMethod() {
        epuipmentsSelectedText.setVisibility(View.VISIBLE);
        equipments.setVisibility(View.GONE);
        whatsHotSelected.setVisibility(View.GONE);
        sportsSelected.setVisibility(View.GONE);
        whatsHot.setVisibility(View.VISIBLE);
        sports.setVisibility(View.VISIBLE);
    }

    private void sportsAccessorites() {
        sportsSelected.setVisibility(View.VISIBLE);
        sports.setVisibility(View.GONE);
        whatsHotSelected.setVisibility(View.GONE);
        whatsHot.setVisibility(View.VISIBLE);
        equipments.setVisibility(View.VISIBLE);
        epuipmentsSelectedText.setVisibility(View.GONE);
    }

    private void whatsHotMethod() {
        whatsHotSelected.setVisibility(View.VISIBLE);
        whatsHot.setVisibility(View.GONE);
        sports.setVisibility(View.VISIBLE);
        sportsSelected.setVisibility(View.GONE);
        epuipmentsSelectedText.setVisibility(View.GONE);
        equipments.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {  /*Closes the Appropriate Drawer*/
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
            System.exit(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_openRight) {
            drawer.openDrawer(GravityCompat.END);
            return true;
        }

        if (id == R.id.action_openSearch) {
            Intent toSearchActivity = new Intent(RewardStore.this, SearchActivity.class);
            startActivity(toSearchActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"What's hot", "Sport accessories", "Equipments"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new RewardStoreWhatsHot();

                case 1:
                    return new RewardStoreWhatsHot();

                case 2:
                    return new RewardStoreWhatsHot();

                default:
                    return new RewardStoreWhatsHot();

            }

        }
    }
}
