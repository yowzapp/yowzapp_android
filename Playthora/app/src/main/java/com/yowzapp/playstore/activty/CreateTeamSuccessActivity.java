package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 14/11/16.
 */

public class CreateTeamSuccessActivity extends Activity {


    ImageView successImage;
    TextView teamCreatedText, allSet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_team_success_layout);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        successImage = (ImageView) findViewById(R.id.team_successful_image);
        teamCreatedText = (TextView) findViewById(R.id.team_created_text);
        allSet = (TextView) findViewById(R.id.allSet);

        teamCreatedText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        allSet.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        successImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CreateTeamSuccessActivity.this, HomeScreenActivity.class);
                i.putExtra("ACTIVITY", 1);
                startActivity(i);
                finishAffinity();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(CreateTeamSuccessActivity.this, HomeScreenActivity.class);
        i.putExtra("ACTIVITY", 0);
        startActivity(i);
        finishAffinity();
    }
}
