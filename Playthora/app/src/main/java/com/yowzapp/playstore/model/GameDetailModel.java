package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 28/12/16.
 */

public class GameDetailModel {
    private String status;
    private String message;
    private GameDetailSuccess success;
    private String ground_name;

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    private String venue;

    public String getGroundName() {
        return ground_name;
    }

    public void setGroundName(String groundName) {
        this.ground_name = groundName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GameDetailSuccess getSuccess() {
        return success;
    }

    public void setSuccess(GameDetailSuccess success) {
        this.success = success;
    }
}
