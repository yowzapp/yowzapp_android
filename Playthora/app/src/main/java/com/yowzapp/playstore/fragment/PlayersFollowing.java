package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.MyProfileLeaderBoardMain;
import com.yowzapp.playstore.adapter.FollowersAdapter;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by nakul on 24/11/16.
 */
public class PlayersFollowing extends Fragment {

    RecyclerView recyclerView;
    String admin, userID;
    PreferenceManager mPref;
    String url;

    ProgressDialog dialog;
    Gson mGson;
    TextView invite;
    AddTeamMembersModel model;
    List<AddTeamMembersList> tempList;
    RecyclerView recyclerFollowers;
    FollowersAdapter adapter;
    TextView noPlayers;
    Button leaderBoard;


    public PlayersFollowing(String admin, String userID) {
        this.admin = admin;
        this.userID = userID;

        //  editText.setHint("Search for following players");
        Log.e("admin", admin);
        Log.e("userID", userID);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.following_fragment, container, false);
        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.following_recycler);
        leaderBoard = (Button) contentView.findViewById(R.id.go_to_leaderboard);
        noPlayers = (TextView) contentView.findViewById(R.id.noPlayers);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        leaderBoard.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        try {
            PopulateRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        leaderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyProfileLeaderBoardMain.class);
                startActivity(intent);
            }
        });

        return contentView;

    }

    private void PopulateRecycler() {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            PlaythoraUtility.showProgressDialog(getActivity());
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();

            if (admin.equalsIgnoreCase("true")) {
                url = Config.FOLLOWING_ME;
                Log.e("me", url);
            } else {
                url = Config.FOLLOWING_ME + "&user_id=" + userID;
                Log.e("other", url);
            }
            httpClient.get(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.v("following", s);
                        model = new AddTeamMembersModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, AddTeamMembersModel.class);
                        if (model.getSuccess().getData().size() != 0) {

                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }
                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerView.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                adapter = new FollowersAdapter(getActivity(), model.getSuccess().getData());
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                                leaderBoard.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                leaderBoard.setVisibility(View.VISIBLE);
                                leaderBoard.setText("Go to leaderboard");
                                noPlayers.setText("You are not following any player yet. Check out the top\n users in your city");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            leaderBoard.setVisibility(View.VISIBLE);
                            leaderBoard.setText("Go to leaderboard");
                            noPlayers.setText("You are not following any player yet. Check out the top\n users in your city");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }
}
