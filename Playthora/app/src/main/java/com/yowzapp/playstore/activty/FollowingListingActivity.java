package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.FollowersAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 13/1/17.
 */
public class FollowingListingActivity extends BaseActivity {

    FollowersAdapter adapter;
    Toolbar toolbar;
    RecyclerView recyclerFollowers;
    PreferenceManager mPref;
    String admin, userID;
    String url;
    AddTeamMembersModel model;
    Gson mGson;
    List<AddTeamMembersList> tempList;
    TextView noPlayers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers_listing);
        mPref = PreferenceManager.instance(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Intent intent = getIntent();
        admin = intent.getStringExtra("admin");
        userID = intent.getStringExtra("userId");
        Log.e("admin", admin);
        Log.e("userID", userID);


        toolbar = (Toolbar) findViewById(R.id.followers_toolbar);
        TextView toolBarTitle = (TextView) findViewById(R.id.followers_toolbar_title);
        toolBarTitle.setText("Following");
        noPlayers = (TextView) findViewById(R.id.noPlayers);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerFollowers = (RecyclerView) findViewById(R.id.followers_recycler);
        recyclerFollowers.setLayoutManager(new LinearLayoutManager(FollowingListingActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerFollowers.setHasFixedSize(true);
        try {
            PopulateRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PopulateRecycler() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(FollowingListingActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();

            if (admin.equalsIgnoreCase("true")) {
                url = Config.FOLLOWING_ME;
                Log.e("me", url);
            } else {
                url = Config.FOLLOWING_ME + "&user_id=" + userID;
                Log.e("other", url);
            }
            httpClient.get(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.v("followers", s);
                        model = new AddTeamMembersModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, AddTeamMembersModel.class);
                        if (model.getSuccess().getData().size() != 0) {

                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }
                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerFollowers.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                adapter = new FollowersAdapter(FollowingListingActivity.this, model.getSuccess().getData());
                                recyclerFollowers.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                noPlayers.setText("You are not following any players");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            noPlayers.setText("You are not following any players");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.followers_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.followers_selected) {
            /*Intent intent=new Intent();
            setResult(2,intent);
            finish();*/
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
