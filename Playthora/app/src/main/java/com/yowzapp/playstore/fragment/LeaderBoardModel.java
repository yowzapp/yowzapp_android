package com.yowzapp.playstore.fragment;

import com.yowzapp.playstore.model.MyProfileLeaderBoardModel;

import java.util.ArrayList;

/**
 * Created by vaishakha on 19/1/17.
 */
public class LeaderBoardModel {
    private String status;
    private ArrayList<MyProfileLeaderBoardModel> success;
    private ArrayList<MyProfileLeaderBoardModel> mine;
    private int currentPage;
    private boolean hasMorePages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<MyProfileLeaderBoardModel> getSuccess() {
        return success;
    }

    public void setSuccess(ArrayList<MyProfileLeaderBoardModel> success) {
        this.success = success;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

    public ArrayList<MyProfileLeaderBoardModel> getMine() {
        return mine;
    }

    public void setMine(ArrayList<MyProfileLeaderBoardModel> mine) {
        this.mine = mine;
    }
}
