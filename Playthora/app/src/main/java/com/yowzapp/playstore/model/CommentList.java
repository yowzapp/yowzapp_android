package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 10/1/17.
 */
public class CommentList {
    private int id;
    private int user_id;
    private int activity_id;
    private String content;
    private String dateTime;
    private UserData user;

    public CommentList(String content, int id, UserData user, String dateTime) {
        this.content = content;
        this.id = id;
        this.user = user;
        this.dateTime = dateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(int activity_id) {
        this.activity_id = activity_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }


}
