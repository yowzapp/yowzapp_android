package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 28/12/16.
 */

public class GameListModel {
    private String status;
    private String message;
    private int currentPage;
    private boolean hasMorePages;
    private GameListSuccess success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

    public GameListSuccess getSuccess() {
        return success;
    }

    public void setSuccess(GameListSuccess success) {
        this.success = success;
    }
}
