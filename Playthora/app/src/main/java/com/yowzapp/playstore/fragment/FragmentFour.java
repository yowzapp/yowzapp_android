package com.yowzapp.playstore.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 25/7/16.
 */
public class FragmentFour extends Fragment {
    ImageView imageView;
    TextView firstText, secText;

    public FragmentFour() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.on_board_fragment_layout, container, false);

        imageView = (ImageView) contentView.findViewById(R.id.first_image);
        firstText = (TextView) contentView.findViewById(R.id.tv_first_text);
        secText = (TextView) contentView.findViewById(R.id.tv_sec_text);

        imageView.setImageResource(R.drawable.ic_claim_it);
        firstText.setText(R.string.claim_it);
        secText.setText(R.string.claim_it_sec_text);
        firstText.setTypeface(PlaythoraUtility.getFont(getContext(), RALEWAY_BOLD));
        secText.setTypeface(PlaythoraUtility.getFont(getContext(), RALEWAY_REGULAR));


        return contentView;
    }
}