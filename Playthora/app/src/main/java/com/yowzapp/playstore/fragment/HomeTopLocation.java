package com.yowzapp.playstore.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.model.HomeTopLocationModel;
import com.yowzapp.playstore.model.VenueListItem;
import com.yowzapp.playstore.model.VenueListModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 15/11/16.
 */
public class HomeTopLocation extends Fragment {

    public GridLayoutManager gridLayoutManager;
    View contentView;
    RecyclerView recyclerView;
    Gson gson;
    ArrayList<HomeTopLocationModel> model;
    TopLocAdapter adapter;
    TextView emptyText;
    String response;
    VenueListModel venueListModel;
    List<VenueListItem> tempList;
    PreferenceManager mPref;
    ProgressBar progressBar;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    String VenueResponse;
    JSONArray jsonArray = new JSONArray();
    SwipeRefreshLayout swipeContainer;
    private int previousTotal = 0, pages = 1, visibleThreshold = 3;
    private boolean loading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.home_team_layout, container, false);
        recyclerView = contentView.findViewById(R.id.team_recycler);
        mPref = PreferenceManager.instance(getActivity());
        emptyText = contentView.findViewById(R.id.tv_empty_state);
        progressBar = contentView.findViewById(R.id.progress_bar_one);
        swipeContainer = contentView.findViewById(R.id.swipeContainer_loc);

        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("SCROLLING", "onScrollStateChanged");
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = gridLayoutManager.getItemCount();
                firstVisibleItem = gridLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();

                try {
                    if (venueListModel.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                pages += 1;
                                try {
                                    loadTopLocations(pages, true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                adapter.notifyDataSetChanged();

                            }
                        }
                    }
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        Log.e("SIZE", "end called");
                        loading = true;
                        // Do something
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                    FloatingActionButton floatingActionButton = HomeFragment.getFloatingActionButton();
                    if (floatingActionButton != null) {
                        floatingActionButton.hide();
                    }
                } else {
                    // Scrolling down
                    FloatingActionButton floatingActionButton = HomeFragment.getFloatingActionButton();
                    if (floatingActionButton != null) {
                        floatingActionButton.show();
                    }
                }
            }
        });

        try {
            loadTopLocations(pages, false);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    loadTopLocations(1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                    swipeContainer.setRefreshing(false);
                }
            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);


        return contentView;
    }

    private void loadTopLocations(final int pages, final boolean swipe) {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!swipe)
                        progressBar.setVisibility(View.VISIBLE);

                    AsyncHttpClient httpClient = new AsyncHttpClient();
                    httpClient.addHeader("accessToken", mPref.getAccessToken());
                    httpClient.get(Config.TOP_LOCATION + "?page=" + pages, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            progressBar.setVisibility(View.GONE);
                            swipeContainer.setRefreshing(false);
                            try {
                                VenueResponse = new String(responseBody);
                                Log.v("TOP_LOCATION", VenueResponse);
                                Log.e("RANK", "5");
                                venueListModel = new VenueListModel();
                                gson = new Gson();
                                venueListModel = gson.fromJson(VenueResponse, VenueListModel.class);
                                if (venueListModel.getSuccess().size() != 0) {
                                    emptyText.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    if (venueListModel.getCurrentPage() > 1) {
                                        for (int i = 0; i < venueListModel.getSuccess().size(); i++) {
                                            tempList.add(venueListModel.getSuccess().get(i));
                                        }
                                        JSONObject jsonObject = new JSONObject(VenueResponse);
                                        JSONArray tempJsonArray = new JSONArray();
                                        tempJsonArray = jsonObject.getJSONArray("success");
                                        for (int j = 0; j < tempJsonArray.length(); j++) {
                                            jsonArray.put(tempJsonArray.get(j));
                                        }

                                        Log.e("venueList", String.valueOf(venueListModel.getCurrentPage()));
                                        adapter.loadData(tempList, 2);
                                    } else {
                                        tempList = venueListModel.getSuccess();
                                        JSONObject jsonObject = new JSONObject(VenueResponse);
                                        jsonArray = jsonObject.getJSONArray("success");
                                        adapter = new TopLocAdapter(getActivity(), venueListModel.getSuccess());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                recyclerView.setAdapter(adapter);
                                            }
                                        });
                                    }
                                } else {
                                    emptyText.setVisibility(View.VISIBLE);
                                    emptyText.setText(R.string.no_venue);
                                    recyclerView.setVisibility(View.GONE);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            progressBar.setVisibility(View.GONE);
                            swipeContainer.setRefreshing(false);
                            try {
                                String s = new String(responseBody);
                                Log.e("myGamefailure", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finishAffinity();

                                } else
                                    Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("myGamefailure", s);
                                    JSONObject object = new JSONObject(s);
                                    Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    });
                }
            });
        } else {
            swipeContainer.setRefreshing(false);
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    public class TopLocAdapter extends RecyclerView.Adapter<TopLocAdapter.TopLocationHolder> {
        Activity context;
        String gName, gLocation, gImage, gDistance, gSports, aminities;
        int gRating;
        private List<VenueListItem> model;


        public TopLocAdapter(FragmentActivity activity, List<VenueListItem> success) {
            context = activity;
            this.model = success;
        }

        @Override
        public TopLocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_loc_item_layout, null);
            return new TopLocationHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(TopLocationHolder holder, final int position) {

            holder.mGroundName.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            holder.mLocation.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

            holder.mGroundName.setText(model.get(position).getName());
            holder.mLocation.setText(model.get(position).getLocation_name());

            try {
                Glide.with(context).load(model.get(position).getCover_pic()).error(R.drawable.test_location).centerCrop().into(holder.mImage);
            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.test_location);
            }

            holder.ratingBar.setRating(model.get(position).getRating());
            Log.e("topGroundName", model.get(position).getName());
            Log.e("topLocName", model.get(position).getLocation_name());
            Log.e("topLocRating", model.get(position).getRating() + "");

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gName = model.get(position).getName();
                    gLocation = model.get(position).getLocation_name();
                    gImage = model.get(position).getCover_pic();
                    gDistance = model.get(position).getDistance();
                    gSports = model.get(position).getSports_types();
                    aminities = model.get(position).getAmenities();
                    gRating = model.get(position).getRating();
                    VenueListFragment venueListFragment = new VenueListFragment();
                    try {
                        venueListFragment.groundDetailDialog(context, gName, gSports, aminities, gLocation,
                                gImage, gDistance, gRating, position, model.get(position).getGrounds(), jsonArray.get(position).toString(), model.get(position).getImages(), false, true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public void loadData(List<VenueListItem> tempList, int i) {
            this.model = tempList;
            notifyDataSetChanged();
        }

        protected class TopLocationHolder extends RecyclerView.ViewHolder {
            ImageView mImage;
            TextView mGroundName, mLocation;
            RatingBar ratingBar;

            public TopLocationHolder(View itemView) {
                super(itemView);
                mImage = itemView.findViewById(R.id.loc_image);
                mGroundName = itemView.findViewById(R.id.ground_name);
                mLocation = itemView.findViewById(R.id.loc_name);
                ratingBar = itemView.findViewById(R.id.ratings);
            }
        }
    }
}
