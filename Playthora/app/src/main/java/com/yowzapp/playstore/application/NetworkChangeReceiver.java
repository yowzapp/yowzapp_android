package com.yowzapp.playstore.application;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.yowzapp.playstore.R;

/**
 * Created by maheshprajapat on 2/20/18.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "NetworkChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        isNetworkAvailable(context, intent);
    }

    private void isNetworkAvailable(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        Intent i = new Intent("BaseActivityIntent");
        if (netInfo != null && netInfo.isConnected()) {
                Log.v(LOG_TAG, "CONNECTED to Internet!");
            i.putExtra("status", "CONNECTED");
        } else {
                Log.v(LOG_TAG, "DISCONNECTED from Internet!");
            i.putExtra("status", "DISCONNECTED");
        }
        context.sendBroadcast(i);
    }
}
