package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 15/12/16.
 */

public class LocationModel {

    private String status;
    private String message;
    private List<LocationList> success;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LocationList> getSuccess() {
        return success;
    }

    public void setSuccess(List<LocationList> success) {
        this.success = success;
    }


}
