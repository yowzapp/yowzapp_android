package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class PaymentSuccessActivity extends BaseActivity {

    RelativeLayout transactionCompletedLayout, transactionFailureLayout;
    TextView teamCreated, allSet, teamNotCreated, allNotSet;
    private String getGameId;
    private String rawdate;
    private String transactionId;
    private PreferenceManager mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_game_success);
        mPref = PreferenceManager.instance(getApplicationContext());
        getGameId = mPref.getGameId();
        rawdate = mPref.getRawDate();
        Log.e("POST_PAYMENT", "SUCCESS");

        Intent i = getIntent();
        // Log.e( "EXTRAS", i.getExtras().getString("tranref"));
        Log.e("EXTRAS", i.hasExtra("tranref") + "");

        transactionCompletedLayout = (RelativeLayout) findViewById(R.id.transactionCompletedLayout);
        transactionFailureLayout = (RelativeLayout) findViewById(R.id.transactionFailureLayout);
        teamCreated = (TextView) findViewById(R.id.team_created_text);
        allSet = (TextView) findViewById(R.id.allSet);
        teamNotCreated = (TextView) findViewById(R.id.team_not_created_text);
        allNotSet = (TextView) findViewById(R.id.allNotSet);

        teamCreated.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        allSet.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        teamNotCreated.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        allNotSet.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        Intent intent = getIntent();
        StatusResponse status = (StatusResponse) intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
        Log.e("statusTrace", status.getTrace());
        transactionId = status.getAuth().getTranref();

        transactionCompletedLayout.setVisibility(View.GONE);
        transactionFailureLayout.setVisibility(View.GONE);
        transactionSuccess();
        transactionCompletedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHomeScreen();
            }
        });
        transactionFailureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHomeScreen();
            }
        });
    }

    void goToHomeScreen() {
        Intent toHomeScreen = new Intent(PaymentSuccessActivity.this, HomeScreenActivity.class);
        startActivity(toHomeScreen);
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        goToHomeScreen();
    }

    private void transactionSuccess() {
        try {
            // Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(PaymentSuccessActivity.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());

                try {
                    JSONObject object = new JSONObject();
                    object.put("game_id", getGameId);
                    object.put("date", rawdate);
                    object.put("transaction_id", transactionId);
//                    object.put("slot", selectedSlotsIds);

                    Log.e("paySucess",object+"");
                    Log.e("game_id",getGameId);
                    Log.e("date",rawdate);
                    Log.e("transaction_id",transactionId);


                    Log.e("JSONOBJ", object.toString());
                    StringEntity entity = new StringEntity(object.toString());
                    Log.v("Razor", Config.RAZORPAY_SUCCESS);
                    httpClient.post(PaymentSuccessActivity.this, Config.RAZORPAY_SUCCESS, entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.v("RazorRuccess", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") != 200) {
                                    transactionCompletedLayout.setVisibility(View.GONE);
                                    transactionFailureLayout.setVisibility(View.VISIBLE);
                                    Toast.makeText(getApplicationContext(), "Transaction Error", Toast.LENGTH_LONG).show();
                                } else {
                                    transactionCompletedLayout.setVisibility(View.VISIBLE);
                                    transactionFailureLayout.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                transactionCompletedLayout.setVisibility(View.GONE);
                                transactionFailureLayout.setVisibility(View.VISIBLE);
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                PlaythoraUtility.hideProgressDialog();
                                transactionCompletedLayout.setVisibility(View.GONE);
                                transactionFailureLayout.setVisibility(View.VISIBLE);
                                String s = new String(responseBody);
                                Log.v("failure", s);
                                JSONObject object = new JSONObject(s);
                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e("Exception", "Exception in onPaymentSuccess", e);
        }
    }
}

