package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 15/11/16.
 */
public class HomeImageModel {
    private String image;
    private String url;
    private int id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
