package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 3/2/17.
 */
public class PlayerChatModel {

    private String name;
    private String id;
    private String followers;
    private String players;
    private String message;
    private String image;

    public PlayerChatModel(String name, String id, String followers, String players, String message, String image) {
        this.name = name;
        this.id = id;
        this.followers = followers;
        this.players = players;
        this.message = message;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
