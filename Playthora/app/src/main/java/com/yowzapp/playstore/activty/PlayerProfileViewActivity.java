package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.PlayerDetailModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.CustomViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class PlayerProfileViewActivity extends BaseActivity implements  View.OnClickListener {
    public CustomViewPager pager;
    public MyPagerAdapter adapter;
    public PlayerDetailModel playerDetailModel;
    TextView associted, associtedSelected, topgame, topgameSelected, favGame, favGameSelected, title, monday, tuesday, wednesday, thursday, friday, saturday, sunday;
    NestedScrollView scrollview;
    Toolbar toolbar;
    ImageView toggle, chatIcon;
    CircleImageView primaryTeamImage;
    Boolean mon = false, tue = false, wed = false, thu = false, fri = false, sat = false, sun = false, toggleb;
    TextView playerName, followersNum, followingNum, txtFollowers, txtFollowing, txtGamesPlayed, gamesPlayed,
            bio, primaryText, searchName, primaryTeamMembers, primaryTeamFollowers, members, followers,
            daysText, privateProfile, privateProfileSub, timeTextValue, locatinNames, level, mProfile;
    Button btnEdit;
    Button requestGame;
    RelativeLayout privateProfileLay, gameRelative, myProfile;
    String s = "me";
    CircleImageView circleImageView;
    PreferenceManager mPref;
    ImageView cover;
    String id;
    String nameValue, profile_pic, cover_pic, isSocial, type, userBio, followersCount,
            followingCount, gameCount, mine, isFollowing, isFollower, isRequested, isConfirm;
    Gson gson;
    String response, pId, pCoverPic, pName, pMembers, pFollowers, prefLocation = "";

    JSONObject object, mainObj;
    JSONArray days;
    String search = "";
    TextView noPrimaryTeam;
    RelativeLayout primRelative;
    View seperateView;
    String primaryResponse = "", favSportsResponse = "", teamsResponse = "", topSportsResponse = "", daysValue = "";
    String mValue, tValue, wValue, thuValue, friValue, satValue, sunValue, fTime, tTime;
    RelativeLayout followersLayout, followingLayout;
    int levelId;
    String s1;
    String admin;
    Toast toast;
    private boolean isNotificationTriggered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_profile_view);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        initializeAll();


        Intent i = getIntent();
        if (i.hasExtra("Id")) {
            id = i.getStringExtra("Id");
        } else if (i.hasExtra("user_id")) {
            id = i.getStringExtra("user_id");
            isNotificationTriggered = true;
        }


        Log.e("userId", id + "");

        if (i.hasExtra("SEARCH")) {
            search = i.getStringExtra("SEARCH");
        }
        if (mPref.getAccessToken().isEmpty())
            search = "";

        if (i.hasExtra("admin")) {
            admin = i.getStringExtra("admin");
        } else {
            admin = "false";
        }
        Log.e("admin", admin + "");


        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        title.setText("Profile");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (admin.equalsIgnoreCase("true")) {
                    Intent intent = new Intent(PlayerProfileViewActivity.this, HomeScreenActivity.class);
                    intent.putExtra("FRAGMENT", "Profile");
                    startActivity(intent);
                    finishAffinity();
                } else {
                    onBackPressed();
                }
            }
        });

        setupListeners();

        primRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlayerProfileViewActivity.this, OtherTeamViewActivity.class);
                i.putExtra("ID", Integer.parseInt(pId));
                startActivity(i);
            }
        });

        gameRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toast != null) {
                    toast.cancel();
                }
                if (PlaythoraUtility.checkInternetConnection(PlayerProfileViewActivity.this)) {
                    if (!gameCount.equalsIgnoreCase("0")) {
                        Intent intent = new Intent(getApplicationContext(), ProfileGames.class);
                        intent.putExtra("ID", Integer.parseInt(id));
                        startActivity(intent);
                    }
                }

            }
        });

        chatIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mine.equalsIgnoreCase("false")) {
                    Intent chat = new Intent(PlayerProfileViewActivity.this, ChattingActivity.class);
                    chat.putExtra("name", nameValue);
                    chat.putExtra("image", profile_pic);
                    chat.putExtra("id", id + "");
                    chat.putExtra("type", "player");
                    chat.putExtra("Conv_id", Integer.parseInt(id));
                    chat.putExtra("Conv_type", 3);
                    startActivity(chat);
                }

            }
        });



/*
        if(!mon){
            monday.setTextColor(Color.parseColor("#9CACBA"));
            monday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back));
            mon=false;

        }else{
            monday.setTextColor(Color.parseColor("#FFFFFF"));
            monday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back));
            mon=true;

        }

        if(!tue){
            tuesday.setTextColor(Color.parseColor("#9CACBA"));
            tuesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back));
            tue=false;
        }else {
            tuesday.setTextColor(Color.parseColor("#FFFFFF"));
            tuesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back));
            tue=true;
        }
        if(!wed){
            wednesday.setTextColor(Color.parseColor("#9CACBA"));
            wednesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_bak_three));
            wed=false;

        }else{
            wednesday.setTextColor(Color.parseColor("#FFFFFF"));
            wednesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_three));
            wed=true;
        }

        if(!thu){
            thursday.setTextColor(Color.parseColor("#9CACBA"));
            thursday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_bak_three));
            thu=false;

        }else{
            thursday.setTextColor(Color.parseColor("#FFFFFF"));
            thursday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_three));
            thu=true;
        }

        if(!fri){
            friday.setTextColor(Color.parseColor("#9CACBA"));
            friday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_five));
            fri=false;
        }else{
            friday.setTextColor(Color.parseColor("#FFFFFF"));
            friday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
            fri=true;

        }

        if(!sat){
            saturday.setTextColor(Color.parseColor("#9CACBA"));
            saturday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_five));
            sat=false;
        }else{
            saturday.setTextColor(Color.parseColor("#FFFFFF"));
            saturday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
            sat=true;

        }

        if(!sun){
            sunday.setTextColor(Color.parseColor("#9CACBA"));
            sunday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_back_five));
            sun=false;
        }else{
            sunday.setTextColor(Color.parseColor("#FFFFFF"));
            sunday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
            sun=true;
        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (toast != null) {
            toast.cancel();
        }
        if (PlaythoraUtility.checkInternetConnection(PlayerProfileViewActivity.this)) {
            getUser(id);
        }
    }

    private void getUser(String id) {
        Log.e("USERID", id + "");
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("user_id", id);

            Log.e("DETAIL_URL", Config.PLAYERS_DETAILS + search);
            Log.e("user_id", id);

            httpClient.get(Config.PLAYERS_DETAILS + search, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        response = new String(responseBody);
                        Log.v("userDetail", response);
                          /*  gson = new Gson();
                            playerDetailModel = new PlayerDetailModel();
                            playerDetailModel = gson.fromJson(response,PlayerDetailModel.class);

                            if(playerDetailModel.getSuccess()!=null){*/

                           /*     nameValue = playerDetailModel.getSuccess().getName();
                                userBio = playerDetailModel.getSuccess().getBio();
                                profile_pic = playerDetailModel.getSuccess().getProfile_pic();
                                cover_pic = playerDetailModel.getSuccess().getCover_pic();
                                isSocial = playerDetailModel.getSuccess().is_social() + "";
                                type = playerDetailModel.getSuccess().is_private() + "";
                                followersCount = playerDetailModel.getSuccess().getFollowers_count()+"";
                                followingCount = playerDetailModel.getSuccess().getFollowing_count()+"";
                                gameCount = playerDetailModel.getSuccess().getGames_count()+"";

                                Log.e("userName", nameValue);
                                Log.e("userImage", profile_pic);
                                Log.e("profileType", type);
                                Log.e("followersCount", followersCount);
                                Log.e("followingCount", followingCount);
                                Log.e("gameCount", gameCount);

                                mPref.setName(nameValue);
                                mPref.setBio(userBio);
                                mPref.setProfilePic(profile_pic);
                                mPref.setCoverPic(cover_pic);

                                playerName.setText(mPref.getName());
                                bio.setText(mPref.getBio());
                                followersNum.setText(followersCount);
                                followingNum.setText(followingCount);
                                gamesPlayed.setText(gameCount);


                                if (mPref.getProfilePic().isEmpty()) {
                                    Glide.with(PlayerProfileViewActivity.this).load(R.drawable.circled_user).centerCrop().into(circleImageView);
                                } else {
                                    Glide.with(PlayerProfileViewActivity.this).load(mPref.getProfilePic()).centerCrop().into(circleImageView);
                                }

                                if (mPref.getCoverPic().isEmpty()) {
                                    Glide.with(PlayerProfileViewActivity.this).load(R.drawable.playthora_doodle).centerCrop().into(cover);
                                } else {
                                    Glide.with(PlayerProfileViewActivity.this).load(mPref.getCoverPic()).centerCrop().into(cover);
                                }

                                if (type.equalsIgnoreCase("true")) {
                                    toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_on));
                                    toggleb = true;
                                } else {
                                    toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_off));
                                    toggleb = false;
                                }*/

                        try {
                            if (response != null) {
                                object = new JSONObject(response).getJSONObject("success");

                                if (!object.isNull("prefer_location")) {
                                    prefLocation = object.getString("prefer_location");
                                }

                                nameValue = object.getString("name");
                                if (nameValue.length() > 15) {
                                    s1 = nameValue.substring(0, 15) + "....";
                                } else {
                                    s1 = nameValue;
                                }

                                userBio = object.getString("bio");
                                profile_pic = object.getString("profile_pic");
                                cover_pic = object.getString("cover_pic");
                                isSocial = object.getBoolean("is_social") + "";
                                type = object.getBoolean("is_private") + "";
                                followersCount = object.getInt("followers_count") + "";
                                followingCount = object.getInt("following_count") + "";
                                gameCount = object.getInt("games_count") + "";
                                mine = object.getBoolean("is_mine") + "";
                                isFollowing = object.getBoolean("is_following") + "";
                                isFollower = object.getBoolean("is_follower") + "";
                                isRequested = object.getBoolean("is_requested") + "";
                                isConfirm = object.getBoolean("is_confirm") + "";
                                levelId = object.getInt("level_id");
//                                mobile = object.getString("mobile");

                                level.setText("L" + levelId);

                                Log.e("userName", nameValue);
                                Log.e("cover_pic", cover_pic);
                                Log.e("userImage", profile_pic);
                                Log.e("profileType", type);
                                Log.e("followersCount", followersCount);
                                Log.e("followingCount", followingCount);
                                Log.e("gameCount", gameCount);
                                Log.e("mine", mine);
                                Log.e("isFollowing", isFollowing);
                                Log.e("isFollower", isFollower);
                                Log.e("isRequested", isRequested);
                                Log.e("isConfirm", isConfirm);
                                Log.e("prefLocation", prefLocation);
//                                Log.e("mobile", mobile);


//                                if (mine.equalsIgnoreCase("true")) {
//                                    mPref.setName(nameValue);
//                                    mPref.setBio(userBio);
//                                    mPref.setProfilePic(profile_pic);
//                                    mPref.setCoverPic(cover_pic);
//                                }

                                //playerName.setText(nameValue);
                                playerName.setText(nameValue.substring(0, 1).toUpperCase() + nameValue.substring(1));

                                if (!userBio.isEmpty() && !userBio.trim().equalsIgnoreCase(""))
                                    bio.setText(userBio);
                                followersNum.setText(followersCount);
                                followingNum.setText(followingCount);

//                                if (!mobile.isEmpty() && !mobile.trim().equalsIgnoreCase("")) {
//                                    mobileNumber.setText(mobile);
//                                    mobileNumber.setVisibility(View.VISIBLE);
//                                } else {
//                                    mobileNumber.setVisibility(View.INVISIBLE);
//                                }

                                if (object.getInt("games_count") > 1)
                                    txtGamesPlayed.setText("Games played");
                                else txtGamesPlayed.setText("Game played");

                                gamesPlayed.setText(gameCount);

                                if (prefLocation.isEmpty()) {
                                    locatinNames.setText("Not Available");
                                } else {
                                    locatinNames.setText(prefLocation);
                                }

                                if (mine.equalsIgnoreCase("true")) {
                                    requestGame.setVisibility(View.GONE);
                                    btnEdit.setText("Edit");
                                    btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                    daysText.setText("Play with " + s1 + " on these days");
                                    chatIcon.setVisibility(View.GONE);
                                    mProfile.setVisibility(View.GONE);
                                    myProfile.setVisibility(View.VISIBLE);
                                } else {

                                    if (type.equalsIgnoreCase("false")) {

                                        myProfile.setVisibility(View.VISIBLE);
                                        mProfile.setVisibility(View.GONE);
                                        chatIcon.setVisibility(View.VISIBLE);
                                        cover.setClickable(true);
                                        followersLayout.setClickable(true);
                                        followingLayout.setClickable(true);
                                        gameRelative.setClickable(true);
                                        level.setClickable(true);
                                        requestGame.setVisibility(View.VISIBLE);

                                        if (isFollowing.equalsIgnoreCase("true")) {
                                            btnEdit.setText("Following");
                                            btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                        } else {
                                            if (isConfirm.equalsIgnoreCase("true")) {
                                                btnEdit.setText("Confirm");
                                                btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                            } else {
                                                btnEdit.setText("Follow");
                                                btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                            }

                                        }


                                    } else {
                                        myProfile.setVisibility(View.VISIBLE);
                                        mProfile.setVisibility(View.GONE);
                                        chatIcon.setVisibility(View.VISIBLE);
                                        cover.setClickable(true);
                                        followersLayout.setClickable(true);
                                        followingLayout.setClickable(true);
                                        gameRelative.setClickable(true);
                                        level.setClickable(true);
                                        requestGame.setVisibility(View.VISIBLE);
                                        if (isConfirm.equalsIgnoreCase("true")) {
                                            btnEdit.setText("Confirm");
                                            btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                            if (isFollower.equalsIgnoreCase("false")) {
                                                myProfile.setVisibility(View.GONE);
                                                mProfile.setVisibility(View.VISIBLE);
                                                requestGame.setVisibility(View.GONE);
                                                chatIcon.setVisibility(View.GONE);
                                                cover.setClickable(false);
                                                followersLayout.setClickable(false);
                                                followingLayout.setClickable(false);
                                                gameRelative.setClickable(false);
                                                level.setClickable(false);
                                            }

                                        } else if (isFollowing.equalsIgnoreCase("true")) {

                                            btnEdit.setText("Following");
                                            btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                                   /*  if (isFollower.equalsIgnoreCase("false")){
                                                         myProfile.setVisibility(View.GONE);
                                                         mProfile.setVisibility(View.VISIBLE);
                                                         requestGame.setVisibility(View.GONE);
                                                         chatIcon.setVisibility(View.GONE);
                                                         cover.setClickable(false);
                                                         followersLayout.setClickable(false);
                                                         followingLayout.setClickable(false);
                                                         gameRelative.setClickable(false);
                                                         level.setClickable(false);

                                                     }*/

                                        } else if (isRequested.equalsIgnoreCase("true")) {
                                            btnEdit.setText("Requested");
                                            btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.shouted_back));
                                            if (isFollower.equalsIgnoreCase("false")) {
                                                myProfile.setVisibility(View.GONE);
                                                mProfile.setVisibility(View.VISIBLE);
                                                requestGame.setVisibility(View.GONE);
                                                chatIcon.setVisibility(View.GONE);
                                                cover.setClickable(false);
                                                followersLayout.setClickable(false);
                                                followingLayout.setClickable(false);
                                                gameRelative.setClickable(false);
                                                level.setClickable(false);
                                            }

                                        } else {
                                            btnEdit.setText("Follow");
                                            btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_back));
                                            if (isFollower.equalsIgnoreCase("false")) {
                                                myProfile.setVisibility(View.GONE);
                                                mProfile.setVisibility(View.VISIBLE);
                                                requestGame.setVisibility(View.GONE);
                                                chatIcon.setVisibility(View.GONE);
                                                cover.setClickable(false);
                                                followersLayout.setClickable(false);
                                                followingLayout.setClickable(false);
                                                gameRelative.setClickable(false);
                                                level.setClickable(false);
                                            }

                                        }

                                    }
                                    requestGame.setText("REQUEST " + s1 + " FOR A GAME");
                                    privateProfile.setVisibility(View.GONE);
                                    privateProfileLay.setVisibility(View.INVISIBLE);
                                    daysText.setText("Play with " + s1 + " on these days");
                                    chatIcon.setBackground(getResources().getDrawable(R.drawable.ic_chat_game_white_icon));

                                }

                                try {
                                    Glide.with(PlayerProfileViewActivity.this).load(profile_pic).error(R.drawable.circled_user).centerCrop().into(circleImageView);
                                } catch (Exception e) {
                                    circleImageView.setImageResource(R.drawable.circled_user);
                                }

                                try {
                                    Glide.with(PlayerProfileViewActivity.this).load(cover_pic).error(R.drawable.playthora_doodle).centerCrop().into(cover);
                                } catch (Exception e) {
                                    cover.setImageResource(R.drawable.playthora_doodle);
                                }

                                if (type.equalsIgnoreCase("true")) {
                                    toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_on));
                                    toggleb = true;
                                } else {
                                    toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_off));
                                    toggleb = false;
                                }


                                if (!object.isNull("available_at")) {
                                    JSONObject available = object.getJSONObject("available_at");
                                    fTime = available.getString("available_from");
                                    tTime = available.getString("available_to");

                                    Log.e("fromTime", fTime + "");
                                    Log.e("toTime", tTime + "");

                                    if (fTime.isEmpty() || tTime.isEmpty()
                                            || fTime.equals("") || tTime.equals("")) {
                                        timeTextValue.setText("Not Available");
                                    } else {
                                        timeTextValue.setText(fTime + " to " + tTime);
                                    }

                                    if (available.has("days") && !available.isNull("days")) {
                                        days = available.getJSONArray("days");
                                        daysValue = days.toString();
                                        for (int i = 0; i < days.length(); i++) {
                                            Log.e("days######", String.valueOf(days.get(i)));
                                            if (days.get(i).equals("monday")) {
                                                mon = true;
                                                monday.setTextColor(Color.parseColor("#FFFFFF"));
                                                monday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back));

                                            }
                                            if (days.get(i).equals("tuesday")) {
                                                tue = true;
                                                tuesday.setTextColor(Color.parseColor("#FFFFFF"));
                                                tuesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back));
                                            }

                                            if (days.get(i).equals("wednesday")) {
                                                wed = true;
                                                wednesday.setTextColor(Color.parseColor("#FFFFFF"));
                                                wednesday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_three));
                                            }

                                            if (days.get(i).equals("thursday")) {
                                                thu = true;
                                                thursday.setTextColor(Color.parseColor("#FFFFFF"));
                                                thursday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_three));
                                            }
                                            if (days.get(i).equals("friday")) {
                                                fri = true;
                                                friday.setTextColor(Color.parseColor("#FFFFFF"));
                                                friday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
                                            }
                                            if (days.get(i).equals("saturday")) {
                                                sat = true;
                                                saturday.setTextColor(Color.parseColor("#FFFFFF"));
                                                saturday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
                                            }
                                            if (days.get(i).equals("sunday")) {
                                                sun = true;
                                                sunday.setTextColor(Color.parseColor("#FFFFFF"));
                                                sunday.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_full_back_five));
                                            }
                                        }
                                    }
                                } else {
                                    timeTextValue.setText("Not Available");
                                }


                                if (object.has("primary_team") && !object.isNull("primary_team")) {
                                    JSONObject primaryTeam = object.getJSONObject("primary_team");
                                    Log.e("primaryTeam", primaryTeam.toString());
                                    primaryResponse = primaryTeam.toString();
                                    pId = String.valueOf(primaryTeam.getInt("id"));
                                    pName = primaryTeam.getString("name");
                                    pCoverPic = primaryTeam.getString("cover_pic");
                                    pMembers = String.valueOf(primaryTeam.getInt("members_count"));
                                    pFollowers = String.valueOf(primaryTeam.getInt("followers_count"));

                                    Log.e("pName", pName + "");
                                    Log.e("pCoverPic", pCoverPic + "");
                                    Log.e("pMembers", pMembers + "");
                                    Log.e("pFollowers", pFollowers + "");
                                    primRelative.setVisibility(View.VISIBLE);
                                    noPrimaryTeam.setVisibility(View.GONE);
                                    members.setText("members");
                                    followers.setText("followers");
                                    seperateView.setVisibility(View.VISIBLE);
                                    searchName.setText(pName);
                                    primaryTeamMembers.setText(pMembers);
                                    primaryTeamFollowers.setText(pFollowers);

                                    try {
                                        Glide.with(getApplicationContext()).load(pCoverPic).error(R.drawable.circled_user).centerCrop().into(primaryTeamImage);
                                    } catch (Exception e) {
                                        primaryTeamImage.setImageResource(R.drawable.circled_user);
                                    }
                                } else {
                                    noPrimaryTeam.setVisibility(View.VISIBLE);
                                    primRelative.setVisibility(View.GONE);
                                    noPrimaryTeam.setText("No primary team");
                                }

                                if (object.getJSONArray("fav_sports").length() != 0) {
                                    JSONArray favSportArray = object.getJSONArray("fav_sports");
                                    Log.e("favSports", favSportArray.toString());
                                    favSportsResponse = favSportArray.toString();
                                }


                                if (object.getJSONArray("teams").length() != 0) {
                                    JSONArray jsonArray = object.getJSONArray("teams");
                                    Log.e("AssociateTeams", jsonArray.toString());
                                    teamsResponse = jsonArray.toString();
                                }

                                if (object.getJSONArray("top_sports").length() != 0) {
                                    JSONArray topSportsArray = object.getJSONArray("top_sports");
                                    Log.e("topSports", topSportsArray.toString());
                                    topSportsResponse = topSportsArray.toString();
                                }


                                adapter = new MyPagerAdapter(getSupportFragmentManager());
                                pager.setAdapter(adapter);
                                pager.setPagingEnabled(false);
                                topgameSelected.setVisibility(View.VISIBLE);
                                topgame.setVisibility(View.GONE);
                                associtedSelected.setVisibility(View.GONE);
                                associted.setVisibility(View.VISIBLE);
                                favGame.setVisibility(View.VISIBLE);
                                favGameSelected.setVisibility(View.GONE);
                                pager.setCurrentItem(1);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers,
                                      byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlayerProfileViewActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(PlayerProfileViewActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(PlayerProfileViewActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }


    private void initializeAll() {
        mPref = PreferenceManager.instance(getApplicationContext());
        followersLayout = (RelativeLayout) findViewById(R.id.followersLayout);
        gameRelative = (RelativeLayout) findViewById(R.id.num_games_played_layout);
        myProfile = (RelativeLayout) findViewById(R.id.profile);
        followingLayout = (RelativeLayout) findViewById(R.id.followingLayout);
        cover = (ImageView) findViewById(R.id.plyer_banner);
        circleImageView = (CircleImageView) findViewById(R.id.imageView_player);
        toolbar = (Toolbar) findViewById(R.id.toolbar_cart);
        title = (TextView) findViewById(R.id.toolbar_title);
        associted = (TextView) findViewById(R.id.asso_teams);
        associtedSelected = (TextView) findViewById(R.id.asso_teams_elected);
        topgame = (TextView) findViewById(R.id.top_game);
        topgameSelected = (TextView) findViewById(R.id.top_game_selected);
        favGame = (TextView) findViewById(R.id.game_fav);
        favGameSelected = (TextView) findViewById(R.id.game_fav_selected);
        scrollview = (NestedScrollView) findViewById(R.id.nested_profile);
        toggle = (ImageView) findViewById(R.id.toggle_private);
        monday = (TextView) findViewById(R.id.monday);
        tuesday = (TextView) findViewById(R.id.tuesday);
        wednesday = (TextView) findViewById(R.id.wednsday);
        thursday = (TextView) findViewById(R.id.thursday);
        friday = (TextView) findViewById(R.id.friday);
        saturday = (TextView) findViewById(R.id.saturday);
        sunday = (TextView) findViewById(R.id.sunday);
        playerName = (TextView) findViewById(R.id.name_player);
        level = (TextView) findViewById(R.id.level_text);
        followersNum = (TextView) findViewById(R.id.no_follower);
        followingNum = (TextView) findViewById(R.id.no_following);
        txtFollowers = (TextView) findViewById(R.id.followers);
        txtFollowing = (TextView) findViewById(R.id.following);
        txtGamesPlayed = (TextView) findViewById(R.id.no_gamesplayed);
        gamesPlayed = (TextView) findViewById(R.id.numberOfGamesPlayed);
        btnEdit = (Button) findViewById(R.id.follow_btn);
        bio = (TextView) findViewById(R.id.player_bio);
        primaryText = (TextView) findViewById(R.id.primary_text);
        searchName = (TextView) findViewById(R.id.search_name);
        primaryTeamMembers = (TextView) findViewById(R.id.primaryTeamMembers);
        members = (TextView) findViewById(R.id.typename);
        primaryTeamFollowers = (TextView) findViewById(R.id.primaryTeamFollowers);
        followers = (TextView) findViewById(R.id.description);
        timeTextValue = (TextView) findViewById(R.id.time_text_value);
        locatinNames = (TextView) findViewById(R.id.location_names);
        mProfile = (TextView) findViewById(R.id.profile_private);
//        mobileNumber = (TextView) findViewById(R.id.mobile_number);

        daysText = (TextView) findViewById(R.id.player_time_text);
        privateProfile = (TextView) findViewById(R.id.private_profile);
        privateProfileSub = (TextView) findViewById(R.id.private_profile_text);
        privateProfileLay = (RelativeLayout) findViewById(R.id.private_profile_lay);
        requestGame = (Button) findViewById(R.id.request_game);
        pager = (CustomViewPager) findViewById(R.id.fragment);
        primaryTeamImage = (CircleImageView) findViewById(R.id.imageView_player_search);
        noPrimaryTeam = (TextView) findViewById(R.id.noPrimaryTeam);
        primRelative = (RelativeLayout) findViewById(R.id.primaryLayout);
        seperateView = (View) findViewById(R.id.sepratot_id);
        chatIcon = (ImageView) findViewById(R.id.chat_icon);

        requestGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        playerName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        followersNum.setTypeface(Typeface.DEFAULT_BOLD);
        followingNum.setTypeface(Typeface.DEFAULT_BOLD);
        txtFollowers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        txtFollowing.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        txtGamesPlayed.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        gamesPlayed.setTypeface(Typeface.DEFAULT);
        btnEdit.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bio.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        primaryText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        searchName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        primaryTeamMembers.setTypeface(Typeface.DEFAULT);
        primaryTeamFollowers.setTypeface(Typeface.DEFAULT);
        members.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        followers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        daysText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        privateProfile.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        privateProfileSub.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        locatinNames.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noPrimaryTeam.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        timeTextValue.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mProfile.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
//        mobileNumber.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    private void setupListeners() {
        associted.setOnClickListener(this);
        associtedSelected.setOnClickListener(this);
        topgame.setOnClickListener(this);
        topgameSelected.setOnClickListener(this);
        favGame.setOnClickListener(this);
        favGameSelected.setOnClickListener(this);

        monday.setOnClickListener(this);
        tuesday.setOnClickListener(this);
        thursday.setOnClickListener(this);
        friday.setOnClickListener(this);
        saturday.setOnClickListener(this);
        sunday.setOnClickListener(this);
        wednesday.setOnClickListener(this);
        toggle.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        requestGame.setOnClickListener(this);
        //txtFollowers.setOnClickListener(this);
        //txtFollowing.setOnClickListener(this);
        followersLayout.setOnClickListener(this);
        followingLayout.setOnClickListener(this);
        level.setOnClickListener(this);
        cover.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollview.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {
                        title.setVisibility(View.VISIBLE);
                        toolbar.setBackground(getResources().getDrawable(R.drawable.whitw_back));
                        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
                        chatIcon.setBackground(getResources().getDrawable(R.drawable.ic_chat_game_icon));
                    }

                    if (scrollY == 0) {
                        title.setVisibility(View.GONE);
                        toolbar.setBackgroundColor(Color.parseColor("#00000000"));
                        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
                        chatIcon.setBackground(getResources().getDrawable(R.drawable.ic_chat_game_white_icon));

                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.asso_teams:
                associtedSelected.setVisibility(View.VISIBLE);
                associted.setVisibility(View.GONE);
                topgame.setVisibility(View.VISIBLE);
                topgameSelected.setVisibility(View.GONE);
                favGameSelected.setVisibility(View.GONE);
                favGame.setVisibility(View.VISIBLE);
                pager.setCurrentItem(0);
                break;

            case R.id.top_game:
                topgameSelected.setVisibility(View.VISIBLE);
                topgame.setVisibility(View.GONE);
                associtedSelected.setVisibility(View.GONE);
                associted.setVisibility(View.VISIBLE);
                favGame.setVisibility(View.VISIBLE);
                favGameSelected.setVisibility(View.GONE);
                pager.setCurrentItem(1);
                break;

            case R.id.game_fav:
                favGameSelected.setVisibility(View.VISIBLE);
                favGame.setVisibility(View.GONE);
                associtedSelected.setVisibility(View.GONE);
                topgameSelected.setVisibility(View.GONE);
                associted.setVisibility(View.VISIBLE);
                topgame.setVisibility(View.VISIBLE);
                pager.setCurrentItem(2);
                break;

            case R.id.monday:
                if (mon) {
                    // timeTextValue.setText("3PM to 9PM");
                    //  locatinNames.setText("Btm jaynagar");
                } else {
                    //   timeTextValue.setText("Not Available");
                    //locatinNames.setText("None");
                }
                break;

            case R.id.tuesday:

                if (tue) {
                    //timeTextValue.setText("8PM to 9PM");
                    //  locatinNames.setText("HSR Marthalli");
                } else {
                    //  timeTextValue.setText("Not Available");
                    //  locatinNames.setText("None");
                }
                break;

            case R.id.wednsday:

                if (wed) {
                    //  timeTextValue.setText("8PM to 9PM");
                    //  locatinNames.setText("HSR Marthalli");
                } else {
                    //  timeTextValue.setText("Not Available");
                    //   locatinNames.setText("None");
                }


                break;
            case R.id.thursday:

                if (thu) {
                    // timeTextValue.setText("8PM to 9PM");
                    //  locatinNames.setText("HSR Marthalli");
                } else {
                    //  timeTextValue.setText("Not Available");
                    //  locatinNames.setText("None");
                }


                break;
            case R.id.friday:

                if (fri) {
                    // timeTextValue.setText("12PM to 2AM");
                    // locatinNames.setText("Banshankari jaynagar");
                } else {
                    // timeTextValue.setText("Not Available");
                    // locatinNames.setText("None");
                }


                break;
            case R.id.sunday:

                if (sun) {
                    // timeTextValue.setText("8PM to 9PM");
                    //locatinNames.setText("HSR Marthalli");
                } else {
                    // timeTextValue.setText("Not Available");
                    //  locatinNames.setText("None");
                }


                break;
            case R.id.saturday:

                if (sat) {
                    // timeTextValue.setText("12PM to 2AM");
                    // locatinNames.setText("Banshankari jaynagar");
                } else {
                    // timeTextValue.setText("Not Available");
                    // locatinNames.setText("None");
                }


                break;
            case R.id.toggle_private:

                // update(toggleb);

                toggleb = !toggleb;
                update(toggleb);
                break;

            case R.id.follow_btn:
                if (toast != null) {
                    toast.cancel();
                }
                if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                    if (mPref.getAccessToken().isEmpty()) {
                        PlaythoraUtility.LoginDialog(PlayerProfileViewActivity.this);
                    } else {
                        if (mine.equalsIgnoreCase("true")) {
                            Intent j = new Intent(PlayerProfileViewActivity.this, PlayerProfileEditActivity.class);
                            j.putExtra("primaryTeam", primaryResponse);
                            j.putExtra("favSports", favSportsResponse);
                            j.putExtra("AssociateTeams", teamsResponse);
                            j.putExtra("from", fTime + "");
                            j.putExtra("to", tTime + "");
                            j.putExtra("days", daysValue);
                            j.putExtra("location", prefLocation);
                            startActivity(j);
                        } else {

                            if (btnEdit.getText().toString().equalsIgnoreCase("following")) {
                                unFollowPopUp();
                            } else if (btnEdit.getText().toString().equalsIgnoreCase("requested")) {
                                rejectPopUp();
                            } else if (btnEdit.getText().toString().equalsIgnoreCase("confirm")) {
                                confirmPopUp();
                            } else {
                                follow();
                            }

                  /*  if(isFollowing.equalsIgnoreCase("true")){
                        btnEdit.setText("Following");

                    }else {
                        btnEdit.setText("Follow");

                    }*/

                        }
                    }
                }
                break;

            case R.id.request_game:
                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(PlayerProfileViewActivity.this);
                } else {
                    if (mine.equalsIgnoreCase("true")) {

                    } else {
                        Intent j = new Intent(PlayerProfileViewActivity.this, RequestPlayerActivity.class);
                        j.putExtra("ID", id);
                        startActivity(j);
                    }
                }
                break;
            case R.id.followingLayout:
                if (toast != null) {
                    toast.cancel();
                }
                if (PlaythoraUtility.checkInternetConnection(PlayerProfileViewActivity.this)) {
                    Intent following = new Intent(PlayerProfileViewActivity.this, FollowingActivityActivity.class);
                    following.putExtra("admin", mine);
                    following.putExtra("userId", id);
                    startActivity(following);
                }

                break;
            case R.id.followersLayout:
                if (toast != null) {
                    toast.cancel();
                }
                if (PlaythoraUtility.checkInternetConnection(PlayerProfileViewActivity.this)) {
                    Intent followers = new Intent(PlayerProfileViewActivity.this, FollowersListingActivity.class);
                    followers.putExtra("admin", mine);
                    followers.putExtra("userId", id);
                    startActivity(followers);
                }


                break;

            case R.id.level_text:
                if (toast != null) {
                    toast.cancel();
                }
                if (PlaythoraUtility.checkInternetConnection(PlayerProfileViewActivity.this)) {
                    Intent intent = new Intent(PlayerProfileViewActivity.this, ProfileBadges.class);
                    intent.putExtra("user_id", id);
                    intent.putExtra("name", nameValue);
                    intent.putExtra("admin", mine);
                    startActivity(intent);
                }

                break;

            case R.id.plyer_banner:
                if (toast != null) {
                    toast.cancel();
                }
                if (PlaythoraUtility.checkInternetConnection(PlayerProfileViewActivity.this)) {
                    Intent badge = new Intent(PlayerProfileViewActivity.this, ProfileBadges.class);
                    badge.putExtra("user_id", id);
                    badge.putExtra("name", nameValue);
                    badge.putExtra("admin", mine);
                    startActivity(badge);
                }

                break;

            default:
                break;

        }
    }

    private void rejectPopUp() {

        final Dialog dialog = new Dialog(PlayerProfileViewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reject_dialog_layout);
        TextView dialogTitle, dialogNo, dialogYes;

        dialogTitle = (TextView) dialog.findViewById(R.id.title);
        dialogNo = (TextView) dialog.findViewById(R.id.no);
        dialogYes = (TextView) dialog.findViewById(R.id.yes);

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogNo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogYes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancel();
            }
        });
        dialog.show();
    }

    private void cancel() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", id);
            httpClient.post(Config.PLAYER_CANCEL_REQUEST, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("cancelRequest", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("success"), Toast.LENGTH_SHORT).show();
                            getUser(id);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }

    }

    private void unFollowPopUp() {
        final Dialog dialog = new Dialog(PlayerProfileViewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.un_follow_dialog);
        TextView dialogTitle, dialogNo, dialogYes;

        dialogTitle = (TextView) dialog.findViewById(R.id.title);
        dialogNo = (TextView) dialog.findViewById(R.id.no);
        dialogYes = (TextView) dialog.findViewById(R.id.yes);

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogNo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogYes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                unFollow();
            }
        });
        dialog.show();
    }

    private void confirmPopUp() {

        final Dialog dialog = new Dialog(PlayerProfileViewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirm_dialog);
        TextView dialogTitle, dialogNo, dialogYes;

        dialogTitle = (TextView) dialog.findViewById(R.id.confirm_title);
        dialogNo = (TextView) dialog.findViewById(R.id.no);
        dialogYes = (TextView) dialog.findViewById(R.id.yes);

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogNo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogYes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancelRequest();
            }
        });

        dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                confirmRequest();
            }
        });
        dialog.show();

    }

    private void cancelRequest() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", id);
            httpClient.post(Config.PLAYER_REJECT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("cancelRequestSuccess", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("success"), Toast.LENGTH_SHORT).show();
                          /*  btnEdit.setText("Follow");

                            myProfile.setVisibility(View.GONE);
                            mProfile.setVisibility(View.VISIBLE);
                            requestGame.setVisibility(View.GONE);
                            chatIcon.setVisibility(View.GONE);
                            cover.setClickable(false);
                            followersLayout.setClickable(false);
                            followingLayout.setClickable(false);
                            gameRelative.setClickable(false);
                            level.setClickable(false);*/
                            getUser(id);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    private void confirmRequest() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", id);
            httpClient.post(Config.PLAYER_ACCEPT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("confirmRequestSuccess", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("success"), Toast.LENGTH_SHORT).show();

                           /* btnEdit.setText("Following");



                            myProfile.setVisibility(View.VISIBLE);
                            mProfile.setVisibility(View.GONE);
                            chatIcon.setVisibility(View.VISIBLE);
                            cover.setClickable(true);
                            followersLayout.setClickable(true);
                            followingLayout.setClickable(true);
                            gameRelative.setClickable(true);
                            requestGame.setVisibility(View.VISIBLE);
                            level.setClickable(true);*/

                            getUser(id);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    private void update(Boolean toggleb) {
        Log.e("isPrivateUpdate", String.valueOf(toggleb));
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("is_private", String.valueOf(toggleb));
            httpClient.post(Config.EDIT_PROFILE, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("updatProfile", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statuscode") == 200) {
                            String s1;
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            s1 = String.valueOf(jsonObject.getJSONObject("success").getBoolean("is_private"));
                            if (s1.equalsIgnoreCase("true")) {
                                toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_on));
                            } else {
                                toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_off));
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlayerProfileViewActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(PlayerProfileViewActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(PlayerProfileViewActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }


    private void follow() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", id);
            httpClient.post(Config.PLAYER_FOLLOW, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("follow", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("success"), Toast.LENGTH_SHORT).show();
                          /*  btnEdit.setText("Requested");
                            btnEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.shouted_back));

                            myProfile.setVisibility(View.GONE);
                            mProfile.setVisibility(View.VISIBLE);
                            requestGame.setVisibility(View.GONE);
                            chatIcon.setVisibility(View.GONE);
                            cover.setClickable(false);
                            followersLayout.setClickable(false);
                            followingLayout.setClickable(false);
                            gameRelative.setClickable(false);
                            level.setClickable(false);*/

                            getUser(id);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    private void unFollow() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PlayerProfileViewActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", id);
            httpClient.post(Config.PLAYER_UNFOLLOW, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("unFollow", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(PlayerProfileViewActivity.this, jsonObject.getString("success"), Toast.LENGTH_SHORT).show();
                          /*  btnEdit.setText("Follow");

                            myProfile.setVisibility(View.GONE);
                            mProfile.setVisibility(View.VISIBLE);
                            requestGame.setVisibility(View.GONE);
                            chatIcon.setVisibility(View.GONE);
                            cover.setClickable(false);
                            followersLayout.setClickable(false);
                            followingLayout.setClickable(false);
                            gameRelative.setClickable(false);
                            level.setClickable(false);*/
                            getUser(id);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        if (requestCode == 2) {
            Log.e("%%%%%%%%%%%%", requestCode + "");
            getUser(id);
        }
        if (requestCode == 3) {

        }
    }

    @Override
    public void onBackPressed() {
        if (isNotificationTriggered) {
            Intent homeIntent = new Intent(PlayerProfileViewActivity.this, HomeScreenActivity.class);
            startActivity(homeIntent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.chat_main, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.chat_notification){
            if(!admin.equalsIgnoreCase("true")){
                Intent toChat = new Intent(PlayerProfileViewActivity.this,ChattingActivity.class);
                toChat.putExtra("name",nameValue);
                startActivity(toChat);
            }

        }
        return super.onOptionsItemSelected(item);
    }*/

    /*   @Override
    public void onBackPressed() {
        super.onBackPressed();


        if(mPref.getAccessToken().isEmpty()){
            onBackPressed();
        }else {
            Intent intent = new Intent(PlayerProfileViewActivity.this,HomeScreenActivity.class);
            startActivity(intent);
        }

       *//* if(search.equalsIgnoreCase("SEARCH")){
            onBackPressed();
        }else {
            Intent intent = new Intent(PlayerProfileViewActivity.this,HomeScreenActivity.class);
            startActivity(intent);
        }*//*
    }*/

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Everyone", "Following", "Team"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
//                    return new PlayerProfileTeamView();
                    PlayerProfileTeamView playerProfileTeamView = new PlayerProfileTeamView();
                    Bundle bundle = new Bundle();
                    bundle.putString("teams", teamsResponse);
                    playerProfileTeamView.setArguments(bundle);
                    return playerProfileTeamView;

                case 1:
                    PlayerProfileTopView playerProfileTopView = new PlayerProfileTopView();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("topSports", topSportsResponse);
                    playerProfileTopView.setArguments(bundle1);
                    return playerProfileTopView;

                case 2:
                    PlayerProfileFavGameView playerProfileFavGameView = new PlayerProfileFavGameView();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("favSports", favSportsResponse);
                    playerProfileFavGameView.setArguments(bundle2);
                    return playerProfileFavGameView;

                default:
                    PlayerProfileTopView playerProfileTopView1 = new PlayerProfileTopView();
                    Bundle bundle11 = new Bundle();
                    bundle11.putString("topSports", topSportsResponse);
                    playerProfileTopView1.setArguments(bundle11);
                    return playerProfileTopView1;
            }

        }
    }
}
