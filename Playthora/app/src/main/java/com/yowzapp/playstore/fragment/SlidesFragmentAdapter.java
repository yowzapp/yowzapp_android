package com.yowzapp.playstore.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by vaishakha on 25/7/16.
 */
public class SlidesFragmentAdapter extends FragmentPagerAdapter {

    private int mCount = 4;


    public SlidesFragmentAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        Fragment currentFragment = new FragmentOne();

        switch (position) {
            case 0:
                currentFragment = new FragmentOne();
                break;
            case 1:
                currentFragment = new FragmentTwo();
                break;
            case 2:
                currentFragment = new FragmentThree();
                break;
            case 3:
                currentFragment = new FragmentFour();

            default:
                break;
        }
        return currentFragment;
    }

    @Override
    public int getCount() {
        return mCount;
    }
}