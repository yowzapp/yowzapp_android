package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 4/1/17.
 */
public class GameDeatailInvitedMembers {

    private int id;
    private String name;
    private String profile_pic;
    private PivotModel pivot;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public PivotModel getPivot() {
        return pivot;
    }

    public void setPivot(PivotModel pivot) {
        this.pivot = pivot;
    }
}
