package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CartListModel;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 10/11/16.
 */
public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ProfileListHolder> {
    Context mContext;


    ArrayList<CartListModel> productList;

    public CartListAdapter(Context context, ArrayList<CartListModel> arrayList) {
        mContext = context;
        productList = arrayList;
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_list_layout, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, int position) {

        Glide.with(mContext).load(productList.get(position).getImage()).into(holder.productImage);
        holder.productName.setText(productList.get(position).getProduct());
        holder.numXp.setText(productList.get(position).getRewardXp() + " XP");


        holder.deleteIcon.setOnClickListener(new MyOnClickListener(position, holder));
        holder.addProduct.setOnClickListener(new MyOnClickListener(position, holder));
        holder.removeProduct.setOnClickListener(new MyOnClickListener(position, holder));

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView deleteIcon, productImage, imgEditIcon;
        TextView productName, numXp, quantity, txtEditLabel, addProduct, removeProduct;
        RelativeLayout lytEditDeleteWrapper, alreadyExchanged;

        public ProfileListHolder(View itemView) {
            super(itemView);

            deleteIcon = (ImageView) itemView.findViewById(R.id.orderVIewItemsdelete);
            productImage = (ImageView) itemView.findViewById(R.id.shoppingCartItemsImage);
            productName = (TextView) itemView.findViewById(R.id.shoppingCartItems);
            numXp = (TextView) itemView.findViewById(R.id.shoppingCartItemsNo);
            quantity = (TextView) itemView.findViewById(R.id.shoppingCartQuantity);


            addProduct = (TextView) itemView.findViewById(R.id.shoppingCartAddQuantity);
            removeProduct = (TextView) itemView.findViewById(R.id.shoppingCartRemoveQuantity);
            productName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
            numXp.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));


        }
    }

    private class MyOnClickListener implements View.OnClickListener {

        int pos;
        ProfileListHolder holder;

        public MyOnClickListener(int position, ProfileListHolder holder) {
            pos = position;
            this.holder = holder;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.shoppingCartAddQuantity:

                    int count = Integer.parseInt(holder.quantity.getText().toString());
                    int total = count + 1;
                    holder.quantity.setText("" + total);

                    break;

                case R.id.shoppingCartRemoveQuantity:

                    int countre = Integer.parseInt(holder.quantity.getText().toString());
                    if (countre > 1) {
                        int totalre = countre - 1;
                        holder.quantity.setText("" + totalre);
                    } else {
                        Toast.makeText(mContext, "Delete the product if you dont want", Toast.LENGTH_SHORT);
                    }


                    break;


            }
        }
    }

}

