package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 4/1/17.
 */
public class CreateGameMembersInvited {

    private int id;
    private String name;
    private String profile_pic;
    private boolean is_private;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public boolean is_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }
}
