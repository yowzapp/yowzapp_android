package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 7/1/17.
 */
public class PlayerDetailSuccess {

    private int id;
    private String name;
    private String bio;
    private String profile_pic;
    private String cover_pic;
    private boolean is_private;
    private boolean is_social;
    private String email;
    private String mobile;
    private int points;
    private String location_name;
    private int notification_count;
    private int followers_count;
    private int following_count;
    private int games_count;
    private PlayerDetailSuccessPrimaryTeam primary_team;
    private List<PlayerDetailSuccessFavSports> fav_sports;
    private List<PlayerDetailSuccessTopSports> top_sports;
    private List<PlayerDetailSuccessAssTeams> teams;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public boolean is_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }

    public boolean is_social() {
        return is_social;
    }

    public void setIs_social(boolean is_social) {
        this.is_social = is_social;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<PlayerDetailSuccessAssTeams> getTeams() {
        return teams;
    }

    public void setTeams(List<PlayerDetailSuccessAssTeams> teams) {
        this.teams = teams;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public int getNotification_count() {
        return notification_count;
    }

    public void setNotification_count(int notification_count) {
        this.notification_count = notification_count;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public int getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(int following_count) {
        this.following_count = following_count;
    }

    public int getGames_count() {
        return games_count;
    }

    public void setGames_count(int games_count) {
        this.games_count = games_count;
    }

    public List<PlayerDetailSuccessFavSports> getFav_sports() {
        return fav_sports;
    }

    public void setFav_sports(List<PlayerDetailSuccessFavSports> fav_sports) {
        this.fav_sports = fav_sports;
    }

    public List<PlayerDetailSuccessTopSports> getTop_sports() {
        return top_sports;
    }

    public void setTop_sports(List<PlayerDetailSuccessTopSports> top_sports) {
        this.top_sports = top_sports;
    }

    public PlayerDetailSuccessPrimaryTeam getPrimary_team() {
        return primary_team;
    }

    public void setPrimary_team(PlayerDetailSuccessPrimaryTeam primary_team) {
        this.primary_team = primary_team;
    }
}
