package com.yowzapp.playstore.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 25/11/16.
 */

public class NetBankingFragment extends Fragment {


    EditText nameOnCard, cardNumber, expiry, cvv;
    TextInputLayout nameLayout, cardLayout, expiryLayout, cvvLayout;

    public NetBankingFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.net_banking_layout, container, false);

        nameOnCard = (EditText) contentView.findViewById(R.id.name_on_card);
        cardNumber = (EditText) contentView.findViewById(R.id.cardNumber);
        expiry = (EditText) contentView.findViewById(R.id.expiry);
        cvv = (EditText) contentView.findViewById(R.id.cvv);

        nameLayout = (TextInputLayout) contentView.findViewById(R.id.input_layout_name);
        cardLayout = (TextInputLayout) contentView.findViewById(R.id.input_layout_pin);
        expiryLayout = (TextInputLayout) contentView.findViewById(R.id.input_layout_street);
        cvvLayout = (TextInputLayout) contentView.findViewById(R.id.input_layout_area);

        nameLayout.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        cardLayout.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        expiryLayout.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        cvvLayout.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));


        return contentView;
    }
}