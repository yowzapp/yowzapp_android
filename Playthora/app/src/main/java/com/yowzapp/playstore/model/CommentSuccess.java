package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by vaishakha on 10/1/17.
 */
public class CommentSuccess {
    private List<CommentList> data;

    public List<CommentList> getData() {
        return data;
    }

    public void setData(List<CommentList> data) {
        this.data = data;
    }
}
