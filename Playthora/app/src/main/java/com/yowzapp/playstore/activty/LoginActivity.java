package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Nakul on 7/25/2016.
 */
public class LoginActivity extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final List<String> READ_PERMISSIONS = Arrays.asList("email");
    private static final int RC_SIGN_IN = 9003;
    private static final String TAG = "LoginActivity";
    public static LoginActivity loginActivity;
    private static CallbackManager callbackManager;
    private static GoogleApiClient mGoogleApiClient;
    TextInputLayout emailBoxInputLayout, passwordBoxInputLayout;
    TextView forgotTxt, loginSubText, or, loginText;
    EditText emailBox, passwordBox;
    ImageView login, eye;
    TextView toolbarTitle;
    boolean isVisible = false;
    Button next;
    RelativeLayout facebookLogin, googleLogin;
    // public   static  SignupLoginActivity signupLoginActivity;
    Context mContext;
    PreferenceManager mPref;
    int id;
    String name, accessToken, mobile_verified, mobile, email;
    String profile_pic_update, interest_update, location_update;
    String authCode, id_token, mail, img, googleId, googleName, device_id, fcm_token, msg;
    String fname, facebookId, facebbookEmail, isSocial, userName, profile_pic, cover_pic, type, emailValue;
    Uri personPhoto;
    int user_id;
    String latitudeValue, longitudeValue;
    String accToken, userEmail, userMobileNumber, googleImage;
    String profilePicUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_layout);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        initializeAllComponents();
        callbackManager = CallbackManager.Factory.create();
        mContext = LoginActivity.this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (mPref.getAndroid_id() == null) {
            device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            Log.e("ANDROID_ID", device_id);
            mPref.setAndroid_id(device_id);
        }


        if (mPref.getGcmToken() == null) {
            fcm_token = FirebaseInstanceId.getInstance().getToken();
            mPref.setGcmToken(fcm_token);
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkForAllValidation()) {
                    loginUser();
                }
            }
        });

        passwordBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (checkForAllValidation()) {
                        loginUser();
                    }
                }
                return false;
            }
        });


        forgotTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgot = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(forgot);
            }
        });

        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // signupLoginActivity.FacebookLogin(mContext);
                Log.e("fb", "facebook");
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, READ_PERMISSIONS);
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        accessToken = loginResult.getAccessToken().getToken();
                        Log.v("ACCESSTOKEN", accessToken);


                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),

                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        response.getError();
                                        Log.e("fbResponse", object.toString());

                                        try {
                                            facebookId = object.getString("id");
                                            facebbookEmail = object.getString("email");
                                            String link = object.getString("link");
                                            fname = object.getString("name");


                                            Log.e("facebookId", facebookId);
                                            Log.e("facebookEmail", facebbookEmail);
                                            Log.e("facebookLink", link);
                                            Log.e("facebookName", fname);


                                            //  profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                            profilePicUrl = "https://graph.facebook.com/" + facebookId + "/picture?type=large";

                                            Log.e("fb_image", profilePicUrl);

                                            // mPref.setProfilePic(profilePicUrl);

                                            socialLogin(accessToken, facebookId, facebbookEmail, fname, profilePicUrl);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,email,name,link,picture");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("ERROR", error.toString());
                    }
                });
            }


        });
        googleLogin.setOnClickListener(this);
     /*
        googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // SignupLoginActivity.signIn((Activity) mContext);
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                loginActivity.startActivityForResult(intent,RC_SIGN_IN);
            }
        });*/

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        String serverClientId = getResources().getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        emailBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailBox.getText().toString()).matches())
                        emailBoxInputLayout.setError("Oops! The entered email is not valid.");
                    else
                        emailBoxInputLayout.setErrorEnabled(false);


                }
            }
        });

        emailBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailBoxInputLayout.setError(null);

            }
        });

        passwordBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                passwordBoxInputLayout.setError(null);

            }
        });


    }


    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "onActivityResult:GET_AUTH_CODE:success:" + result.getStatus().isSuccess());
            Log.v("Response", String.valueOf(result.isSuccess()));


            if (result.isSuccess()) {
                // [START get_auth_code]
                GoogleSignInAccount acct = result.getSignInAccount();

                authCode = acct.getServerAuthCode();
                Log.e("AuthCode  " + authCode, " ");

                if (null != acct.getServerAuthCode()) {
                   /* authCode = acct.getServerAuthCode();
                    Log.e("AuthCode  "+ authCode, " ");*/
                }


                if (null != acct.getIdToken()) {
                    id_token = acct.getIdToken();
                    Log.e("id_token  " + id_token, " ");
                }

                Log.e("googleName " + acct.getDisplayName(), "NAME");
                if (null != acct.getDisplayName()) {
                    googleName = acct.getDisplayName();
                }
                Log.e("googleEmail " + acct.getEmail(), "EMAIL");
                if (null != acct.getEmail()) {
                    mail = acct.getEmail();
                }

                Log.e("googleId " + acct.getId(), " ");
                if (null != acct.getId()) {
                    googleId = acct.getId();
                }

                Log.e("googleImage ", String.valueOf(acct.getPhotoUrl()));
                googleImage = String.valueOf(acct.getPhotoUrl());
                Log.e("Profile", googleImage);
                if (null != personPhoto) {
                    personPhoto = acct.getPhotoUrl();
                    img = String.valueOf(personPhoto);

                }

                try {
                    socialLogin(authCode, googleId, mail, googleName, googleImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

            }
        } else {

        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void socialLogin(String authCode, String googleId, String mail, String googleName, String profilePic) {
        Log.e("Token", authCode);
        Log.e("Id", googleId);
        Log.e("email", mail);
        Log.e("Name", googleName);
        Log.e("profile_Pic", profilePic);


        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(LoginActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("name", googleName);
            params.add("email", mail);
            params.add("device_type", "android");
            params.add("token", authCode);
            params.add("device_id", mPref.getAndroid_id());
            params.add("fcm_token", mPref.getGcmToken());
            params.add("profile_pic", profilePic);

            Log.e("PARAMS", params + "");
            httpClient.post(Config.SOCIAL_LOGIN, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String sucess = new String(responseBody);
                        Log.e("success", sucess);

                        JSONObject object = new JSONObject(sucess);
                        if (object.getInt("statusCode") == 200) {
                            user_id = object.getJSONObject("success").getInt("id");
                            mobile_verified = String.valueOf(object.getJSONObject("success").getBoolean("mobile_verified"));
                            isSocial = String.valueOf(object.getJSONObject("success").getBoolean("is_social"));
                            type = String.valueOf(object.getJSONObject("success").getBoolean("is_private"));
                            accToken = object.getString("accessToken");
                            userName = object.getJSONObject("success").getString("name");
                            profile_pic = object.getJSONObject("success").getString("profile_pic");
                            emailValue = object.getJSONObject("success").getString("email");
                            userMobileNumber = object.getJSONObject("success").getString("mobile");
                            interest_update = String.valueOf(object.getJSONObject("success").getBoolean("interest_updated"));
                            location_update = String.valueOf(object.getJSONObject("success").getBoolean("location_updated"));
                            //  latitudeValue = object.getJSONObject("success").getDouble("lat");
                            // longitudeValue = object.getJSONObject("success").getDouble("lng");

                            Log.e("mobileVerify", String.valueOf(mobile_verified));
                            Log.e("access_token", accToken);
                            Log.e("user_id", user_id + "");
                            Log.e("isSocial", isSocial + "");
                            Log.e("isPrivate", type + "");
                            Log.e("userName", userName);
                            Log.e("profile_pic", profile_pic);
                            Log.e("emailValue", emailValue);
                            Log.e("userMobileNumber", userMobileNumber);
                            Log.e("interest_updated", interest_update);
                            Log.e("location_update", location_update);

                            mPref.setId(String.valueOf(user_id));
                            mPref.setMobilVerified(mobile_verified);
                            mPref.setAccessToken(accToken);
                            mPref.setName(userName);
                            mPref.setProfilePic(profile_pic);
                            mPref.setCoverPic(cover_pic);
                            mPref.setUserEmail(emailValue);
                            mPref.setUserMobileNumber(userMobileNumber);
                            mPref.setInterestUpdated(interest_update);
                            mPref.setLocationUpdated(location_update);


                            if (mPref.getMobilVerified().equalsIgnoreCase("false")) {
                                PlaythoraUtility.hideProgressDialog();
                                Intent intent = new Intent(LoginActivity.this, NumberActivity.class);
                                startActivity(intent);
                                finishAffinity();
//                            }else if(mPref.getName().isEmpty()){
//                                Intent home = new Intent(LoginActivity.this, ProfileSetUpActivity.class);
//                                startActivity(home);
//                                finishAffinity();
//                            }else if(mPref.getInterestUpdated().equalsIgnoreCase("false") || mPref.getLocationUpdated().equalsIgnoreCase("false")) {
//                                Intent intent = new  Intent(LoginActivity.this,InterestsActivity.class);
//                                intent.putExtra("name",mPref.getName());
//                                startActivity(intent);
//                                finishAffinity();
                            } else {
                                Intent intent = new Intent(LoginActivity.this, HomeScreenActivity.class);
                                startActivity(intent);
                                finishAffinity();

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String failure = new String(responseBody);
                        Log.e("failure", failure);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        mContext = this;
        setupUI(findViewById(R.id.login_actvity));
        emailBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_email);
        passwordBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_password);
        forgotTxt = (TextView) findViewById(R.id.forgotPassword);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        emailBox = (EditText) findViewById(R.id.loginEmailId);
        passwordBox = (EditText) findViewById(R.id.loginPassword);
        next = (Button) findViewById(R.id.next);
        eye = (ImageView) findViewById(R.id.password_visibility);
        facebookLogin = (RelativeLayout) findViewById(R.id.relative_layout_facebook);
        googleLogin = (RelativeLayout) findViewById(R.id.relative_layout_google);
        loginSubText = (TextView) findViewById(R.id.login_sub_text);
        or = (TextView) findViewById(R.id.or);
        next = (Button) findViewById(R.id.next);
        loginText = (TextView) findViewById(R.id.login_text);

        loginSubText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        or.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        forgotTxt.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        next.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        emailBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        passwordBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        emailBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        passwordBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        loginText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

    }

    private void loginUser() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(LoginActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("email", emailBox.getText().toString());
            params.add("password", passwordBox.getText().toString());
            params.add("device_type", "android");
            params.add("device_id", mPref.getAndroid_id());
//            params.add("fcm_token", mPref.getGcmToken());
            params.add("fcm_token", mPref.getGcmToken());
            Log.e("GCMTOKEN", mPref.getGcmToken() + "");

            Log.e("params",params+"");

            httpClient.post(Config.LOGIN, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("LoginSuccess", s);

                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {
                            id = object.getJSONObject("success").getInt("id");
                            name = object.getJSONObject("success").getString("name");
                            email = object.getJSONObject("success").getString("email");
                            mobile = object.getJSONObject("success").getString("mobile");
                            mobile_verified = String.valueOf(object.getJSONObject("success").getBoolean("mobile_verified"));
                            accessToken = object.getString("accessToken");

                            profile_pic_update = String.valueOf(object.getJSONObject("success").getBoolean("profile_pic_updated"));
                            interest_update = String.valueOf(object.getJSONObject("success").getBoolean("interest_updated"));
                            location_update = String.valueOf(object.getJSONObject("success").getBoolean("location_updated"));
                            isSocial = String.valueOf(object.getJSONObject("success").getBoolean("is_social"));
                            type = String.valueOf(object.getJSONObject("success").getBoolean("is_private"));
                            profile_pic = object.getJSONObject("success").getString("profile_pic");
                            cover_pic = object.getJSONObject("success").getString("cover_pic");
                            latitudeValue = object.getJSONObject("success").getString("lat");
                            longitudeValue = object.getJSONObject("success").getString("lng");
                            userEmail = object.getJSONObject("success").getString("email");
                            userMobileNumber = object.getJSONObject("success").getString("mobile");

                            mPref.setName(name);
                            mPref.setAccessToken(accessToken);
                            mPref.setId(String.valueOf(id));
                            mPref.setMobilVerified(mobile_verified);
                            mPref.setProfilePic(profile_pic);
                            mPref.setCoverPic(cover_pic);
                            mPref.setProfilePicUpdated(profile_pic_update);
                            mPref.setInterestUpdated(interest_update);
                            mPref.setLocationUpdated(location_update);
                            mPref.setUserEmail(userEmail);
                            mPref.setUserMobileNumber(userMobileNumber);

//                            Toast.makeText(LoginActivity.this, "User login successful", Toast.LENGTH_SHORT).show();

                            if (mPref.getMobilVerified().equalsIgnoreCase("false")) {
                                Intent intent = new Intent(LoginActivity.this, NumberActivity.class);
                                startActivity(intent);
                                finishAffinity();
//                            }else if(mPref.getName().isEmpty()){
//                                Intent home = new Intent(LoginActivity.this, ProfileSetUpActivity.class);
//                                startActivity(home);
//                                finishAffinity();
//                            }else if(mPref.getInterestUpdated().equalsIgnoreCase("false") || mPref.getLocationUpdated().equalsIgnoreCase("false")) {
//                                Intent intent = new  Intent(LoginActivity.this,InterestsActivity.class);
//                                intent.putExtra("name",mPref.getName());
//                                startActivity(intent);
//                                finishAffinity();
                            } else {
                                Intent intent = new Intent(LoginActivity.this, HomeScreenActivity.class);
                                startActivity(intent);
                                finishAffinity();

                            }
                               /* if(mPref.getProfilePicUpdated().equalsIgnoreCase("true") && mPref.getInterestUpdated().equalsIgnoreCase("true")
                                        && mPref.getLocationUpdated().equalsIgnoreCase("true")){
                                    Intent intent = new Intent(LoginActivity.this,HomeScreenActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                                if(mPref.getProfilePicUpdated().equalsIgnoreCase("false") && mPref.getInterestUpdated().equalsIgnoreCase("false")
                                        && mPref.getLocationUpdated().equalsIgnoreCase("false")){
                                    Intent intent = new Intent(LoginActivity.this,ProfileSetUpActivity.class);
                                    startActivity(intent);
                                    finish();
                                }


                                if(mPref.getProfilePicUpdated().equalsIgnoreCase("false") && mPref.getInterestUpdated().equalsIgnoreCase("true")
                                        && mPref.getLocationUpdated().equalsIgnoreCase("true")){
                                    Intent intent = new Intent(LoginActivity.this,ProfileSetUpActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                                if(mPref.getProfilePicUpdated().equalsIgnoreCase("true") && mPref.getInterestUpdated().equalsIgnoreCase("false")
                                        && mPref.getLocationUpdated().equalsIgnoreCase("true")){
                                    Intent intent = new Intent(LoginActivity.this,InterestsActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                                if(mPref.getProfilePicUpdated().equalsIgnoreCase("true") && mPref.getInterestUpdated().equalsIgnoreCase("true")
                                        && mPref.getLocationUpdated().equalsIgnoreCase("false")){
                                    Intent intent = new Intent(LoginActivity.this,LocationActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }*/


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    //  try {
//                        String s = new String(responseBody);
                    //                      Log.e("loginFailure",s);
                    //                    JSONObject object = new JSONObject(s);
                    String s = new String(responseBody);
                    Log.e("LoginFailure", s);

                    try {
                        JSONObject object = new JSONObject(s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(LoginActivity.this, "Invalid user", Toast.LENGTH_SHORT).show();
                    /*} catch (JSONException e) {
                        e.printStackTrace();
                    }*/

                }
            });
        }
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(LoginActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    private boolean checkForAllValidation() {


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailBox.getText().toString()).matches()) {
            emailBoxInputLayout.setError("Erm! That doesn't look like an email ID");
            emailBoxInputLayout.setErrorEnabled(true);

        } else {
            emailBoxInputLayout.setErrorEnabled(false);
        }


        if (passwordBox.getText().toString().isEmpty() || passwordBox.getText().toString().contains(" ")) {
            passwordBoxInputLayout.setError("Oops! The entered password is not valid.");
            passwordBoxInputLayout.setErrorEnabled(true);
        } else {
            passwordBoxInputLayout.setErrorEnabled(false);
        }

        if (emailBoxInputLayout.isErrorEnabled() || passwordBoxInputLayout.isErrorEnabled())
            return false;
        else return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relative_layout_google:
                signIn(LoginActivity.this);
                break;
        }

    }

    private void signIn(Activity mContext) {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mContext.startActivityForResult(intent, RC_SIGN_IN);
    }
}
