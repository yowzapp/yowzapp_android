package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;

public class DeleteThisActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_this);
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}
