package com.yowzapp.playstore.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nakul on 29/12/16.
 */
public class GamePlayers implements Parcelable {
    public static final Creator<GamePlayers> CREATOR = new Creator<GamePlayers>() {
        @Override
        public GamePlayers createFromParcel(Parcel in) {
            return new GamePlayers(in);
        }

        @Override
        public GamePlayers[] newArray(int size) {
            return new GamePlayers[size];
        }
    };
    private int id;
    private String type;
    private String image;

    public GamePlayers(int id, String type, String image) {
        this.id = id;
        this.type = type;
        this.image = image;
    }

    public GamePlayers(int id, String type) {
        this.id = id;
        this.type = type;
    }

    protected GamePlayers(Parcel in) {
        id = in.readInt();
        type = in.readString();
        image = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(type);
        dest.writeString(image);
    }
}
