package com.yowzapp.playstore.activty;

/**
 * Created by pramod on 24/11/16.
 */
public class DataObject {

    private String dateDay;

    public DataObject(String dateDay) {
        this.dateDay = dateDay;
    }

    public String getDateDay() {
        return dateDay;
    }

    public void setDateDay(String dateDay) {
        this.dateDay = dateDay;
    }
}
