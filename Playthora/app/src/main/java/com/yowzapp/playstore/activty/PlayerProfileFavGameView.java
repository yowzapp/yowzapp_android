package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import eu.fiskur.chipcloud.ChipCloud;
import eu.fiskur.chipcloud.ChipListener;

/**
 * Created by nakul on 17/11/16.
 */
public class PlayerProfileFavGameView extends Fragment {

    String favSports;
    String[] stringArray, stringArraynew;
    TextView nofavSports;
    Dialog phoneDialog;

//    public PlayerProfileFavGameView(String s) {
//
//        favSports = s;
//        Log.e("!!!favSports", s);
//
//    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        favSports = getArguments().getString("favSports");
        Log.e("!!!favSports", favSports);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.player_profile_fav_games, container, false);
        ChipCloud chipCloud = (ChipCloud) contentView.findViewById(R.id.chip_cloud_new);
        nofavSports = (TextView) contentView.findViewById(R.id.noFavSports);
        nofavSports.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        // chipCloud.setLayoutParams(new LinearLayout.LayoutParams(2500,LinearLayout.LayoutParams.WRAP_CONTENT));
        //  String[] tags = {"Cricket","Hockey","Table tennis","Handball","Athletics","+6 more"};

        try {
            if (!favSports.isEmpty()) {
                JSONArray jsonArray = new JSONArray(favSports);

                if (jsonArray.length() > 5) {
                    stringArray = new String[5];
                } else {
                    stringArray = new String[jsonArray.length()];
                }
                //  stringArray = new String[5];
                stringArraynew = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (i < 5) {
                        if (i == 4) {
                            stringArray[i] = "more";
                        } else {
                            stringArray[i] = jsonArray.getJSONObject(i).getString("name");
                        }
                    }
                    stringArraynew[i] = jsonArray.getJSONObject(i).getString("name");
                }

                if (jsonArray.length() > 4) {

                    new ChipCloud.Configure()
                            .chipCloud(chipCloud)
                            .selectTransitionMS(500)
                            .deselectTransitionMS(250)
                            .labels(stringArray)
                            .mode(ChipCloud.Mode.SINGLE)
                            .HorizontalSpacing(20)
                            .VerticalSpacing(20)
                            .chipListener(new ChipListener() {
                                @Override
                                public void chipSelected(int index) {
                                    // Toast.makeText(getActivity(),"Nakul1",Toast.LENGTH_SHORT).show();
                                    showDialog();
                                }

                                @Override
                                public void chipDeselected(int index) {
                                    // Toast.makeText(getActivity(),"Nakul2",Toast.LENGTH_SHORT).show();
                                    showDialog();

                                }
                            })
                            .buildtwice((stringArray.length - 1), true);

                } else {
                    //Toast.makeText(getActivity(),"Nakul5",Toast.LENGTH_SHORT).show();
                    new ChipCloud.Configure()
                            .chipCloud(chipCloud)
                            .selectTransitionMS(500)
                            .deselectTransitionMS(250)
                            .labels(stringArray)
                            .mode(ChipCloud.Mode.SINGLE)
                            .HorizontalSpacing(20)
                            .VerticalSpacing(20)
                            .chipListener(new ChipListener() {
                                @Override
                                public void chipSelected(int index) {
                                    // Toast.makeText(getActivity(),"Nakul3",Toast.LENGTH_SHORT).show();
                                    showDialog();
                                }

                                @Override
                                public void chipDeselected(int index) {
                                    // Toast.makeText(getActivity(),"Nakul4",Toast.LENGTH_SHORT).show();
                                    showDialog();

                                }
                            })
                            .buildtwice((stringArray.length - 6), false);
                }
            } else {
                nofavSports.setVisibility(View.VISIBLE);
                nofavSports.setText("Favourite sports list is empty");
            }
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Error fetching Favourite sports list", Toast.LENGTH_SHORT).show();
            nofavSports.setVisibility(View.VISIBLE);
            nofavSports.setText("Favourite sports list is empty");
        }


        return contentView;
    }

    private void showDialog() {
        phoneDialog = new Dialog(getActivity());
        phoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        phoneDialog.setContentView(R.layout.dialog_more_games);

        ChipCloud chipCloud = (ChipCloud) phoneDialog.findViewById(R.id.chip_cloud);
        // chipCloud.setLayoutParams(new LinearLayout.LayoutParams(2500,LinearLayout.LayoutParams.WRAP_CONTENT));
        //   String[] tags = {"Cricket","Hockey","Table tennis","Handball","Athletics","Basketball","Beach Volleyball","Canoe Slalom","Cycling","Tennis","Throwball"};
        new ChipCloud.Configure()
                .chipCloud(chipCloud)
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels(stringArraynew)
                .mode(ChipCloud.Mode.SINGLE)
                .HorizontalSpacing(20)
                .VerticalSpacing(20)
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        // Toast.makeText(getActivity(),"Nakul",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void chipDeselected(int index) {
                        // Toast.makeText(getActivity(),"Nakul",Toast.LENGTH_SHORT).show();


                    }
                })
                .buildnew(stringArraynew.length);


        phoneDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log.e("DIALOG", "dialog");
            }
        });

        phoneDialog.show();

    }
}