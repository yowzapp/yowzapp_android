package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.TeamPhotoViewerAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.TeamPhotoModel;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by bharat on 16/4/18.
 */
public class TeamPhotoViewer extends BaseActivity{

    Toolbar toolbar;
    ViewPager viewPager;
    TeamPhotoViewerAdapter teamPhotoViewerAdapter;
    ArrayList<TeamPhotoModel> model;
    int imagePosition;
    String images;
    int imgPos;
    public static TextView toolbarTitle;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teamphoto_viewer);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        toolbar = (Toolbar) findViewById(R.id.team_member_toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager1);
        Intent intent = getIntent();
        images= intent.getStringExtra("images");
        imgPos = (int) getIntent().getExtras().get("position");
        Log.e("modelimages",images);
        model= new ArrayList<TeamPhotoModel>();
        Gson gson= new Gson();
        try {
            JSONArray jsonArray = new JSONArray(images);
            for(int i = 0; i<jsonArray.length();i++){
                model.add(gson.fromJson(jsonArray.get(i).toString(),TeamPhotoModel.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setSupportActionBar(toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);


       toolbarTitle.setTypeface(PlaythoraUtility.getFont(TeamPhotoViewer.this, RALEWAY_REGULAR));
        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        viewPager.setClipToPadding(false);
//        viewPager.setPadding(45, 14, 45, 14);
//        viewPager.setPageMargin(8);


        teamPhotoViewerAdapter  = new TeamPhotoViewerAdapter(this,model);

        viewPager.setAdapter(teamPhotoViewerAdapter);
        viewPager.setCurrentItem(imgPos);
    }
}
