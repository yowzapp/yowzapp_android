package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.GameMain;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.model.MyTransactionPointsModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by hemanth on 16/11/16.
 */
public class MyTransactionPointsFragment extends Fragment {

    public ArrayList<MyTransactionPointsModel> transactionPointsArrayList;
    public MyTransactionPointsAdapter myTransactionPointsAdapter;
    RecyclerView recyclerView;
    ProgressDialog dialog;
    String response;
    Gson mGson;
    PreferenceManager mPref;
    ProgressBar progressBar;
    TextView noPoints;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.my_transaction_points_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());
        progressBar = (ProgressBar) contentView.findViewById(R.id.points_progressbar);
        recyclerView = (RecyclerView) contentView.findViewById(R.id.myTransactionPointsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        noPoints = (TextView) contentView.findViewById(R.id.no_points);
        noPoints.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        try {
            pointsRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contentView;

    }

    private void pointsRecycler() {


        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.MY_POINTS,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                                progressBar.setVisibility(View.GONE);
                                String s = new String(responseBody);
                                Log.d("RESPONSENOTIFICATION", s);

                                mGson = new Gson();
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) ;
                                    {
                                        JSONArray jsonArray = object.getJSONArray("success");
                                        mGson = new Gson();
                                        transactionPointsArrayList = new ArrayList<MyTransactionPointsModel>();

                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            transactionPointsArrayList.add(mGson.fromJson(jsonArray.get(i).toString(), MyTransactionPointsModel.class));

                                        }

                                        if (!transactionPointsArrayList.isEmpty()) {
                                            if (recyclerView.getAdapter() == null) {
                                                myTransactionPointsAdapter = new MyTransactionPointsAdapter(getActivity(), transactionPointsArrayList);
                                                recyclerView.setAdapter(myTransactionPointsAdapter);// set adapter on recyclerview
                                                myTransactionPointsAdapter.notifyDataSetChanged();
                                                noPoints.setVisibility(View.GONE);
                                            } else {
                                                noPoints.setVisibility(View.VISIBLE);
                                                noPoints.setText("You are yet to win any points");
                                            }
                                        } else {
                                            noPoints.setVisibility(View.VISIBLE);
                                            noPoints.setText("You are yet to win any points");

                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }


    public class MyTransactionPointsAdapter extends RecyclerView.Adapter<MyTransactionPointsHolder> {

        private ArrayList<MyTransactionPointsModel> arrayList;
        private Context context;

        public MyTransactionPointsAdapter(Context context, ArrayList<MyTransactionPointsModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }

        @Override
        public MyTransactionPointsHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.points_item_layout, viewGroup, false);
            MyTransactionPointsHolder listHolder = new MyTransactionPointsHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final MyTransactionPointsHolder holder, final int position) {
            final MyTransactionPointsModel model = arrayList.get(position);
            final MyTransactionPointsHolder mainHolder = (MyTransactionPointsHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
                mainHolder.date.setTypeface(Typeface.DEFAULT_BOLD);
                mainHolder.desc.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

                mainHolder.name.setText(model.getContent());
                mainHolder.desc.setText(model.getType());
                //mainHolder.transactionId.setText("TID"+" "+ model.getType_id());
                mainHolder.points.setText(model.getPoint() + " " + "Points");
                mainHolder.date.setText(model.getDate());

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                //  Glide.with(context).load(model.getImage()).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mainHolder.itemView.setTag(model);

            mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getType().equalsIgnoreCase("game")) {
                        Intent i = new Intent(getActivity(), GameMain.class);
                        i.putExtra("GAME_ID", arrayList.get(position).getMore() + "");
                        startActivity(i);
                    }
                }
            });


        }
    }

    public class MyTransactionPointsHolder extends RecyclerView.ViewHolder {

        TextView name, desc, date, transactionId, points;

        public MyTransactionPointsHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.transactionName);
            date = (TextView) view.findViewById(R.id.transactionDate);
            desc = (TextView) view.findViewById(R.id.transactionDesc);
            transactionId = (TextView) view.findViewById(R.id.transactionId);
            points = (TextView) view.findViewById(R.id.transactionPoints);

        }
    }

}
