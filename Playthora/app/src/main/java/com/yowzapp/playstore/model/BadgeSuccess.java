package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 19/1/17.
 */
public class BadgeSuccess {

    private int id;
    private String level_name;
    private int points_from;
    private int points_to;
    private String badge_icon;
    private String badge_cover;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public int getPoints_from() {
        return points_from;
    }

    public void setPoints_from(int points_from) {
        this.points_from = points_from;
    }

    public int getPoints_to() {
        return points_to;
    }

    public void setPoints_to(int points_to) {
        this.points_to = points_to;
    }

    public String getBadge_icon() {
        return badge_icon;
    }

    public void setBadge_icon(String badge_icon) {
        this.badge_icon = badge_icon;
    }

    public String getBadge_cover() {
        return badge_cover;
    }

    public void setBadge_cover(String badge_cover) {
        this.badge_cover = badge_cover;
    }
}
