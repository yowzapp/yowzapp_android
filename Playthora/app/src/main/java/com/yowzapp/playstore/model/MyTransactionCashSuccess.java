package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 7/1/17.
 */
public class MyTransactionCashSuccess {

    private Boolean transaction_status;
    private int amount;
    private String transaction_number;
    private int booking_id;
    private int user_id;
    private String details;
    private int game_id;
    private String game_name;
    private String game_location;
    private String venue_name;
    private String date;


    public Boolean getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(Boolean transaction_status) {
        this.transaction_status = transaction_status;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTransaction_number() {
        return transaction_number;
    }

    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getGame_id() {
        return game_id;
    }

    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    public String getGame_name() {
        return game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }

    public String getGame_location() {
        return game_location;
    }

    public void setGame_location(String game_location) {
        this.game_location = game_location;
    }


    public String getVenue_name() {
        return venue_name;
    }

    public void setVenue_name(String venue_name) {
        this.venue_name = venue_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
