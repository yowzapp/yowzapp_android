package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by hemanth on 16/2/17.
 */

public class ForgotPasswordActivity extends BaseActivity {

    Toolbar toolbar;
    TextView title, next, main;
    RelativeLayout nextLayout;
    TextInputLayout emailBoxInputLayout;
    EditText emailBox;
    PreferenceManager mPref;
    String emailValue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        initialize();
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        nextLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    emailVerify();
                }

            }
        });

        emailBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                emailBoxInputLayout.setError(null);
            }
        });

    }

    private boolean validation() {

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailBox.getText().toString()).matches()) {
            emailBoxInputLayout.setError("Please provide valid email address");
            emailBoxInputLayout.setErrorEnabled(true);
        } else {
            emailBoxInputLayout.setErrorEnabled(false);
        }

        if (emailBoxInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    private void emailVerify() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(ForgotPasswordActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("email", emailBox.getText().toString());

            httpClient.post(Config.FORGOT_PASSWORD, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("emailSuccess", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            emailValue = jsonObject.getString("success");
                            Log.e("EnteredEmail", emailValue + "");
                            Intent intent = new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class);
                            intent.putExtra("email", emailValue);
                            intent.putExtra("otp", jsonObject.getString("otp"));
                            startActivity(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);
                            Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(ForgotPasswordActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private void initialize() {
        mPref = PreferenceManager.instance(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.forgotPasswordToolbar);
        title = (TextView) findViewById(R.id.forgot_password_title);
        emailBox = (EditText) findViewById(R.id.loginEmailId);
        emailBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_email);
        nextLayout = (RelativeLayout) findViewById(R.id.nextLayout);
        next = (TextView) findViewById(R.id.next);
        main = (TextView) findViewById(R.id.forgotMain);

        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        emailBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        emailBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        next.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        main.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }
}
