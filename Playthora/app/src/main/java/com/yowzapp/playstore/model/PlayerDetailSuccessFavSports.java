package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 7/1/17.
 */
public class PlayerDetailSuccessFavSports {

    private int id;
    private String name;
    private String thumbnail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
