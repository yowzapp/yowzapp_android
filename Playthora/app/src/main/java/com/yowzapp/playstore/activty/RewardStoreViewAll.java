package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


/**
 * Created by pramod on 14/11/16.
 */

public class RewardStoreViewAll extends BaseActivity {

    public static ArrayList<RewardModel> productList;
    protected Gson mGson = new Gson();
    GridView viewAllGridView;
    String orderResponse;
    String imageUrl, imageUrlInactive;
    int id;
    String responseString;
    Toolbar toolbar;

    // ProgressBar progressBar;
    // TextView showText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reward_store_view_all);

        Toast.makeText(getApplicationContext(), "RewardStore,", Toast.LENGTH_SHORT).show();

        viewAllGridView = (GridView) findViewById(R.id.grid_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(RewardStoreViewAll.this, Config.RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //   progressBar = (ProgressBar) findViewById(R.id.progressBarInterest);
        //   showText = (TextView) findViewById(com.feetapart.yowzapp.R.id.noItems);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        try {
            loadAllPhotos();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadAllPhotos() throws Exception {
        Thread thread = new Thread(new Runnable() {

            public void run() {

                Log.v("Inside", "LoadAlPhotos");
                try {
                    responseString = "[{\n" +
                            "    \"product\": \"nike blue\",\n" +
                            "    \"productId\": 1,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"20\",\n" +
                            "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike blak\",\n" +
                            "    \"productId\": 2,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"22\",\n" +
                            "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike white\",\n" +
                            "    \"productId\": 3,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"23\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike white\",\n" +
                            "    \"productId\": 3,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"23\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike white\",\n" +
                            "    \"productId\": 3,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"23\",\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}, {\n" +
                            "    \"product\": \"nike blak\",\n" +
                            "    \"productId\": 2,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"22\",\n" +
                            "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                            "},{\n" +
                            "    \"product\": \"nike blue\",\n" +
                            "    \"productId\": 1,\n" +
                            "    \"rewardXp\": \"9000\",\n" +
                            "    \"categories\": \"nike\",\n" +
                            "    \"categoryId\": \"20\",\n" +
                            "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                            "}]";

                    RewardStoreViewAll.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {

                                Log.v("ResponseString", responseString);
                                JSONArray categoryArray = new JSONArray(responseString);
                                mGson = new Gson();
                                productList = new ArrayList<RewardModel>();

                                for (int i = 0; i < categoryArray.length(); i++) {
                                    productList.add(mGson.fromJson(categoryArray.get(i).toString(), RewardModel.class));

                                }

                                if (!productList.isEmpty()) {
                                    if (viewAllGridView.getAdapter() == null) {
                                        CustomAdapter adapter = new CustomAdapter(RewardStoreViewAll.this, productList);
                                        viewAllGridView.setAdapter(adapter);

                                    } else {


                                    }

                                } else {

                                }


                            } catch (JsonParseException e) {


                                e.printStackTrace();
                            } catch (JSONException e) {

                            }

                        }
                    });
                } catch (Exception e) {
                }
            }
        });//
        thread.start();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_all_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        if (id == R.id.action_openSearch) {
            Intent toSearchActivity = new Intent(RewardStoreViewAll.this, SearchActivity.class);
            startActivity(toSearchActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    public class CustomAdapter extends BaseAdapter {


        Context con;
        private ArrayList<RewardModel> categoryList;

        public CustomAdapter(Context context, ArrayList<RewardModel> data) {
            con = context;
            categoryList = data;
        }

        @Override
        public int getCount() {
            int n = categoryList.size();
            Log.e("PhotosFragment" + n, "PhotosFragment");
            return categoryList.size();

        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            final ListHolder holder;

            LayoutInflater inflater = (LayoutInflater) con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            if (row == null) {
                row = inflater.inflate(R.layout.reward_store_view_all_items, null);

                holder = new ListHolder();

                holder.title = (TextView) row.findViewById(R.id.title);
                holder.xp = (TextView) row.findViewById(R.id.xp);
                holder.imgActive = (ImageView) row.findViewById(R.id.image);

                row.setTag(holder);
            } else {
                holder = (ListHolder) row.getTag();
            }

            try {
                imageUrl = categoryList.get(position).getImage();
                Glide.with(con).load(imageUrl).centerCrop().into(holder.imgActive);

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                holder.title.setTypeface(PlaythoraUtility.getFont(RewardStoreViewAll.this, Config.RALEWAY_REGULAR));
                holder.xp.setTypeface(PlaythoraUtility.getFont(RewardStoreViewAll.this, Config.RALEWAY_REGULAR));
                holder.title.setText(categoryList.get(position).getProduct());
                holder.xp.setText(categoryList.get(position).getRewardXp() + "xp");
            } catch (Exception e) {
                e.printStackTrace();
            }


            return row;
        }

        public class ListHolder {
            ImageView imgActive;
            TextView title, xp;

        }

    }

}
