package com.yowzapp.playstore.model;

/**
 * Created by nakul on 03/02/17.
 */
public class Message {
    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;
    public static final int TYPE_DATE = 5;

    private int mType;
    private String mMessage;
    private String mUsername;
    private String mName;
    private String mProfilePic;
    private String mDate;

    private Message() {

    }

    public String getName() {
        return mName;
    }

    public String getProfilePic() {
        return mProfilePic;
    }

    public int getType() {
        return mType;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getDate() {
        return mDate;
    }


    public static class Builder {
        private final int mType;
        private String mUsername;
        private String mMessage;
        private String mName;
        private String mProfilePic;
        private String mDate;

        public Builder(int type) {
            mType = type;
        }

        public Builder username(String username) {
            mUsername = username;
            return this;
        }

        public Builder profilePic(String profilePic) {
            mProfilePic = profilePic;
            return this;
        }

        public Builder message(String message) {
            mMessage = message;
            return this;
        }

        public Builder name(String name) {
            mName = name;
            return this;
        }

        public Builder date(String date) {
            mDate = date;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.mType = mType;
            message.mUsername = mUsername;
            message.mMessage = mMessage;
            message.mName = mName;
//            message.mProfilePic = mProfilePic;
            message.mDate = mDate;
            return message;
        }
    }
}
