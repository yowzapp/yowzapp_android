package com.yowzapp.playstore.model;

/**
 * Created by vaishakha on 17/11/16.
 */
public class CommentModel {
    private int currentPage;
    private boolean hasMorePages;
    private CommentSuccess success;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

    public CommentSuccess getSuccess() {
        return success;
    }

    public void setSuccess(CommentSuccess success) {
        this.success = success;
    }
}
