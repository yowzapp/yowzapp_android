package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 30/12/16.
 */

public class CreateGameModel {

    private String status;
    private String message;
    private int statusCode;
    // private List<CreateGameMembers>members;
    private CreateGameModelSuccess success;

    public CreateGameModelSuccess getSuccess() {
        return success;
    }

    public void setSuccess(CreateGameModelSuccess success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }


}
