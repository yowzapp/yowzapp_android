package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.GameDetailMembers;
import com.yowzapp.playstore.model.GameDetailModel;
import com.yowzapp.playstore.model.GameDetailSuccess;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.VISIBLE;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by pramod on 22/11/16.
 */

public class GameMain extends BaseActivity {

    public static List<GameDetailMembers> gameDetailMemberses;
    public static String MAX_PLAYERS = "";
    public MembersAdapter adapter;
    TextView sportTitle, sportsCategory, locationName, locationTimeSlot, gameTypeText, privateOrAdmin,
            playersText, thisIsPrivate, onlyTeamMembers, description, shoutText, shoutedText;
    Button repeatGame, rsvp, pay, btnJoin, btnRequested, markAttendence;
    RelativeLayout morePlayers;
    Toolbar toolbar;
    String name, location, coverPic, date, price;
    String isPrivate = "", sportsName, discription, isComplete = "", startTime, endTime, gameId, groundId, isInvitePay = "";
    ImageView deleteGame, gameChat;
    CircleImageView imageView;
    ToggleButton toggleButton;
    PreferenceManager mPref;
    GameDetailModel gameDetailModel;
    Gson gson;
    GameDetailSuccess gameDetailSuccess;
    String admin, isMember, rsvpValue, paymentReq, rawDate, isRequested, canJoin, superAdmin;
    RelativeLayout toggleLayout, bottomLayout, shoutLayout, shoutedLayout;
    String gameDetailResponse, activity;
    RecyclerView recyclerView;
    TextView noPlayers;
    boolean privateGame;
    Button add, btnPay, btnCancel;
    int privateInt;
    LinearLayout payLayout;
    boolean isMarkAttendence;
    String isRefundable, isShout;
    Toast toast;
    boolean payDialog;
    boolean isNotificationTriggered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_main);
        mPref = PreferenceManager.instance(getApplicationContext());
        activity = "";

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent.hasExtra("name")) {
            name = intent.getStringExtra("name");
            location = intent.getStringExtra("location");
            date = intent.getStringExtra("date");
            coverPic = intent.getStringExtra("coverPic");
            //isPrivate = intent.getStringExtra("type");
            sportsName = intent.getStringExtra("sportsName");
            discription = intent.getStringExtra("discription");
            isComplete = intent.getStringExtra("isComplete");
            startTime = intent.getStringExtra("start_time");
            endTime = intent.getStringExtra("end_time");
            gameId = intent.getStringExtra("id");
            // Log.e("name",name);
            //  Log.e("location",location);
            Log.e("date", date);
            //  Log.e("coverPic",coverPic);
            // Log.e("isPrivate",isPrivate);
            Log.e("id", gameId);
            Log.e("start_time", startTime);
            Log.e("end_time", endTime);
            Log.e("isComplete", isComplete);
            // Log.e("discription",discription);

        } else if (intent.hasExtra("GAME_ID")) {
            gameId = intent.getStringExtra("GAME_ID");
            Log.e("GAME_ID", gameId);
        } else if (intent.hasExtra("game_id")) {
            gameId = intent.getStringExtra("game_id");
            Log.e("game_id", gameId);
            isNotificationTriggered = true;
        }else if(intent.hasExtra("SUCCESS")){
            gameId = intent.getStringExtra("SUCCESS");
        }

        if (intent.hasExtra("ACTIVITY")) {
            activity = intent.getStringExtra("ACTIVITY");
        }


        gameDetail();


        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Game detail");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));

        morePlayers = (RelativeLayout) findViewById(R.id.more_layout);
        sportTitle = (TextView) findViewById(R.id.createTeamTitle);
        sportsCategory = (TextView) findViewById(R.id.createTeamSportsName);
        description = (TextView) findViewById(R.id.createTeamSportsDescription);
        locationName = (TextView) findViewById(R.id.createTeamLocationDetail);
        locationTimeSlot = (TextView) findViewById(R.id.createTeamTime);
        gameTypeText = (TextView) findViewById(R.id.createTeamGameType);
        privateOrAdmin = (TextView) findViewById(R.id.createTeamPrivateOrPublic);
        playersText = (TextView) findViewById(R.id.createTeamPlayers);
        thisIsPrivate = (TextView) findViewById(R.id.createTeamPrivateGame);
        onlyTeamMembers = (TextView) findViewById(R.id.onlyTeamMembers);
        repeatGame = (Button) findViewById(R.id.repeat_game);
        markAttendence = (Button) findViewById(R.id.mark_attendence);
        rsvp = (Button) findViewById(R.id.rsvp);
        pay = (Button) findViewById(R.id.pay);
        imageView = (CircleImageView) findViewById(R.id.createTeamImage);
        deleteGame = (ImageView) findViewById(R.id.delete_game);
        gameChat = (ImageView) findViewById(R.id.game_chat);
        toggleButton = (ToggleButton) findViewById(R.id.createTeamPrivateToggle);
        toggleLayout = (RelativeLayout) findViewById(R.id.CreateTeamPrivateOrNot);
        recyclerView = (RecyclerView) findViewById(R.id.player_recycler);
        noPlayers = (TextView) findViewById(R.id.no_players);
        add = (Button) findViewById(R.id.add_btn);
        btnPay = (Button) findViewById(R.id.yes);
        btnCancel = (Button) findViewById(R.id.no);
        payLayout = (LinearLayout) findViewById(R.id.payLayout);
        btnJoin = (Button) findViewById(R.id.join);
        bottomLayout = (RelativeLayout) findViewById(R.id.bottom_layout);
        btnRequested = (Button) findViewById(R.id.requested);
        shoutText = (TextView) findViewById(R.id.shout_text);
        shoutedText = (TextView) findViewById(R.id.shouted_text);
        shoutLayout = (RelativeLayout) findViewById(R.id.shout_layout);
        shoutedLayout = (RelativeLayout) findViewById(R.id.shouted_layout);

        sportTitle.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_SEMIBOLD));
        sportsCategory.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        locationName.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        locationTimeSlot.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        gameTypeText.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_SEMIBOLD));
        privateOrAdmin.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        playersText.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_SEMIBOLD));
        thisIsPrivate.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        onlyTeamMembers.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        description.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        repeatGame.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        rsvp.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        pay.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        btnPay.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        btnCancel.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        btnJoin.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        btnRequested.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        add.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        markAttendence.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        shoutText.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));
        shoutedText.setTypeface(PlaythoraUtility.getFont(GameMain.this, RALEWAY_REGULAR));

        //   sportTitle.setText(name);
        //  sportsCategory.setText(sportsName);
        // locationName.setText(location);
        //locationTimeSlot.setText(date+" "+startTime+" - "+endTime);
        //   description.setText(discription);


        recyclerView.setLayoutManager(new LinearLayoutManager(GameMain.this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);


       /* if(isPrivate.equalsIgnoreCase("false")){
            toggleButton.setChecked(false);
        }else {
            toggleButton.setChecked(true);
        }*/

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toast != null) {
                    toast.cancel();
                }
                if (!MAX_PLAYERS.equalsIgnoreCase("0")) {
                    Intent goToSelectLocation = new Intent(GameMain.this, CreateGameInviteFriends.class);
                    goToSelectLocation.putExtra("gameId", gameId);
                    startActivityForResult(goToSelectLocation, 8);
                } else {
                    toast = Toast.makeText(getApplicationContext(), "players limit exceeded", Toast.LENGTH_LONG);
                    toast.show();
                }

            }
        });



/*
        if(isComplete.equalsIgnoreCase("true")){
            repeatGame.setVisibility(View.VISIBLE);
        }else {
            repeatGame.setVisibility(View.GONE);
        }*/


     /*   if(isPrivate.equalsIgnoreCase("true")){
            toggleButton.setChecked(true);
        }else{
            toggleButton.setChecked(false);
        }*/

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //  editGame(isPrivate);
                    isPrivate = "true";
                } else {
                    // editGame(isPrivate);
                    isPrivate = "false";
                }
            }
        });

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editGame(isPrivate);
            }
        });

        /*if(coverPic.isEmpty()){
            Glide.with(getApplicationContext()).load(R.drawable.circled_user).centerCrop().into(imageView);
        }else {
            Glide.with(getApplicationContext()).load(coverPic).centerCrop().into(imageView);
        }*/


        morePlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toAdminView = new Intent(GameMain.this, GamePlayerAdminView.class);
                toAdminView.putExtra("admin", admin);
                toAdminView.putExtra("otherMembers", isMember);
                toAdminView.putExtra("groundId", groundId);

              /*  try {
                    JSONObject object = new JSONObject(gameDetailResponse);
                    JSONObject jsonSuccess = object.getJSONObject("success");
                    JSONArray jsonArray = jsonSuccess.getJSONArray("members");
                     toAdminView.putExtra("membersList",jsonArray.toString());
                    Log.e("**********",jsonArray.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                startActivity(toAdminView);
            }
        });

        repeatGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toAdminView = new Intent(GameMain.this, CreateGame.class);
                toAdminView.putExtra("game", "repeat");
                toAdminView.putExtra("sportIdFrom", "gameDetail");
                toAdminView.putExtra("response", gameDetailResponse);
                toAdminView.putExtra("gameTitle", name);
                toAdminView.putExtra("sportsName", sportsName);
                toAdminView.putExtra("location", location);
                toAdminView.putExtra("date", date);
                toAdminView.putExtra("startTime", startTime);
                toAdminView.putExtra("endTime", endTime);
                toAdminView.putExtra("description", discription);
                toAdminView.putExtra("coverPic", coverPic);
                toAdminView.putExtra("private", isPrivate);
                startActivity(toAdminView);

            }
        });

        rsvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(GameMain.this,"listen",Toast.LENGTH_SHORT).show();
                rsvpDialog();

            }
        });

        deleteGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog();
            }
        });

        gameChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(GameMain.this);
                } else {
                    if (toast != null) {
                        toast.cancel();
                    }
                    try {
                        if (isMember.equalsIgnoreCase("true") || admin.equalsIgnoreCase("true") || superAdmin.equalsIgnoreCase("true")) {

                            Intent chat = new Intent(GameMain.this, ChattingActivity.class);
                            chat.putExtra("name", name);
                            chat.putExtra("image", coverPic);
                            chat.putExtra("id", gameId + "");
                            chat.putExtra("type", "game");
                            chat.putExtra("Conv_id", Integer.parseInt(gameId));
                            chat.putExtra("Conv_type", 1);
                            startActivity(chat);

                        } else {
                            toast = Toast.makeText(getApplicationContext(), "You should be member of this game to chat", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("CLICK", "CLICK");
                if (admin.equalsIgnoreCase("true") && paymentReq.equalsIgnoreCase("true")) {
                    Log.e("paymentReq", "paymentReq");
                    Intent payment = new Intent(GameMain.this, GamePaymentMain.class);
                    payment.putExtra("gameId", gameId);
                    payment.putExtra("rawdate", rawDate);
                    payment.putExtra("membersList", gameDetailResponse);
                    payment.putExtra("amount", price);
                    payment.putExtra("isInvitePay", isInvitePay);
                    payment.putExtra("admin", admin);
                    if (adapter.arrayList.size() == 1) {
                        payment.putExtra("isAlone", true);
                    } else {
                        payment.putExtra("isAlone", false);
                    }
                    payment.putExtra("payment", "gameDeatil");
                    payment.putExtra("member", isMember);
                    payment.putExtra("gameTitle", name);
                    payment.putExtra("sportsName", sportsName);
                    payment.putExtra("location", location);
                    payment.putExtra("date", date);
                    payment.putExtra("startTime", startTime);
                    payment.putExtra("endTime", endTime);
                    payment.putExtra("description", discription);
                    payment.putExtra("coverPic", coverPic);
                    payment.putExtra("private", isPrivate);
                    payment.putExtra("response", gameDetailResponse);
                    startActivity(payment);

                } else {
                    Log.e("isInvitePay", "isInvitePay");
                    // Toast.makeText(GameMain.this,"listen",Toast.LENGTH_SHORT).show();
                    Intent payment = new Intent(GameMain.this, GamePaymentMain.class);
                    payment.putExtra("gameId", gameId);
                    payment.putExtra("rawdate", rawDate);
                    payment.putExtra("amount", price);
                    payment.putExtra("admin", admin);
                    payment.putExtra("payment", "gameDeatil");
                    payment.putExtra("member", isMember);
                    payment.putExtra("gameTitle", name);
                    payment.putExtra("isInvitePay", isInvitePay);
                    payment.putExtra("sportsName", sportsName);
                    payment.putExtra("location", location);
                    payment.putExtra("date", date);
                    payment.putExtra("startTime", startTime);
                    payment.putExtra("endTime", endTime);
                    payment.putExtra("description", discription);
                    payment.putExtra("coverPic", coverPic);
                    payment.putExtra("private", isPrivate);
                    payment.putExtra("response", gameDetailResponse);
                    startActivity(payment);
                }
            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent payment = new Intent(GameMain.this, GamePaymentMain.class);
                payment.putExtra("gameId", gameId);
                payment.putExtra("rawdate", rawDate);
                payment.putExtra("amount", price);
                payment.putExtra("admin", admin);
                payment.putExtra("payment", "gameDeatil");
                payment.putExtra("member", isMember);
                payment.putExtra("isInvitePay", isInvitePay);
                payment.putExtra("gameTitle", name);
                payment.putExtra("sportsName", sportsName);
                payment.putExtra("location", location);
                payment.putExtra("date", date);
                payment.putExtra("startTime", startTime);
                payment.putExtra("endTime", endTime);
                payment.putExtra("description", discription);
                payment.putExtra("coverPic", coverPic);
                payment.putExtra("private", isPrivate);
                payment.putExtra("response", gameDetailResponse);
                startActivity(payment);

            }
        });

        markAttendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("MARK", "MARK");
                Intent attendence = new Intent(GameMain.this, AttendenceMarker.class);
                attendence.putExtra("gameId", gameId);
                attendence.putExtra("MAX_MEMBER", gameDetailSuccess.getMax_members());
                startActivity(attendence);
            }
        });

        shoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PlaythoraUtility.checkInternetConnection(GameMain.this)) {
                    try {
                        PlaythoraUtility.showProgressDialog(GameMain.this);
                        AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                        mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                        RequestParams params = new RequestParams();
                        params.add("game_id", gameId);
                        Log.e("PARAMS", String.valueOf(params));
                        mHttpClient.post(Config.PAYMENT_REJECT, params,
                                new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        PlaythoraUtility.hideProgressDialog();
                                        String s = new String(responseBody);
                                        Log.e("PAYMENT_CANCEL", s);
                                        try {
                                            JSONObject object = new JSONObject(s);
                                            if (object.getInt("statusCode") == 200) {
                                                Toast.makeText(GameMain.this, "Payment request rejected successfully", Toast.LENGTH_SHORT).show();
                                                Intent home = new Intent(GameMain.this, HomeScreenActivity.class);
                                                startActivity(home);
                                                finishAffinity();

                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                        PlaythoraUtility.hideProgressDialog();
                                        try {
                                            String s = new String(bytes);
                                            Log.d("RESPONSE_FAIL", s);
                                            JSONObject object = new JSONObject(s);
                                            if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                                mPref.setAccessToken("");
                                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                            } else
                                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            try {
                                                String s = new String(bytes);
                                                Log.d("RESPONSE_FAIL", s);
                                                JSONObject object = new JSONObject(s);
                                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                            }

                                        }

                                    }

                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                        PlaythoraUtility.hideProgressDialog();
                    }

                }
//                else
//                    Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

            }
        });


        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPref.getAccessToken().isEmpty()) {
                    PlaythoraUtility.LoginDialog(GameMain.this);
                } else {

                    if (PlaythoraUtility.checkInternetConnection(GameMain.this)) {
                        try {
                            PlaythoraUtility.showProgressDialog(GameMain.this);
                            AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                            RequestParams params = new RequestParams();
                            params.add("game_id", gameId);
                            Log.e("PARAMS", String.valueOf(params));
                            mHttpClient.post(Config.GAME_REQUEST, params,
                                    new AsyncHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                            PlaythoraUtility.hideProgressDialog();
                                            try {
                                                String s = new String(responseBody);
                                                Log.e("JOINRESPONSE", s);
                                                JSONObject object = new JSONObject(s);
                                                if (object.getInt("statusCode") == 200) {
                                                    Toast.makeText(GameMain.this, "Requested to join game successful", Toast.LENGTH_SHORT).show();
                                                    gameDetail();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                            }

                                        }

                                        @Override
                                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                            PlaythoraUtility.hideProgressDialog();
                                            try {
                                                String s = new String(bytes);
                                                Log.e("RESPONSE_FAIL", s);
                                                JSONObject object = new JSONObject(s);
                                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                                    mPref.setAccessToken("");
                                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                                } else
                                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                try {
                                                    String s = new String(bytes);
                                                    Log.d("RESPONSE_FAIL", s);
                                                    JSONObject object = new JSONObject(s);
                                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                        }

                                    });
                        } catch (Exception e) {
                            e.printStackTrace();
                            PlaythoraUtility.hideProgressDialog();
                        }

                    } else {
//                        Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

    }

    private void popUp() {

        final Dialog dialog = new Dialog(GameMain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.shout_dialog);
        TextView dialogTitle, dialogNo, dialogYes;

        dialogTitle = (TextView) dialog.findViewById(R.id.title);
        dialogNo = (TextView) dialog.findViewById(R.id.no);
        dialogYes = (TextView) dialog.findViewById(R.id.yes);

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogNo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogYes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                populateShout();
            }
        });
        dialog.show();
    }

    private void populateShout() {
        if (PlaythoraUtility.checkInternetConnection(GameMain.this)) {
            try {
                PlaythoraUtility.showProgressDialog(GameMain.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("game_id", gameId);
                mHttpClient.post(Config.GAME_SHOUT, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("ShoutResponse", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 200) {
                                        gameDetail();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("Shout_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("Shout_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }

        }
//        else
//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
    }

    private void showDeleteDialog() {

        final Dialog dialog = new Dialog(GameMain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_team_dialog);
        TextView dialogTitle, dialogStatus, dialogCancel, dialogDelete, dialogRefund;

        dialogTitle = (TextView) dialog.findViewById(R.id.delete_title);
        dialogStatus = (TextView) dialog.findViewById(R.id.delete_confirm);
        dialogCancel = (TextView) dialog.findViewById(R.id.cancel_text);
        dialogDelete = (TextView) dialog.findViewById(R.id.delete_text);
        dialogRefund = (TextView) dialog.findViewById(R.id.refund_policy);

        if (!payDialog) {
            dialogTitle.setText("Cancel Booking");
            dialogStatus.setText("Are you sure you want to cancel this booking?");
        } else {
            dialogTitle.setText("Delete Booking");
            dialogStatus.setText("Are you sure you want to delete this game?");
            dialogRefund.setVisibility(View.GONE);
        }

        dialogTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogDelete.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dialogRefund.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        dialogDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                deleteTeam();
            }
        });

        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialogRefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cancellationAndRefund = new Intent(GameMain.this, AdditionalInformation.class);
                cancellationAndRefund.putExtra("title", "cancellation");
                startActivity(cancellationAndRefund);
            }
        });
        dialog.show();

    }

    private void deleteTeam() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GameMain.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("game_id", gameId);
            Log.e("game_id", gameId);
            httpClient.post(Config.DELETE_GAME, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("acceptGame", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getString("status").equalsIgnoreCase("success")) {
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            if (activity.equalsIgnoreCase("PROFILEGAME")) {
                                Intent i = new Intent();
                                setResult(5, i);//to ProfileGames activity
                            } else {
                                Intent i = new Intent(GameMain.this, HomeScreenActivity.class);
                                startActivity(i);
                                finishAffinity();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GameMain.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();

                        } else
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

        }
    }

    private void gameDetail() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GameMain.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("game_id", gameId);
            Log.e("game_id", gameId);
            httpClient.post(Config.GAME_DETAIL, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    gameDetailResponse = new String(responseBody);
                    Log.e("gameDetail", gameDetailResponse);
                    gameDetailModel = new GameDetailModel();
                    gson = new Gson();
                    gameDetailModel = gson.fromJson(gameDetailResponse, GameDetailModel.class);

                    gameDetailSuccess = gameDetailModel.getSuccess();
                    gameDetailMemberses = gameDetailModel.getSuccess().getMembers();

                    isMember = String.valueOf(gameDetailSuccess.is_member());
                    admin = String.valueOf(gameDetailSuccess.is_mine());
                    discription = gameDetailSuccess.getDiscription();
                    name = gameDetailSuccess.getName();
                    sportsName = gameDetailSuccess.getSport_name();
                    location = gameDetailSuccess.getLocation();
                    groundId = String.valueOf(gameDetailSuccess.getId());
                    isPrivate = String.valueOf(gameDetailSuccess.is_private());
                    gameId = String.valueOf(gameDetailSuccess.getId());
                    isInvitePay = String.valueOf(gameDetailSuccess.is_invited_pay());
                    rsvpValue = String.valueOf(gameDetailSuccess.isShowRSVP());
                    paymentReq = String.valueOf(gameDetailSuccess.isPayment_required());
                    rawDate = gameDetailSuccess.getRaw_date();
                    date = gameDetailSuccess.getDate();
                    isComplete = String.valueOf(gameDetailSuccess.is_completed());
                    startTime = gameDetailSuccess.getStart_time();
                    endTime = gameDetailSuccess.getEnd_time();
                    coverPic = gameDetailSuccess.getCover_pic();
                    price = gameDetailSuccess.getPrice() + "";
                    canJoin = String.valueOf(gameDetailSuccess.getCan_join());
                    isRequested = String.valueOf(gameDetailSuccess.is_requested());
                    superAdmin = String.valueOf(gameDetailSuccess.is_super_admin());
                    isMarkAttendence = gameDetailSuccess.isMark_attendance();
                    isRefundable = gameDetailSuccess.is_refundable() + "";
                    isShout = gameDetailSuccess.isCan_shout() + "";

                    try {
                        Glide.with(getApplicationContext()).load(coverPic).error(R.drawable.game_null).centerCrop().into(imageView);
                    } catch (Exception e) {
                        imageView.setImageResource(R.drawable.game_null);
                    }

                    int numbers;
                    numbers = Integer.parseInt(gameDetailSuccess.getMax_members()) - Integer.parseInt(gameDetailSuccess.getNo_members());

                    Log.e("maxMembers", gameDetailSuccess.getMax_members() + "");
                    Log.e("noMembers", gameDetailSuccess.getNo_members() + "");
                    Log.e("isMarkAttendence", gameDetailSuccess.isMark_attendance() + "");
                    Log.e("isRefundable", gameDetailSuccess.is_refundable() + "");


                    MAX_PLAYERS = numbers + "";

                    Log.e("MAX_PLAYERS", MAX_PLAYERS);

                    locationTimeSlot.setText(date + " " + startTime + " - " + endTime);

                    Log.e("isMember", isMember);
                    Log.e("admin", admin);
                    Log.e("isPrivate", isPrivate);
                    Log.e("isInvitePay", isInvitePay);
                    Log.e("rsvp", rsvpValue);
                    Log.e("payment_required", paymentReq);
                    Log.e("rawDate", rawDate);
                    Log.e("price", price);
                    Log.e("canJoin", canJoin);
                    Log.e("isRequested", isRequested);
                    Log.e("superAdmin", superAdmin);
                    Log.e("can_shout", isShout);


                    if (canJoin.equalsIgnoreCase("true")) {
                        //Toast.makeText(getApplicationContext(),"InsideCanJon",Toast.LENGTH_SHORT).show();
                        bottomLayout.setVisibility(VISIBLE);
                        btnJoin.setVisibility(VISIBLE);
                        //  btnJoin.setText("JOIN");
                    } else {
                        btnJoin.setVisibility(View.GONE);

                    }


                    Log.e("name", name);
                    Log.e("location", location);
                    Log.e("date", date);
                    Log.e("coverPic", coverPic);
                    Log.e("id", gameId);
                    Log.e("start_time", startTime);
                    Log.e("end_time", endTime);
                    Log.e("isComplete", isComplete);
                    Log.e("discription", discription);
                   // Log.e("groundname", gameDetailModel.getGroundName());


                    sportTitle.setText(name);
                    sportsCategory.setText(sportsName);
                    locationName.setText(gameDetailModel.getVenue()+"\n"+location);
                    locationTimeSlot.setText(date + " " + startTime + " - " + endTime);
                    description.setText(discription);

                    if (paymentReq.equalsIgnoreCase("true")) {
                        pay.setVisibility(VISIBLE);
                        bottomLayout.setVisibility(VISIBLE);
                        setPayDialog(true);
                    } else {
                        pay.setVisibility(View.GONE);
                        setPayDialog(false);
                    }

                    if (isInvitePay.equalsIgnoreCase("true")) {
                        payLayout.setVisibility(VISIBLE);
                        bottomLayout.setVisibility(VISIBLE);
                    } else {
                        payLayout.setVisibility(View.GONE);
                    }

                   /* if(isMember.equalsIgnoreCase("true") && isInvitePay.equalsIgnoreCase("true")){
                        pay.setVisibility(View.VISIBLE);
                    }else {
                        pay.setVisibility(View.GONE);
                    }*/

                    if (admin.equalsIgnoreCase("true")) {
                        toggleLayout.setVisibility(VISIBLE);

                    } else {
                        toggleLayout.setVisibility(View.GONE);
                    }

                    if (isMarkAttendence) {
                        markAttendence.setVisibility(VISIBLE);
                        bottomLayout.setVisibility(VISIBLE);
                        deleteGame.setVisibility(View.GONE);
                    } else {
                        markAttendence.setVisibility(View.GONE);
                        if (admin.equalsIgnoreCase("true") && isComplete.equalsIgnoreCase("true")) {
                            repeatGame.setVisibility(VISIBLE);
                            bottomLayout.setVisibility(VISIBLE);
                            deleteGame.setVisibility(View.GONE);
                        } else {
                            repeatGame.setVisibility(View.GONE);
                        }
                    }

                    if (isPrivate.equalsIgnoreCase("false")) {
                        privateOrAdmin.setText("Public");
                        toggleButton.setChecked(false);
                        // privateGame=false;
                        isPrivate = "false";
                    } else {
                        privateOrAdmin.setText("Private");
                        toggleButton.setChecked(true);
                        //  privateGame=true;
                        isPrivate = "true";
                    }

                    if (rsvpValue.equalsIgnoreCase("true") && isMember.equalsIgnoreCase("false")) {
                        rsvp.setVisibility(VISIBLE);
                        bottomLayout.setVisibility(VISIBLE);
                    } else {
                        rsvp.setVisibility(View.GONE);
                    }

                    if (isRequested.equalsIgnoreCase("true")) {
                        btnRequested.setVisibility(VISIBLE);
                        bottomLayout.setVisibility(VISIBLE);
                    } else {
                        btnRequested.setVisibility(View.GONE);
                    }

                    if (superAdmin.equalsIgnoreCase("true")) {
                        add.setVisibility(VISIBLE);

                        if (isRefundable.equalsIgnoreCase("false")) {
                            deleteGame.setVisibility(View.GONE);
                        } else if (!(isRefundable.equalsIgnoreCase("false")) && isMarkAttendence) {
                            deleteGame.setVisibility(View.GONE);
                        } else {
                            deleteGame.setVisibility(View.VISIBLE);
                        }
                    } else {
                        add.setVisibility(View.GONE);

                    }


                    if (superAdmin.equalsIgnoreCase("true") || admin.equalsIgnoreCase("true")) {
                        if (isShout.equalsIgnoreCase("true")) {
                            shoutLayout.setVisibility(VISIBLE);
                            shoutedLayout.setVisibility(View.GONE);
                        } else {
                            shoutLayout.setVisibility(View.GONE);
                            shoutedLayout.setVisibility(VISIBLE);
                        }
                    } else {
                        shoutLayout.setVisibility(View.GONE);
                        shoutedLayout.setVisibility(View.GONE);
                    }


                    if (gameDetailModel.getSuccess().getMembers().size() != 0) {

                        if (recyclerView.getAdapter() == null) {

                            adapter = new MembersAdapter(GameMain.this, gameDetailMemberses);
                            recyclerView.setAdapter(adapter);// set adapter on recyclerview
                            adapter.notifyDataSetChanged();
                        } else {
                            adapter.refreshMember(gameDetailMemberses);
                            adapter.notifyDataSetChanged();
                        }
                        noPlayers.setVisibility(View.GONE);
                    } else {
                        noPlayers.setVisibility(VISIBLE);
                        noPlayers.setText("no players");

                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GameMain.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();

                        } else
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            });
        }
    }

    private void rsvpDialog() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(GameMain.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.rsvp_dialog, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(GameMain.this);
        alertDialogBuilderUserInput.setView(mView);
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

        final TextView rsvpText = (TextView) mView.findViewById(R.id.rsvp);
        final TextView iamPlaying = (TextView) mView.findViewById(R.id.im_playing);
        final TextView notNow = (TextView) mView.findViewById(R.id.not_now);

        rsvpText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        iamPlaying.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        notNow.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        iamPlaying.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playing();
                alertDialogAndroid.dismiss();
            }
        });

        notNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reject();
                alertDialogAndroid.dismiss();
            }
        });

        alertDialogAndroid.show();
    }

    private void reject() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GameMain.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("type_id", gameId);

            httpClient.post(Config.REJECT_GAME_REQUEST, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {

                        String s = new String(responseBody);
                        Log.e("rejectSuccess", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(GameMain.this, "Game rejected successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GameMain.this, HomeScreenActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GameMain.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();

                        } else
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            });

        }
    }

    private void playing() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GameMain.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("type_id", gameId);
            httpClient.post(Config.ACCEPT_GAME_REQUEST, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("acceptGame", s);

                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(GameMain.this, "Accepted game request", Toast.LENGTH_SHORT).show();
                            rsvp.setVisibility(View.GONE);
                            gameDetail();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GameMain.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();

                        } else
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    }


                }
            });

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data != null) {

            Log.e("REQUESTCODE", requestCode + "" + " request");
            if (requestCode == 15) {
                Log.e("MARKATTENDENCE", data.getStringExtra("SUCCESS")+"");
                gameId = data.getStringExtra("SUCCESS");
                gameDetail();

            }
        }else {
            Log.e("data", "data");
        }

    }

    private void editGame(final String isPrivate) {
        Log.e("EDITGAME", "editGame");
        Log.e("PRIVATE", isPrivate + "");
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(GameMain.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("game_id", gameId);
            params.add("is_private", isPrivate);
            httpClient.post(Config.EDIT_GAME, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("editGame", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            String privateValue;
                            Toast.makeText(GameMain.this, "Game updated successfully", Toast.LENGTH_SHORT).show();
                            privateValue = String.valueOf(jsonObject.getJSONObject("success").getBoolean("is_private"));
                            if (privateValue.equalsIgnoreCase("true")) {
                                privateOrAdmin.setText("Private");
                            } else {
                                privateOrAdmin.setText("Public");
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GameMain.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();

                        } else
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(GameMain.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(GameMain.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });

        }

    }

    public void setPayDialog(boolean payDialog) {
        this.payDialog = payDialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isNotificationTriggered) {
            Intent homeIntent = new Intent(GameMain.this, HomeScreenActivity.class);
            startActivity(homeIntent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (adapter != null) {
            for (int i = 0; i < gameDetailMemberses.size(); i++) {
                Log.e("&&&&&&&&&&&&&&&&&&&", gameDetailMemberses.get(i).getName());
            }
            adapter.refreshMember(gameDetailMemberses);
            adapter.notifyDataSetChanged();
        } else {
            /*adapter = new MembersAdapter(GameMain.this,gameDetailMemberses);
            recyclerView.setAdapter(adapter);// set adapter on recyclerview
            adapter.notifyDataSetChanged();*/
        }



       /* adapter = new MembersAdapter(GameMain.this,gameDetailMemberses);
        recyclerView.setAdapter(adapter);// set adapter on recyclerview
        adapter.notifyDataSetChanged();*/

    }

    public class MembersAdapter extends RecyclerView.Adapter<MembersHolder> {

        private List<GameDetailMembers> arrayList;
        private Context context;


        public MembersAdapter(Context context, List<GameDetailMembers> members) {
            this.context = context;
            this.arrayList = members;
        }


        @Override
        public MembersHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.game_members_item, viewGroup, false);
            MembersHolder listHolder = new MembersHolder(mainGroup);
            return listHolder;
        }

        @Override
        public int getItemCount() {
            Log.e("members_size",arrayList.size()+"");
            return arrayList.size();
        }

        public void refreshMember(List<GameDetailMembers> members) {
            this.arrayList = members;
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final MembersHolder holder, final int position) {
            final GameDetailMembers model = arrayList.get(position);

            final MembersHolder mainHolder = (MembersHolder) holder;

            try {
                Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setTag(model);

            mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, PlayerProfileViewActivity.class);
                    i.putExtra("Id", model.getId() + "");
                    startActivity(i);
                }
            });

        }
    }

    public class MembersHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;

        public MembersHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.playerImageView);

        }
    }
}
