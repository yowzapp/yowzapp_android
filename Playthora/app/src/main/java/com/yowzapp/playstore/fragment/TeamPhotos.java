package com.yowzapp.playstore.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.appcompat.BuildConfig;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.activty.TeamPhotoViewer;
import com.yowzapp.playstore.model.TeamPhotoModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.stream.Position;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 22/11/16.
 */
public class TeamPhotos extends Fragment {
    private static final int PICK_FROM_CAM = 1;
    View contentView;
    RecyclerView recyclerView;
    TeamPhotosAdapter adapter;
    String response;
    ArrayList<TeamPhotoModel> model;
    Gson gson;
    PreferenceManager mPref;
    int id;
    ProgressBar progressBar;
    FloatingActionButton addImage;
    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    File output = null;
    String selectedImagePath;
    int serverResponseCode = 0;
    boolean isMember;
    TextView emptyText;
    SwipeRefreshLayout swipeContainer;
    private int PERMISSION_REQUEST_CODE = 200;
    private int PICK_IMAGE_REQUEST = 2;
    JSONArray gallery;
    int delId;

    public TeamPhotos(int id, boolean isMember) {
        this.id = id;
        this.isMember = isMember;
    }

    public TeamPhotos() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.home_team_layout, container, false);
        model = new ArrayList<>();

        initializeAllComponent();

        swipeContainer.setRefreshing(false);
        swipeContainer.setEnabled(false);
        mPref = PreferenceManager.instance(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));


        loadPhotos();

        if (isMember) {
            addImage.setVisibility(View.VISIBLE);
        } else {
            addImage.setVisibility(View.GONE);
        }

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("checkPermission", checkPermission() + "");
                if (!checkPermission()) {
                } else {
                    uploadImage();
                }
            }
        });

        return contentView;
    }

    private void initializeAllComponent() {
        addImage = (FloatingActionButton) contentView.findViewById(R.id.fab_add_image);
        progressBar = (ProgressBar) contentView.findViewById(R.id.progress_bar_one);
        recyclerView = (RecyclerView) contentView.findViewById(R.id.team_recycler);
        emptyText = (TextView) contentView.findViewById(R.id.tv_empty_state);
        swipeContainer = (SwipeRefreshLayout) contentView.findViewById(R.id.swipeContainer_loc);

        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
    }

    private boolean checkPermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if (permsRequestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm", "request");
                uploadImage();
            } else {
                Toast.makeText(getActivity(), "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private void uploadImage() {
        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(getActivity(),
                                     getActivity().getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("CODES", requestCode + " " + resultCode + " " + data);

        if(requestCode == 1 || requestCode ==2) {

            if (resultCode == -1) {
                if (requestCode == PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null) {
                    Uri selectedImageUri = data.getData();
                    try {
                        selectedImagePath = getPath(selectedImageUri);
                        //imagesArray.add(new ImageModel(selectedImagePath,"2"));
                        PlaythoraUtility.showProgressDialog(getActivity());
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    uploadFile(selectedImagePath);
                                    Log.e("RUNNING", "RUNNING");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            PlaythoraUtility.hideProgressDialog();
                                            Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }).start();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    try {
                        if ((getRealPathFromURI(Uri.fromFile(output).toString(), getActivity()) == null)) {
                            return;
                        } else {
                            selectedImagePath = getRealPathFromURI(Uri.fromFile(output).toString(), getActivity());
                            //imagesArray.add(new ImageModel(selectedImagePath, "2"));
                            PlaythoraUtility.showProgressDialog(getActivity());
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        uploadFile(selectedImagePath);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                PlaythoraUtility.hideProgressDialog();
                                                Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }
                            }).start();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }


        if(requestCode == 20){
            if(data!=null){
               String deleteId=data.getStringExtra("image_id");
                Log.e("deleted",deleteId+"");
                delId = Integer.parseInt(deleteId);
                Log.e("deleted",delId+"");
            }
        }

    }

    private void uploadFile(final String sourceFileUri) {
        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File sourceFile = new File(PlaythoraUtility.compressImage(sourceFileUri, getActivity()));
        if (!sourceFile.isFile()) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    PlaythoraUtility.hideProgressDialog();
                    Log.e("uploadFile", "Source File not exist :" + sourceFileUri);
                    Toast.makeText(getActivity(), "Source File not exist :" + sourceFileUri, Toast.LENGTH_SHORT).show();
                }
            });
            return;

        } else {
            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.ADD_TEAM_IMAGE);
                PlaythoraUtility.trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());
                conn.setRequestProperty("teamId", String.valueOf(id));
                Log.e("id",String.valueOf(id));
                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                        + fileName + "\"" + lineEnd); //image is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS", "Content-Disposition: form-data; name=\"profile_pic\";filename=\"" + fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                        }
                    });
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                JSONObject object = new JSONObject(total.toString());
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    JSONObject gallery = object.getJSONObject("success");

                                    model.add(0, new TeamPhotoModel(gallery.getInt("id"), gallery.getString("image"), gallery.getInt("team_id")));
                                    //adapter = new TeamPhotosAdapter(getActivity(), model);
                                    // recyclerView.setAdapter(adapter);


                                    if (model.size() > 0) {
                                        emptyText.setVisibility(View.GONE);
                                        try {
                                            loadPhotos();
//                                            adapter.Refresh(model);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    Toast.makeText(getActivity(), "Upload image Complete.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else if (serverResponseCode == 400 || serverResponseCode == 401) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            mPref.setAccessToken("");
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            getActivity().finishAffinity();
                        }
                    });

                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                        PlaythoraUtility.hideProgressDialog();
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                        PlaythoraUtility.hideProgressDialog();
                    }
                });
            } catch (ProtocolException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                        PlaythoraUtility.hideProgressDialog();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), R.string.fail_upload_image, Toast.LENGTH_SHORT).show();
                        PlaythoraUtility.hideProgressDialog();
                    }
                });
            }
        }

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private void loadPhotos() {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("team_id", String.valueOf(id));
                Log.e("PARAMMS", String.valueOf(params)+ "id" +String.valueOf(id));
                mHttpClient.post(Config.TEAM_GALLERY, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                String s = new String(responseBody);
                                Log.e("RESPONSETEAMGALLERY", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        gallery = object.getJSONObject("success").getJSONArray("Galleries");
                                        Log.e("gallery", gallery.length() + "");
                                        gson = new Gson();
                                        model = new ArrayList<>();
                                        for (int i = 0; i < gallery.length(); i++) {
                                            model.add(gson.fromJson(gallery.get(i).toString(), TeamPhotoModel.class));
                                        }
                                        Log.e("model",model+"");
                                        adapter = new TeamPhotosAdapter(getActivity(), model);
                                        recyclerView.setAdapter(adapter);
                                        if (model.size() == 0) {
                                            emptyText.setVisibility(View.VISIBLE);
                                            if (isMember)
                                                emptyText.setText("Start adding photos");
                                            else emptyText.setText("No photos");
                                        } else emptyText.setVisibility(View.GONE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL_GALLERY", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL_GALLERY", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    public class TeamPhotosAdapter extends RecyclerView.Adapter<TeamPhotosAdapter.PhotoList> {
        ArrayList<TeamPhotoModel> model;
        Context context;

        public TeamPhotosAdapter(FragmentActivity activity, ArrayList<TeamPhotoModel> model) {
            context = activity;
            this.model = model;
        }

        public void Refresh(ArrayList<TeamPhotoModel> model) {
            this.model = model;
            notifyDataSetChanged();
        }

        @Override
        public PhotoList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_photos_item, null);
            return new PhotoList(layoutView);
        }

        @Override
        public void onBindViewHolder(PhotoList holder, final int position) {
            Log.e("loader", model.get(position).getImage());
            try {
//                Glide.with(context).load(model.get(position).getImage()).error(R.drawable.no_team).centerCrop().into(holder.mImage);
                Glide.with(context).load(model.get(position).getImage()).error(R.drawable.no_team).fitCenter().into(holder.mImage);

            } catch (Exception e) {
                holder.mImage.setImageResource(R.drawable.no_team);
            }
            if (isMember) {
                holder.clear.setVisibility(View.VISIBLE);
            } else {
                holder.clear.setVisibility(View.GONE);
            }

            holder.clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteImage(position);
                }
            });

            holder.mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), TeamPhotoViewer.class);
                    intent.putExtra("position", position);
                    intent.putExtra("images", gallery.toString());
                    startActivityForResult(intent, 20);
                }
            });


        }

        private void deleteImage(final int position) {
            if (PlaythoraUtility.checkInternetConnection(getActivity())) {
                try {
                    progressBar.setVisibility(View.VISIBLE);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    mHttpClient.addHeader("teamId", String.valueOf(model.get(position).getTeam_id()));
                    mHttpClient.addHeader("galleryId", String.valueOf(model.get(position).getId()));
                    mHttpClient.post(Config.DELETE_TEAM_IMAGE,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    progressBar.setVisibility(View.GONE);
                                    String s = new String(responseBody);
                                    Log.e("RESPONSDELAMGALLERY", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            model.remove(position);
                                            notifyDataSetChanged();
                                            Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    progressBar.setVisibility(View.GONE);

                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL_GALLERY", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finishAffinity();

                                        } else
                                            Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        try {
                                            String s = new String(bytes);
                                            Log.d("RESPONSE_FAIL_GALLERY", s);
                                            JSONObject object = new JSONObject(s);
                                            Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                            //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            } else {
//                Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();

            }

        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class PhotoList extends RecyclerView.ViewHolder {
            ImageView mImage;
            RelativeLayout clear;

            public PhotoList(View itemView) {
                super(itemView);
                mImage = (ImageView) itemView.findViewById(R.id.team_image);
                clear = (RelativeLayout) itemView.findViewById(R.id.rel_clear_image);
            }
        }


    }


}
