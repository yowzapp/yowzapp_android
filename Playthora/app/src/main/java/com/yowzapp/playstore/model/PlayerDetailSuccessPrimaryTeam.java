package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 7/1/17.
 */
public class PlayerDetailSuccessPrimaryTeam {

    private int id;
    private String name;
    private String discription;
    private String cover_pic;
    private boolean activity_meter;
    private int members_count;
    private int followers_count;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public boolean isActivity_meter() {
        return activity_meter;
    }

    public void setActivity_meter(boolean activity_meter) {
        this.activity_meter = activity_meter;
    }

    public int getMembers_count() {
        return members_count;
    }

    public void setMembers_count(int members_count) {
        this.members_count = members_count;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }
}
