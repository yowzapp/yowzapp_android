package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.TeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

/**
 * Created by nakul on 15/11/16.
 */
public class SearchPlayerAdapter extends RecyclerView.Adapter<SearchPlayerAdapter.ProfileListHolder> {
    Context mContext;


    ArrayList<TeamMembersModel> playerList;

    public SearchPlayerAdapter(Context context, ArrayList<TeamMembersModel> arrayList) {
        mContext = context;
        playerList = arrayList;
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_view, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, int position) {
        try {
            Glide.with(mContext).load(playerList.get(position).getImage()).error(R.drawable.circled_user).into(holder.playerImage);
        } catch (Exception e) {
            holder.playerImage.setImageResource(R.drawable.circled_user);
        }
        holder.playerName.setText(playerList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView playerImage;
        TextView playerName;
        RelativeLayout lytEditDeleteWrapper, alreadyExchanged;

        public ProfileListHolder(View itemView) {
            super(itemView);


            playerImage = (ImageView) itemView.findViewById(R.id.imageView_player);

            playerName = (TextView) itemView.findViewById(R.id.player_name);


            playerName.setTypeface(PlaythoraUtility.getFont(mContext, Config.RALEWAY_REGULAR));


        }
    }


}