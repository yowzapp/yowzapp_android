package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 16/11/16.
 */

public class NotificationSettingsActivity extends BaseActivity implements  View.OnClickListener {

    Toolbar toolbar;
    TextView toolbarTitle, notifyMyFollowers, notifyFollowingMemebers, postMyTeam, notifyInvitesForGame,
            notifyContact, push_notify, notifyChallenge;
    ToggleButton toggleButton1, toggleButton2, toggleButton3, toggleButton4, toggleButton5, toggleButton6, toggleButtonpush;
    View overlay;
    LinearLayout additionalToggles;
    PreferenceManager mPref;
    String isPushNotification, isNewFollower, isNewTeam, isOpenGame, isGameInvite, isPayInvite, isChallenge;
    boolean bPushNotification, bNewFollower, bNewTeam, bOpenGame, bGameInvite, bPayInvite, bChallenge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_preferences_layout);


        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        initializeAllComponents();

        notificationList();

        toolbarTitle.setText("Notification settings");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toggleButton1.setOnClickListener(this);
        toggleButton2.setOnClickListener(this);
        toggleButton3.setOnClickListener(this);
        toggleButton4.setOnClickListener(this);
        toggleButton5.setOnClickListener(this);
        toggleButton6.setOnClickListener(this);
        toggleButtonpush.setOnClickListener(this);
    }

    private void notificationList() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(NotificationSettingsActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            httpClient.get(Config.NOTIFICATION_SETTING_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("notificationSuccess", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statuscode") == 200) {

                            isPushNotification = jsonObject.getJSONObject("success").getBoolean("push_notification") + "";
                            isNewFollower = jsonObject.getJSONObject("success").getBoolean("new_follower") + "";
                            isNewTeam = jsonObject.getJSONObject("success").getBoolean("new_team") + "";
                            isOpenGame = jsonObject.getJSONObject("success").getBoolean("open_game") + "";
                            isGameInvite = jsonObject.getJSONObject("success").getBoolean("game_invite") + "";
                            isPayInvite = jsonObject.getJSONObject("success").getBoolean("game_pay_invite") + "";
                            isChallenge = jsonObject.getJSONObject("success").getBoolean("challenge_game") + "";

                            Log.e("pushNotification", isPushNotification);
                            Log.e("newFollower", isNewFollower);
                            Log.e("newTeam", isNewTeam);
                            Log.e("openGame", isOpenGame);
                            Log.e("gameInvite", isGameInvite);
                            Log.e("gamePayInvite", isPayInvite);
                            Log.e("isChallenge", isChallenge);

                            if (isPushNotification.equalsIgnoreCase("true")) {
                                toggleButtonpush.setChecked(true);
                                bPushNotification = true;
                                toggleButton1.setClickable(true);
                                toggleButton2.setClickable(true);
                                toggleButton3.setClickable(true);
                                toggleButton4.setClickable(true);
                                toggleButton5.setClickable(true);
                                toggleButton6.setClickable(true);
                                overlay.setVisibility(View.GONE);
                                additionalToggles.setVisibility(View.VISIBLE);
                            } else {
                                toggleButtonpush.setChecked(false);
                                bPushNotification = false;
                                toggleButton1.setClickable(false);
                                toggleButton2.setClickable(false);
                                toggleButton3.setClickable(false);
                                toggleButton4.setClickable(false);
                                toggleButton5.setClickable(false);
                                toggleButton6.setClickable(false);
                                overlay.setVisibility(View.VISIBLE);
                                additionalToggles.setVisibility(View.GONE);
                            }

                            if (isNewFollower.equalsIgnoreCase("true")) {
                                toggleButton1.setChecked(true);
                                bNewFollower = true;
                            } else {
                                toggleButton1.setChecked(false);
                                bNewFollower = false;
                            }

                            if (isNewTeam.equalsIgnoreCase("true")) {
                                toggleButton2.setChecked(true);
                                bNewTeam = true;
                            } else {
                                toggleButton2.setChecked(false);
                                bNewTeam = false;
                            }

                            if (isOpenGame.equalsIgnoreCase("true")) {
                                toggleButton3.setChecked(true);
                                bOpenGame = true;
                            } else {
                                toggleButton3.setChecked(false);
                                bOpenGame = false;
                            }

                            if (isGameInvite.equalsIgnoreCase("true")) {
                                toggleButton4.setChecked(true);
                                bGameInvite = true;
                            } else {
                                toggleButton4.setChecked(false);
                                bGameInvite = false;
                            }

                            if (isChallenge.equalsIgnoreCase("true")) {
                                toggleButton6.setChecked(true);
                                bChallenge = true;
                            } else {
                                toggleButton6.setChecked(false);
                                bChallenge = false;
                            }

                            if (isPayInvite.equalsIgnoreCase("true")) {
                                toggleButton5.setChecked(true);
                                bPayInvite = true;
                            } else {
                                toggleButton5.setChecked(false);
                                bPayInvite = false;
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(NotificationSettingsActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(NotificationSettingsActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(NotificationSettingsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(NotificationSettingsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.appPrefrencesToolbar);
        toolbarTitle = (TextView) findViewById(R.id.app_preferences_toolbar_title);
        notifyMyFollowers = (TextView) findViewById(R.id.notifyMyFollowers);
        notifyFollowingMemebers = (TextView) findViewById(R.id.notifyFollowingMemebers);
        postMyTeam = (TextView) findViewById(R.id.postMyTeam);
        notifyInvitesForGame = (TextView) findViewById(R.id.notifyInvitesForGame);
        push_notify = (TextView) findViewById(R.id.push_notify);
        notifyContact = (TextView) findViewById(R.id.notifyContact);
        notifyChallenge = (TextView) findViewById(R.id.notifyChallenge);
        toggleButton1 = (ToggleButton) findViewById(R.id.toggleButton1);
        toggleButton2 = (ToggleButton) findViewById(R.id.toggleButton2);
        toggleButton3 = (ToggleButton) findViewById(R.id.toggleButton3);
        toggleButton4 = (ToggleButton) findViewById(R.id.toggleButton4);
        toggleButton5 = (ToggleButton) findViewById(R.id.toggleButton5);
        toggleButton6 = (ToggleButton) findViewById(R.id.toggleButton6);
        toggleButtonpush = (ToggleButton) findViewById(R.id.toggleButtonpush);
        overlay = (View) findViewById(R.id.view_overlay);
        additionalToggles = (LinearLayout) findViewById(R.id.linear_sec);


        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        notifyMyFollowers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        notifyFollowingMemebers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        postMyTeam.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        notifyInvitesForGame.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        notifyContact.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        push_notify.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        notifyChallenge.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toggleButton1:
                if (toggleButton1.isChecked()) {
                    bNewFollower = true;
                    update(bNewFollower, "bNewFollower");
                } else {
                    bNewFollower = false;
                    update(bNewFollower, "bNewFollower");
                }
                break;

            case R.id.toggleButton2:
                if (toggleButton2.isChecked()) {
                    bNewTeam = true;
                    update(bNewTeam, "bNewTeam");
                } else {
                    bNewTeam = false;
                    update(bNewTeam, "bNewTeam");
                }
                break;

            case R.id.toggleButton3:
                if (toggleButton3.isChecked()) {
                    bOpenGame = true;
                    update(bOpenGame, "bOpenGame");
                } else {
                    bOpenGame = false;
                    update(bOpenGame, "bOpenGame");
                }
                break;
            case R.id.toggleButton4:
                if (toggleButton4.isChecked()) {
                    bGameInvite = true;
                    update(bGameInvite, "bGameInvite");
                } else {
                    bGameInvite = false;
                    update(bGameInvite, "bGameInvite");
                }
                break;

            case R.id.toggleButton5:
                if (toggleButton5.isChecked()) {
                    bPayInvite = true;
                    update(bPayInvite, "bPayInvite");
                } else {
                    bPayInvite = false;
                    update(bPayInvite, "bPayInvite");
                }
                break;

            case R.id.toggleButton6:
                if (toggleButton6.isChecked()) {
                    bChallenge = true;
                    update(bChallenge, "bChallenge");
                } else {
                    bChallenge = false;
                    update(bChallenge, "bChallenge");
                }
                break;

            case R.id.toggleButtonpush:
                if (toggleButtonpush.isChecked()) {
                    Log.e("CLICK", toggleButtonpush.isChecked() + "");
                    toggleButton1.setClickable(true);
                    toggleButton2.setClickable(true);
                    toggleButton3.setClickable(true);
                    toggleButton4.setClickable(true);
                    toggleButton5.setClickable(true);
                    toggleButton6.setClickable(true);
                    overlay.setVisibility(View.GONE);
                    additionalToggles.setVisibility(View.VISIBLE);
                    bPushNotification = true;
                    update(bPushNotification, "bPushNotification");
                } else {
                    Log.e("CLICK", toggleButtonpush.isChecked() + "");
                    toggleButton1.setClickable(false);
                    toggleButton2.setClickable(false);
                    toggleButton3.setClickable(false);
                    toggleButton4.setClickable(false);
                    toggleButton5.setClickable(false);
                    toggleButton6.setClickable(false);
                    overlay.setVisibility(View.VISIBLE);
                    additionalToggles.setVisibility(View.GONE);
                    bPushNotification = false;
                    update(bPushNotification, "bPushNotification");
                }
                break;
        }

    }

    private void update(boolean b, String s) {
        Log.e("boolean", b + "");
        Log.e("name", s + "");

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
//            PlaythoraUtility.showProgressDialog(NotificationSettingsActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            if (s.equalsIgnoreCase("bNewFollower")) {
                params.add("new_follower", b + "");
            }
            if (s.equalsIgnoreCase("bNewTeam")) {
                params.add("new_team", b + "");
            }

            if (s.equalsIgnoreCase("bOpenGame")) {
                params.add("open_game", b + "");
            }

            if (s.equalsIgnoreCase("bGameInvite")) {
                params.add("game_invite", b + "");
            }

            if (s.equalsIgnoreCase("bPayInvite")) {
                params.add("game_pay_invite", b + "");
            }

            if (s.equalsIgnoreCase("bPushNotification")) {
                params.add("push_notification", b + "");
            }

            if (s.equalsIgnoreCase("bChallenge")) {
                params.add("challenge_game", b + "");
            }


            httpClient.post(Config.NOTIFICATION_SETTING_UPDATE, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("notificationSuccess", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statuscode") == 200) {
//                             Toast.makeText(NotificationSettingsActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(NotificationSettingsActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(NotificationSettingsActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(NotificationSettingsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(NotificationSettingsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }
}
