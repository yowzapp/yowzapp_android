package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class CreateAddressActivity extends BaseActivity {
    Toolbar toolbar;
    EditText emailText, pinText, addressOneText, addressTwoText, cityText, phoneText;
    Button redeem, saveAddress;
    TextView title, cartTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);


        toolbar = (Toolbar) findViewById(R.id.toolbar_address);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        IntializeViews();
        title.setText("Address");
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (emailText.getText().toString().length() == 0) {

                    emailText.setError("Please provide an email id");
                    return;

                }
                if (emailText.getText().toString().length() != 0) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailText.getText().toString()).matches()) {
                        emailText.setError("Please provide valid email id");
                        return;
                    }
                }

                if (pinText.getText().toString().length() == 0) {
                    pinText.setError("Please enter the pincode");
                    return;
                }
                if (addressOneText.getText().toString().length() == 0) {
                    addressOneText.setError("Please enter the area name");
                    return;
                }

                if (addressTwoText.getText().toString().length() == 0) {
                    addressTwoText.setError("Please enter the landmark");
                    return;
                }

                if (cityText.getText().toString().length() == 0) {
                    cityText.setError("Please enter the street name");
                    return;
                }
                if (phoneText.getText().toString().length() != 10) {
                    phoneText.setError("Please enter the mobile number");
                    return;
                }

            }
        });
        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (emailText.getText().toString().length() == 0) {

                    emailText.setError("Please provide an email id");
                    return;

                }
                if (emailText.getText().toString().length() != 0) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailText.getText().toString()).matches()) {
                        emailText.setError("Please provide valid email id");
                        return;
                    }
                }

                if (pinText.getText().toString().length() == 0) {
                    pinText.setError("Please enter the pincode");
                    return;
                }
                if (addressOneText.getText().toString().length() == 0) {
                    addressOneText.setError("Please enter the area name");
                    return;
                }
                if (addressTwoText.getText().toString().length() == 0) {
                    addressTwoText.setError("Please enter the landmark");
                    return;
                }

                if (cityText.getText().toString().length() == 0) {
                    cityText.setError("Please enter the street name");
                    return;
                }
                if (phoneText.getText().toString().length() != 10) {
                    phoneText.setError("Please enter the mobile number");
                    return;
                }

                Intent login = new Intent(CreateAddressActivity.this, RewardSuccessActivity.class);
                startActivity(login);
            }
        });


    }

    private void IntializeViews() {
        title = (TextView) findViewById(R.id.toolbar_title);
        cartTitle = (TextView) findViewById(R.id.address_text);
        redeem = (Button) findViewById(R.id.redeem_btn);
        saveAddress = (Button) findViewById(R.id.save_address);

        emailText = (EditText) findViewById(R.id.player_email);
        TextInputLayout emailInput = (TextInputLayout) findViewById(R.id.input_layout_name);
        pinText = (EditText) findViewById(R.id.pin_code);
        TextInputLayout pinInput = (TextInputLayout) findViewById(R.id.input_layout_pin);
        addressOneText = (EditText) findViewById(R.id.address_one);
        TextInputLayout addressoneInput = (TextInputLayout) findViewById(R.id.input_layout_street);
        addressTwoText = (EditText) findViewById(R.id.address_two);
        TextInputLayout addresstwoInput = (TextInputLayout) findViewById(R.id.input_layout_area);
        cityText = (EditText) findViewById(R.id.city_state);
        TextInputLayout cityInput = (TextInputLayout) findViewById(R.id.input_layout_city);
        phoneText = (EditText) findViewById(R.id.edit_phone);
        TextInputLayout phoneInput = (TextInputLayout) findViewById(R.id.input_layout_phone);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        cartTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        redeem.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        saveAddress.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        emailText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        emailInput.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        addressOneText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        addressoneInput.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        addressTwoText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        addresstwoInput.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        pinInput.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        pinText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        cityInput.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        cityText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        phoneText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        phoneInput.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
    }
}
