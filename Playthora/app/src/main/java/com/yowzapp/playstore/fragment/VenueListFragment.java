package com.yowzapp.playstore.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.viewpagerindicator.CirclePageIndicator;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGameGroundSlots;
import com.yowzapp.playstore.activty.HomeScreenActivity;
import com.yowzapp.playstore.model.GroundListItem;
import com.yowzapp.playstore.model.HomeTopLocationModel;
import com.yowzapp.playstore.model.VenueListItem;
import com.yowzapp.playstore.model.VenueListModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.LoopViewPager;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by hemanth on 24/11/16.
 */
public class VenueListFragment extends Fragment {

    public ArrayList<HomeTopLocationModel> selectLocationArrayList;
    public VenueListAdapter venueListAdapter;
    RecyclerView recyclerView, recyclerGame, recyclerAmenities, recyclerGrounds;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    String gName, gLocation, gImage, gDistance, gSports, gAmenities, groundList;
    String gSportsName = "";
    int gRating;
    ArrayList<String> gPictures;
    ImageView venueImage, locationGps;
    LoopViewPager imageViewPager;
    CirclePageIndicator indicator;
    TextView groundName, groundLocation, amenities, rateThisPlace, txtGround, amenitiesText; //,groundDistance
    Button book, cancel;
    PreferenceManager mPref;
    VenueListModel venueListModel;
    List<VenueListItem> tempList;
    Gson gson;
    RatingBar rating;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    JSONObject jsobj;
    JSONArray jsArray;
    ArrayList<String> selectGroundsArray = new ArrayList<>();
    List<GroundListItem> gList = new ArrayList<>();
    Boolean[] selectedGround;
    int j;
    ArrayList<String> groundId = new ArrayList<>();
    String sGround = "";
    String sVenueImage, sName, sGroundName;
    SwipeRefreshLayout mSwipeRefreshLayout;
    String VenueResponse;
    List<GroundListItem> dummy;
    JSONArray jsonArray = new JSONArray();
    private int visibleThreshold = 3;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View conView = inflater.inflate(R.layout.ground_list_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) conView.findViewById(R.id.groundListRecyclerView);


        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        mSwipeRefreshLayout = (SwipeRefreshLayout) conView.findViewById(R.id.location_refresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    populateVenueListRecyclerView(1);
                    previousTotal = 0;
                    pages = 1;
                    visibleThreshold = 3;
                } catch (Exception e) {
                    e.printStackTrace();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });


        try {
            populateVenueListRecyclerView(pages);
        } catch (Exception e) {
            e.printStackTrace();
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
               /* visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if(venueListModel.isHasMorePages()){
                    if(loading){
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        pages+=1;
                        venueListAdapter.loadData(null);
                        venueListAdapter.notifyDataSetChanged();

                    }
                }}
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        Log.e("SIZE", "end called");
                        loading = true;
                        try {
                            populateVenueListRecyclerView(pages);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Do something
                    }*/
                // Log.e("SCROLLING","onScrolled");
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("SCROLLING", "onScrollStateChanged");
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (venueListModel.isHasMorePages()) {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                            pages += 1;
                            //venueListAdapter.loadData(venueListModel.getSuccess(),1);
                            try {
                                populateVenueListRecyclerView(pages);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            venueListAdapter.notifyDataSetChanged();

                        }
                    }
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }

            }
        });

        return conView;
    }

    private void populateVenueListRecyclerView(int pages) throws Exception {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            //PlaythoraUtility.showProgressDialog(getActivity());
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            httpClient.get(Config.VENUE_LIST + "?page=" + pages, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    //PlaythoraUtility.hideProgressDialog();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        VenueResponse = new String(responseBody);
                        Log.v("VENUELIST", VenueResponse);
                      /*jsobj= new JSONObject(s);
                         JSONArray jsArrayTemp= new JSONArray();
                        jsArrayTemp=jsobj.getJSONArray("success");
                        for(int i=0;i<jsArrayTemp.length();i++){
                            jsArray.put(jsArrayTemp.get(i));
                        }*/
                        venueListModel = new VenueListModel();
                        gson = new Gson();
                        venueListModel = gson.fromJson(VenueResponse, VenueListModel.class);
                        if (venueListModel.getSuccess().size() != 0) {

                            if (venueListModel.getCurrentPage() > 1) {
                                for (int i = 0; i < venueListModel.getSuccess().size(); i++) {
                                    tempList.add(venueListModel.getSuccess().get(i));
                                }
                                JSONObject jsonObject = new JSONObject(VenueResponse);
                                JSONArray tempJsonArray = new JSONArray();
                                tempJsonArray = jsonObject.getJSONArray("success");
                                for (int j = 0; j < tempJsonArray.length(); j++) {
                                    jsonArray.put(tempJsonArray.get(j));
                                }
                                Log.e("venueList", String.valueOf(venueListModel.getCurrentPage()));
                                venueListAdapter.loadData(tempList, 2);
                            } else {
                                tempList = venueListModel.getSuccess();
                                JSONObject jsonObject = new JSONObject(VenueResponse);
                                jsonArray = jsonObject.getJSONArray("success");
                                venueListAdapter = new VenueListAdapter(getActivity(), venueListModel.getSuccess());
                                recyclerView.setAdapter(venueListAdapter);
                            }
                        }

                        for (int i = 0; i < venueListModel.getSuccess().size(); i++) {
                            //Log.e("LIST", venueListModel.getSuccess().get(i).getCover_pic());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    //PlaythoraUtility.hideProgressDialog();
                    mSwipeRefreshLayout.setRefreshing(false);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    public void groundDetailDialog(final Activity activity, final String gName, final String gSports, final String gAmenities, final String gLocation,
                                   final String gImage, String gDist, final int gRating, final int position,
                                   List<GroundListItem> templist, final String response, ArrayList<String> venuePictures, boolean fromList, boolean fromTopLoc) {

        final Dialog phoneDialog = new Dialog(activity);
        phoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // phoneDialog.getWindow().setBackgroundDrawable(new ColorDrawable(yowzapp.graphics.Color.TRANSPARENT));
        phoneDialog.setContentView(R.layout.ground_detail_dialog_layout);


        final Window window = phoneDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // window.setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        //wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        sGround = "";

        Log.e("name", gName);
        Log.e("loc", gLocation);
        Log.e("img", gImage);
        Log.e("distance", gDist);
        Log.e("rating", String.valueOf(gRating));
        Log.e("posi", String.valueOf(position));
        Log.e("response", response);

        venueImage = (ImageView) phoneDialog.findViewById(R.id.gImage);
        groundName = (TextView) phoneDialog.findViewById(R.id.gName);
        rateThisPlace = (TextView) phoneDialog.findViewById(R.id.rateThisPlace);
        groundLocation = (TextView) phoneDialog.findViewById(R.id.gLocation);
        book = (Button) phoneDialog.findViewById(R.id.button_book);
        cancel = (Button) phoneDialog.findViewById(R.id.cancel_button);
        amenities = (TextView) phoneDialog.findViewById(R.id.amenities);
        txtGround = (TextView) phoneDialog.findViewById(R.id.ground_text);
        recyclerGame = (RecyclerView) phoneDialog.findViewById(R.id.ground_games);
        recyclerAmenities = (RecyclerView) phoneDialog.findViewById(R.id.ground_amenities);
        recyclerGrounds = (RecyclerView) phoneDialog.findViewById(R.id.grounds);
        rating = (RatingBar) phoneDialog.findViewById(R.id.dialog_rating);
//        groundDistance = (TextView) phoneDialog.findViewById(R.id.distance);
        amenitiesText = (TextView) phoneDialog.findViewById(R.id.amenitiesText);
        locationGps = (ImageView) phoneDialog.findViewById(R.id.locationGps);
        imageViewPager = (LoopViewPager) phoneDialog.findViewById(R.id.image_viewpager);
        indicator = (CirclePageIndicator) phoneDialog.findViewById(R.id.circle_indicator_image);

        amenitiesText.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_REGULAR));

//        if(gDist.toString().equalsIgnoreCase("0.0 km")){
//            groundDistance.setVisibility(View.GONE);
//        }else {
//            groundDistance.setVisibility(View.VISIBLE);
//            groundDistance.setText(gDist);
//        }


        rating.setRating(gRating);

        final String[] names = gSports.split(",");
        GamesAdapter adapter = new GamesAdapter(names);
        recyclerGame.setHasFixedSize(true);
        recyclerGame.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        recyclerGame.setAdapter(adapter);

        String[] amenitiesList = gAmenities.split(",");
      /*  AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(amenitiesList);
        recyclerAmenities.setHasFixedSize(true);
        recyclerAmenities.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerAmenities.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerAmenities.setAdapter(amenitiesAdapter);
        */
        for (int i = 0; i < amenitiesList.length; i++) {
            if ((i % 2) == 0) {
                amenitiesText.setText(amenitiesText.getText().toString() + amenitiesList[i] + "   ");
            } else {
                amenitiesText.setText(amenitiesText.getText().toString() + amenitiesList[i] + "\n");
            }

        }

        if (venuePictures.size() != 0) {
            ImageAdapter imageAdapter = new ImageAdapter(activity, venuePictures);
            imageViewPager.setAdapter(imageAdapter);
            imageViewPager.setCurrentItem(0);
            indicator.setViewPager(imageViewPager);
            indicator.setStrokeWidth(0);
        }

        // String[] gList = groundList.split(",");

        if (fromList) {
            GroundAdapter groundAdapter = new GroundAdapter(tempList.get(position).getGrounds());
            recyclerGrounds.setHasFixedSize(true);
            recyclerGrounds.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            recyclerGrounds.setAdapter(groundAdapter);
        } else {
            mPref = PreferenceManager.instance(activity);
            GroundAdapter groundAdapter = new GroundAdapter(templist);
            recyclerGrounds.setHasFixedSize(true);
            recyclerGrounds.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            recyclerGrounds.setAdapter(groundAdapter);
        }


        groundName.setText(gName);
        groundLocation.setText(gLocation);
        if (gLocation.isEmpty())
            locationGps.setVisibility(View.GONE);
        else locationGps.setVisibility(View.VISIBLE);

        try {
            Glide.with(activity).load(gImage).error(R.drawable.test_location).error(R.drawable.test_location).centerCrop().into(venueImage);
        } catch (Exception e) {
            venueImage.setImageResource(R.drawable.test_location);
        }


        groundName.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_BOLD));
        groundLocation.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_REGULAR));
        book.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_REGULAR));
        amenities.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_REGULAR));
        txtGround.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_REGULAR));
        rateThisPlace.setTypeface(PlaythoraUtility.getFont(activity, Config.RALEWAY_REGULAR));
        String grounds = "";
        String images = "";
        if (fromList) {
            try {
                JSONObject joj = new JSONObject(response);
                //JSONArray jarray = new JSONArray(response);
                //JSONObject venueObject = joj.getJSONObject(position);
                grounds = joj.getJSONArray("grounds").toString();
                Log.e("%%%%%%%%%%", joj.getJSONArray("grounds").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                JSONObject joj = new JSONObject(response);
                //JSONArray jarray = new JSONArray(response);
                //JSONObject venueObject = joj.getJSONObject(position);
                images = joj.getJSONArray("images").toString();
                Log.e("imagessssss", joj.getJSONArray("images").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject joj = new JSONObject(response);
                //JSONArray jarray = new JSONArray(response);
                //JSONObject venueObject = jarray.getJSONObject(position);
                grounds = joj.getJSONObject("venue_details").getJSONArray("grounds").toString();
                Log.e("%%%%%%%%%%", grounds);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                JSONObject joj = new JSONObject(response);
                //JSONArray jarray = new JSONArray(response);
                //JSONObject venueObject = joj.getJSONObject(position);
                images = joj.getJSONObject("venue_details").getJSONArray("images").toString();
                Log.e("imagessssss", joj.getJSONArray("images").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (fromTopLoc) {
            try {
                JSONObject joj = new JSONObject(response);
                //JSONArray jarray = new JSONArray(response);
                //JSONObject venueObject = jarray.getJSONObject(position);
                grounds = joj.getJSONArray("grounds").toString();
                Log.e("%%%%%%%%%%", grounds);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                JSONObject joj = new JSONObject(response);
                //JSONArray jarray = new JSONArray(response);
                //JSONObject venueObject = joj.getJSONObject(position);
                images = joj.getJSONArray("images").toString();
                Log.e("imagessssss", joj.getJSONArray("images").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final String finalGrounds = grounds;
        final String finalImages = images;
        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mPref.getAccessToken().isEmpty()) {
                    phoneDialog.dismiss();
                    Intent intent = new Intent(activity, CreateGameGroundSlots.class);
                    intent.putExtra("gID", sGround);
                    intent.putExtra("gName", sGroundName);
                    intent.putExtra("vName", gName);
                    intent.putExtra("vLocation", gLocation);
                    intent.putExtra("venueCover", gImage);
                    intent.putExtra("Rating", gRating);
                    intent.putExtra("venue", "pay");
                    try {
                        intent.putExtra("GroundArrayList", finalGrounds);
                        intent.putExtra("IMAGES", finalImages);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("%%%%%%%%%%", finalGrounds);

                    activity.startActivity(intent);
                } else PlaythoraUtility.LoginDialog(activity);
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneDialog.dismiss();
            }
        });

        phoneDialog.show();

    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public RelativeLayout relativeLayout;
        public ImageView venueImage;


        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            relativeLayout = (RelativeLayout) v.findViewById(R.id.relative_ground);
            this.venueImage = (ImageView) v.findViewById(R.id.groundImage);
        }
    }

    public class VenueListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<VenueListItem> arrayList;
        private Context context;
        private int items;

        public VenueListAdapter(Context context, List<VenueListItem> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_list_item_layout, viewGroup, false);
            GroundListViewHolder listHolder = new GroundListViewHolder(mainGroup);
            return listHolder;

        }

        public void loadData(List<VenueListItem> arrayList, int i) {
            Log.e("LISTING", "LISTING");
            this.arrayList = arrayList;
            notifyDataSetChanged();
            // else this.arrayList = null;
        }


        @Override
        public int getItemCount() {
            items = arrayList.size();
            //Log.e("LISTSIZE", String.valueOf(items));
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final VenueListItem model = arrayList.get(position);

            final GroundListViewHolder mainHolder = (GroundListViewHolder) holder;
            if (model != null) {
                mainHolder.progressBar.setVisibility(View.GONE);
                try {
                    mainHolder.groundName.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_SEMIBOLD));
                    mainHolder.groundLocationName.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                    mainHolder.kms.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                    mainHolder.groundName.setText(model.getName());
                    mainHolder.groundLocationName.setText(model.getLocation_name());
                    mainHolder.rating.setRating(model.getRating());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    // Log.v("Ground", model.getCover_pic());
                    Glide.with(context).load(model.getCover_pic()).error(R.drawable.test_location).centerCrop().into(mainHolder.venueImage);
                } catch (Exception e) {
                    mainHolder.venueImage.setImageResource(R.drawable.test_location);
                }

                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
                mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            gName = model.getName();
                            gLocation = model.getLocation_name();
                            gImage = model.getCover_pic();
                            gDistance = model.getDistance();
                            gAmenities = model.getAmenities();
                            gSports = model.getSports_types();
                            gRating = model.getRating();
                            gPictures = model.getImages();

                            groundDetailDialog(getActivity(), gName, gSports, gAmenities, gLocation, gImage, gDistance, gRating, position, dummy, jsonArray.get(position).toString(), gPictures, true, false);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

                mainHolder.itemView.setTag(model);
            } else {
                mainHolder.progressBar.setVisibility(View.VISIBLE);
                mainHolder.venueImage.setVisibility(View.GONE);
                mainHolder.relativeLayout.setVisibility(View.GONE);
            }

            /*else {
                ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;

                progressViewHolder.venueImage.setVisibility(View.GONE);
                progressViewHolder.relativeLayout.setVisibility(View.GONE);
                progressViewHolder.progressBar.setVisibility(View.VISIBLE);

            }*/


        }


    }

    public class GroundListViewHolder extends RecyclerView.ViewHolder {


        public TextView groundName, groundLocationName, kms;
        public RelativeLayout gameLocationLayout, relativeLayout;
        public ImageView venueImage;
        public RatingBar rating;
        public ProgressBar progressBar;


        public GroundListViewHolder(View view) {
            super(view);

            this.groundName = (TextView) view.findViewById(R.id.groundName);
            this.groundLocationName = (TextView) view.findViewById(R.id.placeName);
            this.kms = (TextView) view.findViewById(R.id.kms);
            this.gameLocationLayout = (RelativeLayout) view.findViewById(R.id.gameListLayout);
            this.venueImage = (ImageView) view.findViewById(R.id.groundImage);
            this.rating = (RatingBar) view.findViewById(R.id.rating);
            progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_ground);

        }
    }

    public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.GamesHolder> {
        String[] names;

        public GamesAdapter(String[] names) {
            this.names = names;
        }

        @Override
        public GamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_games_list, parent, false);
            GamesHolder listHolder = new GamesHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(GamesHolder holder, int position) {
            if (!names[0].toString().isEmpty())
                holder.sportsname.setText(names[position]);
            else holder.sportsname.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return names.length;
        }

        public class GamesHolder extends RecyclerView.ViewHolder {
            TextView sportsname;

            public GamesHolder(View itemView) {
                super(itemView);
                sportsname = (TextView) itemView.findViewById(R.id.gameName);
                sportsname.setTypeface(PlaythoraUtility.getFont(itemView.getContext(), Config.RALEWAY_REGULAR));
            }
        }

    }

    public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.AmenitiesHolder> {
        String[] amenitiesList;

        public AmenitiesAdapter(String[] amenitiesList) {
            this.amenitiesList = amenitiesList;
        }

        @Override
        public AmenitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_dialog_item, parent, false);
            AmenitiesHolder listHolder = new AmenitiesHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(AmenitiesHolder holder, int position) {
            holder.names.setText(amenitiesList[position]);
        }

        @Override
        public int getItemCount() {
            return amenitiesList.length;
        }

        public class AmenitiesHolder extends RecyclerView.ViewHolder {
            TextView names;

            public AmenitiesHolder(View itemView) {
                super(itemView);
                names = (TextView) itemView.findViewById(R.id.amenitiessub);
                names.setTypeface(PlaythoraUtility.getFont(itemView.getContext(), Config.RALEWAY_REGULAR));
            }
        }

    }


    public class GroundAdapter extends RecyclerView.Adapter<GroundAdapter.GroundHolder> {
        List<GroundListItem> gList;
        Boolean[] selectedGround;

        public GroundAdapter(List<GroundListItem> gList) {
            this.gList = gList;
            j = gList.size();
            selectedGround = new Boolean[j];
            for (int i = 0; i < j; i++) {
                selectedGround[i] = false;
            }
        }

        @Override
        public GroundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.ground_item, parent, false);
            GroundHolder listHolder = new GroundHolder(mainGroup);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(final GroundHolder holder, final int position) {
            if (!gList.get(0).toString().isEmpty()) {
                holder.groundName.setText(gList.get(position).getName());
            } else {
                holder.groundName.setVisibility(View.GONE);
            }

            if (!selectedGround[position]) {
                holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                holder.groundName.setTextColor(getResources().getColor(R.color.text_one));
            } else {
                holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                holder.groundName.setTextColor(getResources().getColor(R.color.white));
            }

            holder.groundName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    for (int i = 0; i < j; i++) {
                        selectedGround[i] = false;
                        if (!selectedGround[i]) {
                            holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                        }
                    }
                    sGround = String.valueOf(gList.get(position).getId());
                    sGroundName = gList.get(position).getName();


                    if (!selectedGround[position]) {
                        selectedGround[position] = true;
                        holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.create_game_type_of_game));
                        holder.groundName.setTextColor(getResources().getColor(R.color.text_one));
                        notifyDataSetChanged();
                        selectGroundsArray.add(String.valueOf(gList.get(position).getId()));
                        Log.e("GroundID", String.valueOf(gList.get(position).getId()));

                    } else {
                        selectedGround[position] = false;
                        holder.groundName.setBackgroundDrawable(getResources().getDrawable(R.drawable.leader_board_items));
                        holder.groundName.setTextColor(getResources().getColor(R.color.white));
                        notifyDataSetChanged();
                        selectGroundsArray.remove(gList.get(position).getId());
                        Log.e("GroundID", String.valueOf(gList.get(position).getId()));
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return gList.size();
        }

        public class GroundHolder extends RecyclerView.ViewHolder {
            TextView groundName;

            public GroundHolder(View itemView) {
                super(itemView);
                groundName = (TextView) itemView.findViewById(R.id.groundName);
                groundName.setTypeface(PlaythoraUtility.getFont(itemView.getContext(), Config.RALEWAY_REGULAR));
            }
        }
    }

    private class ImageAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        ArrayList<String> images;
        Context context;
        String imageUrl;

        public ImageAdapter(Context context, ArrayList<String> images) {
            this.context = context;
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            View itemView = mLayoutInflater.inflate(R.layout.image_view_layout, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.images);

            if (PlaythoraUtility.checkInternetConnection(context)) {
                try {
                    imageUrl = images.get(position);
                    Glide.with(context).load(imageUrl).centerCrop().into(imageView);
                    container.addView(itemView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

}
