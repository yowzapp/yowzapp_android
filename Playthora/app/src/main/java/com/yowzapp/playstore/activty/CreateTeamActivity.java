package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.BuildConfig;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.MyProfileLeaderBoardModel;
import com.yowzapp.playstore.model.SportsListModel;
import com.yowzapp.playstore.model.SportsSuccessList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

/**
 * Created by hemanth on 14/11/16.
 */

public class CreateTeamActivity extends BaseActivity {

    private static final int PICK_FROM_CAM = 1;
    public TeamMembersRecyclerAdapter adapter;
    public SportsRecyclerViewAdapter sportsRecyclerAdapter;
    public ArrayList<MyProfileLeaderBoardModel> sportsArrayList;
    Toolbar toolbar;
    TextView toolbarTitle, teamMembers, uploadText, typeSports, addMembersText;
    RecyclerView teamMembersRecyclerView, sportsTypeRecyclerView;
    RelativeLayout relative;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    ArrayList<AddTeamMembersList> addMembersArrayList;
    RelativeLayout coverPictureLayout, addMembersTextLayout;
    ImageView coverPhoto, cameraImage, addMembersImage;
    EditText teamName, description;
    Button btnCreate;
    TextInputLayout nameBoxInputLayout, descriptionBoxInputLayout;
    int j;
    PreferenceManager mPref;
    ProgressBar progressBar, imageProgressBar;
    SportsListModel sportsListModel;
    int serverResponseCode = 0;
    String image = "";
    Boolean[] sportsType;
    Toast toast;
    private int PICK_IMAGE_REQUEST = 2;
    private String responseString;
    private String ImagePath;
    private File output;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.create_team_layout);
        // CreateTeamActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        //CreateTeamActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mPref = PreferenceManager.instance(this);
        toolbar = (Toolbar) findViewById(R.id.createTeamToolbar);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        InitializeAll();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        addMembersArrayList = new ArrayList<>();

        teamMembersRecyclerView.setLayoutManager(new LinearLayoutManager(CreateTeamActivity.this, LinearLayoutManager.HORIZONTAL, false));
        teamMembersRecyclerView.setHasFixedSize(true);
        sportsTypeRecyclerView.setLayoutManager(new LinearLayoutManager(CreateTeamActivity.this, LinearLayoutManager.HORIZONTAL, false));
        sportsTypeRecyclerView.setHasFixedSize(true);

       /* try {
          //  populateTeamMembers();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            populateSportsTypes();
        } catch (Exception e) {
            e.printStackTrace();
        }


        coverPictureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shouldAskPermission()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                        int permsRequestCode = 200;

                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    CoverPhoto();
                }
            }
        });

        addMembersTextLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateTeamActivity.this, AddMembersActivity.class);
                intent.putExtra("TEMPLIST", "CREATE");
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("mylist", addMembersArrayList);
                intent.putExtras(bundle);
                startActivityForResult(intent, 3);
            }
        });

        addMembersImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CreateTeamActivity.this, AddMembersActivity.class);
                i.putExtra("TEMPLIST", "CREATE");
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("mylist", addMembersArrayList);
                i.putExtras(bundle);
                startActivityForResult(i, 3);
            }
        });


        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONArray jsonArraySports = new JSONArray();
                JSONArray jsonArrayMembers = new JSONArray();
                if (checkForAllValidation()) {
                    if (toast != null) {
                        toast.cancel();
                    }
                    if (addMembersArrayList.isEmpty()) {
                        toast = Toast.makeText(CreateTeamActivity.this, "Add members", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        for (int i = 0; i < sportsListModel.getSuccess().size(); i++) {
                            if (sportsType[i])
                                jsonArraySports.put(sportsListModel.getSuccess().get(i).getId());
                        }
                        Log.e("SPORTSIDS", jsonArraySports.toString());
                        if (jsonArraySports.length() == 0) {
                            toast = Toast.makeText(CreateTeamActivity.this, "Add sports", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {

                            for (int i = 0; i < addMembersArrayList.size(); i++) {
                                jsonArrayMembers.put(addMembersArrayList.get(i).getId());
                            }
                            Log.e("MEMBERIDS", jsonArrayMembers.toString());
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("members", jsonArrayMembers);
                                jsonObject.put("sports", jsonArraySports);
                                jsonObject.put("name", teamName.getText().toString().trim());
                                jsonObject.put("cover_pic", image);
                                jsonObject.put("discription", description.getText().toString().trim());
                                Log.e("JSON", jsonObject.toString());
                                CreateTeam(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        teamName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                nameBoxInputLayout.setError(null);
            }
        });
    }

    private void CreateTeam(JSONObject jsonObject) {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(CreateTeamActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("JSONOBJ", se.toString());
                mHttpClient.post(getApplicationContext(), Config.CREATE_TEAM, se, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Intent intent = new Intent(CreateTeamActivity.this, CreateTeamSuccessActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();

                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if (permsRequestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm", "request");
                CoverPhoto();
            } else {
                Toast.makeText(CreateTeamActivity.this, "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private boolean checkForAllValidation() {

        if (toast != null) {
            toast.cancel();
        }

        if (teamName.getText().toString().isEmpty()) {
            nameBoxInputLayout.setError("Please provide team name");
            nameBoxInputLayout.setErrorEnabled(true);
        } else {
            nameBoxInputLayout.setErrorEnabled(false);
        }

//        if (image.isEmpty()) {
//            toast = Toast.makeText(CreateTeamActivity.this, "Please add image", Toast.LENGTH_SHORT);
//            toast.show();
//            return false;
//        }
       /* if(description.getText().toString().isEmpty() || description.getText().toString().trim().isEmpty()){
            descriptionBoxInputLayout.setError("Please provide the description of team");
            descriptionBoxInputLayout.setErrorEnabled(true);
        }else {
            descriptionBoxInputLayout.setErrorEnabled(false);
        }*/

        if (nameBoxInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }

    }

    private void CoverPhoto() {

        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateTeamActivity.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateTeamActivity.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(CreateTeamActivity.this,
                                    getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    private boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (data == null) {
//            return;
//        }

        if (requestCode == 3) {
            // Log.e("#######",AddMembersActivity.stringList.size()+"");
            //  Log.e("@@@@@@@",addMembersArrayList.size()+"");
            Bundle bundle = data.getExtras();
            if(bundle.containsKey("mylist")) {
//                addMembersArrayList = new ArrayList<>();
                addMembersArrayList = bundle.getParcelableArrayList("mylist");
                if (addMembersArrayList.size() > 0) {

                    if (adapter != null) {
                        adapter.refresh(addMembersArrayList);
                        Log.e("@@@@@@@", addMembersArrayList.size() + "");
                    } else {
                        adapter = new TeamMembersRecyclerAdapter(CreateTeamActivity.this, addMembersArrayList);
                        Log.e("@@@@@@", addMembersArrayList.size() + "");
                        teamMembersRecyclerView.setAdapter(adapter);// set adapter on recyclerview
                        teamMembersRecyclerView.setVisibility(View.VISIBLE);
                        teamMembers.setVisibility(View.VISIBLE);
                        addMembersImage.setVisibility(View.VISIBLE);
                        addMembersTextLayout.setVisibility(View.GONE);

                    }
                } else {
                    adapter = null;
                    teamMembersRecyclerView.setVisibility(View.INVISIBLE);
                    addMembersTextLayout.setVisibility(View.VISIBLE);
                    teamMembers.setVisibility(View.GONE);
                    addMembersImage.setVisibility(View.GONE);
                }
            }
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                try {
                    ImagePath = getPath(selectedImageUri);
                } catch (NullPointerException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {
                    ExifInterface exif = new ExifInterface(ImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation = 0;
                    if (rotation == 0) {
                        if (orientation == 6) {
                            rotation = 90;
                        }
                        if (orientation == 3) {
                            rotation = 180;
                        }
                        if (orientation == 8) {
                            rotation = 270;
                        }
                        if (orientation == 4) {
                            rotation = 180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(ImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    coverPhoto.setImageBitmap(bm);
                    cameraImage.setVisibility(View.GONE);
                    uploadText.setVisibility(View.GONE);
                    imageProgressBar.setVisibility(View.VISIBLE);
                    coverPictureLayout.setClickable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(ImagePath);
                            } catch (Exception e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        coverPictureLayout.setClickable(true);
                                        imageProgressBar.setVisibility(View.GONE);
                                        coverPhoto.setVisibility(View.GONE);
                                        cameraImage.setVisibility(View.VISIBLE);
                                        uploadText.setVisibility(View.VISIBLE);
                                        Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {


                ImagePath = getRealPathFromURI(Uri.fromFile(output).toString(), CreateTeamActivity.this);
                Log.e("selected Image", ImagePath);

                File myFile = new File(ImagePath);
                try {
                    myFile = new Compressor(this).compressToFile(myFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ImagePath = myFile.getAbsolutePath();

                Glide.with(CreateTeamActivity.this).load(ImagePath).into(coverPhoto);


//                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
//                final Uri mImageCaptureUri = Uri.fromFile(destination);
//
//                System.out.println("Image Path : " + mImageCaptureUri);
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                ImagePath = getRealPathFromURI(mImageCaptureUri.toString(), CreateTeamActivity.this);
//                try {
//                    ExifInterface exif = new ExifInterface(ImagePath);
//                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//                    Log.e("ROTATE", orientation + "");
//                    Bitmap bm;
//                    int rotation = 0;
//                    if (rotation == 0) {
//                        if (orientation == 6) {
//                            rotation = 90;
//                        }
//                        if (orientation == 3) {
//                            rotation = 180;
//                        }
//                        if (orientation == 8) {
//                            rotation = 270;
//                        }
//                        if (orientation == 4) {
//                            rotation = 180;
//                        }
//
//                    }
//                    Log.e("ooooooo", String.valueOf(currentapiVersion));
//                    Matrix matrix = new Matrix();
//                    matrix.postRotate(rotation);
//                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(ImagePath), 100, 100, true);
//                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
//                    coverPhoto.setImageBitmap(bm);
                try{
                    cameraImage.setVisibility(View.GONE);
                    uploadText.setVisibility(View.GONE);
                    imageProgressBar.setVisibility(View.VISIBLE);
                    coverPictureLayout.setClickable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(ImagePath);
                            } catch (Exception e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        coverPictureLayout.setClickable(true);
                                        imageProgressBar.setVisibility(View.GONE);
                                        coverPhoto.setVisibility(View.GONE);
                                        cameraImage.setVisibility(View.VISIBLE);
                                        uploadText.setVisibility(View.VISIBLE);
                                        Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    }).start();

//                } catch (IOException e) {
//                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private void uploadFile(final String sourceFileUri) {
        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File sourceFile = new File(PlaythoraUtility.compressImage(sourceFileUri, CreateTeamActivity.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :" + sourceFileUri);

            runOnUiThread(new Runnable() {
                public void run() {
                    coverPictureLayout.setClickable(true);
                    imageProgressBar.setVisibility(View.GONE);
                    coverPhoto.setVisibility(View.GONE);
                    cameraImage.setVisibility(View.VISIBLE);
                    uploadText.setVisibility(View.VISIBLE);
                    Toast.makeText(CreateTeamActivity.this, "Source File not exist :" + sourceFileUri, Toast.LENGTH_SHORT).show();
                }
            });

            return;

        } else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.UPLOAD_IMAGE);
                PlaythoraUtility.trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                        + fileName + "\"" + lineEnd); //image is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS", "Content-Disposition: form-data; name=\"profile_pic\";filename=\"" + fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            coverPictureLayout.setClickable(true);
                            JSONObject response = null;
                            try {
                                response = new JSONObject(total.toString());
                                image = response.getString("success");
                                Glide.with(CreateTeamActivity.this).load(image).error(R.drawable.no_team).centerCrop().into(coverPhoto);
                                imageProgressBar.setVisibility(View.GONE);
                                //Toast.makeText(CreateTeamActivity.this, "Upload image Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                coverPhoto.setImageResource(R.drawable.no_team);
                            }
                        }
                    });
                } else if (serverResponseCode == 400 || serverResponseCode == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            coverPictureLayout.setClickable(true);
                            imageProgressBar.setVisibility(View.GONE);
                            coverPhoto.setVisibility(View.GONE);
                            cameraImage.setVisibility(View.VISIBLE);
                            uploadText.setVisibility(View.VISIBLE);
                        }
                    });
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    /*JSONObject object = null;
                    mPref.setAccessToken("");
                    Intent intent = new Intent(CreateTeamActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();*/

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            coverPictureLayout.setClickable(true);
                            imageProgressBar.setVisibility(View.GONE);
                            coverPhoto.setVisibility(View.GONE);
                            cameraImage.setVisibility(View.VISIBLE);
                            uploadText.setVisibility(View.VISIBLE);
                            Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                            //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        coverPictureLayout.setClickable(true);
                        imageProgressBar.setVisibility(View.GONE);
                        coverPhoto.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        uploadText.setVisibility(View.VISIBLE);
                        Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        coverPictureLayout.setClickable(true);
                        imageProgressBar.setVisibility(View.GONE);
                        coverPhoto.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        uploadText.setVisibility(View.VISIBLE);
                        Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            } catch (ProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        coverPictureLayout.setClickable(true);
                        imageProgressBar.setVisibility(View.GONE);
                        coverPhoto.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        uploadText.setVisibility(View.VISIBLE);
                        Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        coverPictureLayout.setClickable(true);
                        imageProgressBar.setVisibility(View.GONE);
                        coverPhoto.setVisibility(View.GONE);
                        cameraImage.setVisibility(View.VISIBLE);
                        uploadText.setVisibility(View.VISIBLE);
                        Toast.makeText(CreateTeamActivity.this, "Couldn't fetch the image try again", Toast.LENGTH_SHORT).show();
                        //Glide.with(CreateTeamActivity.this).load(mPref.getProfilePic()).centerCrop().into(profileImage);
                    }
                });
            }
        }

    }

    private String getRealPathFromURI(String s, Context context) {
        Uri contentUri = Uri.parse(s);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private String getPath(Uri selectedImageUri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }

    private void populateSportsTypes() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SPORTS_LIST,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);

                                    sportsListModel = new SportsListModel();
                                    mGson = new Gson();
                                    sportsListModel = mGson.fromJson(s, SportsListModel.class);
                                    Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));

                                    if (sportsListModel.getSuccess().size() != 0) {
                                        Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                                        if (sportsTypeRecyclerView.getAdapter() == null) {
                                            Log.e("SUCCEss", String.valueOf(sportsListModel.getSuccess()));
                                            sportsRecyclerAdapter = new SportsRecyclerViewAdapter(CreateTeamActivity.this, sportsListModel.getSuccess());
                                            sportsTypeRecyclerView.setAdapter(sportsRecyclerAdapter);
                                            sportsRecyclerAdapter.notifyDataSetChanged();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        } else {

//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private void InitializeAll() {

        relative = (RelativeLayout) findViewById(R.id.create_team_relative);
        setupUI(relative);
        toolbarTitle = (TextView) findViewById(R.id.create_team_toolbar_title);
        teamMembersRecyclerView = (RecyclerView) findViewById(R.id.team_members_recycler_view_horizontal);
        sportsTypeRecyclerView = (RecyclerView) findViewById(R.id.sports_recycler_view_horizontal);
        coverPictureLayout = (RelativeLayout) findViewById(R.id.cover_picture_layout);
        coverPhoto = (ImageView) findViewById(R.id.coverPicture);
        addMembersTextLayout = (RelativeLayout) findViewById(R.id.add_member_text_layout);
        teamMembers = (TextView) findViewById(R.id.team_members_text);
        cameraImage = (ImageView) findViewById(R.id.cam_img);
        uploadText = (TextView) findViewById(R.id.uploadCoverPictureText);
        addMembersImage = (ImageView) findViewById(R.id.addMembersImage);
        typeSports = (TextView) findViewById(R.id.text_sports_type);
        addMembersText = (TextView) findViewById(R.id.addMembersText);
        teamName = (EditText) findViewById(R.id.teamName);
        description = (EditText) findViewById(R.id.teamDescription);
        btnCreate = (Button) findViewById(R.id.btn_create_team);
        nameBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_name);
        descriptionBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_description);
        progressBar = (ProgressBar) findViewById(R.id.sports_progress_bar);
        imageProgressBar = (ProgressBar) findViewById(R.id.team_image_progress_bar);

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        teamMembers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        uploadText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        typeSports.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        addMembersText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        teamName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        description.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        btnCreate.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        nameBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        descriptionBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(CreateTeamActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideSoftKeyboard(this);
    }

    public class SportsRecyclerViewAdapter extends RecyclerView.Adapter<SportsViewHolder> {

        private List<SportsSuccessList> arrayList;
        private Context mContext;

        public SportsRecyclerViewAdapter(Context context, List<SportsSuccessList> arrayList) {
            this.mContext = context;
            this.arrayList = arrayList;
            j = arrayList.size();

            sportsType = new Boolean[j];

            for (int i = 0; i < j; i++) {
                sportsType[i] = false;
                Log.e("total", "" + j);

            }
        }

        @Override
        public SportsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.sports_item_layout, viewGroup, false);
            SportsViewHolder listHolder = new SportsViewHolder(mainGroup);
            return listHolder;
        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final SportsViewHolder holder, final int position) {
            final SportsSuccessList model = arrayList.get(position);


            final SportsViewHolder mainHolder = (SportsViewHolder) holder;

            try {
                mainHolder.sportsName.setTypeface(PlaythoraUtility.getFont(CreateTeamActivity.this, Config.RALEWAY_REGULAR));
                mainHolder.sportsName.setText(model.getName());

            } catch (NullPointerException e) {

            }

            if (!sportsType[position]) {
                mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_item_deselected));
                mainHolder.sportsName.setTextColor(getResources().getColor(R.color.text_one));
            } else {
                mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_item_selected));
                mainHolder.sportsName.setTextColor(getResources().getColor(R.color.white));
            }

            mainHolder.sportsLayout.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {


                    if (!sportsType[position]) {
                        sportsType[position] = true;
                        mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_item_selected));
                        mainHolder.sportsName.setTextColor(getResources().getColor(R.color.white));

                    } else {
                        sportsType[position] = false;
                        mainHolder.sportsLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.sports_item_deselected));
                        mainHolder.sportsName.setTextColor(getResources().getColor(R.color.text_one));


                    }
                }
            });

            mainHolder.itemView.setTag(model);


        }
    }

    public class SportsViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout sportsLayout;
        TextView sportsName;

        public SportsViewHolder(View view) {
            super(view);
            this.sportsName = (TextView) view.findViewById(R.id.sportsName);
            this.sportsLayout = (RelativeLayout) view.findViewById(R.id.sportsLayout);
        }

    }

    public class TeamMembersRecyclerAdapter extends RecyclerView.Adapter<TeamMembersHolder> {

        private ArrayList<AddTeamMembersList> arrayList;
        private Context context;

        public TeamMembersRecyclerAdapter(Context context, ArrayList<AddTeamMembersList> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public TeamMembersHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.team_members_item, viewGroup, false);
            TeamMembersHolder listHolder = new TeamMembersHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final TeamMembersHolder holder, final int position) {
            final AddTeamMembersList model = arrayList.get(position);


            final TeamMembersHolder mainHolder = (TeamMembersHolder) holder;

            try {

            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                e.printStackTrace();
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setTag(model);

            mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, PlayerProfileViewActivity.class);
                    i.putExtra("Id", model.getId() + "");
                    startActivity(i);
                }
            });

        }


        public void refresh(ArrayList<AddTeamMembersList> usersArrayList) {
            this.arrayList = usersArrayList;
            notifyDataSetChanged();

        }
    }

    public class TeamMembersHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;

        public TeamMembersHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.members_image);
        }
    }
}
