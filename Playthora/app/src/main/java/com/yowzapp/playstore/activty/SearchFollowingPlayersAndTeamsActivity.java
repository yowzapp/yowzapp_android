package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.SearchAllModel;
import com.yowzapp.playstore.model.SearchAllModelList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.activty.CreateGame.hideSoftKeyboard;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 23/1/17.
 */

public class SearchFollowingPlayersAndTeamsActivity extends BaseActivity {

    PreferenceManager mPref;
    ImageView cancel, back;
    EditText search;
    TextView noResult;
    ProgressBar progressBar, paginateProgressBar;
    Editable sText;
    String response;
    Gson mGson;
    MainSearchAdapter searchAdapter;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    SearchAllModel model;
    List<SearchAllModelList> tempList;
    String userId;
    private RecyclerView recyclerViewsearch;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private int visibleThreshold = 5;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.following_search_activity);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        mPref = PreferenceManager.instance(this);
        initialize();

        Intent intent = getIntent();
        userId = intent.getStringExtra("user_id");
        Log.e("user_id", userId);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(SearchFollowingPlayersAndTeamsActivity.this);
                onBackPressed();

            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                s.toString().replace("%20", " ");
                s.toString().replace("%", "");
                s.toString().replace(" ", "%20");
                if (s.toString().length() > 2) {
                    populateSearch(s, false);
                    sText = s;
                    //recent.setText("Search results");
                    recyclerViewsearch.setVisibility(View.VISIBLE);
                } else if (s.toString().length() == 0) {
                    recyclerViewsearch.setAdapter(null);
                    cancel.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    noResult.setVisibility(View.GONE);
                }
            }
        });


    }

    private void populateSearch(Editable s, boolean b) {
        if (PlaythoraUtility.checkInternetConnection(SearchFollowingPlayersAndTeamsActivity.this)) {
            try {
                if (!b)
                    progressBar.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.GONE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("user_id", userId);
                mHttpClient.get(Config.SEARCH_FOLLOWING_PLAYER_TEAM + "?q=" + s, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                cancel.setVisibility(View.VISIBLE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("SEARCH_RESPONSE", s);
                                    response = s;
                                    model = new SearchAllModel();
                                    mGson = new Gson();
                                    model = mGson.fromJson(s, SearchAllModel.class);
                                    Log.e("RESPONSE", String.valueOf(model.getSuccess().size()));

                                    if (model.getSuccess().size() != 0) {
                                        if (recyclerViewsearch.getAdapter() == null) {
                                            noResult.setVisibility(View.GONE);
                                            recyclerViewsearch.setVisibility(View.VISIBLE);
                                            tempList = model.getSuccess();
                                            searchAdapter = new MainSearchAdapter(SearchFollowingPlayersAndTeamsActivity.this, model.getSuccess());
                                            recyclerViewsearch.setAdapter(searchAdapter);
                                        }
                                    } else {
                                        noResult.setVisibility(View.VISIBLE);
                                        //recyclerViewsearch.setVisibility(View.GONE);
                                        recyclerViewsearch.setAdapter(null);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressBar.setVisibility(View.GONE);
                                    cancel.setVisibility(View.VISIBLE);
                                    noResult.setVisibility(View.VISIBLE);
                                    //recyclerViewsearch.setVisibility(View.GONE);
                                    recyclerViewsearch.setAdapter(null);
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                cancel.setVisibility(View.VISIBLE);
                                noResult.setVisibility(View.VISIBLE);
                                recyclerViewsearch.setAdapter(null);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(SearchFollowingPlayersAndTeamsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(SearchFollowingPlayersAndTeamsActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    }//else Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(SearchFollowingPlayersAndTeamsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
                cancel.setVisibility(View.VISIBLE);
                noResult.setVisibility(View.VISIBLE);
                recyclerViewsearch.setAdapter(null);
            }
        }
//        else {
//            Toast.makeText(SearchFollowingPlayersAndTeamsActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
//        }
    }

    private void initialize() {
        search = (EditText) findViewById(R.id.edit_search);
        cancel = (ImageView) findViewById(R.id.cancel_icon);
        back = (ImageView) findViewById(R.id.back_icon);
        noResult = (TextView) findViewById(R.id.empty_search);
        progressBar = (ProgressBar) findViewById(R.id.search_progress);


        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noResult.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        recyclerViewsearch = (RecyclerView) findViewById(R.id.search_recycler);
        recyclerViewsearch.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewsearch.setLayoutManager(mLayoutManager);


    }

    public class MainSearchAdapter extends RecyclerView.Adapter<MainSearchAdapter.ProfileListHolder> {
        Activity mContext;
        List<SearchAllModelList> searchList;

        public MainSearchAdapter(Activity context, List<SearchAllModelList> arrayList) {
            mContext = context;
            searchList = arrayList;

        }

        @Override
        public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_layout, null);
            return new ProfileListHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(ProfileListHolder holder, final int position) {

            try {
                Glide.with(mContext).load(searchList.get(position).getCover_pic()).error(R.drawable.circled_user).centerCrop().into(holder.image);
            } catch (Exception e) {
                holder.image.setImageResource(R.drawable.circled_user);
            }

            try {
                holder.name.setText(searchList.get(position).getName());
            } catch (Exception e) {
                e.printStackTrace();
                holder.name.setText("");
            }

            try {
                if (searchList.get(position).getType().equalsIgnoreCase("team")) {
                    holder.typeName.setText("Team");
                    holder.typeIcon.setImageResource(R.drawable.ic_team_icon);
                    holder.desc.setText(searchList.get(position).getNo_players() + " Players");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
         /*   try {
                if(searchList.get(position).getType().equalsIgnoreCase("venue")) {

                    holder.typeName.setText("Place");
                    holder.typeIcon.setImageResource(R.drawable.ic_location_icon);
                    holder.desc.setText(searchList.get(position).getVenue_details().getLocation_name());

                    //groundDetailDialog(getActivity(),gName, gLocation, gImage,gDistance,gRating,position,VenueResponse,tempList);
                }
            }catch (Exception e){
                e.printStackTrace();
            }*/

            try {
                if (searchList.get(position).getType().equalsIgnoreCase("player")) {
                    holder.typeName.setText("Player");
                    holder.typeIcon.setImageResource(R.drawable.ic_new_player_icon);
                    holder.desc.setText(searchList.get(position).getPoints() + " Points");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

          /*  try {
                if(searchList.get(position).getType().equalsIgnoreCase("game")) {
                    holder.typeName.setText("Game");
                    holder.typeIcon.setImageResource(R.drawable.ic_create_game_search);
                    holder.desc.setText(searchList.get(position).getNo_players() + " Players");
                }
            }catch (Exception e){
                e.printStackTrace();
            }*/

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (searchList.get(position).getType().equalsIgnoreCase("team")) {
                        Intent i = new Intent(mContext, OtherTeamViewActivity.class);
                        i.putExtra("ID", searchList.get(position).getId());
                        i.putExtra("SEARCH", "?q=search");
                        mContext.startActivity(i);
                    } else if (searchList.get(position).getType().equalsIgnoreCase("player")) {
                        Intent i = new Intent(mContext, PlayerProfileViewActivity.class);
                        i.putExtra("Id", searchList.get(position).getId() + "");
                        i.putExtra("SEARCH", "?q=search");
                        mContext.startActivity(i);

                    }/*else if(searchList.get(position).getType().equalsIgnoreCase("game")){
                        Intent i = new Intent(mContext, GameMain.class);
                        i.putExtra("GAME_ID",String.valueOf(searchList.get(position).getId()));
                        mContext.startActivity(i);
                    }else if(searchList.get(position).getType().equalsIgnoreCase("venue")){
                        venueDetail = searchList.get(position).getVenue_details();
                        Log.e("VENUED",venueDetail+"");
                        gName = searchList.get(position).getName();
                        gLocation = searchList.get(position).getVenue_details().getLocation_name();
                        gImage = searchList.get(position).getCover_pic();
                        gDistance = searchList.get(position).getVenue_details().getDistance();
                        gRating = searchList.get(position).getVenue_details().getRating();
                        gSports = searchList.get(position).getVenue_details().getSports_types();
                        aminities = searchList.get(position).getVenue_details().getAmenities();


                    }*/
                }
            });

        }

        @Override
        public int getItemCount() {
            return searchList.size();
        }

        public class ProfileListHolder extends RecyclerView.ViewHolder {

            ImageView typeIcon, image;
            TextView name, typeName, desc;

            public ProfileListHolder(View itemView) {
                super(itemView);

                typeIcon = (ImageView) itemView.findViewById(R.id.typeIcon);
                image = (ImageView) itemView.findViewById(R.id.imageView_player_search);
                name = (TextView) itemView.findViewById(R.id.search_name);
                desc = (TextView) itemView.findViewById(R.id.description);
                typeName = (TextView) itemView.findViewById(R.id.typename);

                name.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
                desc.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
                typeName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
            }
        }

    }
}
