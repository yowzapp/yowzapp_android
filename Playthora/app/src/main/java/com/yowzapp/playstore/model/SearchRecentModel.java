package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by vaishakha on 9/1/17.
 */
public class SearchRecentModel {
    private String status;
    private RecentSearchList success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RecentSearchList getSuccess() {
        return success;
    }

    public void setSuccess(RecentSearchList success) {
        this.success = success;
    }

    public class RecentSearchList {
        private List<SearchRecentTeam> teams;
        private List<SearchRecentTeam> players;

        public List<SearchRecentTeam> getTeams() {
            return teams;
        }

        public void setTeams(List<SearchRecentTeam> teams) {
            this.teams = teams;
        }

        public List<SearchRecentTeam> getPlayers() {
            return players;
        }

        public void setPlayers(List<SearchRecentTeam> players) {
            this.players = players;
        }
    }

    public class SearchRecentTeam {
        private String type;
        private int id;
        private String name;
        private String cover_pic;
        private int no_players;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCover_pic() {
            return cover_pic;
        }

        public void setCover_pic(String cover_pic) {
            this.cover_pic = cover_pic;
        }

        public int getNo_players() {
            return no_players;
        }

        public void setNo_players(int no_players) {
            this.no_players = no_players;
        }
    }


}

