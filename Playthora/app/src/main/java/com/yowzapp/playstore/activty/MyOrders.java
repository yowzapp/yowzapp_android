package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 17/11/16.
 */

public class MyOrders extends BaseActivity {

    public RecyclerViewAdapter adapter;
    public ArrayList<RewardModel> myProfileEveryOne;
    String responseString;
    Gson mGson;
    ProgressDialog dialog;
    TextView availableXp;
    Toolbar toolbar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_orders);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("My Orders");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(MyOrders.this, RALEWAY_REGULAR));

        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        availableXp = (TextView) findViewById(R.id.availblePoints);

        availableXp.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        recyclerView = (RecyclerView) findViewById(R.id.orderRecyclerView);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(MyOrders.this, LinearLayoutManager.VERTICAL, false));


        responseString = "[{\n" +
                "    \"product\": \"nike blue\",\n" +
                "    \"productId\": 123KDASJLFEIOWE,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"20-12-12\",\n" +
                "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike blak\",\n" +
                "    \"productId\": 123KDASJLFEIOWE,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"20-12-12\",\n" +
                "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 123KDASJLFEIOWE,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"23-12-12\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike blue\",\n" +
                "    \"productId\": 123KDASJLFEIOWE,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike blak\",\n" +
                "    \"productId\": ODIJASF39294309,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"22\",\n" +
                "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": FASKJDFALEIOADA,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike blue\",\n" +
                "    \"productId\": 123KDASJLFEIOWE,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike blak\",\n" +
                "    \"productId\": DSAFFJKASLDFAK2,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"22\",\n" +
                "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": ASDFJASLKDSAKDJ,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike blue\",\n" +
                "    \"productId\": 1SDFJASDKKJKDJF,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike blak\",\n" +
                "    \"productId\": EWIUAHFSDFJLASD,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 1SDFJASDKKJKDJF,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "},{\n" +
                "    \"product\": \"nike blue\",\n" +
                "    \"productId\": 1SDFJASDKKJKDJF,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://www.wigglestatic.com/product-media/5360101990/Nike-Sports-Water-Bottle-Running-Bottles-Blue-Lagoon-Black-341-009-442A-0.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike blak\",\n" +
                "    \"productId\": 1SDFJASDKKJKDJF,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"22\",\n" +
                "    \"image\": \"http://images.nike.com/is/image/DotCom/PDP_HERO_M/NOB91_048_A/32oz-t1-flow-swoosh-water-bottle.jpg\"\n" +
                "}, {\n" +
                "    \"product\": \"nike white\",\n" +
                "    \"productId\": 1SDFJASDKKJKDJF,\n" +
                "    \"rewardXp\": \"9000\",\n" +
                "    \"categories\": \"nike\",\n" +
                "    \"categoryId\": \"2-2-12\",\n" +
                "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                "}]";
        try {
            populateRecyclerView(responseString);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void populateRecyclerView(String getResponse) throws Exception {

        responseString = getResponse;
        Thread thread = new Thread(new Runnable() {

            public void run() {

                try {


                    MyOrders.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                try {
                                    if (!MyOrders.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                    e.printStackTrace();
                                }

                                JSONArray categoryArray = new JSONArray(responseString);
                                mGson = new Gson();
                                myProfileEveryOne = new ArrayList<RewardModel>();

                                for (int i = 0; i < categoryArray.length(); i++) {
                                    myProfileEveryOne.add(mGson.fromJson(categoryArray.get(i).toString(), RewardModel.class));

                                }

                                if (!myProfileEveryOne.isEmpty()) {

                                    if (recyclerView.getAdapter() == null) {

                                        adapter = new RecyclerViewAdapter(MyOrders.this, myProfileEveryOne);
                                        recyclerView.setAdapter(adapter);// set adapter on recyclerview
                                        adapter.notifyDataSetChanged();


                                    } else {


                                    }

                                } else {

                                }


                            } catch (JsonParseException e) {

                                try {
                                    if (!MyOrders.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception ee) {
                                    ee.printStackTrace();
                                }

                                e.printStackTrace();
                            } catch (JSONException e) {

                                try {
                                    if (!MyOrders.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception eee) {
                                    eee.printStackTrace();
                                }

                                e.printStackTrace();
                            }

                        }
                    });
                } catch (Exception e) {
                }
            }
        });//
        thread.start();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    public class RecyclerViewAdapter extends
            RecyclerView.Adapter<RewardViewHolder> {

        private ArrayList<RewardModel> arrayList;
        private Context context;

        public RecyclerViewAdapter(Context context,
                                   ArrayList<RewardModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.my_orders_items, viewGroup, false);
            RewardViewHolder listHolder = new RewardViewHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final RewardViewHolder holder, final int position) {
            final RewardModel model = arrayList.get(position);

            final RewardViewHolder mainHolder = (RewardViewHolder) holder;

            try {
                mainHolder.orderName.setTypeface(PlaythoraUtility.getFont(MyOrders.this, RALEWAY_REGULAR));
                mainHolder.orderDate.setTypeface(PlaythoraUtility.getFont(MyOrders.this, RALEWAY_REGULAR));
                //  mainHolder.orderXp.setTypeface(PlaythoraUtility.getFont(MyOrders.this,RALEWAY_REGULAR));
                mainHolder.orderStatus.setTypeface(PlaythoraUtility.getFont(MyOrders.this, RALEWAY_REGULAR));
                mainHolder.orderStatusText.setTypeface(PlaythoraUtility.getFont(MyOrders.this, RALEWAY_REGULAR));
                mainHolder.orderName.setText(model.getProduct());
                mainHolder.orderId.setText(model.getProductId());
                mainHolder.orderDate.setText(model.getCategoryId());//change to date later
                mainHolder.orderXp.setText(model.getRewardXp() + " XP");
                /*if(position==arrayList.size()-1){
                    mainHolder.lastItem.setBackgroundColor(getResources().getColor(R.color.divider));
                }*/
            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getImage()).into(mainHolder.orderImage);
            } catch (Exception e) {
                e.printStackTrace();
            }


            mainHolder.itemView.setTag(model);


        }


    }

}
