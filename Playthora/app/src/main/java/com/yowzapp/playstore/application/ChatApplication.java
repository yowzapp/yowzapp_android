package com.yowzapp.playstore.application;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.multidex.MultiDex;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.telr.mobile.sdk.TelrApplication;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.ChattingActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.yowzapp.playstore.fragment.ChatAllFragment.updateMyChatList;

/**
 * Created by nakul on 27/01/17.
 */

public class ChatApplication extends TelrApplication {
//    private static final int NOTIFY_ME_ID = 1337;
//    public static NotificationManager notifyMgr = null;
    private static Context applicationContext;
//    Notification myNotication;
    PreferenceManager mPreference;
    private Socket mSocket;
//    private MyService mService;

    private final ServiceConnection mConnection = new ServiceConnection() {
        @SuppressWarnings("unchecked")
        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            MyService mService = ((LocalBinder<MyService>) service).getService();

            Log.e("TTTTT", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            MyService mService = null;

            Log.e("TTTTT", "onServiceDisconnected");
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            mPreference = PreferenceManager.instance(getPlaythoraContext());

            Log.e("INCOMING", args[0].toString());
            JSONObject data = (JSONObject) args[0];
            String userId;
            String message;
            String senderName, recieverName, profilePic, senderId, convId;
            String send, typeText = null;
            String title;
            String coverPic;
            int type;
            try {

                message = data.getString("message");
                userId = data.getString("userid");
                senderName = data.getString("sender_name");
                recieverName = data.getString("reciever_name");
                profilePic = data.getString("sender_profile_pic");
                coverPic = data.getString("reciever_profile_pic");
                senderId = data.getString("sender_id");
                type = data.getInt("type");
                convId = data.getString("type_id");

                Log.e("userid", userId);
                Log.e("sender_name", senderName);
                Log.e("reciever_name", recieverName);
                Log.e("sender_id", senderId);

                if (type == 1)
                    typeText = "game";
                else if (type == 2)
                    typeText = "team";
                else if (type == 3)
                    typeText = "player";


                if (type == 1 || type == 2) {
                    send = senderName + ": " + message;
                    title = recieverName;
                } else {
                    send = message;
                    title = senderName;
                }
            } catch (JSONException e) {
                return;
            }

            ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(getApplicationContext().ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            Log.e("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            componentInfo.getPackageName();
//            if (!(getString(R.string.package_name) + ".activty.ChattingActivity").equalsIgnoreCase(taskInfo.get(0).topActivity.getClassName())) {
            //notifyMgr.cancel(NOTIFY_ME_ID);
            //if(!offerId.equalsIgnoreCase(AppConstants.OFFERID)){
//                triggerNotification( offerId,message);
//                 sendNotification(message,"123");
//                sendNotification(message, "123", senderName, profilePic, convId, type, typeText);
//                Log.e("OFFERIDDDDINSIDDE", "123");

            Log.e("ChattingConvId", String.valueOf(ChattingActivity.convId));
            Log.e("convId", convId);

            if (ChattingActivity.convId != Integer.parseInt(convId)
                    ||!(getString(R.string.package_name) + ".activty.ChattingActivity").equalsIgnoreCase(taskInfo.get(0).topActivity.getClassName())) {
                if (type == 1 || type == 2) {
//                    if(!userId.equals(mPreference.getId()))
                    Log.e("Triggered", "Group");
                    sendNotification(send, title, recieverName, coverPic, convId, type, typeText);
                } else {
                    Log.e("Triggered", "Player");
                    sendNotification(send, title, senderName, profilePic, convId, type, typeText);
                }
            }
//            }
            updateMyChatList(getApplicationContext(), "GOT THE CONTENT RECEIVER");
        }
    };

    public static Context getPlaythoraContext() {
        return applicationContext;
    }

    public void init() {
        {
            try {
                mSocket = IO.socket(Config.CHAT_SERVER_URL);
                mSocket.on("chat message", onNewMessage);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        init();
        //doBindService();
    }

    void doBindService() {
        if (!PreferenceManager.instance(getPlaythoraContext()).getAccessToken().isEmpty())
            bindService(new Intent(this, MyService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void sendNotification(String body, String title, String senderName, String profilepic, String convId, int type, String typeText) {
        Log.e("NotificationChat", body);
        int icon = R.mipmap.ic_launcher;
//        Gson gson = new Gson();
        //offerList=  gson.fromJson(id, NewOutGoingModel.class);
        //   Log.e("GGGGGCCCCMMMM", offerList.getOfferor().getName());
        Intent chat;

        chat = new Intent(this, ChattingActivity.class);
        chat.putExtra("name", senderName);
        chat.putExtra("image", profilepic);
        chat.putExtra("id", convId + "");
        chat.putExtra("type", typeText);
        chat.putExtra("Conv_id", Integer.parseInt(convId));
        chat.putExtra("Conv_type", type);
        // chat.putExtra("offerId", offerId);

        Log.e("name", senderName);
        Log.e("image", profilepic);
        Log.e("id", convId + "");
        Log.e("type", typeText);
        Log.e("Conv_id", convId);
        Log.e("Conv_type", String.valueOf(type));


        //chat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, chat,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}