package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;
import java.util.List;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 24/11/16.
 */
public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.ProfileListHolder> implements Filterable {
    private Context mContext;
    private List<AddTeamMembersList> playerList;
    private UserFilter userFilter;
    private List<AddTeamMembersList> filteredUserList;

    public FollowersAdapter(Context context, List<AddTeamMembersList> arrayList) {
        mContext = context;
        playerList = arrayList;
        filteredUserList = arrayList;

    }

    public void RefreshPagination(List<AddTeamMembersList> tempList, boolean b) {
        this.playerList = tempList;
        notifyDataSetChanged();

    }

    @Override
    public Filter getFilter() {
        if (userFilter == null)
            userFilter = new UserFilter(this, playerList);
        return userFilter;
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.followers_layout, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, final int position) {

        Log.e("PROFILE_PIC", playerList.get(position).getProfile_pic() + "");
        try {
            Glide.with(mContext).load(playerList.get(position).getProfile_pic()).error(R.drawable.circled_user).centerCrop().centerCrop().into(holder.playerImage);
        } catch (Exception e) {
            holder.playerImage.setImageResource(R.drawable.circled_user);
        }

        holder.playerName.setText(playerList.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, PlayerProfileViewActivity.class);
                i.putExtra("Id", playerList.get(position).getId() + "");
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class UserFilter extends Filter {

        private FollowersAdapter adapter;

        private List<AddTeamMembersList> originalList;

        private List<AddTeamMembersList> filteredList;

        public UserFilter(FollowersAdapter adapter, List<AddTeamMembersList> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new ArrayList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            Log.e("STRING", constraint + "");
            Log.e("LIST", constraint.length() + "");

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
                Log.e("STRING", "0");
            } else {
                Log.e("STRING", constraint.length() + "");
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final AddTeamMembersList user : originalList) {
                    if (user.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(user);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredUserList.clear();
            adapter.filteredUserList.addAll((List<AddTeamMembersList>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView playerImage;
        TextView playerName;
        RelativeLayout lytEditDeleteWrapper, alreadyExchanged;

        public ProfileListHolder(View itemView) {
            super(itemView);
            playerImage = (ImageView) itemView.findViewById(R.id.followers_user_image);
            playerName = (TextView) itemView.findViewById(R.id.userName_follower);
            playerName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
        }
    }
}
