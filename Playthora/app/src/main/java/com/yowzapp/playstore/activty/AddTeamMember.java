package com.yowzapp.playstore.activty;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 24/11/16.
 */
public class AddTeamMember extends BaseActivity {
    TeamAddAdapter adapter;
    Toolbar toolbar;
    RecyclerView recyclerFollowers;
    PreferenceManager mPref;
    int teamId;
    ArrayList<Integer> temp;
    TextView toolBarTitle;
    EditText search;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers_listing);
        temp = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        search = (EditText) findViewById(R.id.edit_search_team);
        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        search.setText("");

        mPref = PreferenceManager.instance(this);
        toolbar = (Toolbar) findViewById(R.id.followers_toolbar);
        TextView toolBarTitle = (TextView) findViewById(R.id.followers_toolbar_title);
        toolBarTitle.setText("Team Members");
        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        if (getIntent().hasExtra("TEAM_ID")) {
            teamId = getIntent().getIntExtra("TEAM_ID", 0);
        }

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                Log.e("DATA", temp.size() + "");
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putIntegerArrayList("NEWLIST", temp);
                intent.putExtras(bundle);
                setResult(1, intent);
                finish();
            }
        });
        recyclerFollowers = (RecyclerView) findViewById(R.id.followers_recycler);
        recyclerFollowers.setLayoutManager(new LinearLayoutManager(AddTeamMember.this, LinearLayoutManager.VERTICAL, false));
        recyclerFollowers.setHasFixedSize(true);
        try {
            LoadMember();
        } catch (Exception e) {
            e.printStackTrace();
        }

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() >= 0) {
                    adapter.getFilter().filter(s);
                    Log.e("STRING", "EDIT" + s);
                }
            }
        });


    }

    private void LoadMember() {
        adapter = new TeamAddAdapter(AddTeamMember.this, EditTeamActivity.addMemberList);
        recyclerFollowers.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.followers_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.followers_selected) {
           /* Intent intent=new Intent();
            intent.putIntegerArrayListExtra("NEWLIST",temp);
            setResult(5, intent);
            finish();*/
            search.setVisibility(View.VISIBLE);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            search.setText("");
            Log.e("DATA", temp.size() + "");
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putIntegerArrayList("NEWLIST", temp);
            intent.putExtras(bundle);
            setResult(1, intent);
            finish();
            return true;
        }
        return false;
    }

    public class TeamAddAdapter extends RecyclerView.Adapter<TeamAddAdapter.MemberList> implements Filterable {
        Context mContext;
        ArrayList<AddTeamMembersList> playerList;
        private UserFilter userFilter;
        private List<AddTeamMembersList> filteredUserList;

        public TeamAddAdapter(AddTeamMember activity, ArrayList<AddTeamMembersList> arrayList) {
            mContext = activity;
            playerList = arrayList;
            filteredUserList = arrayList;
        }

        @Override
        public MemberList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.followers_layout, null);
            return new MemberList(layoutView);
        }

        @Override
        public void onBindViewHolder(MemberList holder, final int position) {
            try {
                Glide.with(mContext).load(playerList.get(position).getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(holder.playerImage);
            } catch (Exception e) {
                holder.playerImage.setImageResource(R.drawable.circled_user);
            }
            holder.playerName.setText(playerList.get(position).getName());
            if (playerList.get(position).getPivot().getIs_admin() == 1) {
                holder.playerSatus.setVisibility(View.VISIBLE);
                holder.playerSatus.setText("Admin");
            } else holder.playerSatus.setVisibility(View.GONE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showdialog(playerList.get(position).getName(), position);
                }
            });

            holder.playerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(AddTeamMember.this, PlayerProfileViewActivity.class);
                    i.putExtra("Id", playerList.get(position).getId() + "");
                    startActivity(i);
                }
            });

        }

        @Override
        public Filter getFilter() {
            if (userFilter == null)
                userFilter = new UserFilter(this, playerList);
            return userFilter;
        }

        private void showdialog(String name, final int position) {
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.team_member_dialog);
            TextView member, remove, makeAdmin;
            member = (TextView) dialog.findViewById(R.id.name_member);
            remove = (TextView) dialog.findViewById(R.id.remove_text);
            makeAdmin = (TextView) dialog.findViewById(R.id.cancel);
            member.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
            remove.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
            makeAdmin.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));

            if (mPref.getId().equalsIgnoreCase(playerList.get(position).getId() + "")) {
                Log.e("DONE", "GONE");
                makeAdmin.setVisibility(View.GONE);
            } else {
                Log.e("DONE", "VISIBLE");
                makeAdmin.setVisibility(View.VISIBLE);
            }

            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePlayer(position);
                    dialog.dismiss();
                }
            });

            makeAdmin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addAdmin(position);
                    dialog.dismiss();
                }
            });

            member.setText(name);
            dialog.show();

        }

        private void addAdmin(final int position) {

            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                try {
                    PlaythoraUtility.showProgressDialog(AddTeamMember.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("user_id", String.valueOf(playerList.get(position).getId()));
                    params.add("team_id", String.valueOf(teamId));
                    Log.e("PARAMS", String.valueOf(params));
                    mHttpClient.post(Config.TEAM_PLAYER_MAKEADMIN, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            EditTeamActivity.success = true;
                                            //Toast.makeText(getApplicationContext(), object.getString("success"), Toast.LENGTH_SHORT).show();
                                            if (playerList.get(position).getId() == EditTeamActivity.addMemberList.get(position).getId()) {
                                                EditTeamActivity.addMemberList.get(position).getPivot().setIs_admin(1);
                                            }
                                            playerList.get(position).getPivot().setIs_admin(1);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                        } else
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        try {
                                            String s = new String(bytes);
                                            Log.d("RESPONSE_FAIL", s);
                                            JSONObject object = new JSONObject(s);
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();
                }
            } else {

//                Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

            }

        }

        private void removePlayer(final int position) {

            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                try {
                    PlaythoraUtility.showProgressDialog(AddTeamMember.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("user_id", String.valueOf(playerList.get(position).getId()));
                    params.add("team_id", String.valueOf(teamId));
                    Log.e("PARAMS", String.valueOf(params));
                    mHttpClient.post(Config.TEAM_PLAYER_REMOVE, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            EditTeamActivity.success = true;
                                            Toast.makeText(getApplicationContext(), object.getString("success"), Toast.LENGTH_SHORT).show();
                                            if (playerList.get(position).getId() == EditTeamActivity.addMemberList.get(position).getId()) {
                                                Log.e("REMOVED", EditTeamActivity.addMemberList.get(position).getId() + "");
                                                temp.add(EditTeamActivity.addMemberList.get(position).getId());
                                                EditTeamActivity.addMemberList.remove(position);
                                            }
                                            //playerList.remove(position);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();

                                        } else
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        try {
                                            String s = new String(bytes);
                                            Log.d("RESPONSE_FAIL", s);
                                            JSONObject object = new JSONObject(s);
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();
                }
            } else {

//                Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

            }


        }

        @Override
        public int getItemCount() {
            return playerList.size();
        }

        public class UserFilter extends Filter {
            private TeamAddAdapter adapter;

            private ArrayList<AddTeamMembersList> originalList;

            private ArrayList<AddTeamMembersList> filteredList;

            public UserFilter(TeamAddAdapter adapter, ArrayList<AddTeamMembersList> originalList) {
                super();
                this.adapter = adapter;
                this.originalList = new ArrayList<>(originalList);
                this.filteredList = new ArrayList<>();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filteredList.clear();
                final FilterResults results = new FilterResults();
                Log.e("STRING", constraint + "");
                Log.e("LIST", constraint.length() + "");

                if (constraint.length() == 0) {
                    filteredList.addAll(originalList);
                    Log.e("STRING", "0");
                } else {
                    Log.e("STRING", constraint.length() + "");
                    final String filterPattern = constraint.toString().toLowerCase().trim();
                    for (final AddTeamMembersList user : originalList) {
                        if (user.getName().toLowerCase().contains(filterPattern)) {
                            filteredList.add(user);
                        }
                    }
                }
                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                adapter.filteredUserList.clear();
                adapter.filteredUserList.addAll((ArrayList<AddTeamMembersList>) results.values);
                adapter.notifyDataSetChanged();
            }
        }

        public class MemberList extends RecyclerView.ViewHolder {
            ImageView playerImage;
            TextView playerName, playerSatus;

            public MemberList(View itemView) {
                super(itemView);
                playerImage = (ImageView) itemView.findViewById(R.id.followers_user_image);

                playerName = (TextView) itemView.findViewById(R.id.userName_follower);

                playerSatus = (TextView) itemView.findViewById(R.id.userName_text);

                playerName.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));

                playerSatus.setTypeface(PlaythoraUtility.getFont(mContext, RALEWAY_REGULAR));
            }
        }
    }
}

