package com.yowzapp.playstore.activty;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.LocationList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class LocationActivity extends BaseActivity implements  OnItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int REQUEST_CHECK_SETTINGS = 1000;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPKwNMVuBU070GTZBWimbfI8hVUNM9ojY";
    public boolean isGPSEnabled = false;
    ImageView locationSuccess;
    TextView mTitle;
    Toolbar mToolbar;
    ProgressBar progressBar;
    AutoCompleteTextView autoCompView;
    PreferenceManager mPref;
    List<LocationList> listOfCities;
    String cityName, location_update, bio, profile_pic, cover_pic, type, social, interest_updated;
    Double latValue, longValue;
    String userName;
    ArrayList<String> interests = new ArrayList<String>();
    ArrayList<String> interesrArray = new ArrayList<String>();
    StringEntity entity;
    JSONArray jsonArray;
    Gson gson;
    private double latitudevalue, curlatitudevalue;
    private double longitudevalue, curlongitudevalue;
    private LocationManager locationManager;
    private android.location.LocationListener listener;
    private Geocoder geocoder;
    private List<Address> addresses;
    private String address = "", subLocality = "", city = "", country = "", state = "", LocationName = "";
    private GoogleApiClient googleApiClient;

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }

    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&types=geocode");
            //sb.append("&types=(cities)" );
            //sb.append("&location=12.9545163,77.3500463");
            //sb.append("&radius=50000");
            // sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            Log.e("GOOGLE_API", sb + "");

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            Log.e("@@@@@@@@@@@@@@@@@", jsonResults.toString());
            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            resultList.add(0, "Use current location");
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                Log.e("ERROR", predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_layout);
        mPref = PreferenceManager.instance(getApplicationContext());

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        geocoder = new Geocoder(this, Locale.ENGLISH);

        locationSuccess = (ImageView) findViewById(R.id.location_circle_image);
        mTitle = (TextView) findViewById(R.id.location_username);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_location);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Intent intent = getIntent();
        if (intent.hasExtra("name")) {
            userName = intent.getStringExtra("name");
            interests = intent.getStringArrayListExtra("interest");
        } else {
            userName = mPref.getName();
            String json = mPref.getInterestList();
            Log.e("JSONTEXT", json);
            gson = new Gson();
            try {
                JSONArray jsonArray = new JSONArray(json);
                for (int i = 0; i < jsonArray.length(); i++) {
                    interests.add(jsonArray.get(i).toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.e("userName", userName + " name");
        Log.e("interests", interests + " interest");


        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        setUpUI(findViewById(R.id.locationActivity));
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        mTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));

        locationSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("LOCATIION", latitudevalue + " " + longitudevalue);
                if (autoCompView.getText().toString().isEmpty() || autoCompView.getText().toString().equals(" ")) {
                    Toast.makeText(LocationActivity.this, "please select the location", Toast.LENGTH_SHORT).show();
                } else updateProfile();
              /*  if(autoCompView.getText().toString().trim().isEmpty()){
                    mPref.setProfileUpdated("true");
                    Intent intent = new Intent(LocationActivity.this,ProfileSuccessActivity.class);
                    startActivity(intent);
                    finish();

                }else {
                    location();
                }*/

            }
        });
        autoCompView = (AutoCompleteTextView) findViewById(R.id.location_text);
        autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
        autoCompView.setOnItemClickListener(this);
        autoCompView.setThreshold(0);

        autoCompView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (autoCompView.getText().toString().trim().isEmpty())
                    autoCompView.setText(" ");
            }
        });

        autoCompView.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        listener = new android.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    curlatitudevalue = location.getLatitude();
                    curlongitudevalue = location.getLongitude();
                    //Toast.makeText(getApplicationContext(), "" + latitudevalue + longitudevalue, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 1, listener);

        //configure_button();

    }

    private boolean validation() {
        if (autoCompView.getText().toString().trim().isEmpty()) {
            Toast.makeText(LocationActivity.this, "select location", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void updateProfile() {

        try {
            jsonArray = new JSONArray();
            for (int i = 0; i < interests.size(); i++) {

                try {
                    JSONObject object = new JSONObject();
                    Log.e("id", interests.get(i));
                    object.put("id", interests.get(i));
                    jsonArray.put(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(LocationActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            try {
                JSONObject object = new JSONObject();
                object.put("name", userName);
                object.put("interests", jsonArray);
                object.put("lat", String.valueOf(latitudevalue));
                object.put("lng", String.valueOf(longitudevalue));
                object.put("location_name", autoCompView.getText().toString());

                Log.e("JSONOBJ", object.toString());
                entity = new StringEntity(object.toString());

                httpClient.post(LocationActivity.this, Config.EDIT_PROFILE, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();

                        try {
                            String s = new String(responseBody);
                            Log.e("profileUpdate", s);

                            JSONObject object = new JSONObject(s);
                            if (object.getInt("statuscode") == 200) {
                                Log.e("profileUpdate", "success");
                                mPref.setProfileUpdated("true");
                                userName = object.getJSONObject("success").getString("name");
                                cityName = object.getJSONObject("success").getString("location_name");
                                bio = object.getJSONObject("success").getString("bio");
                                profile_pic = object.getJSONObject("success").getString("profile_pic");
                                cover_pic = object.getJSONObject("success").getString("cover_pic");
                                latValue = object.getJSONObject("success").getDouble("lat");
                                longValue = object.getJSONObject("success").getDouble("lng");
                                location_update = String.valueOf(object.getJSONObject("success").getBoolean("location_updated"));
                                interest_updated = String.valueOf(object.getJSONObject("success").getBoolean("interest_updated"));
                                type = String.valueOf(object.getJSONObject("success").getBoolean("is_private"));
                                social = String.valueOf(object.getJSONObject("success").getBoolean("is_social"));

                                Log.e("locationName", cityName);
                                Log.e("userName", userName);
                                Log.e("bio", bio);
                                Log.e("latitude", String.valueOf(latValue));
                                Log.e("longitude", String.valueOf(longValue));
                                Log.e("interest_updated", String.valueOf(interest_updated));
                                Log.e("location_update", String.valueOf(location_update));

                                mPref.setName(userName);
                                mPref.setCityName(cityName);
                                mPref.setLocationUpdated(location_update);
                                mPref.setProfilePic(profile_pic);
                                mPref.setInterestUpdated(interest_updated);

                                Toast.makeText(LocationActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LocationActivity.this, ProfileSuccessActivity.class);
                                startActivity(intent);
                                finish();


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            // Toast.makeText(LocationActivity.this,"Some error occurred please try again later",Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpUI(View viewById) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(LocationActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                setUpUI(innerView);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isLocationEnabled(getApplicationContext())) {
            Log.e("TESTING1", "TESTING");
            enableLoc();
        } else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // startLocationUpdates();
                        Log.e("NAKUL", "Ok");
                        /*Thread timerThread = new Thread() {
                            public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            checkLocationPermission();

                                        }
                                    });

                                }
                        };
                        timerThread.start();*/
                        Log.e("TESTING5", "TESTING");
                        checkLocationPermission();
                        break;
                    case Activity.RESULT_CANCELED:
                        //settingsrequest();//keep asking if imp or do whatever
                        Log.e("NAKUL", "No");
                        //Toast.makeText(getApplicationContext(),"")
                        break;
                }
                break;
        }
    }

    public boolean checkLocationPermission() {
        Log.e("TESTING6", "TESTING");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                //  TODO: Prompt with explanation!

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 1, listener);
            return true;
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        if (str.equalsIgnoreCase("Use current location")) {
            //configure_button();

            try {
                addresses = geocoder.getFromLocation(curlatitudevalue, curlongitudevalue, 1);
                Log.e("ADDRESS", String.valueOf(addresses));// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
                autoCompView.setText("");
                Toast.makeText(LocationActivity.this,
                        "Please try again",
                        Toast.LENGTH_LONG).show();
            }

            try {
                if (addresses != null) {

                   /* if (!(addresses.get(0).getAddressLine(0) == null))
                        address = addresses.get(0).getAddressLine(0) + ", "; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    else address = "";*/

                    if (!(addresses.get(0).getLocality() == null))
                        city = addresses.get(0).getLocality() + ", ";
                    else city = "";

                    if (!(addresses.get(0).getAdminArea() == null))
                        state = addresses.get(0).getAdminArea() + ", ";
                    else state = "";

                    /*if (!(addresses.get(0).getCountryName() == null))
                        country = addresses.get(0).getCountryName();
                    else country = "";*/

                    /*if (!(addresses.get(0).getSubLocality() == null))
                        subLocality = addresses.get(0).getSubLocality() + ", ";
                    else subLocality = "";*/

                    autoCompView.setText(city + state);
                    latitudevalue = curlatitudevalue;
                    longitudevalue = curlongitudevalue;

                    //String postalCode = addresses.get(0).getPostalCode();
                    // String knownName = addresses.get(0).getFeatureName();
                    Log.e("Adressss", " city:" + city + "state: " + state);
                    //Log.e("Adressss", addresses.get(0).getSubLocality() + " " + addresses.get(0).getSubAdminArea());
                } else {
                    autoCompView.setText("");
                    Toast.makeText(LocationActivity.this,
                            "Please try again",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                autoCompView.setText("");
                Toast.makeText(getApplicationContext(), "couldn't fetch current location", Toast.LENGTH_SHORT).show();
            }


        } else {
            try {
                LatLng p1 = getLocationFromAddress(LocationActivity.this, str);
                latitudevalue = p1.latitude;
                longitudevalue = p1.longitude;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(LocationActivity.this,
                        "Please try again",
                        Toast.LENGTH_LONG).show();
                autoCompView.setText("");
                latitudevalue = 0.0;
                longitudevalue = 0.0;
            }
            //Toast.makeText(this, "Latitude" + latitudevalue + " Longitude" + longitudevalue, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        // map.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    Log.e("STATUS", String.valueOf(status.getStatusCode()));
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 1, listener);
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        LocationActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                                Log.e("EEEEEE", e.toString());
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }


    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        String text = "";
        ArrayList<String> queryResults;
        private ArrayList resultList;
        private Context context;
        private int layout;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            this.context = context;
            layout = textViewResourceId;
        }

        @Override
        public int getCount() {
            try {
                return resultList.size();
            } catch (Exception e) {
                return 1;
            }
        }

        @Override
        public String getItem(int index) {
            return String.valueOf(resultList.get(index));
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(layout, parent, false);

            TextView suggestion = (TextView) rowView.findViewById(R.id.text_box);
            suggestion.setText(getItem(position).toString());
            suggestion.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            if (position == 0)
                suggestion.setTextColor(getResources().getColor(R.color.login_color));

            suggestion.setText(highlight(text, getItem(position), context));
            return rowView;
        }

        public CharSequence highlight(String search, String originalText, Context context) {
            // ignore case and accents
            // the same thing should have been done for the search_activity_items text
            String normalizedText = Normalizer
                    .normalize(originalText, Normalizer.Form.NFD)
                    .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                    .toLowerCase(Locale.ENGLISH);

            int start = normalizedText.indexOf(search.toLowerCase(Locale.ENGLISH));
            if (start < 0) {
                // not found, nothing to to
                return originalText;
            } else {
                // highlight each appearance in the original text
                // while searching in normalized text
                Spannable highlighted = new SpannableString(originalText);
                while (start >= 0) {
                    int spanStart = Math.min(start, originalText.length());
                    int spanEnd = Math.min(start + search.length(),
                            originalText.length());

                    highlighted.setSpan(new StyleSpan(Typeface.BOLD),
                            spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


                    start = normalizedText.indexOf(search, spanEnd);
                }

                return highlighted;
            }
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();

                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        text = constraint.toString();
                        queryResults = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = queryResults;
                        filterResults.count = queryResults.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                    resultList = (ArrayList<String>) results.values;

                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    } else {
                        notifyDataSetInvalidated();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            };
            return filter;
        }
    }
}
