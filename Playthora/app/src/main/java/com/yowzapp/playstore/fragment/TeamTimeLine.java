package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.model.TeamTimeLineModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 22/11/16.
 */
public class TeamTimeLine extends Fragment {
    View contentView;
    RecyclerView recyclerView;
    TimeLineAdapter adapter;
    ArrayList<TeamTimeLineModel> model;
    String response;
    Gson gson;
    ImageView downArrow, upArrow;
    int teamId;
    PreferenceManager mPref;
    ProgressBar progressBar;
    TextView emptyTimeline;

    public TeamTimeLine(int id) {
        teamId = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.team_time_line_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.timeline_recycler);
        downArrow = (ImageView) contentView.findViewById(R.id.down_arrow);
        upArrow = (ImageView) contentView.findViewById(R.id.upper_arrow);
        progressBar = (ProgressBar) contentView.findViewById(R.id.team_timeline_progress);
        emptyTimeline = (TextView) contentView.findViewById(R.id.empty_timeline);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        loadTimeLine();

        return contentView;
    }

    private void loadTimeLine() {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("team_id", String.valueOf(teamId));
                Log.e("PARAMMS", String.valueOf(params));
                mHttpClient.post(Config.TEAM_TIMELINE, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                emptyTimeline.setVisibility(View.GONE);
                                String s = new String(responseBody);
                                Log.e("TEAMTIMELINE", s);

                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        JSONArray gallery = object.getJSONObject("success").getJSONArray("time_line");
                                        gson = new Gson();
                                        model = new ArrayList<>();
                                        for (int i = 0; i < gallery.length(); i++) {
                                            model.add(gson.fromJson(gallery.get(i).toString(), TeamTimeLineModel.class));
                                        }
                                        if (model.size() != 0) {
                                            adapter = new TimeLineAdapter(getActivity(), model);
                                            recyclerView.setAdapter(adapter);
                                        } else upArrow.setVisibility(View.GONE);
                                        if (model.size() < 4) {
                                            downArrow.setVisibility(View.GONE);
                                        } else downArrow.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), "Some error occurred while fetching Timeline. Try again later", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                emptyTimeline.setVisibility(View.VISIBLE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("FAIL_TEAMTIMELINE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();
                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL_GALLERY", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
                emptyTimeline.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }


    public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineList> {
        Context context;
        ArrayList<TeamTimeLineModel> model;

        public TimeLineAdapter(FragmentActivity activity, ArrayList<TeamTimeLineModel> model) {
            context = activity;
            this.model = model;
        }

        @Override
        public TimeLineList onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_line_item, null);
            return new TimeLineList(layoutView);
        }

        @Override
        public void onBindViewHolder(TimeLineList holder, int position) {
            try {
                holder.mData.setText(model.get(position).getDay());
                holder.mMonth.setText(model.get(position).getMonth());
                String content = model.get(position).getContent();
                String s = content.substring(0, 1).toUpperCase() + content.substring(1);
                holder.mText.setText(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class TimeLineList extends RecyclerView.ViewHolder {
            TextView mData, mMonth, mText;

            public TimeLineList(View itemView) {
                super(itemView);
                mData = (TextView) itemView.findViewById(R.id.timeline_date);
                mMonth = (TextView) itemView.findViewById(R.id.timeline_month);
                mText = (TextView) itemView.findViewById(R.id.text_two);

                mText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
                mMonth.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_SEMIBOLD));
            }
        }
    }
}
