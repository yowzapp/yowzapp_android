package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.AddmemberFollowAdapter;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by vaishakha on 26/12/16.
 */
public class AddMembersFreeAgent extends Fragment {

    public static AddmemberFollowAdapter adapter;
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    AddTeamMembersModel model;
    TextView invite;
    PreferenceManager mPref;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.following_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.team_members_recycler);
        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);

        try {
            allPlayers();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contentView;

    }

    private void allPlayers() {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            //progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            mHttpClient.get(Config.PLAYERS_LIST,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            // progressBar.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);

                                model = new AddTeamMembersModel();
                                mGson = new Gson();
                                model = mGson.fromJson(s, AddTeamMembersModel.class);
                               /* Log.e("RESPONSE", String.valueOf(model.getSuccess().size()));

                                if (model.getSuccess().size() != 0) {
                                    Log.e("RESPONSE", String.valueOf(model.getSuccess().size()));
                                    if (recyclerView.getAdapter() == null) {
                                        Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                        adapter = new AddmemberFollowAdapter(getActivity(), model.getSuccess());
                                        recyclerView.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                    }
                                }*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            progressBar.setVisibility(View.GONE);
                            //progressBar.setVisibility(View.GONE);
                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);

                            } catch (Exception e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        } else {

//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

}
