package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.PlayersFollowing;
import com.yowzapp.playstore.fragment.TeamFollowing;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

public class FollowingActivityActivity extends BaseActivity implements  View.OnClickListener {
    public static ViewPager customViewPager;
    public static MyPagerAdapter myPagerAdapter;
    TextView following, followingSelected, followers, followersSelected, freeAgents, freeAgentsSelected;
    Toolbar toolbar;
    String admin, userID;
    EditText search;
    String string = "player";
    String string1 = "team";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Intent intent = getIntent();
        admin = intent.getStringExtra("admin");
        userID = intent.getStringExtra("userId");
        Log.e("admin", admin);
        Log.e("userID", userID);

   /*     search = (EditText) findViewById(R.id.edit_search_team);
        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        search.setText("");

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(FollowingActivityActivity.this,SearchFollowingPlayersAndTeamsActivity.class);
                startActivity(intent1);

            }
        });*/

        toolbar = (Toolbar) findViewById(R.id.following_toolbar);
        TextView toolBarTitle = (TextView) findViewById(R.id.followers_toolbar_title);
        toolBarTitle.setText("Following");
        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));


        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        intializeAllComponents();
        following.setOnClickListener(this);
        followers.setOnClickListener(this);


        customViewPager = (ViewPager) findViewById(R.id.customFragment);
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        customViewPager.setAdapter(myPagerAdapter);
        customViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        followingSelected.setVisibility(View.VISIBLE);
                        following.setVisibility(View.GONE);
                        followersSelected.setVisibility(View.GONE);
                        followers.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        followingSelected.setVisibility(View.GONE);
                        following.setVisibility(View.VISIBLE);
                        followersSelected.setVisibility(View.VISIBLE);
                        followers.setVisibility(View.GONE);
                        break;

                    default:
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
        //customViewPager.setPagingEnabled(false);
    }

    private void intializeAllComponents() {

        following = (TextView) findViewById(R.id.following);
        followingSelected = (TextView) findViewById(R.id.followingSelected);
        followers = (TextView) findViewById(R.id.followers);
        followersSelected = (TextView) findViewById(R.id.followersSelected);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.following:
                followingSelected.setVisibility(View.VISIBLE);
                following.setVisibility(View.GONE);
                followersSelected.setVisibility(View.GONE);
                followers.setVisibility(View.VISIBLE);

                customViewPager.setCurrentItem(0);

                break;

            case R.id.followers:

                followingSelected.setVisibility(View.GONE);
                following.setVisibility(View.VISIBLE);
                followersSelected.setVisibility(View.VISIBLE);
                followers.setVisibility(View.GONE);


                customViewPager.setCurrentItem(1);

                break;


            default:
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.followers_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.followers_selected) {
            Intent intent1 = new Intent(FollowingActivityActivity.this, SearchFollowingPlayersAndTeamsActivity.class);
            intent1.putExtra("user_id", userID);
            startActivity(intent1);
           /* Intent intent=new Intent();
            setResult(2,intent);
            finish();*/
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Players", "Teams"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new PlayersFollowing(admin, userID);

                case 1:
                    return new TeamFollowing(admin, userID);

                default:
                    return new PlayersFollowing(admin, userID);

            }

        }
    }
}
