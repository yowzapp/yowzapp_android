package com.yowzapp.playstore.activty;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 16/2/17.
 */
public class ResetPasswordActivity extends BaseActivity {

    private static final String passPattern = "(^(?=.*\\d)[0-9a-zA-Z]{8,}$)";
    Toolbar toolbar;
    TextView title;
    PreferenceManager mPref;
    Button save;
    EditText otp, newPassword, confirmPassword;
    TextInputLayout otpInputLayout, newPasswordInputLayout, confirmPasswordInputLayout;
    String email;
    private Pattern pattern;
    private Matcher matcher;
    private String preOtp;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password_layout);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar = (Toolbar) findViewById(R.id.resetPasswordToolbar);
        title = (TextView) findViewById(R.id.reset_password_title);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        initialize();

        if (getIntent().hasExtra("email") && getIntent().hasExtra("otp")) {
            email = getIntent().getStringExtra("email");
            Log.e("forgot_email", email);
            preOtp = getIntent().getStringExtra("otp");
            Log.e("forgot_otp", preOtp);

//            NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(ResetPasswordActivity.this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("OTP")
//                    .setContentText("OTP for resetting password is: " + preOtp)
//                    .setAutoCancel(true); // clear notification after click
//            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            mNotificationManager.notify(0, mBuilder.build());

//            Thread t = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(1000);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                otp.setText(preOtp);
//                            }
//                        });
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//            t.run();

        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForValidation()) {
                    updatePassword();
                }
            }
        });

        otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                otpInputLayout.setError(null);
            }
        });

        newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                newPasswordInputLayout.setError(null);
            }
        });

        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                confirmPasswordInputLayout.setError(null);
            }
        });
    }

    private void updatePassword() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(ResetPasswordActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("email", email);
            params.add("otp", otp.getText().toString());
            params.add("newPassword", newPassword.getText().toString());


            httpClient.post(Config.FORGOT_PASSWORD_VERIFY, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("resetSuccess", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 200) {
                            Toast.makeText(ResetPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getInt("statusCode") == 400 || jsonObject.getInt("statusCode") == 401) {
                            mPref.setAccessToken("");
                            Toast.makeText(ResetPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(ResetPasswordActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);
                            Toast.makeText(ResetPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            otpInputLayout.setError(jsonObject.getString("message"));

                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(ResetPasswordActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    private boolean checkForValidation() {
        if (otp.getText().toString().trim().isEmpty() || otp.getText().toString().contains(" ")) {
            otpInputLayout.setError("Otp is required");
            otpInputLayout.setErrorEnabled(true);
        } else {
            otpInputLayout.setErrorEnabled(false);
        }

        Log.e("otp", otp.getText().toString());

        if (newPassword.getText().toString().length() <= 7) {
            newPasswordInputLayout.setError("password should be atleast 8 characters");
            newPasswordInputLayout.setErrorEnabled(true);
        } else if (!(validate(newPassword.getText().toString()))) {
            newPasswordInputLayout.setError("Psst, your password should contain a numerical");
            newPasswordInputLayout.setErrorEnabled(true);
        } else {
            newPasswordInputLayout.setErrorEnabled(false);
        }
        Log.e("newPassword", newPassword.getText().toString());

        if (confirmPassword.getText().toString().trim().isEmpty() || confirmPassword.getText().toString().contains(" ")) {
            confirmPasswordInputLayout.setError("Confirm password is required");
            confirmPasswordInputLayout.setErrorEnabled(true);
        } else {
            confirmPasswordInputLayout.setErrorEnabled(false);
        }

        if (!(newPassword.getText().toString().equals(confirmPassword.getText().toString()))) {
            confirmPasswordInputLayout.setError("Password does not match");
            confirmPasswordInputLayout.setErrorEnabled(true);
        } else {
            confirmPasswordInputLayout.setErrorEnabled(false);
        }

        Log.e("confirmPassword", confirmPassword.getText().toString());

        if (otpInputLayout.isErrorEnabled() || newPasswordInputLayout.isErrorEnabled() || confirmPasswordInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean validate(final String password) {
        Log.e("pass", "" + password);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    private void initialize() {
        mPref = PreferenceManager.instance(getApplicationContext());
        pattern = Pattern.compile(passPattern);
        otp = (EditText) findViewById(R.id.otp);
        newPassword = (EditText) findViewById(R.id.newPassword);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        otpInputLayout = (TextInputLayout) findViewById(R.id.input_layout_otp);
        newPasswordInputLayout = (TextInputLayout) findViewById(R.id.input_layout_new_password);
        confirmPasswordInputLayout = (TextInputLayout) findViewById(R.id.input_layout_confirm_password);
        save = (Button) findViewById(R.id.save);

        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        // otp.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        newPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        confirmPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        otpInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        newPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        confirmPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }
}
