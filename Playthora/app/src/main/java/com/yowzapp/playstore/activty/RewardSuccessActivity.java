package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

public class RewardSuccessActivity extends BaseActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_success);
        TextView sixxer = (TextView) findViewById(R.id.awesome_text);
        TextView successText = (TextView) findViewById(R.id.success_text);
        Button back = (Button) findViewById(R.id.back_to_reward);
        back.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));
        successText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));
        sixxer.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_BOLD));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(RewardSuccessActivity.this, RewardStore.class);
                startActivity(login);
            }
        });

    }
}
