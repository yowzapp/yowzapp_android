package com.yowzapp.playstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.RewardViewHolder;
import com.yowzapp.playstore.model.TeamSearchModel;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 18/11/16.
 */
public class TeamsProfileAdapter extends RecyclerView.Adapter<RewardViewHolder> {

    private ArrayList<TeamSearchModel> arrayList;
    private Context context;


    public TeamsProfileAdapter(Context context,
                               ArrayList<TeamSearchModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;


    }


    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                R.layout.profile_edit_team_lay, viewGroup, false);
        RewardViewHolder listHolder = new RewardViewHolder(mainGroup);

        return listHolder;
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final RewardViewHolder holder, final int position) {
        final TeamSearchModel model = arrayList.get(position);


        final RewardViewHolder mainHolder = (RewardViewHolder) holder;

        try {
            mainHolder.sportsCategoryName.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            mainHolder.sportsCategoryName.setText(model.getName());

            Log.e("team_name", model.getName());

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


}