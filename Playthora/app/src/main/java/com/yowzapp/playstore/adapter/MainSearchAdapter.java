package com.yowzapp.playstore.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.GameMain;
import com.yowzapp.playstore.activty.OtherTeamViewActivity;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.fragment.VenueListFragment;
import com.yowzapp.playstore.model.SearchAllModelList;
import com.yowzapp.playstore.model.VenueListItem;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Created by nakul on 15/11/16.
 */
public class MainSearchAdapter extends RecyclerView.Adapter<MainSearchAdapter.ProfileListHolder> {
    Activity mContext;
    List<SearchAllModelList> searchList;
    String gName, gLocation, gImage, gDistance, gSports, aminities;
    int gRating;
    JSONArray VenueResponse;
    VenueListItem venueDetail;

    public MainSearchAdapter(Activity context, List<SearchAllModelList> arrayList, JSONArray response) {
        mContext = context;
        searchList = arrayList;
        VenueResponse = response;
    }

    public void RefreshPagination(List<SearchAllModelList> tempList, boolean b, JSONArray jsonArray) {
        searchList = tempList;
        VenueResponse = jsonArray;
        notifyDataSetChanged();
    }

    @Override
    public ProfileListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_layout, null);
        return new ProfileListHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ProfileListHolder holder, final int position) {

        try {
            if (searchList.get(position).getType().equalsIgnoreCase("players")) {
                Glide.with(mContext).load(searchList.get(position).getCover_pic()).error(R.drawable.circled_user).centerCrop().into(holder.image);
            } else if (searchList.get(position).getType().equalsIgnoreCase("team")) {
                Glide.with(mContext).load(searchList.get(position).getCover_pic()).error(R.drawable.no_team).centerCrop().into(holder.image);
            } else if (searchList.get(position).getType().equalsIgnoreCase("venue")) {
                Glide.with(mContext).load(searchList.get(position).getCover_pic()).error(R.drawable.game_null).centerCrop().into(holder.image);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (searchList.get(position).getType().equalsIgnoreCase("players")) {
                holder.image.setImageResource(R.drawable.circled_user);
            } else if (searchList.get(position).getType().equalsIgnoreCase("team")) {
                holder.image.setImageResource(R.drawable.no_team);
            } else if (searchList.get(position).getType().equalsIgnoreCase("venue")) {
                holder.image.setImageResource(R.drawable.game_null);            }
        }

        try {
            holder.name.setText(searchList.get(position).getName());
        } catch (Exception e) {
            holder.name.setText("");
        }

        try {
            if (searchList.get(position).getType().equalsIgnoreCase("team")) {
                holder.typeName.setText("Team");
//                holder.typeIcon.setImageResource(R.drawable.ic_team_icon);
                holder.typeIcon.setImageResource(R.drawable.team);
                holder.desc.setText(searchList.get(position).getNo_players() + " Players");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (searchList.get(position).getType().equalsIgnoreCase("venue")) {

                holder.typeName.setText("Place");
//                holder.typeIcon.setImageResource(R.drawable.ic_location_icon);
                holder.typeIcon.setImageResource(R.drawable.location);
                holder.desc.setText(searchList.get(position).getVenue_details().getLocation_name());

                //groundDetailDialog(getActivity(),gName, gLocation, gImage,gDistance,gRating,position,VenueResponse,tempList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (searchList.get(position).getType().equalsIgnoreCase("player")) {
                holder.typeName.setText("Player");
//                holder.typeIcon.setImageResource(R.drawable.ic_new_player_icon);
                holder.typeIcon.setImageResource(R.drawable.profile);
                holder.desc.setText(searchList.get(position).getPoints() + " Points");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (searchList.get(position).getType().equalsIgnoreCase("game")) {
                holder.typeName.setText("Game");
//                holder.typeIcon.setImageResource(R.drawable.ic_create_game_search);
                holder.typeIcon.setImageResource(R.drawable.game);
                holder.desc.setText(searchList.get(position).getNo_players() + " Players");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchList.get(position).getType().equalsIgnoreCase("team")) {
                    Intent i = new Intent(mContext, OtherTeamViewActivity.class);
                    i.putExtra("ID", searchList.get(position).getId());
                    i.putExtra("SEARCH", "?q=search");
                    mContext.startActivity(i);
                } else if (searchList.get(position).getType().equalsIgnoreCase("player")) {
                    Intent i = new Intent(mContext, PlayerProfileViewActivity.class);
                    i.putExtra("Id", searchList.get(position).getId() + "");
                    i.putExtra("SEARCH", "?q=search");
                    mContext.startActivity(i);

                } else if (searchList.get(position).getType().equalsIgnoreCase("game")) {
                    Intent i = new Intent(mContext, GameMain.class);
                    i.putExtra("GAME_ID", String.valueOf(searchList.get(position).getId()));
                    mContext.startActivity(i);
                } else if (searchList.get(position).getType().equalsIgnoreCase("venue")) {
                    venueDetail = searchList.get(position).getVenue_details();
                    Log.e("VENUED", venueDetail + "");
                    gName = searchList.get(position).getName();
                    gLocation = searchList.get(position).getVenue_details().getLocation_name();
                    gImage = searchList.get(position).getCover_pic();
                    gDistance = searchList.get(position).getVenue_details().getDistance();
                    gRating = searchList.get(position).getVenue_details().getRating();
                    gSports = searchList.get(position).getVenue_details().getSports_types();
                    aminities = searchList.get(position).getVenue_details().getAmenities();
                    VenueListFragment venueListFragment = new VenueListFragment();
                    try {
                        venueListFragment.groundDetailDialog(mContext, gName, gSports, aminities, gLocation,
                                gImage, gDistance, gRating, position, venueDetail.getGrounds(), VenueResponse.get(position).toString(), venueDetail.getImages(), false, false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView typeIcon, image;
        TextView name, typeName, desc;

        public ProfileListHolder(View itemView) {
            super(itemView);

            typeIcon = (ImageView) itemView.findViewById(R.id.typeIcon);
            image = (ImageView) itemView.findViewById(R.id.imageView_player_search);
            name = (TextView) itemView.findViewById(R.id.search_name);
            desc = (TextView) itemView.findViewById(R.id.description);
            typeName = (TextView) itemView.findViewById(R.id.typename);

            name.setTypeface(PlaythoraUtility.getFont(mContext, Config.RALEWAY_REGULAR));
            desc.setTypeface(PlaythoraUtility.getFont(mContext, Config.RALEWAY_REGULAR));
            typeName.setTypeface(PlaythoraUtility.getFont(mContext, Config.RALEWAY_REGULAR));
        }
    }

}


