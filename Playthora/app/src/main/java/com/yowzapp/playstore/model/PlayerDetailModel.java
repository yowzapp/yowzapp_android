package com.yowzapp.playstore.model;

/**
 * Created by hemanth on 7/1/17.
 */

public class PlayerDetailModel {

    private String status;
    private int statusCode;
    private String message;
    private PlayerDetailSuccess success;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PlayerDetailSuccess getSuccess() {
        return success;
    }

    public void setSuccess(PlayerDetailSuccess success) {
        this.success = success;
    }
}
