package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

/**
 * Created by hemanth on 16/11/16.
 */

public class MyProfile extends BaseActivity {

    TextView txtProfileName, txtProfileLevel, txtMyTransactions, txtMyPoints, txtMyPreferences, txtAppSettings;
    RelativeLayout myTransactions, myLeaderBoard, myPreference, settings;
    Button viewProfile;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_layout);

        txtProfileName = (TextView) findViewById(R.id.profileName);
        txtProfileLevel = (TextView) findViewById(R.id.profileLevel);
        txtMyTransactions = (TextView) findViewById(R.id.myTransactions);
        txtMyPoints = (TextView) findViewById(R.id.myPoints);
        txtMyPreferences = (TextView) findViewById(R.id.myPreferences);
        txtAppSettings = (TextView) findViewById(R.id.appSettings);
        viewProfile = (Button) findViewById(R.id.btn_view_profile);

        myTransactions = (RelativeLayout) findViewById(R.id.myTransactionLayout);
        myLeaderBoard = (RelativeLayout) findViewById(R.id.myPointsLayout);
        myPreference = (RelativeLayout) findViewById(R.id.myPreferencesLayout);
        settings = (RelativeLayout) findViewById(R.id.appSettingsLayout);

        txtProfileName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_BOLD));
        txtProfileLevel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        txtMyTransactions.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        txtMyPoints.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        txtMyPreferences.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        txtAppSettings.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        viewProfile.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));

        myTransactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transaction = new Intent(MyProfile.this, MyTransactionsActivity.class);
                startActivity(transaction);
            }
        });

        myLeaderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent leaderboard = new Intent(MyProfile.this, MyProfileLeaderBoardMain.class);
                startActivity(leaderboard);
            }
        });

        myPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent preference = new Intent(MyProfile.this, NotificationSettingsActivity.class);
                startActivity(preference);
            }
        });

    }
}
