package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.FollowersAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

public class FollowersListingActivity extends BaseActivity {

    FollowersAdapter adapter;
    Toolbar toolbar;
    RecyclerView recyclerFollowers;
    PreferenceManager mPref;
    String admin, userID;
    String url;
    AddTeamMembersModel model;
    Gson mGson;
    List<AddTeamMembersList> tempList;
    TextView noPlayers, noSearch;
    EditText search;
    String mSearchQuery;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers_listing);
        mPref = PreferenceManager.instance(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Intent intent = getIntent();
        admin = intent.getStringExtra("admin");
        userID = intent.getStringExtra("userId");
        Log.e("admin", admin);
        Log.e("userID", userID);

        search = (EditText) findViewById(R.id.edit_search_team);
        search.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        search.setText("");

        toolbar = (Toolbar) findViewById(R.id.followers_toolbar);
        TextView toolBarTitle = (TextView) findViewById(R.id.followers_toolbar_title);
        toolBarTitle.setText("Followers");
        noPlayers = (TextView) findViewById(R.id.noPlayers);
        noSearch = (TextView) findViewById(R.id.noSearch);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noSearch.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerFollowers = (RecyclerView) findViewById(R.id.followers_recycler);
        recyclerFollowers.setLayoutManager(new LinearLayoutManager(FollowersListingActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerFollowers.setHasFixedSize(true);
        try {
            PopulateRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mSearchQuery = search.getText().toString();
                tempList = performSearch(model.getSuccess().getData(), mSearchQuery);
                if (tempList.size() == 0) {
                    noSearch.setVisibility(View.VISIBLE);
                    noSearch.setText("No search result");
                } else {
                    noSearch.setVisibility(View.GONE);
                }

                getFollowers().RefreshPagination(tempList, true);
                getFollowers().notifyDataSetChanged();
            }
        });
    }


    private List<AddTeamMembersList> performSearch(List<AddTeamMembersList> success, String query) {
        // First we split the query so that we're able
        // to search word by word (in lower case).
        String[] queryByWords = query.toLowerCase().split("\"([\\w\\s]+)\"");

        // Empty list to fill with matches.
        List<AddTeamMembersList> mFiltered = new ArrayList<AddTeamMembersList>();

        // Go through initial releases and perform search.
        for (AddTeamMembersList movie : model.getSuccess().getData()) {

            // Content to search through (in lower case).
            String content = (
                    movie.getName()).toLowerCase();

            for (String word : queryByWords) {

                // There is a match only if all of the words are contained.
                int numberOfMatches = queryByWords.length;

                // All query words have to be contained,
                // otherwise the release is filtered out.
                if (content.contains(word)) {
                    numberOfMatches--;
                } else {
                    break;
                }

                // They all match.
                if (numberOfMatches == 0) {
                    mFiltered.add(movie);
                }

            }

        }

        return mFiltered;
    }

    private FollowersAdapter getFollowers() {
        return (FollowersAdapter) recyclerFollowers.getAdapter();
    }

    private void PopulateRecycler() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(FollowersListingActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();

            if (admin.equalsIgnoreCase("true")) {
                url = Config.FOLLOWERS_ME;
                Log.e("me", url);
            } else {
                url = Config.FOLLOWERS_ME + "&user_id=" + userID;
                Log.e("other", url);
            }
            httpClient.get(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.v("followers", s);
                        model = new AddTeamMembersModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, AddTeamMembersModel.class);
                        if (model.getSuccess().getData().size() != 0) {

                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }
                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerFollowers.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                Log.e("SUCCEss", String.valueOf(model.getSuccess()));
                                adapter = new FollowersAdapter(FollowersListingActivity.this, model.getSuccess().getData());
                                recyclerFollowers.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                noPlayers.setText("No followers");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            noPlayers.setText("No followers");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("failure", s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.followers_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.followers_selected) {
            if (toast != null) {
                toast.cancel();
            }
            if (model.getSuccess().getData().size() != 0) {
                search.setVisibility(View.VISIBLE);
                search.setHint("Search for followers");
            } else {
                toast = Toast.makeText(FollowersListingActivity.this, "No followers to search", Toast.LENGTH_SHORT);
                toast.show();
            }

            /*Intent intent=new Intent();
            setResult(2,intent);
            finish();*/
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
