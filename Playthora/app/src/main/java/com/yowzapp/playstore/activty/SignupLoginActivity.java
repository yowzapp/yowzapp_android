package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Nakul on 7/25/2016.
 */
public class SignupLoginActivity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final List<String> READ_PERMISSIONS = Arrays.asList("email");
    private static final int RC_SIGN_IN = 9003;
    private static final String TAG = "SignupLoginActivity";
    private static CallbackManager callbackManager;
    private static GoogleApiClient mGoogleApiClient;
    RelativeLayout signUpEmail, signUpFacebook, signUpGoogle, skipLayout;
    TextView login, email, facebook, gmail, alreadyAccount, skipNow, appName;
    String authCode, id_token, mail, googleImage, googleId, googleName, device_id, fcm_token, msg;
    Uri personPhoto;
    Context context;
    String accessToken, fname, facebookId, facebbookEmail, player_name, player_email, token, type;
    PreferenceManager mPref;
    String mobile_verified, accToken, isSocial, userName, profile_pic, emailValue, userMobileNumber, interest_update, location_update;
    double latitudeValue, longitudeValue;
    int user_id;
    String profilePicUrl;
    private ProgressDialog mProgressDialog;

    public static void signIn(Activity context1) {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        context1.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.new_signup);
        mPref = PreferenceManager.instance(getApplicationContext());

//        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        login = (TextView) findViewById(R.id.login);
        signUpEmail = (RelativeLayout) findViewById(R.id.relative_layout_email);
        signUpFacebook = (RelativeLayout) findViewById(R.id.relative_layout_facebook);
        signUpGoogle = (RelativeLayout) findViewById(R.id.relative_layout_google);
        skipLayout = (RelativeLayout) findViewById(R.id.skipLayout);
        email = (TextView) findViewById(R.id.email_text);
        facebook = (TextView) findViewById(R.id.facebook_text);
        gmail = (TextView) findViewById(R.id.google_text);
        alreadyAccount = (TextView) findViewById(R.id.txt_already_login);
        skipNow = (TextView) findViewById(R.id.skip_now);
        appName = (TextView) findViewById(R.id.appNamePlayThora);
        appName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_BOLD));
        email.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        facebook.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        gmail.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        alreadyAccount.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        skipNow.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        login.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_SEMIBOLD));
        callbackManager = CallbackManager.Factory.create();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login = new Intent(SignupLoginActivity.this, LoginActivity.class);
                startActivity(login);

            }
        });
        skipLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaythoraUtility.showProgressDialog(SignupLoginActivity.this);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            PlaythoraUtility.hideProgressDialog();
                            Intent intent = new Intent(SignupLoginActivity.this, HomeScreenActivity.class);
                            startActivity(intent);
                        }
                    }
                }).start();


            }
        });

        signUpEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sign = new Intent(SignupLoginActivity.this, SignUpActivity.class);
                startActivity(sign);
            }
        });

        signUpFacebook.setOnClickListener(this);
        signUpGoogle.setOnClickListener(this);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        String serverClientId = getResources().getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("ANDROID_ID", device_id);
        mPref.setAndroid_id(device_id);
        fcm_token = FirebaseInstanceId.getInstance().getToken();
        msg = getString(R.string.msg_token_fmt, fcm_token);
        mPref.setGcmToken(fcm_token);

        Log.e("gcmToken", "" + fcm_token);

    }

    public void FacebookLogin(Context context) {
        Log.e("fb", "facebook");
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(SignupLoginActivity.this, READ_PERMISSIONS);
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken().getToken();
                Log.v("ACCESSTOKEN", accessToken);


                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),

                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code
                                response.getError();
                                Log.e("fbResponse", object.toString());

                                try {
                                    facebookId = object.getString("id");
                                    String link = object.getString("link");
                                    fname = object.getString("name");
                                    facebbookEmail = object.getString("email");
                                    Log.e("facebookId", facebookId);
                                    Log.e("facebookEmail", facebbookEmail);
                                    Log.e("facebookLink", link);
                                    Log.e("facebookName", fname);

                                    // profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    profilePicUrl = "https://graph.facebook.com/" + facebookId + "/picture?type=large";
                                    Log.e("fb_image", profilePicUrl);


                                    // mPref.setProfilePic(profilePicUrl);


                                    socialLogin(accessToken, facebookId, facebbookEmail, fname, profilePicUrl);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,name,link,picture");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("ERROR", error.toString());
            }
        });
    }

    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "onActivityResult:GET_AUTH_CODE:success:" + result.getStatus().isSuccess());

            Log.v(TAG, result.getStatus() + "" + resultCode);
            Log.v("Response", String.valueOf(result.isSuccess()));


            if (result.isSuccess()) {
                // [START get_auth_code]
                GoogleSignInAccount acct = result.getSignInAccount();

                authCode = acct.getServerAuthCode();
                Log.e("AuthCode  " + authCode, " ");

                if (null != acct.getServerAuthCode()) {
                   /* authCode = acct.getServerAuthCode();
                    Log.e("AuthCode  "+ authCode, " ");*/
                }


                if (null != acct.getIdToken()) {
                    id_token = acct.getIdToken();
                    Log.e("id_token  " + id_token, " ");
                }

                Log.e("googleName " + acct.getDisplayName(), "NAME");
                if (null != acct.getDisplayName()) {
                    googleName = acct.getDisplayName();
                }
                Log.e("googleEmail " + acct.getEmail(), "EMAIL");
                if (null != acct.getEmail()) {
                    mail = acct.getEmail();
                }

                Log.e("googleId " + acct.getId(), " ");
                if (null != acct.getId()) {
                    googleId = acct.getId();
                }

                Log.e("googleImage ", String.valueOf(acct.getPhotoUrl()));
                googleImage = String.valueOf(acct.getPhotoUrl());
                Log.e("########", googleImage);
                if (null != personPhoto) {
                    personPhoto = acct.getPhotoUrl();
                    googleImage = personPhoto + "";
                    //  Log.e("googleImage",googleImage);

                    //  mPref.setProfilePic(googleImage);
                }

                try {
                    socialLogin(authCode, googleId, mail, googleName, googleImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        if (null != callbackManager)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void socialLogin(String token, final String id, final String email, final String name, String profilePic) {

        Log.e("Token", token);
        Log.e("Id", id);
        Log.e("email", email);
        Log.e("Name", name);
        Log.e("profile_Pic", profilePic);


        if (mPref.getGcmToken() == null) {
            fcm_token = FirebaseInstanceId.getInstance().getToken();
            mPref.setGcmToken(fcm_token);
        }

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(SignupLoginActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("name", name);
            params.add("email", email);
            params.add("device_type", "android");
            params.add("token", token);
            params.add("device_id", device_id);
            params.add("fcm_token", mPref.getGcmToken());
            params.add("profile_pic", profilePic);


            httpClient.post(Config.SOCIAL_LOGIN, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String sucess = new String(responseBody);
                    Log.e("success", sucess);

                    try {
                        JSONObject object = new JSONObject(sucess);
                        if (object.getInt("statusCode") == 200) {
                            user_id = object.getJSONObject("success").getInt("id");
                            mobile_verified = String.valueOf(object.getJSONObject("success").getBoolean("mobile_verified"));
                            isSocial = String.valueOf(object.getJSONObject("success").getBoolean("is_social"));
                            type = String.valueOf(object.getJSONObject("success").getBoolean("is_private"));
                            accToken = object.getString("accessToken");
                            userName = object.getJSONObject("success").getString("name");
                            profile_pic = object.getJSONObject("success").getString("profile_pic");
                            emailValue = object.getJSONObject("success").getString("email");
                            userMobileNumber = object.getJSONObject("success").getString("mobile");
                            interest_update = String.valueOf(object.getJSONObject("success").getBoolean("interest_updated"));
                            location_update = String.valueOf(object.getJSONObject("success").getBoolean("location_updated"));
                            //  latitudeValue = object.getJSONObject("success").getDouble("lat");
                            // longitudeValue = object.getJSONObject("success").getDouble("lng");

                            Log.e("mobileVerify", String.valueOf(mobile_verified));
                            Log.e("access_token", accToken);
                            Log.e("user_id", user_id + "");
                            Log.e("isSocial", isSocial + "");
                            Log.e("isPrivate", type + "");
                            Log.e("userName", userName);
                            Log.e("profile_pic", profile_pic);
                            Log.e("emailValue", emailValue);
                            Log.e("userMobileNumber", userMobileNumber);
                            Log.e("interest_updated", interest_update);
                            Log.e("location_update", location_update);

                            mPref.setId(String.valueOf(user_id));
                            mPref.setMobilVerified(mobile_verified);
                            mPref.setAccessToken(accToken);
                            mPref.setName(userName);
                            mPref.setProfilePic(profile_pic);
                            mPref.setUserEmail(emailValue);
                            mPref.setUserMobileNumber(userMobileNumber);

                            if (mPref.getMobilVerified().equalsIgnoreCase("false")) {
                                PlaythoraUtility.hideProgressDialog();
                                Intent intent = new Intent(SignupLoginActivity.this, NumberActivity.class);
                                startActivity(intent);
                                finishAffinity();
//                            }else if(mPref.getName().isEmpty()){
//                                Intent home = new Intent(SignupLoginActivity.this, ProfileSetUpActivity.class);
//                                startActivity(home);
//                                finishAffinity();
//                            }else if(mPref.getInterestUpdated().equalsIgnoreCase("false") || mPref.getLocationUpdated().equalsIgnoreCase("false")) {
//                                Intent intent = new  Intent(SignupLoginActivity.this,InterestsActivity.class);
//                                intent.putExtra("name",mPref.getName());
//                                startActivity(intent);
//                                finishAffinity();
                            } else {
                                Intent intent = new Intent(SignupLoginActivity.this, HomeScreenActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            }

                        }
                    } catch (JSONException e) {
                        PlaythoraUtility.hideProgressDialog();
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String failure = new String(responseBody);
                        Log.e("failure", failure);
                        // Toast.makeText(SignupLoginActivity.this,failure,Toast.LENGTH_SHORT).show();
                      /*  if (statusCode == 400) {
                            Toast.makeText(SignupLoginActivity.this, "session expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignupLoginActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_layout_google:
                signIn(SignupLoginActivity.this);
                break;

            case R.id.relative_layout_facebook:

                FacebookLogin(SignupLoginActivity.this);
              /* Intent facebook = new Intent(SignupLoginActivity.this,FaceBookActivity.class);
               facebook.putExtra("login",1);
               startActivity(facebook);*/

                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


}
