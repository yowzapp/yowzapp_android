package com.yowzapp.playstore.activty;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.NotificationList;
import com.yowzapp.playstore.model.NotificationModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 23/11/16.
 */

public class NotificationActivity extends BaseActivity {

    public NotificationAdapter adapter;
    public NotificationModel notificationArrayList;
    Toolbar toolbar;
    TextView toolbarTitle;
    RecyclerView notificationRecyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    RelativeLayout notification_toolbar;
    ImageView notificationBack;
    PreferenceManager mPref;
    TextView emptyText;
    ProgressBar progressBar, paginateProgressBar;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<NotificationList> tempList;
    boolean isNotificationTriggered = false;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
        InitializeAll();
        mPref = PreferenceManager.instance(this);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        notificationBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

     /*   toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/

        mLayoutManager = new LinearLayoutManager(NotificationActivity.this, LinearLayoutManager.VERTICAL, false);
        notificationRecyclerView.setLayoutManager(mLayoutManager);
        notificationRecyclerView.setHasFixedSize(true);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer_notification);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    populateNotification(1, false);
                    visibleThreshold = 10;
                    previousTotal = 0;
                    pages = 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);

        notificationRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                Log.e("isHasMorePages", String.valueOf(notificationArrayList.isHasMorePages()));
                try {
                    if (notificationArrayList.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                pages += 1;
                                try {
                                    paginateProgressBar.setVisibility(View.VISIBLE);
                                    populateNotification(pages, false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    paginateProgressBar.setVisibility(View.GONE);
                                }
                                adapter.notifyDataSetChanged();

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("isHasMorePages", String.valueOf(notificationArrayList.isHasMorePages()) + " onScrolled");
            }
        });

        populateNotification(pages, true);
    }

    @Override
    public void onBackPressed() {
        if (getIntent() != null && getIntent().hasExtra("notification_type")) {
            Intent intent = new Intent(NotificationActivity.this, HomeScreenActivity.class);
            startActivity(intent);
            finishAffinity();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        try {
//            visibleThreshold = 10;
//            previousTotal = 0;
//            pages = 1;
//            populateNotification(pages, true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    private void populateNotification(final int pages, boolean b) {
        Log.e("PAGE", pages + "");
        if (PlaythoraUtility.checkInternetConnection(NotificationActivity.this)) {
            try {
                swipeContainer.setRefreshing(true);
//                if(b)
//                    progressBar.setVisibility(View.VISIBLE);
//                else progressBar.setVisibility(View.GONE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.NOTIFICATION_LIST + "?page=" + pages,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                swipeContainer.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                Log.e("URL", Config.NOTIFICATION_LIST + "?page=" + pages);
                                String s = new String(responseBody);
                                Log.d("RESPONSENOTIFICATION", s);

                                mGson = new Gson();
                                notificationArrayList = new NotificationModel();
                                notificationArrayList = mGson.fromJson(s, NotificationModel.class);
                                Log.e("RESPONSE", String.valueOf(notificationArrayList.isHasMorePages()));

                                if (notificationArrayList.getSuccess().size() != 0) {

                                    if (notificationArrayList.getCurrentPage() > 1) {
                                        for (int i = 0; i < notificationArrayList.getSuccess().size(); i++) {
                                            tempList.add(notificationArrayList.getSuccess().get(i));
                                        }

                                        Log.e("venueList", String.valueOf(notificationArrayList.getCurrentPage()));
                                        adapter.Refresh(tempList, 2);
                                    } else {
                                        tempList = notificationArrayList.getSuccess();
                                        emptyText.setVisibility(View.GONE);
                                        adapter = new NotificationAdapter(NotificationActivity.this, notificationArrayList.getSuccess());
                                        notificationRecyclerView.setAdapter(adapter);
                                    }
                                } else {
                                    notificationRecyclerView.setAdapter(null);
                                    emptyText.setVisibility(View.VISIBLE);
                                }


                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                swipeContainer.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(NotificationActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
//                                    try{
//                                        String s = new String(bytes);
//                                        Log.d("RESPONSE_FAIL", s);
//                                        JSONObject object = new JSONObject(s);
//                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
//                                    }catch (Exception e1){
//                                        e1.printStackTrace();
//                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
//                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                paginateProgressBar.setVisibility(View.GONE);
            }
        } else {
            swipeContainer.setRefreshing(false);
//            Toast.makeText(NotificationActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
            paginateProgressBar.setVisibility(View.GONE);

        }
    }

    private void InitializeAll() {
        toolbarTitle = (TextView) findViewById(R.id.notification_toolbar_title);
        notificationRecyclerView = (RecyclerView) findViewById(R.id.notification_recycler_view);
        notification_toolbar = (RelativeLayout) findViewById(R.id.notificationToolbar);
        notificationBack = (ImageView) findViewById(R.id.notification_back_image);
        emptyText = (TextView) findViewById(R.id.empty_notification);
        progressBar = (ProgressBar) findViewById(R.id.progress_notification);
        paginateProgressBar = (ProgressBar) findViewById(R.id.notification_progress);

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        emptyText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;
        private List<NotificationList> arrayList;
        private Context mContext;


        public NotificationAdapter(Context context, List<NotificationList> arrayList) {
            this.mContext = context;
            this.arrayList = arrayList;


        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            // Log.e("viewType",arrayList.get(viewType).getType() );

            String type = arrayList.get(viewType).getType();
            if (type.equalsIgnoreCase("TME-PR") || type.equalsIgnoreCase("GME-PR") || type.equalsIgnoreCase("GMER")) {

                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.notification_follow_accept_layout, viewGroup, false);

                return new NotificationFollowAcceptViewHolder(view);

            } else if (type.equalsIgnoreCase("GME-PA")) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.notification_team_accept_layout, viewGroup, false);

                return new NotificationTeamAcceptViewHolder(view);
            } else if (type.equalsIgnoreCase("GME-PY") || type.equalsIgnoreCase("PLY-FR")) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.notification_player_accept_layout, viewGroup, false);

                return new NotificationPlayerAcceptViewHolder(view);
            } else {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.notification_item_layout, viewGroup, false);

                return new NotificationViewHolder(view);
            }
        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public void Refresh(List<NotificationList> arrayList, int i) {
            this.arrayList = arrayList;
            notifyDataSetChanged();
        }


        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            final NotificationList notificationModel = arrayList.get(position);
            final String type = notificationModel.getType();

            if (type.equalsIgnoreCase("TME-PR") || type.equalsIgnoreCase("GME-PR") || type.equalsIgnoreCase("GMER")) {
                final String[] mType = type.split("-");
                NotificationFollowAcceptViewHolder notificationFollowAcceptViewHolder = (NotificationFollowAcceptViewHolder) holder;

                if (mType[0].equalsIgnoreCase("GMER")) {
                    notificationFollowAcceptViewHolder.followAcceptDenyButton.setVisibility(View.GONE);
                    notificationFollowAcceptViewHolder.followAcceptAllowButton.setVisibility(View.GONE);
                    notificationFollowAcceptViewHolder.game.setVisibility(View.VISIBLE);
                } else {
                    notificationFollowAcceptViewHolder.followAcceptAllowButton.setVisibility(View.VISIBLE);
                    notificationFollowAcceptViewHolder.followAcceptDenyButton.setVisibility(View.VISIBLE);
                    notificationFollowAcceptViewHolder.game.setVisibility(View.GONE);
                }

                notificationFollowAcceptViewHolder.followAcceptText.setText(notificationModel.getContent());
                notificationFollowAcceptViewHolder.followAcceptText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationFollowAcceptViewHolder.followAcceptAllowButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationFollowAcceptViewHolder.followAcceptDenyButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationFollowAcceptViewHolder.game.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationFollowAcceptViewHolder.followAcceptTime.setText(notificationModel.getTime() + "");

                if (mType[0].equalsIgnoreCase("GMER")) {
                    if (notificationModel.getStatus() == 0) {
                        notificationFollowAcceptViewHolder.game.setVisibility(View.VISIBLE);
                        notificationFollowAcceptViewHolder.game.setText("Accept challenge");
                    } else if (notificationModel.getStatus() == 1) {
                        notificationFollowAcceptViewHolder.game.setText("Game on!");
                        notificationFollowAcceptViewHolder.game.setVisibility(View.VISIBLE);
                    } else if (notificationModel.getStatus() == 2) {
                        notificationFollowAcceptViewHolder.game.setText("Oops! missed it");
                        notificationFollowAcceptViewHolder.game.setVisibility(View.VISIBLE);
                    } else notificationFollowAcceptViewHolder.game.setVisibility(View.GONE);
                } else {
                    if (notificationModel.getStatus() == 1) {
                        notificationFollowAcceptViewHolder.followAcceptAllowButton.setVisibility(View.GONE);
                        notificationFollowAcceptViewHolder.followAcceptDenyButton.setVisibility(View.GONE);
                    } else {
                        notificationFollowAcceptViewHolder.followAcceptAllowButton.setVisibility(View.VISIBLE);
                        notificationFollowAcceptViewHolder.followAcceptDenyButton.setVisibility(View.VISIBLE);
                    }
                }

                if (!notificationModel.is_read())
                    notificationFollowAcceptViewHolder.readview.setVisibility(View.VISIBLE);
                else notificationFollowAcceptViewHolder.readview.setVisibility(View.GONE);

                try {
                    Glide.with(mContext).load(notificationModel.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(notificationFollowAcceptViewHolder.followAcceptImage);
                } catch (Exception e) {
                    notificationFollowAcceptViewHolder.followAcceptImage.setImageResource(R.drawable.circled_user);
                }
                notificationFollowAcceptViewHolder.followAcceptAllowButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AcceptTeam(position, true, mType[0]);
                    }
                });

                notificationFollowAcceptViewHolder.followAcceptDenyButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AcceptTeam(position, false, mType[0]);
                    }
                });

                notificationFollowAcceptViewHolder.game.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mType[0].equalsIgnoreCase("GMER") && notificationModel.getStatus() == 2) {

                        } else if (notificationModel.getStatus() == 0)
                            notifyRead(position, mType[0]);
                        else notifyRead(position, "GME");
                    }
                });

                notificationFollowAcceptViewHolder.followRelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mType[0].equalsIgnoreCase("GMER")) {
                            if (notificationModel.getStatus() == 0)
                                notifyRead(position, mType[0]);
                            else if (notificationModel.getStatus() == 2) {

                            } else notifyRead(position, "GME");
                        } else notifyRead(position, mType[0]);
                    }
                });

            } else if (type.equalsIgnoreCase("GME-PA")) {

                NotificationTeamAcceptViewHolder notificationTeamAcceptViewHolder = (NotificationTeamAcceptViewHolder) holder;

                notificationTeamAcceptViewHolder.teamAcceptText.setText(notificationModel.getContent());
                notificationTeamAcceptViewHolder.teamAcceptTime.setText(notificationModel.getTime());
                notificationTeamAcceptViewHolder.teamAcceptText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationTeamAcceptViewHolder.teamAcceptJoinButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationTeamAcceptViewHolder.teamAcceptDeclineButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

                if (!notificationModel.is_read())
                    notificationTeamAcceptViewHolder.readView.setVisibility(View.VISIBLE);
                else notificationTeamAcceptViewHolder.readView.setVisibility(View.GONE);

                if (notificationModel.getStatus() == 1) {
                    notificationTeamAcceptViewHolder.teamAcceptJoinButton.setVisibility(View.GONE);
                    notificationTeamAcceptViewHolder.teamAcceptDeclineButton.setVisibility(View.GONE);
                } else {
                    notificationTeamAcceptViewHolder.teamAcceptJoinButton.setVisibility(View.VISIBLE);
                    notificationTeamAcceptViewHolder.teamAcceptDeclineButton.setVisibility(View.VISIBLE);
                }
                try {
                    Glide.with(mContext).load(notificationModel.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(notificationTeamAcceptViewHolder.teamAcceptImage);
                } catch (Exception e) {
                    e.printStackTrace();
                    notificationTeamAcceptViewHolder.teamAcceptImage.setImageResource(R.drawable.circled_user);
                }

                notificationTeamAcceptViewHolder.teamAcceptJoinButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (type.equalsIgnoreCase("GME-PA")) {
                            AcceptGame(position, true);
                        }
                    }
                });

                notificationTeamAcceptViewHolder.teamAcceptDeclineButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (type.equalsIgnoreCase("GME-PA")) {
                            AcceptGame(position, false);
                        }
                    }
                });

                notificationTeamAcceptViewHolder.notificationTeamRelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (type.equalsIgnoreCase("GME-PA")) {
                            notifyRead(position, "GME");
                        }
                    }
                });

            } else if (type.equalsIgnoreCase("GME-PY") || type.equalsIgnoreCase("PLY-FR")) {

                NotificationPlayerAcceptViewHolder notificationPlayerAcceptViewHolder = (NotificationPlayerAcceptViewHolder) holder;

                notificationPlayerAcceptViewHolder.playerAcceptText.setText(notificationModel.getContent());
                notificationPlayerAcceptViewHolder.playerAcceptText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationPlayerAcceptViewHolder.playerAcceptYesButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationPlayerAcceptViewHolder.playerAcceptNoButton.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
                notificationPlayerAcceptViewHolder.playerAcceptTime.setText(notificationModel.getTime());
                if (type.equalsIgnoreCase("PLY-FR")) {
                    notificationPlayerAcceptViewHolder.playerAcceptYesButton.setText("Accept");
                    notificationPlayerAcceptViewHolder.playerAcceptNoButton.setText("Reject");
                } else {
                    notificationPlayerAcceptViewHolder.playerAcceptYesButton.setText("Yes");
                    notificationPlayerAcceptViewHolder.playerAcceptNoButton.setText("No");
                }

                if (notificationModel.getStatus() == 1) {
                    notificationPlayerAcceptViewHolder.playerAcceptYesButton.setVisibility(View.GONE);
                    notificationPlayerAcceptViewHolder.playerAcceptNoButton.setVisibility(View.GONE);
                } else {
                    notificationPlayerAcceptViewHolder.playerAcceptYesButton.setVisibility(View.VISIBLE);
                    notificationPlayerAcceptViewHolder.playerAcceptNoButton.setVisibility(View.VISIBLE);
                }

                try {
                    Glide.with(mContext).load(notificationModel.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(notificationPlayerAcceptViewHolder.playerAcceptImage);
                } catch (Exception e) {
                    notificationPlayerAcceptViewHolder.playerAcceptImage.setImageResource(R.drawable.circled_user);
                }

                if (!notificationModel.is_read())
                    notificationPlayerAcceptViewHolder.readView.setVisibility(View.VISIBLE);
                else notificationPlayerAcceptViewHolder.readView.setVisibility(View.GONE);

                notificationPlayerAcceptViewHolder.playerAcceptYesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* if(!notificationModel.is_read())
                            notifyRead(position,"");*/

                        if (type.equalsIgnoreCase("GME-PY")) {
                            Intent i = new Intent(NotificationActivity.this, GameMain.class);
                            i.putExtra("GAME_ID", String.valueOf(arrayList.get(position).getType_id()));
                            i.putExtra("isInvitePay", "true");
                            startActivity(i);
                            //finish();
                        } else if (type.equalsIgnoreCase("PLY-FR")) {
                            confirmRequest(position);
                        }
                    }
                });

                notificationPlayerAcceptViewHolder.playerAcceptNoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (type.equalsIgnoreCase("GME-PY"))
                            RejectPayment(position);
                        else if (type.equalsIgnoreCase("PLY-FR")) {
                            cancelRequest(position);
                        }

                    }
                });

                notificationPlayerAcceptViewHolder.playerRelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (type.equalsIgnoreCase("GME-PY")) {
                            if (!notificationModel.is_read()) {
                                notifyRead(position, "GME");
                            } else {
                                Intent i = new Intent(NotificationActivity.this, GameMain.class);
                                i.putExtra("GAME_ID", String.valueOf(arrayList.get(position).getType_id()));
                                i.putExtra("isInvitePay", "true");
                                startActivity(i);
                                //finish();
                            }

                        } else if (type.equalsIgnoreCase("PLY-FR")) {
                            if (!notificationModel.is_read()) {
                                notifyRead(position, "PLY");
                            } else {
                                Intent i = new Intent(NotificationActivity.this, PlayerProfileViewActivity.class);
                                i.putExtra("Id", arrayList.get(position).getType_id() + "");
                                startActivity(i);
                            }


                        }
                    }
                });

            } else {

                final String[] mType = type.split("-");
                NotificationViewHolder notificationViewHolder = (NotificationViewHolder) holder;

                notificationViewHolder.notification_text.setText(notificationModel.getContent());
                notificationViewHolder.time.setText(notificationModel.getTime());
                notificationViewHolder.notification_text.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

                if (!notificationModel.is_read())
                    notificationViewHolder.readView.setVisibility(View.VISIBLE);
                else notificationViewHolder.readView.setVisibility(View.GONE);

                try {
                    Glide.with(mContext).load(notificationModel.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(notificationViewHolder.circleImageView);
                } catch (Exception e) {
                    notificationViewHolder.circleImageView.setImageResource(R.drawable.circled_user);
                }

                notificationViewHolder.notificationRelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!notificationModel.is_read())
                            notifyRead(position, mType[0]);
                        else {
                            if (notificationModel.getBelongsTo().equalsIgnoreCase("user")) {
                                Intent i = new Intent(NotificationActivity.this, PlayerProfileViewActivity.class);
                                i.putExtra("Id", notificationModel.getType_id() + "");
                                startActivity(i);
                            }
                            else if (notificationModel.getBelongsTo().equalsIgnoreCase("game")) {
                                Intent i = new Intent(NotificationActivity.this, GameMain.class);
                                i.putExtra("GAME_ID", notificationModel.getType_id() + "");
                                startActivity(i);
                            } else if (notificationModel.getBelongsTo().equalsIgnoreCase("team")) {
                                Intent i = new Intent(NotificationActivity.this, OtherTeamViewActivity.class);
                                i.putExtra("ID", notificationModel.getType_id());
                                startActivity(i);
                            }
                        }
                    }
                });


            }

        }

        private void cancelRequest(int position) {
            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(NotificationActivity.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("id", String.valueOf(arrayList.get(position).getType_id()));
                httpClient.post(Config.PLAYER_REJECT, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("cancelRequestSuccess", s);

                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 200) {
                                populateNotification(pages, true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject jsonObject = new JSONObject(s);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        }


        private void confirmRequest(int position) {

            if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                PlaythoraUtility.showProgressDialog(NotificationActivity.this);
                AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
                httpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("id", String.valueOf(arrayList.get(position).getType_id()));
                httpClient.post(Config.PLAYER_ACCEPT, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.e("confirmRequestSuccess", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getInt("statusCode") == 200) {
                                populateNotification(pages, true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        }


        private void RejectPayment(final int position) {

            if (PlaythoraUtility.checkInternetConnection(NotificationActivity.this)) {
                try {
                    PlaythoraUtility.showProgressDialog(NotificationActivity.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("game_id", String.valueOf(arrayList.get(position).getType_id()));
                    Log.e("PARAMS", String.valueOf(params));
                    mHttpClient.post(Config.PAYMENT_REJECT, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            Toast.makeText(NotificationActivity.this, object.getString("success"), Toast.LENGTH_SHORT).show();
                                            populateNotification(pages, true);
                                                /*Intent i = new Intent(NotificationActivity.this, GameMain.class);
                                                i.putExtra("GAME_ID",arrayList.get(position).getType_id());
                                                startActivity(i);
                                                finish();*/
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(NotificationActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finishAffinity();

                                        } else
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
//                                        try{
//                                            String s = new String(bytes);
//                                            Log.d("RESPONSE_FAIL", s);
//                                            JSONObject object = new JSONObject(s);
//                                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
//                                        }catch (Exception e1){
//                                            e1.printStackTrace();
//                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
//                                        }

                                    }

                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();
                }
            }
//            } else
//                Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

        private void AcceptTeam(final int position, final boolean b, final String type) {
            String URL;
            if (type.equalsIgnoreCase("TME"))
                URL = b ? Config.TEAM_ACCEPT : Config.TEAM_REJECT;
            else
                URL = b ? Config.GAME_ACCEPT : Config.GAME_REJECT;

            if (PlaythoraUtility.checkInternetConnection(NotificationActivity.this)) {
                try {
                    PlaythoraUtility.showProgressDialog(NotificationActivity.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("type_id", String.valueOf(arrayList.get(position).getType_id()));
                    try {
                        params.add("user_id", arrayList.get(position).getMore());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("PARAMS", String.valueOf(params));
                    mHttpClient.post(URL, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            if (b) {
                                                if (type.equalsIgnoreCase("TME")) {
                                                    Toast.makeText(NotificationActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                                    Intent i = new Intent(NotificationActivity.this, OtherTeamViewActivity.class);
                                                    i.putExtra("ID", arrayList.get(position).getType_id());
                                                    startActivity(i);
                                                    //finish();
                                                } else {
                                                    Intent i = new Intent(NotificationActivity.this, GameMain.class);
                                                    i.putExtra("GAME_ID", String.valueOf(arrayList.get(position).getType_id()));
                                                    i.putExtra("isInvitePay", "true");
                                                    startActivity(i);
                                                    //finish();
                                                }
                                                arrayList.get(position).setIs_read(true);
                                                notifyDataSetChanged();
                                            } else {
                                                arrayList.remove(position);
                                                //Refresh(arrayList);
                                                visibleThreshold = 10;
                                                previousTotal = 0;
                                                pages = 1;
                                                populateNotification(pages, false);
                                            }

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(NotificationActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finishAffinity();

                                        } else
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
//                                        try{
//                                            String s = new String(bytes);
//                                            Log.d("RESPONSE_FAIL", s);
//                                            JSONObject object = new JSONObject(s);
//                                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
//                                        }catch (Exception e1){
//                                            e1.printStackTrace();
//                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
//                                        }

                                    }

                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();
                }
            }
//            } else
//                Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }

        private void notifyRead(final int position, final String name) {
            if (PlaythoraUtility.checkInternetConnection(NotificationActivity.this)) {
                try {
                    PlaythoraUtility.showProgressDialog(NotificationActivity.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("notification_id", String.valueOf(arrayList.get(position).getId()));
                    Log.e("PARAMS", String.valueOf(params));
                    mHttpClient.post(Config.NOTIFY_READ, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            if (name.equalsIgnoreCase("GME")) {
                                                Intent i = new Intent(NotificationActivity.this, GameMain.class);
                                                i.putExtra("GAME_ID", String.valueOf(arrayList.get(position).getType_id()));
                                                startActivity(i);
                                                //finish();
                                            } else if (name.equalsIgnoreCase("TME")) {
                                                Intent i = new Intent(NotificationActivity.this, OtherTeamViewActivity.class);
                                                i.putExtra("ID", arrayList.get(position).getType_id());
                                                startActivity(i);
                                                //finish();
                                            } else if (name.equalsIgnoreCase("GMER")) {
                                                Log.e("sport_id", "sport_id");
                                                Intent i = new Intent(NotificationActivity.this, CreateGameSelectLocation.class);
                                                i.putExtra("sport_id", arrayList.get(position).getType_id() + "");
                                                i.putExtra("ACTIVITY", "NOTIFIATION");
                                                startActivity(i);
//                                                finish();
                                            } else if (name.equalsIgnoreCase("PLY")) {
                                                Intent i = new Intent(NotificationActivity.this, PlayerProfileViewActivity.class);
                                                i.putExtra("Id", arrayList.get(position).getType_id() + "");
                                                startActivity(i);
                                            }
                                            arrayList.get(position).setIs_read(true);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(NotificationActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finishAffinity();

                                        } else
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        try {
                                            String s = new String(bytes);
                                            Log.d("RESPONSE_FAIL", s);
                                            JSONObject object = new JSONObject(s);
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();
                }
            }
//            } else
//                Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

        private void AcceptGame(final int position, final boolean b) {
            String URL = b ? Config.GAME_ACCEPT : Config.GAME_REJECT;

            if (PlaythoraUtility.checkInternetConnection(NotificationActivity.this)) {
                try {
                    PlaythoraUtility.showProgressDialog(NotificationActivity.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("type_id", String.valueOf(arrayList.get(position).getType_id()));
                    //params.add("user_id", String.valueOf(mPref.getId()));
                    Log.e("PARAMS", String.valueOf(params));
                    mHttpClient.post(URL, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {
                                            if (b) {
                                                Intent i = new Intent(NotificationActivity.this, GameMain.class);
                                                i.putExtra("GAME_ID", String.valueOf(arrayList.get(position).getType_id()));
                                                startActivity(i);
                                                //finish();
                                                arrayList.get(position).setIs_read(true);
                                                notifyDataSetChanged();
                                            } else if (!b) {
                                                arrayList.remove(position);
                                                //Refresh(arrayList);
                                                visibleThreshold = 10;
                                                previousTotal = 0;
                                                pages = 1;
                                                populateNotification(pages, true);
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(NotificationActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finishAffinity();

                                        } else
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
//                                    try{
//                                        String s = new String(bytes);
//                                        Log.d("RESPONSE_FAIL", s);
//                                        JSONObject object = new JSONObject(s);
//                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
//                                    }catch (Exception e1){
//                                        e1.printStackTrace();
//                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
//                                    }

                                    }

                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();
                }
            }
//            } else
//                Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        TextView notification_text, time;
        CircleImageView circleImageView;
        RelativeLayout notificationRelative;
        View readView;


        public NotificationViewHolder(View iteView) {
            super(iteView);
            this.notification_text = (TextView) iteView.findViewById(R.id.notification_text);
            this.time = (TextView) iteView.findViewById(R.id.notificationTime);
            this.circleImageView = (CircleImageView) iteView.findViewById(R.id.notification_image);
            this.notificationRelative = (RelativeLayout) iteView.findViewById(R.id.notificationLayout);
            this.readView = (View) iteView.findViewById(R.id.read_view_four);


        }

    }

    public class NotificationFollowAcceptViewHolder extends RecyclerView.ViewHolder {

        CircleImageView followAcceptImage;
        TextView followAcceptText, followAcceptTime;
        Button followAcceptAllowButton, followAcceptDenyButton, game;
        RelativeLayout followRelative;
        View readview;

        public NotificationFollowAcceptViewHolder(View itemView) {
            super(itemView);
            this.followAcceptImage = (CircleImageView) itemView.findViewById(R.id.notification_image_follow_accept);
            this.followAcceptText = (TextView) itemView.findViewById(R.id.notificationFollowAcceptMessage);
            this.followAcceptAllowButton = (Button) itemView.findViewById(R.id.notificationFollowAcceptAllowButton);
            this.followAcceptDenyButton = (Button) itemView.findViewById(R.id.notificationFollowAcceptDenyButton);
            this.game = (Button) itemView.findViewById(R.id.notificationCreateGame);
            this.followAcceptTime = (TextView) itemView.findViewById(R.id.notificationFollowAcceptTime);
            this.followRelative = (RelativeLayout) itemView.findViewById(R.id.notificationFollowAcceptLayout);
            this.readview = (View) itemView.findViewById(R.id.read_view);

        }
    }

    public class NotificationTeamAcceptViewHolder extends RecyclerView.ViewHolder {

        CircleImageView teamAcceptImage;
        TextView teamAcceptText, teamAcceptTime;
        Button teamAcceptJoinButton, teamAcceptDeclineButton;
        RelativeLayout notificationTeamRelative;
        View readView;

        public NotificationTeamAcceptViewHolder(View itemView) {
            super(itemView);

            this.teamAcceptImage = (CircleImageView) itemView.findViewById(R.id.notification_image_team_accept);
            this.teamAcceptText = (TextView) itemView.findViewById(R.id.notificationTeamAcceptMessage);
            this.teamAcceptJoinButton = (Button) itemView.findViewById(R.id.notificationTeamAcceptJoinButton);
            this.teamAcceptDeclineButton = (Button) itemView.findViewById(R.id.notificationTeamAcceptDeclineButton);
            this.teamAcceptTime = (TextView) itemView.findViewById(R.id.notificationTeamAcceptTime);
            this.notificationTeamRelative = (RelativeLayout) itemView.findViewById(R.id.notificationTeamAcceptLayout);
            this.readView = (View) itemView.findViewById(R.id.read_view_two);


        }
    }

    public class NotificationPlayerAcceptViewHolder extends RecyclerView.ViewHolder {

        CircleImageView playerAcceptImage;
        TextView playerAcceptText, playerAcceptTime;
        Button playerAcceptYesButton, playerAcceptNoButton;
        RelativeLayout playerRelative;
        View readView;

        public NotificationPlayerAcceptViewHolder(View itemView) {
            super(itemView);

            this.playerAcceptImage = (CircleImageView) itemView.findViewById(R.id.notification_image_player_accept);
            this.playerAcceptText = (TextView) itemView.findViewById(R.id.notificationPlayerAcceptMessage);
            this.playerAcceptYesButton = (Button) itemView.findViewById(R.id.notificationPlayerAcceptYesButton);
            this.playerAcceptNoButton = (Button) itemView.findViewById(R.id.notificationPlayerAcceptNoButton);
            this.playerAcceptTime = (TextView) itemView.findViewById(R.id.notificationPlayerAcceptTime);
            this.playerRelative = (RelativeLayout) itemView.findViewById(R.id.notificationPlayerAcceptLayout);
            this.readView = (View) itemView.findViewById(R.id.read_view_three);

        }
    }
}
