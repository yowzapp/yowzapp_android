package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by hemanth on 26/12/16.
 */

public class VenueListSportsTypeModel {

    private String status;
    private String message;
    private List<VenueListSportsTypeItem> success;
    private int currentPage;
    private boolean hasMorePages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public List<VenueListSportsTypeItem> getSuccess() {
        return success;
    }

    public void setSuccess(List<VenueListSportsTypeItem> success) {
        this.success = success;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

}
