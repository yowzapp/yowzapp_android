package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.HomeScreenActivity;
import com.yowzapp.playstore.activty.OtherTeamViewActivity;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.model.AddTeamMembersModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by nakul on 24/11/16.
 */
public class TeamFollowing extends Fragment {

    RecyclerView recyclerView;
    ProgressDialog dialog;
    Gson mGson;
    ProgressBar progressBar;
    TextView noPlayers;
    TextView invite;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<AddTeamMembersList> tempList;
    AddTeamMembersModel model;
    TeamAdapter adapter;
    Gson gson;
    PreferenceManager mPref;
    String admin, userID, url;
    EditText editText;
    Button teamSection;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;
    private int visibleThreshold = 5;

    public TeamFollowing(String admin, String userID) {
        this.admin = admin;
        this.userID = userID;

        // editText.setHint("Search for teams");
        Log.e("admin", admin);
        Log.e("userID", userID);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.following_fragment, container, false);
        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.following_recycler);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);

        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        noPlayers = (TextView) contentView.findViewById(R.id.noPlayers);
        teamSection = (Button) contentView.findViewById(R.id.go_to_leaderboard);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        teamSection.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

        try {
            PopulateRecycler(pages);
        } catch (Exception e) {
            e.printStackTrace();
        }

        teamSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HomeScreenActivity.class);
                intent.putExtra("ACTIVITY", 1);
                startActivity(intent);
                getActivity().finishAffinity();
            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("ONSCROLL", "onScrollStateChanged");
                Log.e("ONSCROLL", "onScrollStateChanged");
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (model.isHasMorePages()) {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                            pages += 1;
                            //venueListAdapter.loadData(venueListModel.getSuccess(),1);
                            try {
                                PopulateRecycler(pages);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            adapter.notifyDataSetChanged();

                        }
                    }
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return contentView;

    }

    private void PopulateRecycler(int pages) {
        Log.e("teamList", "#####");
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            if (admin.equalsIgnoreCase("true")) {
                url = Config.FOLLOWING_TEAM_ME;
                Log.e("me", url);
            } else {
                url = Config.FOLLOWING_TEAM_ME + "&user_id=" + userID;
                Log.e("other", url);
            }

            httpClient.get(url + "&page=" + pages, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);

                    try {
                        String s = new String(responseBody);
                        Log.e("FollowingTeams", s);
                        model = new AddTeamMembersModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, AddTeamMembersModel.class);


                        if (model.getSuccess().getData().size() != 0) {

                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getData().size(); i++) {
                                    tempList.add(model.getSuccess().getData().get(i));
                                }

                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerView.getAdapter() == null) {
                                tempList = model.getSuccess().getData();
                                adapter = new TeamAdapter(getActivity(), model.getSuccess().getData());
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                                teamSection.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                teamSection.setVisibility(View.VISIBLE);
                                teamSection.setText("Go to teams section");
                                noPlayers.setText("You are not following any team yet. Find out which teams are playing near you.");
                            }


                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            teamSection.setVisibility(View.VISIBLE);
                            teamSection.setText("Go to teams section");
                            noPlayers.setText("You are not following any team yet. Find out which teams are playing near you.");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    private class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.PlayersHolder> implements Filterable {


        private List<AddTeamMembersList> arrayList;
        private Context context;
        private UserFilter userFilter;
        private List<AddTeamMembersList> filteredUserList;


        public TeamAdapter(Context context, List<AddTeamMembersList> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
            this.filteredUserList = arrayList;
        }

        @Override
        public Filter getFilter() {
            if (userFilter == null)
                userFilter = new UserFilter(this, arrayList);
            return userFilter;
        }

        public void RefreshPagination(List<AddTeamMembersList> newString, boolean pagination) {
            if (pagination) {
                this.arrayList = newString;
            }
            notifyDataSetChanged();
        }

        @Override
        public PlayersHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.player_following_item, viewGroup, false);
            PlayersHolder listHolder = new PlayersHolder(mainGroup);
            return listHolder;

        }

        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final PlayersHolder holder, final int position) {
            final AddTeamMembersList model = arrayList.get(position);


            final PlayersHolder mainHolder = (PlayersHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());


                Log.e("name", model.getName());
                Log.e("pic", model.getCover_pic());


            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                e.printStackTrace();
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), OtherTeamViewActivity.class);
                    intent.putExtra("ID", model.getId());
                    startActivity(intent);
                }
            });

            mainHolder.itemView.setTag(model);


        }

        public class UserFilter extends Filter {

            private TeamAdapter adapter;

            private List<AddTeamMembersList> originalList;

            private List<AddTeamMembersList> filteredList;

            public UserFilter(TeamAdapter adapter, List<AddTeamMembersList> originalList) {
                super();
                this.adapter = adapter;
                this.originalList = new ArrayList<>(originalList);
                this.filteredList = new ArrayList<>();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filteredList.clear();
                final FilterResults results = new FilterResults();
                Log.e("STRING", constraint + "");
                Log.e("LIST", constraint.length() + "");

                if (constraint.length() == 0) {
                    filteredList.addAll(originalList);
                    Log.e("STRING", "0");
                } else {
                    Log.e("STRING", constraint.length() + "");
                    final String filterPattern = constraint.toString().toLowerCase().trim();
                    for (final AddTeamMembersList user : originalList) {
                        if (user.getName().toLowerCase().contains(filterPattern)) {
                            filteredList.add(user);
                        }
                    }
                }
                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                adapter.filteredUserList.clear();
                adapter.filteredUserList.addAll((List<AddTeamMembersList>) results.values);
                adapter.notifyDataSetChanged();
            }
        }

        public class PlayersHolder extends RecyclerView.ViewHolder {
            CircleImageView userImage;
            TextView name;

            public PlayersHolder(View view) {
                super(view);
                this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
                this.name = (TextView) view.findViewById(R.id.userName);


            }
        }
    }
}