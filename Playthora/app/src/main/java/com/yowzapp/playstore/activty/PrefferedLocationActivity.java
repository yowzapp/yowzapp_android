package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.adapter.LocationAdapter;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

public class PrefferedLocationActivity extends BaseActivity {
    public static TextView sectedNo;
    static ArrayList<LocationModel> arrayList;
    RecyclerView recyclerView;
    Toolbar toolbar;
    TextView toolbarDone;
    String getResponse;
    PreferenceManager mPref;
    com.yowzapp.playstore.model.LocationModel locationModel;
    Gson mGson;
    //    String response ="[{\n" +
//            "    \n" +
//            "    \"id\": 3,\n" +
//            "    \"loc\": \"HSR CLUB, hsr layout\"\n" +
//            "   \n" +
//            "}, {\n" +
//            "    \n" +
//            "    \"id\": 3,\n" +
//            "    \"loc\": \"BTM CLUB, BTM layout\"\n" +
//            "    \n" +
//            "}, {\n" +
//            "  \n" +
//            "    \"id\": 3,\n" +
//            "    \"loc\": \"Foolball CLUB, jpnagar\"\n" +
//            "}, {\n" +
//            "    \n" +
//            "    \"id\": 3,\n" +
//            "    \"loc\": \"HSR CLUB\"\n" +
//            "}]";
    String response = "";
    private ListView mSearchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preffered_location);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        mPref = PreferenceManager.instance(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.favouriteSportsToolbar);
        toolbarDone = (TextView) findViewById(R.id.toolbar_done);
        TextView toolBarTitle = (TextView) findViewById(R.id.fav_sports_toolbar_title);

        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        toolbarDone.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        toolBarTitle.setText("Preferred Locations");

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(2, intent);
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.team_members_recycler);
        sectedNo = (TextView) findViewById(R.id.invitePlaythora);
        sectedNo.setTypeface(PlaythoraUtility.getFont(PrefferedLocationActivity.this, RALEWAY_REGULAR));
        recyclerView.setLayoutManager(new LinearLayoutManager(PrefferedLocationActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);

        loadCity();

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.fav_sports_main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.fav_sports_selected) {
//            Intent intent = new Intent();
//            setResult(2, intent);
//            finish();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    private void loadCity() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PrefferedLocationActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            httpClient.get(Config.LOCATION_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("locationList", s);

                        Gson gson = new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONArray jsonArray = jsonObject.getJSONArray("success");
                            arrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                arrayList.add(gson.fromJson(jsonObject1.toString(), LocationModel.class));
                                LocationAdapter teamAdapter = new LocationAdapter(PrefferedLocationActivity.this, arrayList, PrefferedLocationActivity.this);
                                recyclerView.setAdapter(teamAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
    }
}