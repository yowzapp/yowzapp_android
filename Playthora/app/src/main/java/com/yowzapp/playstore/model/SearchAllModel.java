package com.yowzapp.playstore.model;

import java.util.List;

/**
 * Created by vaishakha on 7/1/17.
 */
public class SearchAllModel {

    private String status;
    private int statusCode;
    private List<SearchAllModelList> success;
    private int currentPage;
    private boolean hasMorePages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<SearchAllModelList> getSuccess() {
        return success;
    }

    public void setSuccess(List<SearchAllModelList> success) {
        this.success = success;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }
}
