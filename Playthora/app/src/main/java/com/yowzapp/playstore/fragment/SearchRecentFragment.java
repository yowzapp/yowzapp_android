package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.activty.MainSearchActivity;
import com.yowzapp.playstore.adapter.SearchRecentPlayerAdapter;
import com.yowzapp.playstore.adapter.TeamSearchAdapter;
import com.yowzapp.playstore.model.SearchRecentModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

public class SearchRecentFragment extends Fragment {
    Gson mGson;
    ProgressDialog dialog;
    TextView search;
    TextView recent, recentTeam;
    TextView noRecent;
    View contentView;
    Gson gson;
    PreferenceManager mPref;
    SearchRecentModel model;
    TeamSearchAdapter teamAdapter;
    SearchRecentPlayerAdapter searchAdapter;
    private RecyclerView recyclerView, recyclerViewteams;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(contentView==null) {
            contentView = inflater.inflate(R.layout.activity_search_recent, container, false);
            mPref = PreferenceManager.instance(getActivity());
            initialize();
        }
        recent.setVisibility(View.GONE);

        return contentView;
    }

    @Override
    public void onDestroyView() {
        if(contentView.getParent()!=null) {
            ((ViewGroup) contentView.getParent()).removeView(contentView);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("SearchRecentFragment", "onResume");
        try {
            if (!mPref.getAccessToken().isEmpty())
                populateSearch();
            else {
                noRecent.setVisibility(View.VISIBLE);
                recent.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void populateSearch() {

        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                //progressBar.setVisibility(View.VISIBLE);
                //.setVisibility(View.GONE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SEARCH_RECENT,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                //progressBar.setVisibility(View.GONE);
                                try {
                                    final String s = new String(responseBody);
                                    Log.e("SEARCH_RESPONSE", s);
                                    Log.e("RANK", "7");
                                    model = new SearchRecentModel();
                                    mGson = new Gson();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            model = mGson.fromJson(s, SearchRecentModel.class);
                                        }
                                    });

                                    if (model.getStatus().equalsIgnoreCase("success")) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (model.getSuccess().getPlayers().size() == 0
                                                        && model.getSuccess().getTeams().size() == 0) {
                                                    noRecent.setVisibility(View.VISIBLE);
                                                }
                                                if (model.getSuccess().getTeams().size() > 0) {
                                                    recentTeam.setVisibility(View.VISIBLE);
                                                    noRecent.setVisibility(View.GONE);
                                                    teamAdapter = new TeamSearchAdapter(getActivity(), model.getSuccess().getTeams());
                                                    recyclerViewteams.setAdapter(teamAdapter);
                                                } else
                                                    recentTeam.setVisibility(View.GONE);
                                                Log.e("RESPONSE", String.valueOf(model.getSuccess().getTeams().size()));

                                                if (model.getSuccess().getPlayers().size() > 0) {
                                                    recent.setVisibility(View.VISIBLE);
                                                    noRecent.setVisibility(View.GONE);
                                                    searchAdapter = new SearchRecentPlayerAdapter(getActivity(), model.getSuccess().getPlayers());
                                                    recyclerView.setAdapter(searchAdapter);
                                                } else recent.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                    Log.e("RESPONSE", String.valueOf(model.getSuccess().getPlayers().size()));


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // progressBar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                //  progressBar.setVisibility(View.GONE);

                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    }//else Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(MainSearchActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                //progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    private void initialize() {
        recent = contentView.findViewById(R.id.recent_text);
        recentTeam = contentView.findViewById(R.id.recent_team);
        search = contentView.findViewById(R.id.edit_search);
        noRecent = contentView.findViewById(R.id.no_recent_searches);
        recyclerView = contentView.findViewById(R.id.recycler_view_horizontal_players);
        recyclerViewteams = contentView.findViewById(R.id.recycler_view_horizontal_teams);


        recent.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
        recentTeam.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
        search.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        noRecent.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

        recyclerView.setHasFixedSize(true);
        recyclerViewteams.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewteams.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(getActivity(), MainSearchActivity.class);
                startActivity(searchIntent);
            }
        });
    }
}
