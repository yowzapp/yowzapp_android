package com.yowzapp.playstore.activty;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 7/26/2016.
 */
public class VerifyActivity extends BaseActivity {

    RelativeLayout enter;
    TextInputLayout otpBoxInputLayout;
    EditText otpBox;
    ImageView back;
    TextView toolbarTitle, otpdoesntmatch, timeText, resend, userNum, verifySubText, verifyMainText;
    CountDownTimer waitingTimer, retryTimer;
    Button send;
    String[] permissionsSms = new String[]{
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
    };
    PreferenceManager mPref;
    String number;
    boolean value;
    private boolean isSMSRecevied = false;
    private boolean writeAccepted;
    private int PERMISSION_REQUEST_CODE = 200;
    private String marshmallow = "lollipop";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_layout);
        mPref = PreferenceManager.instance(getApplicationContext());

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //  getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); getSupportActionBar().setCustomView(R.layout.toolbar_layout);
        //  toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
     /*   toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // onBackPressed();
            }
        });*/

        Intent i = getIntent();
        number = i.getStringExtra("verifyNum");


        otpBox = (EditText) findViewById(R.id.otpId);
        //setUpUi(findViewById(R.id.verify_activity));
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        otpdoesntmatch = (TextView) findViewById(R.id.otp_desnt_match);
        timeText = (TextView) findViewById(R.id.verificationCoundDownTime);
        userNum = (TextView) findViewById(R.id.verify_number);
        send = (Button) findViewById(R.id.verificationButton);
        verifySubText = (TextView) findViewById(R.id.verify_text);
        verifyMainText = (TextView) findViewById(R.id.verify_numb_text);

        verifySubText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        // userNum.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        // timeText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        // otpBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        send.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        verifyMainText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        otpdoesntmatch.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        Log.e("MESS", "comming");

        userNum.setText(number);

        if (shouldAskPermission()) {
            writeAccepted = true;
            String[] perms = {"android.permission.RECEIVE_SMS", "android.permission.READ_SMS"};

            int permsRequestCode = 200;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, permsRequestCode);
            }
        } else {
            writeAccepted = false;
            // timerstart();
            mobileVerify();

        }


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpBox.setText(" ");
                resendOtp();
            }
        });


        otpBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 6) {
                    if (checkForAllValidation()) {
                        if (otpBox.getText().toString().contains(" ")) {
                            otpBox.setText(otpBox.getText().toString().replaceAll(" ", ""));
                            otpBox.setSelection(otpBox.getText().length());

                            Toast.makeText(getApplicationContext(), "No Spaces Allowed", Toast.LENGTH_LONG).show();
                        } else {
                            otpVerification();
                        }

                    }

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                SmsRadar.stopSmsRadarService(VerifyActivity.this);
                otpdoesntmatch.setVisibility(View.GONE);

            }
        });


    }

    private boolean checkPermissionSms() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissionsSms) {
            result = ContextCompat.checkSelfPermission(VerifyActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private boolean shouldAskPermission() {

        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                Log.e("RERERE", writeAccepted + "");
                if (writeAccepted) {
                    //timerstart();
                    mobileVerify();
                } else {
                    marshmallow = "MarshMallow";
                    // timerstart();
                    mobileVerify();
                    SmsRadar.stopSmsRadarService(VerifyActivity.this);
                    // Toast.makeText(VerifyActivity.this,"Please enter manually",Toast.LENGTH_SHORT).show();
                }
                break;

        }

    }

    private void mobileVerify() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerifyActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            RequestParams params = new RequestParams();
            params.add("mobile", number);
            params.add("user_id", mPref.getId());

            httpClient.post(Config.MOBILE_VEROFICATION, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("mobileSuccess", s);

                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {
                            Toast.makeText(VerifyActivity.this, object.getString("message"), Toast.LENGTH_LONG).show();


                            //Automatically fill the otp in OTP box
//                            otpBox.setText(String.valueOf(object.getInt("OTP")));

                            if (!marshmallow.equalsIgnoreCase("MarshMallow")) {
                                registerSMSReceiver();
                            } else {
                                Toast.makeText(VerifyActivity.this, "Please enter manually", Toast.LENGTH_SHORT).show();
                            }
                            timerstart();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("mobileFail", s);

                }
            });
        }
    }


    private void resendOtp() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerifyActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("user_id", mPref.getId());
            params.add("mobile", number);

            httpClient.post(Config.RESEND_OTP, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("resendSuccess", s);

                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statuscode") == 200) {
                            Toast.makeText(VerifyActivity.this, "OTP resent successful", Toast.LENGTH_SHORT).show();
//                            otpBox.setText(String.valueOf(object.getInt("OTP")));
                            if (!marshmallow.equalsIgnoreCase("MarshMallow")) {
                                registerSMSReceiver();
                            } else {
                                timerstart();
                                Toast.makeText(VerifyActivity.this, "Please enter manually", Toast.LENGTH_SHORT).show();
                            }

                            // timerstart();
                            // registerSMSReceiver();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    if (null != responseBody) {
                        String s = new String(responseBody);
                        Log.e("resendFailure", s);
                    }


                }
            });
        }
    }


    private void timerstart() {


        waitingTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.e("time", String.valueOf(millisUntilFinished / 1000));
                if (String.valueOf(millisUntilFinished / 1000).length() == 2) {
                    timeText.setText("0:" + millisUntilFinished / 1000);
                } else {
                    timeText.setText("0:0" + millisUntilFinished / 1000);
                }

                send.setEnabled(false);
                timeText.setTextColor(getResources().getColor(R.color.text_one));
                timeText.setTypeface(Typeface.DEFAULT);
                timeText.setTextSize(16);


            }

            public void onFinish() {

                if (!isSMSRecevied) {
                    retryTimer = new CountDownTimer(1000, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            timeText.setText("0:0" + millisUntilFinished / 1000);
                            try {
                                //  registerSMSReceiver();
                            } catch (NullPointerException e) {

                            }

                        }

                        @Override
                        public void onFinish() {
                            // otpdoesntmatch.setVisibility(View.VISIBLE);
                            SmsRadar.stopSmsRadarService(VerifyActivity.this);
                            send.setEnabled(true);
                            timeText.setText("Please enter the code in SMS manually");
                            timeText.setTextColor(getResources().getColor(R.color.red));
                            timeText.setTextSize(10);
                            timeText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


                        }

                    };
                    retryTimer.start();
                }

            }
        };
        waitingTimer.start();

    }


    private void registerSMSReceiver() {
        //Log.e("SMgjhgjggjhgjgjgjgjhgjgjhgjS", "wjhgjhgjhgjhgorking");

        SmsRadar.initializeSmsRadarService(VerifyActivity.this, new SmsListener() {

            @Override
            public void onSmsSent(Sms sms) {

            }

            @Override
            public void onSmsReceived(Sms sms) {
                // Log.e("SMgjhgjggjhgjgjgjgjhgjgjhgjS", "wjhgjhgjhgjhgorking");
                String val = String.valueOf(sms);
                if (val.contains("PLYTHR") || val.contains("PAZZOM")) {
                    if (val.contains("msg")) {
                        String id = new String(val);
                        int startindex = id.indexOf("OTP");
                        int endindex = id.indexOf("to");
                        String number = id.substring(startindex, endindex);
                        String numberOnly = number.replaceAll("[^0-9]", "");
                        Log.v("Number", numberOnly);
                        otpBox.setText(numberOnly);
                        isSMSRecevied = true;
                        try {
                            waitingTimer.cancel();
                        } catch (Exception e) {
                        }
                        try {
                            otpVerification();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void otpVerification() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerifyActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            RequestParams params = new RequestParams();
            params.add("user_id", mPref.getId());
            params.add("otp", otpBox.getText().toString());

            httpClient.post(Config.OTP_VERIFICATION, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    SmsRadar.stopSmsRadarService(VerifyActivity.this);
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("otpVerifySucsess", s);
                        JSONObject object = new JSONObject(s);
                        int i;
                        if (object.getInt("statuscode") == 200) {
                            mPref.setMobilVerified("true");
                            mPref.setUserMobileNumber(number);
                            Log.e("numVerified", mPref.getMobilVerified());
                            waitingTimer.cancel();
                            Toast.makeText(VerifyActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent success = new Intent(VerifyActivity.this, VerificationSuccessActivity.class);
                            startActivity(success);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("otpFailure", s);
                        otpBox.setTextColor(Color.parseColor("#dd4b39"));
                        otpdoesntmatch.setVisibility(View.VISIBLE);
                        otpdoesntmatch.setText("Verification code doesn’t match");
                        //Toast.makeText(VerifyActivity.this, "OTP mismatch", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

/*    private void setUpUi(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(VerifyActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setUpUi(innerView);
            }
        }
    }*/

    private boolean checkForAllValidation() {

        if (otpBox.getText().toString().trim().isEmpty() || otpBox.getText().length() < 6) {
            otpdoesntmatch.setVisibility(View.VISIBLE);
            otpdoesntmatch.setText("Enter 6 digit OTP");
            return false;
        } else {
            otpdoesntmatch.setVisibility(View.GONE);
            return true;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        // timerstart();
    }

}
