package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

public class PaymentFailureActivity extends BaseActivity {

    RelativeLayout transactionFailureLayout, transactionCompletedLayout;
    TextView teamCreated, allSet, teamNotCreated, allNotSet;
    private String getGameId;
    private String rawdate;
    private PreferenceManager mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_game_success);
        mPref = PreferenceManager.instance(getApplicationContext());
        getGameId = mPref.getGameId();
        rawdate = mPref.getRawDate();
        Log.e("POST_PAYMENT", "FAILURE");

        transactionCompletedLayout = (RelativeLayout) findViewById(R.id.transactionCompletedLayout);
        transactionFailureLayout = (RelativeLayout) findViewById(R.id.transactionFailureLayout);
        teamCreated = (TextView) findViewById(R.id.team_created_text);
        allSet = (TextView) findViewById(R.id.allSet);
        teamNotCreated = (TextView) findViewById(R.id.team_not_created_text);
        allNotSet = (TextView) findViewById(R.id.allNotSet);

        teamCreated.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        allSet.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        teamNotCreated.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        allNotSet.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        transactionCompletedLayout.setVisibility(View.GONE);
        transactionFailureLayout.setVisibility(View.GONE);
        transactionFailure();
        transactionCompletedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHomeScreen();
            }
        });
        transactionFailureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHomeScreen();
            }
        });
    }


    void goToHomeScreen() {
        Intent toHomeScreen = new Intent(PaymentFailureActivity.this, HomeScreenActivity.class);
        startActivity(toHomeScreen);
        finishAffinity();
    }


    private void transactionFailure() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(PaymentFailureActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            try {
                JSONObject object = new JSONObject();
                object.put("game_id", getGameId);
                object.put("date", rawdate);
                object.put("transaction_id", "");
//                object.put("slot", selectedSlotsIds);

                Log.e("JSONOBJ", object.toString());
                StringEntity entity = new StringEntity(object.toString());

                httpClient.post(PaymentFailureActivity.this, Config.RAZORPAY_FAILURE, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.v("RAZORFAILURE", s);
                        try {
                            JSONObject object = new JSONObject(s);
                            transactionCompletedLayout.setVisibility(View.GONE);
                            transactionFailureLayout.setVisibility(View.VISIBLE);
                            if (object.getInt("statusCode") != 200) {
                                Toast.makeText(getApplicationContext(), "Transaction Error", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        String s = new String(responseBody);
                        Log.v("failure", s);

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }
    }

}
