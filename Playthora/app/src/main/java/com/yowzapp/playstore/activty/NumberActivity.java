package com.yowzapp.playstore.activty;

import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 10/11/16.
 */
public class NumberActivity extends BaseActivity {
    Toolbar mToolbar;
    TextInputLayout phoneNumberBoxInputLayout;
    EditText phoneNumberBox;
    ImageView mImageView;
    TextView mTitle;
    PreferenceManager mPref;
    private boolean numberEditing = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_num_layout);
        mPref = PreferenceManager.instance(getApplicationContext());

        mToolbar = (Toolbar) findViewById(R.id.toolbar_phone_num);
        phoneNumberBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_phone);
        phoneNumberBox = (EditText) findViewById(R.id.phone);
        mImageView = (ImageView) findViewById(R.id.profile_setup_circle_image);
        mTitle = (TextView) findViewById(R.id.tv_phone);
        setUpUI(findViewById(R.id.numberActivity));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        if (getIntent().hasExtra("numberEditing")) {
            numberEditing = getIntent().getBooleanExtra("numberEditing", false);
        }

        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkVerification()) {
                    mobileVerify();
                }
            }
        });
    }

    private void setUpUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(NumberActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                setUpUI(innerView);
            }
        }
    }


    private void mobileVerify() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(NumberActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            RequestParams params = new RequestParams();
            params.add("mobile", phoneNumberBox.getText().toString());
            params.add("user_id", mPref.getId());

            httpClient.post(Config.MOBILE_VEROFICATION, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("mobileSuccess", s);

                    try {
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {

                            Intent intent = new Intent(NumberActivity.this, VerifyNumberActivity.class);
                            intent.putExtra("Number", phoneNumberBox.getText().toString());
                            if (numberEditing)
                                intent.putExtra("numberEditing", true);
                            startActivity(intent);
                            finish();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    if(responseBody!=null && responseBody.length!=0) {
                        String s = new String(responseBody);
                        Log.e("mobileFail", s);
                        try {
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(NumberActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(NumberActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

    private boolean checkVerification() {
        if (phoneNumberBox.getText().toString().isEmpty() || phoneNumberBox.getText().toString().contains(" ") || phoneNumberBox.getText().length() < 10) {
            phoneNumberBoxInputLayout.setError("Enter valid mobile number");
            return false;
        } else {
            phoneNumberBoxInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            Log.i("activityStatus", "This is last activity in the stack");
            Intent login = new Intent(NumberActivity.this, SignupLoginActivity.class);
            startActivity(login);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
