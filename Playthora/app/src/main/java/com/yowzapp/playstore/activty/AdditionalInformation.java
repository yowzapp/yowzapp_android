package com.yowzapp.playstore.activty;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.io.IOException;
import java.io.InputStream;

public class AdditionalInformation extends BaseActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_information);
        webView = (WebView) findViewById(R.id.info_webview);
        PlaythoraUtility.hideProgressDialog();

        InputStream is = null;
        try {
            if (getIntent().getExtras().getString("title").equals("about"))
                is = getAssets().open("html/about.html");
            else if (getIntent().getExtras().getString("title").equals("privacy"))
                is = getAssets().open("html/privacy.html");
            else if (getIntent().getExtras().getString("title").equals("cancellation"))
                is = getAssets().open("html/cancellation.html");
            else
                is = getAssets().open("html/terms.html");
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String str = new String(buffer);
            webView.loadDataWithBaseURL("file:android_asset/html/", str, "text/html", "utf-8", null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
