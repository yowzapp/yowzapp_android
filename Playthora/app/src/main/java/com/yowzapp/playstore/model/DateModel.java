package com.yowzapp.playstore.model;

/**
 * Created by pramod on 23/11/16.
 */

public class DateModel {
    private String date;

    public DateModel(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
