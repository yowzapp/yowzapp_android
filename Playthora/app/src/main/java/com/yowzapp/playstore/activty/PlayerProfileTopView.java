package com.yowzapp.playstore.activty;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.model.PlayerDetailSuccessTopSports;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by nakul on 17/11/16.
 */
public class PlayerProfileTopView extends Fragment {

    TextView gameName, gameNameSecond, gameNameThird, gameLevel, gameLevelSecond, gameLevelThird, gameSub, gameSubSecond, gameSubThird;
    List<PlayerDetailSuccessTopSports> topSportses;
    String topSports;
    Gson gson;
    ArrayList arrayList;
    PlayerDetailSuccessTopSports detailSuccessTopSports;
    ImageView firstImage, secondImage, thirdImage;
    TextView noSports;
    RelativeLayout fLayout, sLayout, tLayout;

//    public PlayerProfileTopView(String s) {
//        topSports = s;
//        gson = new Gson();
//        Log.e("TOPSPORTS", s);
//    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        topSports = getArguments().getString("topSports");
        gson = new Gson();
        Log.e("TOPSPORTS", topSports);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.player_profile_top_games, container, false);

        gameName = (TextView) contentView.findViewById(R.id.game_name);
        gameLevel = (TextView) contentView.findViewById(R.id.game_level);
        gameSub = (TextView) contentView.findViewById(R.id.game_sub);
        gameNameSecond = (TextView) contentView.findViewById(R.id.game_name_second);
        gameLevelSecond = (TextView) contentView.findViewById(R.id.game_level_second);
        gameSubSecond = (TextView) contentView.findViewById(R.id.game_sub_second);
        gameNameThird = (TextView) contentView.findViewById(R.id.game_name_three);
        gameLevelThird = (TextView) contentView.findViewById(R.id.game_level_three);
        gameSubThird = (TextView) contentView.findViewById(R.id.game_sub_three);
        firstImage = (ImageView) contentView.findViewById(R.id.imageView_game);
        secondImage = (ImageView) contentView.findViewById(R.id.imageView_game_second);
        thirdImage = (ImageView) contentView.findViewById(R.id.imageView_game_three);
        noSports = (TextView) contentView.findViewById(R.id.nSports);
        fLayout = (RelativeLayout) contentView.findViewById(R.id.first);
        sLayout = (RelativeLayout) contentView.findViewById(R.id.second);
        tLayout = (RelativeLayout) contentView.findViewById(R.id.third_lay);

        gameName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
        gameNameSecond.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
        gameNameThird.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
        gameLevel.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        gameLevelSecond.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        gameLevelThird.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        gameSub.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
        gameSubSecond.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
        gameSubThird.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
        noSports.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

        try {
            if (!topSports.isEmpty()) {
                JSONArray jsonArray = new JSONArray(topSports);
                topSportses = new ArrayList<PlayerDetailSuccessTopSports>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    topSportses.add(gson.fromJson(jsonArray.get(i).toString(), PlayerDetailSuccessTopSports.class));
                    Log.e("name", topSportses.get(i).getName());
                    Log.e("id", String.valueOf(topSportses.get(i).getId()));
                    Log.e("cover", String.valueOf(topSportses.get(i).getThumbnail()));
                    noSports.setVisibility(View.GONE);


                }
                try {

                    if (topSportses.size() == 1) {

                        gameName.setText(topSportses.get(0).getName());
                        gameSub.setText("Novice");
                        gameLevel.setText("Level");
                        gameLevel.setVisibility(View.GONE);
                        try{
                            Glide.with(getActivity()).load(topSportses.get(0).getThumbnail()).error(R.drawable.game_null).into(firstImage);
                        } catch (Exception e){
                            firstImage.setImageResource(R.drawable.game_null);
                        }

                    }
                    if (topSportses.size() == 2) {

                        gameName.setText(topSportses.get(0).getName());
                        gameSub.setText("Novice");
                        gameLevel.setText("Level");
                        gameLevel.setVisibility(View.GONE);
                        try {
                            Glide.with(getActivity()).load(topSportses.get(0).getThumbnail()).error(R.drawable.game_null).into(firstImage);
                        } catch (Exception e){
                            firstImage.setImageResource(R.drawable.game_null);
                        }

                        gameNameSecond.setText(topSportses.get(1).getName());
                        gameSubSecond.setText("Novice");
                        gameLevelSecond.setText("Level");
                        gameLevelSecond.setVisibility(View.GONE);
                        try {
                            Glide.with(getActivity()).load(topSportses.get(1).getThumbnail()).error(R.drawable.game_null).into(secondImage);
                        } catch (Exception e){
                            secondImage.setImageResource(R.drawable.game_null);
                        }

                    }

                    if (topSportses.size() == 3) {

                        gameName.setText(topSportses.get(0).getName());
                        gameSub.setText("Novice");
                        gameLevel.setText("Level");
                        gameLevel.setVisibility(View.GONE);
                        try {
                            Glide.with(getActivity()).load(topSportses.get(0).getThumbnail()).error(R.drawable.game_null).into(firstImage);
                        } catch (Exception e){
                            firstImage.setImageResource(R.drawable.game_null);
                        }

                        gameNameSecond.setText(topSportses.get(1).getName());
                        gameSubSecond.setText("Novice");
                        gameLevelSecond.setText("Level");
                        gameLevelSecond.setVisibility(View.GONE);
                        try {
                            Glide.with(getActivity()).load(topSportses.get(1).getThumbnail()).error(R.drawable.game_null).into(secondImage);
                        } catch (Exception e){
                            secondImage.setImageResource(R.drawable.game_null);
                        }

                        gameNameThird.setText(topSportses.get(2).getName());
                        gameSubThird.setText("Novice");
                        gameLevelThird.setText("Level");
                        gameLevelThird.setVisibility(View.GONE);
                        try {
                            Glide.with(getActivity()).load(topSportses.get(2).getThumbnail()).error(R.drawable.game_null).into(thirdImage);
                        } catch (Exception e){
                            thirdImage.setImageResource(R.drawable.game_null);
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();


                }
            } else {
                noSports.setVisibility(View.VISIBLE);
                noSports.setText("Top sports list is empty");
            }

        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Error fetching Top sports", Toast.LENGTH_SHORT).show();
            noSports.setVisibility(View.VISIBLE);
            noSports.setText("Top sports list is empty");
        }

        return contentView;
    }
}