package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

/**
 * Created by hemanth on 15/2/17.
 */
public class WebViewActivity extends BaseActivity {

    public WebView webView;
    public WebViewClient client;
    String url, titleName;
    TextView title;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        toolbar = (Toolbar) findViewById(R.id.webToolbar);
        title = (TextView) findViewById(R.id.web_title);
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        webView = (WebView) findViewById(R.id.webView);

        //Enable javascript to load web page
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;

            }

        });

        Intent intent = getIntent();
        url = intent.getStringExtra("URL");
        titleName = intent.getStringExtra("title");
        Log.e("URL", url);
        Log.e("title", titleName);

        if (titleName.equalsIgnoreCase("terms")) {
            title.setText("Terms and conditions");
        }

        if (titleName.equalsIgnoreCase("privacy")) {
            title.setText("Privacy policy");
        }

        if (titleName.equalsIgnoreCase("about")) {
            title.setText("About us");
        }


        webView.setWebViewClient(client);
        webView.setVerticalScrollBarEnabled(false);
        webView.loadUrl(url);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
