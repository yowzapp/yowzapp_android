package com.yowzapp.playstore.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.LoginActivity;
import com.yowzapp.playstore.activty.PlayerProfileViewActivity;
import com.yowzapp.playstore.activty.RewardViewHolder;
import com.yowzapp.playstore.model.MyProfileLeaderBoardModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by pramod on 15/11/16.
 */

public class MyProfileLeaderBoardEveryOne extends Fragment {

    public static RecyclerViewAdapter adapter;
    public static ArrayList<MyProfileLeaderBoardModel> myProfileEveryOne;
    public RelativeLayout previousButton, nextButton;
    public RelativeLayout rankViewLayout;
    Gson mGson;
    ProgressDialog dialog;
    String responseString = "", responseString2 = "", responseString1 = "";
    TextView nextText, PreviousText, rankText, playerText, pointsText, levelText;
    ImageView whichSportsLogo;
    String responseStrings, sportId;
    PreferenceManager mPref;
    LeaderBoardModel models;
    int page = 1, count = 0, ITEMS_PER_PAGE = 10;
    ArrayList<MyProfileLeaderBoardModel> templist, newList, previousList;
    ProgressBar progressBar;
    TextView emptyText;
    int rank;
    private RecyclerView recyclerView;
    View contentView;

    public MyProfileLeaderBoardEveryOne() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        contentView = inflater.inflate(R.layout.my_profile_leader_board_every_one, container, false);

        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.my_profile_leader_board_every_one);
        previousButton = (RelativeLayout) contentView.findViewById(R.id.previous);
        nextButton = (RelativeLayout) contentView.findViewById(R.id.next);
        nextText = (TextView) contentView.findViewById(R.id.nextText);
        PreviousText = (TextView) contentView.findViewById(R.id.previousText);
        emptyText = (TextView) contentView.findViewById(R.id.leader_board_empty);
        rankViewLayout = (RelativeLayout) contentView.findViewById(R.id.rankView);
        progressBar = (ProgressBar) contentView.findViewById(R.id.leader_board_progressbar);

        rankText = (TextView) contentView.findViewById(R.id.rank);
        playerText = (TextView) contentView.findViewById(R.id.personName);
        pointsText = (TextView) contentView.findViewById(R.id.points);
        levelText = (TextView) contentView.findViewById(R.id.level);
        whichSportsLogo = (ImageView) contentView.findViewById(R.id.sportsLogo);

        rankText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        playerText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        pointsText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        levelText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        nextText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        PreviousText.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));

        sportId = "all";
        loadLeaderBoard(page, sportId);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previousButton.setVisibility(View.VISIBLE);
                nextButton.setClickable(false);
                previousButton.setClickable(false);

                if (rank != 0)
                    rankViewLayout.setVisibility(View.VISIBLE);
                if (models.isHasMorePages() && models.getCurrentPage() == page) {
                    page++;
                    loadLeaderBoard(page, sportId);
                } else {
                    previousList = new ArrayList<>();
                    int lastItem = (count + 1) * ITEMS_PER_PAGE + 9;
                    int startItem = (count + 1) * ITEMS_PER_PAGE;
                    try {
                        for (int i = startItem; i <= lastItem; i++) {
                            previousList.add(templist.get(i));
                            Log.e("IIIIIII", "  " + i);
                            if (rank == templist.get(i).getRank()) {
                                rankViewLayout.setVisibility(View.INVISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        nextButton.setVisibility(View.INVISIBLE);
                    }
                    adapter.refresh(previousList);
                    page++;
                    nextButton.setClickable(true);
                    previousButton.setClickable(true);
                }
                count++;
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("COUNT", ":" + count);
                nextButton.setVisibility(View.VISIBLE);
                nextButton.setClickable(false);
                previousButton.setClickable(false);
//                previousButton.setVisibility(View.INVISIBLE);
                count--;
                page--;
                if (page == 1)
                    previousButton.setVisibility(View.INVISIBLE);
                if (rank != 0)
                    rankViewLayout.setVisibility(View.VISIBLE);
                previousList = new ArrayList<>();
                int startItem = count * ITEMS_PER_PAGE;
                int lastItem = (count * ITEMS_PER_PAGE) + 9;
                for (int i = startItem; i <= lastItem; i++) {
                    previousList.add(templist.get(i));
                    Log.e("IIIIIII", "  " + i);
                    if (rank == templist.get(i).getRank()) {
                        rankViewLayout.setVisibility(View.INVISIBLE);
                    }
                }
                adapter.refresh(previousList);
                nextButton.setClickable(true);
                previousButton.setClickable(true);
            }
        });
        return contentView;
    }

    public void filterSports(int sportId) {
        if(previousButton==null)
            previousButton = (RelativeLayout) getView().findViewById(R.id.previous);
        if(nextButton==null)
            nextButton = (RelativeLayout) getView().findViewById(R.id.next);
        previousButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        rankViewLayout.setVisibility(View.INVISIBLE);
        page = 1;
        count = 0;
        String sport = sportId == 999 ? "all" : String.valueOf(sportId);
        loadLeaderBoard(page, sport);
    }

    private void loadLeaderBoard(int page, String sportId) {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                nextButton.setClickable(false);
                previousButton.setClickable(false);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("accessToken", mPref.getAccessToken());
                Log.e("URL_LEADERBOARD", Config.LEADER_BOARD + "?all=true" + "&sport_id=" + sportId + "" + "&page=" + page);
                mHttpClient.get(Config.LEADER_BOARD + "?all=true" + "&sport_id=" + sportId + "" + "&page=" + page,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                emptyText.setText("");
                                progressBar.setVisibility(View.GONE);
                                Log.e("URL", Config.LEADER_BOARD);
                                try {
                                    String s = new String(responseBody);
                                    Log.d("RESPONSEEVERYONE", s);
                                    models = new LeaderBoardModel();
                                    mGson = new Gson();
                                    models = mGson.fromJson(s, LeaderBoardModel.class);

                                    if (models.getSuccess().size() != 0) {
                                        if (models.isHasMorePages()) {
                                            nextButton.setVisibility(View.VISIBLE);
                                        } else nextButton.setVisibility(View.INVISIBLE);

                                        if (models.getCurrentPage() > 1) {
                                            newList = models.getSuccess();
                                            for (int i = 0; i < models.getSuccess().size(); i++) {
                                                templist.add(models.getSuccess().get(i));
                                            }
                                            adapter.refresh(newList);
                                        } else {
                                            templist = models.getSuccess();
                                            adapter = new RecyclerViewAdapter(getActivity(), models.getSuccess());
                                            recyclerView.setAdapter(adapter);// set adapter on recyclerview
                                            adapter.notifyDataSetChanged();
                                        }

                                        if (models.getMine().size() != 0) {
                                            rank = models.getMine().get(0).getRank();
                                            rankViewLayout.setVisibility(View.VISIBLE);
                                            rankText.setText(models.getMine().get(0).getRank() + "");
                                            playerText.setText(models.getMine().get(0).getName());
                                            pointsText.setText(models.getMine().get(0).getPoints() + "");
                                            levelText.setText(models.getMine().get(0).getLevel_id() + "");
                                            try {
                                                Glide.with(getActivity()).load(models.getMine().get(0).getBadge()).into(whichSportsLogo);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else rankViewLayout.setVisibility(View.INVISIBLE);

                                    } else {
                                        emptyText.setText("No players");
                                        recyclerView.setAdapter(null);
                                    }
                                } catch (Exception e) {
                                    progressBar.setVisibility(View.GONE);
                                    e.printStackTrace();
                                } finally {
                                    nextButton.setClickable(true);
                                    previousButton.setClickable(true);
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();

                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                } finally {
                                    nextButton.setClickable(true);
                                    previousButton.setClickable(true);
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        } else {
//            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }


    public class RecyclerViewAdapter extends
            RecyclerView.Adapter<RewardViewHolder> {

        int rank;
        private List<MyProfileLeaderBoardModel> arrayList;
        private Context context;

        public RecyclerViewAdapter(Context context,
                                   List<MyProfileLeaderBoardModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public RewardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.my_profile_leader_board_items, viewGroup, false);
            RewardViewHolder listHolder = new RewardViewHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final RewardViewHolder holder, final int position) {
            final MyProfileLeaderBoardModel model = arrayList.get(position);


            final RewardViewHolder mainHolder = (RewardViewHolder) holder;

            try {
                mainHolder.rank.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.personName.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.points.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.level.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.rank.setText(model.getRank() + "");
                mainHolder.personName.setText(model.getName());
                mainHolder.points.setText(model.getPoints() + "");
                mainHolder.level.setText(model.getLevel_id() + "");
                if (model.getUser_id() == Integer.parseInt(mPref.getId())) {
                    mainHolder.lastItem.setBackgroundColor(getResources().getColor(R.color.divider));
                    rank = model.getRank();
                } else {
                    mainHolder.lastItem.setBackgroundColor(getResources().getColor(R.color.white));
                    if (model.getUser_id() == Integer.parseInt(mPref.getId())) {
                    }
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                Glide.with(context).load(model.getBadge()).into(mainHolder.sportsLogo);
                Log.e("BADGE","ALL:"+model.getBadge());
            } catch (Exception e) {
                e.printStackTrace();
            }


            mainHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, PlayerProfileViewActivity.class);
                    i.putExtra("Id", model.getUser_id() + "");
                    startActivity(i);
                }
            });

            mainHolder.itemView.setTag(model);


        }

        public void refresh(List<MyProfileLeaderBoardModel> arrayList) {
            this.arrayList = arrayList;
            notifyDataSetChanged();
            Log.d("userFeedddd11", responseString);

        }
    }


}
