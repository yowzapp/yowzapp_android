package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.fragment.AddMembersFollowing;
import com.yowzapp.playstore.fragment.AddMembersFree;
import com.yowzapp.playstore.fragment.AddMembersNewFollowers;
import com.yowzapp.playstore.model.AddTeamMembersList;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import java.util.ArrayList;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 14/11/16.
 */

public class AddMembersActivity extends BaseActivity implements View.OnClickListener {


    public static MyPagerAdapter myPagerAdapter;
    public static ArrayList<AddTeamMembersList> stringList;
    public ViewPager viewPager;
    TextView following, followingSelected, followers, followersSelected, freeAgents, freeAgentsSelected, invite;
    Toolbar toolbar;
    TextView toolbarDone;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_members_main_layout);


        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        intializeAllComponents();
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(AddMembersActivity.this, Config.RALEWAY_REGULAR));
        getSupportActionBar().setElevation(0);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("SSSSSSSSSS", stringList.size() + "");
                if (stringList.size() >= 1) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("mylist", stringList);
                    intent.putExtras(bundle);
                    setResult(3, intent);
                    finish();
                } else
                    Toast.makeText(AddMembersActivity.this, "You should add at least one member", Toast.LENGTH_SHORT).show();
            }
        });

        stringList = new ArrayList<>();
        if (getIntent().hasExtra("TEMPLIST")) {
            if (getIntent().getStringExtra("TEMPLIST").equalsIgnoreCase("CREATE")) {
                Bundle bundle = getIntent().getExtras();
                stringList = bundle.getParcelableArrayList("mylist");
                // stringList = CreateTeamActivity.addMembersArrayList;
                Log.e("STRINGLIST", stringList.size() + "");
            } else {
                Bundle bundle = getIntent().getExtras();
                stringList = bundle.getParcelableArrayList("mylist");
                Log.e("STRINGLIST", stringList.size() + "");
            }
        }
        Log.e("STRINGLIST", stringList.size() + "");

        following.setOnClickListener(this);
        followers.setOnClickListener(this);
        freeAgents.setOnClickListener(this);
        invite.setOnClickListener(this);

        viewPager = (ViewPager) findViewById(R.id.customFragment);
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(myPagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        membersFollowing();
                        if (AddMembersFollowing.adapter != null)
                            AddMembersFollowing.adapter.Refresh(stringList);
                        break;
                    case 1:
                        membersFollowers();
                        if (AddMembersNewFollowers.adapter != null)
                            AddMembersNewFollowers.adapter.Refresh(stringList);
                        break;
                    case 2:
                        membersFreeAgents();
                        //AddMembersFollowing.reloadData();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void membersFollowing() {
        followingSelected.setVisibility(View.VISIBLE);
        following.setVisibility(View.GONE);
        followersSelected.setVisibility(View.GONE);
        followers.setVisibility(View.VISIBLE);
        freeAgentsSelected.setVisibility(View.GONE);
        freeAgents.setVisibility(View.VISIBLE);
    }

    private void membersFollowers() {
        followingSelected.setVisibility(View.GONE);
        following.setVisibility(View.VISIBLE);
        followersSelected.setVisibility(View.VISIBLE);
        followers.setVisibility(View.GONE);
        freeAgents.setVisibility(View.VISIBLE);
        freeAgentsSelected.setVisibility(View.GONE);
    }

    private void membersFreeAgents() {
        freeAgentsSelected.setVisibility(View.VISIBLE);
        freeAgents.setVisibility(View.GONE);
        followersSelected.setVisibility(View.GONE);
        followers.setVisibility(View.VISIBLE);
        followingSelected.setVisibility(View.GONE);
        following.setVisibility(View.VISIBLE);
    }

    private void intializeAllComponents() {
        toolbar = (Toolbar) findViewById(R.id.team_member_toolbar);
        toolbarDone = (TextView) findViewById(R.id.toolbar_done);
        following = (TextView) findViewById(R.id.following);
        followingSelected = (TextView) findViewById(R.id.followingSelected);
        followers = (TextView) findViewById(R.id.followers);
        followersSelected = (TextView) findViewById(R.id.followersSelected);
        freeAgents = (TextView) findViewById(R.id.freeAgents);
        freeAgentsSelected = (TextView) findViewById(R.id.freeAgentsSelected);
        invite = (TextView) findViewById(R.id.invitePlaythora);
        toolbarDone.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        invite.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), Config.RALEWAY_REGULAR));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.following:
                membersFollowing();
                viewPager.setCurrentItem(0);
                if (AddMembersFollowing.adapter != null)
                    AddMembersFollowing.adapter.Refresh(stringList);
                break;

            case R.id.followers:
                membersFollowers();
                viewPager.setCurrentItem(1);
                if (AddMembersNewFollowers.adapter != null)
                    AddMembersNewFollowers.adapter.Refresh(stringList);
                break;

            case R.id.freeAgents:
                membersFreeAgents();
                viewPager.setCurrentItem(2);
                //AddMembersFollowing.reloadData();
                break;

            case R.id.invitePlaythora:
                Intent invi = new Intent(AddMembersActivity.this, InviteFriendsActivity.class);
                startActivity(invi);

            default:
                break;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_memebers_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_openSearch) {

            Intent i = new Intent(AddMembersActivity.this, SearchPlayerActivity.class);
            /*Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("mylist", stringList);
            i.putExtras(bundle);*/
            startActivityForResult(i, 5);

        }

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_user_selected) {
//
//            Log.e("SSSSSSSSSS", stringList.size() + "");
//            if (stringList.size() >= 1) {
//                Intent intent = new Intent();
//                Bundle bundle = new Bundle();
//                bundle.putParcelableArrayList("mylist", stringList);
//                intent.putExtras(bundle);
//                setResult(3, intent);
//                finish();
//            } else
//                Toast.makeText(AddMembersActivity.this, "You should add at least one member", Toast.LENGTH_SHORT).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode", requestCode + "");
        if (requestCode == 5) {
            Log.e("requestCode", requestCode + "");
            if (AddMembersFollowing.adapter != null)
                AddMembersFollowing.adapter.Refresh(stringList);
            if (AddMembersNewFollowers.adapter != null)
                AddMembersNewFollowers.adapter.Refresh(stringList);

            if (AddMembersFree.adapter != null)
                AddMembersFree.adapter.Refresh(stringList);
        }

    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = {"Following", "Followers", "Free Agents"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new AddMembersFollowing(0);

                case 1:
                    return new AddMembersNewFollowers(1);

                case 2:
                    return new AddMembersFree(2);

                default:
                    return new AddMembersFollowing(0);

            }

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        setResult(3, intent);
        finish();
    }
}
