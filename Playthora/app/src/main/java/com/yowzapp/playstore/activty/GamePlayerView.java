package com.yowzapp.playstore.activty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by pramod on 25/11/16.
 */

public class GamePlayerView extends BaseActivity {

    public ArrayList<TeamMembersModel> teamMembersArrayList;
    public AddMembersFollowingAdapter adapter;
    RecyclerView recyclerView;
    String response;
    ProgressDialog dialog;
    Gson mGson;
    TextView invite;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_player_admin_recycler);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Players");
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(GamePlayerView.this, RALEWAY_REGULAR));

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }


        recyclerView = (RecyclerView) findViewById(R.id.game_admin_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(GamePlayerView.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);


        try {
            followingRecycler();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void followingRecycler() throws Exception {

        Thread thread = new Thread(new Runnable() {

            public void run() {

                try {
                    response = "[{\n" +
                            "    \"name\": \"Hemanth\",\n" +
                            "    \"id\": 1,\n" +
                            "    \"image\": \"https://upload.wikimedia.org/wikipedia/commons/f/fc/Nemer_Saade_Profile_Picture.jpg\"\n" +
                            "}, {\n" +
                            "    \"name\": \"Nakul\",\n" +
                            "    \"id\": 2,\n" +
                            "    \"image\": \"http://youthkinect.com/static/images/users/4-1475520033.jpg\"\n" +
                            "}, {\n" +
                            "    \"name\": \"Pramod\",\n" +
                            "    \"id\": 3,\n" +
                            "    \"image\": \"http://media.supercheapauto.com.au/sports/images/zooms/415405-zoom.jpg\"\n" +
                            "}, {\n" +
                            "    \"name\": \"Punith\",\n" +
                            "    \"id\": 4,\n" +
                            "    \"image\": \"https://s-media-cache-ak0.pinimg.com/236x/40/16/5a/40165a87e68b083cf7dcc138a511ffda.jpg\"\n" +
                            "}, {\n" +
                            "    \"name\": \"Bharath\",\n" +
                            "    \"id\": 5,\n" +
                            "    \"image\": \"http://youthkinect.com/static/images/users/4-1475520033.jpg\"\n" +
                            "}, {\n" +
                            "    \"name\": \"Bharath\",\n" +
                            "    \"id\": 6,\n" +
                            "    \"image\": \"ttps://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQRgzfwrP-XqYZVTp3-rIpbpTxxkPtLHsw8GWAkd0Zx-2FrV2y_\"\n" +
                            "}, {\n" +
                            "    \"name\": \"Ruthiwick\",\n" +
                            "    \"id\": 7,\n" +
                            "    \"image\": \"https://www.newschool.edu/uploadedImages/Parsons/Profiles/jamer_hunt_profile.jpg\"\n" +
                            "}]";

                    GamePlayerView.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                try {
                                    if (!GamePlayerView.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                    e.printStackTrace();
                                }

                                JSONArray teamMembersArray = new JSONArray(response);
                                mGson = new Gson();
                                teamMembersArrayList = new ArrayList<TeamMembersModel>();

                                for (int i = 0; i < teamMembersArray.length(); i++) {
                                    teamMembersArrayList.add(mGson.fromJson(teamMembersArray.get(i).toString(), TeamMembersModel.class));

                                }

                                if (!teamMembersArrayList.isEmpty()) {
                                    if (recyclerView.getAdapter() == null) {

                                        adapter = new AddMembersFollowingAdapter(GamePlayerView.this, teamMembersArrayList);
                                        recyclerView.setAdapter(adapter);// set adapter on recyclerview
                                        adapter.notifyDataSetChanged();


                                    }

                                } else {

                                }


                            } catch (JsonParseException e) {

                                try {
                                    if (!GamePlayerView.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception ee) {
                                    ee.printStackTrace();
                                }

                                e.printStackTrace();
                            } catch (JSONException e) {

                                try {
                                    if (!GamePlayerView.this.isFinishing() && dialog != null) {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (Exception eee) {
                                    eee.printStackTrace();
                                }

                                e.printStackTrace();
                            }

                        }
                    });
                } catch (Exception e) {
                }
            }
        });//
        thread.start();
    }

    private void showPopUp(String name) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(GamePlayerView.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.game_make_admin_or_remove, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(GamePlayerView.this);
        alertDialogBuilderUserInput.setView(mView);
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

        final TextView userName = (TextView) mView.findViewById(R.id.userName);
        final TextView makeAdmin = (TextView) mView.findViewById(R.id.makeAdmin);
        final TextView removeAdmin = (TextView) mView.findViewById(R.id.remove);

        userName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        makeAdmin.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        removeAdmin.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        userName.setText(name);

        makeAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });

        removeAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });


        alertDialogAndroid.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_all_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();

        }

        if (id == R.id.action_openSearch) {
            Intent toSearchActivity = new Intent(GamePlayerView.this, SearchActivity.class);
            startActivity(toSearchActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    public class AddMembersFollowingAdapter extends RecyclerView.Adapter<AddMembersFollowingHolder> {

        private ArrayList<TeamMembersModel> arrayList;
        private Context context;

        public AddMembersFollowingAdapter(Context context, ArrayList<TeamMembersModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;

        }


        @Override
        public AddMembersFollowingHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.game_player_items, viewGroup, false);
            AddMembersFollowingHolder listHolder = new AddMembersFollowingHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final AddMembersFollowingHolder holder, final int position) {
            final TeamMembersModel model = arrayList.get(position);


            final AddMembersFollowingHolder mainHolder = (AddMembersFollowingHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(GamePlayerView.this, RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());
                if (position == 0) {
                    mainHolder.name.setText("You");
                }


            } catch (NullPointerException e) {

            }


            try {
                Glide.with(context).load(model.getImage()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mainHolder.itemView.setTag(model);


        }
    }

    public class AddMembersFollowingHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView name;
        RelativeLayout playerLayout;

        public AddMembersFollowingHolder(View view) {
            super(view);
            this.userImage = (CircleImageView) view.findViewById(R.id.playerImageView);
            this.name = (TextView) view.findViewById(R.id.userName);
            this.playerLayout = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);

        }
    }
}
