package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import eu.fiskur.chipcloud.ChipCloud;
import eu.fiskur.chipcloud.ChipListener;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 27/7/16.
 */
public class InterestsActivity extends BaseActivity {
    Toolbar toolbar;
    ImageView interestImage;
    TextView mTitle, mText;
    PreferenceManager mPref;
    int id;
    String name, thumbNail;
    ArrayList<String> list = new ArrayList<String>();
    String[] stringArray, stringId;
    StringEntity entity;
    String interest_update, location_update, userName;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interests_layout);
        mPref = PreferenceManager.instance(getApplicationContext());
        gson = new Gson();

        mTitle = (TextView) findViewById(R.id.interests_username);
        mText = (TextView) findViewById(R.id.chooe_intrest_txt);
        toolbar = (Toolbar) findViewById(R.id.toolbar_interests);

        interestList();

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        Intent intent = getIntent();
        userName = intent.getStringExtra("name");

        Log.e("userName", userName);

        mTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        mText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        interestImage = (ImageView) findViewById(R.id.interest_circle_image);

        interestImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (list.size() != 0) {
                    // interestUpdate();
                    interests();
                } else {
                    Toast.makeText(InterestsActivity.this, "Please select interests", Toast.LENGTH_SHORT).show();
                }/*else {
                    Intent location = new Intent(InterestsActivity.this, LocationActivity.class);
                    location.putExtra("name", userName);
                    startActivity(location);
                    finish();
                }*/


                Log.e("list", String.valueOf(list));

            }
        });

    }

    private void interests() {
      /*   try {
             JSONArray jsonArray = new JSONArray();
             for(int i=0;i<list.size();i++) {
                 JSONObject object = new JSONObject();
                 Log.e("id", list.get(i));
                 object.put("id", list.get(i));

                 jsonArray.put(object);

                 JSONObject jsonObject = new JSONObject();
                 jsonObject.put("interests", jsonArray);

                 Log.e("jsonObject", jsonObject.toString());

                 entity = new StringEntity(jsonObject.toString());
                 Log.e("entity", String.valueOf(entity));
             }


        }catch (JSONException e){
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        String jsonText = gson.toJson(list);

        mPref.setInterestList(jsonText);
        mPref.setInterestUpdated("true");
        Intent location = new Intent(InterestsActivity.this, LocationActivity.class);
        Log.e("interestList", String.valueOf(list));
        location.putStringArrayListExtra("interest", list);
        location.putExtra("name", userName);
        startActivity(location);
        finish();

    }

    private void interestList() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(InterestsActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            httpClient.get(Config.INTEREST_LIST, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("intrestList", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {
                            final JSONArray jsonArray = object.getJSONArray("success");

                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject childJSONObject = jsonArray.getJSONObject(j);
                                name = childJSONObject.getString("name");
                                id = childJSONObject.getInt("id");
                                thumbNail = childJSONObject.getString("thumbnail");

                                stringArray = new String[jsonArray.length()];
                                stringId = new String[jsonArray.length()];
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    stringId[i] = jsonArray.getJSONObject(i).getString("id");
                                    stringArray[i] = jsonArray.getJSONObject(i).getString("name");

                                }
                            }


                            ChipCloud chipCloud = (ChipCloud) findViewById(R.id.chip_cloud);
                            // chipCloud.setLayoutParams(new LinearLayout.LayoutParams(2500,LinearLayout.LayoutParams.WRAP_CONTENT));
                            //  String[] tags = {"Cricket","Hockey","Football","Handball","Archery","Athletics","Badminton","Boxing","Basketball","Beach Volleyball","Canoe Slalom","Cycling","Tennis","Throwball","Volleyball","Aerobatics","Aerobatics","Karate","Kick Boxing","Rock climbing","Judo","Freestyle wrestling","Billiards","Snooker"};

                            chipCloud.removeAllViews();
                            new ChipCloud.Configure()
                                    .chipCloud(chipCloud)
                                    .selectTransitionMS(500)
                                    .deselectTransitionMS(250)
                                    .labels(stringArray)

                                    .mode(ChipCloud.Mode.MULTI)
                                    .chipListener(new ChipListener() {
                                        @Override
                                        public void chipSelected(int index) {

                                            list.add(stringId[index]);
                                            Log.e("list", String.valueOf(list));

                                        }

                                        @Override
                                        public void chipDeselected(int index) {
                                            list.remove(stringId[index]);
                                            Log.e("list", String.valueOf(list));

                                        }
                                    })
                                    .build();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    }


    private void interestUpdate() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(InterestsActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());


            try {

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < list.size(); i++) {
                    JSONObject object = new JSONObject();
                    Log.e("id", list.get(i));
                    object.put("id", list.get(i));

                    jsonArray.put(object);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("interests", jsonArray);

                    Log.e("jsonObject", jsonObject.toString());

                    entity = new StringEntity(jsonObject.toString());
                    Log.e("entity", String.valueOf(entity));

                }


                httpClient.post(InterestsActivity.this, Config.EDIT_PROFILE, entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("response", s);
                            JSONObject object = new JSONObject(s);
                            if (object.getString("status").equalsIgnoreCase("success")) {
                                object.getJSONObject("success").getBoolean("profile_pic_updated");
                                interest_update = String.valueOf(object.getJSONObject("success").getBoolean("interest_updated"));
                                location_update = String.valueOf(object.getJSONObject("success").getBoolean("location_updated"));
                                mPref.setInterestUpdated(interest_update);
                                mPref.setLocationUpdated(location_update);
                                Toast.makeText(InterestsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                                Intent location = new Intent(InterestsActivity.this, LocationActivity.class);
                                startActivity(location);
                                finish();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {

        }

    }


}

