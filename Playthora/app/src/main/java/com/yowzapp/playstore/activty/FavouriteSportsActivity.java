package com.yowzapp.playstore.activty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.SportsListModel;
import com.yowzapp.playstore.model.TeamDetailSportsModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import eu.fiskur.chipcloud.ChipCloud;
import eu.fiskur.chipcloud.ChipListener;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 17/11/16.
 */

public class FavouriteSportsActivity extends BaseActivity {

    public static ArrayList<TeamDetailSportsModel> sportslist;
    Toolbar toolbar;
    TextView toolBarTitle;
    PreferenceManager mPref;
    SportsListModel sportsListModel;
    Gson gson;
    ChipCloud chipCloud;
    String[] tags;
    ProgressBar progressBar;
    TextView toolbarDone;
    String[] sports;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourite_sports_layout);
        mPref = PreferenceManager.instance(this);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        initializeAllComponents();

        if (getIntent().hasExtra("SPORTS")) {
            sports = getIntent().getStringArrayExtra("SPORTS");
        }

        if (getIntent().hasExtra("SPORTSLIST")) {
            sports = getIntent().getStringArrayExtra("SPORTSLIST");
        }

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(3, intent);
                finish();
            }
        });
        sportslist = new ArrayList<>();
        chipCloud = (ChipCloud) findViewById(R.id.chip_cloud);
        // chipCloud.setLayoutParams(new LinearLayout.LayoutParams(2500,LinearLayout.LayoutParams.WRAP_CONTENT));
        populateSportsTypes();
    }

    private void populateSportsTypes() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                mHttpClient.get(Config.SPORTS_LIST,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);

                                    sportsListModel = new SportsListModel();
                                    gson = new Gson();
                                    sportsListModel = gson.fromJson(s, SportsListModel.class);
                                    Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));

                                    if (sportsListModel.getSuccess().size() != 0) {
                                        Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                                        tags = new String[sportsListModel.getSuccess().size()];
                                        for (int i = 0; i < sportsListModel.getSuccess().size(); i++) {
                                            tags[i] = sportsListModel.getSuccess().get(i).getName();
                                        }
                                        new ChipCloud.Configure()
                                                .chipCloud(chipCloud)
                                                .selectTransitionMS(500)
                                                .deselectTransitionMS(250)
                                                .labels(tags)
//                                                .selectedColor(R.color.endColor)
                                                .mode(ChipCloud.Mode.MULTI)
                                                .chipListener(new ChipListener() {
                                                    @Override
                                                    public void chipSelected(int index) {
                                                        sportslist.add(new TeamDetailSportsModel(tags[index], sportsListModel.getSuccess().get(index).getId()));
                                                    }

                                                    @Override
                                                    public void chipDeselected(int index) {
                                                        if (sportslist.size() != 0) {
                                                            for (int i = 0; i < sportslist.size(); i++) {
                                                                if (tags[index].equalsIgnoreCase(sportslist.get(i).getName())) {
                                                                    sportslist.remove(i);
                                                                }
                                                            }
                                                        }

                                                    }
                                                })
                                                .buildtwo(0, sports);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        }
//        else {
//
//            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
//
//        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.fav_sports_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.fav_sports_selected) {
//            Intent intent = new Intent();
//            setResult(3, intent);
//            finish();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void initializeAllComponents() {

        toolbar = (Toolbar) findViewById(R.id.favouriteSportsToolbar);
        toolBarTitle = (TextView) findViewById(R.id.fav_sports_toolbar_title);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_two);
        toolbarDone = (TextView) findViewById(R.id.toolbar_done);
        toolbarDone.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        toolBarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }


}
