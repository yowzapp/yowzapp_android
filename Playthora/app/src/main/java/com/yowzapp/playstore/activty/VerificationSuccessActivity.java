package com.yowzapp.playstore.activty;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.yowzapp.playstore.R;
import com.yowzapp.playstore.utils.PlaythoraUtility;

import static com.yowzapp.playstore.utils.Config.RALEWAY_BOLD;
import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 7/26/2016.
 */
public class VerificationSuccessActivity extends Activity {

    ImageView successImage;
    TextView getSweatGo, allSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success_layout);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }

        successImage = (ImageView) findViewById(R.id.verificationSuccessImage);
        getSweatGo = (TextView) findViewById(R.id.awesome_text);
        allSet = (TextView) findViewById(R.id.allSet);

        getSweatGo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        allSet.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        successImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                 Intent profile = new Intent(VerificationSuccessActivity.this,ProfileSetUpActivity.class);
                Intent profile = new Intent(VerificationSuccessActivity.this, HomeScreenActivity.class);
                startActivity(profile);
                finishAffinity();

            }
        });


    }
}
