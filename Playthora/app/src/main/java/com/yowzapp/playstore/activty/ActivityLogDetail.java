package com.yowzapp.playstore.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.application.BaseActivity;
import com.yowzapp.playstore.model.CommentList;
import com.yowzapp.playstore.model.CommentModel;
import com.yowzapp.playstore.model.UserData;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.yowzapp.playstore.utils.Config.RALEWAY_REGULAR;
import static com.yowzapp.playstore.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 17/11/16.
 */
public class ActivityLogDetail extends BaseActivity {

    Context context;
    Toolbar toolbar;
    TextView toolbarTitle, noOfLike, noOfCmts, mLevelText, mTypeText, mStatusText;
    RecyclerView commentList;
    CommentModel commentModel;
    CommentAdapter adapter;
    Gson gson;
    Button sendComment;
    EditText comment;
    String commentText, mType, mDataType="", mStatus, mLevel, mImageUrl = "", mUserImage = "", mLikes;
    int mComments, id, position, mDataTypeId;
    RelativeLayout likes, mainRelative, mComment;
    boolean value, likeByUser, keyBoerdValue, hasMorePage;
    ImageView mImage, mLikeButton, mUnlikeButton;
    CircleImageView userImage;
    PreferenceManager mPref;
    List<CommentList> tempList;
    ProgressBar progressBar;
    NestedScrollView nested;
    int pages = 1;
    private LinearLayoutManager mLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_detail_layout);

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#FD9700"));
        }
        mPref = PreferenceManager.instance(this);
        initializeAllComponents();
        setFont();
        commentList.setNestedScrollingEnabled(false);
        mLayoutManager = new LinearLayoutManager(ActivityLogDetail.this, LinearLayoutManager.VERTICAL, false);
        commentList.setHasFixedSize(true);
        commentList.setLayoutManager(mLayoutManager);
        commentList.setFocusable(false);

        //commentList.setExpanded(true);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Activity Log");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                Intent i = getIntent();
                i.putExtra("POSITION", position);
                i.putExtra("ISLIKE", likeByUser);
                i.putExtra("COMMENTS", mComments);
                i.putExtra("LIKESCOUNT", mLikes);
                setResult(5, i);
                finish();
            }
        });

        mUserImage = "";
        Intent intent = getIntent();

        if(intent.hasExtra("DATA_TYPE")){
            mDataType = intent.getStringExtra("DATA_TYPE");
            mDataTypeId = intent.getIntExtra("DATA_TYPE_ID", -1);
        }

        if (intent.hasExtra("LEVEL")) {
            mImage.setVisibility(View.GONE);
            mLevel = intent.getStringExtra("LEVEL");
            mUserImage = intent.getStringExtra("IMAGE");
            try {
                Glide.with(context)
                        .load(mUserImage)
                        .error(R.drawable.circled_user)
                        .centerCrop()
                        .into(userImage);
            } catch (Exception e) {
                userImage.setImageResource(R.drawable.circled_user);
            }
        } else {
            userImage.setVisibility(View.GONE);
            mLevelText.setVisibility(View.GONE);
            mImageUrl = intent.getStringExtra("IMAGE");
            try {
                if (!mImageUrl.isEmpty())
                    Glide.with(context).load(mImageUrl).centerCrop().into(mImage);
                else mImage.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                mImage.setVisibility(View.GONE);
            }
        }
        mLikes = intent.getStringExtra("LIKES");
        mComments = intent.getIntExtra("COMMENTS_COUNT", 0);
        id = intent.getIntExtra("ID", 0);
        position = intent.getIntExtra("POSITION", 0);
        mType = intent.getStringExtra("TYPE");
        mStatus = intent.getStringExtra("STATUS");
        likeByUser = intent.getBooleanExtra("LIKEBYUSER", false);
        keyBoerdValue = intent.getBooleanExtra("KEYBOARD", false);
        hasMorePage = intent.getBooleanExtra("HASMOREPAGE", false);

        if (keyBoerdValue) {
            showKeyBoard();
        }

        if (likeByUser) {
//            mLikeButton.setImageResource(R.drawable.ic_filled_heart);
            mLikeButton.setVisibility(View.INVISIBLE);
            mUnlikeButton.setVisibility(View.VISIBLE);
        } else {
            mLikeButton.setVisibility(View.VISIBLE);
            mUnlikeButton.setVisibility(View.INVISIBLE);
        }

        mComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showKeyBoard();
            }
        });

        mTypeText.setText(mType);
        mStatusText.setText(mStatus);
        mLevelText.setText(mLevel);
        noOfLike.setText(mLikes);
        noOfCmts.setText(mComments + "");

        if (!mPref.getAccessToken().isEmpty())
            LoadComments(pages, false);

        nested.setOnScrollChangeListener(new EndlessParentScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    if (!mPref.getAccessToken().isEmpty()) {
                        if (commentModel.isHasMorePages()) {
                            Log.e("page_DONE", page + "");
                            pages = page;
                            LoadComments(pages, false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        });

        if(!mDataType.equalsIgnoreCase("")) {
            userImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDataType.equalsIgnoreCase("game")) {
                        Intent i = new Intent(ActivityLogDetail.this, GameMain.class);
                        i.putExtra("GAME_ID", String.valueOf(mDataTypeId));
                        startActivity(i);
                    } else {
                        Intent i = new Intent(ActivityLogDetail.this, PlayerProfileViewActivity.class);
                        i.putExtra("Id", String.valueOf(mDataTypeId));
                        i.putExtra("SEARCH", "");
                        startActivity(i);
                    }

                }
            });
        }

        sendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PlaythoraUtility.checkInternetConnection(ActivityLogDetail.this)) {
                    commentText = comment.getText().toString().replaceAll("^\\s+", "").replaceAll("\\s+$", ""); //removed leading and trailing spaces
                    if (!commentText.isEmpty()) {
                        //commentModel.add(new CommentModel(commentText,"http://youthkinect.com/static/images/users/4-1475520033.jpg",i+""));
                        populateComment(commentText);
                        //adapter.notifyDataSetChanged();
                        hideKeyboard();
                    }
                }
//                else
//                    Toast.makeText(ActivityLogDetail.this, "Not connected to internet", Toast.LENGTH_LONG).show();
            }
        });

        likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PlaythoraUtility.checkInternetConnection(ActivityLogDetail.this)) {
                    if (!likeByUser) {
                        likeActivity(id, true);
                       /* final Animation animation = new AlphaAnimation(1, 0);
                        animation.setDuration(1000);
                        animation.setInterpolator(new LinearInterpolator());
                        animation.setRepeatCount(Animation.ABSOLUTE);
                        mLikeButton.startAnimation(animation);*/
                        mLikeButton.setVisibility(View.INVISIBLE);
                        mUnlikeButton.setVisibility(View.VISIBLE);
                        int i = Integer.parseInt(mLikes);
                        i += 1;
                        mLikes = String.valueOf(i);
                        noOfLike.setText(i + "");
                        likeByUser = true;
                    } else {
                        likeActivity(id, false);
                        int i = Integer.parseInt(mLikes);
                        i -= 1;
                        mLikes = String.valueOf(i);
                        noOfLike.setText(i + "");
                        mLikeButton.setVisibility(View.VISIBLE);
                        mUnlikeButton.setVisibility(View.INVISIBLE);
                        likeByUser = false;
                    }
                }
//                else
//                    Toast.makeText(ActivityLogDetail.this, "Not connected to internet", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void LoadComments(int pages, boolean b) {
        if (PlaythoraUtility.checkInternetConnection(ActivityLogDetail.this)) {
            try {
                if (!b)
                    progressBar.setVisibility(View.VISIBLE);

                AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("id", String.valueOf(id));
                Log.e("PARAMMS", String.valueOf(params));
                mHttpClient.post(Config.ACTIVITY_COMMENT_LIST + "?page=" + pages, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject jsonObject = new JSONObject(s);
                                    gson = new Gson();
                                    commentModel = new CommentModel();
                                    commentModel = gson.fromJson(s, CommentModel.class);
                                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                                        if (commentModel.getCurrentPage() > 1) {
                                            for (int i = 0; i < commentModel.getSuccess().getData().size(); i++) {
                                                tempList.add(commentModel.getSuccess().getData().get(i));
                                            }
                                            for (int i = 0; i < tempList.size(); i++) {
                                                Log.e("TEMPLLISTADAPTER", tempList.get(i).getContent());
                                            }
                                            adapter.Refresh(tempList);
                                        } else {
                                            tempList = commentModel.getSuccess().getData();
                                            for (int i = 0; i < tempList.size(); i++) {
                                                Log.e("TEMPLLIST", tempList.get(i).getContent());
                                            }
                                            adapter = new CommentAdapter(getApplicationContext(), commentModel.getSuccess().getData());
                                            commentList.setAdapter(adapter);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(ActivityLogDetail.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(ActivityLogDetail.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();
                                    } else
                                        Toast.makeText(ActivityLogDetail.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(ActivityLogDetail.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        }
//        else
//            Toast.makeText(ActivityLogDetail.this, "Not connected to internet", Toast.LENGTH_LONG).show();


    }

    private void populateComment(final String commentText) {
        PlaythoraUtility.showProgressDialog(ActivityLogDetail.this);
        try {
            AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", String.valueOf(id));
            params.add("comment", commentText);
            Log.e("PARAMMS", String.valueOf(params));
            mHttpClient.post(Config.ACTIVITY_COMMENT, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                                    tempList.add(new CommentList(commentText, id, new UserData(mPref.getProfilePic()), "few seconds ago"));
                                    mComments += 1;
                                    noOfCmts.setText(mComments + "");
                                    adapter.Refresh(tempList);
                                    comment.setText("");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(ActivityLogDetail.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ActivityLogDetail.this, LoginActivity.class);
                                    startActivity(intent);
                                    finishAffinity();
                                } else
                                    Toast.makeText(ActivityLogDetail.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    Toast.makeText(ActivityLogDetail.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
            PlaythoraUtility.hideProgressDialog();
        }


    }

    private void likeActivity(int id, final boolean like) {
        String URL = like ? Config.ACTIVITY_LIKE : Config.ACTIVITY_UNLIKE;
        try {
            AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("id", String.valueOf(id));
            Log.e("PARAMMS", String.valueOf(params));
            mHttpClient.post(URL, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            try {
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(ActivityLogDetail.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ActivityLogDetail.this, LoginActivity.class);
                                    startActivity(intent);
                                    finishAffinity();
                                } else
                                    Toast.makeText(ActivityLogDetail.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    Toast.makeText(ActivityLogDetail.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
    }

    private void showKeyBoard() {
        comment.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(comment, InputMethodManager.SHOW_IMPLICIT);
    }

    private void setFont() {
        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        sendComment.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        comment.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mLevelText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mTypeText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        mStatusText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    private void initializeAllComponents() {
        context = this;
        toolbar = (Toolbar) findViewById(R.id.activitydetailToolbar);
        toolbarTitle = (TextView) findViewById(R.id.create_team_toolbar_title);
        commentList = (RecyclerView) findViewById(R.id.comment_list);
        sendComment = (Button) findViewById(R.id.send_btn);
        comment = (EditText) findViewById(R.id.detail_cmd_text);
        likes = (RelativeLayout) findViewById(R.id.detail_like_status_relative);
        noOfLike = (TextView) findViewById(R.id.tv_activity_detail_likes);
        mImage = (ImageView) findViewById(R.id.detail_img);
        userImage = (CircleImageView) findViewById(R.id.detail_profile);
        mLevelText = (TextView) findViewById(R.id.tv_activity_detail_status);
        mTypeText = (TextView) findViewById(R.id.detail_type);
        mStatusText = (TextView) findViewById(R.id.detail_status_text);
        noOfCmts = (TextView) findViewById(R.id.activity_detail_cmd);
        mainRelative = (RelativeLayout) findViewById(R.id.main_relative);
        mComment = (RelativeLayout) findViewById(R.id.cmt_relative);
        mLikeButton = (ImageView) findViewById(R.id.activity_detail_like_button);
        mUnlikeButton = (ImageView) findViewById(R.id.activity_detail_unlike_button);
        progressBar = (ProgressBar) findViewById(R.id.comment_progress);
        nested = (NestedScrollView) findViewById(R.id.nestedScroll);
        //setupUI(mainRelative);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(ActivityLogDetail.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = getIntent();
            i.putExtra("POSITION", position);
            i.putExtra("ISLIKE", likeByUser);
            i.putExtra("COMMENTS", mComments);
            i.putExtra("LIKESCOUNT", mLikes);
            setResult(5, i);
            finish();
            return true;
        }
        return false;
    }

    public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder> {

        Context context;
        List<CommentList> commentModel;

        public CommentAdapter(Context context, List<CommentList> commentModel) {
            this.context = context;
            this.commentModel = commentModel;
        }

        public void Refresh(List<CommentList> commentModel) {
            this.commentModel = commentModel;
            notifyDataSetChanged();
        }

        @Override
        public CommentAdapter.CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_bubble, null);
            return new CommentHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(CommentAdapter.CommentHolder holder, int position) {
            final CommentList model = commentModel.get(position);

            holder.cmtText.setText(model.getContent());
            holder.time.setText(model.getDateTime());
            try {
                Glide.with(context).load(model.getUser().getProfile_pic()).error(R.drawable.circled_user).centerCrop().into(holder.userImage);
            } catch (Exception e) {
                holder.userImage.setImageResource(R.drawable.circled_user);
            }

        }

        @Override
        public int getItemCount() {
            return commentModel.size();
        }

        public class CommentHolder extends RecyclerView.ViewHolder {
            CircleImageView userImage;
            TextView cmtText, time;

            public CommentHolder(View itemView) {
                super(itemView);

                userImage = (CircleImageView) itemView.findViewById(R.id.img_user_image);
                cmtText = (TextView) itemView.findViewById(R.id.tv_cmd);
                time = (TextView) itemView.findViewById(R.id.time_ago);

                cmtText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
                time.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            }
        }
    }
}
