package com.yowzapp.playstore.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yowzapp.playstore.R;
import com.yowzapp.playstore.activty.CreateGameInviteFriends;
import com.yowzapp.playstore.model.GamePlayers;
import com.yowzapp.playstore.model.HomeMyTeamList;
import com.yowzapp.playstore.model.HomeTeamModel;
import com.yowzapp.playstore.utils.Config;
import com.yowzapp.playstore.utils.EndlessParentScrollListener;
import com.yowzapp.playstore.utils.PlaythoraUtility;
import com.yowzapp.playstore.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hemanth on 29/12/16.
 */
public class InvitePlayersTeams extends Fragment {

    RecyclerView recyclerView;
    PreferenceManager mPref;
    HomeTeamModel model;
    TextView s;
    Gson mGson;
    ProgressBar progressBar;
    TextView noPlayers;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;
    List<HomeMyTeamList> tempList;
    NestedScrollView nested;
    private TeamAdapter adapter;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLayoutManager;
    private int previousTotal = 0, pages = 1;
    private boolean loading = false;

    public InvitePlayersTeams(TextView toolbarTitle) {
        s = toolbarTitle;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View contentView = inflater.inflate(R.layout.invite_player_layout, container, false);
        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) contentView.findViewById(R.id.invite_players_recycler);
        progressBar = (ProgressBar) contentView.findViewById(R.id.following_progress_bar);
        noPlayers = (TextView) contentView.findViewById(R.id.noPlayers);
        nested = (NestedScrollView) contentView.findViewById(R.id.nested_player);
        noPlayers.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        //  invite = (TextView) contentView.findViewById(R.id.invitePlaythora);
        //  invite.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        final EndlessParentScrollListener endlessParentScrollListener = new EndlessParentScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                try {
                    Log.e("RESPONSEPAGonLoadMoreE", String.valueOf(model.isHasMorePages()));
                    if (model.isHasMorePages()) {
                        Log.e("page_DONE", page + "");
                        pages = page;
                        teamListRecycler(pages);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void scrollDown() {
            }

            @Override
            public void scrollUp() {
            }
        };

        nested.setOnScrollChangeListener(endlessParentScrollListener);


        try {
            teamListRecycler(pages);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

        return contentView;
    }

    private void teamListRecycler(int pages) {
        Log.e("teamList", "#####");
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            httpClient.get(Config.TEAM_LIST + "&page=" + pages, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);

                    try {
                        String s = new String(responseBody);
                        Log.e("TEAMLIST", s);
                        model = new HomeTeamModel();
                        mGson = new Gson();
                        model = mGson.fromJson(s, HomeTeamModel.class);
                        Log.e("RESPONSE", String.valueOf(model.getSuccess().getAll_teams()));

                        if (model.getSuccess().getAll_teams().size() != 0) {
                            Log.e("RESPONSE", String.valueOf(model.getSuccess().getAll_teams().size()));
                            if (model.getCurrentPage() > 1) {
                                for (int i = 0; i < model.getSuccess().getAll_teams().size(); i++) {
                                    tempList.add(model.getSuccess().getAll_teams().get(i));
                                }

                                Log.e("venueList", String.valueOf(model.getCurrentPage()));
                                adapter.RefreshPagination(tempList, true);
                            } else if (recyclerView.getAdapter() == null) {
                                tempList = model.getSuccess().getAll_teams();
                                adapter = new TeamAdapter(getActivity(), model.getSuccess().getAll_teams());
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                noPlayers.setVisibility(View.GONE);
                            } else {
                                noPlayers.setVisibility(View.VISIBLE);
                                noPlayers.setText("You are not following any teams");
                            }
                        } else {
                            noPlayers.setVisibility(View.VISIBLE);
                            noPlayers.setText("You are not following any teams");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    private void callAlertMaxPlayers() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        View mView = layoutInflaterAndroid.inflate(R.layout.user_limit_pop_up, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setView(mView);
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

        final TextView maxPlayer = (TextView) mView.findViewById(R.id.maxPlayer);
        final TextView okay = (TextView) mView.findViewById(R.id.ok);
        final TextView unSelect = (TextView) mView.findViewById(R.id.adminDetails);

        maxPlayer.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        okay.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
        unSelect.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));


        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogAndroid.dismiss();
            }
        });


        alertDialogAndroid.show();
    }

    public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamHolder> {

        List<HomeMyTeamList> arrayList;
        private Context context;
        private boolean[] mCheckedState;

        public TeamAdapter(FragmentActivity activity, List<HomeMyTeamList> all_teams) {
            this.context = activity;
            this.arrayList = all_teams;
            mCheckedState = new boolean[this.arrayList.size()];
            for (int i = 0; i < this.arrayList.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < this.arrayList.size(); i++) {
                for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                    if (CreateGameInviteFriends.stringArrayList.get(j).getType().equalsIgnoreCase("team")) {
                        if (this.arrayList.get(i).getId() == CreateGameInviteFriends.stringArrayList.get(j).getId()) {
                            //AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                            mCheckedState[i] = true;
                            //break;
                        }
                    }
                }
            }
        }


        public void RefreshPagination(List<HomeMyTeamList> newString, boolean pagination) {
            if (pagination) {
                mCheckedState = new boolean[this.arrayList.size()];
                this.arrayList = newString;
                for (int i = 0; i < this.arrayList.size(); i++) {
                    mCheckedState[i] = false;
                }
                Log.e("stringList", CreateGameInviteFriends.stringArrayList.size() + "," + this.arrayList.size());
                for (int i = 0; i < this.arrayList.size(); i++) {
                    for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                        if (CreateGameInviteFriends.stringArrayList.get(j).getType().equalsIgnoreCase("team")) {
                            if (this.arrayList.get(i).getId() == CreateGameInviteFriends.stringArrayList.get(j).getId()) {
                                Log.e("stringListID", String.valueOf(newString.get(j).getId()));
                                // AddMembersActivity.stringList.add(AddMembersActivity.tempList.get(j));
                                mCheckedState[i] = true;
                                break;
                            }
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }


        @Override
        public TeamHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.invite_player_item_layout, viewGroup, false);
            TeamHolder listHolder = new TeamHolder(mainGroup);
            return listHolder;

        }


        @Override
        public int getItemCount() {
            return arrayList.size();

        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public void onBindViewHolder(final TeamHolder holder, final int position) {
            final HomeMyTeamList model = arrayList.get(position);


            final TeamHolder mainHolder = (TeamHolder) holder;

            try {
                mainHolder.name.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
                mainHolder.name.setText(model.getName());
                mainHolder.count.setText("Members: " + String.valueOf(model.getMembers_count()));

                Log.e("name", model.getName());
                Log.e("pic", model.getCover_pic());
                Log.e("count", String.valueOf(model.getMembers_count()));

                if (mCheckedState[position])
                    mainHolder.checkBox.setChecked(true);
                else mainHolder.checkBox.setChecked(false);
               /* if(addPlayers>5){
                    mainHolder.checkBox.setClickable(false);
                    callAlertMaxPlayers();
                }else {
                    mainHolder.checkBox.setClickable(true);
                }*/

                mainHolder.checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mCheckedState[position]) {
                            //Toast.makeText(context,"checked",Toast.LENGTH_SHORT).show();
                            Log.e("CheckedFree", CreateGameInviteFriends.stringArrayList.size() + "");
                          /*  if(new Integer(CreateGameInviteFriends.playersNumber)<(CreateGameInviteFriends.stringArrayList.size()+arrayList.get(position).getMembers_count())){
                                mCheckedState[position] = false;
                                callAlertMaxPlayers();
                                notifyDataSetChanged();
                                return;
                            }*/
                            if (CreateGameInviteFriends.stringArrayList.size() > 0) {
                                for (int j = 0; j < CreateGameInviteFriends.stringArrayList.size(); j++) {
                                    if (CreateGameInviteFriends.stringArrayList.get(j).getId() == arrayList.get(position).getId()) {

                                    } else {
                                        for (int i = 0; i < arrayList.get(position).getMembers_count(); i++) {
                                            CreateGameInviteFriends.stringArrayList.add(new GamePlayers(arrayList.get(position).getId(), "team", arrayList.get(position).getCover_pic()));
                                            // mCheckedState[position]=true;
                                            mCheckedState[position] = true;
                                        }
                                        return;
                                    }
                                }
                            } else {
                                for (int i = 0; i < arrayList.get(position).getMembers_count(); i++) {
                                    CreateGameInviteFriends.stringArrayList.add(new GamePlayers(arrayList.get(position).getId(), "team", arrayList.get(position).getCover_pic()));
                                    // mCheckedState[position]=true;
                                    mCheckedState[position] = true;
                                }

                            }
                            Log.e("CheckedFreeADDED", CreateGameInviteFriends.stringArrayList.size() + "");
                            notifyDataSetChanged();

                        } else {
                            //Toast.makeText(context,"unchecked",Toast.LENGTH_SHORT).show();
                            // int size=CreateGameInviteFriends.stringArrayList.size();
                            ArrayList<GamePlayers> teamPlayer = new ArrayList<GamePlayers>();
                            for (int i = 0; i < CreateGameInviteFriends.stringArrayList.size(); i++) {
                                if (CreateGameInviteFriends.stringArrayList.get(i).getId() == arrayList.get(position).getId()) {

                                } else {
                                    teamPlayer.add(CreateGameInviteFriends.stringArrayList.get(i));
                                }
                            }
                            CreateGameInviteFriends.stringArrayList.clear();
                            CreateGameInviteFriends.stringArrayList = teamPlayer;
                            //notifyDataSetChanged();
                            Log.e("CheckedFreeRemoved", CreateGameInviteFriends.stringArrayList.size() + "");
                            mCheckedState[position] = false;
                            notifyDataSetChanged();
                        }

                    }


                });

            } catch (NullPointerException e) {

            }

            try {
                Glide.with(context).load(model.getCover_pic()).error(R.drawable.circled_user).centerCrop().into(mainHolder.userImage);
            } catch (Exception e) {
                mainHolder.userImage.setImageResource(R.drawable.circled_user);
            }

            mainHolder.itemView.setTag(model);


        }

        private void showPopUp(String name) {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
            View mView = layoutInflaterAndroid.inflate(R.layout.make_admin_pop_up, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
            alertDialogBuilderUserInput.setView(mView);
            final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();

            final TextView userName = (TextView) mView.findViewById(R.id.userName);
            final TextView select = (TextView) mView.findViewById(R.id.select);
            final TextView unSelect = (TextView) mView.findViewById(R.id.unSelect);

            userName.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
            select.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));
            unSelect.setTypeface(PlaythoraUtility.getFont(getActivity(), Config.RALEWAY_REGULAR));


            userName.setText(name);

            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogAndroid.dismiss();
                }
            });

            unSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogAndroid.dismiss();
                }
            });

            alertDialogAndroid.show();
        }

        public class TeamHolder extends RecyclerView.ViewHolder {
            CircleImageView userImage;
            TextView name, count;
            CheckBox checkBox;
            RelativeLayout makeAdmin;

            public TeamHolder(View view) {
                super(view);
                this.userImage = (CircleImageView) view.findViewById(R.id.following_user_image);
                this.name = (TextView) view.findViewById(R.id.userName);
                this.count = (TextView) view.findViewById(R.id.count);
                this.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                this.makeAdmin = (RelativeLayout) view.findViewById(R.id.makeAdminLayout);
            }
        }
    }
}
