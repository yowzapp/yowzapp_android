package com.yowzapp.playstore;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.yowzapp.playstore.activty.HomeScreenActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;

/**
 * Created by maheshprajapat on 2/22/18.
 */

@RunWith(AndroidJUnit4.class)
public class HomeScreenTest {

    @Rule
    public ActivityTestRule<HomeScreenActivity> homeScreenActivityActivityTestRule =
            new ActivityTestRule<HomeScreenActivity>(HomeScreenActivity.class);

    @Test
    public void check_flow() throws Exception{
        onView(withId(R.id.tv_top_loc)).perform(click());
        onView(withId(R.id.tv_games)).perform(click());
        onView(withId(R.id.tv_activity_log)).perform(click());
        onView(withId(R.id.tv_my_team)).perform(click());
    }
}
