package com.yowzapp.playstore;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.yowzapp.playstore.activty.HomeScreenActivity;
import com.yowzapp.playstore.activty.LoginActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by maheshprajapat on 2/22/18.
 */

@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);

    private void launchActivityWithIntent()
    {
        Intent intent = new Intent();
        mActivityRule.launchActivity(intent);
    }

    @Test
    public void loginSuccess() throws Exception {

        launchActivityWithIntent();

        onView(withId(R.id.loginEmailId)).perform(typeText("mahesh@designstring.com"));
        onView(withId(R.id.loginPassword)).perform(typeText("password123"), closeSoftKeyboard());
        onView(allOf(withId(R.id.next), isDescendantOfA(withId(R.id.next_layout)))).perform(click());

        //Check if intent with Activity 2 its been launched
        intended(hasComponent(HomeScreenActivity.class.getName()));
    }

    @Test
    public void loginFailure() throws Exception {
        onView(withId(R.id.loginEmailId)).perform(typeText("mahesh@designstring.com"));
        onView(withId(R.id.loginPassword)).perform(typeText("password123"));
        onView(allOf(withId(R.id.next), isDescendantOfA(withId(R.id.next_layout)))).perform(click());

        //Check if intent with Activity 2 its been launched
        intended(hasComponent(HomeScreenActivity.class.getName()));
    }
}
